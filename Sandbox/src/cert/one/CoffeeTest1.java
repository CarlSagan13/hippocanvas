package cert.one;


class CoffeeDrink {
	CoffeeTest1.CoffeeSize size;
}

public class CoffeeTest1 {
	enum CoffeeSize { BIG, HUGE, OVERWHELMING }  // this cannot be
	// private or protected
	public static void main(String[] args) {
		CoffeeDrink drink = new CoffeeDrink();
		drink.size = CoffeeSize.BIG;        // enum outside class
	}
}