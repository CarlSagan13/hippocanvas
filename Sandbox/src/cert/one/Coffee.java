package cert.one;

enum CoffeeSize {
    // 8, 10 & 16 are passed to the constructor
    BIG(8, "tall"), HUGE(10,"grande"), OVERWHELMING(16, "venti");
    CoffeeSize(int ounces, String starbucks) { // constructor
      this.ounces = ounces;
      this.starbucks = starbucks;
    }

    private int ounces;      // an instance variable
    public int getOunces() {
      return ounces;
    }
    private String starbucks;
    public String getStarbucks() {
    	return starbucks;
    }
}

class Coffee {
   CoffeeSize size;    // each instance of Coffee has an enum

   public static void main(String[] args) {
      Coffee drink1 = new Coffee();
      drink1.size = CoffeeSize.BIG;

      Coffee drink2 = new Coffee();
      drink2.size = CoffeeSize.OVERWHELMING;

      System.out.println(drink1.size.getOunces()); // prints 8
      for(CoffeeSize cs: CoffeeSize.values())
         System.out.println(cs + " " + cs.getOunces() + " " + cs.getStarbucks());
   }
}
