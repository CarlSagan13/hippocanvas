package cert.one;

public class OneMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		SubClass subClass = new SubClass();
//		System.out.println(subClass.saySomething());
		
//		SuperClass superClass = new SuperClass();
//		System.out.println(superClass.sayAnotherThing(5, "Holy!"));
		
		ClassB b = new ClassB();
		System.out.println(b.saySomethingElse());
	}
}
class ClassB extends AbstractClassB {

	@Override
	String saySomething() {
		// TODO Auto-generated method stub
		return null;
	}

}
abstract class AbstractClassB {
	abstract String saySomething();
	String saySomethingElse() {
		return "Something Else!";
	}
}
