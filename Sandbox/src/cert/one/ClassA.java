package cert.one;

import java.io.IOException;

public class ClassA implements InterfaceA{
	public void doSomething() throws IOException {
		int integer = 0;
		int anotherInteger = 5;
		if (integer == 0) {
			throw new IOException("Whatever dude");
		}
	}
	
	public void doSomethingB() {
		//nothing much
	}
}
