package cert.one;

public class SuperClass {
	String saySomething() {
		return "howdy!";
	}
	
	String sayAnotherThing(int repeat, final String thingToSay) {
		StringBuffer buffer = new StringBuffer();
//		thingToSay = "bollocks!";
		for (int i = 0; i < repeat; i++) {
			buffer.append(thingToSay).append(" ");
		}
		return buffer.toString();
	}
}
