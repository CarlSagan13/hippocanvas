package cert.one;

public abstract class Car extends Vehicle {
	  public void goUpHill() {
		  // whatever
	  }
	  public void doCarThings() {
	    // special car code goes here
	  }
}