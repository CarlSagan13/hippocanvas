package sandbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

@SandboxAnnotation(
		description = "Show global map entry contents",
 		parameterDescriptions = {
 	            SandboxParameters.FOCUSED_NAME, "globalKey",
 	            SandboxParameters.YESNO_NAME, "showContents"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool4 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
	        String[] globalKey = getStringArray("globalKey");
	        boolean showContents = getBoolean("showContents");

	        if (params.globals.map.size() > 0) {
		        if (globalKey == null) {
		        	printLine("Select a global key");
		        	globalKey = new String[] {"default"};
		        }

		        Set<String> globalKeys = params.globals.map.keySet();
	            Iterator<String> iterGlobalKeys = globalKeys.iterator();
	            printLine("Keys in map: ");
	            while (iterGlobalKeys.hasNext()) {
	                String key = iterGlobalKeys.next();
	                if (getGlobalMapConnection(key) != null && getGlobalMapConnection(key).getObj() != null) {
	                    printIt(key + " --> " + getGlobalMapConnection(key).getObj().getClass().getName());
	                    if (getGlobalMapConnection(key).getOwner() != null) {
	                    	printIt(" :: Owned by " + getGlobalMapConnection(key).getOwner());
	                    }
	                } else {
	                    printIt(key + " --> null");
	                }
	                printLine();
	            }
	            printLine(SandboxParameters.VISUAL_SEPARATOR);

	            SandboxConnection connection = getGlobalMapConnection(globalKey[0]);
	            if (connection != null && connection.getOwner() != null) {
	                printLine(globalKey[0] + " is owned by " + connection.getOwner().getClass().getName());
	            } else {
	                Object obj = getGlobalMapObject(globalKey[0]);
	                if (obj != null) {
	                    translate(obj, showContents);
	                } else {
	                    printLine(globalKey[0] + " not in map");
	                }
	            }
	        } else {
	            printLine("Global map is empty");
	        }
	        //END TEST
	 }
}
