// Sandbox.java
package sandbox; 
 
 import java.awt.Dimension; 
 import java.awt.Toolkit;
import java.util.List;

import javax.swing.SwingUtilities; 
import javax.swing.UIManager;

 public class Sandbox implements SandboxCommon { 
	
 	 boolean packFrame =  false; 
 	 private SandboxTest frame; 
 
 	 public Sandbox(String[] arguments) {
 		 this(arguments, null);
 	 }

 	 public Sandbox(String[] arguments, List<Object> mapObjects) { 
 	 	 sandboxLogger.debug("Starting"); 
 	 
 	 	 try { 
 	 	 	 SandboxParameters.targetPackageName = Sandbox.class.getPackage().getName(); 
 	 	 	 if (arguments.length > 0) { 
 	 	 		 SandboxParameters.targetPackageName =  arguments[0]; 
 	 	 	 } 
 	 	 	 if (arguments.length > 1) { //got the password passed, so set the environment too
 	 	 	 	 SandboxParameters.passedPassword = Utilities.base64Decode (arguments[1]); 
 	 	 	 } 
 	 	 	 
 	 	 	 frame =  new SandboxTest(); 
 	 	 	 if (packFrame) { 
 	 	 	 	 frame.pack(); 
 	 	 	 } else { 
 	 	 	 	 frame.validate(); 
 	 	 	 } 
 

	 	 	 Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); 
 	 	 	 Dimension frameSize = frame.getSize(); 
 	 	 	 if (frameSize.height > screenSize.height) { 
 	 	 	 	 frameSize.height = screenSize.height; 
 	 	 	 } 
 	 	 	 if (frameSize.width > screenSize.width) { 
 	 	 	 	 frameSize.width =  screenSize.width; 
 	 	 	 } 
 	 	 	 frame.setLocation(0, 0); 
 //	 	 	 frame.setLocation((screenSize.width - frameSize.width) / 2, 
 //	 	 	 	 	 (screenSize.height - frameSize.height) / 2); 
 	 	 	 frame.setMinimumSize(new Dimension(SandboxParameters.FRAME_HEIGHT, 
 	 	 	 	 	 SandboxParameters.FRAME_HEIGHT) ); 
 	 	 	 frame.setVisible(true); 
 	 	 	 
 	 	 } catch(Throwable thr) { 
 	 		sandboxLogger.info("Could not start "+ this.getClass().getName(), thr); 
 	 	 	 System.err.println(SandboxParameters.SANDBOX_CLOSED + " :: " + this.getClass().getName()+ " :: " + thr.getMessage()); 
 	 	 	 for (StackTraceElement ste : thr.getStackTrace()) { 
 	 	 	 	 System.err.println("dmh243A " + ste.toString()); 
 	 	 	 } 
 	 	 } 
 	 } 
 
 	 public static void main(String[ ] args) {
 		sandboxLogger.info("Starting"); 
 	 	 final String[] arguments = args;
 	 	 SwingUtilities.invokeLater(new Runnable() { 
 	 	 	 public void run() { 
 	 	 		 try { 
 	 	 	 	 	 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
	 	 	 	 } catch (Exception exception) { 
	 	 	 		sandboxLogger.info ("Cannot set to system look and feel", exception); 
 	 	 	 	 } 
 	 	 	 	 new Sandbox(arguments); 
 	 	 	 } 
 	 	 });
 	 } 
 }
