package sandbox;

import java.net.URLDecoder;
import java.util.Hashtable;

import javax.swing.JButton;

@SandboxAnnotation(
		description = "Run a recorded test",
 		parameterDescriptions = {
 		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool156 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String testString = getParameters().getFocusedText();
		 
		 params.runTest(testString);
		 //END TEST
	 }

}
