// Tool6.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Find plain string in files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "fileRegex,search,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
//			SandboxParameters.YESNO_NAME, "unlock",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class FindInFiles extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String search = getString("search");
		String regex = getString("fileRegex");
		String directory = getString("directory");
		boolean recurse = getBoolean("recurse");
//		boolean unlock = getBoolean("unlock");
		
		
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), 
				regex + " :: " + search + " :: " + directory);

		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(search));
		params.selectTab(params.getTabbedPane().getSelectedIndex());


		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			boolean finding = false;
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				if (file.isFile()) {
					String string = new String(Utilities.getFileBytes(file));
					if (string.indexOf(search) >= 0) {
						if (!finding) {
							printALine();
							finding = true;
						}
						printLine(file.getAbsolutePath());
					}
				}
				checkRunning();
			}
			if (finding) {
				printALine();
			}
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
}
