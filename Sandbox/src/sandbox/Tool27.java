package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Compare directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"directoryOne,directoryTwo"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool27 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TOOL
		String directoryOne = getString("directoryOne");
		String directoryTwo = getString("directoryTwo");

		File directory = new File(directoryOne);
		List<String> alListFile1 = new ArrayList<String>();
		for (File file : sandbox.Utilities.listFilesDirs(directory,null,false)){
			alListFile1.add(file.getName());
		}
		directory = new File(directoryTwo);
		List<String> alListFile2 = new ArrayList<String> ();
		for (File file : sandbox.Utilities.listFilesDirs(directory,null,false)){
			alListFile2.add(file.getName ());
		}

		Set <String> set1= new HashSet<String> ();
		for (String line : alListFile1){
			set1.add(line);
		}
		
		Set<String> set2 = new HashSet <String> ();
		for (String line : alListFile2){
			set2.add(line);
		}

		List<String> bothLists = new ArrayList<String> ();
		List<String> listOneOnly = new ArrayList<String> ();
		for (Object line : set1.toArray()){
			if (set2.contains(line)){
				bothLists.add((String)line);
			}else {
				listOneOnly.add((String)line);
			}
		}
		List<String> listTwoOnly = new ArrayList<String> ();
		for (Object line : set2.toArray()){
			if (!set1.contains (line)){
				listTwoOnly.add((String)line);
			}
		}

		Collections.sort(bothLists);
		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nBOTH LISTS\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : bothLists){
			printLine(line);
		}
		
		Collections.sort(listOneOnly);
		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST ONE ONLY ("+ directoryOne + ")\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listOneOnly){
			printLine(line);
		}
		
		Collections.sort(listTwoOnly);
		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST TWO ONLY ("+ directoryTwo + ")\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listTwoOnly){
			printLine(line);
		}
		//END TOOL
	}
}