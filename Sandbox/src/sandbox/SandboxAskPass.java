package sandbox;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
public class SandboxAskPass {
	 public static void main(String[]args){	 
		 JPasswordField pwd = new JPasswordField(10);
	 	 int action = JOptionPane.showConfirmDialog(null, pwd, "Enter Password", JOptionPane.OK_CANCEL_OPTION);
	 	 if (action < 0){
	 	 	 System.err.println("CANCELLED");
	 	 } else {
	 	 	 System.out.println(pwd.getPassword());
	 	 }
	 }
}
