package sandbox.certification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with global Queue.class.getName() objects", parameterDescriptions = {

		"11", "values", "21", "key",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test34 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] values = getStringArray("values");
		String key = getString("key");

		PriorityQueue<T30> priorityQueue = (PriorityQueue<T30>) this.getGlobalMapObject(PriorityQueue.class.getName());
		T30[] t30Array = new T30[1];
		T30[] t30s = priorityQueue.toArray(t30Array);
		printLine("From " + priorityQueue.getClass().getName() + ": " + priorityQueue.contains(new T30(key)));
		while (!priorityQueue.isEmpty()) {
			printIt(priorityQueue.poll().value + " ");
		}
		printLine();
		printLine();
		priorityQueue = reload(priorityQueue, t30s);

		printLine("Offer:");
		priorityQueue.offer(new T30(values[0]));
		while (!priorityQueue.isEmpty()) {
			printIt(priorityQueue.poll().value + " ");
		}
		printLine();
		printLine();
		priorityQueue = reload(priorityQueue, t30s);

		printLine("Add:");
		priorityQueue.add(new T30(values[1]));
		while (!priorityQueue.isEmpty()) {
			printIt(priorityQueue.poll().value + " ");
		}
		printLine();
		printLine();
		priorityQueue = reload(priorityQueue, t30s);

		printLine("Peek:");
		printLine(priorityQueue.peek().value);
		printLine();

		printLine("Remove '" + key + "':");
		printLine("Result: " + priorityQueue.remove(new T30(key)));
		while (!priorityQueue.isEmpty()) {
			printIt(priorityQueue.remove().value + " ");
		}
		printLine();
		printLine();
		priorityQueue = reload(priorityQueue, t30s);

		printLine("Polling from global " + PriorityQueue.class.getSimpleName());
		while (!priorityQueue.isEmpty()) {
			printIt(priorityQueue.poll().value + " ");
		}
		printLine();
		printLine();
		priorityQueue = reload(priorityQueue, t30s);
		// END TEST
	}

	PriorityQueue<T30> reload(PriorityQueue<T30> queue, T30[] t30s) {
		for (T30 t30 : t30s) {
			queue.add(t30);
		}
		return queue;
	}
}
