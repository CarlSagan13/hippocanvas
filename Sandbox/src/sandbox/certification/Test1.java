package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Demonstrate can't instantiate a default class; other access stuff", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test1 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		cert.one.ClassA a;

		new JustForGiggles(); // Show that this inner class, although default by
								// default,
								// can be instantiated because it's in this
								// package.

		// NOPE cert.one.DefaultClass dc; //Would have to have been declared
		// public for this to work in this package
		// NOPE new AbstractClass(); //Because you can't instantiate an abstract
		// class
		printLine(new ExtendsAbstractClassClass().number);
		// END TEST
	}

	final class FinalClass {

	}

	// NOPE class ExtendsFinalClassClass extends FinalClass { } //Cannot extend
	// a final class
	abstract class AbstractClass {
		int number = 42;
		public static final int number1 = 0; // this is the same as 'number'
												// only explicitly declared

		void function() {
		}
	}

	class ExtendsAbstractClassClass extends AbstractClass {
	};
	
	//NOPE class ImplementsAbstractClassClass implements AbstractClass {};

	interface Interface {
		int number = 56;

		void functionBlah();
	}

	class ImplementsAbstractClassClass implements Interface {
		// NOPE void function() { //Cannot reduce visibility; I guess interfaces
		// imply public functions
		public void functionBlah() {
			// NOPE number = 85; //default is public static final
		}
	}

	class JustForGiggles {
	}

}
