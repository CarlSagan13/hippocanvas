package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Var-args", parameterDescriptions = {

		// "1", "string",
		SandboxParameters.FOCUSED_NAME, "strings",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test6 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] strings = getStringArray("strings");

		doSomething(strings);
		doSomething("here's another");
		doSomething("um", "dois", "tres", "quatro");
		// END TEST
	}

	private void doSomething(String... s) {
		printLine("Entering doSomething(): ");
		printIt("s instanceof String[] is ");
		printLine(s instanceof String[]);
//		printLine(whgw(s));
		printLine(s);
		printLine("Leaving doSomething()");
	}

	private String whgw(String[] sa) {
		StringBuffer sb = new StringBuffer();
		for (String s : sa) {
			sb.append(s + "::");
		}
		return sb.toString();
	}
}
