package sandbox.certification;

public class T19A {
	T19A() {
		System.out.println("no-arg constructor");
	}

	T19A(int number) {
		System.out.println("1-arg constructor");
	}

	static {
		System.out.println("1st static init");
	}

	{
		System.out.println("1st instance init");
	}
	{
		System.out.println("2nd instance init");
	}

	static {
		System.out.println("2nd static init");
	}

	public static void main(String[] args) {
		new T19A();
		new T19A(7);
	}
}
