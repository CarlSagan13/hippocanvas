package sandbox.certification;

import java.util.ArrayList;
import java.util.List;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Using <?>", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test42 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");

		List<? extends Number> genericList = new ArrayList<Number>();
		// NOPE genericList.add(new Integer(2009)); //Can be declared but not modified
		List<Number> anotherNumberList = new ArrayList<Number>();
		anotherNumberList.add(new Integer(2009)); // This works because not
													// using generic <? extends
													// Number>
		List<? extends Number> numberListTwo = getIntegerList();
		printLine(numberListTwo); // See? accesses but not modifiable
		printLine(numberListTwo.get(0) instanceof Float);

		List<? extends Animal> animalExtendsAnimalList = new ArrayList<Animal>();
		List<? extends Animal> animalExtendsDogList = new ArrayList<Dog>();
		List<? extends Animal> animalExtendsBeagelList = new ArrayList<Beagel>();

		// NOPE List<? extends Dog> dogExtendsAnimalList = new ArrayList<Animal> ();
		List<? extends Dog> dogExtendsDogList = new ArrayList<Dog>();
		List<? extends Dog> dogExtendsBeagelList = new ArrayList<Beagel>();

		// NOPE List<? extends Beagel> beagelExtendsAnimalList = new
		// ArrayList<Animal> ();
		// NOPE List<? extends Beagel> beagelExtendsDogList = new ArrayList<Dog>
		// ();
		List<? extends Beagel> beagelExtendsBeagelList = new ArrayList<Beagel>();

		List<? super Animal> animalSuperAnimalList = new ArrayList<Animal>();
		// NOPE List<? super Animal> animalSuperDogList = new ArrayList<Dog> ();
		// NOPE List<? super Animal> animalSuperBeagelList = new
		// ArrayList<Beagel> ();

		List<? super Dog> dogSuperAnimalList = new ArrayList<Animal>();
		List<? super Dog> dogSuperDogList = new ArrayList<Dog>();
		// NOPE List<? super Dog> dogSuperBeagelList = new ArrayList<Beagel> ();

		List<? super Beagel> beagelSuperAnimalList = new ArrayList<Animal>();
		List<? super Beagel> beagelSuperDogList = new ArrayList<Dog>();
		List<? super Beagel> beagelSuperBeagelList = new ArrayList<Beagel>();

		// In summary:
		List<? super Dog> superDogList = new ArrayList<Animal>();
		List<? extends Dog> extendsDogList = new ArrayList<Beagel>();
		// END TEST
	}

	List<? extends Number> getIntegerList() {
		List<Number> list = new ArrayList<Number>();
		list.add(new Integer(1492));
		return list;
	}

	class Animal {
	};

	class Dog extends Animal {
	};

	class Beagel extends Dog {
	};
}
