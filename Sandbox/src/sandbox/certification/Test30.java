package sandbox.certification;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import certification.chapter.seven.StringLengthComparator;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Put objects in global map",
 		parameterDescriptions = {
 				"19", "keys", 
 				"20", "values"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test30 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] keys = getStringArray("keys");
		String[] values = getStringArray("values");

		// refresh global map
		printLine("Clearing global map");
		params.getGlobals().getMap().clear();

		Map<String, T30> hashMap = new HashMap<String, T30>();
		int index = 0;
		for (String key : keys) {
			hashMap.put(key, new T30(values[index++]));
		}
		this.addToGlobalMap(hashMap);

		Map<String, T30> hashTable = new Hashtable<String, T30>();
		index = 0;
		for (String key : keys) {
			hashTable.put(key, new T30(values[index++]));
		}
		this.addToGlobalMap(hashTable);

		Map<String, T30> treeMap = new TreeMap<String, T30>();
		index = 0;
		for (String key : keys) {
			treeMap.put(key, new T30(values[index++]));
		}
		this.addToGlobalMap(treeMap);

		Map<String, T30> linkedHashMap = new LinkedHashMap<String, T30>();
		index = 0;
		for (String key : keys) {
			linkedHashMap.put(key, new T30(values[index++]));
		}
		this.addToGlobalMap(linkedHashMap);

		Set<T30> hashSet = new HashSet<T30>();
		index = 0;
		for (String key : keys) {
			hashSet.add(new T30(values[index++]));
		}
		this.addToGlobalMap(hashSet);

		Set<T30> treeSet = new TreeSet<T30>();
		index = 0;
		for (String key : keys) {
			treeSet.add(new T30(values[index++]));
		}
		this.addToGlobalMap(treeSet);

		Set<T30> linkedHashSet = new LinkedHashSet<T30>();
		index = 0;
		for (String key : keys) {
			linkedHashSet.add(new T30(values[index++]));
		}
		this.addToGlobalMap(linkedHashSet);

		List<T30> arrayList = new ArrayList<T30>();
		index = 0;
		for (String key : keys) {
			arrayList.add(new T30(values[index++]));
		}
		this.addToGlobalMap(arrayList);

		List<T30> vector = new Vector<T30>();
		index = 0;
		for (String key : keys) {
			vector.add(new T30(values[index++]));
		}
		this.addToGlobalMap(vector);

		List<T30> linkedList = new LinkedList<T30>();
		index = 0;
		for (String key : keys) {
			linkedList.add(new T30(values[index++]));
		}
		this.addToGlobalMap(linkedList);

		Comparator<T30> comparator = new StringLengthComparator();
		Queue<T30> priorityQueue = new PriorityQueue<T30>(keys.length, comparator);
		index = 0;
		for (String key : keys) {
			priorityQueue.add(new T30(values[index++]));
		}
		this.addToGlobalMap(priorityQueue);
		// END TEST
	}

	class StringLengthComparator implements Comparator<T30> {
		@Override
		public int compare(T30 x, T30 y) {
			// Assume neither string is null. Real code should
			// probably be more robust
			if (x.value.length() < y.value.length()) {
				return -1;
			}
			if (x.value.length() > y.value.length()) {
				return 1;
			}
			return 0;
		}
	}
}
