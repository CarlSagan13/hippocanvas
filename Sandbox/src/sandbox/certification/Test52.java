package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.certification.Test51.StaticClassT51;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Simulate main()", parameterDescriptions = {

		// "1", "args",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "args",
		// SandboxParameters.PASSWORD_NAME, "args"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test52 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");

		new MainClass().main(null);
		// END TEST
	}

	class MainClass {
		public void main(String[] args) {
			printLine("Hello from inside " + MainClass.class.getName());
			MainClass mainClass = new MainClass();

			mainClass.new Food().p.pop();
			mainClass.new Food().pa.pop();
		}

		class Popcorn {
			public void pop() {
				printLine("popcorn");
			}
		}

		class Food {
			Popcorn p = new Popcorn();
			Popcorn pa = new Popcorn() {
				public void pop() {
					printLine("anonymous popcorn");
				}
			};
		}
	}
}
