package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Final variables, and instance references", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test8 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		final int hello = 0;

		printLine(hello);

		// NOPE hello = 1; //cannot reassign a final primitive

		final ClassC congo = new ClassC();
		congo.setMarker("I be Congo the great, you cannot change me!");
		ClassC notCongo = new ClassC();
		notCongo.setMarker("I'm just a wimp!");
		notCongo = congo;
		printLine(notCongo.getMarker());
		// NOPE congo = notCongo; //cannot assign a final instance either,
		// although you CAN change the value of it...
		congo.setMarker("I just pooped my great Congo pants!");
		printLine(congo.getMarker());
		ClassC c = new ClassC();
		c.setMarker("One");
		c.setMarker("Two");
		printLine(c.getMarker());

		ClassC c1 = new ClassC();
		c1.setMarker("Three");
		printLine(c1.getMarker());

		c1 = c;
		printLine(c1.getMarker());
		// END TEST
	}

	class ClassC {
		private String marker = "uninitialized";

		public String getMarker() {
			return marker;
		}

		public void setMarker(String marker) {
			this.marker = marker;
		}
	}
}
