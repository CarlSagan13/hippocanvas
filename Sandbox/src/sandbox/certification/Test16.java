package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Parent child play", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test16 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		ClassA a1 = new ClassA();
		ClassB b1 = new ClassB();
		ClassA a2 = new ClassB();
		// ClassB b2 = new ClassA();

		printLine(a1.aMember);
		printLine(b1.aMember);
		printLine(a2.aMember);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine(a1.anInt + " :: " + b1.anInt + " :: " + a2.anInt);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		a1.aMethod();
		b1.aMethod();
		a2.aMethod();

		// END TEST
	}

	class ClassA {
		public String aMember = "a";
		public int anInt = 1;

		public void aMethod() {
			printLine("A");
		}
	}

	class ClassB extends ClassA {
		public String aMember = "b";
		public int anInt = 2;

		public void aMethod() {
			printLine("B");
		}
	}
}
