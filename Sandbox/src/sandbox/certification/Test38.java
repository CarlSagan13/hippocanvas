package sandbox.certification;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with TreeMap",
 		parameterDescriptions = {
 				"22", "times", 
 				"23", "lastFerryLeavesBefore", 
 				"24", "firstFerryLeavesAfter", 
 				"25", "ferryNames", 
 				"26", "timeFerry", 
 				"27", "range"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test38 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		String[] saTimes = getStringArray("times");
		int lastFerryLeavesBefore = getInteger("lastFerryLeavesBefore");
		int firstFerryLeavesAfter = getInteger("firstFerryLeavesAfter");
		String[] ferryNames = getStringArray("ferryNames");
		String[] timeFerry = getStringArray("timeFerry");
		String[] range = getStringArray("range");

		Integer[] iTimes = new Integer[saTimes.length];
		int index = 0;
		for (String time : saTimes) {
			iTimes[index++] = Integer.parseInt(time);
		}

		TreeMap<Integer, String> times = new TreeMap<Integer, String>();
		index = 0;
		for (Integer integer : iTimes) {
			times.put(integer, ferryNames[index++]);
		}

		printLine(
				"Last Ferry Leaves Before " + lastFerryLeavesBefore + " is " + times.lowerEntry(lastFerryLeavesBefore));
		printLine("First Ferry Leaves After " + firstFerryLeavesAfter + " is "
				+ times.higherEntry(firstFerryLeavesAfter));
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("ceiling " + lastFerryLeavesBefore + ": " + times.ceilingEntry(lastFerryLeavesBefore));
		printLine("higher " + lastFerryLeavesBefore + ": " + times.higherEntry(lastFerryLeavesBefore));
		printLine("floor " + lastFerryLeavesBefore + ": " + times.floorEntry(lastFerryLeavesBefore));
		printLine("lower " + lastFerryLeavesBefore + ": " + times.lowerEntry(lastFerryLeavesBefore));

		printLine("*** Backed Collections ***");
		SortedMap<Integer, String> submap = times.subMap(Integer.parseInt(range[0]), Integer.parseInt(range[1]));
		printLine("times: " + times + " :: submap: " + submap);
		times.put(Integer.parseInt(timeFerry[0]), timeFerry[1]);
		printLine("times: " + times + " :: submap: " + submap);
		try {
			printLine("Try to add " + timeFerry[2] + "=" + timeFerry[3] + " to the submap");
			submap.put(Integer.parseInt(timeFerry[2]), timeFerry[3]);
			printLine("times: " + times + " :: submap: " + submap);
		} catch (Throwable th) {
			printLine("Couldn't insert directly into sub map cause out of range: " + range[0] + "-" + range[1], th);
		}

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("times: " + times + " :: submap: " + submap);
		printLine("times.pollFirstEntry: " + times.pollFirstEntry());
		printLine("times: " + times + " :: submap: " + submap);
		printLine("times.pollFirstEntry (again): " + times.pollFirstEntry());
		printLine("times: " + times + " :: submap: " + submap);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("pollLastEntry: " + times.pollLastEntry());
		printLine(times);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("descendingMap: ");
		printLine(times.descendingMap());
		printLine(SandboxParameters.VISUAL_SEPARATOR);

		// END TEST
	}
}
