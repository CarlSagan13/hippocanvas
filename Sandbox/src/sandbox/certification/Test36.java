package sandbox.certification;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Sets with sorting", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test36 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");

		boolean[] ba = new boolean[5];
		// insert code here

		printLine(HashSet.class.getName());
		Set theSet = new HashSet();
		ba[0] = theSet.add("a");
		ba[1] = theSet.add(new Integer(42));
		ba[2] = theSet.add("b");
		ba[3] = theSet.add("a");
		ba[4] = theSet.add(new Object());
		for (int x = 0; x < ba.length; x++)
			printIt(ba[x] + " ");
		printLine("\n");
		for (Object o : theSet)
			printIt(o + " ");
		printLine();

		printLine(TreeSet.class.getName() + ": Will exception because this set is sortable,");
		printLine("but you cannot sort differing types 'cause you can't compare 'em");
		theSet = new TreeSet();
		ba[0] = theSet.add("a");
		ba[1] = theSet.add(new Integer(42)); //Exceptions here because different type
		ba[2] = theSet.add("b");
		ba[3] = theSet.add("a");
		ba[4] = theSet.add(new Object());
		for (int x = 0; x < ba.length; x++)
			printIt(ba[x] + " ");
		printLine("\n");
		for (Object o : theSet)
			printIt(o + " ");
		printLine();
		// END TEST
	}
}
