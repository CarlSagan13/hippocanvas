package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "MyWonderfulClass.class.getName() :: 'Argument local'",
parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test54 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		new MyWonderfulClass().go();
		// END TEST
	}

	class MyWonderfulClass {
		void go() {
			Bar b = new Bar();
			b.doStuff(new Foo() {
				public void foof() {
					printLine("foofy");
				} // end foof method
			}); // end inner class def, arg, and b.doStuff stmt.
		} // end go()
	} // end class

	interface Foo {
		void foof();
	}

	class Bar {
		void doStuff(Foo f) {
			f.foof();
		}
	}
}
