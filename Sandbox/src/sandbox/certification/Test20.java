package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Wrappers and boxing", parameterDescriptions = {

		"7", "number", "13", "float"
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		, }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test20 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String strNumber = getString("number");
		String strFloat = getString("float");

		// valueOf: static
		Double dbl = Double.valueOf(strFloat);

		// parseXxx: static
		double pdbl = Double.parseDouble(strFloat);

		// xxxValue: not static (on an instance)
		int idbl = dbl.intValue();

		String strInt = strNumber;

		Integer intObject = Integer.valueOf(strInt);

		printLine("intObject = " + intObject.intValue());

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		Integer y = 257;
		Integer x = y;
		printLine(y == x);
		y++;
		printLine(y == x);
		y--;
		printLine(y == x);
		printLine("y = " + y + " :: x = " + x);

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		int a = 10;
		int b = 10;
		printLine(a == b);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("Integer object of < 128 acts like primitive in comparisons:");
		Integer aObj = 11;
		Integer bObj = 11;
		printLine("Integer(" + aObj + ") != Integer(" + bObj + ") = " + (aObj != bObj));
		printLine("Integer(" + aObj + ") == Integer(" + bObj + ") = " + (aObj == bObj));
		printLine("Integer(" + aObj + ").equals(Integer(" + bObj + ")) = " + (aObj.equals(bObj)));
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("Integer object of >= 128 acts like object in comparisons:");
		Integer cObj = 1100;
		Integer dObj = 1100;
		printLine("Integer(" + cObj + ") != Integer(" + dObj + ") = " + (cObj != dObj));
		printLine("Integer(" + cObj + ") == Integer(" + dObj + ") = " + (cObj == dObj));
		printLine("Integer(" + cObj + ").equals(Integer(" + dObj + ")) = " + (cObj.equals(dObj)));
		// END TEST
	}

}
