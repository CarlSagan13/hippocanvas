package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Show that statics are class singletons (per class, not instance)", parameterDescriptions = {

		"1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test11 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");

		Test11Class t11c = new Test11Class();
		printLine(t11c.hello);
		Test11Class t11cTwo = new Test11Class();
		printLine(t11cTwo.hello);
		t11c.hello = string;
		printLine(t11cTwo.hello);
		// END TEST
	}

	static class Test11Class {
		static String hello = "initial value";

	}
}
