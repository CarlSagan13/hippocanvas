package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Threads chapter; exercise 9-2", parameterDescriptions = {

		"6", "synchronize",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test64 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		boolean synchronize = getBoolean("synchronize");
		StringBuffer sb = new StringBuffer("A");
		if (synchronize) {
			new InSync(sb).start();
			new InSync(sb).start();
			new InSync(sb).start();
		} else {
			new NotInSync(sb).start();
			new NotInSync(sb).start();
			new NotInSync(sb).start();
		}
		// END TEST
	}

	class InSync extends Thread {
		StringBuffer letter;

		public InSync(StringBuffer letter) {
			this.letter = letter;
		}

		public void run() {
			synchronized (letter) { // #1
				for (int i = 1; i <= 100; ++i)
					printLine(letter);
				printLine();
				char temp = letter.charAt(0);
				++temp; // Increment the letter in StringBuffer:
				letter.setCharAt(0, temp);
			} // #2
		}
	}

	class NotInSync extends Thread {
		StringBuffer letter;

		public NotInSync(StringBuffer letter) {
			this.letter = letter;
		}

		public void run() {
			for (int i = 1; i <= 100; ++i)
				printLine(letter);
			printLine();
			char temp = letter.charAt(0);
			++temp; // Increment the letter in StringBuffer:
			letter.setCharAt(0, temp);
		}
	}
}
