package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Instantiating an inner class", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test58 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");
		// Class57.IC57 ic57 = new Class57().new IC57();
		new Car().main(null);
		// END TEST
	}

	public class Car {
		class Engine {
			// insert code here
			{
				Car.this.drive();
			}
			{
				Car.Engine.this.justForGiggles();
			}

			Engine() {
				Car.this.drive();
			}

			void justForGiggles() {
				printLine("ha ha");
			}
		}

		public void main(String[] args) {
			new Car().go();
		}

		void go() {
			new Engine();
		}

		void drive() {
			printLine("hi");
		}
	}
}
