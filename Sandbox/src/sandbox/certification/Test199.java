package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.certification.Broom.B2;
import sandbox.certification.Test51.StaticClassT51;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Simulate main()", parameterDescriptions = {

		// "1", "args",
		SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "args",
		// SandboxParameters.PASSWORD_NAME, "args"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test199 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] args = getStringArray("args");

		new B199().main(args);
		// END TEST
	}

	static class BO199 {
		static class Nest {
			SandboxParameters params = SandboxParameters.createSandboxParameters();

			void go() {
				params.getTest(Test199.class.getSimpleName()).printLine("hi");
			}
		}
	}

	static class B199 {
		static class B2 {
			SandboxParameters params = SandboxParameters.createSandboxParameters();

			void goB2() {
				params.getTest(Test199.class.getSimpleName()).printLine("hi 2");
			}
		}

		public static void main(String[] args) {
			BO199.Nest n = new BO199.Nest(); // both class names
			n.go();
			B2 b2 = new B2(); // access the enclosed class
			b2.goB2();
		}
	}
}
