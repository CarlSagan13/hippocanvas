package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "pg 676", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test53 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		new Food().popIt();
		// END TEST
	}

	class Popcorn {
		SandboxTestBase thistest = SandboxParameters.createSandboxParameters().getTest(Test53.class.getSimpleName());

		public void pop() {
			thistest.printLine("popcorn");
		}
	}

	class Food {
		Popcorn p = new Popcorn() {
			public void sizzle() {
				printLine("anonymous sizzling popcorn");
			}

			public void pop() {
				printLine("anonymous popcorn");
			}
		};

		public void popIt() {
			p.pop(); // OK, Popcorn has a pop() method
			// NOPE p.sizzle(); // Not Legal! Popcorn does not have sizzle() to
			// be overridden in the first place
		}
	}
}
