package sandbox.certification;

import java.util.PriorityQueue;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Natural ordering", parameterDescriptions = {

		"29", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test39 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");

		String[] sa = { ">ff<", "> f<", ">f <", ">FF<" }; // ordered?
		PriorityQueue<String> pq3 = new PriorityQueue<String>();
		for (String s : sa)
			pq3.offer(s);
		for (String s : sa)
			printLine(pq3.poll() + " ");

		String[] sab = new String[string.length()];
		for (int i = 0; i < string.length(); i++) {
			sab[i] = String.valueOf(string.charAt(i));
		}
		PriorityQueue<String> pq3b = new PriorityQueue<String>();
		for (String s : sab)
			pq3b.offer(s);
		for (String s : sab)
			printLine(pq3b.poll() + " ");

		// END TEST
	}
}
