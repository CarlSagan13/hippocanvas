package sandbox.certification;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "AbstractTest",
 		parameterDescriptions = {
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test4 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");

		// NOPE AbstractTest nopeTest = new AbstractTest(); //Cannot instantiate
		// an abstract class w/o just-in-time
		AbstractTest abstractTest = new AbstractTest() { // Note that you can
															// instantiate this
															// abstract class
															// only because
															// of Just-In-Time concrete definition here:
			public int getNum() {
				return 22;
			}

			public String getStr() {
				return "boonda!";
			}

			public boolean getReal() {
				return true;
			}
		};
		AbstractTest.Bar abstractTestBar = abstractTest.new Bar() {
			public int getNum() {
				return 57;
			}
		};
		printLine("abstractTest.getNum() = " + abstractTest.getNum());
		printLine("abstractTest.getStr() = " + abstractTest.getStr());
		printLine("abstractTestBar.getNum() = " + abstractTestBar.getNum());
		// END TEST
	}
}

abstract class AbstractTest implements DavidInterface {
	public int getNum() {
		return 45;
	}

	public abstract String getStr();

	public abstract class Bar {
		public int getNum() {
			return 38;
		}
	}
}

interface DavidInterface {
	public boolean getReal();
}