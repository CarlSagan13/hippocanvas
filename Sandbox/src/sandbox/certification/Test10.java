package sandbox.certification;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import cert.one.Employee;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Deserialize volatile", parameterDescriptions = {

		"18", "filePath",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test10 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String filePath = getString("filePath");
		// printLine("Hello from " + this.getClass().getSimpleName());

		Employee e = null;
		FileInputStream fileIn = new FileInputStream(filePath);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		e = (Employee) in.readObject();
		in.close();
		fileIn.close();
		printLine("Deserialized Employee...");
		printLine(e.toString());
		e.mailCheck();
		// END TEST
	}
}
