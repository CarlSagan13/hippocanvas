package sandbox.certification;

import sandbox.SandboxParameters;

public class Junk {
	SandboxParameters params = SandboxParameters.createSandboxParameters();

	public static void main(String[] args) {
		AnimalTwo h = new Horse();
		h.eat(); // Legal, class Animal has an eat() method
	}
}

class Horse extends AnimalTwo {
	SandboxParameters params = SandboxParameters.createSandboxParameters();

	void buck() {
		params.printLine("buck!");
	}
}

class AnimalTwo {
	SandboxParameters params = SandboxParameters.createSandboxParameters();

	void eat() {
		params.printLine("eat!");
	}
}
