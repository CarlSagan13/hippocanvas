package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "More enum stuff", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test13 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		// printLine("Hello from " + this.getClass().getSimpleName());
		new Coffee().main(null);
		// END TEST
	}

	enum CoffeeSize {
		// 8, 10 & 16 are passed to the constructor
		BIG(8, "tall"), HUGE(10, "grande"), OVERWHELMING(16, "venti");
		CoffeeSize(int ounces, String starbucks) { // constructor
			this.ounces = ounces;
			this.starbucks = starbucks;
		}

		private int ounces; // an instance variable

		public int getOunces() {
			return ounces;
		}

		private String starbucks;

		public String getStarbucks() {
			return starbucks;
		}
	}

	class Coffee {
		CoffeeSize size; // each instance of Coffee has an enum

		public void main(String[] args) {
			Coffee drink1 = new Coffee();
			drink1.size = CoffeeSize.BIG;

			Coffee drink2 = new Coffee();
			drink2.size = CoffeeSize.OVERWHELMING;

			printLine(drink1.size.getOunces());
			printLine(drink1.size.getStarbucks());
			for (CoffeeSize cs : CoffeeSize.values())
				printLine(cs + " " + cs.getOunces() + " " + cs.getStarbucks());
		}
	}
}
