package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Inner classes", parameterDescriptions = {

		"1", "string", }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test50 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");

		new InnerTest50().main();

		class Jeez {
			public Jeez(String s) {
				printLine(this.getClass());
			}
		}
		new Jeez(string);
		// END TEST
	}

	class InnerTest50 {
		void main() {
			new InnerInnerTest50().seeOuter();
		}

		class InnerInnerTest50 {
			int aValue = 69;

			void seeOuter() {
				printLine("A: " + this);
				printLine("B: " + InnerTest50.this);
				doSomething();
			}

			void doSomething() {
				int methodValue = 1492;
				final int finalMethodValue = 1956;
				// NOPE new
				// InnerInnerInnerTest50_method_doSomething().seeOuterFromMethod();
				// //Not yet defined so can't instantiate
				class InnerInnerInnerTest50 {
					void seeOuterFromMethod() {
						printLine(aValue);
						// NOPE printLine(methodValue); //Cause the
						// doSomething() method is on the stack, once it
						// completes it dies
						// and its local member methodValue goes away also, but
						// the InnerInnerInnerTest50 object is on the heap
						// and could outlive the doSomething() method and thus
						// the methodValue member
						// so a reference to it cannot be guaranteed
						printLine(finalMethodValue);
						printLine("C: " + this);
						printLine("D: " + InnerInnerTest50.this);
						printLine("E: " + InnerInnerTest50.this.aValue);
						printLine("F: " + InnerTest50.InnerInnerTest50.this);
					}
				}
				new InnerInnerInnerTest50().seeOuterFromMethod();
			}
		}
	}
}
