package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with instantiation", parameterDescriptions = {

		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test19 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		new Bottom("howdy");
		// END TEST
	}

	class Top {
		public Top() {
			printLine("I'm in the Top no-arg constructor");
		}

		public Top(String s) {
			printLine("I'm in the Top(String) constructor");
		}
	}

	class Bottom extends Top {
		public Bottom(String s) {
			printLine("I'm in the Bottom(String) constructor");
		}
	}
}
