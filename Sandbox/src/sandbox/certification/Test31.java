package sandbox.certification;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with global Map.class.getName() objects", parameterDescriptions = {

		"21", "key",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test31 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String key = getString("key");

		Map<String, T30> hashMap = (HashMap<String, T30>) this.getGlobalMapObject(HashMap.class.getName());
		printLine("From " + hashMap.getClass().getName() + ": " + hashMap.get(key).value);
		for (T30 t30 : hashMap.values()) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();

		Map<String, T30> hashTable = (Hashtable<String, T30>) this.getGlobalMapObject(Hashtable.class.getName());
		printLine("From " + hashTable.getClass().getName() + ": " + hashTable.get(key).value);
		for (T30 t30 : hashTable.values()) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();

		Map<String, T30> treeMap = (TreeMap<String, T30>) this.getGlobalMapObject(TreeMap.class.getName());
		printLine("From " + treeMap.getClass().getName() + ": " + treeMap.get(key).value);
		for (T30 t30 : treeMap.values()) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine("Find out why this is not in natural order!");
		printLine();

		Map<String, T30> linkedHashMap = (LinkedHashMap<String, T30>) this
				.getGlobalMapObject(LinkedHashMap.class.getName());
		printLine("From " + linkedHashMap.getClass().getName() + ": " + linkedHashMap.get(key).value);
		for (T30 t30 : linkedHashMap.values()) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();
		// END TEST
	}
}
