package sandbox.certification;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import cert.one.Employee;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Serialize and volatile", parameterDescriptions = {

		"18", "filePath",
		SandboxParameters.POPPAD_NAME, "strings",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test9 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String filePath = getString("filePath");
		String[] strings = getStringArray("strings");

		Employee employee = new Employee();
		employee.name = strings[0];
		employee.address = strings[1];
		employee.SSN = 123456789;
		Employee.number = 69;

		printLine(employee);
		FileOutputStream fileOut = new FileOutputStream(filePath);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(employee);
		out.close();
		fileOut.close();
		// END TEST
	}
}
