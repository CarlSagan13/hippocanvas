package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Chapter 9; Self-test question 15",
 		parameterDescriptions = {
 				"6", "synchIt"

			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test68 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		boolean synchIt = getBoolean("synchIt");
		new DudesChat(synchIt).go();
		// END TEST
	}

	static class Dudes {
		static long flag = 0;

		synchronized void chatSynch(long id) {
			SandboxTestBase test = SandboxParameters.createSandboxParameters().getTest(Test68.class.getSimpleName());
			if (flag == 0)
				flag = id;
			for (int x = 1; x < 3; x++) {
				if (flag == id)
					test.printIt(id + ":s:yo ");
				else
					test.printIt(id + ":s:dude ");
			}
		}

		void chat(long id) {
			SandboxTestBase test = SandboxParameters.createSandboxParameters().getTest(Test68.class.getSimpleName());
			if (flag == 0)
				flag = id;
			for (int x = 1; x < 3; x++) {
				if (flag == id)
					test.printIt(id + ":u:yo ");
				else
					test.printIt(id + ":u:dude ");
			}
		}
	}

	static class DudesChat implements Runnable {
		static Dudes d;
		static boolean synchIt = false;

		public DudesChat(boolean synchIt) {
			DudesChat.synchIt = synchIt;
		}

		void go() {
			d = new Dudes();
			Thread dc1 = new Thread(new DudesChat(synchIt));
			dc1.start();

			Thread dc2 = new Thread(new DudesChat(synchIt));
			dc2.start();
		}

		public void run() {
			if (synchIt) {
				d.chatSynch(Thread.currentThread().getId());
			} else {
				d.chat(Thread.currentThread().getId());
			}
		}
	}
}
