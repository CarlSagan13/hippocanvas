package sandbox.certification;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class Test99 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = Arrays.class.getName();
	private String[] parameterDescriptions = { "1", "strings", "5", "classNames", "22", "numbers"
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"
	};

	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] strings = getStringArray("strings");
		String[] classNames = getStringArray("classNames");
		String[] numbers = getStringArray("numbers");
		// printLine("Hello from " + this.getClass().getSimpleName());

		List<Object> objects = new ArrayList<Object>();

		int index = 0;
		for (String className : classNames) {
			Class aClass = Class.forName(className);
			Constructor constructor = aClass.getConstructor(String.class);
			Class[] paramTypes = constructor.getParameterTypes();
			Object obj = null;
			for (Class paramClass : paramTypes) {
				if (paramClass.getCanonicalName().equals(String.class.getName())) {
					try {
						obj = constructor.newInstance(strings[index++]);
						objects.add(obj);
						break;
					} catch (java.lang.reflect.InvocationTargetException ite) {
						printLine("'" + strings[--index] + "' is the wrong format for this constructor");
					}
				}
				if (paramClass.getCanonicalName().equals(Integer.class.getName())) {
					try {
						obj = constructor.newInstance(numbers[index++]);
						objects.add(obj);
						break;
					} catch (java.lang.reflect.InvocationTargetException ite) {
						printLine("'" + strings[--index] + "' is the wrong format for this constructor");
					}
				}
			}
			// Object obj = constructor.newInstance(strings[index++]);
			// printLine(aClass.getCanonicalName());
			// Object obj = aClass.newInstance();
			// printLine(obj.getClass().getName());
		}

		// String string = "hello";
		// Integer integer = new Integer(0);

		Object[] objs = new Object[classNames.length];
		index = 0;
		for (Object object : objects) {
			objs[index++] = object;
		}
		// objs[0] = string;
		// objs[1] = integer;
		Arrays.sort(objs);
		for (Object obj : objs) {
			printLine(obj);
		}
		// END TEST
	}
}
