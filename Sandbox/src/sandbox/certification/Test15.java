package sandbox.certification;

import java.io.IOException;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with finally", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test15 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		try {
			printLine("test.try");
			doIt();
		} catch (IOException ioe) {
			printLine("test.catch");
			printLine(ioe.getMessage());
		} finally {
			printLine("test.finally");
		}

		// END TEST
	}

	void doIt() throws Exception {
		try {
			printLine("doIt.try");
			throw new IOException("This is an IOException");
		} catch (NullPointerException npe) {
			printLine("doIt.catch");
		} finally {
			printLine("doIt.finally");
		}

	}
}
