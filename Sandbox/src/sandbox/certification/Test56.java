package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
//import sandbox.certification.Broom.B2;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Broom.class.getName() :: Nested static class", parameterDescriptions = {

		// "1", "args",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test56 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");
		new Broom().main(null);
		// END TEST
	}
}

class BigOuter {
	static SandboxTestBase thistest = SandboxParameters.createSandboxParameters().getTest(Test56.class.getSimpleName());

	class Wasps {
	}

	static class Hornets {
	}

	static class Nest {
		SandboxParameters params = SandboxParameters.createSandboxParameters();

		void go() {
			thistest.printLine("hi");
			BigOuter.Nest nest = new Nest();
			BigOuter.Wasps wasps = new BigOuter().new Wasps();
			BigOuter.Hornets hornets = new Hornets();
			Broom.B1 b1 = new Broom().new B1();
			Broom.B2 b2 = new Broom.B2();
		}
	}
}

class BroomHilda {
}

class Broom {
	static SandboxTestBase thistest = SandboxParameters.createSandboxParameters().getTest(Test56.class.getSimpleName());

	class B1 {
	}

	static class B2 {
		SandboxParameters params = SandboxParameters.createSandboxParameters();

		void goB2() {
			thistest.printLine("hi 2");
			BigOuter.Nest n = new BigOuter.Nest();
			Broom.B1 b1 = new Broom().new B1();
		}
	}

	class JumpTheBroom {
	}

	public static void main(String[] args) {
		Broom.B1 b1 = new Broom().new B1();
		BigOuter.Nest n = new BigOuter.Nest(); // both class names
		n.go();
		B2 b2 = new B2(); // access the enclosed static class (note how a static
							// class is instantiated like a root class)
		b2.goB2();
		BroomHilda hilda = new BroomHilda();
		Broom.JumpTheBroom jump = new Broom().new JumpTheBroom();
	}
}