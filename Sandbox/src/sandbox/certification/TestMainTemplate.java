package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.certification.Test51.StaticClassT51;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Simulate main()", parameterDescriptions = {

		// "1", "args",
		SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "args",
		// SandboxParameters.PASSWORD_NAME, "args"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class TestMainTemplate extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] args = getStringArray("args");

		MainClass.doMain(args);
		// END TEST
	}

	static class MainClass {
		public static void doMain(String[] args) {
			SandboxParameters params = SandboxParameters.createSandboxParameters();
			params.printLine("Hello from inside " + MainClass.class.getName());
		}
	}
}
