package sandbox.certification;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Enums and Coffee.class.getName()",
 		parameterDescriptions = {
	}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test3 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");
		// new Coffee().main(null);
		Coffee drink1 = new Coffee();
		drink1.size = CoffeeSize.BIG;

		Coffee drink2 = new Coffee();
		drink2.size = CoffeeSize.OVERWHELMING;

		printLine(drink1.size.getOunces()); // prints whatever ounces is for BIG
		printLine(drink2.size.getOunces()); // prints whatever ounces is for
											// OVERWHELMING
		for (CoffeeSize cs : CoffeeSize.values())
			printLine(cs + " " + cs.getOunces() + " " + cs.getRefillable());

		// END TEST
	}

	enum CoffeeSize {
		// 8, 10 & 16 are passed to the constructor
		BIG(8, true), HUGE(10, true), OVERWHELMING(16, true), ASTRONOMICAL(30, false);
		CoffeeSize(int ounces, boolean refillable) { // constructor
			this.ounces = ounces;
			this.refillable = refillable;
		}

		private int ounces; // an instance variable
		private boolean refillable;

		public int getOunces() {
			return ounces;
		}

		public boolean getRefillable() {
			return refillable;
		}
	}

	class Coffee {
		CoffeeSize size; // each instance of Coffee has an enum
	}
}
