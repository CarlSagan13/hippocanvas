package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Demonstrate scope", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test2 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		new TestServer().logIn();
		new TestServer().count();
		// END TEST
	}

	class TestServer {
		int count = 9; // Declare an instance variable named count

		public void logIn() {
			int count = 10; // Declare a local variable named count
			printLine("local variable count is " + count);
		}

		public void count() {
			printLine("instance variable count is " + count);
		}
	}
}
