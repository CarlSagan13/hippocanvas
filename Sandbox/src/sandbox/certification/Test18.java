package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Equate two objects and observe behavior", parameterDescriptions = {

		"1", "stringOne",
		"4", "stringTwo", 
		"7", "number",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test18 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String stringOne = getString("stringOne");
		String stringTwo = getString("stringTwo");
		int number = getInteger("number");

		T18Class t18A = new T18Class();
		T18Class t18B = t18A;

		t18A.integer = number;

		printLine("A: " + t18B.integer);

		t18A.string = stringOne;

		printLine("B: " + t18B.string);

		String sOne = stringOne;
		String sTwo = sOne;

		sOne = stringTwo;
		printLine("sOne = " + sOne);
		printLine("sTwo = " + sTwo);

		printLine("C: " + t18A.integer);
		printLine("D: " + t18A.string);
		printLine("E: " + "doOne()...");
		doOne(t18A);
		printLine("F: " + t18A.integer);
		printLine("G: " + t18A.string);

		printLine("H: " + "doTwo()...");
		doTwo(t18A);
		printLine("I: " + t18A.integer);
		printLine("J: " + t18A.string);
		// END TEST
	}

	class T18Class {
		public int integer;
		public String string = "Hello from inside " + this.getClass().getName();
	}

	private void doOne(T18Class icA) {
		icA.integer = 1;
		icA.string = "Howdy from inside doOne()";
	}

	private void doTwo(T18Class icA) {
		icA = new T18Class();
		icA.integer = 2;
		icA.string = "Howdy from inside doTwo()!!!!";
	}
}
