package sandbox.certification;

import java.util.Arrays;
import java.util.Comparator;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Sorting arrays", parameterDescriptions = {

		"1", "strings", "21", "search"
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		, }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test35 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] strings = getStringArray("strings");
		String search = getString("search");

		printLine("Searching before the sort (Arrays.binarySearch([" + getString("strings") + "]," + search + "))):");
		for (String s : strings)
			printIt(s + " ");
		printLine("\n" + search + " = " + Arrays.binarySearch(strings, search)); // #2
		printLine("Searching after Arrays.sort(strings):");
		Arrays.sort(strings); // #1
		for (String s : strings)
			printIt(s + " ");
		printLine("\n" + search + " = " + Arrays.binarySearch(strings, search)); // #2

		printLine("Now reverse sort:");
		ReSortComparator reSortComparator = new ReSortComparator(); // #3
		Arrays.sort(strings, reSortComparator);
		for (String s : strings)
			printIt(s + " ");
		printLine("\n" + search + " = " + Arrays.binarySearch(strings, search)); // #4
		printLine("Using the Comparator in the search Arrays.binarySearch(strings,search,reSortComparator):");
		printLine(search + " = " + Arrays.binarySearch(strings, search, reSortComparator)); // #5

		// END TEST
	}

	class ReSortComparator implements Comparator<String> {
		public int compare(String a, String b) {
			return b.compareTo(a);
		}
	}
}
