package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Assert (use 'java -ea ...')", parameterDescriptions = {

		"7", "shouldBeNumber9",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test14 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		int shouldBeNumber9 = getInteger("shouldBeNumber9");

		int x = shouldBeNumber9;
		assert (x == 9) : "Hard to be alone";
		// END TEST
	}
}
