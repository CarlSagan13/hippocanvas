package sandbox.certification;

import java.io.Console;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Playing with Console class",
 		parameterDescriptions = {
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test21 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");

		Console c = System.console(); // #1: get a Console
		char[] pw;
		pw = c.readPassword("%s", "pw: "); // #2: return a char[]
		for (char ch : pw)
			c.format("%c ", ch); // #3: format output
		c.format("\n");

		String name = null;
		MyUtility mu = new MyUtility();
		while (true) {
			name = c.readLine("%s", "input?: "); // #4: return a String

			c.format("output: %s \n", mu.doStuff(name));
		}

		// END TEST
	}

	class MyUtility { // #5: class to test
		String doStuff(String arg1) {
			// stub code
			return "result is " + arg1;
		}
	}
}
