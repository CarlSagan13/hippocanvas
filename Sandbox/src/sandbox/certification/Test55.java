package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Argument-local playground", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test55 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		new Inner55().doit();
		new Bones().doit();
		// END TEST
	}

	class Inner55 {
		void doit() {
			new Do55().go(new IF55() {
				public void growUp() {
					printLine("yabba dabba do!");
				}
			});
		}
	}

	interface IF55 {
		void growUp();
	}

	class Do55 {
		void go(IF55 if55) {
			if55.growUp();
		}
	}

	class Bones {
		void doit() {
			Skin skin = new Skin();
			skin.peel(new Hair() {
				public void comb() {
					printLine("shave and a haircut");
				}
			});
		}
	}

	interface Hair {
		void comb();
	}

	class Skin {
		void peel(Hair hair) {
			hair.comb();
		}
	}
}
