package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "enums", parameterDescriptions = {

		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test12 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		CoffeeSize cs = CoffeeSize.OVERWHELMING;
		printLine(cs.ordinal());
		printLine(cs);

		if (cs instanceof CoffeeSize) {
			printLine("YUP");
		}

		// NOPE enum DonutTopping {PLAIN, GLAZED, SPRINKLED}; //cannot be
		// declared in a method, must be class level, 'cause it's just like a
		// class
		// END TEST
	}

	enum CoffeeSize {
		BIG, HUGE, OVERWHELMING
	};

	enum DonutTopping {
		PLAIN, GLAZED, SPRINKLED
	};
}
