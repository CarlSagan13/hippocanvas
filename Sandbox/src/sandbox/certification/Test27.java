package sandbox.certification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Serialization practice", parameterDescriptions = {

		"18", "filePath",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test27 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String filePath = getString("filePath");
		File file = new File(filePath);
		if (!file.exists()) {
			file.createNewFile();
		}
		SpecialSerial specialSerialOne = new SpecialSerial();
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filePath));
		objectOutputStream.writeObject(specialSerialOne);
		objectOutputStream.close();
		printIt(++specialSerialOne.z + " ");

		ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath));
		SpecialSerial specialSerialTwo = (SpecialSerial) objectInputStream.readObject();
		objectInputStream.close();
		printLine(specialSerialTwo.y + " " + specialSerialTwo.z + " " + specialSerialTwo.s);
		// END TEST
	}

	static class SpecialSerial implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		transient int y = 7;
		static int z = 9;
		static String s = "dmh1012";
		ICSerializable icns = new ICSerializable(); // Because it's a
													// serializable class, no
													// problem
		// NOPE RUNTIME InnerClass ic = new InnerClass(); //This causes runtime
		// Serialization Exception
	}
}

class ICSerializable implements Serializable {
}

class InnerClass {
}