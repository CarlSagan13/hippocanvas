package sandbox.certification;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with Regex", parameterDescriptions = {

		// "11", "regex",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		SandboxParameters.SCRATCHPAD_NAME, "sourceRegex",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test23 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String regex = getString("regex");
		// String string = getString("string");
		String[] sourceRegex = getString("sourceRegex").split("\n");
		String source = sourceRegex[0];
		String regex = sourceRegex[1];

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(source);

		char[] chars = new char[source.length() + 1]; // ' + 1' is for
														// zero-length matches
		Arrays.fill(chars, ' ');
		while (matcher.find()) {
			printLine(matcher.group() + "\tend='" + matcher.end() + "'");
			// printIt(m.start() + " ");
			chars[matcher.start()] = '^';
		}
		printLine(source);
		printLine(String.valueOf(chars));
		// END TEST
	}
}
