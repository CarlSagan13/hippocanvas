package sandbox.certification;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with Dates, Numbers and Currencies", parameterDescriptions = {

		"8", "languageCountry", "10", "rollAmount", "13", "doubleNumber"
		// "11", "style"
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		, }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test22 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String language = getStringArray("languageCountry")[0];
		String country = getStringArray("languageCountry")[1];
		int rollAmount = getInteger("rollAmount");
		double doubleNumber = getFloat("doubleNumber");
		String strDoubleNumber = getString("doubleNumber");
		// String style = getString("style");

		printLine("A: " + (new Date()).toString());
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		Calendar c = Calendar.getInstance();
		printLine("B: " + c.getTime().toString());
		c.add(Calendar.HOUR, -1);
		printLine("C: " + c.getTime().toString());
		c.roll(Calendar.MINUTE, rollAmount);
		printLine("D: " + c.getTime().toString());
		c.add(Calendar.MINUTE, 30);
		printLine("E: " + c.getTime().toString());
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		Locale loc = new Locale(language, country);
		Date d = c.getTime();
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, loc);
		printLine("F: " + df.format(d));
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		// Locale germany = Locale.GERMANY;
		// printLine("Using " + Locale.GERMANY.getDisplayCountry());
		NumberFormat nf = NumberFormat.getInstance(loc);
		printLine("nf.parse(" + strDoubleNumber + ") = " + nf.parse(strDoubleNumber)); // parse()
																						// has
																						// to
																						// be
																						// in
																						// a
																						// try/catch
		printLine("nf.format(" + doubleNumber + ") = " + nf.format(doubleNumber));

		// END TEST
	}
}
