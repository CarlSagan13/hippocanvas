package sandbox.certification;

import java.util.HashMap;
import java.util.Map;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Play with Maps",
		parameterDescriptions = {
				"19", "englishNumbers",
				"20", "portugeseNumbers",
				"21", "key"
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		, }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test44 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] englishNumbers = getStringArray("englishNumbers");
		String[] portugeseNumbers = getStringArray("portugeseNumbers");
		String key = getString("key");

		Map<NumberKey, String> mapEngPortNums = new HashMap<NumberKey, String>();

		for (int i = 0; i < englishNumbers.length; i++) {
			mapEngPortNums.put(new NumberKey(englishNumbers[i]), portugeseNumbers[i]);
		}

		printLine("Size = " + mapEngPortNums.size());
		printLine(mapEngPortNums);
		printLine(mapEngPortNums.get(new NumberKey(key)));
		// END TEST
	}

	class NumberKey {
		String string;

		public NumberKey(String string) {
			this.string = string;
		}

		public boolean equals(Object obj) {
			return ((NumberKey) obj).string.equals(this.string);
		}

		public int hashCode() {
			return this.string.hashCode();
		}

		public String toString() {
			return this.string;
		}
	}
}
