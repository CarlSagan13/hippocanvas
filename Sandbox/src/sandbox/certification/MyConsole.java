package sandbox.certification;

import java.io.Console;

public class MyConsole {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Console c = System.console(); // #1: get a Console
		char[] pw;
		pw = c.readPassword("%s", "pw: "); // #2: return a char[]
		for (char ch : pw)
			c.format("%c ", ch); // #3: format output
		c.format("\n");

		String name = null;
		MyUtility mu = new MyUtility();
		while (true) {
			name = c.readLine("%s", "input?: "); // #4: return a String

			c.format("output: %s \n", mu.doStuff(name));
		}
	}

}

class MyUtility {
	String doStuff(String name) {
		return "Howdy " + name;
	}
}
