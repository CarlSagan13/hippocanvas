package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Using more than one generic type", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test40 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		UseTwo<String, Integer> twos = new UseTwo<String, Integer>("foo", 42);

		printLine("The T is " + twos.getT() + " :: " + twos.getT().getClass()); // returns a String
		printLine("The X is " + twos.getX() + " :: " + twos.getX().getClass()); // returns Integer, unboxes int

		UseThree<String, Integer, SandboxTestBase> threes = new UseThree<String, Integer, SandboxTestBase>("eatatjoes",	69, this);

		printLine("The T is " + threes.getT() + " :: " + threes.getT().getClass()); // returns a String
		printLine("The X is " + threes.getX() + " :: " + threes.getX().getClass()); // returns Integer, unboxes to int
		printLine("The D is '" + threes.getTEST().getDescription() + "' :: " + threes.getTEST().getClass()); // returns Integer, unboxes to int

		UseThree<Boolean, String, Float> threesToo = new UseThree<Boolean, String, Float>(true, "cara",
				Float.valueOf("1.23"));
		printLine("The T is " + threesToo.getT() + " :: " + threesToo.getT().getClass()); // returns a String
		printLine("The X is " + threesToo.getX() + " :: " + threesToo.getX().getClass()); // returns Integer, unboxes to int
		printLine("The D is " + threesToo.getTEST() + " :: " + threesToo.getTEST().getClass()); // returns Integer, unboxes to int

		UseThree<Boolean, String, Integer> threesAlso = new UseThree<Boolean, String, Integer>(true, "verga", 57);

		printLine("The T is " + threesAlso.getT() + " :: " + threesAlso.getT().getClass()); // returns a String
		printLine("The X is " + threesAlso.getX() + " :: " + threesAlso.getX().getClass()); // returns Integer, unboxes to int
		printLine("The D is " + threesAlso.getTEST() + " :: " + threesAlso.getTEST().getClass()); // returns Integer, unboxes to int
		// END TEST
	}

	class UseTwo<T, X> {
		T one;
		X two;

		UseTwo(T one, X two) {
			this.one = one;
			this.two = two;
		}

		T getT() {
			return one;
		}

		X getX() {
			return two;
		}
	}

	class UseThree<WEM, X, TEST> {
		WEM one;
		X two;
		TEST three;

		UseThree(WEM one, X two, TEST three) {
			this.one = one;
			this.two = two;
			this.three = three;
		}

		WEM getT() {
			return one;
		}

		X getX() {
			return two;
		}

		TEST getTEST() {
			return three;
		}
	}
}
