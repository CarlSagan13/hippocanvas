package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Playing with T", 
		parameterDescriptions = {
			"1", "string", 
			"7", "number",
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test43 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");
		int number = getInteger("number");

		Foo<Integer> fi = new Foo<Integer>(number);
		printLine(fi.whatAmI());
		fi.myValue(fi.getValue());

		Foo<? extends Number> fn = new Foo<Number>(number);
		printLine(fn.whatAmI());
		// NOPE fn.myValue(fn.getValue());

		Foo<String> fs = new Foo<String>(string);
		printLine(fs.whatAmI());
		fs.myValue(fs.getValue());
		// END TEST
	}

	class Foo<T> {
		T anInstance;

		Foo(T t) {
			this.anInstance = t;
		}

		String whatAmI() {
			return anInstance.getClass().getName();
		}

		void myValue(T t) {
			printLine(t);
		}

		T getValue() {
			return anInstance;
		}
	}

}
