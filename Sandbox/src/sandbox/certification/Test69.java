package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Static initialization blocks",
 		parameterDescriptions = {

			// "1", "string",
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test69 extends SandboxTestBase {	 
	private static int staticTest;

	static {
		staticTest = 69;
	}
	 @Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		printLine(staticTest);
		printLine(T69.t69Test);
		printLine(new T69().getTest2());
		// END TEST
	}
}

class T69 {
	public static int t69Test;
	private static String test2;
	static {
		t69Test = 42;
	}
	{
		test2 = "Hair design";
	}

	public static String getTest2() {
		return test2;
	}
}
