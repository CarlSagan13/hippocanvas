package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Variable scope", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test7 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		// printLine("Hello from " + this.getClass().getSimpleName());

		String string = "not changed";
		doSomething(string);
		printLine(string);

		InnerClass innerClass = new InnerClass();
		innerClass.setMyString("not changed");
		doSomething(innerClass);
		printLine(innerClass.getMyString());

		innerClass.setMyString("not changed");
		doSomethingElse(innerClass);
		printLine(innerClass.getMyString());

		innerClass.doSomething();
		innerClass.doSomethingElse();
		// END TEST
	}

	private void doSomething(String s) {
		s = "is changed";
	}

	private void doSomething(InnerClass ic) {
		ic.setMyString("is changed");
	}

	private void doSomethingElse(InnerClass ic) {
		ic = new InnerClass();
		ic.setMyString("hoop hoop");
	}

	private class InnerClass {
		private String myString = "initial value";
		private int count;

		public String getMyString() {
			return myString;
		}

		public void setMyString(String myString) {
			this.myString = myString;
		}

		public void doSomething() {
			printLine(count);
		}

		public void doSomethingElse() {
			int count = 1;

			printLine(count);
		}
	}
}
