package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Show need to synchronize: Unsynchronized", parameterDescriptions = {

		"7", "balance",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test62 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		int balance = getInteger("balance");
		// String[] args = getStringArray("args");
		// AccountDanger.main(args);

		AccountDanger r = new AccountDanger(balance);

		Thread one = new Thread(r);
		Thread two = new Thread(r);
		one.setName("Fred");
		two.setName("Lucy");
		one.start();
		two.start();

		// END TEST
	}

	class Account {
		private int balance = 50;

		public int getBalance() {
			return balance;
		}

		public void withdraw(int amount) {
			balance = balance - amount;
		}

		public void setBalance(int bal) {
			balance = bal;
		}
	}

	class AccountDanger implements Runnable {
		private Account acct = new Account();

		public AccountDanger(int balance) {
			acct.setBalance(balance);
		}

		public void run() {
			for (int x = 0; x < 5; x++) {
				makeWithdrawal(10);
				if (acct.getBalance() < 0) {
					printLine("account is overdrawn!");
				}
			}
		}

		private void makeWithdrawal(int amt) {
			if (acct.getBalance() >= amt) {
				printLine(Thread.currentThread().getName() + " is going to withdraw. Sees " + acct.getBalance());
				try {
					Thread.sleep(500);
				} catch (InterruptedException ex) {
				}
				acct.withdraw(amt);
				printLine(Thread.currentThread().getName() + " completes the withdrawal. " + acct.getBalance()
						+ " is left");
			} else {
				printLine("Not enough in account for " + Thread.currentThread().getName() + " to withdraw "
						+ acct.getBalance());
			}
		}
	}
}
