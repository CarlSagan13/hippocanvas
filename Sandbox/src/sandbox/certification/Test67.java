package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Thread IDs", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test67 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		new Starter().main(null);
		printLine();
		// END TEST
	}

	public class Starter implements Runnable {
		void go(long id) {
			printLine(id);
		}

		public void main(String[] args) {
			printIt(Thread.currentThread().getId() + " ");
			// new Starter().run();
			// NOPE new Starter().start();
			// new Thread(new Starter());
			// new Thread(new Starter()).run();
			// new Thread(new Starter()).start();
		}

		@Override
		public void run() {
			go(Thread.currentThread().getId());
		}

	}
}
