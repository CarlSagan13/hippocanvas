package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Operator and Machine threading example",
 		parameterDescriptions = {

			// "1", "string",
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test66 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		Operator operator = new Operator();
		Machine machine = new Machine(operator);
		machine.start();
		synchronized (machine) {
			machine.wait();
			printLine(this.getClass().getName() + " is done");
		}
		// END TEST
	}

	class Operator extends Thread {
		public void run() {
			while (isRunning) {
				// Get shape from user
				synchronized (this) {
					// Calculate new machine steps from shape
					try {
						Thread.sleep(1000);
						notify();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						printLine("Interrupted");
					}
					notify();
				}
			}
		}
	}

	class Machine extends Thread {
		Operator operator; // assume this gets initialized

		public Machine(Operator operator) {
			this.operator = operator;
		}

		public void run() {
			operator.start();
			synchronized (operator) {
				try {
					operator.wait();
				} catch (InterruptedException ie) {
					printLine("All done");
				}
				// Send machine steps to hardware
				printLine("Send machine steps to hardware");
			}
			notify();
		}
	}
}
