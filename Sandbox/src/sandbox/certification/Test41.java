package sandbox.certification;

import java.util.ArrayList;
import java.util.List;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Generic methods", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test41 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		List<Integer> integerList = makeNumberList(new Integer(69));
		printLine("1: " + integerList);
		List<Float> floatList = makeNumberList(new Float(19.56));
		printLine("2: " + floatList);
		// NOPE List<String> stringList = makeNumberList(new String("howdy"));

		List<Number> numberList = new ArrayList<Number>();
		addNumber(numberList);
		printLine("3: " + numberList);

		List<String> stringList = new ArrayList<String>();
		// NOPE addNumber(stringList);

		printLine("4: " + addNumber(integerList, new Integer(2013)));
		printLine("5: " + addNumber(floatList, new Float(20.13)));
		// NOPE printLine(addNumber(floatList, new Integer(2013)));
		// END TEST
	}

	<T extends Number> List<T> makeNumberList(T t) {
		List<T> list = new ArrayList<T>();
		list.add(t);
		return list;
	}

	void addNumber(List<? super Number> numbers) {
		numbers.add(new Integer(1999));
	}

	<T extends Number> List<T> addNumber(List<T> numbers, T t) {
		numbers.add(t);
		return numbers;
	}

}
