package sandbox.certification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with global List.class.getName() objects", parameterDescriptions = {

		"21", "key",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test33 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String key = getString("key");

		List<T30> arrayList = (ArrayList<T30>) this.getGlobalMapObject(ArrayList.class.getName());
		printLine("From " + arrayList.getClass().getName() + ": " + arrayList.contains(new T30(key)));
		for (T30 t30 : arrayList) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();

		List<T30> vector = (Vector<T30>) this.getGlobalMapObject(Vector.class.getName());
		printLine("From " + vector.getClass().getName() + ": " + vector.contains(new T30(key)));
		for (T30 t30 : vector) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();

		List<T30> linkedList = (LinkedList<T30>) this.getGlobalMapObject(LinkedList.class.getName());
		printLine("From " + linkedList.getClass().getName() + ": " + linkedList.contains(new T30(key)));
		for (T30 t30 : linkedList) {
			printIt(t30.value + " ");
		}
		printLine();
		printLine();

		// END TEST
	}
}
