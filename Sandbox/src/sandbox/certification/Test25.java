package sandbox.certification;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Tokenizing with scanner",
 		parameterDescriptions = {
 				SandboxParameters.SCRATCHPAD_NAME, "source",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test25 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String source = getString("source");

		boolean b2, b;
		int i;
		String s, hits = " ";
		Scanner s1 = new Scanner(source);
		Scanner s2 = new Scanner(source);
		while (b = s1.hasNext()) {
			s = s1.next();
			hits += "s";
		}
		while (b = s2.hasNext()) {
			if (s2.hasNextInt()) {
				i = s2.nextInt();
				hits += "i";
			} else if (s2.hasNextBoolean()) {
				b2 = s2.nextBoolean();
				hits += "b";
			} else {
				s2.next();
				hits += "s2";
			}
		}
		printLine("hits " + hits);

		// END TEST
	}
}
