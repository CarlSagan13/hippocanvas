package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Threads: wait() and notify()", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test65 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		ThreadA a = new ThreadA();
		a.start();
		synchronized (a) {
			a.wait();
			printLine(this.getClass().getSimpleName() + " is done");
		}
		// END TEST
	}

	class ThreadA extends Thread {
		public void run() {
			ThreadB b = new ThreadB();
			b.start();
			synchronized (b) {
				try {
					printLine("A: Waiting for b to complete...");
					b.wait();
					printLine("A: ... done waiting");
				} catch (InterruptedException e) {
					printLine(e.getMessage());
				}
				printLine("A: Total is: " + b.total);
				notify();
			}
		}
	}

	class ThreadB extends Thread {
		int total;

		public void run() {
			synchronized (this) {
				printLine("B: Working ...");
				for (int i = 0; i < 100; i++) {
					total += i;
					printIt(total + " ");
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						printLine("B: I was interruped");
					}
				}
				printLine();
				printLine("B: OK, I'm done. Notifying ...");
				notify();
				printLine("B: ... listener (probably A) was notified.");
			}
		}
	}
}
