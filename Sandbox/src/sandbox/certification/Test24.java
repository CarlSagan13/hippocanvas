package sandbox.certification;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Searching with Scanner",
 		parameterDescriptions = {
 				SandboxParameters.SCRATCHPAD_NAME, "sourceRegex",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test24 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] sourceRegex = getStringArray("sourceRegex");
		String source = sourceRegex[0];
		String regex = sourceRegex[1];

		// Scanner scanner = new Scanner(source).useDelimiter(regex);
		// printLine(scanner.nextInt());
		// printLine(scanner.nextInt());
		// printLine(scanner.next());
		// printLine(scanner.next());
		// scanner.close();
		//
		// printLine(SandboxParameters.VISUAL_SEPARATOR);

		Scanner s1 = new Scanner(source);
		String token;
		do {
			token = s1.findInLine(regex);
			printLine("found " + token);

		} while (token != null);
		// END TEST
	}
}
