package sandbox.certification;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Play with global Set.class.getName() objects", parameterDescriptions = {

		"21", "key",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test32 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String key = getString("key");

		printLine("W/O generics");
		Set untypedTreeSet = new TreeSet();

		untypedTreeSet.add(69); // autoboxing
		untypedTreeSet.add(new Integer(1956));
		printLine("From " + untypedTreeSet.getClass() + ": " + untypedTreeSet.contains(69));
		try {
			String string = "Howdy doodie!";
			untypedTreeSet.add(string);
			printLine("From " + untypedTreeSet.getClass() + ": " + untypedTreeSet.contains(string));
		} catch (ClassCastException cce) {
			printLine("NOPE RUNTTIME: Trying to add string after adding integer to set", cce);
		}

		untypedTreeSet = new TreeSet();
		String string = "Howdy doodie!";
		untypedTreeSet.add(string);
		printLine("From " + untypedTreeSet.getClass() + ": " + untypedTreeSet.contains(string));
		try {
			untypedTreeSet.add(69); // autoboxing
			untypedTreeSet.add(new Integer(1956));
			printLine("From " + untypedTreeSet.getClass() + ": " + untypedTreeSet.contains(69));
		} catch (ClassCastException cce) {
			printLine("NOPE RUNTTIME: Trying to add integer after adding string to set", cce);
		}
		printLine();

		printLine("With generics");
		Set<T30> hashSet = (HashSet<T30>) this.getGlobalMapObject(HashSet.class.getName());
		printLine("From " + hashSet.getClass().getName() + ": " + hashSet.contains(new T30(key)));
		Iterator<T30> iterHashSet = hashSet.iterator();
		while (iterHashSet.hasNext()) {
			printIt(iterHashSet.next().value + " ");
		}
		printLine();

		Set<T30> treeSet = (TreeSet<T30>) this.getGlobalMapObject(TreeSet.class.getName());
		printLine("From " + treeSet.getClass().getName() + ": " + treeSet.contains(new T30(key)));
		Iterator<T30> iterTreeSet = treeSet.iterator();
		while (iterTreeSet.hasNext()) {
			printIt(iterTreeSet.next().value + " ");
		}
		printLine();

		Set<T30> linkedHashSet = (LinkedHashSet<T30>) this.getGlobalMapObject(LinkedHashSet.class.getName());
		printLine("From " + linkedHashSet.getClass().getName() + ": " + linkedHashSet.contains(new T30(key)));
		Iterator<T30> iterLinkedHashSet = linkedHashSet.iterator();
		while (iterLinkedHashSet.hasNext()) {
			printIt(iterLinkedHashSet.next().value + " ");
		}
		printLine();

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		Set<T32WithHash> withhash = new LinkedHashSet<T32WithHash>();
		withhash.add(new T32WithHash(1));
		withhash.add(new T32WithHash(2));
		withhash.add(new T32WithHash(1));
		printLine("T32WithHash size is " + withhash.size());
		Set<T32NoHash> nohash = new LinkedHashSet<T32NoHash>();
		nohash.add(new T32NoHash(1));
		nohash.add(new T32NoHash(2));
		nohash.add(new T32NoHash(1));
		printLine("T32NoHash size is " + nohash.size());
		nohash = new LinkedHashSet<T32NoHash>();
		for (int i = 0; i < 10; i++) {
			nohash.add(new T32NoHash(56));
		}
		printLine("T32NoHash size is " + nohash.size());
		// END TEST
	}

	class T32WithHash {
		int value;

		public T32WithHash(int value) {
			this.value = value;
		}

		public int hashCode() {
			return value / 5;
		}

		public boolean equals(Object o) {
			return value == ((T32WithHash) o).value;
		}
	}

	class T32NoHash {
		int value;

		public T32NoHash(int value) {
			this.value = value;
		}

		public boolean equals(Object o) {
			return value == ((T32NoHash) o).value;
		}
	}
}
