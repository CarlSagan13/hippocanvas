package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Static access", parameterDescriptions = {

		// "1", "args",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test51 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		// String[] args = getStringArray("args");

		StaticClassT51.main(null);
		new StaticClassT51().main(null);
		new InnerClassT51().notMain(null);
		// END TEST
	}

	static class StaticClassT51 {
		String strSCT51 = "A member inside " + StaticClassT51.class.getName();
		static String strSCT51_static = "A static member inside " + StaticClassT51.class.getName();
		static SandboxTestBase test_static = SandboxParameters.createSandboxParameters()
				.getTest(Test51.class.getSimpleName());

		public static void main(String[] args) {
			// NOPE printLine(); //not a static method
			// NOPE this.anything; //No 'this' in static context
			// NOPE printLine(strIT51); //not a static member
			test_static.printLine(strSCT51_static);

			test_static.printLine("Hello from inside " + StaticClassT51.class.getName());
		}

		public void doSomething() {
			this.strSCT51 = "dobedo";
			StaticClassT51.strSCT51_static = "torpedo";
			// NOPE printLine();
		}
	}

	class InnerClassT51 {
		String strICT51 = "A member inside " + StaticClassT51.class.getName();

		// NOPE static String strICT51 = "A static member inside " +
		// StaticClassT51.class.getName(); //Cannot declare static anything in
		// an inner class or top-level class
		// NOPE static ITest test =
		// SandboxParameters.createSandboxParameters().getTest(Test51.class.getSimpleName());
		// //same as above
		public void notMain(String[] args) {
			printLine();
			this.strICT51 = "whatever";
			printLine(strICT51);

			printLine("Hello from inside " + InnerClassT51.class.getName());
		}

		public void doSomething() {
			strICT51 = "dobedo";
			StaticClassT51.strSCT51_static = "torpedo";
			printLine();
		}

	}
}
