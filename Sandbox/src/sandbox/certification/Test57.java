package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Instantiating an inner class", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test57 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");
		Class57.IC57 ic57 = new Class57().new IC57();
		ic57.doB();

		new Class57().main(null);
		// END TEST
	}
}

class Class57 {
	public static void main(String[] args) {
		Class57 c57 = new Class57();
		c57.doA();
	}

	void doA() {
		SandboxTestBase test = SandboxParameters.createSandboxParameters().getTest(Test57.class.getSimpleName());
		test.printLine("a");
	}

	class IC57 {
		{
			Class57.this.doA();
		}

		void doB() {
			SandboxTestBase test = SandboxParameters.createSandboxParameters().getTest(Test57.class.getSimpleName());
			test.printLine("b");
		}
	}

}
