package sandbox.certification;

import java.util.PriorityQueue;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = "Play with a PriorityQueue",
		parameterDescriptions = {
				"28", "strings"
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test45 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] strings = getStringArray("strings");

		for (String string1 : strings) {
			for (String string2 : strings) {
				printLine(string1 + " compares to " + string2 + ": " + string1.compareTo(string2));
			}
		}
		printLine();
		printLine();

		PriorityQueue<String> pq = new PriorityQueue<String>();
		pq.add(strings[0]);		 	//One
		pq.add(strings[1]); 		//One-Two
		printIt(pq.peek() + " "); 	//"One"
		pq.offer(strings[2]); 		//One-Three-Two (natural ordering)
		pq.add(strings[3]);			//Four-One-Three-Two
		pq.remove(strings[2]);		//Four-One-Two
		printIt(pq.poll() + " ");	//"Four" One-Two
		if (pq.remove(strings[0])) {	//Two
			printIt(pq.poll() + " ");	//"Two" []
		}
		printLine(pq.poll() + " " + pq.peek());	//"null" "null"
		printLine();
		printLine();

		PriorityQueue<String> pq1 = new PriorityQueue<String>();
		for (String string : strings) {
			pq1.add(string);
		}
		while (pq1.size() > 0) {
			printIt(pq1.poll() + " ");
		}
		printLine();
		printLine();
		// END TEST
	}
}
