package sandbox.certification;

//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Instantiating an inner class", parameterDescriptions = {

		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "args",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password"
		,}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test59 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String[] args = getStringArray("args");

		new Navel().main(null);
		// END TEST
	}
}

class Navel {
	private int size = 7;
	private static int length = 3;

	public void main(String[] args) {
		new Navel().go();
		// NOPE new Gazer(); //Class Gazer would have to be static for access,
		// but then Navel.size would also have to be static because Gazer is
		// static
		Navel.Gazer navelGazer = new Navel().new Gazer();
	}

	void go() {
		int size = 5;
		SandboxTestBase test = SandboxParameters.createSandboxParameters().getTest(Test59.class.getSimpleName());
		test.printLine(new Gazer().adder());
	}

	class Gazer {
		int adder() {
			return size * length;
		} // Note that size and length are both visible
	}
}
