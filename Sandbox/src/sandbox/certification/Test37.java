package sandbox.certification;

import java.util.TreeSet;

import sandbox.SandboxAnnotation;
//replaced
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with TreeSet",
 		parameterDescriptions = {
 				"22", "times", 
 				"23", "lastFerryLeavesBefore", 
 				"24", "firstFerryLeavesAfter"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test37 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = "Play with " + TreeSet.class.getName();
	private String[] parameterDescriptions = { "22", "times", "23", "lastFerryLeavesBefore", "24",
			"firstFerryLeavesAfter"
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password"
	};

	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		// String string = getString("string");
		String[] saTimes = getStringArray("times");
		int lastFerryLeavesBefore = getInteger("lastFerryLeavesBefore");
		int firstFerryLeavesAfter = getInteger("firstFerryLeavesAfter");

		Integer[] iTimes = new Integer[saTimes.length];
		int index = 0;
		for (String time : saTimes) {
			iTimes[index++] = Integer.parseInt(time);
		}

		TreeSet<Integer> times = new TreeSet<Integer>();
		for (Integer integer : iTimes) {
			times.add(integer);
		}

		printLine("Last Ferry Leaves Before " + lastFerryLeavesBefore + " is " + times.lower(lastFerryLeavesBefore));
		printLine("First Ferry Leaves After " + firstFerryLeavesAfter + " is " + times.higher(firstFerryLeavesAfter));
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("ceiling " + lastFerryLeavesBefore + ": " + times.ceiling(lastFerryLeavesBefore));
		printLine("higher " + lastFerryLeavesBefore + ": " + times.higher(lastFerryLeavesBefore));
		printLine("floor " + lastFerryLeavesBefore + ": " + times.floor(lastFerryLeavesBefore));
		printLine("lower " + lastFerryLeavesBefore + ": " + times.lower(lastFerryLeavesBefore));

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		for (Integer time : times) {
			printIt(time + " ");
		}
		printLine();
		printLine("pollFirst: " + times.pollFirst());
		for (Integer time : times) {
			printIt(time + " ");
		}
		printLine();
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("pollLast: " + times.pollLast());
		for (Integer time : times) {
			printIt(time + " ");
		}
		printLine();
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("descendingSet: ");
		for (Integer time : times.descendingSet()) {
			printIt(time + " ");
		}
		printLine();
		printLine(SandboxParameters.VISUAL_SEPARATOR);

		// END TEST
	}
}
