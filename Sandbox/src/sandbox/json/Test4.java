package sandbox.json;

import com.google.gson.Gson;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "2.1. Member JSON Array to Java Array",
 		parameterDescriptions = {
 				SandboxParameters.POPPAD_NAME, "departmentJson",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test4 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		String departmentJson = getString("departmentJson");
		
//		String departmentJson = "{'id' : 1, "
//				+ "'name': 'HR',"
//				+ "'users' : ["
//					+ "{'name': 'Alex','id': 1}, "
//					+ "{'name': 'Brian','id':2}, "
//					+ "{'name': 'Charles','id': 3}]}";

		Gson gson = new Gson(); 

		Department department = gson.fromJson(departmentJson, Department.class);  

		printLine(department);
		// END TEST
	 }
	 
	class User {
		private long id;
		private String name;

		protected long getId() {
			return id;
		}

		protected void setId(long id) {
			this.id = id;
		}

		protected String getName() {
			return name;
		}

		protected void setName(String name) {
			this.name = name;
		}
		
		public String toString() {
			return "id=" + String.valueOf(id) + " : name=" + name;
		}
	}
	
	class Department
	{
		private long id;
		private String name;
		private User[] users;

		protected long getId() {
			return id;
		}
		protected void setId(long id) {
			this.id = id;
		}
		protected String getName() {
			return name;
		}
		protected void setName(String name) {
			this.name = name;
		}
		protected User[] getUsers() {
			return users;
		}
		protected void setUsers(User[] users) {
			this.users = users;
		}
		public String toString() {
			StringBuffer sbReturn = new StringBuffer();
			sbReturn.append("id=").append(id).append(" : name=").append(name);
			for (User user : getUsers()) {
				sbReturn.append('\n').append('\t').append(user.toString());
			}
			return sbReturn.toString();
		}
	}

}
