package sandbox.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Recurse into json element/array values ('.' delimiter)",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "path,jsonFilePath",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test15 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String jsonFilePath = getString("jsonFilePath");
		String json = Utilities.getFileString(jsonFilePath);
		String[] path = getStringArray("path", "\\.");

		JsonParser parser = new JsonParser();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);

		if (je.isJsonArray()) {
			json = "{\"array\": " + json + "}";
		}
		JsonElement jsonTree = parser.parse(json);

		if (jsonTree.isJsonObject()) {
			JsonObject jsonObject = jsonTree.getAsJsonObject();
			printLine(stepIn(jsonObject, path, 0));
			
			/////////////////DMH
			jsonObject.addProperty("poonPerty", "vacalue");
			printLine(jsonObject);
			printLine(jsonObject.get("actors"));
			////////////////////
			
		} else {
			printLine("NOT JSON!");
		}
		 //END TEST
	 }
	 
	 private JsonElement stepIn(JsonObject job, String[] path, int pathIndex) {
		if (pathIndex >= path.length) {
			return job;
		}

		JsonElement je = null;
		if (path[pathIndex].matches(".*\\[\\d+?\\].*")) {
			int arrayIndex = Integer.parseInt(
					path[pathIndex].substring(path[pathIndex].indexOf('[') + 1, path[pathIndex].length() - 1));
			String arrayName = path[pathIndex].substring(0, path[pathIndex].indexOf('['));
			je = job.get(arrayName);
			JsonArray ja = je.getAsJsonArray();
			
			try {
				je = ja.get(arrayIndex);
			} catch(IndexOutOfBoundsException ioobe) {
				printLine("Array size is " + ja.size());
				je = ja.get(ja.size() - 1);
			}
			
			if (je != null && !je.isJsonPrimitive()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		} else {
			je = job.get(path[pathIndex]);
			
			if (je != null && !je.isJsonPrimitive() && je.isJsonArray()) {
				JsonArray ja = je.getAsJsonArray();
				printLine(path[pathIndex] + ": " + ja.size());
			}
			
			if (je != null && !je.isJsonPrimitive() && !je.isJsonArray()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		}
		return je;
	 }

}
