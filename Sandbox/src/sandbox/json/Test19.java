package sandbox.json;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Find json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "fileRegex,valueRegex,directory",
 				SandboxParameters.TRUEFALSE_NAME, "recurse"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test19 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String fileRegex = getString("fileRegex");
		 String valueRegex = getString("valueRegex");
		File directory = getFileDir("directory");
		boolean recurse = getBoolean("recurse");

		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
		Pattern valuePattern = Pattern.compile(valueRegex, Pattern.MULTILINE);

//		List<List<String>> returnPaths = new ArrayList<List<String>>();
		for (File file : Utilities.listFilesRegex(directory, recurse, false, pattern, this)){
			if (file.isFile()) {
				 JsonElement jsonTree = this.getGlobalMapThing(file.getPath(), JsonElement.class);
				 if (jsonTree == null) {
					 printLine("Getting json from file:");
					 printLine("\t" + file.getPath());
					 StringBuffer json = new StringBuffer(new String(Utilities.getFileBytes(file)));

						JsonParser parser = new JsonParser();

						JsonParser jp = new JsonParser();
						JsonElement je = jp.parse(json.toString());

						if (je.isJsonArray()) {
							json = new StringBuffer("{\"array\": " + json + "}");
						}
						jsonTree = parser.parse(json.toString());
//remove for final version						this.addToGlobalMap(file.getPath(), jsonTree);
				 } else {
					 printLine("Getting json from global map:");
					 printLine("\t" + file.getPath());
				 }
				 

				if (jsonTree.isJsonObject()) {
					JsonObject jsonObject = jsonTree.getAsJsonObject();
					try {
						printLine(stepIn(jsonObject, valuePattern, ""));
					} catch(NullPointerException npe) {
						printLine("Invalid path:");
						return;
					}
				} else {
					printLine("NOT JSON!");
				}

				
			}
			checkRunning();
		}

		 //END TEST
	 }
	
//*/////////////////////////////////////DMH	 
	private boolean stepIn(JsonElement jel, Pattern valuePattern, String path) throws NullPointerException {
//		printALine();
//		printLine("jel.isJsonArray() = " + jel.isJsonArray());
//		printLine("jel.isJsonObject() = " + jel.isJsonObject());
//		printLine("jel.isJsonPrimitive() = " + jel.isJsonPrimitive());
		
		if(jel.isJsonPrimitive()) {
			printLine(jel.getAsString());
		} else if(jel.isJsonObject()) {
//			printLine(jel.getAsJsonObject().entrySet().size());
			for (Entry<String, JsonElement> entry : jel.getAsJsonObject().entrySet()) {
				printLine(entry.getKey());
//				printLine("entry.getValue().isJsonArray() = " + entry.getValue().isJsonArray());
//				printLine("entry.getValue().isJsonObject() = " + entry.getValue().isJsonObject());
//				printLine("entry.getValue().isJsonPrimitive() = " + entry.getValue().isJsonPrimitive());
				stepIn(entry.getValue(), valuePattern, path);
			}
		} else if(jel.isJsonArray()) {
//			printLine(jel.getAsJsonArray().size());
			for (JsonElement element : jel.getAsJsonArray()) {
				stepIn(element, valuePattern, path);
			}
		}
		
//		for (Entry<String, JsonElement> entry : jel.entrySet()) {
//		printLine(entry.getKey());
//		printLine("entry.getValue().isJsonArray() = " + entry.getValue().isJsonArray());
//		printLine("entry.getValue().isJsonObject() = " + entry.getValue().isJsonObject());
//		printLine("entry.getValue().isJsonPrimitive() = " + entry.getValue().isJsonPrimitive());
		
		
		
//		if (entry.getValue().isJsonPrimitive()) {
//			printLine(entry.getValue().getAsString());
//		} else if (entry.getValue().isJsonObject()){
//			stepIn(entry.getValue().getAsJsonObject(), valuePattern, path);
//		}
//	}			

		
//		for (Entry<String, JsonElement> entry : job.entrySet()) {
//			
//			printALine();
//			printLine(entry.getKey());
//			printLine("job.isJsonArray() = " + entry.getValue().isJsonArray());
//			printLine("job.isJsonObject() = " + entry.getValue().isJsonObject());
//			printLine("job.isJsonPrimitive() = " + entry.getValue().isJsonPrimitive());
			
			
//			if (entry.getValue().isJsonPrimitive()) {
//				printLine(path + "." + entry.getKey() + " :: " + entry.getValue());
//			} else if (entry.getValue().isJsonObject()) {
//				stepIn(entry.getValue().getAsJsonObject(), valuePattern, path + "." + entry.getKey() );
//			} else if (entry.getValue().isJsonArray()) {
//				for (JsonElement element : entry.getValue().getAsJsonArray()) {
//					if(element.isJsonPrimitive()) {
//						printLine(path + "." + entry.getKey() + " :: " + entry.getValue());
//					} else {
//						JsonObject jo = element.getAsJsonObject();
//						for(Entry<String,JsonElement> subentry : jo.entrySet()) {
//							printLine(subentry.getKey() + " :1: " + subentry.getValue());
//						}					
//					}
//				}
//			}
//		}
		return true;
	}
/*////////////////////////////////////////	 
	private boolean stepIn(JsonObject job, Pattern valuePattern, String path) throws NullPointerException {
		for (Entry<String, JsonElement> entry : job.entrySet()) {
			if (entry.getValue().isJsonPrimitive()) {
				printLine(path + "." + entry.getKey() + " :: " + entry.getValue());
			} else if (entry.getValue().isJsonObject()) {
				stepIn(entry.getValue().getAsJsonObject(), valuePattern, path + "." + entry.getKey() );
			} else if (entry.getValue().isJsonArray()) {
				for (JsonElement element : entry.getValue().getAsJsonArray()) {
					if(element.isJsonPrimitive()) {
						printLine(path + "." + entry.getKey() + " :: " + entry.getValue());
					} else {
						JsonObject jo = element.getAsJsonObject();
						for(Entry<String,JsonElement> subentry : jo.entrySet()) {
							printLine(subentry.getKey() + " :1: " + subentry.getValue());
						}					
					}
				}
			}
		}
		return true;
	}
//*////////////////////////////////////////	 
	
	
}
