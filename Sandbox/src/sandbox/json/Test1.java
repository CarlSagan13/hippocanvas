package sandbox.json;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Convert java class to/from json",
 		parameterDescriptions = {
 				SandboxParameters.POPPAD_NAME, "json",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test1 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String json = getString("json");
		 
		 Employee emp = Utilities.jsonToJava(json, Employee.class);
		 
		 printALine();
		 printLine(emp.toString());

		 printALine();
		 printLine(Utilities.javaToJson(emp));
		 printALine();
		 
		 //END TEST
	 }

}
