package sandbox.weddingphotos;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class Test6 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = "Resize photo files";
	private String[] parameterDescriptions = 
		{
			"9", "recurse",
			"10", "filenameFilter",
			"11", "destinationPath",
			"12", "sourcePath",
			"15", "targetFileSize",

//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "poppad"
		};
	
	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String filenameFilter = getString("filenameFilter");
		String sourcePath = getString("sourcePath");
		String destinationPath = getString("destinationPath");
		float targetFileSize = getFloat("targetFileSize");
		boolean recurse = getBoolean("recurse");

		FilenameFilter filter = new sandbox.SandboxFilenameFilter(filenameFilter); 
		File directory = new File(sourcePath);
		File[] files = sandbox.Utilities.listFilesDirsAsArray(directory, filter, recurse);
		
		for (File file : files) {
			float scale = (float)(Math.sqrt(targetFileSize) / Math.sqrt(file.length()));

			String newPhotoFilePath = destinationPath
				+ File.separator + file.getName().split("\\.")[0]
				+ "." + file.getName().split("\\.")[1];
			
			BufferedImage image = ImageIO.read(file);
			int w = image.getWidth(), h = image.getHeight();
			int newWidth = (int)(w * scale);
			int newHeight = (int)(h * scale);
			Image rescaled = image.getScaledInstance(newWidth, newHeight, Image.SCALE_AREA_AVERAGING);
			BufferedImage biRescaled = toBufferedImage(rescaled, BufferedImage.TYPE_INT_RGB);
			File newFile = new File(newPhotoFilePath);
			ImageIO.write(biRescaled, "jpeg", newFile);
			printLine(newFile.getName());
			if (!isRunning) break;
		}
		//END TEST
	}
	
    public static BufferedImage toBufferedImage(Image image, int type) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        BufferedImage result = new BufferedImage(w, h, type);
        Graphics2D g = result.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return result;
    }

}
