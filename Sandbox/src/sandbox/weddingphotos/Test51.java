package sandbox.weddingphotos;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;

public class Test51 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = "Rename files in numeric order";
	private String[] parameterDescriptions = 
		{
			"6", "regex",
			"9", "recurse",
			"10", "filenameFilter",
			"11", "destinationPath",
			"12", "sourcePath"
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "poppad"
		};
	
	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String regex = getString("regex");
		boolean recurse = getBoolean("recurse");
		String filenameFilter = getString("filenameFilter");
		String sourcePath = getString("sourcePath");
		String destinationPath = getString("destinationPath");
		
		FilenameFilter filter = new sandbox.SandboxFilenameFilter(filenameFilter); 
		File directory = new File(sourcePath);
		File[] files = sandbox.Utilities.listFilesDirsAsArray(directory, filter, recurse);
		
		for (File file : files) {
			printLine(file.getName());
		}
		Arrays.sort(files);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		for (File file : files) {
			printLine(file.getName());
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR);

		HashMap<Integer,NameObject> mapNameFile = new HashMap<Integer,NameObject>();
		int fileIndex = 0;
		Integer[] fileNumbers = new Integer[1000];
		for (File file : files) {
			NameObject nameObject = new NameObject(file.getName(), file);
			String fileName = file.getName();
			String[] fileNameParts = fileName.split(regex);
//			fileNumbers[fileIndex++] = Integer.parseInt(fileNameParts[0]);
			if (fileNameParts.length == 2) {
				printLine(fileNameParts[0] + "." + fileNameParts[1]);
			} else {
				printLine(fileNameParts[0]);
			}
			mapNameFile.put(Integer.parseInt(fileNameParts[0]), nameObject);
			fileIndex++;
		}
		
//		Arrays.sort(fileNumbers);
//		for (int fileNumber : fileNumbers) {
//			printLine(mapNameFile.get(fileNumber));
//		}
		
		
		//END TEST
	}
	
	public class NameObject{
		private String name = "";
		private Object obj = null;
		
		public NameObject(String name, Object obj) {
			this.name = name;
			this.obj = obj;
		}
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Object getObj() {
			return obj;
		}

		public void setObj(Object obj) {
			this.obj = obj;
		}

	}
}
