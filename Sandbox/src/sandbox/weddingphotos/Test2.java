package sandbox.weddingphotos;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display string, focused string, scratch pad string, password, and boolean",
 		parameterDescriptions = {
 				"15", "targetFileSize",
 				"17", "scaledPhotoDirectory",
 				"18", "photoFilePath",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test2 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
			String photoFilePath = getString("photoFilePath");
			String scaledPhotoDirectory = getString("scaledPhotoDirectory");
			float targetFileSize = getFloat("targetFileSize");
			File file = new File(photoFilePath);
			float scale = (float)(Math.sqrt(targetFileSize) / Math.sqrt(file.length()));

			String extension = photoFilePath.substring(photoFilePath.lastIndexOf('.'));
			String newPhotoFilePath = scaledPhotoDirectory +
				File.separator + file.getName().split("\\.")[0]
				+ "_new_" + scale + extension;
			
			BufferedImage image = ImageIO.read(file);
			int w = image.getWidth(), h = image.getHeight();
			int newWidth = (int)(w * scale);
			int newHeight = (int)(h * scale);
			printLine(newWidth);
			printLine(newHeight);
			Image rescaled = image.getScaledInstance(newWidth, newHeight, Image.SCALE_AREA_AVERAGING);
			BufferedImage biRescaled = toBufferedImage(rescaled, BufferedImage.TYPE_INT_RGB);
			ImageIO.write(biRescaled, "jpeg", new File(newPhotoFilePath));
			printLine("New photo file path:\n\t" + newPhotoFilePath);

		 //END TEST
	 }
	    public static BufferedImage toBufferedImage(Image image, int type) {
	        int w = image.getWidth(null);
	        int h = image.getHeight(null);
	        BufferedImage result = new BufferedImage(w, h, type);
	        Graphics2D g = result.createGraphics();
	        g.drawImage(image, 0, 0, null);
	        g.dispose();
	        return result;
	    }

}
