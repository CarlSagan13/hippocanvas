package sandbox.weddingphotos;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display a generated image",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test3 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
//		 String string = getString("string");
		 
			BufferedImage sandboxImage = null;

			String hippoCanvasImageFilePath = System.getProperty("sbx.home.icon.file.path", "blueball.png");
			sandboxImage = ImageIO.read(new File(hippoCanvasImageFilePath));
			Graphics2D g2d = sandboxImage.createGraphics();
			String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
			Color borderColor = new Color(255, 255, 255);
			try {
				borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]), Integer.parseInt(ebc[2]));
			} catch (NumberFormatException nfe) {
				params.printLine("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");
			}
			g2d.setColor(borderColor);
			g2d.fillRect(0, 3 * (sandboxImage.getHeight() / 4), sandboxImage.getWidth(), sandboxImage.getHeight() / 4);
		 //END TEST
	 }

}
