// Tool29.java (erase me later)
package sandbox;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Compare lists of files",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "listFilePath1,listFilePath2"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool29 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		String listFilePath1 = getString("listFilePath1");
		String listFilePath2 = getString("listFilePath2");
		
		String[] saListFile1= Utilities.getFileString(listFilePath1).split("\n");
		List<String> alListFile1= new ArrayList<String>();
		for (String filePath : saListFile1){
			int fileSeparatorPosition = filePath.lastIndexOf(System.getProperty("file.separator"))+ 1;
			int resourceSeparatorPosition = filePath.lastIndexOf("/")+ 1;
			int separatorPosition = fileSeparatorPosition > resourceSeparatorPosition ? fileSeparatorPosition : resourceSeparatorPosition;
			alListFile1.add(filePath.substring(separatorPosition,filePath.length()));
		}

		String[]saListFile2 = Utilities.getFileString(listFilePath2).split("\n");
		List<String> alListFile2 = new ArrayList <String>();
		for (String filePath : saListFile2){
			int fileSeparatorPosition = filePath.lastIndexOf(System.getProperty("file.separator"))+ 1;
			int resourceSeparatorPosition = filePath.lastIndexOf("/") + 1;
	
			int separatorPosition = fileSeparatorPosition > resourceSeparatorPosition ? fileSeparatorPosition : resourceSeparatorPosition;
			alListFile2.add(filePath.substring(separatorPosition,filePath.length()));
		}

		Set<String> set1= new HashSet<String> ();
		for (String line : alListFile1){
			set1.add(line);
		}
		
		Set<String> set2 = new HashSet<String> ();
		for (String line : alListFile2){
			set2.add(line);
		}

		List <String> bothLists = new ArrayList<String> ();
		List<String> listOneOnly = new ArrayList<String> ();
		for (Object line : set1.toArray ()){
			if (set2.contains (line)){
				bothLists.add((String)line);
			}else {
				listOneOnly.add((String)line);
			}
		}
		List<String> listTwoOnly = new ArrayList <String> ();
		for (Object line : set2.toArray()){
			if (!set1.contains(line)){
				listTwoOnly.add((String)line);
			}
		}

		Collections.sort(bothLists);
		printLine (SandboxParameters.VISUAL_SEPARATOR + "\nBOTH LISTS \n"+ SandboxParameters.VISUAL_SEPARATOR);
		for (String line : bothLists){
			printLine(line);
		}
		
		Collections.sort(listOneOnly);
		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST ONE ONLY ("+ listFilePath1+ ")\n"+ SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listOneOnly) {
			printLine(line);
		}
		
		Collections.sort(listTwoOnly);
		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST TWO ONLY ("+ listFilePath2 + ")\n"+ SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listTwoOnly) {
			printLine(line);
		}
		//END TOOL
	}
}