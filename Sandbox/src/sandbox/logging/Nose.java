package sandbox.logging;

import java.io.IOException;
import java.util.logging.*;

public class Nose {
    private static Logger logger = Logger.getLogger("com.wombat.nose");
    private static FileHandler fh;
    public static void main(String argv[]) {
    	try {
        	fh = new FileHandler("/home/dmharte/crap/mylog.txt");
            // Send logger output to our FileHandler.
            logger.addHandler(fh);
            // Request that every detail gets logged.
            logger.setLevel(Level.ALL);
            // Log a simple INFO message.
            logger.info("doing stuff");
            try {
                Wombat.sneeze();
            } catch (Exception ex) {
                logger.log(Level.WARNING, "trouble sneezing", ex);
            }
            logger.fine("done");
    		
    	} catch(IOException ioe) {
    		System.err.println("woopsy doodle");
    	}
    }

}
class Wombat {
	public static void sneeze() {
		
	}
}
