// Tool24.java (erase me later)
package sandbox;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Pretty print JSON",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"jsonString"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool24 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		String jsonString = getString ("jsonString");
		String[]lines = jsonString.split("\n");
		boolean uglyPrint = lines.length > 1;
		
		if (uglyPrint){
			printLine(Utilities.jsonUglyPrint(jsonString));
		} else {
			printLine(Utilities.jsonPrettyPrint(jsonString));
		}
		//END TOOL
	}
}