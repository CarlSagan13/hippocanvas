// Tool23.java (erase me later)
package sandbox;

import java.net.URLDecoder;
import java.net.URLEncoder;

import sandbox.SandboxAnnotation;

@SandboxAnnotation( description = "Encode/Decode URL",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"selectedText",
			SandboxParameters.TRUEFALSE_NAME ,"encode",
			SandboxParameters.YESNO_NAME,"replaceInFocused"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool23 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TEST
		String selectedText = getString("selectedText");
		boolean encode = getBoolean("encode");
		boolean replace = getBoolean("replaceInFocused");

		if (! replace){
			selectedText = params.getFocusedText();
		}
		
		String[]charsetNames = {"US-ASCII","ISO-8859-1","UTF-8","UTF-16BE","UTF-16LE","UTF-16"};
		Object[]charset = this.getValueFromWindowList(charsetNames,"Type encoding");
		if (encode){
			printLine(URLEncoder.encode(selectedText ,(String)charset[0]));
		}else {
			printLine(URLDecoder.decode(selectedText,(String)charset[0]));
		}
		//END TEST
	}
}