//SandboxTextPane.java (erase me later)
package sandbox;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTextPane;

public class SandboxTextPane extends JTextPane implements ISandboxBackedUp, SandboxCommon {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private File file = null;
	private Object mapId = null;
	private SandboxProcess sandboxProcess = null;

	protected boolean isDirty = false;

	protected boolean text = true;
	public SandboxTextPane(){
		
		/*///////////////////////DMH
		java.awt.Font font = new java.awt.Font("Courier", java.awt.Font.PLAIN, 20); 
		this.setFont(font);
		//*//////////////////////////
//		String[] clf = System.getProperty("sbx.conf.textFont","Dialog_2_11").split("_");
		String[] clf = Utilities.getEnvFont();

		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		setFont(textFont);


		this.addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e){
				isDirty = true;
			}
			@Override
			public void keyPressed(KeyEvent e){			
			}
			@Override
			public void keyReleased(KeyEvent e){			
			}
		});

		this.addMouseListener(new SandboxMouseListener());
	}

	public SandboxTextPane(File file, SandboxProcess sandboxProcess) {
		this();
		this.file = file;
		this.sandboxProcess = sandboxProcess;
		sandboxLogger.info("Opening file in "+ file.getPath());
	}
	public File getFile(){
		return file;
	}
	public void setFile(File file){
		this.file = file;
	}

	@Override
	public void setMapId(Object mapId) {
		this.mapId = mapId;
	}

	@Override
	public Object getMapId() {
		// TODO Auto-generated method stub
		return mapId;
	}
	public void saveBackedUp() throws SandboxException {
		if (isEditable() && file != null) {
			if (file.setWritable(true)){				
				try {
					Utilities.saveFileString(file.getPath(), getText());
					file.setWritable(false);
					
				} catch (Exception en){
					String message = "Could not save " + file.getPath();
					sandboxLogger.error(message,en);
					throw new SandboxException("Could not save " + file.getPath());
				}
				
			} else {
				throw new SandboxException("Could not save " + file.getPath());
			}
		}
	}
	
	@Override
	public void close() {
		try {
			saveBackedUp();
			closeBackedUp();
		} catch (SandboxException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void closeBackedUp() throws SandboxException {
		if (isEditable() && isDirty) {
			saveBackedUp();
			if (sandboxProcess != null) {
				String[] aLines = getText().split(System.getProperty("line.separator", "\n"));
				List<String> lines = Arrays.asList(aLines);
				
				try {
					sandboxProcess.saveFileText(lines);
				} catch (IOException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new SandboxException(e.getMessage());
				}
			}
		}
		if (isEditable() && file != null) {
			file.setWritable(true);
		}
		if (sandboxProcess != null) {
			sandboxProcess.setDone(true);
		}
	}
	@Override
	public boolean isDirty(){
		return isDirty;
	}
	@Override
	public void setDirty(boolean dirty)	 {	
		isDirty = dirty;			
	}
	@Override
	public String getBackupText(){		
		return getText();
	}
	@Override
	public boolean isText(){
		return text;
	}
}
