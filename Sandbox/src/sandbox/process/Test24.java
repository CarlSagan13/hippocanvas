package sandbox.process;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use process join",
 		parameterDescriptions = {
// 				"1", "command",
 				SandboxParameters.FOCUSED_NAME, "command",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test24 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String command = getString("command");
		 
		    Runtime rt = Runtime.getRuntime();
		    Process proc = rt.exec(command);
		 
		    // Any error message?
		    Thread errorGobbler
		      = new Thread(new StreamGobbler(proc.getErrorStream(), System.err));
		  
		    // Any output?
		    Thread outputGobbler
		      = new Thread(new StreamGobbler(proc.getInputStream(), System.out));
		 
		    errorGobbler.start();
		    outputGobbler.start();
		 
		    // Any error?
		    int exitVal = proc.waitFor();
		    errorGobbler.join();   // Handle condition where the
		    outputGobbler.join();  // process ends before the threads finish

		 //END TEST
	 }
	 
	 class StreamGobbler implements Runnable {
		  private final InputStream is;
		  private final PrintStream os;
		 
		  StreamGobbler(InputStream is, PrintStream os) {
		    this.is = is;
		    this.os = os;
		  }
		 
		  public void run() {
		    try {
		      int c;
		      while ((c = is.read()) != -1)
		          os.print((char) c);
		    } catch (IOException x) {
		      // Handle error
		    }
		  }
		}


}
