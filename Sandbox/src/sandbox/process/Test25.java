package sandbox.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use process join with lines",
 		parameterDescriptions = {
// 				"1", "command",
 				SandboxParameters.FOCUSED_NAME, "command",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test25 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String command = getString("command");

		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(command);

		// Any error message?
		Thread errorGobbler = new Thread(new StreamGobbler(proc.getErrorStream(), System.err, this));

		// Any output?
		Thread outputGobbler = new Thread(new StreamGobbler(proc.getInputStream(), System.out, this));

		errorGobbler.start();
		outputGobbler.start();

		// Any error?
		int exitVal = proc.waitFor();
		errorGobbler.join(); // Handle condition where the
		outputGobbler.join(); // process ends before the threads finish

		// END TEST
	}

	class StreamGobbler implements Runnable {
		private final InputStream is;
		private final InputStreamReader inputStreamReader;
		private final BufferedReader bufferedReader;
		private final PrintStream os;
		private SandboxTestBase test;

		StreamGobbler(InputStream is, PrintStream os, SandboxTestBase test) {
			this.is = is;
			this.os = os;
			this.inputStreamReader = new InputStreamReader(is);
			this.bufferedReader = new BufferedReader(inputStreamReader);
			this.test = test;
		}

		public void run() {
			try {
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					printLine(line);
				}
			} catch (IOException x) {
				printLine("Error: " + x.getMessage());
				// Handle error
			}
		}
	}

}
