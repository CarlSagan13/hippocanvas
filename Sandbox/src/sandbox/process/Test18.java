package sandbox.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import sandbox.SandboxAnnotation;
import sandbox.SandboxConnection;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(description = SandboxTestBase.UNDER_CONSTRUCTION
		+ "Put a process in the global map", 
		parameterDescriptions = { 
				"16", "processId",
			// SandboxParameters.FOCUSED_NAME, "focusedString",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password",
				SandboxParameters.POPPAD_NAME, "commands",
			// SandboxParameters.TRUEFALSE_NAME, "truefalse",
			// SandboxParameters.YESNO_NAME, "yesno",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test18 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String processId = getString("processId");
		String[] commands = getStringArray("commands");

		String strProcessStart = "/bin/bash";
		if (sandbox.Utilities.isWindows()) {
			strProcessStart = "cmd";
		}

		ProcessBuilder builder = new ProcessBuilder(strProcessStart);
		builder.directory(new File(parameters.getConfigurationDirectory()));
		proc = builder.start();

		initStreams(proc);

		for (String command : commands) {
			runCommand(command);
		}
//		runCommand("cmd /c dir");
//		runCommand("cmd /c echo boonda");
//		runCommand("cmd /c cd C:\\Users\\carls\\GIT\\hippocanvas\\Sandbox\\sound");
//		runCommand("cmd /c dir");

		// END TEST
	}

	Process proc = null;
	InputStream inputStream = null;
	BufferedReader bufferedReader = null;
	BufferedReader errorBufferedReader = null;
	BufferedWriter procStdIn = null;
	OutputStream outputStream = null;
	OutputStreamWriter outputStreamWriter = null;
	InputStream errorStream = null;
	String strProcessEnd = "exit";

	private void initStreams(Process proc) {
		inputStream = proc.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		bufferedReader = new BufferedReader(inputStreamReader);

		errorStream = proc.getErrorStream();
		InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
		errorBufferedReader = new BufferedReader(errorStreamReader);

		outputStream = proc.getOutputStream();
		outputStreamWriter = new OutputStreamWriter(outputStream);
		procStdIn = new BufferedWriter(outputStreamWriter);
	}

	private void runCommand(String command) throws IOException, InterruptedException {
		String line = new String();
		procStdIn.write(command);
		procStdIn.newLine();
		 if (Utilities.isWindows()) {
				procStdIn.write("cmd /c echo " + SandboxParameters.SANDBOX_END_OF_FILE);
		 } else {
			 procStdIn.write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
		 }
		procStdIn.newLine();
		procStdIn.flush();
		
		printLine("Standard Out");
		printALine();
		line = bufferedReader.readLine();
		while (isRunning && line != null && !line.contains(SandboxParameters.SANDBOX_END_OF_FILE)) {
			printLine(line);
			line = bufferedReader.readLine();
		}
		bufferedReader.readLine();
		
		if (errorStream.available() > 0) {
			String	errorLine = errorBufferedReader.readLine();
			printIt("ERROR");
			printALine();
			printLine(errorLine);
		}

	}
}
