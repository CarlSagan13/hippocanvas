package sandbox.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class SandboxProcess {
	Process proc = null;
	InputStream inputStream = null;
	BufferedReader bufferedReader = null;
	BufferedReader errorBufferedReader = null;
	BufferedWriter procStdIn = null;
	OutputStream outputStream = null;
	OutputStreamWriter outputStreamWriter = null;
	InputStream errorStream = null;
	String strProcessEnd = "exit";

	public SandboxProcess(Process proc) {
		super();
		this.proc = proc;
		initStreams(proc);
	}

	private void initStreams(Process proc) {
		inputStream = proc.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		bufferedReader = new BufferedReader(inputStreamReader);

		errorStream = proc.getErrorStream();
		InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
		errorBufferedReader = new BufferedReader(errorStreamReader);

		outputStream = proc.getOutputStream();
		outputStreamWriter = new OutputStreamWriter(outputStream);
		procStdIn = new BufferedWriter(outputStreamWriter);
	}

	protected Process getProc() {
		return proc;
	}

	protected void setProc(Process proc) {
		this.proc = proc;
	}

	protected InputStream getInputStream() {
		return inputStream;
	}

	protected void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	protected BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	protected void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	protected BufferedReader getErrorBufferedReader() {
		return errorBufferedReader;
	}

	protected void setErrorBufferedReader(BufferedReader errorBufferedReader) {
		this.errorBufferedReader = errorBufferedReader;
	}

	protected BufferedWriter getProcStdIn() {
		return procStdIn;
	}

	protected void setProcStdIn(BufferedWriter procStdIn) {
		this.procStdIn = procStdIn;
	}

	protected OutputStream getOutputStream() {
		return outputStream;
	}

	protected void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	protected OutputStreamWriter getOutputStreamWriter() {
		return outputStreamWriter;
	}

	protected void setOutputStreamWriter(OutputStreamWriter outputStreamWriter) {
		this.outputStreamWriter = outputStreamWriter;
	}

	protected InputStream getErrorStream() {
		return errorStream;
	}

	protected void setErrorStream(InputStream errorStream) {
		this.errorStream = errorStream;
	}

}
