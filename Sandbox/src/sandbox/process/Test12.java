package sandbox.process;

import java.io.File;

import sandbox.SandboxAnnotation;
import sandbox.SandboxConnection;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Put a process in the global map",
 		parameterDescriptions = {
 				"16", "processId",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test12 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String processId = getString("processId");

		String strProcessStart = "/bin/bash";
		if (sandbox.Utilities.isWindows()) {
			strProcessStart = "cmd";
		}

		ProcessBuilder builder = new ProcessBuilder(strProcessStart);
		builder.directory(new File(parameters.getConfigurationDirectory()));
		Process proc = builder.start();

		parameters.addToGlobalMap(processId, proc);
		 //END TEST
	 }

}
