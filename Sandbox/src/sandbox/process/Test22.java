package sandbox.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import sandbox.SandboxAnnotation;
import sandbox.SandboxConnection;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(description = SandboxTestBase.UNDER_CONSTRUCTION
		+ "Use a process in the global map", 
		parameterDescriptions = { 
				"16", "processId",
			 SandboxParameters.FOCUSED_NAME, "commands",
			// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
			// SandboxParameters.PASSWORD_NAME, "password",
			//	SandboxParameters.POPPAD_NAME, "commands",
			// SandboxParameters.TRUEFALSE_NAME, "truefalse",
			// SandboxParameters.YESNO_NAME, "yesno",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test22 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String processId = getString("processId");
		String[] commands = getStringArray("commands");

		sandboxProcess = this.getGlobalMapThing(processId, SandboxProcess.class);
		if (sandboxProcess == null) {
			printLine("Creating new process");
			String strProcessStart = "/bin/bash";
			if (sandbox.Utilities.isWindows()) {
				strProcessStart = "cmd";
			}

			ProcessBuilder builder = new ProcessBuilder(strProcessStart);
			builder.directory(new File(parameters.getConfigurationDirectory()));
			sandboxProcess = new SandboxProcess(builder.start());
			this.addToGlobalMap(processId, sandboxProcess);
		}

		for (String command : commands) {
			int status = runCommand(command);
			if (status < 0) {
				break;
			}
		}
//		runCommand("cmd /c dir");
//		runCommand("cmd /c echo boonda");
//		runCommand("cmd /c cd C:\\Users\\carls\\GIT\\hippocanvas\\Sandbox\\sound");
//		runCommand("cmd /c dir");

		// END TEST
	}
	SandboxProcess sandboxProcess = null;

	private int runCommand(String command) throws IOException, InterruptedException {
		String line = new String();
		sandboxProcess.getProcStdIn().write(command);
		sandboxProcess.getProcStdIn().newLine();
		sandboxProcess.getProcStdIn().write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
		sandboxProcess.getProcStdIn().newLine();
		sandboxProcess.getProcStdIn().flush();

		printLine("Command");
		printALine();
		printLine(command);
		
		printLine("Standard Out");
		printALine();
		line = sandboxProcess.getBufferedReader().readLine();
		while (isRunning && line != null && !line.contains(SandboxParameters.SANDBOX_END_OF_FILE)) {
			printLine(line);
			line = sandboxProcess.getBufferedReader().readLine();
		}
		if (Utilities.isWindows()) {
			sandboxProcess.getBufferedReader().readLine();
		}
		
		if (sandboxProcess.getErrorStream().available() > 0) {
			String errorLine = new String();
			
//*///////////////////////////DMH			
			printIt("ERROR");
			printALine();
			while (isRunning && (sandboxProcess.getErrorStream().available() > 0)
					&& (errorLine = sandboxProcess.getErrorBufferedReader().readLine()) != null) {
				printLine(errorLine);
			}
/*//////////////////////////////
			printIt("ERROR");
			printALine();
			while (isRunning && (errorLine = sandboxProcess.getErrorBufferedReader().readLine()) != null) {
				printLine(errorLine);
				if (sandboxProcess.getErrorStream().available() <= 0) {
					break;
				}
			}
//*//////////////////////////////

			return -1;
		}
		return 0;
	}
	
//	class SandboxProcess {
//		Process proc = null;
//		InputStream inputStream = null;
//		BufferedReader bufferedReader = null;
//		BufferedReader errorBufferedReader = null;
//		BufferedWriter procStdIn = null;
//		OutputStream outputStream = null;
//		OutputStreamWriter outputStreamWriter = null;
//		InputStream errorStream = null;
//		String strProcessEnd = "exit";
//
//		public SandboxProcess(Process proc) {
//			super();
//			this.proc = proc;
//			initStreams(proc);
//		}
//
//		private void initStreams(Process proc) {
//			inputStream = proc.getInputStream();
//			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//			bufferedReader = new BufferedReader(inputStreamReader);
//
//			errorStream = proc.getErrorStream();
//			InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
//			errorBufferedReader = new BufferedReader(errorStreamReader);
//
//			outputStream = proc.getOutputStream();
//			outputStreamWriter = new OutputStreamWriter(outputStream);
//			procStdIn = new BufferedWriter(outputStreamWriter);
//		}
//
//		protected Process getProc() {
//			return proc;
//		}
//
//		protected void setProc(Process proc) {
//			this.proc = proc;
//		}
//
//		protected InputStream getInputStream() {
//			return inputStream;
//		}
//
//		protected void setInputStream(InputStream inputStream) {
//			this.inputStream = inputStream;
//		}
//
//		protected BufferedReader getBufferedReader() {
//			return bufferedReader;
//		}
//
//		protected void setBufferedReader(BufferedReader bufferedReader) {
//			this.bufferedReader = bufferedReader;
//		}
//
//		protected BufferedReader getErrorBufferedReader() {
//			return errorBufferedReader;
//		}
//
//		protected void setErrorBufferedReader(BufferedReader errorBufferedReader) {
//			this.errorBufferedReader = errorBufferedReader;
//		}
//
//		protected BufferedWriter getProcStdIn() {
//			return procStdIn;
//		}
//
//		protected void setProcStdIn(BufferedWriter procStdIn) {
//			this.procStdIn = procStdIn;
//		}
//
//		protected OutputStream getOutputStream() {
//			return outputStream;
//		}
//
//		protected void setOutputStream(OutputStream outputStream) {
//			this.outputStream = outputStream;
//		}
//
//		protected OutputStreamWriter getOutputStreamWriter() {
//			return outputStreamWriter;
//		}
//
//		protected void setOutputStreamWriter(OutputStreamWriter outputStreamWriter) {
//			this.outputStreamWriter = outputStreamWriter;
//		}
//
//		protected InputStream getErrorStream() {
//			return errorStream;
//		}
//
//		protected void setErrorStream(InputStream errorStream) {
//			this.errorStream = errorStream;
//		}
//	}
}
