//SandboxFilenameFilter.java (erase me later)
package sandbox; 
 
 import java.io.File; 
 import java.io.FilenameFilter; 
 import java.util.HashSet; 
 import java.util.Set; 
 
 public class SandboxFilenameFilter implements FilenameFilter, SandboxCommon {
 
 	 String regex; 
 	 String contains; 
 	 Set<String> extensions = new HashSet<String> (); 
 
 	 public SandboxFilenameFilter(){
 	 }
 	 
 	 public SandboxFilenameFilter(String ext) {
 	 	 extensions. add(ext); 
 	 }
 
 	 public SandboxFilenameFilter(Set<String> extensions){
 	 	 this. extensions = extensions; 
 	 }
 
 	 public boolean accept (File dir, String name){
 	 	 if (extensions. size()> 0){
 	 	 	 if (extensions. contains("."))return true; 
 	 	 	 int lastIndexOfPeriod = name.lastIndexOf('.'); 
 	 	 	 if (lastIndexOfPeriod >= 0){
 	 	 	 	String extension = name. substring(name. lastIndexOf ('.')); 
 	 	 	 	 if (extensions. contains(extension)){
 	 	 	 	 	 return true; 
 	 	 	 	 }
 	 	 	 }
 	 	 }
 	 	 if (regex != null){
 	 	 	 return name . matches(regex); 
 	 	 }
 	
 	 	 if (contains != null){
 	 	 	 return name. toLowerCase(). contains(contains. toLowerCase()); 
 	 	 }
 	
	 return false; 
 	 }
 
 	 public String getRegex(){
 		 return regex; 
 	 }
 
 	 /**
 	 *@param regex the regex to set
 	 */
 	 public void setRegex(String regex) {
 	 	 this. regex = regex; 
 	 }
 
 	 /**
 	 *@return the contains
 	 */
 	 public String getContains (){
 	 	 return contains; 
 	 }
 
 	 /**
 	 *@param contains the contains to set
 	 */
 	 public void setContains(String contains){
 	 	 this . contains = contains; 
 	 }
 }
