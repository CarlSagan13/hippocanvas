package sandbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
//import javax.swing.JOptionPane;

@SandboxAnnotation(
		description = "Set system properties v1.3", 
		parameterDescriptions = {
				SandboxParameters.FOCUSED_NAME,	"cdNamesValues",
				SandboxParameters.TRUEFALSE_NAME, "clearProperties"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool8 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] namesValues = getStringArray("cdNamesValues");
		boolean clearProperties = getBoolean("clearProperties");
		String entered = getString("cdNamesValues");
		String rawEntered = params.getFocusedText();
		
		if (namesValues.length > 20) {
			if (!confirm("Confirm that you want to set " + namesValues.length + " properties.")) {
				printLine("YOU CANCELLED");
				return;
			} else {
				printLine("*** SETTING " + namesValues.length + " VARIABLES ***");
			}
		}
		
		if (clearProperties) {
			if (!confirm("Confirm that you want to clear the properties")) {
				clearProperties = false;
			} else {
				printLine("*** CLEARING PROPERTY(IES) ***");
			}
		}
		
		boolean isMultiLine = entered.split("\n").length > 1;
		
		if (!isMultiLine) { //single line
			if (clearProperties && entered.contains(",")) {
				String systemId = entered.split(",")[0];
				System.clearProperty(systemId);
			} else if (clearProperties && !entered.contains(",")) { //clearing
				printLine(entered + "," + System.getProperty(entered));
				System.clearProperty(entered);
			} else {
				String[] nameValue = entered.split(",", 2);
				String name = nameValue[0];
				String value = nameValue[1];
				if (alertOnPattern(entered, params.tabbedPane)) {
					System.setProperty(name, value);
				}
			}
		} else { //multi-line
			if (!namesValues[0].contains(",") && !clearProperties) { // name/value
				if (namesValues.length > 2) {
					String message = "*** INCORRECT FORMAT FOR SETTING (To clear properties, select clearProperties) ***"; 
					//JOptionPane.showMessageDialog(parameters.tabbedPane, message);
					showConfirmMessage(message);

					printLine(message);
				} else {
					System.setProperty(namesValues[0], namesValues[1]);
				}
			} else { // name,value/name,value/...
				List<String> processedNamesValues = new ArrayList<String>();
				for (String line : rawEntered.split("\n")) {
					String[] nameValue = line.split(",",2);
//					String name = replaceValuesLocal(nameValue[0]);
					
					String processedName = replaceValuesLocal(nameValue[0]);
					String replacedName = params.replaceValues(processedName);

					if (nameValue.length == 2) {
						String processedValue = replaceValuesLocal(nameValue[1]);
						String replacedValue = params.replaceValues(processedValue);
						processedNamesValues.add(replacedName + "," + replacedValue);
						localProps.put(replacedName, replacedValue);
					} else if (nameValue.length == 1) {
						processedNamesValues.add(replacedName); //For clearing property
					}
				}
				
				for (String nameValue : processedNamesValues) {
					if (!nameValue.contains(",") || clearProperties) {
						printLine(nameValue + "," + System.getProperty(nameValue));
						String name = nameValue.split(",")[0];
						System.clearProperty(name);
						localProps.remove(name);
					} else {
						String name = nameValue.split(",")[0];
						String value = nameValue.split(",",2)[1];
						if (alertOnPattern(nameValue, params.tabbedPane)) {
							System.setProperty(name, value);
						}
					}
				}
			}
		}
		// END TEST
	}
	private boolean alertOnPattern(String string, JComponent frame) {
		Matcher confirmMatcher = null;
		String strPattern = "sandbox\\.java.*\\,.*\\s.*";
		confirmMatcher = Pattern.compile(strPattern).matcher(string);
		if (confirmMatcher.find()) {
			int decision = this.showConfirmMessage("Are you sure?\n\t" + string);
			if (decision != 1) {
				return false;
			}
//			int decision = JOptionPane.showConfirmDialog(frame, "Are you sure? " + string);
//			if (decision != 0) {
//				return false;
//			}
		}
		return true;
	}
	
	private Properties localProps = new Properties();

//*//////////////////////////////////DMH	
	private String replaceValuesLocal(String string) throws SandboxException {
		StringBuffer stringBuffer = new StringBuffer(string);
		String[] groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer);
		int sbxTSEnd = 0;
		while (groupsEnv != null) {
			this.checkRunning();
			int sbxTSStart = stringBuffer.indexOf(groupsEnv[0], sbxTSEnd);
			sbxTSEnd = sbxTSStart + groupsEnv[0].length();
			String environmentName = groupsEnv[1];
			String localValue = localProps.getProperty(environmentName);
			if (localValue != null) {
				stringBuffer.replace(sbxTSStart, sbxTSEnd, localValue);
				sbxTSEnd = sbxTSStart + localValue.length();
			}
			groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer, sbxTSEnd);
		}
		return stringBuffer.toString();
	}
/*/////////////////////////////////////
	private String replaceValuesLocal(String string) throws SandboxException {
		StringBuffer stringBuffer = new StringBuffer(string);
		String[] groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer);
		int sbxTSEnd = 0;
		while (groupsEnv != null) {
			this.checkRunning();
			int sbxTSStart = stringBuffer.indexOf(groupsEnv[0], sbxTSEnd);
			sbxTSEnd = sbxTSStart + groupsEnv[0].length();
			String environmentName = groupsEnv[1];
			String localValue = localProps.getProperty(environmentName);
			if (localValue != null) {
				stringBuffer.replace(sbxTSStart, sbxTSEnd, localValue);
			}
			groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer, sbxTSEnd);
		}
		return stringBuffer.toString();
	}
//*/////////////////////////////////////	

}
