// Tool44.java (erase me later)
package sandbox;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = SandboxParameters.UNDER_CONSTRUCTION + "Find/Delete large files",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"largerThan,searchDirectory",
			SandboxParameters.TRUEFALSE_NAME,"recurse",
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool44 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		int largerThan = getInteger("largerThan");
		String searchDirectory = getString("searchDirectory");
		boolean recurse = getBoolean("recurse");
		
		Collection<File> files = sandbox.Utilities.listFiles(new File(searchDirectory),
				null,
				new SandboxFileFilter(largerThan),
				recurse,this);
		
		List <File> filteredFiles = new ArrayList<File>();
		for (File file : files){
			if (file.isFile()){
				printLine(file.length());
				printLine(file.getPath());

				filteredFiles.add(file);
			}
		}
		
		if (filteredFiles.size()== 0){
			printLine("No files larger than "+ largerThan);
			return;

		}

		String[]headers = {"Size","File Name","Last Modified","File Path"};
		String[][]columnRows = new String[filteredFiles.size()][headers.length];
		int rowIndex = 0;
		for (File file : filteredFiles){
			columnRows[rowIndex][0]= String.valueOf (file.length());
			columnRows[rowIndex][1]= file.getName ();
			columnRows[rowIndex][2]= Utilities.parseDate(new Date(file.lastModified()),SandboxParameters.DATE_FORMAT);
			columnRows[rowIndex][3]= file.getPath();
			rowIndex++;
		}
		
//		NumberSort sorter = new NumberSort();
//		Arrays.sort(columnRows,sorter);
		JTable table = this.tabulate(columnRows, headers, getTestName(), false);

		SandboxMessage message = new sandbox.SandboxMessage("Select files",
				"Select files to delete from table, and hit [OK] (this will close in 10 minutes)",
				params.getTabbedPane(), 10, 60000, true, true, 0, null);
		if (message.getStatus()< 0){
			printLine("ABORTED: No files were deleted");
			return;
		}
		
		int[]selectedRows = table.getSelectedRows();
//		TableModel model = table.getModel();
		int total = 0;
		for (int row : selectedRows) {
			String filePath = (String)table.getValueAt(row,filePathColNo);
			int size = 0;
			try {
				String strSize = (String)table.getValueAt(row,1);
				size = Integer.parseInt(strSize);
				total += size;
			}catch(NumberFormatException nfe){
				printLine(filePath + "did not have a valid size");
				continue;
			}
			File file = new File(filePath);
			file.delete();
			printLine(filePath);

		}
		printLine("TOTAL DELETED: "+ total);
		//END TEST
	}

	private static int filePathColNo = 4;

}