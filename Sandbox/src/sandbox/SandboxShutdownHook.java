package sandbox;

import javax.swing.JOptionPane;

public class SandboxShutdownHook {
	private SandboxFrameBase frame = null;

	public SandboxShutdownHook(SandboxFrameBase frame) {
		this.frame = frame;
	}

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Shutdown Hook is running !");
				SandboxFrameBase frame = (SandboxFrameBase) SandboxParameters.frame;
				JOptionPane.showMessageDialog(frame, "Shutdown Hook is running !");
				frame.closeSandbox();
//				SandboxParameters params = SandboxParameters.createSandboxParameters(true);
			}
		});
		System.out.println("Application Terminating ...");
	}

}
