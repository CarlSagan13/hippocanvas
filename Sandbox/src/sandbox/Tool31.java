// Tool31.java (erase me later)
package sandbox;
import sandbox.SandboxAnnotation;

@SandboxAnnotation( 
		description = "Count characters (including spaces)and lines (not including spaces)",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "string",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool31 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TOOL
		String string = params.getFocusedText();//have to get the value this way instead of getString()
			//because want the actual string,without automatic replacement
		printLine("Character length: " + string.length());
		printLine("Line count: " + string.split("\n").length);
		printLine("Bytes: " + Utilities.bytesToHex(string.getBytes(), "-"));
		//END TOOL
	}
}

