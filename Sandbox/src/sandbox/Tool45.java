package sandbox;

import java.io.File;

@SandboxAnnotation(
        description = "Run commands serially in a separate process",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "commandWorkspace",
 				SandboxParameters.YESNO_NAME, "showProcessOutput",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool45 extends SandboxTestBase {
	 
	 @Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		boolean show = true;
		boolean showProcessOutput = getBoolean("showProcessOutput");
		String[] commandWorkspace = getStringArrayNoDelim("commandWorkspace");
		if (commandWorkspace.length % 2 != 0) {
			throw new SandboxException("Each command must include a working directory");
		}

		for (int index = 0; index < commandWorkspace.length; index += 2) {
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			String command = commandWorkspace[index];
			String workSpace = commandWorkspace[index + 1];
			printLine("COMMAND: " + command);
			printLine("WORKSPACE: " + workSpace);
			File workingDir = new File(workSpace);
			long delay = 0;

			if (workingDir.exists() && workingDir.isDirectory()) {
				setTooltip(command + " :: " + workingDir);
				delay = runInSeparateProcess(command, workingDir, show, showProcessOutput);
				if (delay < 0) {
					break;
				}
			} else {
				printLine(workingDir.getPath() + " is not a working directory: ABORTING");
				break;
			}
			printLine(SandboxParameters.VISUAL_SEPARATOR + File.separator + "Delay = " + delay);
		}
		// END TEST
	}

}
