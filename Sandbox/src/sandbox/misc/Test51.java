package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Double a negative number",
 		parameterDescriptions = {

			SandboxParameters.FOCUSED_NAME, "number"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test51 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
//		short number = Short.parseShort((String)parameterValues.get("number"));
//		double number = Double.parseDouble((String)parameterValues.get("number"));
//		long number = Long.parseLong((String)parameterValues.get("number"));
		int number = Integer.parseInt((String)parameterValues.get("number"));
		printLine(number + number);
		//END TEST
	}
}
