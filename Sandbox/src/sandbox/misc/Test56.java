package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display character values in a string",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "scratchPadString",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test56 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String scratchPadString = getString("scratchPadString");
		
		for (int i = 0; i < scratchPadString.length(); i++) {
			char c = scratchPadString.charAt(i);
			printLine(i + " :: " + c + " :: " + (int)c);
		}
		//END TEST
	}
}
