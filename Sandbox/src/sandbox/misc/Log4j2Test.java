package sandbox.misc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4j2Test {

//	private static final Logger logger = LogManager.getLogger("Sandbox");
	private static final Logger logger = LogManager.getLogger(Log4j2Test.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.err.println("Hello from " + Log4j2Test.class.getName());
		logger.debug("Hello world - debug log");
		logger.info("Hello world - info log");
		logger.warn("Hello world - warn log");
		logger.error("Hello world - error log");
	}

}
