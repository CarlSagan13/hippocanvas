package sandbox.misc;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Show color",
 		parameterDescriptions = {
 				"12", "colorInHex",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test23 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String colorInHex = getString("colorInHex");
		 javax.swing.text.JTextComponent component = (javax.swing.text.JTextComponent)params.getSelectedTabComponent();
		 java.awt.Color color = new java.awt.Color(java.lang.Integer.parseInt(colorInHex, 16)); 
		 component.setBackground(color);
		 //END TEST
	 }

}
