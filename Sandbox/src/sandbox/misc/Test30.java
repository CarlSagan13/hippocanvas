package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Play with class loader",
 		parameterDescriptions = {


 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test30 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		printLine("Hello from " + this.getClass().getSimpleName());
		
	    printLine("ClassLoaderDemo1 starting");
	    ClassLoaderDemo1 loader = new ClassLoaderDemo1();
	    Class theClass = null;
	    Object obj;
	    try {
	      /* Load the "Demo" class from memory */

	      printLine("About to load class  Demo");
	      theClass = loader.loadClass("Demo", true);
	      printLine("About to instantiate class Demo");
	      obj = theClass.newInstance();
	      printLine("Got Demo class loaded: " + obj);

	      /* Now try to call a method */

	      Method method = theClass.getMethod("test", null);
	      method.invoke(obj, null);

	    } catch (InvocationTargetException e) {
	      // The invoked method threw an exception. We get it
	      // wrapped up inside another exception, hence the
	      // extra call here:
	      e.getTargetException().printStackTrace();
	      printLine("Could not run test method");
	    } catch (Exception e) {
	      e.printStackTrace();
	      printLine("Could not run test method");
	    }

	    /**
	     * Try to load some arbitrary class, to see if our ClassLoader gets
	     * called.
	     */
	    printLine("Trying to load an unrelated class");
	    java.awt.image.DirectColorModel jnk = new java.awt.image.DirectColorModel(
	        24, 8, 8, 8);
	    System.out
	        .println("Load an unrelated class - was your ClassLoader called?");

	    /** Try to instantiate a second ClassLoader */
	    printLine("Trying to install another ClassLoader");
	    ClassLoaderDemo1 loader2 = new ClassLoaderDemo1();
	    printLine("Instantiated another ClassLoader...");

		//END TEST
	}
}
