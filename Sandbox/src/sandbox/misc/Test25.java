package sandbox.misc;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Generate pseudo-random GUID",
 		parameterDescriptions = {
 				"25", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test25 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 
		 java.util.UUID uuid = java.util.UUID.randomUUID();
		 printLine(uuid.toString());
		
		java.util.UUID uuidCs = java.util.UUID.nameUUIDFromBytes(string.getBytes());
		printLine("Generated from " + string + ":\n\t" + uuidCs.toString());
		 //END TEST
	 }

}
