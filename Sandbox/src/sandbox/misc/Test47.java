package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class Test47 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = "Load comma-delimited file, format as my calendar";
	private String[] parameterDescriptions = 
		{"2", "lineNumber", "18", "filepath"};
	
	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String filepath = (String)parameterValues.get("filepath");
//		int lineNumber = Integer.parseInt((String)parameterValues.get("lineNumber"));
		String fileContent = sandbox.Utilities.getFileString(filepath);
		java.util.StringTokenizer st = new java.util.StringTokenizer(fileContent, "\"");
		
		//throw away the header
		for (int i = 0; i <= 13; i++) {
			st.nextToken();
		}
		
		while (st.hasMoreTokens()) {
			YahooCalendarEntry yce = new YahooCalendarEntry();
			String subject = st.nextToken();
			if (!subject.trim().equals("")) {
				yce.setSubject(subject);
				st.nextToken(); //throw away comma

				String strStartDate = st.nextToken();
				if (!strStartDate.equals(",")) {
					st.nextToken();
					String strStartTime = st.nextToken();
					if (!strStartTime.equals(",")) {
						st.nextToken();
					} else {
						strStartTime = "";
					}
					
					String dateTimeFormat = "";
					if (!strStartDate.trim().equals("")) {
						dateTimeFormat = "MM/dd/yyyy";
						if (!strStartTime.trim().equals("")) {
							dateTimeFormat += " hh:mm aaa";
						}
						java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat(dateTimeFormat);
						String strStartDateTime = strStartDate + " " + strStartTime;
						java.util.Date startDate = dateFormat.parse(strStartDateTime);
						yce.setStartDate(startDate);
					}
				} else {
					st.nextToken();
				}
				
				String strEndDate = st.nextToken();
				if (!strEndDate.equals(",")) {
					st.nextToken();
					String strEndTime = st.nextToken();
					if (!strEndTime.equals(",")) {
						st.nextToken();
					} else {
						strEndTime = "";
					}
					
					String dateTimeFormat = "";
					if (!strEndDate.trim().equals("")) {
						dateTimeFormat = "MM/dd/yyyy";
						if (!strEndTime.trim().equals("")) {
							dateTimeFormat += " hh:mm aaa";
						}
						java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat(dateTimeFormat);
						String strEndDateTime = strStartDate + " " + strEndTime;
						java.util.Date endDate = dateFormat.parse(strEndDateTime);
						yce.setStartDate(endDate);
					}
				} else {
					st.nextToken();
				}
				
				String strAllDayEvent = st.nextToken();
				yce.setAllDayEvent(new Boolean(strAllDayEvent));
				st.nextToken();
				String description = st.nextToken();
				if (!description.equals(",")) {
					yce.setDescription(description);
				}

				
				
/*//////////////DMH				
				java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm aaa");
				String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
				// create a Pacific Standard Time time zone
				SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
				
				// set up rules for daylight savings time
				pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
				pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

				// create a GregorianCalendar with the Pacific Daylight time zone
				// and the current date and time
				Calendar calendar = new GregorianCalendar(pdt);
				calendar.setTime(yce.getStartDate());

//				printLine("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));


				
				
//				printLine(calendar.get(Calendar.DAY_OF_WEEK)
//						+ " " + dateFormat.format(yce.getStartDate())
//						+ " " + yce.getSubject());
				printLine(DayOfWeek.getDay(calendar.get(Calendar.DAY_OF_WEEK) - 1)
						+ " " + dateFormat.format(yce.getStartDate())
						+ " " + yce.getSubject());
/*/////////////////				
				java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm aaa");
				printLine(dateFormat.format(yce.getStartDate()) + " " + yce.getSubject());
//*/////////////////				
				
				
				
				
				printLine("*************************************************");
			}
		}
		//END TEST
	}
}
