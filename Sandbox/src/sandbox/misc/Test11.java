package sandbox.misc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Only load part of a file",
 		parameterDescriptions = {
 				"18", "filePath",
 				"19", "startLine",
 				"20", "numLines"
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test11 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String filePath = getString("filePath");
		 int startLine = getInteger("startLine");
		 int numLines = getInteger("numLines");
//		 char delimiter = getChar("delimiter");
		 
//		 File file = getFileDir(filePath);
		 
		 
			FileReader fileReader = null;
			fileReader = new FileReader(filePath);
			BufferedReader bufferedReader = new BufferedReader(fileReader );

			List<String> lines = new ArrayList<String>();
			
			String string = "";
			int linePos = 0;
			int lineCount = 0;
//			int lastLineCount = 0;
			while((string = bufferedReader.readLine())!= null){
				if (linePos > startLine && lineCount < numLines) {
					lines.add(string);
					lineCount++;
				}
				linePos++;
			}

			fileReader.close();

			for (String line : lines){
				printLine(line);
			}
			lines.clear();

		 //END TEST
	 }

}
