package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.Graphics;

import javax.swing.plaf.basic.BasicArrowButton;

public class SandboxComboBoxButton1 extends BasicArrowButton {
	static final long serialVersionUID = 0;
	private String buttonLabel = "?";
	
	public SandboxComboBoxButton1() {
		super(BasicArrowButton.WEST);
		this.buttonLabel = this.getText();
	}
	
//	public void setText(String text) {
//		super.setText(text);
//		this.buttonLabel = text;
//	}
	public void paint(Graphics g) {
//		this.paintTriangle(g, 1, 1, 1, BasicArrowButton.SOUTH_WEST, true);
		g.drawString(this.buttonLabel, 3, 12);
	}
}
