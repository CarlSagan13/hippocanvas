package sandbox.misc;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class Test2 extends SandboxTestBase {
	/**
	 *Instantiate for dynamic loading
	 */
	private String description = SandboxParameters.UNDER_CONSTRUCTION + 
		"More Mockito";
	private String[]parameterDescriptions = 
		{
			"7", "strings",
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "poppad",
			SandboxParameters.TRUEFALSE_NAME, "trueFalse"
		};
	
	public void init(){
		super.init(description, parameterDescriptions );
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] strings = getStringArray("strings");
		String string = getString("strings");
		boolean trueFalse = getBoolean("trueFalse");
		
		MockitoAnnotations.initMocks(this);

		when(server.getSomething(anyString())).thenReturn("Chinga Te");

		Resource resource = new Resource();
		resource.server = server;
		
		assertEquals(resource.getSomethingFromService("Whatever dude"), "Chinga Te");
		//END TEST
	}
	@Mock Server server;
	
	interface Server {
		String aString = null;
		
		String getSomething(String aString);
		void dontGetSomething(String aString);
	}
	class Resource {
		Server server = null;
		String getSomethingFromService(String aString) {
			return server.getSomething(aString);
		}
	}
	
}
