package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display characters 0-4096",
 		parameterDescriptions = {
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test55 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		for (int index = 0; index < 14096; index++) {
			String hex = (index < 16 ? "0" + Integer.toHexString(index) : Integer.toHexString(index));
			printLine(index + " :: " + hex + " :: " + (new Character((char)index)));
		}
		//END TEST
	}
}
