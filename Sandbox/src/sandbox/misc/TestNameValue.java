package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class TestNameValue {
	private Object name = null;
	private Object value = null;
	
	public TestNameValue(Object name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public Object getName() {
		return this.name;
	}
	public void setName(Object name) {
		this.name = name;
	}
	
	public Object getValue() {
		return this.value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
