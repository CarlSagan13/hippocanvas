package sandbox.misc;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Instantiate any class by name",
 		parameterDescriptions = {
 				"4", "classFQN",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test8 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String classFQN = getString("classFQN");
		Class aClass = Class.forName(classFQN);
		printLine(aClass.getCanonicalName());
		Object obj = aClass.newInstance();
		printLine(obj.getClass().getName());
		
		for (Field field : obj.getClass().getDeclaredFields()) {
			printLine(field.getName());
		}
		
		for (Method method : obj.getClass().getDeclaredMethods()) {
			printLine(method.getName());
		}
		//END TEST
	}
}
