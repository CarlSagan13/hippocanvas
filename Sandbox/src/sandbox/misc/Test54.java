package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Logarithms base 10",
 		parameterDescriptions = {

			"10", "doubleNumber", 
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test54 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String string = getString("string");
		printLine("Hello from " + this.getClass().getSimpleName());
		double doubleNumber = Double.parseDouble(getString("doubleNumber"));
		double logBase10 = java.lang.Math.log10(doubleNumber);
		printLine(logBase10);
		double doubleTimes10 = 10 * logBase10;
		int intDoubleTimes10 = (new Double(doubleTimes10)).intValue();
		printLine("intDoubleTimes10 = " + intDoubleTimes10);
		//END TEST
	}
}
