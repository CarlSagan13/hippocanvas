package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class Test24 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public Test24() {
		super(SandboxParameters.createSandboxParameters());
		init();
	}

	public Test24(SandboxParameters parameters) {
		super(parameters);
		init();
	}

	public void init() {
		this.setTestName(this.getClass().getSimpleName());
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(1), "buttonText");
		this.setDescription("Add SandboxComboBoxButton to test component: 1-buttonText");
		this.setNumParams(1);
	}

	public void test(SandboxParameters sandboxParameters) {
		String s = sandboxParameters.getParameterValueAt(1);
		try {
//			javax.swing.JButton button = new javax.swing.JButton(s);
			SandboxComboBoxButton button = new SandboxComboBoxButton();
			button.setText(s);
			sandboxParameters.addTestComponent(button);
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + ": "
					+ en.getMessage());
		} finally {
			//put cleanup here
		}
	}
}
