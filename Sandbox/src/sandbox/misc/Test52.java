package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Play with iterations",
 		parameterDescriptions = {

			"8", "cdString"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test52 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] strings = this.getStringArray("cdString");
		for (String string : strings) {
			printLine(string);
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		
		java.util.Collection<String> listCollection = new java.util.ArrayList(strings.length);
		for (String string : strings) {
			listCollection.add(string);
		}
		for (String string : listCollection) {
			printLine(string);
		}
		
		//END TEST
	}
}
