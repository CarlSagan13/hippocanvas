package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class SandboxComboBoxUI extends BasicComboBoxUI {
	private String buttonLabel = "25";
	
	public SandboxComboBoxUI(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}
	
	public static ComponentUI createUI(JComponent c) {
		return new SandboxComboBoxUI(c.getName());
	}

	protected JButton createArrowButton() {
		
//*//////////DMH
//		javax.swing.ImageIcon imageIcon = new javax.swing.ImageIcon();
		SandboxComboBoxButton4 newButton = new SandboxComboBoxButton4(this.buttonLabel);
//		SandboxComboBoxButton2 newButton = new SandboxComboBoxButton2();
//		newButton.setName(this.buttonLabel);
		
		System.out.println(this.getClass().getName() + ".createArrowButton(dmh239) : name = " + newButton.getName());
		
//		SandboxComboBoxButton newButton = new SandboxComboBoxButton();
//		newButton.setText(buttonLabel);
//		newButton.setIcon(imageIcon);
/*/////////////
		JButton newButton = new BasicArrowButton(BasicArrowButton.EAST);
//*/////////////
		
		return newButton;
	}
}
