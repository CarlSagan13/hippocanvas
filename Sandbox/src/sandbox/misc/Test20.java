package sandbox.misc;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

import sandbox.misc.PopupComboSample.MyComboBoxUI;

public class Test20 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public Test20() {
		super(SandboxParameters.createSandboxParameters());
		init();
	}

	public Test20(SandboxParameters parameters) {
		super(parameters);
		init();
	}

	public void init() {
		this.setTestName(this.getClass().getSimpleName());
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(1), "string");
		this.setDescription("Show combo box with numbered drop-down button");
		this.setNumParams(1);
	}

	public void test(SandboxParameters sandboxParameters) {
		String s = sandboxParameters.getParameterValueAt(1);
		try {
		    String labels[] = { "A", "B", "C", "D", "E", "F", "G" };
		    JFrame frame = new JFrame("Popup JComboBox");
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		    JComboBox comboBox = new JComboBox(labels);
		    comboBox.setUI((ComboBoxUI) MyComboBoxUI.createUI(comboBox));
		    frame.add(comboBox, BorderLayout.NORTH);

		    frame.setSize(300, 200);
		    frame.setVisible(true);
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + ": "
					+ en.getMessage());
		} finally {
			//put cleanup here
		}
	}
	
	static class MyComboBoxUI extends BasicComboBoxUI {
  
		public static ComponentUI createUI(JComponent c) {
			return new MyComboBoxUI();
		}

		protected JButton createArrowButton() {
			JButton newButton = new JButton("8");
			java.awt.Dimension dimension = new java.awt.Dimension(100, 100);
			newButton.setSize(dimension);
			newButton.setMinimumSize(dimension);
			
			java.awt.Font font = new java.awt.Font("Courier", java.awt.Font.PLAIN, 6); 
			newButton.setFont(font);
    	
			newButton.validate();

			return newButton;
		}
	}	
}
