package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class Test41 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	private String description = "Load comma-delimited file";
	private String[] parameterDescriptions = 
		{"18", "filepath"};
	
	public void init() {
		super.init(description, parameterDescriptions);
	}

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String filepath = (String)parameterValues.get("filepath");
		String string = sandbox.Utilities.getFileString(filepath);
		String[] values = string.split("\n");
		for (int i = 0; i < values.length; i++) {
			printLine(values[i]);
		}
		//END TEST
	}
}
