package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Set value in a static class",
 		parameterDescriptions = {

			"1", "string", 
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "popPadString"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test31 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String string = getString("string");
		Misc.test = string;
		//END TEST
	}
}
