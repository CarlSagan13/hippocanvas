package sandbox.misc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with regex",
 		parameterDescriptions = {
 				"9", "regex",
 				SandboxParameters.FOCUSED_NAME, "text",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test9 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String regex = getString("regex");
		 String text = getString("text");

//		 Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
//		 Matcher matcher = pattern.matcher(text);
//		 matcher.region(0, text.length());
//		 if (matcher.find()) {
//			 if (matcher.groupCount() > 0) {
//				 for (int groupIndex = 0; groupIndex < matcher.groupCount() + 1; groupIndex++) {
//					 printLine( groupIndex + "-[" + matcher.group(groupIndex) + "]");
//				 }
//			 }
//		 }
		 
		 
		 String[] returns = Utilities.grabRegex(regex, new StringBuffer(text));
		 if (returns != null) {
			 printLine(returns.length);
			 printLine(returns);
		 } else {
			 printLine("Not found");
		 }
		 printALine();
		 printLine(Utilities.groupMatch(regex, new StringBuffer(text)));
		 //END TEST
	 }

}
