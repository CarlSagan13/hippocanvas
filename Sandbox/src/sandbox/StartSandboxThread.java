package sandbox;

public class StartSandboxThread extends StartThread {
	private String sandboxPackage = "sandbox";

	public StartSandboxThread(String sandboxHome, String sandboxPackage) {
		super(sandboxHome);
		this.sandboxHome = sandboxHome;
		this.sandboxPackage = sandboxPackage;
	}

	public void run() {
		try {
			runInSeparateJavaProcess(Sandbox.class.getName() + " " + sandboxPackage);
		} catch (Exception en) {
			startLogger.error("Could not start", en);
		}
	}

}
