// Tool9.java (erase me later)
package sandbox;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SandboxAnnotation(description = SandboxParameters.UNDER_CONSTRUCTION
		+ "Regex replace in clipboard (Use Tool9 sandbox help for directions)", parameterDescriptions = {
				SandboxParameters.FOCUSED_NAME, "changes", 
				SandboxParameters.YESNO_NAME, "showChanges", 
				}, showInButtonPanel = false, showInTabPanel = false, isTest = false, isTool = true)

public class Tool50 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] changes = getStringArray("changes");
		boolean showChanges = getBoolean("showChanges");

		if (changes.length % 2 != 0) {
			throw new SandboxException("Each change must have a from/to");
		}

		StringBuffer display = new StringBuffer();
		String newline = System.getProperty("line.separator", "\n");
		String tab = "\t ";
		display.append("changes:").append(newline);
		for (String change : changes) {
			display.append(tab).append(change).append(newline);
		}

		Map<String, String> grabSlapMap = new HashMap<String, String>();

		String directoryTimeStamp = Utilities.parseDate(new Date(), SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP);
		String strTargetDir = params.getConfigurationDirectory() + File.separator + "storage"
				+ File.separator + directoryTimeStamp;
		File targetDir = new File(strTargetDir);
		if (!targetDir.exists()) {
			targetDir.mkdirs();
		}

		int changeCount = 0;
		List<String> changeHistory = new ArrayList<String>();
		try {
			grabSlapMap.clear();
			Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
			java.awt.datatransfer.Transferable tText = clip.getContents(null);
			String clipboardText = (String) tText.getTransferData(DataFlavor.stringFlavor);
			StringBuffer fileStringBuffer = new StringBuffer(clipboardText);

			changeHistory.add(changeCount++, fileStringBuffer.toString());

			if (changes.length > 0) {
				for (int index = 0; index < changes.length; index += 2) {

					try {
						String search = changes[index];
						String replace = changes[index + 1];
						if (search.startsWith(regexPrefix)) {

							if (replace.startsWith(grabPrefix)) {
								int groupCount = 0;
								String[] groups = Utilities.grabRegex(search.substring(regexPrefix.length()),
										fileStringBuffer);
								if (groups != null) {
									while (groupCount < groups.length) {
										grabSlapMap.put(replace.substring(grabPrefix.length()) + "-" + groupCount,
												groups[groupCount++]);
									}
								}
								search = null;
							} else if (replace.startsWith(replacePrefix)) {
								String[] groups = Utilities.grabRegex(search.substring(regexPrefix.length()),
										fileStringBuffer);

								if (groups == null || groups.length == 0) {
									throw new SandboxToolException(search + "\nDid not	find");
								}
								search = groups[0];
								String[] groupReplaces = replace.substring(replacePrefix.length()).split(":");
								for (String groupReplace : groupReplaces) {
									String strGroup = groupReplace.split(",")[0];

									String strReplacement = groupReplace.split(",")[1];

									int group = Integer.valueOf(strGroup);
									if (strReplacement.matches("\\d+")) {
										groups[group] = groups[Integer.valueOf(strReplacement)];
									} else {
										groups[group] = URLDecoder.decode(strReplacement, "US-ASC II");
									}
								}
								replace = "";
								for (int groupIndex = 1; groupIndex < groups.length; groupIndex++) {
									replace += groups[groupIndex];
								}
								replace = URLEncoder.encode(replace, "US-ASCII");

							} else {
								// replace search with capture from
								// fileStringBuffer
								String[] searches = Utilities.grabRegex(search.substring(regexPrefix.length()),
										fileStringBuffer);
								if (searches != null) {
									search = searches[0];
								} else {
									search = null;
								}
							}
						} else {
							search = URLDecoder.decode(search, "US-ASCII");
						}

						if (replace.startsWith(required)) {
							if (search == null) {
								throw new SandboxException("Aborting because " + search + "does NOT get result");
							}
							search = null;// Do not abort and don't replace
											// anything
						}
						if (search != null) {
							if (replace.equals(abort)) {
								throw new SandboxException("Aborting because " + search + "gets result");
							}
							if (replace.startsWith(grabPrefix)) {
								grabSlapMap.put(replace.substring(grabPrefix.length()), search);
							} else {
								if (replace.startsWith(slapPrefix)) {

									replace = grabSlapMap.get(replace.substring(slapPrefix.length()));
								} else if (replace.startsWith(regexPrefix)) {

									replace = Utilities.grabRegex(search.substring(regexPrefix.length()),
											fileStringBuffer)[0];
								} else {
									replace = URLDecoder.decode(replace, "US-ASCII");
								}

								if (replace != null) {
									if (replace.indexOf(search) >= 0) {// Automate infinite regression workaround
										StringBuffer replaceBuffer = new StringBuffer(replace);
										String encodedSearch = Utilities.base64Encode(search);
										while (replaceBuffer.indexOf(search) > 0) { // mask search with encoded search
											int searchPositionIndex = replaceBuffer.indexOf(search);
											replaceBuffer.replace(searchPositionIndex,
													searchPositionIndex + search.length(), encodedSearch);
										}

										replace = replaceBuffer.toString();// safe to search/replace because search string has been masked
										while (fileStringBuffer.indexOf(search) > 0) {
											int positionIndex = fileStringBuffer.indexOf(search);
											fileStringBuffer.replace(positionIndex, positionIndex + search.length(),
													replace);
										}
										while (fileStringBuffer.indexOf(encodedSearch) > 0) {// now return unencoded search string
											int positionIndex = fileStringBuffer.indexOf(encodedSearch);

											fileStringBuffer.replace(positionIndex,
													positionIndex + encodedSearch.length(), search);
										}
										changeHistory.add(changeCount++, fileStringBuffer.toString());
									} else
										while (fileStringBuffer.indexOf(search) >= 0) {
											int positionIndex = fileStringBuffer.indexOf(search);
											fileStringBuffer.replace(positionIndex, positionIndex + search.length(),
													replace);
											changeHistory.add(changeCount++, fileStringBuffer.toString());
										}
								}
							}
						}
					} catch (SandboxToolException ste) {
						printLine(ste);
					}

				}
			}
			if (showChanges) {
				this.newTab(fileStringBuffer.toString(), "Changed");
			} else {
				printLine(fileStringBuffer.toString());
			}
		} catch (Exception en) {
			printLine("Did not process :: " + en.getMessage());
		}

		if (changeHistory.size() > 1) {
			this.addToGlobalMap(this.getClass().getSimpleName() + this.getTime("HHmmss"), changeHistory);
		}
		// END TEST
	}

	class SandboxToolException extends sandbox.SandboxException { // TODO Use SandboxException instead
		/**
		*
		*/
		private static final long serialVersionUID = 1L;

		public SandboxToolException(String message) {
			super(message);
		}
	}

	public static final String abort = "abort:";
	public static final String required = "required:";
	public static final String regexPrefix = "regex:";
	public static final String groups = "groups:";
	public static final String grabPrefix = "grab:";
	public static final String slapPrefix = "slap:";
	public static final String replacePrefix = "replace:";

}