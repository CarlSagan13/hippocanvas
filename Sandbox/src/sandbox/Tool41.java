package sandbox;

@SandboxAnnotation(
		description = "Put serialized object from file into global map",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME, "mapKey,serializedFilePath", 
			SandboxParameters.TRUEFALSE_NAME, "wrap" 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool41 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String mapKey = getString("mapKey");
		String serializedFilePath = getString("serializedFilePath");
		boolean wrap = getBoolean("wrap");
		if (wrap) {
			SandboxSerializable ser = (SandboxSerializable) sandbox.Utilities.getFileObject(serializedFilePath);
			this.addToGlobalMap(mapKey, ser.getObj());
		} else {
			this.addToGlobalMap(mapKey, sandbox.Utilities.getFileBytes(serializedFilePath));
		}
		// END TEST
	}

}
