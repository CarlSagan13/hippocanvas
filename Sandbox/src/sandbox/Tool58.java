package sandbox;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Group replace in clipboard",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "groupSearchReplace",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool58 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] saSearchReplace = getStringArray("groupSearchReplace");

		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		java.awt.datatransfer.Transferable tText = clip.getContents(null);
		String clipboardText = (String) tText.getTransferData(DataFlavor.stringFlavor);
		
		String regex = saSearchReplace[0].split("\n")[0];
		String preSplitString = params.replaceValues(clipboardText);

		printLine();

		String[] replace = saSearchReplace[1].split(",");
		StringBuffer passedText = new StringBuffer(preSplitString);
		StringBuffer newText = new StringBuffer();

		if (regex.matches(".*\\(.*\\).*")) {
			Utilities.grabReplaceRegex(regex, replace, passedText, newText);
			StringSelection selection = new StringSelection(newText.toString());
			clip.setContents(selection, null);
		} else { //regex not grouped so advise
			params.printLine("Use grouped regex");
		}
		// END TEST
	}

}
