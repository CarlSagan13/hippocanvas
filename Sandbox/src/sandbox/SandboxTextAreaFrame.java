//SandboxTextAreaFrame.java (erase me later)
package sandbox;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class SandboxTextAreaFrame extends SandboxFrame {
	private static final long	serialVersionUID	= 9216982226346813049L;
	private String BUTTON_NAME_OK = "OK";
	private String BUTTON_NAME_SAVE = "Save";
	private String BUTTON_NAME_CANCEL = "Cancel";
	private String BUTTON_NAME_PHONE = "Phone";

	JTextArea textArea = null;
	String parameterName = null;
	String popPadFilePath = null;
	SandboxTestBase test = null;

	public SandboxTextAreaFrame() {
		//Just so I can make a object without showing it
	}
	public SandboxTextAreaFrame(SandboxTestBase test, String parameterName, String popPadFilePath, int maxLinesVisible){
		this.setTitle(parameterName);
		this.setIconImage(SandboxParameters.sandboxImage);
		this.test = test;

		JPanel panel = new JPanel(new BorderLayout ());
		this.parameterName = parameterName;
		this.popPadFilePath = popPadFilePath;
		test.printLine("popPadFilePath = "+ popPadFilePath);		
		String popPadText = null;
		
		try { 
			popPadText = Utilities.getFileString(popPadFilePath);
			if (popPadText.indexOf(SandboxParameters.POPPAD_NOTES)< 0){
				popPadText += SandboxParameters.POPPAD_NOTES;
			}
		}catch (Exception en){
			test.printLine("Failed loading poppad: "+ popPadFilePath);
			popPadText = "#no text loaded\n\n"+ SandboxParameters.POPPAD_NOTES;
		}
		
		int lineCount = popPadText.split("\n").length + 2;
		if (lineCount > maxLinesVisible ){
			lineCount = maxLinesVisible;
		}
		textArea = new JTextArea (popPadText, lineCount, 80);
		test.addConnection(Utilities.addUndo(textArea));
		
		String[] clf = Utilities.getEnvFont();
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		} catch(Throwable th) {
			System.err.println(th);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		textArea.setFont(textFont);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		panel.add(scrollPane,BorderLayout.SOUTH);
		
		JPanel buttonPanel = new JPanel();
		JButton btnOk = new JButton(BUTTON_NAME_OK);
		btnOk.addActionListener(new ButtonListener(this, test));
		buttonPanel.add(btnOk);
		JButton btnSave = new JButton(BUTTON_NAME_SAVE);
		btnSave.addActionListener(new ButtonListener(this, test ));
		buttonPanel.add(btnSave);
		JButton btnCancel = new JButton(BUTTON_NAME_CANCEL);
		btnCancel.addActionListener(new ButtonListener(this, test));
		buttonPanel.add(btnCancel);
		JButton btnPhone = new JButton(BUTTON_NAME_PHONE);
		btnPhone.addActionListener(new ButtonListener(this, test));
		buttonPanel.add(btnPhone);
		Utilities.setBorderColor(this);

		getContentPane().add(scrollPane,BorderLayout.CENTER);
		getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		setDefaultCloseOperation(SandboxFrameBase.DISPOSE_ON_CLOSE);
		pack();
		setAlwaysOnTop(true);
//		setAlwaysOnTop(Boolean.valueOf(System.getProperty(SandboxFrameBase.ALWAYS_ON_TOP_VAR_NAME, "false")));
		setLocationRelativeTo(test.getParameters().getTabbedPane());
		
		textArea.addMouseListener(new SandboxMouseListener());
		textArea.addKeyListener(new SandboxKeyListener(test.parameters));
		setVisible (true);
		
//		try {
//			while(isVisible()){
//				Thread.sleep(100);
//			}
//		} catch(InterruptedException ie){
//			test.printLine("Wait for input was interrupted.");
//		} finally {
//			test.resetStartTime();
//		}
	}
	
	/**
	 * Make this temporarily always on top, so it pokes above the ground
	 */
//	protected void phoneIn() {
//		this.setState(Frame.NORMAL);
//		this.setAlwaysOnTop(true);
//		try {
//			Thread.sleep(100);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			this.setAlwaysOnTop(false);
//		}
//	}

	public class ButtonListener implements ActionListener {
		private SandboxTestBase test;
		private SandboxTextAreaFrame frame;
		
		public ButtonListener(SandboxTextAreaFrame frame, SandboxTestBase test){
			this .frame = frame;
			this.test = test;
		}
		
		public void actionPerformed(ActionEvent actionEvent){
			if (test != null){
				try {
					JButton button = (JButton)actionEvent.getSource();
					if (button.getText().equals(frame.BUTTON_NAME_CANCEL)){
						test.isRunning = false;
						throw new SandboxException("User cancelled");
					}

					if (button.getText().equals(frame.BUTTON_NAME_SAVE)){
						test.isRunning = false;
					}
					
					if (actionEvent.getActionCommand().equals(BUTTON_NAME_PHONE)) {
						SandboxFrameBase.phoneCall("Oh, there you are!", "Where are you?");
						return;
					}

					test.getPopPadValues().put(this.frame.parameterName,frame.textArea.getText());
					Utilities.saveFileString (frame.popPadFilePath,frame.textArea.getText());
				} catch (Exception en){
					test.printLine(this.getClass().getName()+ " :: "+ en.getClass().getName()
							+ ": message = "+ en .getMessage(),en);
				}
			}
			frame.setVisible(false);
			frame.dispose ();
		}
	}

	protected SandboxTestBase getTest() {
		return test;
	}
}