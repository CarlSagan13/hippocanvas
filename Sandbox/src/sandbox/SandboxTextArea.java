//SandboxTextArea.java (erase me later)
package sandbox;
import java.awt.Font;

import javax.swing.JTextArea;
public class SandboxTextArea extends JTextArea {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private SandboxTestBase test = null;
	private SandboxThread sandboxThread = null;

	public SandboxTextArea (){
		super();
		setTextFont();
	}
	public SandboxTextArea (SandboxTestBase test){
		super();
		this.test = test;
		setTextFont();
	}
	public SandboxTextArea (SandboxThread sandboxThread){
		super();
		this.sandboxThread = sandboxThread;
		setTextFont();
	}

	public SandboxTestBase getTest(){
		return test;
	}
	public void setTest(SandboxTestBase test){
		this.test = test;
	}
	public SandboxThread getSandboxThread(){
		return sandboxThread;
	}
	public void setSandboxThread(SandboxThread sandboxThread){
		this.sandboxThread = sandboxThread;
	}
	
	private void setTextFont() {
		String[] clf = System.getProperty("sbx.conf.textFont","Dialog_2_11").split("_");
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		setFont(textFont);
	}
}