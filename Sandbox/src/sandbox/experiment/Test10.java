package sandbox.experiment;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Dynamically load test class",
 		parameterDescriptions = {
 				"14", "name",
 				"15", "path",
// 				SandboxParameters.FOCUSED_NAME, "name,path",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test10 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String name = getString("name");
		 String path = getString("path");
		 
		    ClassLoader testClassLoader = TestClassLoader.class.getClassLoader();
		    TestClassLoader classLoader = new TestClassLoader(testClassLoader);
		    Class testObjectClass = classLoader.loadClass(name, path);
	    
		    SandboxTestBase object2 =
		            (SandboxTestBase) testObjectClass.newInstance();
		    
		 //END TEST
	 }

}
