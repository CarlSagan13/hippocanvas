package sandbox.experiment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class TestClassLoader extends ClassLoader {

	public TestClassLoader() {
		// TODO Auto-generated constructor stub
	}

	public TestClassLoader(ClassLoader parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}
	
	public Class loadClass(String name, String path) throws ClassNotFoundException {
		
		
	       if(!name.startsWith("sandbox."))
               return super.loadClass(name);
		
		
//	       if(!"sandbox.experiment.ChildClass".equals(name))
//               return super.loadClass(name);

       try {
           String url = "file:" + path;
//           String url = "file:C:/Users/carls/GIT/workspace/Experiment/bin/sandbox/experiment/ChildClass.class" + path;
           URL myUrl = new URL(url);
           URLConnection connection = myUrl.openConnection();
           InputStream input = connection.getInputStream();
           ByteArrayOutputStream buffer = new ByteArrayOutputStream();
           int data = input.read();

           while(data != -1){
               buffer.write(data);
               data = input.read();
           }

           input.close();

           byte[] classData = buffer.toByteArray();

           return defineClass(name, classData, 0, classData.length);

       } catch (MalformedURLException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }

       return null;

	}

}
