package sandbox.experiment;

public class MyStaticClass {
	public static int myNumber = 9;
	public int myInstanceNumber = -7;
	public static void printANumber() {
		System.out.println("My number is: " + myNumber);
	}
	public void printAnInstanceNumber() {
		System.out.println("My number is: " + myInstanceNumber);
	}
}