package sandbox.experiment;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Dynamically load class",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "name,path",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test9 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
//		 String string = getString("string");
		 
		 String name = getString("name");
		 String path = getString("path");
		 
		    ClassLoader parentClassLoader = MyClassLoader.class.getClassLoader();
		    MyClassLoader classLoader = new MyClassLoader(parentClassLoader);
//		    Class myObjectClass = classLoader.loadClass("sandbox.experiment.ChildClass", "/home/dmharte/MYGIT/hippocanvas/Sandbox/bin/sandbox/experiment/ChildClass.class");
		    Class myObjectClass = classLoader.loadClass(name, path);
	    
		    ParentClass object2 =
		            (ParentClass) myObjectClass.newInstance();
		    
		    printLine(object2.getValue());

//		    printLine(object2.getValue());
		    
//		    //create new class loader so classes can be reloaded.
//		    classLoader = new MyClassLoader(parentClassLoader);
//		    myObjectClass = classLoader.loadClass("sandbox.experiment.ChildClass");
//
//		    object2 = (ChildClass) myObjectClass.newInstance();

//		    ChildClass child = (ChildClass) object2.;
		    
//		    printLine(((ChildClass) object2).getValue());
		    
		 
		 //END TEST
	 }

}
