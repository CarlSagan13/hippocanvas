package sandbox.experiment;

import java.io.IOException;
import java.io.InputStream;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Update experiment.Test2 class",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test2 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 
         Class cls = new TestClassLoader().loadClass("sandbox.experiment.Test1");
         Object obj = cls.newInstance();
//         cls.getMethod("hello").invoke(obj);
//         Thread.sleep(5000);

		 //END TEST
	 }
	 
	    static class TestClassLoader extends ClassLoader {
	        @Override
	        public Class<?> loadClass(String name) throws ClassNotFoundException {
	            if (name.equals("sandbox.experiment.Test1")) {
	                try {
	                    InputStream is = Test1.class.getClassLoader().getResourceAsStream("sandbox/experiment/Test1.class");
	                    byte[] buf = new byte[10000];
	                    int len = is.read(buf);
	                    return defineClass(name, buf, 0, len);
	                } catch (IOException e) {
	                    throw new ClassNotFoundException("", e);
	                }
	            }
	            return getParent().loadClass(name);
	        }
	    }


}
