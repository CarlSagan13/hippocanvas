package sandbox.experiment;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display string, focused string, scratch pad string, password, and boolean",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test7 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
//		 String string = getString("string");
		    ClassLoader parentClassLoader = MyClassLoader.class.getClassLoader();
		    MyClassLoader classLoader = new MyClassLoader(parentClassLoader);
		    Class myObjectClass = classLoader.loadClass("sandbox.experiment.ChildClass");

		    ExperimentIF object1 =
		            (ExperimentIF) myObjectClass.newInstance();

		    //create new class loader so classes can be reloaded.
		    classLoader = new MyClassLoader(parentClassLoader);
		    myObjectClass = classLoader.loadClass("sandbox.experiment.ChildClass");

		    object1 = (ExperimentIF)       myObjectClass.newInstance();
		    
		    printLine(object1.getClass().getName());
		    
		 //END TEST
	 }

}
