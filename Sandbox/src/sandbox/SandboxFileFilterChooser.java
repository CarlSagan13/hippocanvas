//SandboxFileFilterChooser.java (erase me later)
package sandbox; 
 
 import java. io. File; 
 import javax. swing. filechooser. FileFilter; 
 
 public class SandboxFileFilterChooser extends FileFilter {
 	 
 	 private String endsWith = null; 
 	 private String contains = null; 
 	 
 	 public SandboxFileFilterChooser(String endsWith,String contains){
 	 	 this. endsWith = endsWith; 
 	 	 this. contains = contains; 
 	 }
 	 public String getDescription(){
 	 	 return "Configuration files"; 
 	 }
 
 	 public boolean accept(File file){
 	 	 String filename = file. getName(); 
 	 	 if (file. isDirectory()){
 	 	 	 return true; 
 	 	 }
 	 	 if (endsWith != null &&contains != null){
 	 	 	 if (filename. contains(contains)&&
 	 	 	 	 	 filename. endsWith(endsWith)){
 	 	 	 	 return true; 
 	 	 	 }else {
 	 	 	 	 return false; 
 	 	 	 }
 	 	 }
 	 	 if (endsWith != null &&contains == null){
 	 	 	 if (filename. substring (filename. length()- 4). equals(endsWith)){
 	 	 	 	 return true; 
 	 	 	 }else {
 	 	 	 	 return false; 
 	 	 	 }
 	 	 }
 	 	 if (endsWith == null &&contains != null){
 	 	 	 if (filename. contains (contains)){
 	 	 	 	 return true ; 
 	 	 	 }else {
 	 	 	 	 return false; 
 	 	 	 }
 	 	 }
 	 	 return false; 
 	 }
 }
