// Tool40.java (erase me later)
package sandbox;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
	description = "Serialize global object to file",
	parameterDescriptions = 
	{
		SandboxParameters.FOCUSED_NAME, "mapKey,serializedFilePath",
		SandboxParameters.TRUEFALSE_NAME, "wrap"
	}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool40 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TEST
		String mapKey = getString("mapKey");
		String serializedFilePath = getString("serializedFilePath");
		boolean wrap = getBoolean("wrap");
		
		if (wrap){
			SandboxSerializable ser = new SandboxSerializable(getGlobalMapObject(mapKey));
			sandbox.Utilities.saveFileObject(serializedFilePath,ser);
		}else {
			byte[] baObject = (byte[]) getGlobalMapObject(mapKey);
			sandbox.Utilities.saveFileString(serializedFilePath ,baObject);
		}
		//END TEST
	}
}