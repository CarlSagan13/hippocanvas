package sandbox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface SandboxLogging {
	public static final Logger sandboxLogger = LogManager.getLogger("Sandbox");
	public static final Logger startLogger = LogManager.getLogger("SandboxStart");
}
