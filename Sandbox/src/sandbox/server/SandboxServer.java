package sandbox.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SandboxServer {
	
	static boolean serverIsRunning = true;
	static String serverFilePath = "/tmp";

	public static void main(String[] args) {
		
		
		
		System.out.println(System.getProperty("java.class.path", "unknown"));
		
		if (args.length != 3) {
			System.err.println("Three arguments required");
			System.err.println("1 portIndex");
			System.err.println("2 portLimit");
			System.err.println("3 serverFilePath");
			System.exit(1);
		}
		serverFilePath = args[2];
		String headersFilePath = serverFilePath
				+ File.separator + "headers";
		printLine("Headers are in: " + headersFilePath);
		String headers = "";
		try {
			headers = getFileString(headersFilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			printLine("Could not get headers in " + headersFilePath, e);
			System.exit(1);
		}

		// Find open port
		boolean success = false;
		ServerSocket serverSocket = null;

		int portIndex = Integer.parseInt(args[0]);
		int portLimit = Integer.parseInt(args[1]);
		while (!success) {
			try {
				serverSocket = new ServerSocket(portIndex);
				printLine("Listening on port " + portIndex);
				success = true;
			} catch (IOException ioe) {
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			}
		}

		try {
			while (serverIsRunning) {
				Socket socket = serverSocket.accept();
				printLine("accepting message.");
				printLine(socket.getInetAddress().getHostAddress());
				printLine(SandboxServer.VISUAL_SEPARATOR);
				
				
				SandboxServer server = new SandboxServer();
				
				ClientThread clientThread = server.new ClientThread(socket, headers);
				
				clientThread.start();
			}
		} catch(IOException ioe) {
			printLine("Exception in socket receipt.", ioe);
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static void printLine(String s) {
		System.out.println(s);
	}
	private static void printLine(String s, Throwable th) {
		System.err.println(s);
		th.printStackTrace();
	}
	
	public static String getFileString(File file, long length) throws IOException {
		String fileString = null;
		

		FileInputStream fileInputStream = new FileInputStream(file);
		byte[] ba = new byte[Integer.parseInt(Long.toString(length))];
		fileInputStream.read(ba, 0, (int) length);
		try {
			fileString = new String(ba);
		} catch (OutOfMemoryError oome) {
			IOException ioException = new IOException("File too large.");
			throw ioException;
		} finally {
			fileInputStream.close();
		}
		return fileString;
	}

	public static String getFileString(String filename) throws FileNotFoundException, IOException {
		File file = new File(filename);
		return getFileString(file, file.length());
	}
	
	public static void saveFileObject(String filePath, Serializable ser) throws FileNotFoundException, IOException {
		FileOutputStream underlyingStream = new FileOutputStream(filePath);
		ObjectOutputStream serializer = new ObjectOutputStream(underlyingStream);
		serializer.writeObject(ser);
		serializer.close();
	}
		
	class ClientThread extends Thread {
		Socket client;
		String headers;
		boolean isRunning;

		ClientThread(Socket client, String headers) {
			this.client = client;
			this.headers = headers;
		}

		@Override
		public void run() {
			try {
				// Get streams to talk to the client
				InputStream inputStream = client.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				OutputStream out = client.getOutputStream();
				PrintWriter pout = new PrintWriter(out);

				List<String> contentParameters = showHeaders(bufferedReader);

				if (contentParameters.contains("Sandbox-Shutdown: yes")) {
					System.err.println(this.getClass().getSimpleName() + " is shutting down");
					serverIsRunning = false;
				}
				
				String filePath = "/";
				if (contentParameters.size() > 0) {
					filePath = contentParameters.remove(0);
					int contentInputLength = 0;

					Map<String, List<String>> map = getHeaderMap(contentParameters);
					List<String> contentLengths = map.get("Content-Length");
					if (contentLengths != null && contentLengths.size() > 0) {
						contentInputLength = Integer.valueOf(contentLengths.get(0));
					}
					List<String> contentTypes = map.get("Content-Type");
					if (contentTypes != null && contentTypes.size() > 0) {
						if (contentTypes.contains("application/x-java-serialized-object")) {
							printLine("DANGER ZONE: Might seize up here because the content has to be a Java serialized object");
							ObjectInputStream ois = new ObjectInputStream(inputStream);
							try {
								Object objContent = ois.readObject();
								String mapId = "content" + new Date().getTime();
								printLine("Putting object in global map with ID: " + mapId);
								contentInputLength = 0;
							} catch (ClassNotFoundException e) {
								printLine("Problem with getting serialized object", e);
							}
						}
					}

					if (contentInputLength > 0) {
						byte[] contentInput = new byte[contentInputLength];
						inputStream.read(contentInput, 0, contentInputLength);
						String contentName = SandboxServer.class.getSimpleName() + "."
								+ (new SimpleDateFormat(SandboxServer.DATE_FORMAT_FILE_TIMESTAMP))
										.format(new Date());
						String contentFilePath = serverFilePath + File.separator + contentName;
						printLine("Saving received content to global map with ID " + contentName);
						printLine("Saving received content to " + contentFilePath);
						saveFileObject(contentFilePath, contentInput);
					}

					printLine("Input content length = " + contentInputLength);
					String[] filePathParts = filePath.split(" ", 4);

					filePath = URLDecoder.decode(filePathParts[1], "US-ASCII"); // TODO Suspicious OCR artifacts

				}
				if (filePath.trim().length() == 0) {
					printLine("No file path specified");
				} else {
					printLine("filePath is " + filePath);
				}
				String contentOutput = null;
				if (filePath.endsWith("shutdown")) {
					contentOutput = "Shutting down";
				} else {
					try {
						contentOutput = sandbox.Utilities.getResourceString(filePath);
					} catch (FileNotFoundException fnfe) {
						contentOutput = "File not found";
					}
				}

				headers = headers.concat("\n" + "Content-Length: " + contentOutput.length());
				try {
					out.write(headers.trim().getBytes());
					out.write("\n\n".getBytes());
					pout.println(contentOutput);
				} catch (Exception en) {
					printLine(this.getClass().getName() + ": " + en.getClass().getSimpleName());
					for (StackTraceElement element : en.getStackTrace()) {
						printLine(element.toString());
					}
				} finally {
					pout.close();
					bufferedReader.close();
					client.close();
				}

				pout.close();
				bufferedReader.close();
				client.close();

			} catch (IOException ioe) {
				printLine(ioe.getClass().getName() + ": " + ioe.getMessage());
				for (StackTraceElement element : ioe.getStackTrace()) {
					printLine(element.toString());
				}
			}
		}
	}

	List<String> showHeaders(BufferedReader bufferedReader) throws IOException {
		String line = null;
		List<String> contentParameters = new ArrayList<String>();
		printLine("******************HEADERS START");
		while ((line = bufferedReader.readLine()) != null) {
			if (line.length() == 0)
				break;
			printLine("HEADER: " + line);
			contentParameters.add(line);
		}
		printLine("******************HEADERS DONE ");
		return contentParameters;
	}

	Map<String, List<String>> getHeaderMap(List<String> listHeaders) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String header : listHeaders) {
			String[] nameValue = header.split(": ", 2);
			String name = nameValue[0];
			String value = nameValue[1];
			List<String> list = map.get(name);
			if (list == null) {// header is new
				list = new ArrayList<String>();
			}
			list.add(value);
			map.put(name, list);
		}
		return map;
	}
	public static final String DATE_FORMAT_FILE_TIMESTAMP = "ddMMMyy_HH.mm.ss";
	public static final String VISUAL_SEPARATOR = new String(new char[32]).replace("\0", "_");

}
