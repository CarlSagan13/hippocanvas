package sandbox;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JScrollPane;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Filter lines in open tab using selected regex", parameterDescriptions = {
		SandboxParameters.FOCUSED_NAME, "regex",
		SandboxParameters.TRUEFALSE_NAME, "noDupes", 
		SandboxParameters.YESNO_NAME, "filterOut", 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Tool54 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TOOL
		String regex = getString("regex");
		boolean noDupes = getBoolean("noDupes");
		boolean filterOut = getBoolean("filterOut");

		String tabText = params.getTabComponentText(params.preTestSelectedTab);

		StringBuffer newText = new StringBuffer();
		StringBuffer rejected = new StringBuffer();
		String[] lines = tabText.split("\n");
		
		String[] headers = lines[0].split(",");
		if (params.getTabComponent(params.preTestSelectedTab) instanceof SandboxTable) {
			headers = lines[0].split(",");
			tabText = tabText.substring(tabText.indexOf('\n') + 1);

			lines = tabText.split("\n");

		}
		if (filterOut) {
			for (String line : lines) {
				if (!line.matches(regex)) {
					newText.append(line).append('\n');
				} else {
					rejected.append(line).append('\n');
				}
			}
		} else {
			for (String line : lines) {
				if (line.matches(regex)) {
					newText.append(line).append('\n');
				} else {
					rejected.append(line).append('\n');
				}
			}
		}

		if (params.getTabComponent(params.preTestSelectedTab) instanceof SandboxTable) {
			if (newText.length() == 0) {
				printLine("No Rows Match ");
				return;
			}
			String[] newLines = newText.toString().split("\\n");
			String[][] data = new String[newLines.length][headers.length];
			int index = 0;
			for (String row : newLines) {
				// Had to put this in because giving an empty character to the JTable messed it up
				row = row.replaceAll("\\,\\,", ",,").replaceAll("\\,$", ","); 
				data[index++] = row.split(",");
			}
			SandboxTable table = Utilities.tabulate(params, data, headers, "FILTERED", false);
			JScrollPane scrollPaneTable = new JScrollPane(table);
			String inOut = filterOut ? "out" : "in";
			params.newTab("Table filtered " + inOut, scrollPaneTable, inOut);
		} else {
			
			if (noDupes) {
				Set<String> setLines = new LinkedHashSet<String> ();
				for (String line : newText.toString().split("\\n")) {
					setLines.add(line);
				}
				newText = new StringBuffer();
				for (String line : setLines) {
					newText.append(line).append("\n");
				}
			}
			
			printLine(newText);
		}

		if (rejected.length() > 0) {
			Date date = new Date();
			this.addToGlobalMap(this.getTestName() + "vRejectedv" + date.getTime(), rejected);
		}
		// END TOOL
	}

}
