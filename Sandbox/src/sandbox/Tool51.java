// Tool6.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Find files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "search,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
			SandboxParameters.YESNO_NAME, "unlock",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool51 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String search = getString("search");
		String directory = getString("directory");
		boolean recurse = getBoolean("recurse");
		boolean unlock = getBoolean("unlock");

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(Pattern.quote(search), Pattern.MULTILINE);
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				printLine(file.getAbsolutePath());
				if (!file.canWrite()){
					printLine("\tLOCKED!");
					if (unlock) {
						file.setWritable(true);
					}
				}
				checkRunning();
			}
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
}
