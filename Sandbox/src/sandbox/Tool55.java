// Tool30.java (erase me later)
package sandbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Set files to modification date",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"fileRegex,modDateTime,fileDir",
			SandboxParameters.TRUEFALSE_NAME,"modify",
			SandboxParameters.YESNO_NAME, "recurse"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool55 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String fileRegex = getString("fileRegex");
		File fileDir = getFileDir("fileDir");
		Date modDateTime = this.getDate("modDateTime");
		boolean modify = getBoolean("modify");
		boolean recurse = getBoolean("recurse");

		//Check that duplicate is older than active
		if (!fileDir.exists()){
			throw new SandboxException("Directory/File doesn't exist");
		}

		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
		Collection<File> activeFiles = Utilities.listFilesRegex(fileDir, recurse, true, pattern, this);
		for (File file : activeFiles){
			if (file.isFile()){
				printLine(file.getPath());
				if (modify) {
					file.setLastModified(modDateTime.getTime());
				}
			}	
		}
		//END TEST
	}	
}