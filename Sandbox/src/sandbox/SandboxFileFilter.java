//SandboxFileFilter.java (erase me later)
package sandbox;
import java. io. File;
import java. io. FileFilter;
public class SandboxFileFilter implements FileFilter {
	 private int largerThan = 0;
	
	 public SandboxFileFilter(int largerThan){
	 	 this. largerThan = largerThan;
	 }
	 @Override
	 public boolean accept(File pathname){
	 	 //TODO Auto-generated method stub
	 	 if (pathname. isDirectory()|| pathname. length()> largerThan){
	 	 	 return true;
	 	 }
	 	 return false;
	 }
}
