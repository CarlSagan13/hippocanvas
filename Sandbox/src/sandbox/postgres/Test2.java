package sandbox.postgres;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Close database connection",
 		parameterDescriptions = {
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test2 extends SandboxTestBase {
	
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		((java.sql.Connection) getGlobalMapThing(java.sql.Connection.class)).close();
		//END TEST
	}
}
