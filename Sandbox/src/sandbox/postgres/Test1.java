package sandbox.postgres;

import java.util.Properties;

import com.mongodb.connection.Connection;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Connect to database",
 		parameterDescriptions = {
 				"1", "connectionString",
 				"3", "databaseDriverClass",
 				"13","userid",
 				SandboxParameters.POPPAD_NAME, "connectionProperties",
 				SandboxParameters.PASSWORD_NAME, "password",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test1 extends SandboxTestBase {
	/**
	*Instantiate for dynamic loading
	*/
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST 
		printLine("Hello from " + this.getClass ().getSimpleName());
		String connectionString = getString("connectionString");
		String databaseDriverClass = getString("databaseDriverClass");
		String userid = getString("userid");
		String password = getString("password");
		
		String[] connectionProperties = getStringArray("connectionProperties");
		
		Properties props = new Properties();
		props.put("user", userid);
		props.put("password", password);
//		props.put("ssl","true");

		for (String nameValue : connectionProperties) {
			props.put(nameValue.split("=")[0], nameValue.split("=")[1]);
		}
		
		Class.forName(databaseDriverClass);
		this.replaceInGlobalMap(java.sql.Connection.class.getName(), java.sql.DriverManager.getConnection(connectionString, props));
		//END TEST
	}
}
