//SandboxLogger.java (erase me later)
package sandbox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.IOException;
import java.util .ArrayList;
import java.util.List;

public class SandboxLogger extends SandboxThread {
	
	private int frequencyInMilliseconds = 100;
	private String filePath = null;
	
	public void run(){
		File tailedFile = new File(filePath);
		long lastModified = tailedFile.lastModified();
		long lastSize = tailedFile.length();
		try {
			//find parameters of file to show initial tailing
			FileReader fileReader = null;
			fileReader = new FileReader(tailedFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader );

			List<String> lines = new ArrayList<String>();
			
			String string = "";
			int lastLineCount = 0;
			while((string = bufferedReader.readLine())!= null){
				lines.add(string);
				if (lines.size()> windowSize){
					lines.remove(0);
				}
				lastLineCount++;
			}

			fileReader.close();

			for (String line : lines){
				printLine(line);
			}
			lines.clear();
			
			while (this.isRunning){
				Thread.sleep(frequencyInMilliseconds);
				if (lastModified != tailedFile.lastModified()
						|| lastSize < tailedFile.length()){
					
					tailedFile = new File(filePath);
					fileReader = new FileReader(tailedFile);
					bufferedReader = new BufferedReader(fileReader);
			
					String line = "";
					int index = 0 ;
					
					while((line = bufferedReader.readLine())!= null){
						index++;
						if (index > lastLineCount){
							printLine(line);
						}
					}
					fileReader.close();
					if (index < lastLineCount){
						printLine("File shortened");
						isRunning = false;
					}
					lastLineCount = index;
					lastModified = tailedFile.lastModified();
					lastSize = tailedFile.length();
				}
			}
			
		}catch (FileNotFoundException fnfe){
			printLine("Could not open file: "+ fnfe.getMessage());
		}catch(IOException ioe ){
			printLine("Could not read file: "+ ioe.getMessage());
		}catch(InterruptedException ie){
			printLine("Thread interrupted unexpectedly: " + ie.getMessage());
		}
		
	}
	
	/**
	*@return the filePath
	*/
	public String getFilePath(){
		return filePath;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath ;
	}

	/**
	*@return the frequencyInMilliseconds
	*/
	public int getFrequencyInMilliseconds(){
		return frequencyInMilliseconds;
	}

	public void setFrequencyInMilliseconds(int frequencyInMilliseconds){
		this.frequencyInMilliseconds = frequencyInMilliseconds;
	}

}
