// Tool39.java (erase me later)
package sandbox;

import org.apache.commons.lang.StringEscapeUtils;


import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Unicode code/decode",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"string",
			SandboxParameters.TRUEFALSE_NAME,"decode",
			SandboxParameters.YESNO_NAME,"replaceInFocused"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool39 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		String selectedText = getString("selectedText");
		boolean decode = getBoolean("decode");
		boolean replace = getBoolean("replaceInFocused");

		if (!replace){
			selectedText = params.getFocusedText();
		}
		
		String tab = "\t";
		if (decode){
			printLine("HTML: "+ tab + StringEscapeUtils.unescapeHtml(selectedText));
			printLine("Java: "+ tab + StringEscapeUtils.unescapeJava(selectedText));
			printLine("XML: "+ tab + StringEscapeUtils.unescapeXml (selectedText));
			printLine("JS: "+ tab + StringEscapeUtils.unescapeJavaScript(selectedText));

		}else {

			printLine("HTML: "+ tab + StringEscapeUtils.escapeHtml(selectedText));
			printLine("Java: "+ tab + StringEscapeUtils.escapeJava(selectedText));
			printLine("XML: "+ tab + StringEscapeUtils.escapeXml(selectedText));
			printLine("JS: "+ tab + StringEscapeUtils.escapeJavaScript(selectedText));
		}
		//END TOOL
	}
}