// Tool46.java (erase me later)
package sandbox;

import java.io.File;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = "Run Java commands serially in a separate process with default workspace",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"commands"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool46 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] commands = getStringArray("commands");
		for (String command : commands){
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("COMMAND: "+ command);
			setTooltip(command);

			long delay = runInSeparateJavaProcess(command, new File(System.getProperty("user.dir",".")), this);
			printLine(SandboxParameters.VISUAL_SEPARATOR + File.separator + "Delay = "+ delay);
		}
		//END TEST
	}
}

