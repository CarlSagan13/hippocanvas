//SandboxMessage.java (erase me later)
package sandbox;

import javax.swing.*;

import javax.swing.border.Border;

import sandbox.SandboxTestTab.ButtonListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SandboxMessage extends SandboxFrame implements SandboxCommon {

//	private SandboxKeyListener messageKeyListener = null;

	private String BUTTON_NAME_OK = "OK";
	private String BUTTON_NAME_NO = "No";
	private String BUTTON_NAME_CANCEL = "Cancel";
	private String BUTTON_NAME_DELETE = "Delete";
	private String BUTTON_NAME_PHONE = "Phone";

	private JTextArea textArea = null;
//	private Component component = null;
	private SandboxParameters params = SandboxParameters.createSandboxParameters();
	private SandboxBasket<String,String> envSet = null;
	private String title = "unknown";
	private String toolTip = "unknown";
	private String mapId = "unknown";
	// private int staticTagNumber = -<N0>;

	private final JButton btnNo = new JButton(BUTTON_NAME_NO);
	private final JButton btnCancel = new JButton(BUTTON_NAME_CANCEL);

	private int status = 0;

	public SandboxMessage() {
	}

	public SandboxMessage(String message, Component component) {
		this("Sandbox Message", message, component, 20, true);
	}

	public SandboxMessage(String title, String message, Component component, int maxLinesVisible, boolean modal) {
		this(title, message, component, maxLinesVisible, 30, true, false, 0, null);
	}

	public SandboxMessage(String title, String message, Component parentComponent, int maxLinesVisible,
			int closeSeconds, boolean modal, boolean editable,  
			int messageCount, SandboxBasket<String,String> envSet) {
		super();
		
		this.title = title;
		this.setTitle(title);
		this.envSet = envSet;
		setBorderColor();
		this.setIconImage(SandboxParameters.sandboxImage);

		sandboxLogger.debug("message = " + message);
		this.setLayout(new BorderLayout());

		int lineCount = message.split("\n").length + 2;
		if (lineCount > maxLinesVisible) {
			lineCount = maxLinesVisible;
		}
		
		int msgWidthDivider = Integer.parseInt((System.getProperty("sbx.conf.msgWidthDivider", "20")));
		
		System.err.println("dmh1455 msgWidthDivider = " + msgWidthDivider);
		
		textArea = new JTextArea(message, lineCount, params.getTabbedPane().getWidth() / msgWidthDivider);
//		textArea = new JTextArea(message, lineCount, 160);
//		textArea.setFont(Font.decode(Font.MONOSPACED).deriveFont(Font.PLAIN));
		
		String[] clf = {"Monospace", "0", "11"};
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont","Monospace_0_11"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		textArea.setFont(textFont);

		textArea.setEditable(editable);
				
		textArea.addMouseListener(new SandboxMouseListener());
		textArea.addKeyListener(new SandboxKeyListener(params));
		textArea.addFocusListener(params.focusListener);
				
		JScrollPane scrollPane = new JScrollPane(textArea);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton btnOk = new JButton(BUTTON_NAME_OK);
		btnOk.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnOk);
		
		btnNo.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnNo);
		
		btnCancel.addActionListener(new ButtonListener(this));
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		buttonPanel.add(btnCancel);
		
		
		
		
		
		if (envSet != null) {
			JButton btnDel = new JButton(BUTTON_NAME_DELETE);
			btnDel.addActionListener(new ButtonListener(this));
			this.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					btnDel.doClick();
				}
			});
			String envValue = System.getProperty("sandbox.java." + envSet.getName());
			if (envValue != null) {
				buttonPanel.add(btnDel);
			}
		}
		
		JButton btnPhone = new JButton(BUTTON_NAME_PHONE);
		btnPhone.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnPhone);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		setDefaultCloseOperation(SandboxFrameBase.DISPOSE_ON_CLOSE);
		pack();
		this.setLocationRelativeTo(parentComponent);
		int offset = messageCount * 50;
		this.setLocation(this.getLocation().x + offset, this.getLocation().y + offset); // TODO subsequent multi-messages will obscure previous ones
		setVisible(true);
		this.setAlwaysOnTop(true);
//		phoneIn();
		this.mapId = params.addToGlobalMap(this);
		
		if (modal) {
			try {
				int count = 0;
				while (isVisible() && count < closeSeconds) {
					Thread.sleep(1000);
					count++;
				}
				close();

			} catch (InterruptedException ie) {
				sandboxLogger.debug("Wait for input was interrupted.");
			} finally {
				params.removeFromGlobalMap(mapId);
			}
		} else {
			// start timer so message can close itself
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			CloseTimer clockTimer = new CloseTimer();

			scheduleHandle = executor.schedule(clockTimer, closeSeconds, TimeUnit.SECONDS);
		}
	}

	public int getStatus() {
		return status;

	}

	public String getText() {
		return this.textArea.getText();
	}

	protected static boolean isEditing(Component component, SandboxParameters params) {
		if (component == null) {
			return false;
		}
		ISandboxBackedUp backedUp = SandboxMessage.getSandboxTabComponent(component);
		if (backedUp != null) {
			if (backedUp.isEditable() && backedUp.isEnabled()) {
				return true;
			}
		}
		return false;
	}

	protected static boolean isRunning(Component component, SandboxParameters params) {
		if (component == null) {
			return false;
		}
		SandboxTextArea textArea = SandboxMessage.getSandboxTextArea(component);
		if (textArea != null) {
			SandboxTestBase test = textArea.getTest();
			SandboxThread sandboxThread = textArea.getSandboxThread();
			if ((test != null && test.getIsRunning()) || (sandboxThread != null && sandboxThread.isRunning())) {
				return true;
			}
		}
		return false;
	}

	protected static boolean stopTest(Component component) {
		if (component == null) {
			return false;
		}
		SandboxTextArea textArea = SandboxMessage.getSandboxTextArea(component);
		if (textArea != null) {
			SandboxTestBase test = textArea.getTest();

			if (test != null && test.getIsRunning()) {
				test.endMe();
				return true;
			}
		}
		return false;
	}

	protected static SandboxTextArea getSandboxTextArea(Component component) {
		if (component instanceof JScrollPane) {
			JScrollPane scrollPane = (JScrollPane) component;
			if (scrollPane.getComponent(0) instanceof JViewport) {
				JViewport viewport = (JViewport) scrollPane.getComponent(0);
				if (viewport.getComponent(0) instanceof SandboxTextArea) {
					SandboxTextArea textArea = (SandboxTextArea) viewport.getComponent(0);
					return textArea;
				}
			}
		}
		return null;

	}

	protected static ISandboxBackedUp getSandboxTabComponent(Component component) {
		if (component instanceof JScrollPane) {
			JScrollPane scrollPane = (JScrollPane) component;
			if (scrollPane.getComponent(0) instanceof JViewport) {
				JViewport viewport = (JViewport) scrollPane.getComponent(0);
				if (viewport.getComponent(0) instanceof ISandboxBackedUp) {
					return (ISandboxBackedUp) viewport.getComponent(0);
				}
			}
		}
		return null;
	}

	private void setBorderColor() {

		String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
		Color borderColor = new Color(255, 255, 255);

		try {
			borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]),	Integer.parseInt(ebc[2]));
		} catch (NumberFormatException nfe) {
			System.err.println("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");
		}
		Border border = BorderFactory.createLineBorder(borderColor, 5);
		this.getRootPane().setBorder(border);
	}
	
	public class ButtonListener implements ActionListener {
		private SandboxMessage frame;

		public ButtonListener(SandboxMessage frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent actionEvent) {
			if (actionEvent.getActionCommand().equals(BUTTON_NAME_OK)) {
				status = 1;
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_NO)) {
				status = 2;
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_CANCEL)) {

				status = 3;
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_DELETE)) {
				if (actionEvent.getSource() instanceof JButton) {
					System.clearProperty("sandbox.java." + envSet.getName());
					JButton deleteButton = (JButton)actionEvent.getSource();
					deleteButton.setEnabled(false);
					return;
				}
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_PHONE)) {
				SandboxFrameBase.phoneCall("Oh, there you are!", "Where are you?");
				return;
			}

			frame.setVisible(false);
			frame.dispose();
		}

	}

	protected ScheduledFuture scheduleHandle = null;

	class CloseTimer implements Runnable {

		public CloseTimer() {
		}

		@Override
		public void run() {
			System.err.println("Auto-closing '" + title + "'");
			if (status == 0) {
				status = 1;
			}
			setVisible(false);
			params.removeFromGlobalMap(mapId);
		}
	}

	public void close() {
		setVisible(false);
		dispose();
	}

	@Override
	public String toString() {
		if (this.textArea != null) {
			return this.textArea.getText();

		} else {
			return "dmh321";
		}
	}

}
