// Tooll3.java (erase me later)
package sandbox;

import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Monitor an HTTPS website",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "strUrl,frequency",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool13 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String strUrl = getString("strUrl");
		int seconds = getInteger("frequency");

		URL url = new URL(strUrl);
		long lastModified = 0;

		while (this.isRunning){
			checkRunning(seconds);
//			Thread.sleep(frequencyInMilliseconds);
			
			URLConnection urlConn = url.openConnection();
			if (lastModified != urlConn.getLastModified()){
				lastModified = urlConn.getLastModified();
				playSound ();
				if (new SandboxMessage (url.getPath(),params.getTabbedPane()).getStatus()!=	0){
					return;
				}
				Date modifiedDate = new Date(lastModified);
				SimpleDateFormat newSdf = new SimpleDateFormat(SandboxParameters.DATE_FORMAT);
				printLine("Page was modified "+ newSdf.format(modifiedDate));
			}
		}
		//END TEST 
	}
}

