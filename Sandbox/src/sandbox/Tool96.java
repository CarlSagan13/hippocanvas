package sandbox;

import java.awt.Color;
import java.awt.Font;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Change text fonts, colors",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool96 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
//		 String string = getString("string");
		 this.textArea.setFont(this.textArea.getFont().deriveFont(Font.ITALIC));
		 this.textArea.setBackground(Color.blue);
		 this.textArea.setForeground(Color.lightGray);
		 //END TEST
	 }

}
