//Tool2.java (erase me later)
package sandbox;

import java.io.File;

@SandboxAnnotation(
		description = "Run command in a separate process",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"commandLine,workingDirectory",
			SandboxParameters.FOCUSED_NAME,"recordedTest",
			SandboxParameters.YESNO_NAME,"showErrOutput"
},showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool2 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		boolean showErrOutput = getBoolean("showErrOutput");
		String[] commandLine = getStringArrayNoDelim("commandLine");
		String command = commandLine[0].trim();
		
//		if (command.matches("^(Tool|Test)\\d+?")) {
		if (command.matches("^(Tool|Test)\\d.*")) {
			printLine("*** Running as recorded test ***");
			params.runTest(getString("recordedTest"));
			return;
		}
		
		File workingDir = null;
		try {
			if (commandLine.length > 1) {
				workingDir = getDirectory(commandLine[1]);
				printLine("Working directory is " + workingDir);
			} else {
				workingDir = getDirectory(getString("workingDirectory"));
			}
		} catch(Throwable th) {
			printLine("Directory not valid", th);
			return;
		}
		setTooltip(command.replace('\t', ' ') + " :: " + workingDir);

		long delay = 0;

		if (workingDir.exists() && workingDir.isDirectory()) {
			delay = runInSeparateProcess(command, workingDir, true, showErrOutput);
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR + System.getProperty("line.separator", "\n") + "Delay = " + delay);
		// END TEST
	}
}
