package sandbox.junk;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxException;
import sandbox.SandboxTestBase;
import sandbox.SandboxTextArea;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(description = SandboxTestBase.UNDER_CONSTRUCTION + "Desperate!", parameterDescriptions = { "9",
		"regex", "11", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test1 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");
		String regex = getString("regex");
		
		Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		
		
		//Matcher matcher = pattern.matcher(textComponent.getText().replaceAll("\r\n", "\n"));
		Matcher matcher = pattern.matcher(string);
		
//		matcher.reset();
		int startHere = 0;
		while (matcher.find(startHere)) {
			printALine();
			printLine("Start: " + matcher.start());
			printLine("End:" + matcher.end());
			printLine("Group: " + matcher.group());
			for (int index=0; index <= matcher.groupCount(); index++) {
				printLine("Group " + index + ": " + matcher.group(index));
			}
			startHere = matcher.end();
		}


		// END TEST
	}
	
}
