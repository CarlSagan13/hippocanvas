// Tool6.java (erase me later)
package sandbox.junk;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxException;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Regex find in files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "fileRegex,regexString,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class Test18 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regex = getString("fileRegex");
		String directory = getString("directory");
		String regexString = getString("regexString");
		boolean recurse = getBoolean("recurse");
		
		Pattern patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), 
				regex + " :: " + regexString + " :: " + directory);

		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
		params.selectTab(params.getTabbedPane().getSelectedIndex());

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			boolean finding = false;
			Map<String, Integer> map = new HashMap<String, Integer>();
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				if (file.isFile()) {
					StringBuffer searchMe = new StringBuffer(new String(Utilities.getFileBytes(file)));
					List<String> finds = grabRegexList(patternSearch, searchMe, 0);
					if (finds.size() > 0) {
						printLine(file.getPath());
						
						for (String find : finds) {
							if (map.get(find.trim()) == null) {
								map.put(find.trim(), 0);
							} else {
								map.put(find.trim(), map.get(find.trim()) + 1);
							}
							printLine(find.replace('\n', ' '));
						}
					}
				}
				checkRunning();
			}
			if (finding) {
				printALine();
			}
			
			if (map.size() > 0) {
				String[] headers = {"count","key"};
				String[][] columnRows = new String[map.size()][2];
				int row = 0;
				for (String key : map.keySet()) {
					columnRows[row][0] = String.valueOf(map.get(key));
					columnRows[row++][1] = key; 
				}
				String tooltip = "report";
				boolean sort = true;
				this.tabulate(columnRows, headers, tooltip, sort);
			}
			
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
	
	public static List<String> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<String> returnList = new ArrayList<String>(); //Re22.09.28.16.29.32.840
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length());
			boolean aMatch = matcher.find();
			while (aMatch) {
				returnList.add("\t" + matcher.group(0).trim());
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
					if (matcher.group(groupIndex).trim().length() > 0) {
						returnList.add("\t\t" + matcher.group(groupIndex).trim());
					}
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
		return returnList;
	}

}
