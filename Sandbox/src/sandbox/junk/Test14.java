package sandbox.junk;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import sandbox.SandboxAnnotation;
import sandbox.SandboxFrame;
import sandbox.SandboxMessage;
import sandbox.SandboxTestBase;
import sandbox.SandboxTextAreaFrame;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Make Chiclets!",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test14 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
//		 String string = getString("string");

		 
//			new SandboxMessage("Could not start sandbox(es)", "the message", this.scrollPane, 10, 3600,
//					false, false, 0, null); // Open for 1 hour

			JLabel label = new JLabel(this.getClass().getPackage().getName());
			JPanel panel = new JPanel(new BorderLayout());
			panel.add(label);
			JScrollPane scrollPane = new JScrollPane(panel);
			JFrame frame = new JFrame();
			frame.setTitle("the frame title");
			label.setToolTipText("the label tooltip");
			frame.add(scrollPane);
			frame.setLocationRelativeTo(params.getTabbedPane());
			label.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					// TODO Auto-generated method stub
					params.printLine("mouseClicked");
					if (arg0.getClickCount() == 2) {
						params.printLine("here in the label");
						//SandboxFrameBase.phoneCall("Oh, there you are!", "Where are you?");
					}
					
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					params.printLine("mouseEntered");

				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					params.printLine("mouseExited");

				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					params.printLine("mousePressed");

				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					params.printLine("mouseReleased");

				}
				
			});
			frame.pack();
			frame.setVisible(true);
			

//			JPanel panel = new JPanel(new BorderLayout ());
//		 panel.add(new JLabel("hello"));
//		 JScrollPane scrollPane = new JScrollPane();
//		 scrollPane.add(panel);
//
//		 SandboxFrame chiclet = new SandboxFrame();
//		 chiclet.add(scrollPane);
//		 chiclet.setLocation(100,100);
////		 chiclet.setSize(200, 200);
//		 chiclet.setVisible(true);
//		 
//		 while(true)
//			 this.checkRunning(1);
		 
		 
//		 this.setAnnounce(true);
//		 showMessage();
		 
		 
//			SandboxMessage sbm = new SandboxMessage("testing",
//					textArea.getText(), params.getTabbedPane(), 10, 5, 
//					true, false, 10, null);
//			printLine(sbm.getStatus());

//			SandboxTextAreaFrame sbxTAF = new SandboxTextAreaFrame(this, "frap",
//					params.getConfigurationDirectory() + File.separator
//							+ "frapfilename",
//					10);
			
		 

		 //END TEST
	 }

}
