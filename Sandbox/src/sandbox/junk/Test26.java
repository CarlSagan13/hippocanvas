package sandbox.junk;

import java.util.Optional;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Optional Basic example",
 		parameterDescriptions = {
 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test26 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 
	        Optional<String> gender = Optional.of("MALE");
	        String answer1 = "Yes";
	        String answer2 = null;

	        printALine();
	        printLine("Non-Empty Optional:" + gender);
	        printLine("Non-Empty Optional: Gender value : " + gender.get());
	        printLine("Empty Optional: " + Optional.empty());

	        printLine("ofNullable on Non-Empty Optional: " + Optional.ofNullable(answer1));
	        printLine("ofNullable on Empty Optional: " + Optional.ofNullable(answer2));
	        
	        // java.lang.NullPointerException
//NOPE	        printLine("ofNullable on Non-Empty Optional: " + Optional.of(answer2));
	        
	        printALine();
	        
	        Optional<String> gender1 = Optional.of("MALE");

	        Optional<String> nonEmptyGender = Optional.of("male");
	        Optional<String> emptyGender = Optional.empty();

	        printLine("Non-Empty Optional:: " + nonEmptyGender.map(String::toUpperCase));
	        printLine("Empty Optional    :: " + emptyGender.map(String::toUpperCase));

	        Optional<Optional<String>> nonEmptyOtionalGender = Optional.of(Optional.of("male"));
	        printLine("Optional value   :: " + nonEmptyOtionalGender);
//	        printLine("Optional.map     :: " + nonEmptyOtionalGender.map(gender1 -> gender1.map(String::toUpperCase)));
//	        printLine("Optional.flatMap :: " + nonEmptyOtionalGender.flatMap(gender1 -> gender1.map(String::toUpperCase)));
	 
	        
		 //END TEST
	 }

}
