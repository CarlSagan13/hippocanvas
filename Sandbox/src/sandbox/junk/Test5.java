package sandbox.junk;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;

import javax.swing.text.JTextComponent;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with autoselect",
 		parameterDescriptions = {
 				"5", "pattern",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test5 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String pattern = getString("pattern");
		 
		LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
		LocalDate lastWorkDayCurrentMonth = Utilities.getLastWorkingDayOfMonth(lastDayofCurrentMonth);

		DateTimeFormatterBuilder dtfBuilder = new DateTimeFormatterBuilder();
//		dtfBuilder.appendLiteral(string);
		dtfBuilder.appendPattern(pattern);
//		dtfBuilder.appendLiteral('.');
//		dtfBuilder.append(DateTimeFormatter.ISO_TIME);
	
//		LocalDate formattedDate = lastWorkDayCurrentMonth.parse("ddMMyy.hh:mm:ss");
//		printLine(formattedDate);
//		printLine(lastWorkDayCurrentMonth.format(dtfBuilder.toFormatter()));
		
		printLine(lastWorkDayCurrentMonth.format(dtfBuilder.toFormatter()));
		 //END TEST
	 }
	 
		private void autoSelect(SandboxParameters params) {
//			SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
			if (textArea instanceof JTextComponent) {
				JTextComponent component = (JTextComponent) textArea;
				int caretPos = component.getCaretPosition();
				
				String text = component.getText();
				String emptyLine = "\n\n";
				
				int commandSpaceBeginning = text.lastIndexOf(emptyLine, caretPos);
				if (commandSpaceBeginning < 0) {
					commandSpaceBeginning = 0;
				} else {
					commandSpaceBeginning += emptyLine.length();
				}
				int commandSpaceEnding = text.indexOf(emptyLine, caretPos);
				if (commandSpaceEnding < 0) {
					commandSpaceEnding = text.length(); 
				}

				int commandLineBeginning = text.lastIndexOf(SandboxParameters.VISUAL_SEPARATOR, caretPos);
				
				if (commandLineBeginning < 0) {
					commandLineBeginning = 0;
				} else if (commandLineBeginning > 0) {
					commandLineBeginning = text.indexOf('\n', commandLineBeginning) + 1;
				}

				int commandLineEnding = text.indexOf(SandboxParameters.VISUAL_SEPARATOR, caretPos);
				if (commandLineEnding < 0) {
					commandLineEnding = text.length(); 
				}

				int commandBeginning = (commandSpaceBeginning > commandLineBeginning) ? commandSpaceBeginning : commandLineBeginning;
				int commandEnding = (commandSpaceEnding > commandLineEnding && commandLineEnding > 0) ? commandLineEnding : commandSpaceEnding;
				
				component.setSelectionStart(commandBeginning);
				component.setSelectionEnd(commandEnding);
			}
		}


}
