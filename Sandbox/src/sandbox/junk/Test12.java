package sandbox.junk;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Infinite loop",
 		parameterDescriptions = {
 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test12 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 
		 doSomething(string);
		 
		 for (;;) {
			 printLine(string);
			 this.checkRunning(1);
		 }
		 //END TEST
	 }
	 void doSomething(String one) {
		 final int myFinal = 69;
		 
		 //NOPE myFinal = 1956;
		 
	 }
	 void doSomething(String two, String three) {
		 
	 }
	 
	 final class JunkClass {
		 final int myFinal = 69;
		 final String myFinalUnassigned;
		 public JunkClass() {
			//NOPE myFinal = 1956;
			 myFinalUnassigned = "sixty-nine";
		 }
		 void method1() {
			 //NOPE myFinalUnassigned = "two";
		 }
	 }
	 //NOPE class JunkClassOne extends JunkClass {}
	 // can't extend a final class

}
