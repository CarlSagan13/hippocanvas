package sandbox.junk;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import sandbox.Sandbox;
import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Start Sandbox",
 		parameterDescriptions = {
 				"10", "arguments",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test15 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String[] arguments = getStringArray("arguments");
		 Sandbox sandbox = null;
		 
		 Runnable runnable = new Runnable() {
			 public void run() {
 	 	 	 	 new Sandbox(arguments); 
			 }
		 };
		 Thread sandboxThread = new Thread(runnable);
		 sandboxThread.start();
		 
// 	 	 SwingUtilities.invokeLater(new Runnable() { 
// 	 	 	 public void run() { 
// 	 	 		 try { 
// 	 	 	 	 	 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
//	 	 	 	 } catch (Exception exception) { 
//	 	 	 		System.err.println("Cannot set to system look and feel"); 
// 	 	 	 	 } 
// 	 	 	 	 sandbox = new Sandbox(arguments); 
// 	 	 	 } 
// 	 	 });

		 //END TEST
	 }

}
