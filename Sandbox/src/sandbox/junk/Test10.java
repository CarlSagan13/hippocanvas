package sandbox.junk;

import java.util.List;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with Utilities regex methods",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "regex",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
 				SandboxParameters.POPPAD_NAME, "text",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test10 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String regex = getString("regex");
		 String text = getString("text");
		 
//		 String[] finds = Utilities.grabRegex(regex, new StringBuffer(text));
//		 
//		 for (String find : finds) {
//			 printLine(find);
//			 printALine();
//		 }
		 
		 /**
		  * 	public static List<SandboxBasket<String[], String[]>> grabReplaceRegex(String regex, String[] replace,
			StringBuffer passedText, StringBuffer newText)
		  */

		 Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);

		 List<Integer> ends = Utilities.grabEndsRegex(pattern,  new StringBuffer(text), 0);
		 for (Integer end : ends) {
			 printLine(end);
		 }
		 //END TEST
	 }

}
