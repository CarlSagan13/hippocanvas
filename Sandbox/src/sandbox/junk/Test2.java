// Tool53.java (erase me later)
package sandbox.junk;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = SandboxParameters.SANDBOX_UNDER_CONSTRUCTION
			+ "Shuffle text in clipboard using selected regex/order",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"regexOrder",
			SandboxParameters.TRUEFALSE_NAME,"showChanged"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Test2 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		
		printLine(Utilities.getResourceString("/start/sandbox/environment.xml"));
		
//		String[] regexOrder = getStringArray("regexOrder");
//		boolean showChanged = getBoolean("showChanged");
//		
//		String regex = regexOrder[0];
//		String[] replace = regexOrder[1].split(",");
//		
//		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
//		java.awt.datatransfer.Transferable tText = clip.getContents(null);
//		String clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);
//		StringBuffer newText = new StringBuffer();
//		Utilities.grabReplaceRegex(regex,replace,new StringBuffer(clipboardText),newText);
//
//		if (showChanged){
//			newTab(newText.toString(),"Changed ");
//		}else {
//			StringSelection stringSelection = new StringSelection(newText.toString());
//			clip.setContents(stringSelection,null);
//		}
		//END TOOL
	}

}
