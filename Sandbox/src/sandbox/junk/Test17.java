// Tool6.java (erase me later)
package sandbox.junk;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxException;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Find plain string in files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "fileRegex,search,directory",
			SandboxParameters.POPPAD_NAME, "regexString",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
			SandboxParameters.YESNO_NAME, "regexSearch",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class Test17 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String search = getString("search");
		String regex = getString("fileRegex");
		String directory = getString("directory");
		String regexString = getString("regexString");
		boolean regexSearch = getBoolean("regexSearch");
		boolean recurse = getBoolean("recurse");
		
		Pattern patternSearch = null;
		if (regexSearch) {
			patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		}
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), 
				regex + " :: " + search + " :: " + directory);

		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(search));
		params.selectTab(params.getTabbedPane().getSelectedIndex());

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			boolean finding = false;
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				if (file.isFile()) {
					StringBuffer string = new StringBuffer(new String(Utilities.getFileBytes(file)));
					if (string.indexOf(search) >= 0) {
						if (!finding) {
							printALine();
							finding = true;
						}
						printLine(file.getAbsolutePath());
						if (regexSearch) {
							StringBuffer searchMe = new StringBuffer(new String(Utilities.getFileBytes(file)));
							
							List<String> finds = grabRegexList(patternSearch, searchMe, 0);
							for (String find : finds) {
								printLine(find);
							}
						}
					}
				}
				checkRunning();
			}
			if (finding) {
				printALine();
			}
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
	
	public static List<String> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<String> returnList = new ArrayList<String>(); //Re22.09.28.16.29.32.840
		
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length());
			boolean aMatch = matcher.find();
			while (aMatch) {
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
//					returnList.add("groupCount: " + matcher.groupCount() + " groupIndex: " + groupIndex + "\n");
//					returnList.add(matcher.group(groupIndex));
//					returnList.add("groupCount: " + matcher.groupCount() + " groupIndex: 1\n");
					returnList.add(matcher.group(groupIndex));
					
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
		return returnList;
	}

}
