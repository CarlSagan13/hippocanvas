package sandbox.junk;

import java.net.URLDecoder;

import javax.swing.JOptionPane;

import sandbox.SandboxAnnotation;
import sandbox.SandboxMessage;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display string, focused string, scratch pad string, password, and boolean",
 		parameterDescriptions = {
 				"1", "message",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test9 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		String message = getString("message");

		this.sleep(3000);
		printLine("Showing sandbox message");
		SandboxMessage sandboxMessage = new SandboxMessage("tst title", URLDecoder.decode(message, "US-ASCII"), null,
				20, 10 * 60, false, false, 0, null);
		sandboxMessage.setAlwaysOnTop(true);
		this.sleep(3000);
		sandboxMessage.setAlwaysOnTop(false);
		printLine("Showing option pane");
		
		JOptionPane.showMessageDialog(this.textArea, message);
//		JOptionPane optionPane = new JOptionPane();
//		optionPane.showMessageDialog(this.textArea, message);
		
		 //END TEST
	 }

}
