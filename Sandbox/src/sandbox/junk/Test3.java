package sandbox.junk;

import java.awt.Toolkit;
import java.lang.management.ManagementFactory;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxException;
import sandbox.SandboxMessage;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Testing TimeRunner",
 		parameterDescriptions = {
// 				"3", "fromIndex",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test3 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

			TimeRunner timeRunner = new TimeRunner();
			ScheduledFuture scheduleHandle = executor.scheduleAtFixedRate(timeRunner, 1, 5, TimeUnit.SECONDS);
			this.addConnection(scheduleHandle);
			
			this.waitUntilCancelled();
		 //END TEST
	 }

		class TimeRunner implements Runnable {
			private int messagesCount = 0;
			private Map<String, SandboxMessage> messageMap = null;
			private SandboxParameters params = SandboxParameters.createSandboxParameters(); //junk

			public TimeRunner() {
				messageMap = new HashMap<String, SandboxMessage>();
			}

			@Override
			public void run() {
				printLine("running");
				SimpleDateFormat hourMinuteFormat = new SimpleDateFormat("HH:mm");
				SimpleDateFormat dayFormat = new SimpleDateFormat("ddMMMyy");
				SimpleDateFormat dotwFormat = new SimpleDateFormat("E");

				Date now = new Date();

				Properties systemProps = System.getProperties();
				Enumeration<?> systemPropNames = systemProps.propertyNames();
				while (systemPropNames.hasMoreElements()) {
					String propName = (String) systemPropNames.nextElement();
					if (propName.startsWith("sbx.time")) {
						if (messageMap.get(propName) != null) {

							SandboxMessage sandboxMessage = messageMap.get(propName);
							if (sandboxMessage.getStatus() == 1) {
								messageMap.remove(propName); // A message has been
																// OKed so ready to
																// show again next
																// time
								continue;
							} else if (sandboxMessage.getStatus() == -1) {
								messageMap.remove(propName); // A message has been
																// cancelled so
																// allow future
																// start by removing
																// from map,and stop
																// by removing from
																// environment
								System.clearProperty(propName);
								continue;
							}
							continue; // Leave because no interaction with message
										// yet; no need to duplicate existing
										// message
						}
					}
					if (propName.startsWith("sbx.time.eso")) {
						try {
							String[] timeStrings = systemProps.getProperty(propName, "10:15__01:05__5__No+eso+set")
									.split(params.getSandboxDelimiter());
							String day = dayFormat.format(now);

							Date planned = null;
							
							//Check for day-of-week ESO format: ^E.hh:mm__hh:mm__\d+__url+encoded+message
							if (timeStrings[0].matches("^[a-zA-Z]{3}\\.\\d\\d:\\d\\d.*$")) { //DOTW
								if (timeStrings[0].startsWith(dotwFormat.format(now))) {
									SimpleDateFormat dotwTimeFormat = new SimpleDateFormat("E.HH:mm");
									planned = dotwTimeFormat.parse(timeStrings[0]);
								}
							} else if (timeStrings[0].matches("^\\d\\d[a-zA-Z]{3}\\d\\d.*$")) { //Date
								SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMMyy.HH:mm");
								planned = dateFormat.parse(timeStrings[0]);
							} else if (timeStrings[0].matches("^\\d\\d:\\d\\d.*$")) { //Time
								SimpleDateFormat timeFormat = new SimpleDateFormat("ddMMMyy.HH:mm");
								planned = timeFormat.parse(day + "." + timeStrings[0]);
							}
							if (planned != null && now.after(planned)) {
								int minutesOpen = Integer.parseInt(timeStrings[2]);

								String title = "[" + SandboxParameters.targetPackageName + "]"
										+ hourMinuteFormat.format(now) + "("
										+ ManagementFactory.getRuntimeMXBean().getName() + ")";

								String[] deltas = timeStrings[1].split(":");

								long hourDelta = Long.parseLong(deltas[0]) * 60 * 60 * 1000;
								long minuteDelta = Long.parseLong(deltas[1]) * 60 * 1000;
								planned.setTime(now.getTime() + hourDelta + minuteDelta - 2000); // subtract 2 seconds to stay within the delta window

								timeStrings[0] = hourMinuteFormat.format(planned);

								printLine(timeStrings[0] + " :: Next message for " + propName);
								String propertyValue = timeStrings[0] + params.getSandboxDelimiter() + timeStrings[1] + params.getSandboxDelimiter() + timeStrings[2] + params.getSandboxDelimiter()
										+ timeStrings[3];
								System.setProperty(propName, propertyValue);
								String defaultValue = "30Jan56.10:15" + SandboxParameters.DEFAULT_DELIMITER + "01:05"
										+ SandboxParameters.DEFAULT_DELIMITER + "5" + SandboxParameters.DEFAULT_DELIMITER + "No eso set";

								StringBuffer value = new StringBuffer("30Jan56." + systemProps.getProperty(propName, defaultValue));
								int startOfMessage = value.lastIndexOf(SandboxParameters.DEFAULT_DELIMITER) + SandboxParameters.DEFAULT_DELIMITER.length();
								value.insert(startOfMessage, "{sbx-url-ascii ");
								value.append("}");
								
								
								String message = timeStrings[3] + "\n" + SandboxParameters.VISUAL_SEPARATOR
										+ "\nNext message at " + timeStrings[0] + "\nOpen for " + minutesOpen + " minutes"
										+ "\nsbx.eso.message," + value
										+ "\n" + propName + ",{sbx-prop sbx.eso.message}"
										+ "\n" + "sandbox.java." + propName + ",{sbx-prop sbx.eso.message}"
										+ "\n" + SandboxParameters.VISUAL_SEPARATOR;
								showMessage(title, message, minutesOpen, 
										new SandboxBasket<String,String>(propName, systemProps.getProperty(propName, defaultValue)));
							}

						} catch (Exception en) {
							System.err.println("dmh1459: " + en.getMessage());
							en.printStackTrace();
						}
					}
				}
				// TODO something like this is going to have to be done to keep the
				// messages from sliding of the edge of the screen
				messagesCount = 0;

			}

			void showMessage(String title, String message, int minutesOpen, SandboxBasket<String,String> basket) throws SandboxException {
				Toolkit.getDefaultToolkit().beep();

				try {
					SandboxMessage sandboxMessage = new SandboxMessage(title, URLDecoder.decode(message, "US-ASCII"),
							params.getTabbedPane(), 20, minutesOpen * 60, false, false, messagesCount++, basket);
					messageMap.put(basket.getName(), sandboxMessage);
				} catch (Throwable thr) {
					throw new SandboxException("Bad thing with timer: " + thr.getMessage());
				}
			}

		}

}
