package sandbox.junk;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Playing with dates",
 		parameterDescriptions = {
 				"5", "pattern",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test4 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String pattern = getString("pattern");
		 
		LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
		LocalDate lastWorkDayCurrentMonth = Utilities.getLastWorkingDayOfMonth(lastDayofCurrentMonth);

		DateTimeFormatterBuilder dtfBuilder = new DateTimeFormatterBuilder();
//		dtfBuilder.appendLiteral(string);
		dtfBuilder.appendPattern(pattern);
//		dtfBuilder.appendLiteral('.');
//		dtfBuilder.append(DateTimeFormatter.ISO_TIME);
	
//		LocalDate formattedDate = lastWorkDayCurrentMonth.parse("ddMMyy.hh:mm:ss");
//		printLine(formattedDate);
//		printLine(lastWorkDayCurrentMonth.format(dtfBuilder.toFormatter()));
		
		printLine(lastWorkDayCurrentMonth.format(dtfBuilder.toFormatter()));
		 //END TEST
	 }

}
