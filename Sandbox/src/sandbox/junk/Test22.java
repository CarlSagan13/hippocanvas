package sandbox.junk;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Study lambda",
 		parameterDescriptions = {
 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test22 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String string = getString("string");

		// Java code to print the elements of Stream
		// without using double colon operator
		Stream<String> stream = Stream.of("Geeks", "For", string, "A", "Computer", "Portal");

		// Print the stream
		stream.forEach(s -> printLine(s));

		printALine();

		// Java code to print the elements of Stream
		// using double colon operator

		stream = Stream.of("Geeks", "For", string, "A", "Computer", "Portal");

		// Print the stream
		stream.forEach(s -> printLine(s));

		printALine();

		// Java code to show use of double colon operator
		// for static methods
		List<String> list = new ArrayList<String>();
		list.add("Geeks");
		list.add(string);
		list.add("GEEKS");

		// call the static method
		// using double colon operator
		list.forEach(this::printLine);

		printALine();

		// Java code to show use of double colon operator
		// for instance methods
		list = new ArrayList<String>();
		list.add("Geeks");
		list.add(string);
		list.add("GEEKS");

		// call the instance method
		// using double colon operator
		list.forEach(this::printLine);

		printALine();

		list = new ArrayList<String>();
		list.add("Geeks");
		list.add(string);
		list.add("GEEKS");

		// call the instance method
		// using double colon operator
		list.forEach(new GFG()::print);
		
		printALine();
		
		Stream.of("one", "two", "three").forEach(anumber -> printLine(anumber));
		
		// END TEST
	}

	class Test {

		// super function to be called
		String print(String str) {
			return ("Hello " + str + "\n");
		}
	}

	class GFG extends Test {

		// instance method to override super method
		@Override
		String print(String s) {

			// call the super method
			// using double colon operator
			Function<String, String> func = super::print;

			String newValue = func.apply(s);
			newValue += "Bye " + s + "\n";
			printLine(newValue);

			return newValue;
		}

	}

}

/*////////////////////////////////////////////////


// Java code to print the elements of Stream
// without using double colon operator

import java.util.stream.*;

class GFG {
	public static void main(String[] args)
	{

		// Get the stream
		Stream<String> stream
			= Stream.of("Geeks", "For",
						"Geeks", "A",
						"Computer",
						"Portal");

		// Print the stream
		stream.forEach(s -> System.out.println(s));
	}
}


// Java code to print the elements of Stream
// using double colon operator

import java.util.stream.*;

class GFG {
	public static void main(String[] args)
	{

		// Get the stream
		Stream<String> stream
			= Stream.of("Geeks", "For",
						"Geeks", "A",
						"Computer",
						"Portal");

		// Print the stream
		// using double colon operator
		stream.forEach(System.out::println);
	}
}


// Java code to show use of double colon operator
// for static methods

import java.util.*;

class GFG {

	// static function to be called
	static void someFunction(String s)
	{
		System.out.println(s);
	}

	public static void main(String[] args)
	{

		List<String> list = new ArrayList<String>();
		list.add("Geeks");
		list.add("For");
		list.add("GEEKS");

		// call the static method
		// using double colon operator
		list.forEach(GFG::someFunction);
	}
}

// Java code to show use of double colon operator
// for instance methods

import java.util.*;

class GFG {

	// instance function to be called
	void someFunction(String s)
	{
		System.out.println(s);
	}

	public static void main(String[] args)
	{

		List<String> list = new ArrayList<String>();
		list.add("Geeks");
		list.add("For");
		list.add("GEEKS");

		// call the instance method
		// using double colon operator
		list.forEach((new GFG())::someFunction);
	}
}

// Java code to show use of double colon operator
// for super methods

import java.util.*;
import java.util.function.*;

class Test {

	// super function to be called
	String print(String str)
	{
		return ("Hello " + str + "\n");
	}
}

class GFG extends Test {

	// instance method to override super method
	@Override
	String print(String s)
	{

		// call the super method
		// using double colon operator
		Function<String, String>
			func = super::print;

		String newValue = func.apply(s);
		newValue += "Bye " + s + "\n";
		System.out.println(newValue);

		return newValue;
	}

	// Driver code
	public static void main(String[] args)
	{

		List<String> list = new ArrayList<String>();
		list.add("Geeks");
		list.add("For");
		list.add("GEEKS");

		// call the instance method
		// using double colon operator
		list.forEach(new GFG()::print);
	}
}


//*///////////////////////////////////////////////