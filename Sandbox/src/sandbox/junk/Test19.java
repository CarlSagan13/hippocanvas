// Tool6.java (erase me later)
package sandbox.junk;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxException;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Regex find in files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "fileRegex,regexString,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class Test19 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regex = getString("fileRegex");
		String directory = getString("directory");
		String regexString = getString("regexString");
		boolean recurse = getBoolean("recurse");
		
		Pattern patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), 
				regex + " :: " + regexString + " :: " + directory);

		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
		params.selectTab(params.getTabbedPane().getSelectedIndex());

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			boolean finding = false;
			Map<String, Integer> map = new HashMap<String, Integer>();
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				if (file.isFile()) {
					StringBuffer searchMe = new StringBuffer(new String(Utilities.getFileBytes(file)));
					List<SandboxBasket<String, Integer>> finds = grabRegexList(patternSearch, searchMe, 0);
					if (finds.size() > 0) {
						printLine(file.getPath());
						
						for (SandboxBasket<String, Integer> find : finds) {
							if (map.get(find.getName().trim()) == null) {
								map.put(find.getName().trim(), 1);
							} else {
								map.put(find.getName().trim(), map.get(find.getName().trim()) + 1);
							}
							printLine(find.getValue() + "\t" + find.getName().replace('\n', ' '));
						}
					}
				}
				checkRunning();
			}
			if (finding) {
				printALine();
			}
			
			if (map.size() > 0) {
				String[] headers = {"count","group","key"};
				String[][] columnRows = new String[map.size()][3];
				int row = 0;
				for (String key : map.keySet()) {
					columnRows[row][0] = String.valueOf(map.get(key));
					columnRows[row][1] = "g"; 
					columnRows[row++][2] = key; 
				}
				String tooltip = "report";
				boolean sort = true;
				this.tabulate(columnRows, headers, tooltip, sort);
			}
			
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
	
	public static List<SandboxBasket<String, Integer>> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<SandboxBasket<String, Integer>> returnList = new ArrayList<SandboxBasket<String, Integer>>(); //Re22.09.28.16.29.32.840
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length());
			boolean aMatch = matcher.find();
			while (aMatch) {
				String match = matcher.group(0).trim();
				SandboxBasket<String, Integer> basket = new SandboxBasket<String, Integer>(match, 0);
				returnList.add(basket);
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
					if (matcher.group(groupIndex).trim().length() > 0) {
						match = matcher.group(groupIndex).trim();
						basket = new SandboxBasket<String, Integer>(match, groupIndex);
						returnList.add(basket);
					}
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
		return returnList;
	}

}
