package sandbox.junk;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Regex find in files/string",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "regexString,fileOrString",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool60 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regexString = getString("regexString");
		StringBuffer searchMe = new StringBuffer(getStuff("fileOrString"));
		
//		this.newTab(searchMe.toString(), "searching");
		
		Pattern patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), regexString);

		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
//		params.selectTab(params.getTabbedPane().getSelectedIndex());

		try {
			boolean finding = false;
			Map<SandboxBasket<String, Integer>, Integer> map = new HashMap<SandboxBasket<String, Integer>, Integer>();
			List<SandboxBasket<String, Integer>> finds = grabRegexList(patternSearch, searchMe, 0);
			if (finds.size() > 0) {
				for (SandboxBasket<String, Integer> find : finds) {
					if (map.get(find) == null) {
						map.put(find, 1);
					} else {
						map.put(find, map.get(find) + 1);
					}
					printLine(find.getValue() + "\t" + find.getName().replace('\n', ' '));
				}
			}
			if (finding) {
				printALine();
			}
			
			if (map.size() > 0) {
				String[] headers = {"count","group","key"};
				String[][] columnRows = new String[map.size()][3];
				int row = 0;
				for (SandboxBasket<String, Integer> key : map.keySet()) {
					columnRows[row][0] = String.valueOf(map.get(key));
					columnRows[row][1] = String.valueOf(key.getValue()); 
					columnRows[row++][2] = key.getName(); 
				}
				String tooltip = "report";
				boolean sort = true;
				this.tabulate(columnRows, headers, tooltip, sort);
			}
			
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
	}
	
	public static List<SandboxBasket<String, Integer>> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<SandboxBasket<String, Integer>> returnList = new ArrayList<SandboxBasket<String, Integer>>(); //Re22.09.28.16.29.32.840
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length() - 1);
			boolean aMatch = matcher.find();
			while (aMatch) {
				String match = matcher.group(0).trim();
				SandboxBasket<String, Integer> basket = new SandboxBasket<String, Integer>(match, 0);
				returnList.add(basket);
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
					if (matcher.group(groupIndex).trim().length() > 0) {
						match = matcher.group(groupIndex).trim();
						basket = new SandboxBasket<String, Integer>(match, groupIndex);
						returnList.add(basket);
					}
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
		return returnList;
	}

}
