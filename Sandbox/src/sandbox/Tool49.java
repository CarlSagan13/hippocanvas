// Tool49.java (erase me later)
package sandbox;

import java.util.ArrayList;

import java.util.List;
import difflib.ChangeDelta;
import difflib.Chunk;
import difflib.DeleteDelta;
import difflib.Delta;
import difflib.DiffUtils;
import difflib.InsertDelta;
import difflib.Patch;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = "Compare tabs",
		parameterDescriptions = 
		{
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool49 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TOOL	
		int selectedTab = params.preTestSelectedTab;		
		int numTabs = params.tabbedPane.getTabCount();
		if (numTabs == SandboxParameters.LAST_STATIC_TAB + 1){	
			printLine("No tabs to compare");	
		}
		if(selectedTab <= SandboxParameters.LAST_STATIC_TAB) {
			selectedTab = SandboxParameters.LAST_STATIC_TAB + 1;	
		}
		String sandboxTestRegex = "\\-\\-SBXTESTSTART\\-\\-\\n([\\s|\\S]+)\\-\\-SBXTESTSTOP\\-\\-";
		StringBuffer textOne = new StringBuffer(params.getTabComponentText(selectedTab - 1));	
		String[] foundTextOne = Utilities.grabRegex(sandboxTestRegex,textOne);	
		StringBuffer textTwo = new StringBuffer(params.getTabComponentText(selectedTab));
		String[]foundTextTwo = Utilities.grabRegex(sandboxTestRegex,textTwo);		
		List<String> linesOne = new ArrayList<String> ();
		List<String> linesTwo = new ArrayList<String> ();
		if (foundTextOne != null && foundTextTwo != null){
			for (String line : foundTextOne[1].split("\n")){
				linesOne.add(line);	
			}
			for (String line : foundTextTwo[1].split("\n")){
				linesTwo.add(line);	
			}
		} else {
			for (String line : params.getTabComponentText(selectedTab - 1).split("\n")){
				linesOne.add(line);	
			}
			for (String line : params.getTabComponentText(selectedTab).split("\n")){
				linesTwo.add(line);	
			}
		}
		Patch<String> patch = DiffUtils.diff(linesTwo, linesOne);
		
		for (Delta<String> delta : patch.getDeltas()){
			if (delta instanceof InsertDelta){
				Chunk<String> revised = delta.getRevised();
				printLine("INSERTED "+ revised.getLines().size()+ " lines at "+ revised.getPosition());
				for (String revision : revised.getLines()){
					printLine("\t"+ revision);
				}
			}else if (delta instanceof ChangeDelta){
				Chunk <String> originals = delta.getOriginal();
				printLine("CHANGED:");
				printLine("\tORIGINAL "+ originals.getLines().size()+ " lines at "+ originals.getPosition());
				for (String original : originals.getLines()){
					printLine("\t\t "+ original);
				}
				Chunk<String> revised = delta.getRevised();
				printLine("\tREVISED "+ revised.getLines().size()+ " lines at "+ revised.getPosition());
				for (String revision : revised.getLines()){
					printLine("\t\t"+ revision);
				}
			}else if (delta instanceof DeleteDelta){
				Chunk <String> originals = delta.getOriginal();
				printLine("DELETED("+ originals.getLines().size()+ " lines at "+ originals.getPosition()+ ")");
				for (String original : originals.getLines()){
					printLine("\t"+ original);
				}
			}else {
				printLine(delta);
			}
		}
		//END TOOL
	}
}
