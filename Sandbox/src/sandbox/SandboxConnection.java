//SandboxConnection.java (erase me later)
package sandbox;

import java.io.File;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.List;

public class SandboxConnection implements SandboxCommon {

	private Object wrappedObject = null;
	private Object owner = null;
//	private SandboxParameters params = SandboxParameters.createSandboxParameters();

	public SandboxConnection(Object obj) {
		super();
		this.wrappedObject = obj;
	}

	public List<String> close() {
		List<String> messages = new ArrayList<String>();
		if (wrappedObject == null) {
			messages.add("Null wrapped object");
			return messages;
		}

		String message = "Trying to close " + wrappedObject.getClass().getName();

		messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
		sandboxLogger.debug(message);
		try {
			if (wrappedObject.getClass().getName().endsWith(".SSLServerSocketImpl")) {
				message = "Closing " + wrappedObject.getClass().getName();
				messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
				((ServerSocket) wrappedObject).close();
				return messages;
			} else if (wrappedObject instanceof SandboxProcess) {
				SandboxProcess sp = (SandboxProcess) wrappedObject;
//				if (!sp.done) {
					message = "Closing sandbox process " + ((SandboxProcess) wrappedObject).hashCode();
					messages.add(message);
					sp.close();
					sp.proc.destroy();
					message = ((SandboxProcess) wrappedObject).hashCode() + " closed";
					messages.add(message);
//				}
				return messages;
			} else if (wrappedObject instanceof Process) {
				message = "Destroying process " + ((Process) wrappedObject).hashCode();
				messages.add(message);

				((Process) wrappedObject).destroy();
				message = ((Process) wrappedObject).hashCode() + " destroyed";
				return messages;
			} else {
				Boolean isConnected = true;
				Object[] parameters = new Object[1];
				parameters[0] = Boolean.TRUE;

				Method[] methods = wrappedObject.getClass().getDeclaredMethods();

				for (Method method : methods) {
					DatabaseMetaData metaData = null;
					if (method.getName().equals("getMetaData")) {
						try {
							metaData = (DatabaseMetaData) method.invoke(wrappedObject, (Object[]) null);
						} catch (Exception en) {
							message = "Could not call " + wrappedObject.getClass().getName() + "." + method.getName()
									+ "(). . . ";
							messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
							sandboxLogger.debug(message);
						}
					}
					if (method.getName().equals("setWritable") && method.getParameterTypes().length == 1) {// Make sure files are writable again
						try {
							boolean setWritableSuccess = (Boolean) method.invoke(wrappedObject, true);
							if (!setWritableSuccess) {
								SandboxParameters params = SandboxParameters.createSandboxParameters();
								if (wrappedObject instanceof File) {
									File file = (File) wrappedObject;
									params.alert("Could not set writable: " + file.getPath(), params.iconWatch);
								} else {
									params.alert("Could not open", params.iconWatch);
								}
							}
						} catch (Exception en) {
							message = "Could not call " + wrappedObject.getClass().getName() + "." + method.getName() + "(). . . ";
							messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
							sandboxLogger.info(message, en);
						}
					}
					if (method.getName().equals("isConnected")) {
						try {
							isConnected = (Boolean) method.invoke(wrappedObject, (Object[]) null);
						} catch (Exception en) {
							message = "Could not call " + wrappedObject.getClass().getName() + "." + method.getName()
									+ "(). . . ";
							messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
							sandboxLogger.debug(message);
						}
					}
					if (method.getName().equals("close") || method.getName().equals("disconnect")
						|| (method.getName().equals("closeBackedUp"))) {
						if (isConnected) {

							String identifier = wrappedObject.getClass().getName();

							try {
								if (metaData != null) {
									identifier = metaData.getURL();
								}
								message = "Closing " + identifier + " connection with "
										+ method.getParameterTypes().length + " parameters . . . ";

								messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
								sandboxLogger.debug(message);

								if (method.getParameterTypes().length == 0) {
									method.invoke(wrappedObject, (Object[]) null);
									isConnected = false;
								} else {
									method.invoke(wrappedObject, parameters);
									isConnected = false;
								}
								message = "success!";
								messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
								sandboxLogger.debug(message);
							} catch (Exception en) {
								message = "Could not close/disconnect method with " + method.getParameterTypes().length
										+ " parameters for " + identifier + " in map . . . ";
								messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
								sandboxLogger.debug(message);
							}
						} else {
							try {
								method.invoke(wrappedObject);
								message = "success!";

								messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);

							} catch (Throwable th) {
								messages.add("Could not close " + wrappedObject.getClass().getName());
							}
						}
					}
				}
				wrappedObject = null;
				return messages;
			}

		} catch (Throwable th) {
			message = "Could not close " + wrappedObject.getClass().getName() + " :: " + th.getClass() + " :: "
					+ th.getMessage();
			messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
			sandboxLogger.debug(message);
		}
		// message = "Did not close "+ wrappedObject. getClass(). getName();
		messages.add(wrappedObject.getClass().getSimpleName() + ": " + message);
		sandboxLogger.debug(message);
		return messages;
	}

	public Object getObj() {
		return wrappedObject;
	}

	public void setObj(Object wrappedObject) {
		this.wrappedObject = wrappedObject;
	}

	public Object getOwner() {
		return owner;
	}

	public void setOwner(Object owner) {
		this.owner = owner;
	}

}
