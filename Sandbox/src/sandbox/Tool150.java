// Tool6.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Show all files in a directory",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "directory",
		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)

public class Tool150 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regex = ".*";
		String directory = this.getParameters().replaceValues(this.getParameters().getFocusedText());

		boolean recurse = false;
		boolean unlock = false;

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			printALine();
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
				printLine(file.getAbsolutePath());
				if (!file.canWrite()){
					printLine("\tLOCKED!");
					if (unlock) {
						file.setWritable(true);
					}
				}
				checkRunning();
			}
			printALine();
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
}
