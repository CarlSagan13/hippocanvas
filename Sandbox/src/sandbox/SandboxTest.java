package sandbox; 
 
import java.awt.Color; 
import java.awt.Dimension; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.io.File; 
import java.lang.management.ManagementFactory; 
import java.net.InetAddress; 
import java.util.Iterator; 
 
import javax.swing.BorderFactory; 
import javax.swing.JButton; 
import javax.swing.border.Border; 
 
public class SandboxTest extends SandboxFrameBase implements SandboxCommon { 
 	 static final long serialVersionUID =  0; 
 
 	 public SandboxTest(){ 
 		sandboxLogger.debug("Starting"); 
 	 	 try { 
 	 	 	 setSize(new Dimension(sandboxParameters.frameWidth, sandboxParameters.frameHeight)); 
 	 	 	 setTitle(SandboxParameters.targetPackageName + " - " + (new File(".").getCanonicalPath()) 
 	 	 	 	 	 + " // " + sandboxParameters.sandboxHome + " (" + System.getProperty("user.name", "user") 
 	 	 	 	 	 + " " + ManagementFactory.getRuntimeMXBean().getName() + ")"); 
 	 	 	 Iterator<SandboxTestBase> iterTests = sandboxParameters.tests.iterator(); 
 	 	 	 while (iterTests.hasNext()) { 
 	 	 	 	 SandboxTestBase test =  iterTests.next(); 
 	 	 	 	 test.setParameters(this.sandboxParameters); 
 	 	 	 	 String buttonName =  SandboxParameters.BUTTON_PREFIX + test.getTestName(); 
 	 	 	 	 JButton button =  (JButton) sandboxParameters.buttons.get(buttonName); 
 	 	 	 	 button.addActionListener(new ButtonListener(this, test, button) ); 
 	 	 	 	 sandboxParameters.buttons.put(buttonName, button); 
 	 	 	 } 
 	 	 	 System.out.println(SandboxParameters.SANDBOX_OPENED); 
 	 	 	 System.err.println(SandboxParameters.SANDBOX_OPENED); 
 
 	 	 	 String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR,"255_255_255").split("_"); 
 	 	 	 Color borderColor = new Color(255, 255, 255); 

	 	 	 try { 
 	 	 	 	 borderColor =  new Color(
 	 	 	 	 	 	 Integer.parseInt(ebc[0]), 
 	 	 	 	 	 	 Integer.parseInt(ebc[1]), 
 	 	 	 	 	 	 Integer.parseInt(ebc[2]) 
 	 	 	 	 	 ); 	 	 
 	 	 	 } catch(NumberFormatException nfe) { 
 	 	 	 	 sandboxParameters.printLine("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment", true, true ); 
 	 	 	 } 
 	 	 	 Border border =  BorderFactory.createLineBorder(borderColor, 5 ); 
 	 	 	 this.getRootPane().setBorder(border); 
 	 	 } catch (Exception exception) { 
 	 	 	 System.err.println("dmh243H " + this.getClass().getName()+ " :: " + SandboxParameters.SANDBOX_CLOSED); 
 	 	 	 exception.printStackTrace(); 
 	 	 } 
 	 } 
 	 
 	 public class ButtonListener implements ActionListener { 
 	 	 private SandboxTest sandbox; 
 
 	 	 private SandboxTestBase testObj; 
 
 	 	 ButtonListener(SandboxFrameBase sandbox, SandboxTestBase test, JButton button ) { 
 	 	 	 this.sandbox =  (SandboxTest) sandbox; 
 	 	 	 this.testObj =  test; 
 	 	 	 button.setToolTipText(test.getDescription()); 
 	 	 	 button.setText(test.getClass().getSimpleName()); 
 	 	 } 
 
 	 	 public void actionPerformed(ActionEvent actionEvent) { 
 	 	 	 try { 
 	 	 	 	 SandboxTestBase test =  sandbox.sandboxParameters.getTest(testObj.getTestName()); 
 	 	 	 	 if (test.isReady()) { 
 	 	 	 		 Class<?> testClass =  Class.forName(test.getClass ().getName()); 
 	 	 	 	 	 SandboxTestBase newTest =  (SandboxTestBase) testClass.newInstance(); 
 	 	 	
 	 	 	 	 	 newTest.setParameters(sandboxParameters); 
 	 	 	 	 	 newTest.setTestName(test.getClass().getSimpleName()); 
 	
 	 	 	 	 	 newTest.init(); 
 	 	 	 	 	 if (test.presetParameters) { 
 	 	 	 	 	 	 newTest.parameterValues =  test.parameterValues; 
 	 	 	 	 	 	 newTest.presetParameters =  true; 
 	 	 	 	 	 } 
 	 	 	 	 	 newTest.start(); 
 	 	 	 	 	 newTest.setIsRunning(true); 

	 	 	 	 	 sandboxParameters.unhighlightParameters(); 
 	 	 	 	 	 sandboxParameters.unhiglightButtons(); 
 	 	 	 	 	 sandboxParameters.setTest(newTest); 
 	 	 	 	 	 sandboxParameters.showButtonNotReady(newTest.getTestName());
 	 	 	 	 	 
// 	 	 	 		 if (sandboxParameters.preTestSelectedTab > SandboxParameters.LAST_STATIC_TAB) {
// 	 	 	 			sandboxParameters.tabHistory.add(0, sandboxParameters.preTestSelectedTab);
// 	 	 	 		 }
//	 	 	 			Object nothing = sandboxParameters.tabHistory; //just so you can find it later
	 	 	 			
 	 	 	 		 newTest.setTabDisplayName(test.getTabDisplayName()); 	 	 	 	 	 
 					try {
						Thread.sleep(200); //TODO is this really necessary? Seems to fix a problem with freezing the sandbox.
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

 	 	 	 	 	 
 	 	 	 	 } else { //test not ready or not running
 	 	 	 	 	 sandboxParameters.preTestSelectedTab =  sandboxParameters.getSelectedTab(); //DMH
 	 	 	 	 	
 	 	 	 	 	 sandboxParameters.unhighlightParameters(); 
 	 	 	 	 	 sandboxParameters.highlightParameters(test.getParameterDescriptions()); 
 	 	 	 	 	 sandboxParameters.setAllButtonsNotReady(); 
 	 	 	 	 	 test.setReady(true); 
 	 	 	 	 	 sandboxParameters.selectTestInList(test.getTestName()); 
 	 	 	 	 	 sandboxParameters.showButtonReady(test.getTestName()); 
 	 	 	 	 	 sandboxParameters.announce.setEnabled(false); 
 	 	 	 	 	 sandboxParameters.announce.setSelected(false); 
 	 	 	 	 	 
 	 	 	 	 	 
// 	 	 	 	 	 test.setTabDisplayName("dmh1122");
// 	 	 	 	 	 test.setTabDisplayName(test.getClass().getSimpleName()); 

 	 	 	 	 } 
 	 	 	 } catch (Throwable th) { 
 	 	 		 th.printStackTrace();
 	 	 	 	 sandbox.sandboxParameters.printLine(th.getClass().getName()
 	 	 	 	 	 	 + " : message = " + th.getMessage(), th); 
 	 	 	 } 
 	 	 } 
 	 	 
 	 	 public SandboxTestBase getTest(){ 
 	 	 	 return this.testObj; 
 	 	 } 
 	 } 
 }
