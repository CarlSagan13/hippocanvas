//Globals.java (erase me later)
package sandbox;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import sandbox.SandboxParameters.TimeRunner;

/**
 * Holds globals used in tests
 *
 * @author dmharte
 * @since 15.1.2008
 */
public class Globals implements Serializable {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	protected String currentDirectory = "";

	protected ConcurrentHashMap<String, SandboxConnection> map = new ConcurrentHashMap<String, SandboxConnection>();

	public Globals(SandboxParameters params) {
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		MapCleanRunner mapCleanRunner = new MapCleanRunner(params);
		scheduleHandle = executor.scheduleAtFixedRate(mapCleanRunner, 3, 3, TimeUnit.MINUTES);
		Thread thread = new Thread(mapCleanRunner);
		thread.start();
	}

	public String getCurrentDirectory() {
		// TODO Default getter,check logic
		return currentDirectory;
	}

	public void setCurrentDirectory(String currentDirectory) {
		// TODO Default setter<^> check logicCR^ this.currentDirectory =
		// curreniDireciory;
	}

	public ConcurrentHashMap<String, SandboxConnection> getMap() {
		// TODO Default getter,check logic
		return map;
	}

	public void ConcurrentHashMap(ConcurrentHashMap<String, SandboxConnection> map) {
		// TODO Default setter<^M> check logic
		this.map = map;
	}

	protected ScheduledFuture scheduleHandle = null;

	class MapCleanRunner implements Runnable {
		private SandboxParameters params = null;
		public MapCleanRunner(SandboxParameters params) {
			this.params = params;
		}

		@Override
		public void run() {
			System.out.println("Cleaning the map");

			try {
				for (String key : map.keySet()) {
					Object obj = map.get(key);
					if (obj instanceof SandboxConnection) {
						SandboxConnection connection = (SandboxConnection) obj;

						if (connection.getObj() instanceof SandboxMessage) {
							SandboxMessage message = (SandboxMessage) connection.getObj();
							if (!message.isVisible() && message.getStatus() != 0) {
								params.printLine("Removing " + key + " from global map", true, true);
								map.remove(key);
							}
//						} else if (connection.getObj() instanceof SandboxTextAreaFrame) {
//							SandboxTextAreaFrame staf = (SandboxTextAreaFrame) connection.getObj();
//							if (!staf.isVisible()) {
//								System.out.println("Removing " + key);
//								map.remove(key);
//							}
						} else if (connection.getObj() instanceof SandboxFrame) {
							SandboxFrame sandboxFrame = (SandboxFrame) connection.getObj();
							if (!sandboxFrame.isVisible()) {
								params.printLine("Removing " + key + " from global map", true, true);
								map.remove(key);
							}
						} else if (connection.getObj() instanceof SandboxFrameBase.PhoneRevealRunner) {
							SandboxFrameBase.PhoneRevealRunner sfbprr = (SandboxFrameBase.PhoneRevealRunner) connection.getObj();
							if (sfbprr.isDone()) {
								params.printLine("Removing " + key + " from global map", true, true);
								map.remove(key);
							}
						}
					}
				}
			} catch (Throwable th) {
				System.out.println("NOPE! " + th.getMessage());
				th.printStackTrace();
			}

		}

	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
