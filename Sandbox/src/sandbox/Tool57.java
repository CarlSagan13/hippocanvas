package sandbox;

@SandboxAnnotation(
		description = "Put string in global map",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "mapidString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool57 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String mapidString = getString("mapidString");
		 mapidString = params.replaceValues(params.getFocusedText());
		 String[] saMapidString = mapidString.split("\n", 2);
		 String mapId = saMapidString[0];
		 String string = saMapidString[1];
		 
		 this.addToGlobalMap(mapId, string);
		 //END TEST
	 }

}
