// Tooll5.java (erase me later)
package sandbox;

import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PublicKey;
import java.util.Enumeration;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Show keystore contents",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"filePath",
			SandboxParameters.PASSWORD_NAME ,"password",
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool15 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String filePath = getString ("filePath");
		String password = getString("password");
		
		String[]ksTypes = {"PKCS12","JKS","JCEKS"};
		Object []choices = getValueFromWindowList(ksTypes,"Type of cert");
		KeyStore keystore = KeyStore.getInstance((String)choices[0]);//"PKCS12");
		
		FileInputStream fileInStream = new FileInputStream(filePath);
		keystore.load(fileInStream,password.toCharArray());

		Enumeration<String> aliases = keystore.aliases();
		while(aliases.hasMoreElements()) {
			String alias = (aliases.nextElement());
			if (keystore.isCertificateEntry(alias))	{
				//Not supported in PKCS #12 keystore!
			} else if (keystore.isKeyEntry(alias)) {
				Key key = keystore.getKey(alias,password.toCharArray());
				printLine("Algorithm: "+ key.getAlgorithm());
				printLine("Format: "+ key.getFormat());
				java.security.cert.Certificate[]certificateChain = keystore.getCertificateChain(alias);
				printLine("Certificate chains:");
				for (int i = 0; i < certificateChain.length;i++){
					java.security.cert.Certificate cert = certificateChain[i];
					printLine("Cert Type: "+ cert.getType());
					PublicKey publicKey = cert.getPublicKey();
					printLine("Key Algorithm: "+ publicKey.getAlgorithm());
					printLine("Key Format: "+ publicKey.getFormat());
				}
			}
		}		
		//END TEST 
	}
}

