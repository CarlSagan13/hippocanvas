package sandbox.sound;

import java.io.File;

import javax.media.Manager;
import javax.media.Player;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description ="Play MP3 file",
 		parameterDescriptions = {

			"12","mp3FilePath",

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test5 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String mp3FilePath = getString("mp3FilePath");

		Player myMp3File = Manager.createPlayer(new File(mp3FilePath).toURI().toURL());
		myMp3File.start();
		//END TEST
	}
}

