package sandbox.sound;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play a sound file",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "filePath",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test9 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 File file = getFileDir("filePath");
		 String filePath = getString("filePath");
			FileInputStream fileInputStream = new FileInputStream(file);

//			InputStream inputstream = file.getInputStream();
//			Utilities.playThreadedSound(fileInputStream, parameters);
			Utilities.playThreadedSound(filePath, params);
		 //END TEST
	 }

}
