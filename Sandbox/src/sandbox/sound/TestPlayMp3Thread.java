package sandbox.sound;

import java.io.File;

public class TestPlayMp3Thread {
	public static void main(String args[]) {
		File myFile = new File("test.mp3");
		PlayMP3Thread myMp3 = new PlayMP3Thread(myFile);
		myMp3.run();
	}
}
