package sandbox.sound;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Port;
import javax.sound.sampled.TargetDataLine;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Under Construction: Record audio to a file",
 		parameterDescriptions = {

			"1","sampleRate",
			"2","sampleSizeInBits",
			"3","channels",
			"4","frameSize",
			"5","frameRate",
			"6","bigEndian",
			"15","recordSoundFilePath"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test3 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String recordSoundFilePath = getString("recordSoundFilePath");
		Float sampleRate = getFloat("sampleRate");
		int sampleSizeInBits = getInteger("sampleSizeInBits");
		int channels = getInteger("channels");
		int frameSize = getInteger("frameSize");
		Float frameRate = getFloat("frameRate");
		boolean bigEndian = getBoolean("bigEndian");
		File outputFile = new File(recordSoundFilePath);

		/* For simplicity, the audio data format used for recording
		   is hardcoded here. We use PCM 44.1 kHz, 16 bit signed,
		   stereo.
		*/
		
		//Choose the encoding
		AudioFormat.Encoding[] encodings = {AudioFormat.Encoding.PCM_SIGNED, AudioFormat.Encoding.PCM_UNSIGNED, AudioFormat.Encoding.ALAW, AudioFormat.Encoding.ULAW};
		Map<String, AudioFormat.Encoding> encodingMap = new HashMap<String, AudioFormat.Encoding>();
		ArrayList<String> encodingStringsList = new ArrayList<String> ();
		for (AudioFormat.Encoding encoding : encodings ) {
			encodingStringsList.add(encoding.toString());
			encodingMap.put(encoding.toString(), encoding);
		}
		String[] encodingStrings = new String[encodingStringsList.size()];
		encodingStringsList.toArray(encodingStrings);
		String chosenEncodingString = (String)getValueFromWindowList(encodingStrings)[0];
		
		AudioFormat	audioFormat = new AudioFormat(
				encodingMap.get(chosenEncodingString),
				sampleRate, sampleSizeInBits, channels, frameSize, frameRate, bigEndian);
//		AudioFormat	audioFormat = new AudioFormat(
//			encodingMap.get(chosenEncodingString),
//			44100.0F, 16, 2, 4, 44100.0F, false);

		/* Now, we are trying to get a TargetDataLine. The
		   TargetDataLine is used later to read audio data from it.
		   If requesting the line was successful, we are opening
		   it (important!).
		*/
		DataLine.Info	info = new DataLine.Info(TargetDataLine.class, audioFormat);
		TargetDataLine	targetDataLine = null;
		targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
		targetDataLine.open(audioFormat);
		printLine("Unable to get a recording line");

		/* Again for simplicity, we've hardcoded the audio file
		   type, too.
		*/

		//Chose the audio file type	
		AudioFileFormat.Type[] types = {AudioFileFormat.Type.WAVE,AudioFileFormat.Type.AIFC,AudioFileFormat.Type.AIFF,AudioFileFormat.Type.AU,AudioFileFormat.Type.SND};
		Map<String, AudioFileFormat.Type> typeMap = new HashMap<String, AudioFileFormat.Type>();
		ArrayList<String> typeStringsList = new ArrayList<String> ();
		for (AudioFileFormat.Type type : types ) {
			typeStringsList.add(type.toString());
			typeMap.put(type.toString(), type);
		}
		String[] typeStrings = new String[typeStringsList.size()];
		typeStringsList.toArray(typeStrings);
		String chosenTypeString = (String)getValueFromWindowList(typeStrings)[0];
			
		AudioFileFormat.Type	targetType = typeMap.get(chosenTypeString);
//		AudioFileFormat.Type	targetType = AudioFileFormat.Type.WAVE;

		/* Now, we are creating an SimpleAudioRecorder object. It
		   contains the logic of starting and stopping the
		   recording, reading audio data from the TargetDataLine
		   and writing the data to a file.
		*/
		SimpleAudioRecorder	recorder = new SimpleAudioRecorder(
			targetDataLine,
			targetType,
			outputFile);

		/* We are waiting for the user to press ENTER to
		   start the recording. (You might find it
		   inconvenient if recording starts immediately.)
		*/
//		System.in.read();
		/* Here, the recording is actually started.
		 */
		recorder.start();

		/* And now, we are waiting again for the user to press ENTER,
		   this time to signal that the recording should be stopped.
		*/

		while(isRunning) {
			printLine("Recording...");
			Thread.sleep(1000);
		}
		/* Here, the recording is actually stopped.
		 */
		recorder.stopRecording();
		printLine("Recording stopped.");
		//END TEST
	}
}

