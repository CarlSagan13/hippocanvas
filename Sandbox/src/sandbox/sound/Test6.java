package sandbox.sound;

import java.io.File;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description ="Play MP3 file",
 		parameterDescriptions = {

			"12","mp3FilePath",

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test6 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String mp3FilePath = getString("mp3FilePath");
		
		File myFile = new File(mp3FilePath);
		PlayMP3Thread myMp3 = new PlayMP3Thread(myFile);
		myMp3.run();
		//END TEST
	}
}

