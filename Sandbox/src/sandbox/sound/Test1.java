package sandbox.sound;

import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.Port;
import javax.sound.sampled.TargetDataLine;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description ="Play a sound file",
 		parameterDescriptions = {

			SandboxParameters.FOCUSED_NAME,"soundFilePath"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test1 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String soundFilePath = getString("soundFilePath");

	    // specify the sound to play
	    // (assuming the sound can be played by the audio system)
	    File soundFile = new File(soundFilePath);
	    AudioInputStream sound = AudioSystem.getAudioInputStream(soundFile);

	    // load the sound into memory (a Clip)
	    DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
	    Clip clip = (Clip) AudioSystem.getLine(info);
	    clip.open(sound);

	    // due to bug in Java Sound, explicitly exit the VM when
	    // the sound has stopped.
	    clip.addLineListener(new LineListener() {
	      public void update(LineEvent event) {
	        if (event.getType() == LineEvent.Type.STOP) {
	          event.getLine().close();
	          return;
	        }
	      }
	    });

	    // play the sound clip
	    clip.start();
		//END TEST
	}
}

