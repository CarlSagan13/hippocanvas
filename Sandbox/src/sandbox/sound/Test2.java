package sandbox.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Port;
import javax.sound.sampled.TargetDataLine;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Record audio to a .wav file",
 		parameterDescriptions = {

			"15","recordSoundFilePath"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test2 extends SandboxTestBase {	 
	 @Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String recordSoundFilePath = getString("recordSoundFilePath");
		
		/* We have made shure that there is only one command line
		   argument. This is taken as the filename of the soundfile
		   to store to.
		*/
		File outputFile = new File(recordSoundFilePath);

		/* For simplicity, the audio data format used for recording
		   is hardcoded here. We use PCM 44.1 kHz, 16 bit signed,
		   stereo.
		*/
		AudioFormat	audioFormat = new AudioFormat(
			AudioFormat.Encoding.PCM_SIGNED,
			44100.0F, 16, 2, 4, 44100.0F, false);

		/* Now, we are trying to get a TargetDataLine. The
		   TargetDataLine is used later to read audio data from it.
		   If requesting the line was successful, we are opening
		   it (important!).
		*/
		DataLine.Info	info = new DataLine.Info(TargetDataLine.class, audioFormat);
		TargetDataLine	targetDataLine = null;
		try
		{
			targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
			targetDataLine.open(audioFormat);
		}
		catch (LineUnavailableException e)
		{
			printLine("unable to get a recording line");
			e.printStackTrace();
			return;
		}

		/* Again for simplicity, we've hardcoded the audio file
		   type, too.
		*/
		AudioFileFormat.Type	targetType = AudioFileFormat.Type.WAVE;

		/* Now, we are creating an SimpleAudioRecorder object. It
		   contains the logic of starting and stopping the
		   recording, reading audio data from the TargetDataLine
		   and writing the data to a file.
		*/
		SimpleAudioRecorder	recorder = new SimpleAudioRecorder(
			targetDataLine,
			targetType,
			outputFile);

		/* We are waiting for the user to press ENTER to
		   start the recording. (You might find it
		   inconvenient if recording starts immediately.)
		*/
//		System.in.read();
		/* Here, the recording is actually started.
		 */
		recorder.start();

		/* And now, we are waiting again for the user to press ENTER,
		   this time to signal that the recording should be stopped.
		*/

		while(isRunning) {
			printLine("Recording...");
			Thread.sleep(1000);
		}
		/* Here, the recording is actually stopped.
		 */
		recorder.stopRecording();
		printLine("Recording stopped.");
		//END TEST
	}
}

