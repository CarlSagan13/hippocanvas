package sandbox.database;

import java.sql.Connection;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Update",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "sql",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test3 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String sql = getString("sql");
		Database.update(sql, getGlobalMapThing(Connection.class));
		//END TEST
	}
}
