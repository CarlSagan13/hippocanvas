//SandboxException.java (erase me later)
package sandbox; 
 
public class SandboxException extends Exception {
 	 private static final long serialVersionUID = 1L; 
 
 	 public SandboxException(String message){
 	 	 super(message); 
 	 }
 	 
 	 public SandboxException(Throwable th){
 	 	 super(th. getMessage ()); 
 	 }
}
