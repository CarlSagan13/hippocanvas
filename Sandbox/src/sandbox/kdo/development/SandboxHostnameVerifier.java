package sandbox.kdo.development;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class SandboxHostnameVerifier implements HostnameVerifier {
//	private String[] hostnames = new String[1000];
	private Set<String> hostnames = null;
	
	public SandboxHostnameVerifier(List<String> names){
//		super();
		hostnames = new HashSet<String>();
		if (names.size() > 0) {
			for (String name : names) {
				hostnames.add(name);
			}
		}
	}

	@Override
	public boolean verify(String hostname, SSLSession sslSession) {
		// TODO Auto-generated method stub
		if (hostnames.contains(hostname)) {
			return true;
		}
		return false;
	}

}
