package sandbox.kdo.development;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Access HTTPS",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "strUrl",
 				SandboxParameters.POPPAD_NAME, "content",
 				SandboxParameters.POPPAD_NAME,"contentParameters",
 				SandboxParameters.POPPAD_NAME, "hostnameExceptions"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test2 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
			String strUrl = getString("strUrl");
			String content = getString("content");
			String[] contentParameters = getStringArray("contentParameters");
			List<String> hostnameExceptions = getStringList("hostnameExceptions");

			printLine("strUrl = " + strUrl);		
			URL url = new URL(strUrl);

			URLConnection urlConn = null; 
			try {
				HttpsURLConnection httpsUrlConn = (HttpsURLConnection) url.openConnection();
				addConnection(httpsUrlConn);
				if (hostnameExceptions.size() > 0) { //Only allow these exceptions
					boolean isHostnameException = false;
					for(String hostname : hostnameExceptions) {
						if (strUrl.startsWith(hostname)) {
							isHostnameException = true;
						}
					}
					
					if (isHostnameException) {
						printLine("Setting host name verifier");
						httpsUrlConn.setHostnameVerifier(new SandboxHostnameVerifier(hostnameExceptions));
					}
				}
				urlConn = (URLConnection) httpsUrlConn;
			} catch(ClassCastException cce) {
				urlConn = (URLConnection) url.openConnection();
			}
			if (contentParameters.length > 1){
				for (int i = 0; i < contentParameters .length; i += 2) {
					urlConn.addRequestProperty(contentParameters[i], contentParameters[i + 1]);		
				}
			}
			
			if (content.length() > 0 ) {
				printLine("Sending content,length=" + content.length());
				urlConn.setDoOutput(true);
				OutputStream output = urlConn.getOutputStream();
				this.addConnection(output);
				output.write (content.getBytes());
				output.flush();
			}
					
			InputStream inputStream = null; 
			try {
				urlConn.setConnectTimeout(10000);
				inputStream = urlConn.getInputStream();
			} catch(Exception en) { //handle server exceptions
				printLine("TYPE: " + en.getClass().getName());
				printLine("MESSAGE: " + en.getMessage());
				printLine("***ENCODING: " + urlConn.getContentEncoding());
				printLine("***LENGTH: " + urlConn.getContentLength());
				printLine("***TYPE: " + urlConn.getContentType());
				
				if (urlConn != null){
					printLine("***HEADERS START***");
					Map<String, List<String>> headers = urlConn.getHeaderFields();
					Set<String> keys = headers.keySet();
					for (String key : keys) {
						printIt(key + ": ");
						List<String> values = headers.get(key);
						for (String value : values ) {
							printIt(value + " :: ");
						}
						printLine();
					}
					printLine("***HEADERS STOP***");
				}
				return;
			}
			this .addConnection(inputStream);
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			this.addConnection(inputStreamReader);
			BufferedReader input = null;
			
			File file = null; 
			printLine("***HEADERS START***");
			Map<String, List<String>> headers = urlConn .getHeaderFields();
			Set<String> keys =headers.keySet();
			for (String key : keys){
				printIt(key + ": ");
				List<String> values = headers.get(key);
				for (String value : values) {
					printIt(value + " :: ");
					if (value.toLowerCase().startsWith("attachment")) {
						try {
							String strAttachmentsDir = params.getConfigurationDirectory() + File.separator + "attachments";
							file = new File(strAttachmentsDir);
							if (! file.exists()) {
								file.mkdir();
							}

							String filename = value.split("=")[1];
							String filePath = strAttachmentsDir + File.separator + filename; 
							file = new File(filePath);
						} catch(Exception en) {
							printLine("Could not create attachment file");
						}
					}
				}
				printLine();
			}
			printLine("***HEADERS STOP***");

			if (file == null) {
				String startDir = params.getConfigurationDirectory() + File.separator + "files";
				file =new File(startDir);
				if (!file.exists ()) {
					file.mkdir();
				}
				String fileDateStamp =(new SimpleDateFormat(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP)).format(new Date());
				file = new File(startDir + File.separator 
						+ this.getClass().getName() + "." + fileDateStamp);
			}
			FileOutputStream fos = null; 
			fos = new FileOutputStream(file);
			this.addConnection(fos);
			input = new BufferedReader(inputStreamReader);
			this.addConnection(input);
			String str = "";
			
			while (null != ((str = input.readLine()))) {
				fos.write((str + "\n").getBytes());
			}
			fos.close();
			input.close ();
			long maxFileOpenSize = Long.valueOf(System.getProperty("sandbox.conf.maxFileOpenSize", "1000000"));
			if (file.length() < maxFileOpenSize) {
				if (headers.get("Content-Type") != null) {
					//Determine content type
					for (String header : headers.get("Content-Type")) {
						if (header.contains("application/json")) {
							this.newTab(Utilities.jsonPrettyPrint(Utilities.getFileString(file.getAbsolutePath())), "json");
						} else if (header.contains("text/xml")) {
							this.newTab(Utilities.xmlPrettyPrint(Utilities.getFileString(file.getAbsolutePath()),5), "xml");
						} else if (header .contains("application/xml")) {
							this.newTab(Utilities.xmlPrettyPrint(Utilities.getFileString(file.getAbsolutePath()),5), "xml");
						} else if (header.contains("text/html")) {
							this.newTab(Utilities.getFileString(file.getAbsolutePath()), "Browser", "text/html");
						}
					}
				}
				this.newTab(Utilities.getFileString(file.getAbsolutePath()));
			} else {
				printIt ("File size: " + file.length());
				printLine(" which is larger than maximum of " + maxFileOpenSize);
			}
			printLine("File saved as " + file.getName());
			input.close ();
		 //END TEST
	 }

}
