package sandbox.kdo.development;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(description = SandboxTestBase.UNDER_CONSTRUCTION
		+ "Generate JSON from java class, then create the class from JSON", parameterDescriptions = {
		// "1", "string",
		// SandboxParameters.FOCUSED_NAME, "focusedString",
		// SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
		// SandboxParameters.PASSWORD_NAME, "password",
		// SandboxParameters.POPPAD_NAME, "poppad",
		// SandboxParameters.TRUEFALSE_NAME, "truefalse",
		// SandboxParameters.YESNO_NAME, "yesno",
}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test1 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		TestingJsonSerializationData data1 = new TestingJsonSerializationData("hello", 1);
		TestingJsonSerializationData data2 = new TestingJsonSerializationData("there", 2);
		List<TestingJsonSerializationData> listTJSD = new ArrayList<TestingJsonSerializationData>();
		listTJSD.add(data1);
		listTJSD.add(data2);
		Gson gson = new Gson();
		String fromJava = gson.toJson(listTJSD);
		printLine(fromJava);

		Type listType = new TypeToken<ArrayList<TestingJsonSerializationData>>() {
		}.getType();
		Object fromJson = gson.fromJson(fromJava, listType);

		if (fromJson instanceof List) {
			printLine("It's a list!");
			List<TestingJsonSerializationData> list = (List<TestingJsonSerializationData>) fromJson;
			for (TestingJsonSerializationData data : list) {
				printLine(data.getOne() + "::" + data.getTwo());
			}
		}
		// END TOOL
	}

	class TestingJsonSerializationData {
		private String pone = null;
		private Integer ptwo = null;

		public TestingJsonSerializationData(String pone, Integer ptwo) {
			super();
			this.pone = pone;
			this.ptwo = ptwo;
		}

		/**
		 * @return the one
		 */
		public String getOne() {
			return pone;
		}

		/**
		 * @param one
		 *            the one to set
		 */
		public void setOne(String pone) {
			this.pone = pone;
		}

		/**
		 * @return the two
		 */
		public Integer getTwo() {
			return ptwo;
		}

		/**
		 * @param two
		 *            the two to set
		 */
		public void setTwo(Integer ptwo) {
			this.ptwo = ptwo;
		}
	}
}
