// Tool7.java (erase me later)
package sandbox;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = "Show system properties",	
	parameterDescriptions = 
		{
		},showInButtonPanel = true, showInTabPanel = true,isTest = true ,isTool = false)

public class Tool7 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TEST
		java.util.Properties props = System.getProperties();
		java.util.Enumeration<?> propNames = props.propertyNames();
		while(propNames.hasMoreElements()){
			String propName = (String)propNames.nextElement();
			String propValue = props.getProperty(propName);
			printLine(propName + " = "+ propValue);
		}
		printLine("FINISHED");
		//END TEST
	}
}