package sandbox.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import sandbox.SandboxAnnotation;
import sandbox.SandboxException;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Start UDP server",
 		parameterDescriptions = {
 				"3", "csvPortRange",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test1 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		int[] iaPortRange = getIntegerArray("csvPortRange");
		int serverPort = Utilities.findOpenPort(iaPortRange[0], iaPortRange[1], this);
		printLine("Using port " + serverPort);

		byte[] buf = new byte[256];
		DatagramSocket socket = new DatagramSocket(serverPort);
		socket.setSoTimeout(1000);
		this.addConnection(socket);
//		this.addToGlobalMap(socket);

//		this.checkRunning(1);

		while (isRunning) {
			buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			try {
				socket.receive(packet);

				InetAddress address = packet.getAddress();
				int port = packet.getPort();
				packet = new DatagramPacket(buf, buf.length, address, port);
				String received = new String(packet.getData(), 0, packet.getLength());
				printLine("[" + received.trim() + "] (Trimmed)");
				if (received.trim().equals("end")) {
					String message = "terminated";
					packet = new DatagramPacket(message.getBytes(), message.length(), address, port);
					socket.send(packet);
					throw new SandboxException("terminated");
				}
				socket.send(packet);

			} catch (IOException ioe) {
				testLogger.info(ioe.getMessage());
			}
		}
//		socket.close(); //this is closed by closing the test 
		//END TEST
	}
}
