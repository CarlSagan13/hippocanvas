package sandbox.network;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Get IPv6 address",
 		parameterDescriptions = {
 				"13", "address",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test6 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String address = getString("address");
		 printLine(Utilities.noNulls(getThisHostIPV6Address(address)));
		 //END TEST
	 }
	 private byte[] getThisHostIPV6Address(String hostName)
		        throws Exception {
		    InetAddress[] thisHostIPAddresses = null;
		    try {
		        thisHostIPAddresses = InetAddress.getAllByName(InetAddress
		                .getLocalHost().getHostName());
		    } catch (UnknownHostException uhEx) {
		        uhEx.printStackTrace();
		        throw uhEx;
		    }
		    byte[] thisHostIPV6Address = null;
		    for (InetAddress inetAddress : thisHostIPAddresses) {
		        if (inetAddress instanceof Inet6Address) {
		            if (inetAddress.getHostName().equals(hostName)) {
		                thisHostIPV6Address = inetAddress.getAddress();
		                break;
		            }
		        }
		    }
		    // System.err.println("getThisHostIPV6Address: address is "
		    // + Arrays.toString(thisHostIPV6Address));
		    return thisHostIPV6Address;
		}

}
