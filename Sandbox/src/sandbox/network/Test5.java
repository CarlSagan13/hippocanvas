package sandbox.network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION + "UDP IPv6 client",
 		parameterDescriptions = {
 				"13", "address",
 				"14", "port",
 				SandboxParameters.FOCUSED_NAME, "message",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test5 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String strAddress = getString("address");
		 int serverPort = getInteger("port");
		String message = getString("message");

		DatagramSocket socket = new DatagramSocket();
		socket.setSoTimeout(Integer.parseInt(System.getProperty("sandbox.connect.timeout", "1000")));
		this.addConnection(socket);
//		InetAddress address = InetAddress.getByName(strAddress);
		InetAddress address = Inet6Address.getByName(strAddress);

		byte[] buf = message.getBytes();
		DatagramPacket packetToSend = new DatagramPacket(buf, buf.length, address, serverPort);
		socket.send(packetToSend);
		DatagramPacket packetReceived = new DatagramPacket(buf, buf.length);
		socket.receive(packetReceived);
		String received = new String(packetReceived.getData(), 0, packetReceived.getLength());
		printLine(received);
		socket.close();
		 //END TEST
	 }

}
