package sandbox.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import sandbox.SandboxAnnotation;
import sandbox.SandboxException;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Start UDP server",
 		parameterDescriptions = {
 				"3", "csvPortRange",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test97 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		int[] iaPortRange = getIntegerArray("csvPortRange");
		int serverPort = Utilities.findOpenPort(iaPortRange[0], iaPortRange[1], this);
		printLine("Using port " + serverPort);

		byte[] buf = new byte[256];
		DatagramSocket socket = new DatagramSocket(serverPort);
		socket.setSoTimeout(1000);
		this.addConnection(socket);
		this.addToGlobalMap(socket);


		while (isRunning) {
			buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			try {
				socket.receive(packet);

				InetAddress address = packet.getAddress();
				int port = packet.getPort();
				packet = new DatagramPacket(buf, buf.length, address, port);

				printLine("packet.getData().length = " + packet.getData().length);
				printLine("packet.getLength() = " + packet.getLength());

				String received = new String(packet.getData(), 0, packet.getLength());
				printLine("[" + received + "]");
				if (received.equals("end")) {
					throw new SandboxException("terminated");
				}
				socket.send(packet);

			} catch (IOException ioe) {
				printLine(ioe.getMessage());
			}
		}
		printLine("Attempting to close socket ...");
		socket.close();
		printLine("... Socket successfully closed");
		//END TEST
	}
}
