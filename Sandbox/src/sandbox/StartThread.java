//StartThread.java (erase me later)
package sandbox;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class StartThread extends Thread implements SandboxCommon {

	private boolean isRunning = true;
	protected String sandboxHomeBackup = System.getProperty("user.home") + File.separator
			+ SandboxParameters.SANDBOX_HOME_DIR;
	protected String sandboxHome = System.getProperty("sandbox.home", sandboxHomeBackup);
	protected String selectedSandbox = "sandbox";
	protected SandboxParameters params = SandboxParameters.createSandboxParameters(true);

	public StartThread(String sandboxHome) {
		this.sandboxHome = sandboxHome;
	}

	public StartThread(String sandboxHome, String selectedSandbox) {
		this.sandboxHome = sandboxHome;
		this.selectedSandbox = selectedSandbox;
	}

	public void run() {
		try {
			runInSeparateJavaProcess(Start.class.getName() + " " + sandboxHome + " " + selectedSandbox);
		} catch (Exception en) {
			startLogger.error("Could not start", en);
		}
	}

	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * @param isRunning
	 *            the isRunning to set
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public void close() {
		this.isRunning = false;
	}

	public long runInSeparateProcess(String commandLine) throws SandboxException {
		return runInSeparateProcess(commandLine, new File(System.getProperty("user.dir"), "."));
	}

	public long runInSeparateProcess(String commandLine, File workingDir) throws SandboxException {
		return runInSeparateProcess(commandLine, workingDir, true);
	}

	public long runInSeparateProcess(String commandLine, File workingDir, boolean show) throws SandboxException {
		params.printLine(commandLine);
		params.printLine(workingDir.getPath());
		startLogger.info("commandLine = " + commandLine);
		System.out.println(commandLine);
		startLogger.info("workingDir = " + workingDir.getPath());
		startLogger.info("show = " + show);
		long delay = -1;

		Process process = null;
		try {
			Runtime runtime = Runtime.getRuntime();

			boolean unixShell = !Utilities.isWindows();
			if (unixShell) {
				commandLine = commandLine.replace('"', '\'');
				commandLine = "sh -c \"" + commandLine + "\"";
			}

			List<String> commandParts = new ArrayList<String>();
			Matcher matcher = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(commandLine);
			while (matcher.find()) {
				commandParts.add(matcher.group(1).replace("\"", ""));// Add.replace("\"","")to
																		// remove
																		// surrounding
																		// quotes
			}
			Date start = new Date();
			startLogger.info("Command parts: ");
			startLogger.info(commandParts);
			// String[]environment = {"sandbox.identity= true"};
			process = runtime.exec(commandParts.toArray(new String[commandParts.size()]), null, workingDir);

			// process = runtime.exec(commandLine,null,workingDir);

			InputStream inputStream = process.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			InputStream errorStream = process.getErrorStream();
			InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
			BufferedReader errorBufferedReader = new BufferedReader(errorStreamReader);

			String line;
			while ((line = bufferedReader.readLine()) != null) {
				startLogger.info(line);
			}

			while ((line = errorBufferedReader.readLine()) != null) {
				startLogger.error(line);
				if (line.startsWith("Error: Could not find or load main class")) {
					JOptionPane.showMessageDialog(null, line);
				}
			}
			Date end = new Date();
			delay = end.getTime() - start.getTime();
		} catch (IOException en) {
			en.printStackTrace();
			startLogger.info(en.getClass().getSimpleName() + ": " + en.getMessage());
		} finally {
			startLogger.info("Process done");
		}
		try {

			startLogger.info("Exit value = " + process.waitFor());
		} catch (InterruptedException e) {
			throw new SandboxException(e);
		}

		if (process.exitValue() < 0) {
			startLogger.info(SandboxParameters.VISUAL_SEPARATOR);
			startLogger.error("***ABNORMAL PROCESS TERMINATION AT: ***");
			startLogger.info("Command: " + commandLine);
			startLogger.info("Workspace: " + workingDir);
			startLogger.info(SandboxParameters.VISUAL_SEPARATOR);
			return -2;
		}
		return delay;
	}

	public long runInSeparateJavaProcess(String javaCommandLine) throws IOException, SandboxException {
		File workingDir = new File(System.getProperty("user.dir", sandboxHome));
		startLogger.info("Working directory: " + workingDir.getAbsolutePath());
		return runInSeparateJavaProcess(javaCommandLine, workingDir);
	}

	public long runInSeparateJavaProcess(String javaCommandLine, File workingDir) throws IOException, SandboxException {

		java.util.Properties props = System.getProperties();
		StringBuffer javaSettings = new StringBuffer();
		Enumeration<?> propNames = props.propertyNames();
		while (propNames.hasMoreElements()) {
			String propName = (String) propNames.nextElement();
			if (propName.startsWith("sandbox.java.")) {
				javaSettings.append("-D");
				javaSettings.append(propName.substring("sandbox.java.".length()));
				javaSettings.append("=");
				javaSettings.append(props.getProperty(propName));
				javaSettings.append(" ");
			}
			if (propName.startsWith("sandbox.conf.")) {
				javaSettings.append("-D");
				javaSettings.append(propName);
				javaSettings.append("=");
				javaSettings.append(props.getProperty(propName));
				javaSettings.append(" ");
			}
			if (propName.startsWith("sbx.conf.")) {
				javaSettings.append("-D");
				javaSettings.append(propName);
				javaSettings.append("=");
				javaSettings.append(props.getProperty(propName));
				javaSettings.append(" ");
			}
		}
		javaSettings.append("-Dsandbox.home=" + sandboxHome).append(" ");
		javaSettings.append("-Duser.home=" + System.getProperty("user.home")).append(" ");

		String classPath = System.getProperty("java.class.path");
		String commandLine = "java -cp " + classPath + " " + javaSettings + " " + javaCommandLine;
		return runInSeparateProcess(commandLine, workingDir);
	}

}
