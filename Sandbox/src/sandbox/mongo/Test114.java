package sandbox.mongo;

import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use global mongo client"
 			+ "to get a database and put collection",
 		parameterDescriptions = {
 				"13", "fieldName",
 				"14", "value",
 				"15", "database",
 				"19", "collection",
 				SandboxParameters.TRUEFALSE_NAME, "tabulate",
 				SandboxParameters.YESNO_NAME, "show",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test114 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST

			boolean tabulate = getBoolean("tabulate");
			String fieldName = getString("fieldName");
			String value = getString("value");
			String database = getString("database");
			String collectionName = getString("collection ");
				
			MongoClient mongo = this.getGlobalMapThing(MongoClient.class);
			DB mongoDb = mongo.getDB(database);
			
			DBCollection collection = mongoDb.getCollection(collectionName);
			
			printLine(collection.getFullName());
			
			printLine(collection.count());
			
			DBObject dbObj = collection.findOne();
			
			BasicDBObject whereQuery = new BasicDBObject();
			whereQuery.put(fieldName,value);
			DBCursor cursor = collection.find(whereQuery);

			if (cursor.count()== 0){
				tabulate = false;
			}
			String[]trash = new String[1];
			String[]headers = dbObj.keySet().toArray(trash);
			String[][]columnRows = new String[(int)cursor.count()][headers.length];

			int rowIndex = 0;

			StringBuffer headerRow = new StringBuffer();
			for (String header : headers){
				headerRow.append(header).append (",");
			}
			headerRow.deleteCharAt(headerRow.length() - 1);
			printLine(headerRow);
			
			while(cursor.hasNext())	{
				int colIndex = 0;
				DBObject dbObject = cursor.next();
				for (String header : headers){
					printIt(sandbox.Utilities.noNulls(dbObject.get(header)).toString());
					if (colIndex +1	< headers.length){
						printIt(",");
					}
					columnRows[rowIndex][colIndex]= sandbox.Utilities.noNulls(dbObject.get(header)).toString();
					colIndex++;
				}
				printLine();
				rowIndex++;
			}

			if (tabulate){
				this.tabulate(columnRows,headers);
			}
			//END TEST
	 }

}
