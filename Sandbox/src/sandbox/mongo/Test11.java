package sandbox.mongo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Use global MongoDB client to get database",
 		parameterDescriptions = {
 				SandboxParameters.TRUEFALSE_NAME, "tabulate",
 				SandboxParameters.YESNO_NAME, "show",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test11 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		// BEGIN TEST

		boolean tabulate = getBoolean("tabulate");
		boolean show = getBoolean("show");

		MongoClient mongo = this.getGlobalMapThing(MongoClient.class);
		if (mongo == null) {
			printLine("Initialize " + MongoClient.class.getSimpleName() + " in global map first");
			return;
		}

		String[] emptyStringArray = {""};

		Set<String> setDatabaseNames = new HashSet<String>();
		for (String name : mongo.listDatabaseNames()) {
			setDatabaseNames.add(name);
		}
		Object[] chosenDatabaseNames = this.getValueFromWindowList(setDatabaseNames.toArray(emptyStringArray),
				"Database Names");
		if (chosenDatabaseNames == null || chosenDatabaseNames[0] == null) {
			printLine("Aborted");
			return;
		}
		String strDatabase = (String) chosenDatabaseNames[0];
		MongoDatabase mongoDb = mongo.getDatabase(strDatabase);

		Set<String> setCollectionNames = new HashSet<String>();
		for (String name : mongoDb.listCollectionNames()) {
			setCollectionNames.add(name);
		}
		Object[] chosenCollectionNames = this.getValueFromWindowList(setCollectionNames.toArray(emptyStringArray),
				"Collection Names");
		if (chosenCollectionNames == null || chosenCollectionNames[0] == null) {
			printLine("Aborted");
			return;
		}
		String strCollection = (String) chosenCollectionNames[0];

		MongoCollection<Document> collection = mongoDb.getCollection(strCollection);

		FindIterable<Document> fi = collection.find();
		Object obj = fi.first();
		String[] headers = { "" };
		if (obj instanceof org.bson.Document) {
			org.bson.Document doc = (org.bson.Document) obj;
			headers = doc.keySet().toArray(emptyStringArray);
		}

		printLine(collection.getNamespace());

		printLine("Count: " + collection.count());

		FindIterable<Document> cursor = collection.find();

		int rowIndex = 0;
		StringBuffer headerRow = new StringBuffer();
		for (String header : headers) {
			headerRow.append(header).append(",");
		}
		headerRow.deleteCharAt(headerRow.length() - 1);
		printLine(headerRow);
		if (tabulate || show) {
			int range[] = sandbox.Utilities.getRange(0, (int) collection.count(), 1000, params.getTabbedPane());
			List<Document> list = new ArrayList<Document>();
			
			cursor = cursor.skip(range[0]);
			cursor = cursor.limit(range[1] - range[0]);
			cursor.into(list);
			printLine("listAll.size() = " + list.size());
			String[][] columnRows = new String[list.size()][headers.length];

			StringBuffer display = new StringBuffer();
			String newline = "\n";
			if (tabulate) {
				for (Document doc : list) {
					checkRunning();
					int colIndex = 0;
					for (String header : headers) {
						if (show)
							display.append(sandbox.Utilities.noNulls(doc.get(header)).toString());
						if (colIndex + 1 < headers.length) {
							if (show)
								display.append(",");
						}
						columnRows[rowIndex][colIndex] = sandbox.Utilities.noNulls(doc.get(header)).toString();
						colIndex++;
					}
					if (show)
						display.append(newline);
					rowIndex++;
				}
				this.tabulate(columnRows, headers, strDatabase + "." + strCollection + "- " + columnRows.length);
			} else {
				if (show) {
					for (Document doc : list) {
						checkRunning();
						for (String header : headers) {
							display.append(header).append(newline);
							display.append("\t" + sandbox.Utilities.noNulls(doc.get(header)).toString())
									.append(newline);
						}
						display.append(SandboxParameters.VISUAL_SEPARATOR).append(newline);
					}
				}
			}
			printLine(display);
		}
		// END TEST
	 }

}
