package sandbox.mongo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.bson.types.Binary;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxParameters.UNDER_CONSTRUCTION + "Use global mongo client"
 			+ " to get an array of database collection field binary values and put in global map",
 		parameterDescriptions = {
 				"15", "database",
 				"19", "collection",
 				"23", "fieldName",
 				SandboxParameters.YESNO_NAME, "show",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test22 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		boolean show = getBoolean("show");
		String strDatabase = getString("database");
		String strCollection = getString("collection");
		String strFieldName = getString("fieldName");

		MongoClient mongo = this.getGlobalMapThing(MongoClient.class);
		if (mongo == null) {
			printLine("Initialize " + MongoClient.class.getSimpleName() + " in global map first");
			return;
		}

		String[] emptyStringArray = { "" };

		MongoDatabase mongoDb = mongo.getDatabase(strDatabase);

		MongoCollection<Document> collection = mongoDb.getCollection(strCollection);

		FindIterable<Document> fi = collection.find();
		Object obj = fi.first();
		String[] headers = { "" };
		if (obj instanceof org.bson.Document) {
			org.bson.Document doc = (org.bson.Document) obj;
			headers = doc.keySet().toArray(emptyStringArray);
		}

		printLine(collection.getNamespace());

		printLine("Count: " + collection.count());

		FindIterable<Document> cursor = collection.find();

		StringBuffer headerRow = new StringBuffer();
		for (String header : headers) {
			headerRow.append(header).append(",");
		}
		headerRow.deleteCharAt(headerRow.length() - 1);
		printLine(headerRow);
		int range[] = sandbox.Utilities.getRange(0, (int) collection.count(), 1000, params.getTabbedPane());
		List<Document> list = new ArrayList<Document>();

		cursor = cursor.skip(range[0]);
		cursor = cursor.limit(range[1] - range[0]);
		cursor.into(list);
		printLine("listAll.size() = " + list.size());
		this.addToGlobalMap(list);
		if (show) {

			StringBuffer display = new StringBuffer();
			String newline = "\n";
			for (Document doc : list) {
				checkRunning();
				display.append(strFieldName).append(newline);
				display.append("\t" + sandbox.Utilities.noNulls(doc.get(strFieldName)).toString())
						.append(newline);
				display.append(SandboxParameters.VISUAL_SEPARATOR).append(newline);
			}
			printLine(display);
		}
		 //END TEST
	 }

}
