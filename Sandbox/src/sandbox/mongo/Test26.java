package sandbox.mongo;

import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use global mongo client"
			+ " to add to a collection",
 		parameterDescriptions = {
 				"13","fieldName",
 				"14","value",
 				"15","database",
 				"19","collection",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test26 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 	String fieldName = getString("fieldName");
		 	String value = getString("value");
			String strDatabase = getString("database");
			String strCollection = getString("collection");
			
			MongoClient mongo = this.getGlobalMapThing(MongoClient.class);
			MongoDatabase mongoDb = mongo.getDatabase(strDatabase);
			MongoCollection<Document> collection = mongoDb.getCollection(strCollection);
			Document doc = new Document();
			doc.append(fieldName, value);
			collection.insertOne(doc);
//			MongoCollection collection = mongoDb.getCollection(strCollection);
//			collection.drop();
			//END TEST
	 }

}
