package sandbox.mongo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Use global mongo client"
 			+ " to get a database collection and show specific fields",
 		parameterDescriptions = {
 				"13", "fieldName",
 				"14", "value",
				"15", "database",
 				"19", "collection",
 				SandboxParameters.TRUEFALSE_NAME, "tabulate",
 				SandboxParameters.YESNO_NAME, "show",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test13 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		boolean tabulate = getBoolean("tabulate");
		boolean show = getBoolean("show");
		String fieldName = getString("fieldName");
		String value = getString("value");
		String strDatabase = getString("database");
		String strCollection = getString("collection");

		MongoClient mongo = this.getGlobalMapThing(MongoClient.class);
		if (mongo == null) {
			printLine("Initialize " + MongoClient.class.getSimpleName() + " in global map first");
			return;
		}

		String[] emptyStringArray = { "" };

		MongoDatabase mongoDb = mongo.getDatabase(strDatabase);

		MongoCollection<Document> collection = mongoDb.getCollection(strCollection);

		
		// cursor = collection.find(new Document("$text", new Document("$search", query).append("$caseSensitive", 
		// new Boolean(caseSensitive)).append("$diacriticSensitive", new Boolean(diacriticSensitive)))).iterator();

		

		FindIterable<Document> fi = collection.find();
		Object obj = fi.first();
		String[] headers = { "" };
		if (obj instanceof org.bson.Document) {
			org.bson.Document doc = (org.bson.Document) obj;
			headers = doc.keySet().toArray(emptyStringArray);
		}

		printLine(collection.getNamespace());

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put(fieldName,value);
		
		FindIterable<Document> cursor = collection.find(whereQuery);

		printLine("Total Count: " + collection.count());

		int rowIndex = 0;
		StringBuffer headerRow = new StringBuffer();
		for (String header : headers) {
			headerRow.append(header).append(",");
		}
		headerRow.deleteCharAt(headerRow.length() - 1);
		printLine(headerRow);
		if (tabulate || show) {
			List<Document> list = new ArrayList<Document>();
			
			cursor.into(list);
			printLine("list.size() = " + list.size());
			
			int range[] = sandbox.Utilities.getRange(0, list.size(), 1000, params.getTabbedPane());
			if (range[0] == range[1]) {
				range[0] = 0;
				range[1] = Integer.valueOf(new Long(collection.count()).intValue());
			}

//			cursor = cursor.skip(range[0]);
//			cursor = cursor.limit(range[1] - range[0]);
//			cursor.into(list);
			list = list.subList(range[0], range[1]);	
			printLine("Sublist size = " + list.size());

			String[][] columnRows = new String[list.size()][headers.length];

			StringBuffer display = new StringBuffer();
			String newline = "\n";
			if (tabulate) {
				for (Document doc : list) {
					checkRunning();
					int colIndex = 0;
					for (String header : headers) {
						if (show)
							display.append(sandbox.Utilities.noNulls(doc.get(header)).toString());
						if (colIndex + 1 < headers.length) {
							if (show)
								display.append(",");
						}
						columnRows[rowIndex][colIndex] = sandbox.Utilities.noNulls(doc.get(header)).toString();
						colIndex++;
					}
					if (show)
						display.append(newline);
					rowIndex++;
				}
				this.tabulate(columnRows, headers, strDatabase + "." + strCollection + "- " + columnRows.length);
			} else {
				if (show) {
					for (Document doc : list) {
						checkRunning();
						for (String header : headers) {
							display.append(header).append(newline);
							display.append("\t" + sandbox.Utilities.noNulls(doc.get(header)).toString())
									.append(newline);
						}
						display.append(SandboxParameters.VISUAL_SEPARATOR).append(newline);
					}
				}
			}
			printLine(display);
		}
		 //END TEST
	 }

}
