package sandbox.mongo;

import com.mongodb.MongoClient;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Put MongoDB client connection in global map",
 		parameterDescriptions = {
 				"9", "hostIp",
 				"10", "port"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test10 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
			String hostIp = getString("hostIp");
			int port = getInteger("port");
			this.replaceInGlobalMap(new	MongoClient(hostIp,port));
		 //END TEST
	 }

}
