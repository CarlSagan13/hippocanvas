package sandbox;

import java.io.File;

@SandboxAnnotation(
		        description = "Monitor a file and announce",
		        parameterDescriptions = {
 	            SandboxParameters.FOCUSED_NAME, "frequency,filePath"
 	    }, showInButtonPanel = false, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool5 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST

	        int frequency = getInteger("frequency");

	        String filePath = getString("filePath");
	        setTooltip(frequency + " ::" + filePath);

	        File monitoredFile = new File(filePath);

	        long lastModified = monitoredFile.lastModified();
	        while (this.isRunning) {

	            this.checkRunning(frequency);

	            if (lastModified != monitoredFile.lastModified()) {
	                 playSound();

	                 lastModified = monitoredFile.lastModified();
	                 java.util.Date currentDate = new java.util.Date();

	                 java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(SandboxParameters.DATE_FORMAT_TIMESTAMP);
	                 printLine("File was modified " + newSdf.format(currentDate));
	                 if (isAnnounce()) {
	                     new SandboxMessage(this.getClass().getSimpleName() + ": 'CHANGED: " + filePath + "'", params.getTabbedPane());
	                 }
	            }
	        }
	        //END TEST
	 }
}
