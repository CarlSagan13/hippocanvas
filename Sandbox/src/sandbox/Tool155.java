package sandbox;

import java.net.URLDecoder;

@SandboxAnnotation(
		description = "Open up something using OS (sbx.open.command.url.ascii.encoded defines command)",
 		parameterDescriptions = {
 		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool155 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String filePath = this.getParameters().replaceValues(this.getParameters().getFocusedText());
		 String commandLineEncoded = System.getProperty("sbx.open.command.url.ascii.encoded", "cmd+%2Fq+%2Fc");
		 String commandLine = URLDecoder.decode(commandLineEncoded, "US-ASCII");
		 commandLine = commandLine.replace("sbx.address.replacement", filePath);
		 printLine("Using this command line:");
		 printLine(commandLine);
		 this.runInSeparateProcess(commandLine);
//		 this.runInSeparateProcess(commandLine + " \"" + filePath + "\"");
		 //END TEST
	 }

}
