//SandboxTranslatorImpl.java (erase me later) 
package sandbox;

public class SandboxTranslatorImpl extends SandboxTranslatorAbstract implements SandboxTranslator, SandboxCommon {
	 public SandboxTranslatorImpl( ) {
	 	 super( );
	 	 try {
	 	 	 Class<?> translatorClass =  Class.forName(SandboxParameters.TRANSLATOR_CONFIG_PACKAGE + "."
	 	 			 + SandboxTranslatorImpl.class.getSimpleName());
	 	 	 translator = (SandboxTranslator) translatorClass.newInstance();
	 	 } catch( Exception en) {
	 		sandboxLogger.info("Not using local " + SandboxTranslator.class.getSimpleName()) ;
	 		System.err.println("Not using local " + SandboxTranslator.class.getSimpleName());
	 	 }
	 }
}