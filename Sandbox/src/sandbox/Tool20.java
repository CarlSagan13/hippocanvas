// Tool20.java (erase me later)
package sandbox;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.util.Enumeration;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Convert PKCS to JKS keystore",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"pkcsFilePath",
			SandboxParameters.PASSWORD_NAME,"password",
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool20 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String pkcsFilePath = getString("pkcsFilePath");
		String password = getString("password");

		String keystoreFilePath = pkcsFilePath + ".jks";

		KeyStore keystore = KeyStore.getInstance("PKCS12");
		
		FileInputStream fileInStream = new FileInputStream(pkcsFilePath);
		keystore.load(fileInStream,password.toCharArray());

		KeyStore keystoreCopy = KeyStore.getInstance("JKS");
		keystoreCopy.load(null,null);

		Enumeration<String> aliases = keystore.aliases();
		while(aliases.hasMoreElements()) {
			String alias = (aliases.nextElement());
			if (keystore.isCertificateEntry(alias)) {
				//Not supported in PKCS #12 keystore!
			} else if (keystore.isKeyEntry(alias ))	{
				Key key = keystore.getKey(alias,password.toCharArray());
				java.security.cert.Certificate[]certificateChain = keystore.getCertificateChain(alias);
	
				keystoreCopy.setKeyEntry(alias,key,password.toCharArray(),certificateChain);
			}
		}		
	
		FileOutputStream fileOutStream = new FileOutputStream(keystoreFilePath);
		keystoreCopy.store(fileOutStream,password.toCharArray());
		fileOutStream.close();
		//END TEST
	}
}