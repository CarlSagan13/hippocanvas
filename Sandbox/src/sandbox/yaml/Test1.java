package sandbox.yaml;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.yaml.snakeyaml.Yaml;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Load a pojo from a yaml file",
 		parameterDescriptions = {
 				"6", "filePath",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test1 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String filePath = getString("filePath");
		 

        LogConfiguration config = null;
        
        Yaml yaml = new Yaml();
//        try(InputStream in = ClassLoader.getSystemResourceAsStream("logging.yml")) {
        
        /**
         * 		FileInputStream fileInputStream = new FileInputStream(file);
		byte[] ba = new byte[Integer.parseInt(Long.toString(file.length()))];
		fileInputStream.read(ba, 0, (int) file.length());
		fileInputStream.close();

         */
        File file = new File(filePath);
        
        try(InputStream in = new FileInputStream(file)) {
            config = yaml.loadAs(in, LogConfiguration.class);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        
        String path =  config.getPaths().get("logsPath");
        printLine("path=" + path);
        
        String packagePrefix =  config.getPackagePrefix();
        printLine("packagePrefix=" + packagePrefix);    
		 //END TEST
	 }

}
