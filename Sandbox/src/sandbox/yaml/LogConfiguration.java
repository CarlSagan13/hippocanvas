package sandbox.yaml;

import java.util.Map;

public class LogConfiguration {

    private Map<String, String> paths;
    private String packagePrefix;

    public Map<String, String> getPaths() {
        return paths;
    }

    public void setPaths(Map<String, String> paths) {
        this.paths = paths;
    }
    
    public String getPackagePrefix() {
        return packagePrefix;
    }

    public void setPackagePrefix(String packagePrefix) {
        this.packagePrefix = packagePrefix;
    }

    @Override
    public String toString() {
        return "LogConfiguration [paths=" + paths + "]";
    }
}


