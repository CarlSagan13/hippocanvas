//SandboxTranslator Abstract.java (erase me later) 
package sandbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.undo.UndoManager;
public abstract class SandboxTranslatorAbstract implements SandboxTranslator {
	private boolean verbose = false;
	protected SandboxTranslator translator = null;
	public void translate(Object obj, SandboxTestBase test, String depth)throws SandboxException {
		if (test == null ||	!test.getIsRunning()){
			return;
		}
		if (obj == null){
			test.printLine("Object is null");
			return;
		}
		test.printLine("Class: "+ obj.getClass ().getName());
		if (obj instanceof byte[]){
			test.printLine(new String((byte[])obj));
			test.printLine("Byte array length = "+ ((byte[])obj).length);
		} else if (obj instanceof String || obj instanceof Integer
				|| obj instanceof Long || obj instanceof byte []){
			test.printLine(obj);
		} else if (obj instanceof java.util.Properties){
		
			java.util.Properties properties = (java.util.Properties)obj;
			test.printLine("Number of properties: "+ properties.size());
			if (getVerbose()){
				for (Object key : properties.keySet()) {
					test.printLine(key + " -- ");
					translate(properties.get(key), test, depth + "\t");
				}
			}
		} else if (obj instanceof StringBuffer) {
			StringBuffer buffer = (StringBuffer) obj;
			test.printLine(buffer);
		} else if (obj instanceof Map) {
			Map map = (Map)obj;
			test.printLine("Number of items in map: " + map.size());
			if (getVerbose()) {
				for (Object key : map.keySet()) {
					test.printLine(key + "--");
					translate(map.get(key), test, depth + "\t");
				}
			}
		} else if (obj.getClass().isArray()) {
			Object[] objArray = (Object[]) obj;
			test.printLine("Number of items in array: " + objArray.length);
			List list = new ArrayList();
			for (Object object : objArray) {
				list.add(object);
			}
			translateList(list, test, depth);
		} else if (obj instanceof Set) {
			Set set = (Set)obj;
			test.printLine("Number of items in set: " + set.size());
			List list = new ArrayList();
			for (Object setObj : set.toArray()) {
				list.add(setObj);
			}
			translateList(list, test, depth);
		}else if (obj instanceof List){
			List list = (List)obj;
			test.printLine("Number of items in list: "+ list.size());
			if (list.size()> 0){
				translateList(list,test,depth);
			}
		}else if (obj instanceof Object[]){
			Object[]array = (Object[])obj;
			test.printLine("Number of items in array: "+ array.length);
			List list = new ArrayList();
			for (Object item : list){
				list.add(item);
			}
			translateList(list,test,depth);
		} else if (obj instanceof SandboxTextPane){
			SandboxTextPane textPane = (SandboxTextPane)obj;
			test.printLine("Name: "+ textPane.getName());
			if (getVerbose()){
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
				test.printLine("Text:");
				test.printLine(textPane.getText());
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
				test.printLine("Selected Text:");
				test.printLine(textPane.getSelectedText());
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
			}
		} else if (obj instanceof SandboxTextAreaFrame){
			SandboxTextAreaFrame staf = (SandboxTextAreaFrame)obj;
			test.printLine("Name: "+ staf.getName());
			if (getVerbose()){
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
				test.printLine("Text:");
				test.printLine(staf.textArea.getText());
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
				test.printLine("Selected Text:");
				test.printLine(staf.textArea.getSelectedText());
				test.printLine(SandboxParameters.VISUAL_SEPARATOR);
			}
		} else if (obj instanceof SandboxTestTab){
			SandboxTestTab stt = (SandboxTestTab)obj;
			test.printLine("Title: "+ stt.getTitle());
			if (getVerbose()){
				test.printLine("Text:");
				test.printLine(stt.toString());
			}
		} else if (obj instanceof SandboxOpen){
			SandboxOpen sandboxOpen = (SandboxOpen)obj;
			test.printLine("State: "+ sandboxOpen.getState());
			if (getVerbose()){
				test.printLine("Address: "+ sandboxOpen.getAddress());
				test.printLine("Editable: "+ sandboxOpen.isEdit());
			}
		} else if (obj instanceof SandboxProcess){
			SandboxProcess message = (SandboxProcess)obj;
			test.printLine("Prompt: " + message.getHost() + " - " +  message.getWorkspace() + " - " + message.getUser());
			if (getVerbose()){
				test.printLine("History: " + message.getHistory());
			}
		} else if (obj instanceof SandboxMessage){
			SandboxMessage message = (SandboxMessage)obj;
			test.printLine("Name: "+ message.getName());
			if (getVerbose()){
				test.printLine("Title: "+ message.getTitle());
				test.printLine("Text: "+ message.getText());
			}
		} else if (obj instanceof UndoManager){
			UndoManager manager = (UndoManager)obj ;
			test.printLine(manager.getPresentationName());
			if (getVerbose()){
				test.printLine(manager.toString());
			}
		} else if (obj instanceof java.io.File){
			java.io.File file = (java.io.File) obj;
			try {
				test.printLine(file.getAbsolutePath());
				if (getVerbose()){
					test.printLine(Utilities.getFileBytes(file));
				}
			}catch(java.io.IOException ioe){
				test.printLine("Could not translate: " + file);
			}
		} else if (obj instanceof java.sql.Connection){
			java.sql.Connection connection = (java.sql.Connection)obj;
			try {
				test.printLine(connection.getCatalog ());
				if (getVerbose()){
					translate(connection.getClientInfo(),test);
				}
			}catch(java.sql.SQLException sqle){
				test.printLine("Could not translate: "+ connection);
			}
		} else {
			if (translator != null){
				translator.setVerbose(getVerbose());
				translator.translate(obj, test);
			} else {
				test.printLine(obj);
			}
		}
	}
	@Override
	public void translate(Object obj, SandboxTestBase test)	throws SandboxException {
		translate(obj,test,"");	
	}
	private void translateList(List list, SandboxTestBase test, String depth)
		throws SandboxException {
		if (list.size()== 0){
			return;
		}
		int[] range = Utilities.getRange(0,list.size(),test.getParameters().getTabbedPane());
		if (range.length == 1){
			translate(list.get(range[0]),test,depth + "\t");
		}else {
			List subset = list.subList(range[0 ],range[1]);
			int count = 0;
			for (Object item : subset){
				test.printIt("["+ (range[0]	+ count++)+ "]");
				translate(item, test, depth + "\t");
			}
		}
	}

	public void setVerbose(boolean verbose){
		this.verbose = verbose;
	}

	public boolean getVerbose(){
		return verbose;
	}
}
