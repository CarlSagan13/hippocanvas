package sandbox.tables;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Make table with multi-line entries",
 		parameterDescriptions = {
// 				"1", "string",
 				"10","string",
 				"11","row",
 				"12","col"
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test1 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 int row = this.getInteger("row");
		 int col = this.getInteger("col");
		 
		 	String[] headers = {"A","B","C"};
			String[][] columnRows = new String[2][3];
			columnRows[0][0] = "zero-zero";
			columnRows[0][1] = "zero-one";
			columnRows[0][2] = "zero-two";
			columnRows[1][0] = "one-zero";
			columnRows[1][1] = "two-one";
			columnRows[1][2] = "three-two";
			String tooltip = "report";
			boolean sort = true;
			
			
			columnRows[row][col] = string;

			
			this.tabulate(columnRows, headers, tooltip, sort);
		 
		 
		 //END TEST
	 }

}
