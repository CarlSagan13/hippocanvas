// Start.java (erase me later)
package sandbox;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import org.apache.logging.log4j.LogManager;

public class Start extends JFrame implements SandboxCommon {
	static {
		
//*/////////////////////////DMH
		try {
			LogManager.exists("SandboxStart");
		} catch (NoClassDefFoundError ncdfe) {
			JOptionPane.showMessageDialog(null, "Need a class: " + ncdfe.getMessage(), "Class missing",
					JOptionPane.INFORMATION_MESSAGE, null);
			System.exit(-1);
		}
		System.out.println("booyah!");
/*////////////////////////////		
		try {
			logger = LogManager.exists("SandboxStart");
		} catch (NoClassDefFoundError ncdfe) {
			JOptionPane.showMessageDialog(null, "Need a class: " + ncdfe.getMessage(), "Class missing",
					JOptionPane.INFORMATION_MESSAGE, null);
			System.exit(-1);
		}
		System.out.println("booyah!");
//*////////////////////////////		

	}
	private static boolean newSandboxEnvironment = false;

	private static final long serialVersionUID = 1L;
	private static String OK_COMMAND = "OK";
	private static String SORT_COMMAND = "Sort";
	public static String OS_WINNDOWS = "windows";
	public static String OS_UNIX = "unix";
	public static String SANDBOX_ERROR_FLAG = "SANDBOXERRORFLAG";
	public static final String SANDBOX_CLASS_PATH_NAME = "sandbox.class.path";
	private String fileSeparator = System.getProperty("file.separator");
	private static String sandboxHome = null;
	private static String selectedSandbox = null;

	Process process = null;

	JButton okButton, sortButton, deleteButton = null;
	// JComponent component = null;
	SandboxList list = null;

	public Start() {
		// this(System.getProperty("sandbox.home",System.getProperty("user.home")+
		// File.separator + SandboxParameters.SANDBOX_HOME_DIR));
		// }
		//
		// public Start(String sandboxHome){

		if (Start.sandboxHome == null) {
			Start.sandboxHome = System.getProperty("sandbox.home",
					System.getProperty("user.home") + File.separator + SandboxParameters.SANDBOX_HOME_DIR);
		}
		try {

			if (newSandboxEnvironment) {
				//
				String[] choices = { "sandbox" };
				// list = new sandboxList(choices);
				//
//				okButton.doClick(); //TODO this ain't right. Fix for starting sandbox from scratch.
				System.err.println("Setting up sandbox environment - start again to get list of sandboxes (Fix this later)");
				return;
			}

			JPanel panel = new JPanel(new BorderLayout());

			boolean recurse = false;

			startLogger.info("Starting");

//			this.setTitle("Start Sandboxes (" + SandboxParameters.VERSION + ")");
			setTitle("(" + ManagementFactory.getRuntimeMXBean().getName() + ") Start Sandboxes");


			SandboxFilenameFilter filter = new SandboxFilenameFilter();
			filter.setRegex(".*");

			SandboxFilenameFilter sandboxfilter = new SandboxFilenameFilter();
			sandboxfilter.setRegex("sandbox.properties");
			File[] files = sandbox.Utilities.listFilesDirsAsArray(new File(sandboxHome), filter, recurse);

			List<String> packages = new ArrayList<String>();
			for (File file : files) {
				if (file.isDirectory()) {
					if (file.listFiles(sandboxfilter).length > 0) {
						packages.add(file.getName());
					}
				}
			}

			String[] saChoices = {};
			saChoices = packages.toArray(saChoices);
			list = new SandboxList(saChoices);

			Border border = BorderFactory.createEmptyBorder(2, 5, 2, 5);
			// Border border = BorderFactory.createLineBorder(Color.red,5,true);

			list.setBorder(border);
			
			JScrollPane scrollPane = new JScrollPane(list);
			panel.add(scrollPane, BorderLayout.NORTH);

			list.addMouseListener(new java.awt.event.MouseAdapter() {

				@Override
				public void mouseClicked(java.awt.event.MouseEvent e) {
					handleListMouseClicks(e);
				}
			});

			JPanel buttonPanel = new JPanel();

			ButtonListener buttonListener = new ButtonListener();
			okButton = new JButton(Start.OK_COMMAND);

			okButton.addActionListener(buttonListener);
			buttonPanel.add(okButton);
			sortButton = new JButton(Start.SORT_COMMAND);
			sortButton.addActionListener(buttonListener);
			buttonPanel.add(sortButton);

			panel.add(buttonPanel);

//			JScrollPane scrollPane = new JScrollPane(panel);

			getContentPane().add(panel);

			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

			pack();
			// setsize(getwidth()+ 100,getHeight());

			int minimumStartListwidth = Integer
					.parseInt(System.getProperty("sandbox.conf.minimumStartListWidth", String.valueOf("300")));
			this.setSize(new Dimension(minimumStartListwidth, getHeight() + 10));
			this.setMinimumSize(new Dimension(100, getHeight() + 10));
			if (Start.selectedSandbox != null) {
				this.setSandbox(Start.selectedSandbox);
			}
			if (okButton != null) {
				okButton.getRootPane().setDefaultButton(okButton);
			}
			setVisible(true);

		} catch (Exception en) {
			startLogger.info("Could not start: " + en.getMessage());
			String newline = System.getProperty("line.separator", "\n");
			StringBuffer message = new StringBuffer();

			for (StackTraceElement element : en.getStackTrace()) {
				message.append(newline).append(element.toString());
			}
			JOptionPane.showMessageDialog(getContentPane(), message.toString(), "Could not start: " + en.getMessage(),
					JOptionPane.INFORMATION_MESSAGE, null);
		}

	}

	private void handleListMouseClicks(java.awt.event.MouseEvent e) {
		SandboxList list = (SandboxList) e.getComponent();
		Object selectedText = list.getSelectedValue();
		if (e.getClickCount() == 2) {
			System.out.println("double click");
			okButton.doClick();
		} else {
			System.out.println("single click");
		}
	}

	public class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			try {

				if (actionEvent.getActionCommand().equals(Start.OK_COMMAND)) {

					// okButton.getParent().add(new
					// JLabel(SandboxParameters.createSandboxParameters(true).iconPretty),O);
					// okButton.setICOn(sandboxParameters.createSandboxParameters().iconRunm'ng);
					// okBUtton.setName("");
					//
					okButton.paint(okButton.getGraphics());

					List<String> errors = new ArrayList<String>();
					List selectedValues = new ArrayList();

					try {
						selectedValues = list.getSelectedValuesList();
					} catch (NoSuchMethodError nsme) {// Allowing for earlier
														// Java version
						for (Object selectedvalue : list.getSelectedValues()) {
							selectedValues.add(selectedvalue);
						}
						startLogger.info("Could not use selected values list; probably java version problem", nsme);
					}
					if (selectedValues.size() == 0) {
						System.exit(0);
					}
					for (Object value : selectedValues) {
						try {
							System.err.println("Trying to open " + value);
							CopyOnWriteArrayList<String> report = openSandbox((String) value);

							for (String line : report) {
								if (line.startsWith(SANDBOX_ERROR_FLAG)) {
									errors.add(line.substring(SANDBOX_ERROR_FLAG.length()));
								}
							}
						} catch (Throwable th) {
//							JOptionPane.showMessageDialog((JButton) actionEvent.getSource(), "could not open " + value 
//									+ ". Make sure workspace contains classpath-referenced folders.");
							th.printStackTrace();
						}
					}
					if (errors.size() > 0) {
						StringBuffer buffer = new StringBuffer();
						for (String error : errors) {
							buffer.append(error).append('\n');
						}
						new SandboxMessage("Could not start sandbox(es). Ensure workspace contains classpath-referenced folders.",
								"Ensure workspace contains classpath-referenced folders.\n" + buffer.toString(), getContentPane(), 10, 3600,
								false, false, 0, null); // Open for 1 hour
					}
					setVisible(false);
					dispose();
				} else if (actionEvent.getActionCommand().equals(Start.SORT_COMMAND)) {
					int size = list.getModel().getSize();
					ArrayList arrayList = new ArrayList();
					for (int i = 0; i < size; i++) {
						arrayList.add(list.getModel().getElementAt(i));

					}
					Collections.sort(arrayList);
					list.setListData(arrayList.toArray());
				}
			} catch (Exception en) {
				startLogger.debug(this.getClass().getName() + ":: " + en.getClass().getName() + ": message = " + en.getMessage(), en);
			}
		}

		private CopyOnWriteArrayList<String> openSandbox(String packageName) throws SandboxException {
			CopyOnWriteArrayList<String> report = new CopyOnWriteArrayList<String>();

			try {
				setTitle("*(" + ManagementFactory.getRuntimeMXBean().getName() + ") " + packageName + " Starting");

				list.setSelectedValue(packageName, true);

				Properties globalProps = new Properties();
				String globalFilePath = sandboxHome + fileSeparator + "environment.xml";
				File globalEnvironmentFile = new File(globalFilePath);
				if (globalEnvironmentFile.exists()) {
					FileInputStream environmentFIS = new FileInputStream(globalEnvironmentFile);

					globalProps.loadFromXML(environmentFIS);
					startLogger.info("Loaded global properties = " + globalFilePath);
				}

				Properties localProps = new Properties();
				String configurationDirectory = sandboxHome + fileSeparator + packageName;
				String filepath = configurationDirectory + fileSeparator + "environment.xml";
				startLogger.debug("filepath = " + filepath);
				File environmentFile = new File(filepath);
				if (!environmentFile.exists()) {
					try {
						environmentFile = Utilities.getResourceFile("/start/sandbox/environment.xml");
						startLogger.debug("Using default resource environment");

					} catch (FileNotFoundException fnfe) {
						InputStream inputStream = this.getClass().getResourceAsStream("/start/sandbox/environment.xml");
						environmentFile = File.createTempFile("tmpfile", ".tmp");

						OutputStream out = new FileOutputStream(environmentFile);

						int read;

						byte[] bytes = new byte[SandboxParameters.MAX_BYTE_ARRAY_SIZE];
						while ((read = inputStream.read(bytes)) != -1) {
							out.write(bytes, 0, read);
						}
						environmentFile.deleteOnExit();
						new SandboxMessage("INTERNAL DEFAULT ENVIRONMENT", "Could not use environment", null, 3, false);
					}
				}
				FileInputStream environmentFIS = new FileInputStream(environmentFile);
				localProps.loadFromXML(environmentFIS);
				environmentFIS.close();

				String javaMemorySettings = localProps.getProperty("sandbox.javamemory", "-Xms64m -Xmx512m")
						.replaceAll(SandboxParameters.SANDBOX_NULL, "");

				StringBuffer javaSettings = new StringBuffer();

				Enumeration<?> localPropNames = localProps.propertyNames();
				Set<String> locals = new HashSet<String>();

				while (localPropNames.hasMoreElements()) {
					String propName = (String) localPropNames.nextElement();
					if (propName.startsWith("sandbox.java.")) {
						javaSettings.append("-D");
						String name = propName.substring("sandbox.java.".length());
						locals.add(name);
						javaSettings.append(name);
						javaSettings.append("=");
						javaSettings.append(localProps.getProperty(propName));
						javaSettings.append(" ");
					}
				}

				Enumeration<?> globalPropNames = globalProps.propertyNames();
				while (globalPropNames.hasMoreElements()) {
					String propName = (String) globalPropNames.nextElement();
					if (!locals.contains(propName)) {

						javaSettings.append("-D");

						javaSettings.append(propName);
						javaSettings.append("=");
						javaSettings.append(globalProps.getProperty(propName));
						javaSettings.append(" ");

					}
				}

				javaSettings.append("-Dsandbox.home=" + sandboxHome).append(" ");
				javaSettings.append("-Duser.home=" + System.getProperty("user.home")).append(" ");

				String classPath = localProps.getProperty(Start.SANDBOX_CLASS_PATH_NAME);
				if (classPath == null) {

					classPath = System.getProperty("java.class.path");
				}

				String javaCommandLine = "sandbox.Sandbox " + packageName;
				String commandLine = "java ";
				if (javaMemorySettings.length() > 0)
					commandLine += javaMemorySettings + " ";

				if (javaSettings.length() > 0)
					commandLine += javaSettings;

				String quote = "";
				if (Utilities.isWindows()) {
					quote = "\"";
				}

				commandLine += "-cp " + quote + classPath + quote + " " + javaCommandLine;

				process = Runtime.getRuntime().exec(commandLine);
				startLogger.info("Starting new Sandbox: " + commandLine);

//				System.err.println("dmh243I " + commandLine);

				report = logProcess(process);

				for (String line : report) {

					if (line.startsWith("Error")) {
						report.add(SANDBOX_ERROR_FLAG + packageName + " :: " + line);
					}
				}

			} catch (Exception en) {
				System.err.println("dmh243J " + en);
				startLogger.debug("something bad");
			} finally {
				startLogger.info("Done");
			}
			return report;

		}

		public CopyOnWriteArrayList<String> logProcess(Process process) throws SandboxException {
			CopyOnWriteArrayList<String> report = new CopyOnWriteArrayList<String>();

			try {
				InputStream inputStream = process.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

				InputStream errorStream = process.getErrorStream();
				InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
				BufferedReader errorBufferedReader = new BufferedReader(errorStreamReader);

				ExecutorService executor = Executors.newSingleThreadExecutor();
				Task inputStreamTask = new Task(bufferedReader);
				Future<String> inputFuture = executor.submit(inputStreamTask);
				try {
					startLogger.debug("started input...");
					startLogger.debug(inputFuture.get(10000, TimeUnit.MILLISECONDS));// Start
																				// process
																				// input
																				// logging
																				// task,timeout
																				// 10
																				// seconds
					startLogger.debug("...finished input!");
				} catch (TimeoutException te) {
					startLogger.debug("Timed out input!");
				}
				executor.shutdownNow();

				for (String line : inputStreamTask.getLines()) {
					report.add(line);
				}

				executor = Executors.newSingleThreadExecutor();
				Task errorStreamTask = new Task(errorBufferedReader);
				Future<String> errorFuture = executor.submit(errorStreamTask);
				try {
					startLogger.debug("started error input...");
					startLogger.debug(errorFuture.get(10000, TimeUnit.MILLISECONDS));// Start
																				// process
																				// error
																				// logging
																				// task,timeout
																				// 10
																				// Seconds
					startLogger.debug("...finished error input!");
				} catch (TimeoutException te) {
					startLogger.debug("Timed out error input!");
				}
				executor.shutdownNow();

				for (Iterator<String> it = errorStreamTask.getLines().iterator(); it.hasNext();) {
					String line = it.next();
					report.add(line);
				}

				return report;
			} catch (Exception en) {
				startLogger.info("Could not start sandbox: " + en.getMessage());
				// String newline = System.getProperty("line.separator","\n");
				// stringBuffer message = new StringBuffer(en.getMessage());
				for (StackTraceElement element : en.getStackTrace()) {
					// message.append(newline).append(element.toString());
					report.add(element.toString());
				}
				return report;
				// new
				// sandboxMessage(en.getClass().getName(),message.toString(),this,20,false);
			}
		}

		class Task implements Callable<String> {

			protected BufferedReader bufferedReader;
			private CopyOnWriteArrayList<String> lines = new CopyOnWriteArrayList<String>();
			// List list = Collections.synchronizedList(new ArrayList(...));

			public Task(BufferedReader bufferedReader) {
				this.bufferedReader = bufferedReader;
			}

			@Override
			public String call() {
				String line;
				try {
					while ((line = bufferedReader.readLine()) != null) {
						startLogger.debug(line);
						lines.add(line);
						if (line.equals(SandboxParameters.SANDBOX_CLOSED)) {
							System.err.println("***" + line + "***");
							break;
						} else if (line.equals(SandboxParameters.SANDBOX_OPENED)) {
							break;
						} else if (line.startsWith("Error")) {
							System.err.println("***" + line + "***");
							break;
						}
					}
				} catch (IOException ioe) {
					System.err.println("dmh243K " + ioe);
				}
				return "Ready!";
			}

			public CopyOnWriteArrayList<String> getLines() {
				return lines;
			}

		}

	}

	public static void main(String[] args) {
		startLogger.info("starting");

		File sandboxHome = new File(System.getProperty("user.home") + System.getProperty("file.separator")
				+ SandboxParameters.SANDBOX_HOME_DIR);
		System.err.println("dmh243L " + sandboxHome.getPath());

		if (args.length == 1) {
			sandboxHome = new File(args[0]);
		} else if (args.length == 2) {
			sandboxHome = new File(args[0]);
			Start.selectedSandbox = args[1];
		}

		Start.sandboxHome = sandboxHome.getAbsolutePath();
		try {
			newSandboxEnvironment = Utilities.createSandboxHome(sandboxHome);
			System.setProperty("sandbox.home", sandboxHome.getPath());
		} catch (NoClassDefFoundError ncdfe) {
			startLogger.info("Could not start sandbox: " + ncdfe.getMessage());
			String newline = System.getProperty("line.separator", "\n");
			StringBuffer message = new StringBuffer(ncdfe.getMessage());
			for (StackTraceElement element : ncdfe.getStackTrace()) {
				message.append(newline).append(element.toString());
			}
			new SandboxMessage(ncdfe.getClass().getName(), message.toString(), null, 20, false);

			return;
		} catch (Throwable th) {
			startLogger.info("Could not start sandbox: " + th.getMessage());
			String newline = System.getProperty("line.separator", "\n");
			StringBuffer message = new StringBuffer(th.getMessage());
			for (StackTraceElement element : th.getStackTrace()) {
				message.append(newline).append(element.toString());
			}
			new SandboxMessage(th.getClass().getName(), message.toString(), null, 20, false);

		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

				} catch (Throwable th) {
					th.printStackTrace();
					startLogger.info("Could not start sandbox: " + th.getMessage());
					String newline = System.getProperty("line.separator", "\n");
					StringBuffer message = new StringBuffer(th.getMessage());
					for (StackTraceElement element : th.getStackTrace()) {
						message.append(newline).append(element.toString());
					}
					new SandboxMessage(th.getClass().getName(), message.toString(), null, 20, false);
				}
				try {
					new Start();
				} catch (NoClassDefFoundError ncdfe) {
					startLogger.info("Could not start sandbox: " + ncdfe.getMessage());
					String newline = System.getProperty("line.separator", "\n");
					StringBuffer message = new StringBuffer(ncdfe.getMessage());
					for (StackTraceElement element : ncdfe.getStackTrace()) {
						message.append(newline).append(element.toString());
					}
					new SandboxMessage(ncdfe.getClass().getName(), message.toString(), null, 20, false);
					return;
				} catch (Throwable th) {
					startLogger.info("could not start sandbox: " + th.getMessage());
					// String newline =
					// System.getProperty("line.separator","\n");
					// Stn'ngBuffer message = new StringBuffer(th.getMessage());
					for (StackTraceElement element : th.getStackTrace()) {
						System.err.println("dmh243M " + element.toString());
					}
					// new
					// SandboxMessage(th.getClass().getName(),message.toString(),null,20,false);
				}
			}
		});
	}
	
	protected void setSandbox(String sandboxPackage) {
		list.setSelectedValue(sandboxPackage, true);
	}
}
