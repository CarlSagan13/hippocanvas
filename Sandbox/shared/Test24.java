package sandbox.junk;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.FindInFiles;
import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxException;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Tool14;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Regex find in files/string with correlative tabulation",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "regexString,fileOrString",
			SandboxParameters.TRUEFALSE_NAME, "correlate"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class Test24 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regexString = getString("regexString");
		StringBuffer searchMe = new StringBuffer(getStuff("fileOrString"));
		boolean correlate = getBoolean("correlate");
		
//		this.newTab(searchMe.toString(), "searching");
		
		Pattern patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), regexString);

//		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
//		params.selectTab(params.getTabbedPane().getSelectedIndex());

		try {
			boolean finding = false;
			Map<SandboxBasket<String, Integer>, Integer> map = new HashMap<SandboxBasket<String, Integer>, Integer>();
			List<SandboxBasket<String, Integer>> finds = Utilities.grabRegexList(patternSearch, searchMe, 0);
			StringBuffer delimitedData = new StringBuffer();
			if (finds.size() > 0) {
				for (SandboxBasket<String, Integer> find : finds) {
					if (map.get(find) == null) {
						map.put(find, 1);
					} else {
						map.put(find, map.get(find) + 1);
					}
//					if (find.getValue() == 0) {
//						printLine();
//						delimitedData.append('\n');
//					}
//					printIt(find.getValue() + "\t" + find.getName().replace('\n', ' '));
					if (find.getValue() > 0) {
						printIt("\t");
						delimitedData.append('\t');
					}
//					printIt(find.getName().replace('\n', ' '));
//					delimitedData.append(find.getName().replace('\n', ' '));
					printIt(find.getName().replaceAll(System.getProperty("line.separator", "\n"), "{sbx-prop line.separator}"));
//					delimitedData.append("poop");
//					delimitedData.append(find.getName().replaceAll(System.getProperty("line.separator", "\n"), "{sbx-prop line.separator}"));
					delimitedData.append(find.getName());
				}
				printLine();
				delimitedData.append('\n');
			}
//			if (finding) {
//				printALine();
//				delimitedData.append('\n');
//			}
			
			if (map.size() > 0) {
				if (correlate) {
					String recordedTest = Tool14.class.getSimpleName() + "\nquotes	false"
							+ "\ndelimitedData	-encoded " + URLEncoder.encode(delimitedData.toString(), "US-ASCII")
							+ "\nheader	false"
							+ "\nregexRowColumn	-encoded %5Cn%0A%5Ct";
					params.runTest(recordedTest, Utilities.limitTabLength("tabulated"));
				}

				/**
				 * 		String[]charsetNames = {"US-ASCII","ISO-8859-1","UTF-8","UTF-16BE","UTF-16LE","UTF-16"};
		Object[]charset = this.getValueFromWindowList(charsetNames,"Type encoding");
		if (encode){
			printLine(URLEncoder.encode(selectedText ,(String)charset[0]));

				 */
				
				String[] headers = {"count","group","key"};
				String[][] columnRows = new String[map.size()][3];
				int row = 0;
				for (SandboxBasket<String, Integer> key : map.keySet()) {
					columnRows[row][0] = String.valueOf(map.get(key));
					columnRows[row][1] = String.valueOf(key.getValue()); 
//					columnRows[row++][2] = key.getName(); 
//					columnRows[row++][2] = key.getName().replaceAll(System.getProperty("line.separator", "\n"), "{sbx-bk}"); 
					columnRows[row++][2] = key.getName().replaceAll(System.getProperty("line.separator", "\n"), "{sbx-prop line.separator}"); 
				}
				String tooltip = "report";
				boolean sort = true;
				this.tabulate(columnRows, headers, tooltip, sort);
			}
			
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
	}
	

}
