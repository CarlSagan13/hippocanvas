package sandbox.translator;

import sandbox.SandboxException;
import sandbox.SandboxTestBase;
import sandbox.SandboxTranslator;

public class SandboxTranslatorImpl implements SandboxTranslator {
	private boolean verbose = true;

	@Override
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	@Override
	public void translate(Object obj, SandboxTestBase test, String depth) throws SandboxException {
		if (obj instanceof org.bson.types.Binary) {
			org.bson.types.Binary binary = (org.bson.types.Binary) obj;
			test.printLine(binary.getData());
			for (byte b : binary.getData()) {
				test.printIt(b);
				test.printIt(",");
			}
			test.printLine();
		}
	}

	@Override
	public void translate(Object obj, SandboxTestBase test) throws SandboxException {
		translate(obj, test, "");
	}

}
