package sandbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import sandbox.SandboxProcess;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use global process",
 		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME, "procidCommands",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class RunProcess extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] procidCommands = getStringArray("procidCommands");
	
		//Grab any process parameters
//		String processId = procidCommands[0];
		String[] processIdParams = procidCommands[0].split("\t+");
		List<String> processParams = new ArrayList<String> ();
		String processId = Utilities.getTime("hhmmss.S");
		if (processIdParams.length == 1) {
			processId = procidCommands[0];
		} else {
			processId = processIdParams[0];
			for (String param : processIdParams[1].split(",",1)) {
				processParams.add(param);
			}
		}
		
		String[] commands = Arrays.copyOfRange(procidCommands, 1, procidCommands.length);

		if (!procidCommands[0].startsWith(SandboxProcess.PROCESS_ID_PREFIX)) {
			printLine("Process ID must start with '" + SandboxProcess.PROCESS_ID_PREFIX + "'");
			return;
		}
		SandboxProcess sandboxProcess = this.getGlobalMapThing(processId, SandboxProcess.class);
		if (sandboxProcess != null && !sandboxProcess.done) {
			printLine("Process " + processId + " is in use: " + sandboxProcess.history.get(0));
			return;
		}
		if (sandboxProcess == null) {
			printLine("Creating new process");
			String strProcessStart = "/bin/bash";
			if (sandbox.Utilities.isWindows()) {
				strProcessStart = "cmd";
			}

			ProcessBuilder builder = new ProcessBuilder(strProcessStart);
			builder.directory(new File(parameters.getConfigurationDirectory()));
			sandboxProcess = new SandboxProcess(builder.start(), this);
			sandboxProcess.globalMapId = processId;
			this.addToGlobalMap(processId, sandboxProcess);
		} else {
			sandboxProcess.setTest(this);
		}

//		this.addConnection(sandboxProcess);

		printIt(SandboxParameters.VISUAL_SEPARATOR);
		printLine(sandboxProcess.getHost() + " - " + sandboxProcess.getWorkspace() + " - " + sandboxProcess.getUser());
		List<String> verfiedWorkspaces = new ArrayList<String> (); //DMH

		for (String command : commands) {

			boolean capturePid = false;
			String pidEnvName = "unknown";
			List<String> arguments = new ArrayList<String> ();
			String[] commandArguments = command.split("\t+?");
			if (commandArguments.length > 1) {
				for (String argument : commandArguments) {
					if (argument.startsWith("capture.pid")) {
						capturePid = true;
						pidEnvName = argument;
					} else if (argument.startsWith("capture.regex")) {
						arguments.add(argument);
					}
				}
				command = commandArguments[0];
			}

			this.setTooltip(command);
			
			if (command.startsWith("sbxedit ")) {
//				String resource = "C:\\Users\\carls\\HOMES\\SANDBOX_HOME_DEV\\sandbox.test\\junk.txt";
				String processResource = command.split("\\s+")[1]; // This is the remote file
				String fileName = processResource;
				if (fileName.contains("/") || fileName.contains("\\")) {
					try {
						fileName = processResource.substring(processResource.lastIndexOf('/'));
					} catch(StringIndexOutOfBoundsException sioobe) {
						fileName = processResource.substring(processResource.lastIndexOf('\\'));
					}
				}
				
				String serverName = sandboxProcess.getHost();
				if (serverName != null && serverName.length() > 0) {
					serverName = serverName.replaceAll("[^a-zA-Z0-9]", "");;
				} else {
					serverName = "unknown";
				}
				String pathFiles = params.configurationDirectory + File.separator + "process" + File.separator + processId + File.separator;
				
				File filePathFiles = new File(pathFiles);
				if (!filePathFiles.mkdirs()) {
					printLine("Could not create process directory for local file:\n\t" + pathFiles);
				}
				String editFileName = processResource.replace('\\', '_').replace('/', '_').replace(':', '-');
				
				String editFilePath = pathFiles + serverName + "@" +  editFileName;
				File tempFile = new File(editFilePath); // This is the local SandboxOpen copy of the remote file
				if (!tempFile.exists()) {
					tempFile.createNewFile();
				} else {
					if (!tempFile.canWrite()) {
						printLine("Sandbox edit file is locked:\n\t" + editFilePath);
						return;
					}
				}
				List<String> lines = sandboxProcess.getFileText(processResource);
				lines.remove(0); //Remove the command description line
				printLine("Writing " + lines.size() + " lines to\n\t" + editFilePath);
				try {
					Utilities.saveFileList(editFilePath, lines);
				} catch(IOException ioe) {
					printLine("Sandbox edit temp file might be locked:\n\t" + editFilePath);
					return;
				}
				
//				params.hideMessages();
//				params.setAlertUp(true);
//				int result = JOptionPane.showConfirmDialog(params.getTabbedPane(), "Edit file?");
//				params.setAlertUp(false);
//				params.showMessages();

				boolean edit = false;
				if (sandboxProcess.processCommands == null 
						|| sandboxProcess.processCommands.processPlaceholders == null
						|| sandboxProcess.processCommands.processPlaceholders.size() == 0) {
					printLine("Process commands must be set to allow editing.");
				} else {
					int result = Utilities.showConfirmDialog("Edit file?", params);
					if (result == 0 && sandboxProcess.processCommands.canEdit) {
						edit = true;
					}
					if (lines.size() > 1000) {
						edit = false;
						printLine("File too large to edit: " + lines.size() + " lines");
					}
				}

				SandboxOpen sandboxOpen = new SandboxOpen(params, editFilePath, fileName, sandboxProcess, edit);
//				SandboxOpen sandboxOpen = new SandboxOpen(params, filePath, processResource, sandboxProcess, edit);
				sandboxOpen.setName("Editing " + editFilePath);
				sandboxOpen.start();
				if (edit) {
					printLine("Editing " + processResource);
					sandboxProcess.setDone(false);
					while(!sandboxProcess.done) {
						Thread.sleep(100);
					}
				} else {
					printLine("Printing " + processResource);
				}
			} else if (command.startsWith("sbxprops ")) {
				String systemFilePath = command.split("\\s+")[1]; // This is the remote file
				
				File systemSandboxFile = new File(systemFilePath);
				Properties processProps = new Properties();
				if (systemSandboxFile.exists() && systemSandboxFile.isFile()) {
					FileInputStream fileInputStream;
					try {
						fileInputStream = new FileInputStream(systemSandboxFile);
						processProps.loadFromXML(fileInputStream);
						printLine("Loaded sandbox system configuration file");
						for (Object key : processProps.keySet()) {
							printLine(key);
							printLine("\t" + processProps.get(key));
						}
						printLine(processProps.values());
					} catch (Exception en) {
						printLine("Could not load sandbox system configuration file");
					}
					sandboxProcess.initConfig(processProps);
				} else {
					printLine("Properties file could not be loaded.\n\t" + systemSandboxFile.getPath());
				}

			} else {
				int status = 0;
				
//*///////////////////////////////////////////DMH
				Properties systemProps = System.getProperties();
				boolean commandRefused = false;
				for (Object key : systemProps.keySet()) {
					if (((String) key).startsWith(SandboxTestBase.SANDBOX_CONFIRM_COMMAND_PREFIX)) {
						String regex = URLDecoder.decode((String) systemProps.getProperty((String) key), "US-ASCII");
						if (Utilities.groupMatch(regex, new StringBuffer(command))) {
							int decision = Utilities.showConfirmDialog("Are you sure of the command? " + key, parameters);
							if (decision != 0) {
								String message = "YOU CANCELLED: You didn't like command '" + command + "'\n" + key;
								printLine(message);
								printALine();
								commandRefused = true;
							} else {
								String message = "YOU APPROVED: Consider resetting the confirm:\n" + key;
								params.printLine(message, true, true);
								commandRefused = false;
							}
						}
					}
				}
				if (commandRefused) {
					break;
				}
//*//////////////////////////////////////////////

					
					
				
				if (command.startsWith("sbxconfirm ")) {
					command = command.substring("sbxconfirm ".length());
					int decision = Utilities.showConfirmDialog("CONFIRM: " + command, params);
					if (decision == 1) {
						printLine("Cancelled '" + command + "'");
						continue;
					} else if (decision == 2) {
						printLine("Cancelled all commands");
						break;
					}
				} else if (command.startsWith("sbxcomment ")) {
					printLine("COMMENT: " + command.substring("sbxcomment ".length()));
					continue;
				}

				
				
				if (capturePid) {
					String processCommand = params.properties.getProperty(SandboxProcess.PROCESS_CMD_PROP_NAME);
					String processPidCapture = params.properties.getProperty(SandboxProcess.PROCESS_PID_CAPTURE);
					status = sandboxProcess.runCommand(command, processCommand, processPidCapture, pidEnvName, arguments, null);
				} else {
					status = sandboxProcess.runCommand(command, arguments);
				}
				if (!sandboxProcess.done) {
					sandboxProcess.close();
					this.removeFromGlobalMap(processId);
					throw new SandboxException(processId + " terminated");
				}
				if (status < 0) {
					if (processParams.contains("err.alert")) {
						params.hideMessages();
						params.setAlertUp(true);
						int ret = JOptionPane.showConfirmDialog(null,
					             "Continue?", "[" + command + "] Yes to continue", JOptionPane.YES_NO_OPTION);
						params.setAlertUp(false);
						params.showMessages();
						if (ret == 1) {
							break;
						} else {
							printALine();
						}
					} else {
						break;
					}
				}
			}
			
		}
		printIt(SandboxParameters.VISUAL_SEPARATOR);
//		printLine("Process exit value = " + sandboxProcess.getProc().exitValue());
		int retval = sandboxProcess.updatePromptInfo();
		if (retval != 0) {
			printIt(retval + "::");
		}
		printLine(sandboxProcess.getHost() + " - " + sandboxProcess.getWorkspace() + " - " + sandboxProcess.getUser());
		// END TEST
	}

}
