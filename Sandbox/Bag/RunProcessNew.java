package sandbox;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import sandbox.SandboxProcess;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Use global process",
 		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME, "procidCommands",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class RunProcessNew extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
			String[] procidCommands = getStringArray("procidCommands");
			String processId = procidCommands[0];
			String[] commands = Arrays.copyOfRange(procidCommands, 1, procidCommands.length);
			
			String processCommand = params.properties.getProperty(PROCESS_CMD_PROP_NAME);
			String processPidCapture = params.properties.getProperty(PROCESS_PID_CAPTURE);

			if (!procidCommands[0].startsWith(PROCESS_ID_PREFIX)) {
				printLine("Process ID must start with '" + PROCESS_ID_PREFIX + "'");
				return;
			}
			sandboxProcess = this.getGlobalMapThing(processId, SandboxProcess.class);
			if (sandboxProcess != null && !sandboxProcess.done) {
				printLine("Process " + processId + " is in use: " + sandboxProcess.history.get(0));
				return;
			}
			if (sandboxProcess == null) {
				printLine("Creating new process");
				String strProcessStart = "/bin/bash";
				if (sandbox.Utilities.isWindows()) {
					strProcessStart = "cmd";
				}

				ProcessBuilder builder = new ProcessBuilder(strProcessStart);
				builder.directory(new File(parameters.getConfigurationDirectory()));
				sandboxProcess = new SandboxProcess(builder.start(), this);
				this.addToGlobalMap(processId, sandboxProcess);
			}

			this.addConnection(sandboxProcess);
			
			
			
			printALine();
			for (String command : commands) {
				
				boolean capturePid = true;
				String[] commandArguments = command.split("\t");
				if (commandArguments.length > 1) {
					for (String argument : commandArguments) {
						if (argument.startsWith("do.not.capture.pid")) {
							capturePid = false;
						}
					}
					command = commandArguments[0];
				}
				
				this.setTooltip(command);
				int status = 0;
				if (capturePid) {
					status = runCommand(command, processCommand, processPidCapture);
				} else {
					status = runCommand(command);
				}
				if (!sandboxProcess.done) {
					sandboxProcess.close();
					this.removeFromGlobalMap(processId);
					throw new SandboxException(processId + " terminated");
				}
				if (status < 0) {
					break;
				}
			}
		 //END TEST
	 }

	SandboxProcess sandboxProcess = null;
	protected static String PROCESS_ID_PREFIX = "processid-";
	protected static String PROCESS_CMD_PROP_NAME = "processCommand";
	protected static String PROCESS_PID_CAPTURE = "processPidCapture";

	private int runCommand(String command) throws IOException, InterruptedException, SandboxException {
		return runCommand(command, null, null);
	}
	private int runCommand(String command, String processCommand, String processPidCapture)
			throws IOException, InterruptedException, SandboxException {
		sandboxProcess.commandPid = -1; //guarantee no accidental pid kills
		sandboxProcess.history.add(command);
		sandboxProcess.done = false;
		String line = new String();
		
		if (!Utilities.isWindows()) {
			printLine(": " + command);
		}

		if (processCommand != null && processPidCapture != null) {
			command = processCommand.replaceFirst("sbx.replace.command", command);
			sandboxProcess.capturingPid = true;
		}
		
		sandboxProcess.getProcStdIn().write(command);
		
		System.err.println("Actual command:");
		System.err.println(command);
		
		
		sandboxProcess.getProcStdIn().newLine();
		sandboxProcess.getProcStdIn().write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
		sandboxProcess.getProcStdIn().newLine();
		sandboxProcess.getProcStdIn().flush();

		line = sandboxProcess.getBufferedReader().readLine();
		if (sandboxProcess.capturingPid) {
			String[] groups = Utilities.grabRegex(processPidCapture, new StringBuffer(line));
			if (groups.length > 1) {
				try {
					sandboxProcess.commandPid = Integer.parseInt(groups[1]);
					System.err.println("PID = " + sandboxProcess.commandPid);
				} catch(NumberFormatException nfe) {
					sandboxProcess.capturingPid = false;
				}
			}
			line = sandboxProcess.getBufferedReader().readLine();
		}
		while (isRunning && line != null && !line.contains(SandboxParameters.SANDBOX_END_OF_FILE)) {
			printLine(line);
			line = sandboxProcess.getBufferedReader().readLine();
		}
		if (Utilities.isWindows()) {
			sandboxProcess.getBufferedReader().readLine();
		}

		if (isRunning) {
			sandboxProcess.done = true;
		}
		if (sandboxProcess.getErrorStream().available() > 0) {
			String errorLine = new String();

			printIt("PROCESS ERROR OUT");
			printALine();
			while (isRunning && (sandboxProcess.getErrorStream().available() > 0)
					&& (errorLine = sandboxProcess.getErrorBufferedReader().readLine()) != null) {
				printLine(errorLine);
			}
			return -1;
		}
		return 0;
	}
}
