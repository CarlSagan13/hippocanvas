@echo off
SETLOCAL ENABLEDELAYEDEXPANSION 

SET "filepath=%1"
SET "out_file_path=%1.sbf"

ECHO %filepath%
ECHO %out_file_path%

DEL %out_file_path%

FOR /f "usebackqdelims=" %%a IN ("%filepath%") DO (

	SET "line=%%a"
	SET "line=!line:"=!"
	SET "line=!line:{sbx-quote}="!"
	SET "line=!line:{sbx-tab}=	!"
	SET "line=!line:{sbx-dollar}=$!"
	SET "line=!line:{sbx-single}='!"

	 ECHO !line!
) >>"%out_file_path%"

COPY %out_file_path% %filepath%
GOTO :EOF

