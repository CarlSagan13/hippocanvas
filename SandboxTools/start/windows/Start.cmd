@echo off
set SANDBOX_CLASSPATH=.;lib\sandbox.jar;lib\commons-codec-1.3.jar;lib\log4j-core-2.11.1.jar;lib\log4j-api-2.11.1.jar;lib\commons-httpclient-3.1.jar;lib\commons-lang.jar;lib\jmf.jar;lib\spring.jar;lib\gson-2.6.2.jar;lib\diffutils-1.3.0.jar
set SANDBOX_ENVIRONMENT=-Dsbx.conf.listFont=Dialog_2_12 -Dsbx.conf.listFixedHeight=15 -Dcatalina.base=%1 -Dsbx.conf.visible.row.count=50 
java %SANDBOX_ENVIRONMENT% -cp %SANDBOX_CLASSPATH% sandbox.Start %1
rem pause