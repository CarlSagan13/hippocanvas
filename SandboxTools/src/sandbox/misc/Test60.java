package sandbox.misc;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Create a tomorrow at break of dawn",
 		parameterDescriptions = {
 				"5", "zoneId",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test60 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String zoneId = getString("zoneId");
		 Date now = new Date();
			SimpleDateFormat timeFormat = new SimpleDateFormat("ddMMMyy.HH:mm");
			printLine(timeFormat.format(now));
			
			printLine(TimeZone.getAvailableIDs());
			
			Instant thisInstant = now.toInstant();
			
			TimeZone timeZone = TimeZone.getTimeZone(zoneId);
			
			
			
			ZoneId z = ZoneId.of(zoneId);
			ZonedDateTime zdt = thisInstant.atZone( z );
			LocalDate ld = zdt.toLocalDate();
			zdt = ld.atStartOfDay( z );
			zdt = zdt.plusDays(1);
			zdt = zdt.plusSeconds(1);
			Instant firstSecondOfMorning = zdt.toInstant();
			Date morn = Date.from(firstSecondOfMorning);
			printLine(timeFormat.format(morn));
			
			/**
			 * 			Calendar calendar = Calendar.getInstance();
			Date now = calendar.getTime();
			
			Instant instant = now.toInstant();
			ZoneId z = ZoneId.of(systemProps.getProperty("sbx.zone.id", "America/New_York"));
			ZonedDateTime zdt = instant.atZone( z );
			LocalDate ld = zdt.toLocalDate();
			zdt = ld.atStartOfDay( z );
			zdt = zdt.plusDays(1);
			zdt = zdt.plusMinutes(1);
			instant = zdt.toInstant();
			Date startOfTomorrow = Date.from(instant);
			
			printLine("Now: " + timeFormat.format(now));
			printLine("Start of tomorrow: " + timeFormat.format(startOfTomorrow));

			 */
		 //END TEST
	 }

}
