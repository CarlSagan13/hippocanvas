package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Show color",
 		parameterDescriptions = {

			"12", "colorInHex", 
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "popPadString"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class OldTest23 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String colorInHex = getString("colorInHex");
		javax.swing.text.JTextComponent component = (javax.swing.text.JTextComponent)params.getSelectedTabComponent();
		java.awt.Color color = new java.awt.Color(java.lang.Integer.parseInt(colorInHex, 16)); 
		component.setBackground(color);
		//END TEST
	}
}
