package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.util.Date;

public class YahooCalendarEntry {
	
	private String subject = "";
	private Date startDate = null;
	private Date endDate = null;
	private boolean allDayEvent = true;
	private String description = "";
	
	public YahooCalendarEntry() {
		//Empty constructor
	}
	
	public YahooCalendarEntry(YahooCalendarEntry yce) {
		this.setSubject(yce.getSubject());
		this.setAllDayEvent(yce.isAllDayEvent());
		this.setDescription(yce.getDescription());
		this.setEndDate(yce.getEndDate());
		this.setStartDate(yce.getStartDate());
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public boolean isAllDayEvent() {
		return allDayEvent;
	}
	public void setAllDayEvent(boolean allDayEvent) {
		this.allDayEvent = allDayEvent;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return checkNull(subject) + " : "
		+ checkNull(startDate) + " : "
		+ checkNull(endDate) + " : "
		+ allDayEvent + " : "
		+ checkNull(description);
	}
	
	private String checkNull(Object obj) {
		if (obj == null) {
			return "";
		}
		if (obj.getClass().getName().equals(String.class.getName()))
			return (String)obj;
		else return obj.toString();
	}
}
