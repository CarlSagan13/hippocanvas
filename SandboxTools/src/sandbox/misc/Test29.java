package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

public class Test29 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public Test29() {
		super(SandboxParameters.createSandboxParameters());
		init();
	}

	public Test29(SandboxParameters parameters) {
		super(parameters);
		init();
	}

	public void init() {
		this.setTestName(this.getClass().getSimpleName());
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(1), "name");
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(7), "Comma-delimited items for JComboBox components");
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(11), "componentClassName");
		this.setDescription("Add test component: 1-name 7-cdItems 11-componentClassName");
		this.setNumParams(3);
	}

	public void test(SandboxParameters sandboxParameters) {
		String name = sandboxParameters.getParameterValueAt(1);
		String cdItems = sandboxParameters.getParameterValueAt(7);
		String componentClassName = sandboxParameters.getParameterValueAt(11);
		try {
			Class testClass = Class.forName(componentClassName);
			javax.swing.JComponent component = (javax.swing.JComponent) testClass.newInstance();
			component.setName(name);
			if (component.getClass().getName().equals(javax.swing.JButton.class.getName())) {
				java.awt.Font font = new java.awt.Font("Courier", java.awt.Font.PLAIN, 9); 
				((javax.swing.JButton)component).setFont(font);
				((javax.swing.JButton)component).setText(name);
			}
			if (component.getClass().getName().equals(javax.swing.JLabel.class.getName())) {
				((javax.swing.JLabel)component).setText(name);
			}
			if (component.getClass().getName().equals(javax.swing.JComboBox.class.getName())) {
				String[] items = cdItems.split(",");
				for (int i = 0; i < items.length; i++) {
					((javax.swing.JComboBox)component).addItem(items[i]);
				}
			}
			sandboxParameters.addTestComponent(component);
			sandboxParameters.validateTestComponent();
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + ": "
					+ en.getMessage());
		} finally {
			//put cleanup here
		}
	}
}
