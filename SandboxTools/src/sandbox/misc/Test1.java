package sandbox.misc;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Demonstrate Mockito",
 		parameterDescriptions = {
 				"7", "strings",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test1 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] strings = getStringArray("strings");
		boolean trueFalse = getBoolean("trueFalse");
		
		Iterator iter = mock(Iterator.class);
		when(iter.next()).thenReturn(strings[0]).thenReturn(strings[1]);
		String result = iter.next()+" "+iter.next();
		assertEquals("Hello World", result);
		
		Comparable comparable = mock(Comparable.class);
		when(comparable.compareTo(strings[2])).thenReturn(1);
		assertEquals(1,comparable.compareTo("Test"));

		SandboxTestBase testInterface = mock(SandboxTestBase.class);
		when(testInterface.getTestName()).thenReturn("Test69");
		assert(testInterface.getTestName().equals("Test69"));
		
		Comparable comparable2 = mock(Comparable.class);
		when(comparable2.compareTo(anyInt())).thenReturn(-1);
		assertEquals(-1, comparable2.compareTo(5));

		OutputStream mockOS = mock(OutputStream.class);
		OutputStreamWriter osw = new OutputStreamWriter(mockOS);
		try {
			doThrow(new IOException("Yep, I wanted this to be thrown")).when(mockOS).close();
			osw.close();
		} catch(IOException ioe) {
			printLine(ioe.getMessage());
		}

		OutputStream mockOS1 = mock(OutputStream.class);
		OutputStreamWriter osw1 = new OutputStreamWriter(mockOS1);
		if (!trueFalse) osw1.close();
		verify(mockOS1).close();

		TestMockito testMockito = mock(TestMockito.class);
		when(testMockito.getSomething(anyString())).thenReturn("World");
		assertEquals("World",testMockito.getSomething("What ever dude"));
		//END TEST
	}
	class TestMockito {
		public String getSomething(String input) {
			String output = null;
			return input.split(",")[1];
		}
	}
}
