package sandbox.misc;

import java.io.File;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with hash",
 		parameterDescriptions = {
 				"32", "fileDir",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test80 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String fileDir = getString("fileDir");
		 StringBuffer sb = new StringBuffer(new String(Utilities.getFileBytes(fileDir)));
		 
		 printLine(sb.toString().hashCode());
		 //END TEST
	 }

}
