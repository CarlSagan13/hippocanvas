package sandbox.misc;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Read properties from an XML file",
 		parameterDescriptions = {
 				"6", "systemFilePath",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test14 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String systemFilePath = getString("systemFilePath");
		 
			File systemSandboxFile = new File(systemFilePath);
			Properties properties = new Properties();
			if (systemSandboxFile.exists() && systemSandboxFile.isFile()) {
				FileInputStream fileInputStream;
				fileInputStream = new FileInputStream(systemSandboxFile);
				properties.loadFromXML(fileInputStream);
				printLine("Loaded sandbox system configuration file");
			} else {
				printLine("Isn't a file");
			}

		 //END TEST
	 }

}
