package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Generate random numbers",
 		parameterDescriptions = {

//			"1", "string",
//			SandboxParameters.FOCUSED_NAME, "focusedString",
//			SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
//			SandboxParameters.PASSWORD_NAME, "password",
//			SandboxParameters.POPPAD_NAME, "poppad"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test3 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
//		String string = getString("string");
		
		java.util.Random random = new java.util.Random();
		while (this.isRunning) {
			printLine(random.nextFloat());
			printLine(random.nextInt(10));
			Thread.sleep(1000);
		}
		//END TEST
	}
}
