package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.Graphics;

import javax.swing.JButton;

public class SandboxComboBoxButton2 extends JButton {
	static final long serialVersionUID = 0;
	private String label = "?";
	
	public SandboxComboBoxButton2() {
		this.label = this.getName();
		this.setToolTipText("bogus308");
	}

	public SandboxComboBoxButton2(String label) {
		this.label = label;
	}

/*///////////DMH
//	public void paint(Graphics g) {
//		System.out.println(this.getClass().getName() + ".paint(dmh252)");
//	}
/*//////////////
	public void paint(Graphics g) {
		System.out.println(this.getClass().getName() + ".paint(dmh252)");
		if (label == null) {
			label = "??";
		}
		g.drawString(label, 3, 12);
	}
//*//////////////
	
//	public void paintAll(Graphics g) {
//		System.out.println(this.getClass().getName() + ".paintAll(dmh252)");
//	}
//	
//	public void paintChildren(Graphics g) {
//		System.out.println(this.getClass().getName() + ".paintChildren(dmh252)");
//	}
//	
//	public void paintComponent(Graphics g) {
//		System.out.println(this.getClass().getName() + ".paintComponent(dmh252)");
//	}
	
}
