package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Do a for loop types",
 		parameterDescriptions = {

			"8", "cdItems"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test4 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] saStrings = getStringArray("cdItems");
		java.util.ArrayList<String> alStrings = new java.util.ArrayList<String>(saStrings.length);
		printLine("Iterate through a string array:");
		for (String s : saStrings) {
		    printLine(s);
		    alStrings.add(s);
		}

		printLine("Iterate through a collection:");
		for (java.util.Iterator<String> iter = alStrings.iterator(); iter.hasNext(); ) {
		    String var = iter.next();
		    printLine(var);
		}
		//END TEST
	}
}
