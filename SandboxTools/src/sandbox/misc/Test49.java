package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display string, focused string, scratch pad string, password, and boolean",
 			parameterDescriptions = 
		{
				"22", "dateFormat", "23", "dateString", "24", "newDateFormat"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test49 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String dateFormat = (String)parameterValues.get("dateFormat");
		String dateString = (String)parameterValues.get("dateString");
		String newDateFormat = (String)parameterValues.get("newDateFormat");
		java.util.Date date = new java.util.Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(dateFormat);
		printLine(sdf.format(date));

		java.util.Date newDate = sdf.parse(dateString);
		java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
		printLine(newSdf.format(newDate));
		
		
		
		String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
		// create a Pacific Standard Time time zone
		SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
		
		// set up rules for daylight savings time
		pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
		pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

		// create a GregorianCalendar with the Pacific Daylight time zone
		// and the current date and time
		Calendar calendar = new GregorianCalendar(pdt);
		calendar.setTime(newDate);
		
		printLine("Day of week: " + DayOfWeek.getDay(calendar.get(Calendar.DAY_OF_WEEK) - 1));
		//END TEST
	}
}
