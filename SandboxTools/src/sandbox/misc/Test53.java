package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Format current time",
 		parameterDescriptions = {

			"24", "newDateFormat"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 public class Test53 extends SandboxTestBase {	 
	 @Override	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String newDateFormat = getString("newDateFormat");
		printLine("Hello from " + this.getClass().getSimpleName());
		
		java.util.Date date = new java.util.Date();
		java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
		printLine(newSdf.format(date));
		//END TEST
	}
}
