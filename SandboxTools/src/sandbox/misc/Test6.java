package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display formatted date",
 		parameterDescriptions = {
 				"22", "dateFormat", "23", "dateString", "24", "newDateFormat"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test6 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String dateFormat = (String)parameterValues.get("dateFormat");
		String dateString = (String)parameterValues.get("dateString");
		String newDateFormat = (String)parameterValues.get("newDateFormat");
		java.util.Date date = new java.util.Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(dateFormat);
		printLine(sdf.format(date));

		java.util.Date newDate = sdf.parse(dateString);
		java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
		printLine(newSdf.format(newDate));
		//END TEST
	}
}
