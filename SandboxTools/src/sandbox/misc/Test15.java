package sandbox.misc;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Show available fonts",
 		parameterDescriptions = {
 				"26", "font",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
 				SandboxParameters.POPPAD_NAME, "sampleText",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test15 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String strFont = getString("font");
		 String sampleText = getString("sampleText");
		 
	        Font font = new Font(strFont, Font.BOLD, 20);
	        textArea.setFont(font);
	        textArea.setForeground(Color.BLUE);

		 
		 
//		 this.textArea.setFont(Font.getFont(font).deriveFont((float) 20.0));
		 for (Font afont : GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts()) {
			 printLine(afont.getName());
		 }
		 
		 printALine();
		 printALine();

		 printLine(sampleText);
		 //END TEST
	 }

}
