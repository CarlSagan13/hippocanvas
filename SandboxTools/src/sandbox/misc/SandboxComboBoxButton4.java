package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;

public class SandboxComboBoxButton4 extends BasicArrowButton {
	static final long serialVersionUID = 0;
	private String label = "?";
	
	public SandboxComboBoxButton4(String label) {
		super(BasicArrowButton.WEST);
		this.setToolTipText(label);
	}
}
