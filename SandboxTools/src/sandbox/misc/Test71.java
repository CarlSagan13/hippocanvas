package sandbox.misc;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
	description = "Survey grouped regex founds",
	parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME, "groupedRegex,filePath",
	}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test71 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String groupedRegex = getString("groupedRegex");
		File file = getFileDir("filePath");
		StringBuffer text = new StringBuffer(new String(Utilities.getFileBytes(file)));

		Pattern patternSearch = null;
		patternSearch = Pattern.compile(groupedRegex, Pattern.MULTILINE);

		Map<String, Integer> map = new HashMap<String, Integer>();
		List<String> strings = grabRegexList(patternSearch, text, 0);
		if (strings == null) {
			printLine("Verify regex \"" + groupedRegex + "\" is a grouped expression");
			return;
		}
		for (String string : strings) {
			Integer count = map.get(string);
			if (count != null) {
				map.put(string, ++count);
			} else {
				map.put(string, 1);
			}
		}
		printALine();
		String[] headers = { "count", "group", "word" };
		String[][] columnRows = new String[strings.size()][3];
		int row = 0;
		int entryNum = 0;
		for (String key : strings) {
			columnRows[row][0] = String.valueOf(map.get(key));
			columnRows[row][1] = String.valueOf(entryNum++);
			columnRows[row++][2] = key;
		}
//		for (String key : map.keySet()) {
//			columnRows[row][0] = String.valueOf(map.get(key));
//			columnRows[row][1] = String.valueOf(entryNum++);
//			columnRows[row++][2] = key;
//		}
		String tooltip = "group breakdown";
		boolean sort = true;
		this.tabulate(columnRows, headers, tooltip, sort);
		// END TEST
	}

	public List<String> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<String> returnList = new ArrayList<String>(); //Re22.09.28.16.29.32.840
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length());
			boolean aMatch = matcher.find();
			while (aMatch) {
				returnList.add(0, matcher.group(0).trim());
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
					if (matcher.group(groupIndex).trim().length() > 0) {
						returnList.add(groupIndex, matcher.group(groupIndex).trim());
					}
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			printLine(ise.getMessage(), ise);
		} catch (IndexOutOfBoundsException ioobe) {
			printLine(ioobe.getMessage(), ioobe);
		}
		return returnList;
	}

}
