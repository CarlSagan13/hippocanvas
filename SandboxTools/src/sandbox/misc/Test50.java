package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Show date",
 		parameterDescriptions = {
 				"3", "longDate",
 				"24", "newDateFormat"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test50 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		long longDate = getLong("longDate");
		String newDateFormat = getString("newDateFormat");
		
		if (longDate == 0) {
			printLine("long date must be > 0");
			return;
		}
		while (longDate < 1000000000000.0) {
			printIt("x10 ");
			longDate *= 10;
		}
		printLine();
		
		java.util.Date currentDate = new java.util.Date();
		java.util.Date newDate = new java.util.Date();
		newDate.setTime(longDate);
		java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
		printLine("current: " + currentDate.getTime());
		printLine("current formatted: " + newSdf.format(currentDate));
		printLine("new: " + newDate.getTime());
		printLine("new formatted: " + newSdf.format(newDate));
		//END TEST
	}
}
