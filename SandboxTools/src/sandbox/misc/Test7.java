package sandbox.misc;
import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.util.TimeZone;
import java.util.SimpleTimeZone;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Play with dates",
 		parameterDescriptions = {
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Test7 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST

		// get the supported ids for GMT-08:00 (Pacific Standard Time)
		 String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);

		  // begin output
		 printLine("Current Time");

		 // create a Pacific Standard Time time zone
		 SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);

		 // set up rules for daylight savings time
		 pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
		 pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

		 // create a GregorianCalendar with the Pacific Daylight time zone
		 // and the current date and time
		 Calendar calendar = new GregorianCalendar(pdt);
		 Date trialTime = new Date();
		 calendar.setTime(trialTime);

		 // print out a bunch of interesting things
		 printLine("ERA: " + calendar.get(Calendar.ERA));
		 printLine("YEAR: " + calendar.get(Calendar.YEAR));
		 printLine("MONTH: " + calendar.get(Calendar.MONTH));
		 printLine("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
		 printLine("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
		 printLine("DATE: " + calendar.get(Calendar.DATE));
		 printLine("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
		 printLine("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
		 printLine("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
		 printLine("DAY_OF_WEEK_IN_MONTH: "
		                    + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		 printLine("AM_PM: " + calendar.get(Calendar.AM_PM));
		 printLine("HOUR: " + calendar.get(Calendar.HOUR));
		 printLine("HOUR_OF_DAY: " + calendar.get(Calendar.HOUR_OF_DAY));
		 printLine("MINUTE: " + calendar.get(Calendar.MINUTE));
		 printLine("SECOND: " + calendar.get(Calendar.SECOND));
		 printLine("MILLISECOND: " + calendar.get(Calendar.MILLISECOND));
		 printLine("ZONE_OFFSET: "
		                    + (calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000)));
		 printLine("DST_OFFSET: "
		                    + (calendar.get(Calendar.DST_OFFSET)/(60*60*1000)));

		 printLine("Current Time, with hour reset to 3");
		 calendar.clear(Calendar.HOUR_OF_DAY); // so doesn't override
		 calendar.set(Calendar.HOUR, 3);
		 printLine("ERA: " + calendar.get(Calendar.ERA));
		 printLine("YEAR: " + calendar.get(Calendar.YEAR));
		 printLine("MONTH: " + calendar.get(Calendar.MONTH));
		 printLine("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
		 printLine("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
		 printLine("DATE: " + calendar.get(Calendar.DATE));
		 printLine("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
		 printLine("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
		 printLine("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
		 printLine("DAY_OF_WEEK_IN_MONTH: "
		                    + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		 printLine("AM_PM: " + calendar.get(Calendar.AM_PM));
		 printLine("HOUR: " + calendar.get(Calendar.HOUR));
		 printLine("HOUR_OF_DAY: " + calendar.get(Calendar.HOUR_OF_DAY));
		 printLine("MINUTE: " + calendar.get(Calendar.MINUTE));
		 printLine("SECOND: " + calendar.get(Calendar.SECOND));
		 printLine("MILLISECOND: " + calendar.get(Calendar.MILLISECOND));
		 printLine("ZONE_OFFSET: "
		        + (calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000))); // in hours
		 printLine("DST_OFFSET: "
		        + (calendar.get(Calendar.DST_OFFSET)/(60*60*1000))); // in hours
		 		//END TEST
	}
}
