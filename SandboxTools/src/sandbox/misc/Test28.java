package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class Test28 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public Test28() {
		super(SandboxParameters.createSandboxParameters());
		init();
	}

	public Test28(SandboxParameters parameters) {
		super(parameters);
		init();
	}

	public void init() {
		this.setTestName(this.getClass().getSimpleName());
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(1), "string");
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(11), "Comma-delimited items");
		this.setDescription("Add combobox test component with custom button: 1-buttonLabel 11-cdItems");
		this.setNumParams(2);
	}

	public void test(SandboxParameters sandboxParameters) {
		String buttonLabel = sandboxParameters.getParameterValueAt(1);
		String cdItems = sandboxParameters.getParameterValueAt(11);
		try {
			String[] items = cdItems.split(",");
		    JComboBox comboBox = new JComboBox(items);
		    comboBox.setName(buttonLabel);
		    ComboBoxUI comboBoxUI = (ComboBoxUI)SandboxComboBoxUI.createUI(comboBox);
		    comboBox.setUI((ComboBoxUI)comboBoxUI);
		    sandboxParameters.addTestComponent(comboBox);
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + ": "
					+ en.getMessage());
		} finally {
			//put cleanup here
		}
	}
}
