package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class Test26 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public Test26() {
		super(SandboxParameters.createSandboxParameters());
		init();
	}

	public Test26(SandboxParameters parameters) {
		super(parameters);
		init();
	}

	public void init() {
		this.setTestName(this.getClass().getSimpleName());
		this.initParameter((javax.swing.JComponent)parameters.getParameterComponentAt(1), "string");
		this.setDescription("Show combobox with icon button");
		this.setNumParams(1);
	}

	public void test(SandboxParameters sandboxParameters) {
		String string = sandboxParameters.getParameterValueAt(1);
		try {
		    String labels[] = { "A", "B", "C", "D", "E", "F", "G" };
		    JFrame frame = new JFrame("Popup JComboBox");
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		    JComboBox comboBox = new JComboBox(labels);
		    
//*/////////////DMH
		    ComboBoxUI comboBoxUI = (ComboBoxUI)SandboxComboBoxUI.createUI(comboBox);
		    comboBox.setUI((ComboBoxUI)comboBoxUI);
/*////////////////
		    ComboBoxUI comboBoxUI = (ComboBoxUI) SandboxComboBoxUI.createUI(comboBox);
		    comboBox.setUI(comboBoxUI);
//*////////////////
		    
		    
		    
		    frame.add(comboBox, BorderLayout.NORTH);

		    frame.setSize(300, 200);
		    frame.setVisible(true);
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + ": "
					+ en.getMessage());
		} finally {
			//put cleanup here
		}
	}
}
