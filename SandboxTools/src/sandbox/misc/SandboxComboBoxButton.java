package sandbox.misc;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

import java.awt.Graphics;

import javax.swing.JButton;

public class SandboxComboBoxButton extends JButton {
	static final long serialVersionUID = 0;
	private String buttonLabel = "?";
	
	public SandboxComboBoxButton() {
		this.buttonLabel = this.getText();
	}
	
	public void setText(String text) {
		super.setText(text);
		this.buttonLabel = text;
	}
	public void paint(Graphics g) {
		g.drawString(this.buttonLabel, 3, 12);
	}
}
