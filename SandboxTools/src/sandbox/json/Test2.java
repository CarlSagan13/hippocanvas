package sandbox.json;

import com.google.gson.Gson;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "1.1. Converting JSON Array to Array of Objects",
 		parameterDescriptions = {
 				SandboxParameters.POPPAD_NAME, "userJson",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test2 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		String userJson = getString("userJson");

//		String userJson = "[{'name': 'Alex','id': 1}, " + "{'name': 'Brian','id':2}, " + "{'name': 'Charles','id': 3}]";

		Gson gson = new Gson();

		User[] userArray = gson.fromJson(userJson, User[].class);

		for (User user : userArray) {
			printLine(user);
		}
		// END TEST
	 }
	 
	class User {
		private long id;
		private String name;

		protected long getId() {
			return id;
		}

		protected void setId(long id) {
			this.id = id;
		}

		protected String getName() {
			return name;
		}

		protected void setName(String name) {
			this.name = name;
		}
		
		public String toString() {
			return "id=" + String.valueOf(id) + " : name=" + name;
		}
	}

}
