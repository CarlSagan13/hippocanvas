package sandbox.json;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Find json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "fileRegex,valueRegex,directory",
 				SandboxParameters.TRUEFALSE_NAME, "recurse"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test18 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String fileRegex = getString("fileRegex");
		 String valueRegex = getString("valueRegex");
		File directory = getFileDir("directory");
		boolean recurse = getBoolean("recurse");

		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
		Pattern valuePattern = Pattern.compile(valueRegex, Pattern.MULTILINE);

//		List<List<String>> returnPaths = new ArrayList<List<String>>();
		for (File file : Utilities.listFilesRegex(directory, recurse, false, pattern, this)){
			if (file.isFile()) {
				 JsonElement jsonTree = this.getGlobalMapThing(file.getPath(), JsonElement.class);
				 if (jsonTree == null) {
					 printLine("Getting json from file:");
					 printLine("\t" + file.getPath());
					 StringBuffer json = new StringBuffer(new String(Utilities.getFileBytes(file)));

						JsonParser parser = new JsonParser();

						JsonParser jp = new JsonParser();
						JsonElement je = jp.parse(json.toString());

						if (je.isJsonArray()) {
							json = new StringBuffer("{\"array\": " + json + "}");
						}
						jsonTree = parser.parse(json.toString());
//remove for final version						this.addToGlobalMap(file.getPath(), jsonTree);
				 } else {
					 printLine("Getting json from global map:");
					 printLine("\t" + file.getPath());
				 }
				 

				if (jsonTree.isJsonObject()) {
					JsonObject jsonObject = jsonTree.getAsJsonObject();
					try {
						printLine(stepIn(jsonObject, valuePattern, ""));
					} catch(NullPointerException npe) {
						printLine("Invalid path:");
						return;
					}
				} else {
					printLine("NOT JSON!");
				}

				
			}
			checkRunning();
		}

		 //END TEST
	 }

//*////////////////////////////DMH
	private boolean stepIn(JsonObject job, Pattern valuePattern, String path) throws NullPointerException {
		for (Entry<String, JsonElement> entry : job.entrySet()) {
			if (entry.getValue().isJsonPrimitive()) {
				printLine(path + "." + entry.getKey() + " :: " + entry.getValue());
			} else if (entry.getValue().isJsonObject()) {
				stepIn(entry.getValue().getAsJsonObject(), valuePattern, path + "." + entry.getKey() );
			} else if (entry.getValue().isJsonArray()) {
				for (JsonElement element : entry.getValue().getAsJsonArray()) {
					JsonObject jo = element.getAsJsonObject();
					for(Entry<String,JsonElement> subentry : jo.entrySet()) {
						printLine(subentry.getKey() + " :1: " + subentry.getValue());
//						if (subentry.getValue().isJsonPrimitive()) {
//							printLine(path + "." + subentry.getKey() + " :: " + subentry.getValue());
//						} else {
//							stepIn(subentry.getValue().getAsJsonObject(), valuePattern, path + "." + subentry.getKey());
//						}
					}					
					
					
					
//					printLine("element.isJsonArray() = " + element.isJsonArray());
//					printLine("element.isJsonObject() = " + element.isJsonObject());
//					printLine("element.isJsonPrimitive() = " + element.isJsonPrimitive());
					
					
					
//					if (element.isJsonObject()) {
//						stepIn(element.getAsJsonObject(), valuePattern, path + "." + entry.getKey());
//					} else if (element.isJsonArray()) {
//						printLine("It's an array");
//					}

					
//					JsonObject jo = element.getAsJsonObject();
//					for(Entry<String,JsonElement> subentry : jo.entrySet()) {
//						printLine("subentry.getValue().isJsonArray() = " + subentry.getValue().isJsonArray());
//						printLine("subentry.getValue().isJsonObject() = " + subentry.getValue().isJsonObject());
//						printLine("subentry.getValue().isJsonPrimitive() = " + subentry.getValue().isJsonPrimitive());
//						printLine(subentry.getKey() + " :: " + subentry.getValue());
//					}
					
					
					
					
//					stepIn(element.getAsJsonObject(), valuePattern, path + "." + entry.getKey());
//					printLine("BLORG " + element);
				}
			}
		}
		return true;
	}
/*///////////////////////////////
	private boolean stepIn(JsonObject job, Pattern valuePattern) throws NullPointerException {
		if (job.isJsonPrimitive()) {
			printLine(job.toString());
			return true;
		} else if (job.isJsonObject()) {
			for (Entry<String, JsonElement> entry : job.getAsJsonObject().entrySet()) {
				if (entry.getValue().isJsonPrimitive()) {
					printLine(entry.getKey() + ":" + entry.getValue());
				} else if (entry.getValue().isJsonArray()) {
					stepIn(entry.getValue().getAsJsonObject(), valuePattern);
				} else if (entry.getValue().isJsonObject()) {
					stepIn(entry.getValue().getAsJsonObject(), valuePattern);
				} else if (job.isJsonArray()) {
					for (JsonElement element : job.getAsJsonArray()) {
						stepIn(element.getAsJsonObject(), valuePattern);
					}
				}
			}
		}
		return false;
	}
//*///////////////////////////////

}
