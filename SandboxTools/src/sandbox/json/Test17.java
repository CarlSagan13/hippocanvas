package sandbox.json;

import java.io.File;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Get json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "fileRegex,path,directory",
 				SandboxParameters.TRUEFALSE_NAME, "recurse",
 				SandboxParameters.YESNO_NAME, "fromFile"

 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test17 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String fileRegex = getString("fileRegex");
		String[] path = getStringArray("path", "\\.");
		File directory = getFileDir("directory");
		boolean recurse = getBoolean("recurse");
		boolean fromFile = getBoolean("fromFile");

		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);

		for (File file : Utilities.listFilesRegex(directory, recurse, false, pattern, this)){
			if (file.isFile()) {
				 JsonElement jsonTree = this.getGlobalMapThing(file.getPath(), JsonElement.class);
				 if (jsonTree == null || fromFile) {
					 printLine("Getting json from file:");
					 printLine("\t" + file.getPath());
					 StringBuffer json = new StringBuffer(new String(Utilities.getFileBytes(file)));

						JsonParser parser = new JsonParser();

						JsonParser jp = new JsonParser();
						JsonElement je = jp.parse(json.toString());

						if (je.isJsonArray()) {
							json = new StringBuffer("{\"array\": " + json + "}");
						}
						jsonTree = parser.parse(json.toString());
						this.addToGlobalMap(file.getPath(), jsonTree);
				 } else {
					 printLine("Getting json from global map:");
					 printLine("\t" + file.getPath());
				 }
				 

				if (jsonTree.isJsonObject()) {
					JsonObject jsonObject = jsonTree.getAsJsonObject();
					try {
						int pathIndex = 0;
						if (path[0] == null || path[0].isEmpty()) {
							pathIndex = 1; //allow for '.' prefix
						}
						printLine(stepIn(jsonObject, path, pathIndex));
					} catch(NullPointerException npe) {
						printLine("Invalid path:");
						printLine(path);
						return;
					}
				} else {
					printLine("NOT JSON!");
				}

			}
			checkRunning();
		}

		 //END TEST
	 }
	 
	 private JsonElement stepIn(JsonObject job, String[] path, int pathIndex) 
	 	throws NullPointerException {
		if (pathIndex >= path.length) {
			return job;
		}

		JsonElement je = null;
		if (path[pathIndex].matches(".*\\[\\d+?\\].*")) {
			int arrayIndex = Integer.parseInt(
					path[pathIndex].substring(path[pathIndex].indexOf('[') + 1, path[pathIndex].length() - 1));
			String arrayName = path[pathIndex].substring(0, path[pathIndex].indexOf('['));
			je = job.get(arrayName);
			JsonArray ja = je.getAsJsonArray();
			
			try {
				je = ja.get(arrayIndex);
			} catch(IndexOutOfBoundsException ioobe) {
				printLine(arrayName + "[" + (ja.size() - 1) + "]");
				je = ja.get(ja.size() - 1);
			}
			
			if (je != null && !je.isJsonPrimitive()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		} else {
			je = job.get(path[pathIndex]);
			
			if (je != null && !je.isJsonPrimitive() && je.isJsonArray()) {
				JsonArray ja = je.getAsJsonArray();
				printLine(path[pathIndex] + ": " + ja.size());
			}
			
			if (je != null && !je.isJsonPrimitive() && !je.isJsonArray()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		}
		return je;
	 }

}
