package sandbox.json;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.UtilitiesTools;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Recurse into json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.POPPAD_NAME, "json",
 				"10", "path"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test14 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		String json = getString("json");
		String[] path = getStringArray("path", "\\.");
		
		printLine(UtilitiesTools.getFromJson(path, json));
		 //END TEST
	 }
}
