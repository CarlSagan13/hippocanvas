package sandbox.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Recurse into json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "path,jsonFilePath",
 				SandboxParameters.TRUEFALSE_NAME, "fromFile"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test16 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 boolean fromFile = getBoolean("fromFile");
		 String jsonFilePath = getString("jsonFilePath");
		String[] path = getStringArray("path", "\\.");
		 JsonElement jsonTree = this.getGlobalMapThing(jsonFilePath, JsonElement.class);
		 if (jsonTree == null || fromFile) {
			 printLine("Getting json from file");
				String json = Utilities.getFileString(jsonFilePath);

				JsonParser parser = new JsonParser();

				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(json);

				if (je.isJsonArray()) {
					json = "{\"array\": " + json + "}";
				}
				jsonTree = parser.parse(json);
				this.addToGlobalMap(jsonFilePath, jsonTree);
		 }
		 

		if (jsonTree.isJsonObject()) {
			JsonObject jsonObject = jsonTree.getAsJsonObject();
			try {
				int pathIndex = 0;
				if (path[0] == null || path[0].isEmpty()) {
					pathIndex = 1; //allow for '.' prefix
				}
				printLine(stepIn(jsonObject, path, pathIndex));
			} catch(NullPointerException npe) {
				printLine("Invalid path:");
				printLine(path);
				return;
			}
		} else {
			printLine("NOT JSON!");
		}
		 //END TEST
	 }
	 
	 private JsonElement stepIn(JsonObject job, String[] path, int pathIndex) 
	 	throws NullPointerException {
		if (pathIndex >= path.length) {
			return job;
		}

		JsonElement je = null;
		if (path[pathIndex].matches(".*\\[\\d+?\\].*")) {
			int arrayIndex = Integer.parseInt(
					path[pathIndex].substring(path[pathIndex].indexOf('[') + 1, path[pathIndex].length() - 1));
			String arrayName = path[pathIndex].substring(0, path[pathIndex].indexOf('['));
			je = job.get(arrayName);
			JsonArray ja = je.getAsJsonArray();
			
			try {
				je = ja.get(arrayIndex);
			} catch(IndexOutOfBoundsException ioobe) {
				printLine(arrayName + "[" + (ja.size() - 1) + "]");
				je = ja.get(ja.size() - 1);
			}
			
			if (je != null && !je.isJsonPrimitive()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		} else {
			je = job.get(path[pathIndex]);
			
			if (je != null && !je.isJsonPrimitive() && je.isJsonArray()) {
				JsonArray ja = je.getAsJsonArray();
				printLine(path[pathIndex] + ": " + ja.size());
			}
			
			if (je != null && !je.isJsonPrimitive() && !je.isJsonArray()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		}
		return je;
	 }

}
