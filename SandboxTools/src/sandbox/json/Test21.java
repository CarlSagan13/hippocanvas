package sandbox.json;

import java.io.File;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.Utilities;

@SandboxAnnotation(
		description = "Find json element/array values",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "fileRegex,valueRegex,directory",
 				SandboxParameters.TRUEFALSE_NAME, "recurse"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test21 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String fileRegex = getString("fileRegex");
		 String valueRegex = getString("valueRegex");
		File directory = getFileDir("directory");
		boolean recurse = getBoolean("recurse");

		if (directory.isFile()) {
			printLine(directory.getName() + " is a file");
			return;
		}
		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
		Pattern valuePattern = Pattern.compile(valueRegex, Pattern.MULTILINE);

		for (File file : Utilities.listFilesRegex(directory, recurse, false, pattern, this)){
			if (file.isFile()) {
				 JsonElement jsonTree = this.getGlobalMapThing(file.getPath(), JsonElement.class);
				 if (jsonTree == null) {
					 printLine("Getting json from file:");
					 printLine("\t" + file.getPath());
					 StringBuffer json = new StringBuffer(new String(Utilities.getFileBytes(file)));

						JsonParser parser = new JsonParser();

						JsonParser jp = new JsonParser();
						JsonElement je = jp.parse(json.toString());

						if (je.isJsonArray()) {
							json = new StringBuffer("{\"array\": " + json + "}");
						}
						jsonTree = parser.parse(json.toString());
						this.addToGlobalMap(file.getPath(), jsonTree);
				 } else {
					 printLine("Getting json from global map:");
					 printLine("\t" + file.getPath());
				 }
				 

				if (jsonTree.isJsonObject()) {
					JsonObject jsonObject = jsonTree.getAsJsonObject();
					try {
						printLine(stepIn(jsonObject, valuePattern, ""));
					} catch(NullPointerException npe) {
						printLine("Invalid path:");
						return;
					}
				} else {
					printLine("NOT JSON!");
				}
			}
			checkRunning();
		}

		 //END TEST
	 }
	
	private boolean stepIn(JsonElement jel, Pattern valuePattern, String path) throws NullPointerException {
		if(jel.isJsonPrimitive()) {
			if (jel.getAsString().matches(valuePattern.pattern())) {
				printLine(path);
				printLine("\t" + jel.getAsString());
			}
		} else if(jel.isJsonObject()) {
			for (Entry<String, JsonElement> entry : jel.getAsJsonObject().entrySet()) {
				stepIn(entry.getValue(), valuePattern, path + "." + entry.getKey());
			}
		} else if(jel.isJsonArray()) {
			int index = 0;
			for (JsonElement element : jel.getAsJsonArray()) {
				stepIn(element, valuePattern, path + "[" + index++ + "]");
			}
		}
		return true;
	}
	
}
