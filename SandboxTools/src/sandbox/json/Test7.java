package sandbox.json;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "practicing recursion",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "string",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test7 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 int[] myArray = new int[string.length()];
		 printLine(recurseMe(myArray, 0, string));
		 //END TEST
	 }
	 
	 private int[] recurseMe(int[] theIntArray, int index, String string) {
		 if (index < string.length()) {
			 char letter = string.charAt(index);
			 theIntArray[index++] = letter;
			 recurseMe(theIntArray, index, string);
		 }
		 return theIntArray;
	 }

}
