package sandbox.json;

public class Employee implements java.io.Serializable
{
   public String name;
   public transient String address;
   public int SSN;
   public String[] creditCards;
   public static int number;
   public void mailCheck()
   {
      System.out.println("Mailing a check to " + name
                           + " " + address);
   }
   @Override
   public String toString() {
	   StringBuffer sb = new StringBuffer();
		sb.append("Name: " + name).append('\n');
		sb.append("Address: " + address).append('\n');
		sb.append("SSN: " + SSN).append('\n');
		sb.append("Number: " + number).append('\n');
		sb.append("creditCards:").append('\n');
		for (String card : creditCards) {
			sb.append('\t').append(card).append('\n');
		}
		return sb.toString();
   }
   
}