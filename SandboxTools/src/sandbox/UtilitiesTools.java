package sandbox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.sql.DatabaseMetaData;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import difflib.ChangeDelta;
import difflib.Chunk;
import difflib.DeleteDelta;
import difflib.Delta;
import difflib.DiffUtils;
import difflib.InsertDelta;
import difflib.Patch;

class DateComparator<T> implements Comparator<String> {
	private String dateFormat = SandboxParameters.DATE_FORMAT;

	public DateComparator(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	public int compare(String a, String b) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try {
			Date dateA = sdf.parse(a);
			Date dateB = sdf.parse(b);
			return dateB.compareTo(dateA);
		} catch (ParseException e) {
			return -1;
		}
	}
}

class TestBaseTableModel extends DefaultTableModel implements SandboxCommon {
	private static final long serialVersionUID = 1L;
	private int numCols = 0;

	public TestBaseTableModel(Vector dataRows, Vector headers) {
		super(dataRows, headers);
	}
	
	public TestBaseTableModel(String[][] dataRows, String[] headers) {
		super(dataRows, headers);
		this.numCols = headers.length;
		this.addColumn("sbr");
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (col == this.numCols) {
			return String.valueOf(row);
		}
		try {
			return ((String) super.getValueAt(row, col)).replaceAll(SandboxParameters.SANDBOX_COMMA_DELIMITER, ",");
		} catch (Exception en) {
			sandboxLogger.log(Level.INFO, "Could not get value from table", en);
			return "";
		}
	}
}


public class UtilitiesTools implements SandboxLogging {
	public static class PlaySoundThread extends Thread {
		private InputStream inputStream;
		private SandboxParameters params;

		public PlaySoundThread(InputStream inputstream, SandboxParameters params) {
			this.inputStream = inputstream;
			this.params = params;
		}

		@Override
		public void run() {
			try {
				AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputStream);
				AudioFormat audioFormat = audioInputStream.getFormat();

				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
				try {
					SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);

					line.open(audioFormat);
					line.start();

					int nBytesRead = 0;
					byte[] abData = new byte[128000];
					while (nBytesRead != -1) {
						nBytesRead = audioInputStream.read(abData, 0, abData.length);
						if (nBytesRead >= 0) {
							line.write(abData, 0, nBytesRead);
						}
					}
					line.drain();
					line.close();
				} catch (LineUnavailableException lue) {
					params.printLine("Can't play sound file", true, true);
				}
			} catch (IOException ioe) {
				params.printLine("Sound file not found", true, true);
			} catch (UnsupportedAudioFileException uafe) {
				params.printLine("Not a supported audio file format ", true, true);
			} catch (Exception en) {
				params.printLine("Sound exception", true, true);
			}
		}
	}

	private static int[] lastRange = { 0, 0 };
	static String GRAB_REPLACE = "replace:";
	protected static String TAB_LENGTH_MAX = "sbx.conf.tabLength";

	
//*//////////////////////////////////////////////////////////////////////DMH
    public static ImageIcon resizeIcon(File src, int size) {
        int newHeight = size, newWidth = size; // Variables for the new height and width
        int priorHeight = 0, priorWidth = 0;
        BufferedImage image = null;
        ImageIcon sizeImage; // Need to return an imageicon for JLabel

        try {//w  w w  .  j a  v a2s. co m
            image = ImageIO.read(src); // read the image
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane
                    .showMessageDialog(null, "Image could not be found!");
        }
        //set icon to image
        sizeImage = new ImageIcon(image);
        //set old dimensions
        if (sizeImage != null) {
            priorHeight = sizeImage.getIconHeight();
            priorWidth = sizeImage.getIconWidth();
        }

        //Create a new Buffered Image and Graphic2D object
        //Notice I am using ABGR and not RGB, alpha channel must be used for transparency
        BufferedImage resizedImg = new BufferedImage(newWidth, newHeight,
                BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2 = resizedImg.createGraphics();

        //This was to rotate the image of wire 45 degrees(method takes radians so 45 * pi/180 = .785
        //It works just fine but the layout manager is clipping it and only half is showing. We can fix later or throw it out. Not a big priority issue obviously
        if (src.toString().contains("WIRE")) {
            //g2.rotate(.785, 10, 0);
        }
        //Use the Graphic object to draw a new image to the image in the buffer
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(image, 0, 0, newWidth, newHeight, null);
        //need to dispose after creating Graphics instance
        g2.dispose();

        // Convert the buffered image into an ImageIcon for return
        return (new ImageIcon(resizedImg));
    }
//*/////////////////////////////////////////////////////////////////////////
	
	
	
	
	// Create text undo mechanism
	public static UndoManager addUndo(JTextComponent component) {
		final UndoManager undoManager = new UndoManager();
		undoManager.setLimit(1000);
		Document doc = component.getDocument();
		doc.addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				// TODO Auto-generated method stub
				undoManager.addEdit(e.getEdit());
			}

		});

		InputMap scratchPadInputMap = component.getInputMap(JComponent.WHEN_FOCUSED);
		ActionMap scratchPadActionMap = component.getActionMap();

		try {
			scratchPadInputMap.put(
					KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
					"Undo");
			scratchPadInputMap.put(
					KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
					"Redo");

			scratchPadActionMap.put("Undo", new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					try {
						if (undoManager.canUndo()) {
							undoManager.undo();
						}
					} catch (CannotUndoException cue) {
						sandboxLogger.info(cue.getMessage());
					}
				}

			});
			scratchPadActionMap.put("Redo", new AbstractAction() {
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					try {
						if (undoManager.canRedo()) {
							undoManager.redo();
						}
					} catch (CannotUndoException cue) {
						sandboxLogger.info(cue.getMessage());
					}
				}
			});

		} catch (HeadlessException he) {
			sandboxLogger.log(Level.FINE, "Could not implement undo", he);
			return null;

		}
		return undoManager;
	}

	public static String base64Decode(String encoded) {
		Base64 base64 = new Base64();
		return new String(base64.decode(encoded.getBytes())).trim();
	}

	public static String base64Encode(File file) throws FileNotFoundException, IOException {
		return base64Encode(UtilitiesTools.getFileString(file.getAbsolutePath()));
	}

	public static Object base64Encode(Object obj) {
		Base64 base64 = new Base64();
		try {
			return base64.encode(obj);
		} catch (EncoderException ee) {
			System.out.println(ee.getClass().getName() + ": message= " + ee.getMessage());
			ee.printStackTrace();
			return null;
		}
	}

	public static String base64Encode(String unencoded) {
		Base64 base64 = new Base64();
		return new String(base64.encode(unencoded.getBytes())).trim();

	}

	/**
	 * Convert a byte array into a string representation of HEX
	 *
	 * @param bytes
	 *            byte[]bytes to convert
	 * @return String bytes as HEX string
	 */
	public static final String bytesToHex(byte[] bytes) {
		return UtilitiesTools.bytesToHex(bytes, "");
	}
	
	public static final String bytesToHex(byte[] bytes, String delim) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			int n = Integer.parseInt(Byte.toString(bytes[i])) & 0xff;
			String hex = (n < 16 ? "0" + Integer.toHexString(n) : Integer.toHexString(n));
			buffer.append(hex);
			buffer.append(delim);
		}
		return buffer.toString();
	}

	public static List<String> closeConnections(Set<Object> connections) {
		int connectionsClosed = 0;
		List<String> messages = new ArrayList<String>();
		if (connections == null) {
			messages.add("Connections were null");
			return messages;
		}
		try {// Close connections if any

			Iterator<Object> iterConnections = connections.iterator();
			while (iterConnections.hasNext()) {
				Boolean isConnected = true;
				Object obj = iterConnections.next();
				Object[] parameters = new Object[1];
				parameters[0] = Boolean.TRUE;

				Method[] methods = obj.getClass().getDeclaredMethods();

				for (Method method : methods) {
					DatabaseMetaData metaData = null;
					if (method.getName().equals("getMetaData")) {
						try {
							metaData = (DatabaseMetaData) method.invoke(obj, (Object[]) null);
						} catch (Exception en) {
							messages.add(
									"Could not call " + obj.getClass().getName() + "." + method.getName() + "()...");
						}
					}
					if (method.getName().equals("isConnected")) {
						try {
							isConnected = (Boolean) method.invoke(obj, (Object[]) null);
						} catch (Exception en) {
							messages.add(
									"Could not call " + obj.getClass().getName() + "." + method.getName() + "()...");
						}
					}
					if (method.getName().equals("close") || method.getName().equals("disconnect")) {
						if (isConnected) {
							String identifier = obj.getClass().getName();
							try {
								if (metaData != null) {
									identifier = metaData.getURL();
								}
								messages.add("Closing " + identifier + " connection with "
										+ method.getParameterTypes().length + " parameters...");
								if (method.getParameterTypes().length == 0) {
									method.invoke(obj, (Object[]) null);
									isConnected = false;
								} else {
									method.invoke(obj, parameters);
									isConnected = false;
								}
								messages.add("success!");
								connectionsClosed++;
							} catch (Exception en) {
								messages.add("Could not close method with " + method.getParameterTypes().length
										+ " parameters for " + identifier + " in map...");
							}
						}
					}
				}
				obj = null;
			}
		} catch (Exception en) {
			messages.add(en.getClass().getSimpleName() + ": Connections/Readers closing interrupted.");
		}
		messages.add("Number of connections closed: " + connectionsClosed);

		return messages;
	}

	public static final String colorToString(Color c) {
		String s = Integer.toHexString(c.getRGB() & 0xffffff);
		if (s.length() < 6) {
			s = "000000".substring(0, 6 - s.length()) + s;

		}
		return s;
	}

	/**
	 * Return a report of string differences
	 * 
	 * @param beforeChange
	 * @param afterChange
	 * @return
	 */
	public static String compare(String beforeChange, String afterChange) {
		StringBuffer report = new StringBuffer();
		List<String> clipboardList = new ArrayList<String>();
		for (String line : beforeChange.split("\n")) {
			clipboardList.add(line);
		}
		List<String> selectedList = new ArrayList<String>();
		for (String line : afterChange.split("\n")) {
			selectedList.add(line);
		}
		Patch<String> patch = DiffUtils.diff(clipboardList, selectedList);
		for (Delta<String> delta : patch.getDeltas()) {
			if (delta instanceof InsertDelta) {
				Chunk<String> revised = delta.getRevised();
				report.append("INSERTED " + revised.getLines().size() + " lines at " + revised.getPosition())
						.append("\n");

			} else if (delta instanceof ChangeDelta) {
				Chunk<String> originals = delta.getOriginal();
				report.append("CHANGED:").append("\n");
				report.append("\tORIGINAL " + originals.getLines().size() + " lines at " + originals.getPosition());
				for (String original : originals.getLines()) {
					report.append("\t\t" + original);
				}
				Chunk<String> revised = delta.getRevised();
				report.append("\tREVISED " + revised.getLines().size() + " lines at " + revised.getPosition());
				for (String revision : revised.getLines()) {
					report.append("\t\t" + revision);
				}
			} else if (delta instanceof DeleteDelta) {
				Chunk<String> originals = delta.getOriginal();
				report.append("DELETED(" + originals.getLines().size() + " lines at " + originals.getPosition() + ")");
				for (String original : originals.getLines()) {
					report.append("\t" + original);
				}
			} else {
				report.append(delta);
			}
		}
		return null;
	}

//*//////////////////////////////////////////////////////////////////////////////////DMH
	public static void copyDirectory(File sourceLocation, File targetLocation, Date start, Date end,
			Set<String> extensions, Set<File> filteredFiles, SandboxTestBase test) throws IOException {
		if (sourceLocation.isDirectory()) {

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]), start, end,
						extensions, filteredFiles, test);
			}
		} else {// It's a file!
			String extension = null;
			if (sourceLocation.getName().indexOf('.') >= 0) {
				extension = sourceLocation.getName().substring(sourceLocation.getName().lastIndexOf('.'));
			}
			if (extensions == null || extensions.size() == 0) {// DON'T filter
																// by extension

				if (start == null && end == null) {// DON'T filter by date range
					if (filteredFiles != null) {
						filteredFiles.add(sourceLocation);
					} else {
						saveFile(sourceLocation, targetLocation, test);
					}
				} else {// Filter by date range
					if (start.getTime() < sourceLocation.lastModified()
							&& sourceLocation.lastModified() < end.getTime()) {
						if (filteredFiles != null) {

							filteredFiles.add(sourceLocation);
						} else {
							saveFile(sourceLocation, targetLocation, test);
						}
					}
				}
			} else if (extensions.contains(extension)) {// Filter by extension
				if (start == null && end == null) {// DON'T filter by data range
					if (extensions.contains(extension)) {
						if (filteredFiles != null) {
							filteredFiles.add(sourceLocation);
						} else {
							saveFile(sourceLocation, targetLocation, test);
						}
					}
				} else {// Filter by date range
					if (start.getTime() < sourceLocation.lastModified()
							&& sourceLocation.lastModified() < end.getTime()) {
						if (filteredFiles != null) {
							filteredFiles.add(sourceLocation);
						} else {
							saveFile(sourceLocation, targetLocation, test);
						}
					}
				}
			}
		}
	}
//*/////////////////////////////////////////////////////////////////////////////////////
	public static void copyDirectory(File sourceLocation, File targetLocation, Date start, Date end,
			Pattern pattern, Set<File> filteredFiles, SandboxTestBase test, boolean copy) throws IOException {
		if (sourceLocation.isDirectory()) {
			Collection<File> files = sandbox.UtilitiesTools.listFilesRegex(sourceLocation, false, false,  pattern, test);
			for (File file : files) {
				if (file.isFile()) {
					File targetFilePath = new File(targetLocation.getPath() + File.separator + file.getName());
					if (copy) {
						saveFile(file, targetFilePath, test);
					}
					filteredFiles.add(file);
				} else {
					copyDirectory(file, new File(targetLocation, file.getName()), start, end,
							pattern, filteredFiles, test, copy);
				}
			}
		} else {// It's a file!
			if (start.getTime() < sourceLocation.lastModified()
					&& sourceLocation.lastModified() < end.getTime()) {
				if (copy) {
					File targetFilePath = new File(targetLocation.getPath() + File.separator + sourceLocation.getName());
					saveFile(sourceLocation, targetFilePath, test);
					filteredFiles.add(sourceLocation);
				}
				test.printLine(sourceLocation.getAbsolutePath());
			}
		}
	}
//*/////////////////////////////////////////////////////////////////////////////////////

	public static void copyFile(File sourceFile, File targetFile) throws IOException {
		copyFile(sourceFile, targetFile, false);
	}

	public static void copyFile(File sourceFile, File targetFile, boolean force) throws IOException {

		FileInputStream fileInputStream = new FileInputStream(sourceFile);
		byte[] ba = new byte[Integer.parseInt(Long.toString(sourceFile.length()))];
		fileInputStream.read(ba, 0, (int) sourceFile.length());
		fileInputStream.close();
		FileOutputStream fileOutputStream = null;

		try {
			fileOutputStream = new FileOutputStream(targetFile);
		} catch (FileNotFoundException fnfe) {
			if (force) {
				targetFile.getParentFile().mkdirs();
				fileOutputStream = new FileOutputStream(targetFile);
			} else {
				throw fnfe;
			}
		}
		fileOutputStream.write(ba);
		fileOutputStream.close();
	}

	public static boolean createSandbox(File sandboxHome, String packageName) throws SandboxException, IOException {
		String fileSeparator = System.getProperty("file.separator", "/");

		File sandboxDir = new File(sandboxHome.getAbsolutePath() + fileSeparator + packageName);
		File sandboxLogsDir = new File(
				sandboxHome.getAbsolutePath() + fileSeparator + packageName + fileSeparator + "logs");
		boolean createdSandboxDir = sandboxLogsDir.mkdirs();
		if (!createdSandboxDir) {
			throw new SandboxException(
					"Could not create sandbox configuration directory: " + sandboxHome.getAbsolutePath());
		}
		// Get default files
		UtilitiesTools.saveFileString(sandboxDir.getAbsolutePath() + fileSeparator + "environment.xml",
				UtilitiesTools.getResourceString("/start/sandbox/environment.xml"));
		UtilitiesTools.saveFileString(sandboxDir.getAbsolutePath() + fileSeparator + "sandbox.xml",
				UtilitiesTools.getResourceString("/start/sandbox/sandbox.xml"));
		UtilitiesTools.saveFileString(sandboxDir.getAbsolutePath() + fileSeparator + "sandbox.properties",
				UtilitiesTools.getResourceString("/start/sandbox/sandbox.properties"));
		UtilitiesTools.saveFileString(sandboxDir.getAbsolutePath() + fileSeparator + "scratchpad.txt",
				UtilitiesTools.getResourceString("/start/sandbox/scratchpad.txt"));
		
		return false;
	}

	protected static SandboxConnection createSandboxConnection(String mapId) throws IOException {

		SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		SandboxConnection connection = parameters.getGlobals().getMap().get(mapId);
		if (connection != null) {
			return connection;
		}

		String strProcessStart = "/bin/bash";
		if (sandbox.UtilitiesTools.isWindows()) {
			strProcessStart = "cmd";
		}

		ProcessBuilder builder = new ProcessBuilder(strProcessStart);
		builder.directory(new File(parameters.getConfigurationDirectory()));
		Process proc = builder.start();

		parameters.addToGlobalMap(mapId, proc);
		connection = new SandboxConnection(proc);
		return connection;
	}

	public static boolean createSandboxHome(File sandboxHome) throws SandboxException, IOException {

		
		System.out.println("DMH1248 : " + UtilitiesTools.class.getName() + ".createSandboxHome()");
		System.out.println("DMH1248 : " + sandboxHome.getPath());
		
		
		String fileSeparator = System.getProperty("file.separator", "/");
		if (!sandboxHome.exists()) {
			boolean createdRootDir = sandboxHome.mkdirs();
			if (!createdRootDir) {
				throw new SandboxException("Could not create sandbox root directory: " + sandboxHome.getAbsolutePath());
			}
//			Utilities.createSandbox(sandboxHome, "sandbox"); // Create default
//																// sandbox to
//																// put in home
			UtilitiesTools.saveFileString(sandboxHome.getAbsolutePath() + fileSeparator + "environments xml",
					UtilitiesTools.getResourceString("/start/sandbox/environment.xml"));
			return true;
		}
		return false;
	}

	public static Map<String, List<String>> findClasses(File dirJar, String className, boolean recurse,
			SandboxTestBase test)

			throws IOException {

		// Allow for dot-delimited entries
		className = className.replace('.', '/');

		Map<String, List<String>> map = new HashMap<String, List<String>>();

		if (dirJar.isDirectory()) {// It's a directory
			java.util.Collection<File> files = UtilitiesTools.listFilesDirs(dirJar, new SandboxFilenameFilter(".jar"), recurse);
			if (files != null) {
				java.util.Iterator<File> iterFiles = files.iterator();
				while (iterFiles.hasNext()) {
					File file = iterFiles.next();
					
					JarFile jarFile = null;
					try {
						jarFile = new JarFile(file);
					} catch(Throwable th) {
						test.getParameters().printLine(test.getClass().getSimpleName() + " :: " + th.getMessage(), true, true);
						continue;
					}

					Enumeration<JarEntry> entries = jarFile.entries();
					List<String> list = new ArrayList<String>();
					while (entries.hasMoreElements()) {
						JarEntry entry = entries.nextElement();
						if (!entry.isDirectory()) {
							if (entry.getName().endsWith(".class") && entry.getName().indexOf(className) >= 0) {
								list.add(entry.getName());
							}
						}
					}
					if (list.size() > 0) {
						map.put(jarFile.getName(), list);
						jarFile.close();
					}
				}
			}
		} else {// Its a jar file
			JarFile jarFile = null;

			try {
				jarFile = new JarFile(dirJar);
			} catch(Throwable th) {
				test.getParameters().printLine(test.getClass().getSimpleName() + " :: " + th.getMessage(), true, true);
			}
			Enumeration<JarEntry> entries = jarFile.entries();
			List<String> list = new ArrayList<String>();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (!entry.isDirectory()) {
					if (entry.getName().endsWith(".class") && entry.getName().indexOf(className) >= 0) {
						list.add(entry.getName());
					}
				}
			}
			if (list.size() > 0) {
				map.put(jarFile.getName(), list);
				jarFile.close();
			}
		}
		return map;
	}

	/**
	 * Search for string in files archived in a JAR file
	 *
	 * @param searchString
	 * @param jarFile
	 * @return
	 * @throws IOException
	 */
	public static Map<String, List<String>> findInJar(String searchString, JarFile jarFile, SandboxTestBase test)
			throws IOException {
		InputStream inputStream = null;

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		Enumeration<JarEntry> entries = jarFile.entries();

		int window = 10 * searchString.length();
		while (entries.hasMoreElements()) {
			List<String> list = new ArrayList<String>();

			JarEntry entry = entries.nextElement();
			if (!entry.isDirectory()) {
				try {
					inputStream = jarFile.getInputStream(entry);

					byte[] ba = new byte[Integer.parseInt(Long.toString(entry.getSize()))];
					inputStream.read(ba, 0, (int) entry.getSize());
					inputStream.close();

					String fileString = new String(ba);
					int foundAt = -1;
					if ((foundAt = fileString.indexOf(searchString)) >= 0) {
						list.add("..." + fileString.substring(foundAt - window >= 0 ? foundAt - window : 0,
								foundAt + window + window > fileString.length() ? fileString.length()
										: foundAt + window + window)
								+ "...");
					}
				} catch (IOException ioe) {
					test.printLine(ioe.getMessage());
				} finally {
					inputStream.close();
				}
			}
			if (list.size() > 0) {
				map.put(entry.getName(), list);
			}
		}

		return map;
	}

	public static Map<String, Map<String, List<String>>> findInJars(File dirJar, String searchString, boolean recurse,
			SandboxTestBase test) throws IOException {

		Map<String, Map<String, List<String>>> jarMap = new HashMap<String, Map<String, List<String>>>();
		if (dirJar.isDirectory()) {// It's a directory
			java.util.Collection<File> files = UtilitiesTools.listFilesDirs(dirJar, new SandboxFilenameFilter(".jar"),
					recurse);
			if (files != null) {
				java.util.Iterator<java.io.File> iterFiles = files.iterator();
				while (iterFiles.hasNext()) {
					File file = iterFiles.next();
					JarFile jarFile = new JarFile(file);

					Map<String, List<String>> entryMap = findInJar(searchString, jarFile, test);
					if (entryMap.size() > 0) {
						jarMap.put(jarFile.getName(), entryMap);
					}
					if (test != null) {
						if (!test.getIsRunning()) {
							break;
						}
					}
				}
			}
		} else {// Its a jar file
			JarFile jarFile = new JarFile(dirJar);

			Map<String, List<String>> entryMap = findInJar(searchString, jarFile, test);
			jarMap.put(jarFile.getName(), entryMap);
		}
		return jarMap;
	}

	public static int findInTable(String toFind, JTable table, boolean regex) {
		for (int row = table.getSelectedRow() + 1; row < table.getRowCount(); row++) {
			for (int col = 0; col < table.getColumnCount(); col++) {
				String value = (String) table.getModel().getValueAt(row, col);
				if (regex) {
					if (value.matches(toFind)) {
						
						
						return (table.getRowSorter().convertRowIndexToView(row));
//						return row;
					}
				} else {
					if (value.equals(toFind)) {
						return (table.getRowSorter().convertRowIndexToView(row));
//						return row;
					}
				}
			}
		}
		return 0;
	}

	public static String getDay() {

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(SandboxParameters.DATE_DAY_FORMAT);
		return dateFormat.format(date);
	}
	
	public static String[] getEnvFont() {
		String[] clf = {"Monospace", "0", "11"};
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont","Monospace_0_11"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			String message = "Using default font: " + clf;
			System.err.println(message);
			SandboxParameters.createSandboxParameters().printLine(message);
		}
		return clf;
	}
	
	/**
	 * Method calculates last working day for last day of month as input
	 * 
	 * @param lastDayOfMonth
	 * @return LocalDate instance containing last working day
	 */
	public static LocalDate getLastWorkingDayOfMonth(LocalDate lastDayOfMonth) {
		LocalDate lastWorkingDayofMonth;
		switch (DayOfWeek.of(lastDayOfMonth.get(ChronoField.DAY_OF_WEEK))) {
		case SATURDAY:
			lastWorkingDayofMonth = lastDayOfMonth.minusDays(1);
			break;
		case SUNDAY:
			lastWorkingDayofMonth = lastDayOfMonth.minusDays(2);
			break;
		default:
			lastWorkingDayofMonth = lastDayOfMonth;
		}
		return lastWorkingDayofMonth;
	}


	/**
	 * Return byte array of the file
	 *
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getFileBytes(File file) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		byte[] ba = new byte[Integer.parseInt(Long.toString(file.length()))];
		fileInputStream.read(ba, 0, (int) file.length());
		fileInputStream.close();
		return ba;
	}

	public static byte[] getFileBytes(String filePath) throws IOException {
		File file = new File(filePath);
		return getFileBytes(file);
	}

	/**
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object getFileObject(String filePath)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		FileInputStream underlyingStream = new FileInputStream(filePath);
		ObjectInputStream deserializer = new ObjectInputStream(underlyingStream);
		Object object = deserializer.readObject();
		deserializer.close();
		return object;
	}

	public static String getFileString(File file, long length) throws IOException {
		String fileString = null;
		

		FileInputStream fileInputStream = new FileInputStream(file);
		byte[] ba = new byte[Integer.parseInt(Long.toString(length))];
		fileInputStream.read(ba, 0, (int) length);
		try {
			fileString = new String(ba);
		} catch (OutOfMemoryError oome) {
			IOException ioException = new IOException("File too large.");
			throw ioException;
		} finally {
			fileInputStream.close();
		}
		return fileString;
	}

	public static String getFileString(String filename) throws FileNotFoundException, IOException {
		File file = new File(filename);
		return getFileString(file, file.length());
	}

	public static String getFileTimestamp() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP);
		return dateFormat.format(date);
	}

	public static String getPoppadFileName(SandboxTestBase test, String parameterName) {
		return test.getClass().getSimpleName() + "_" + parameterName + ".txt";
	}

	public static int[] getRange(int from, int to, Component parentComponent) throws SandboxException {
		return getRange(from, to, 1000, parentComponent);
	}

	/**
	 *
	 * <b>Description:«FS> b> Validates a range of integers
	 *
	 * @param from
	 *            Start index
	 * 
	 * @param to
	 *            End index
	 * @param boundary
	 *            If 'to' is higher than this number,the range is queried for.
	 * @param lastRange
	 *            Preset range
	 * @return
	 */
	public static int[] getRange(int from, int to, int boundary, Component parentComponent) throws SandboxException {
		int[] range = { from, to };
		if ((to - from) < boundary && (to - from) > 0) {
			UtilitiesTools.lastRange = range;
			return range;
		}
		String initialSelection = String.valueOf(lastRange[0])
				+ (lastRange.length > 1 ? "," + String.valueOf(lastRange[1]) : "");
		String strRange = JOptionPane.showInputDialog(parentComponent, "Range from,to (total: " + to + ")",
				initialSelection);
		if (strRange == null) {
			throw new SandboxException("Aborted");
//			range[1] = 0;
//			return range;
		} else if (strRange.equals("")) {
			return range;
		} else {
			String[] saRange = strRange.split(",");
			range[0] = Integer.parseInt(saRange[0]);
			if (saRange.length > 1) {
				range[1] = Integer.parseInt(saRange[1]);
			} else {
				range = Arrays.copyOf(range, 1);
			}
		}
		UtilitiesTools.lastRange = range;
		return range;
	}

	public static File getResourceFile(String resourcePath)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		ClassPathResource resource = new ClassPathResource(resourcePath);
		return resource.getFile();
	}

	public static byte[] getResourceFileBytes(String resourcePath) {
		File file;
		try {
			file = getResourceFile(resourcePath);
			return getFileBytes(file);
		} catch (Exception en) {
			return null;
		}
	}

	public static Object getResourceObject(String resourcePath)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		ClassPathResource resource = new ClassPathResource(resourcePath);
		InputStream underlyingStream = resource.getInputStream();
		ObjectInputStream deserializer = new ObjectInputStream(underlyingStream);
		Object object = deserializer.readObject();
		deserializer.close();
		return object;
	}

	public static String getResourceString(String resourcePath) throws IOException {
		ClassPathResource resource = new ClassPathResource(resourcePath);
		InputStream inputStream = resource.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

		BufferedReader input = new BufferedReader(inputStreamReader);
		String inputString = "";

		StringBuffer buffer = new StringBuffer();

		while (null != ((inputString = input.readLine()))) {
			buffer.append(inputString).append("\n");
		}
		return buffer.toString();
	}

	public static String getResourceStringInternal(String resourcePath) throws IOException {
		InputStream fileInputStream = UtilitiesTools.class.getClassLoader().getResourceAsStream(resourcePath);
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
		BufferedReader input = new BufferedReader(inputStreamReader);
		String inputString = "";
		StringBuffer buffer = new StringBuffer();

		while (null != ((inputString = input.readLine()))) {
			buffer.append(inputString).append("\n");
		}
		return buffer.toString();
	}

	/**
	 * @param params
	 * @return
	 */
	public static ArrayList<SandboxTestBase> getTests(SandboxParameters params) {
		String packageName = UtilitiesTools.class.getPackage().getName();
		int testCount = 0;
		ArrayList<SandboxTestBase> tests = new ArrayList<SandboxTestBase>();

		try {
			SandboxTestBase command = new Command(); //Get special case Command tool
			command.setParameters(params);
			command.init();
			command.setTestName(command.getClass().getSimpleName());
			tests.add(command);
			params.printLine("Added special case tool class " + command.getClass().getName() + " to tests");
			SandboxTestBase smartOpen = new SmartOpen(); //Get special case SmartOpen tool
			smartOpen.setParameters(params);
			smartOpen.init();
			smartOpen.setTestName(smartOpen.getClass().getSimpleName());
			tests.add(smartOpen);
			params.printLine("Added special case tool class " + smartOpen.getClass().getName() + " to tests");
			FindInFiles findInFiles = new FindInFiles(); //Get special case SmartOpen tool
			findInFiles.setParameters(params);
			findInFiles.init();
			findInFiles.setTestName(findInFiles.getClass().getSimpleName());
			tests.add(findInFiles);
			params.printLine("Added special case tool class " + findInFiles.getClass().getName() + " to tests");
			SandboxTestBase runProcess = new RunProcess(); //Get special case RunProcess tool
			runProcess.setParameters(params);
			runProcess.init();
			runProcess.setTestName(runProcess.getClass().getSimpleName());
			tests.add(runProcess);
			params.printLine("Added special case tool class " + runProcess.getClass().getName() + " to tests");
//			SandboxTestBase findInFiles = new Tool51(); //Get special case RunProcess tool
//			findInFiles.setParameters(params);
//			findInFiles.init();
//			findInFiles.setTestName(findInFiles.getClass().getSimpleName());
//			tests.add(findInFiles);
//			params.printLine("Added special case tool class " + findInFiles.getClass().getName() + " to tests");
			for (testCount = 1; testCount < SandboxParameters.MAX_NUM_TESTS; testCount++) {
				try {
					Class<?> testClass = Class.forName(params.getTargetPackageName() + ".Test" + testCount);
					SandboxTestBase thisTest = (SandboxTestBase) testClass.newInstance();

					thisTest.setParameters(params);
					thisTest.init();

					thisTest.setTestName(thisTest.getClass().getSimpleName());

					tests.add(thisTest);
				} catch (ClassNotFoundException cnfe) {
					System.out.print("t" + testCount + "");
				} catch (NoClassDefFoundError ncdfe) {
					System.out.println("NoClassDefFoundError: " + ncdfe.getMessage());

					for (StackTraceElement element : ncdfe.getStackTrace()) {
						System.out.println("NoClassDefFoundError: " + element.toString());
					}
				}
			}
			for (testCount = 1; testCount < SandboxParameters.MAX_NUM_TESTS; testCount++) {
				try {
					Class<?> testClass = Class.forName(packageName + ".Tool" + testCount);
					SandboxTestBase thisTest = (SandboxTestBase) testClass.newInstance();

					thisTest.setParameters(params);
					thisTest.init();

					thisTest.setTestName(thisTest.getClass().getSimpleName());

					tests.add(thisTest);
				} catch (ClassNotFoundException cnfe) {
					System.out.print("o" + testCount + "");
				} catch (NoClassDefFoundError ncdfe) {
					System.out.println("NoClassDefFoundError: " + ncdfe.getMessage());
					for (StackTraceElement element : ncdfe.getStackTrace()) {
						System.out.println("NoClassDefFoundError: " + element.toString());
					}
				}
			}
			System.out.println();

		} catch (InstantiationException ie) {
			System.out.println(ie.getClass().getName() + ": message= " + ie.getMessage());
		} catch (IllegalAccessException iae) {
			System.out.println(iae.getClass().getName() + ": message= " + iae.getMessage());
		} catch (Exception en) {
			System.out.println(en.getClass().getName() + ": message= " + en.getMessage());
		}
		return tests;
	}

	public static String getTime() {
		return UtilitiesTools.getTime(SandboxParameters.DATE_FORMAT);
	}

	public static String getTime(String format) {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	public static void autoSelectText() {
		SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
		if (sandboxParameters.getFocusedObject() instanceof JTextComponent) {
			JTextComponent component = (JTextComponent) sandboxParameters.getFocusedObject();
			int caretPos = component.getCaretPosition();
			
			String text = component.getText();

			text = text.replaceAll("\\r\\n", "\n");
			text = text.replaceAll("\\r", "\n");
			
			//Check if indented by a tab, then just select the line
			int lineBeginning = text.lastIndexOf('\n', caretPos);
			if (text.charAt(lineBeginning + 1) == '\t') {
				component.setSelectionStart(lineBeginning);
				component.setSelectionEnd(text.indexOf('\n', caretPos));
				return;
			}
			
			String emptyLine = "\n\n";

			int commandSpaceBeginning = text.lastIndexOf(emptyLine, caretPos);
			if (commandSpaceBeginning < 0) {
				commandSpaceBeginning = 0;
			} else {
				commandSpaceBeginning += emptyLine.length();
			}
			int commandSpaceEnding = text.indexOf(emptyLine, caretPos);
			if (commandSpaceEnding < 0) {
				commandSpaceEnding = text.length(); 
			}

			int commandLineBeginning = text.lastIndexOf(SandboxParameters.VISUAL_SEPARATOR, caretPos);
			
			if (commandLineBeginning < 0) {
				commandLineBeginning = 0;
			} else if (commandLineBeginning > 0) {
				commandLineBeginning = text.indexOf('\n', commandLineBeginning) + 1;
			}

			int commandLineEnding = text.indexOf(SandboxParameters.VISUAL_SEPARATOR, caretPos);
			if (commandLineEnding < 0) {
				commandLineEnding = text.length(); 
			}

			int commandBeginning = (commandSpaceBeginning > commandLineBeginning) ? commandSpaceBeginning : commandLineBeginning;
			int commandEnding = (commandSpaceEnding > commandLineEnding && commandLineEnding > 0) ? commandLineEnding : commandSpaceEnding;
			
			component.setSelectionStart(commandBeginning);
			component.setSelectionEnd(commandEnding);
		}
	}

	public static String[] grabRegex(String regex, StringBuffer stringBuffer) {
		return grabRegex(regex, stringBuffer, 0);
	}

	public static String[] grabRegex(Pattern pattern, StringBuffer stringBuffer) {
		return grabRegex(pattern, stringBuffer, 0);
	}

	public static String[] grabRegex(String regex, StringBuffer stringBuffer, int start) {
		Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		return grabRegex(pattern, stringBuffer, start);
	}

	public static String[] grabRegex(Pattern pattern, StringBuffer stringBuffer, int start) {
		String[] returnArray = new String[1];
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length());
			if (matcher.find()) {
				if (matcher.groupCount() > 0) {
					returnArray = new String[matcher.groupCount() + 1];
					for (int groupIndex = 0; groupIndex < matcher.groupCount() + 1; groupIndex++) {
						returnArray[groupIndex] = matcher.group(groupIndex);
					}
					return returnArray;
				}
				returnArray[0] = stringBuffer.substring(matcher.start(), matcher.end());
				return returnArray;
			} else {
				return null;
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
	}

	public static List<SandboxBasket<String, Integer>> grabRegexList(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<SandboxBasket<String, Integer>> returnList = new ArrayList<SandboxBasket<String, Integer>>(); //Re22.09.28.16.29.32.840
		try {
			Matcher matcher = pattern.matcher(stringBuffer.toString());
			matcher.region(start, stringBuffer.length() - 1);
			boolean aMatch = matcher.find();
			while (aMatch) {
				String match = matcher.group(0).trim();
				SandboxBasket<String, Integer> basket = new SandboxBasket<String, Integer>(match, 0);
				returnList.add(basket);
				for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
					if (matcher.group(groupIndex).trim().length() > 0) {
						match = matcher.group(groupIndex).trim();
						basket = new SandboxBasket<String, Integer>(match, groupIndex);
						returnList.add(basket);
					}
				}
				aMatch = matcher.find();
			}
		} catch (IllegalStateException ise) {
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			return null;
		}
		return returnList;
	}

	public static List<Integer> grabEndsRegex(Pattern pattern, StringBuffer stringBuffer, int start) {
		List<Integer> returnList= new ArrayList<Integer>();
		Matcher matcher = pattern.matcher(stringBuffer.toString());
		matcher.reset();

		matcher.region(start, stringBuffer.length());
		
		while(matcher.find()) {
			int foundStart = matcher.start();
			System.err.println("foundStart = " + foundStart);
			int foundEnd = matcher.end();
			System.err.println("foundEnd = " + foundEnd);
			returnList.add(foundStart);
			returnList.add(foundEnd);
		}
		
		return returnList;
	}

	public static boolean groupMatch(String regex, StringBuffer string) {
		Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);

		return groupMatch(pattern, string);
	}

	public static boolean groupMatch(Pattern pattern, StringBuffer string) {
		String[] finds = grabRegex(pattern, string);
		if (finds == null) {
			return false;
		}
		if (finds.length == 1) {
			return true;
		}
		boolean found = true;
		for (int index = 1; index < finds.length - 1; index++) {
			if (finds[index] != null) {
				found = false; //found one of the negate groups, so return false
			}
		}
		return found;
	}

	public static List<SandboxBasket<String[], String[]>> grabReplaceRegex(String regex, String[] replace,
			StringBuffer passedText, StringBuffer newText) {
		List<SandboxBasket<String[], String[]>> findList = new ArrayList<SandboxBasket<String[], String[]>>();
		StringBuffer refString = new StringBuffer(passedText.toString());
		int lastStart = 0;
		try {
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			Matcher matcher = pattern.matcher(refString.toString());
			matcher.reset();
			while (matcher.find()) {
				String[] groupArray = new String[1];
				String[] limitArray = new String[1];
				newText.append(refString.substring(lastStart, matcher.start()));
				lastStart = matcher.end();
				if (matcher.groupCount() > 0) {
					groupArray = new String[matcher.groupCount() + 1];
					limitArray = new String[matcher.groupCount() + 1];
					StringBuffer insert = new StringBuffer();

					for (String replaceString : replace) {
						if (replaceString.startsWith(GRAB_REPLACE)) {
							insert.append(
									URLDecoder.decode(replaceString.substring(GRAB_REPLACE.length()), "US-ASCII"));
						} else {
							int groupIndex = Integer.parseInt(replaceString);
							insert.append(refString.substring(matcher.start(groupIndex), matcher.end(groupIndex)));
						}
					}
					newText.append(insert);
					findList.add(new SandboxBasket<String[], String[]>(groupArray, limitArray));
				}
				groupArray[0] = refString.substring(matcher.start(), matcher.end());
			}
			newText.append(refString.substring(lastStart, refString.length()));

			return findList;
		} catch (IllegalStateException ise) {
			return null;
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static int indexInByteArray(int start, byte[] outerArray, byte[] smallerArray) {
		if (start >= 0 && start < outerArray.length) {
			for (int i = start; i < outerArray.length - smallerArray.length + 1; ++i) {
				boolean found = true;
				for (int j = 0; j < smallerArray.length; ++j) {
					if (outerArray[i + j] != smallerArray[j]) {
						found = false;
						break;
					}
				}
				if (found)
					return i;
			}
		}
		return -1;
	}

	public static int indexOfNthCharInString(String string, char ch, int index) {
		if (string == null || index < 1) {
			return -1;
		}
		int pos = 0;
		while (index-- > 0 && pos != -1) {
			pos = string.indexOf(ch, pos + 1);
		}
		return pos;
	}

	public static boolean isWindows() {
		String osName = System.getProperty("os.name", "Windows");
		return (osName.toLowerCase().indexOf("win") >= 0);
	}

	/**
	 * 	public <T> T getGlobalMapThing(Object key, Class theClass) throws InstantiationException, IllegalAccessException {
		return (T) this.getGlobalMapObject(key);
	}

	 * @param input
	 * @param type
	 * @return
	 */

//*///////////////////DMH	
	@SuppressWarnings("unchecked")
	public static <T> T jsonToJava(String input, Class type) {
		Gson gson = new Gson();
		return (T) gson.fromJson(input, type);
	}
/*//////////////////////
	@SuppressWarnings("unchecked")
	public static Object jsonToJava(String input, Class type) {
		Gson gson = new Gson();
		return gson.fromJson(input, type);
	}
//*//////////////////////

	
	public static String javaToJson(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}
	
	public static String jsonPrettyPrint(String input) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(input);
		return gson.toJson(je);
	}

	public static String jsonUglyPrint(String input) {
		Gson gson = new GsonBuilder().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(input);
		return gson.toJson(je);
	}
	
	/**
	 * Get value from json.
	 * 'json' is the json string;
	 * 'path' is like 'array[2].address.street'
	 *  or 'car[1].make.model.occupant[2]'
	 * @param path
	 * @param json
	 * @return
	 */
	public static String getFromJson(String[] path, String json) {
		JsonParser parser = new JsonParser();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);

		if (je.isJsonArray()) {
			json = "{\"array\": " + json + "}";
		}
		JsonElement jsonTree = parser.parse(json);

		if (jsonTree.isJsonObject()) {
			JsonObject jsonObject = jsonTree.getAsJsonObject();
			return String.valueOf(stepIn(jsonObject, path, 0));
		} else {
			return "{NOT JSON!}";
		}
	}
	 private static JsonElement stepIn(JsonObject job, String[] path, int pathIndex) {
		if (pathIndex >= path.length) {
			return job;
		}

		JsonElement je = null;
		if (path[pathIndex].matches(".*\\[\\d+?\\].*")) {
			int arrayIndex = Integer.parseInt(
					path[pathIndex].substring(path[pathIndex].indexOf('[') + 1, path[pathIndex].length() - 1));
			String arrayName = path[pathIndex].substring(0, path[pathIndex].indexOf('['));
			je = job.get(arrayName);
			JsonArray ja = je.getAsJsonArray();
			je = ja.get(arrayIndex);
			if (je != null && !je.isJsonPrimitive()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		} else {
			je = job.get(path[pathIndex]);
			if (je != null && !je.isJsonPrimitive() && !je.isJsonArray()) {
				je = stepIn(je.getAsJsonObject(), path, pathIndex + 1);
			}
		}
		return je;
	 }

	public static Collection<File> listFiles(File directory, FilenameFilter filter, boolean recurse, boolean onlyFiles,
			boolean onlyDirs, SandboxTestBase test) {
		if (test != null && !test.isRunning()) {
			return null;
		}
		// List of files / directories
		Vector<File> files = new Vector<File>();
		// Java4: Vector files = new Vector();

		// Get files /directories in the directory
		File[] entries = directory.listFiles();

		// Go over entries
		if (entries != null) {
			for (File entry : entries) {
				if (onlyFiles) {
					if (entry.isFile() && (filter == null || filter.accept(directory, entry.getName()))) {
						files.add(entry);
					}
				} else if (onlyDirs) {
					if (entry.isDirectory() &&

							(filter == null || filter.accept(directory, entry.getName()))) {
						files.add(entry);
					}
				} else {
					if (filter == null || filter.accept(directory, entry.getName())) {
						files.add(entry);
					}
				}
				if (recurse && entry.isDirectory()) {
					try {
						files.addAll(listFilesDirs(entry, filter, recurse));
					} catch (Exception en) {
						sandboxLogger.log(Level.ALL, "Could not list " + entry.getPath());
					}
				}
			}
			return files;
		} else {
			return null;
		}
	}

	public static Collection<File> listFiles(File directory, FilenameFilter filter, boolean recurse, boolean onlyFiles,
			SandboxTestBase test) {
		return listFiles(directory, filter, recurse, onlyFiles, false, test);
	}

	public static Collection<File> listFiles(File directory, FilenameFilter filter, FileFilter fileFilter,
			boolean recurse, SandboxTestBase test) {
		// List of files /directories
		Vector<File> files = new Vector<File>();
		// Java4: Vector files = new Vector();

		// Get files /directories in the directory
		File[] entries = directory.listFiles(fileFilter);

		try {
			// Go over entries
			if (entries != null) {
				for (File entry : entries) {
					if (filter == null || filter.accept(directory, entry.getName())) {
						files.add(entry);
					}

					if (recurse && entry.isDirectory()) {
						files.addAll(listFiles(entry, filter, fileFilter, recurse, test));
					}
					test.checkRunning();
				}
				return files;
			} else {
				return null;
			}

		} catch (SandboxException se) {
			test.printLine("Cancelling");
			return files;
		}
	}

	public static Collection<File> listFilesDirs(File directory, FilenameFilter filter, boolean recurse) {
		return UtilitiesTools.listFiles(directory, filter, recurse, false, null);
	}

	public static Collection<File> listFilesDirs(File directory, FilenameFilter filter, boolean recurse,
			SandboxTestBase test) {
		return UtilitiesTools.listFiles(directory, filter, recurse, false, test);
	}

	public static File[] listFilesDirsAsArray(File directory, FilenameFilter filter, boolean recurse) {
		Collection<File> files = listFiles(directory, filter, recurse, false, null);
		// Java4: Collection files = listFiles(directory,filter,recurse);

		File[] arr = new File[files.size()];
		return files.toArray(arr);
	}

	public static Collection<File> listFilesRegex(File directory, boolean recurse, boolean onlyFiles, Pattern pattern,
			SandboxTestBase test) {
		return listFilesRegex(directory, null, null, recurse, onlyFiles, pattern, test);
	}
	
	public static Collection<File> listFilesRegex(File directory, Date fromDate, Date toDate, boolean recurse, boolean onlyFiles, Pattern pattern,
			SandboxTestBase test) {
		if (test != null && !test.isRunning()) {
			return null;
		}
		// List of files /directories
		Vector<File> files = new Vector<File>();
		// Java4: Vector files = new Vector();

		// Get files /directories in the directory
		File[] entries = directory.listFiles();

		// Go over entries
		if (entries != null) {
			for (File entry : entries) {
				boolean patternMatch = UtilitiesTools.groupMatch(pattern, new StringBuffer(entry.getPath()));
				boolean modifiedTimeSpanMatch = true;
				if (fromDate != null && toDate != null) {
					modifiedTimeSpanMatch = (entry.lastModified() > fromDate.getTime() && entry.lastModified() < toDate.getTime());
				}
				if (patternMatch && modifiedTimeSpanMatch) {
					if (onlyFiles) {
						if (entry.isFile()) {
							files.add(entry);
						}
					} else {
						files.add(entry);
					}
				}
				if (recurse && entry.isDirectory()) { //Recurse into sub-directories

					try {
						files.addAll(listFilesRegex(entry, fromDate, toDate, recurse, onlyFiles, pattern, test));
					} catch (Exception en) {
						String message = "Could not list " + entry.getPath();
						test.printLine(message, en);
						sandboxLogger.log(Level.ALL, message);
					}
				}
			}
			return files;
		} else {
			return null;
		}
	}

	public static String noEmptyStrings(String string) {
		if (string.length() == 0) {
			return null;
		}
		return string;
	}

	public static boolean noNulls(boolean b) {
		return b;
	}

	public static File noNulls(File file) {
		if (file == null)
			file = new File(SandboxTestBase.class.getName() + ".temp");
		return file;
	}

	public static int noNulls(int integer) {
		return integer;
	}

	public static long noNulls(long l) {
		return l;
	}

	public static Object noNulls(Object obj) {
		if (obj == null)
			obj = "";
		return obj;
	}

	public static String noNulls(String string) {
		return noNulls(string, "");
	}

	public static String noNulls(String string, String ifNull) {
		if (string == null)
			string = ifNull;
		return string;
	}

	public static Object[][] parseCsv(String filePath, SandboxTestBase test) throws IOException {
		FileInputStream inputStream = new FileInputStream(filePath);
		String[][] data = null;
		if (inputStream != null) {
			test.addConnection(inputStream);
			LineNumberReader reader = new LineNumberReader(new InputStreamReader(inputStream));
			List<String> lines = new ArrayList<String>();
			String line = reader.readLine();
			test.printLine(line);
			String[] headers = line.split(",");

			int byteSize = line.length();
			lines.add(line);
			while ((line = reader.readLine()) != null) {
				lines.add(line);
				byteSize += line.length();
				if (byteSize > test.getParameters().getMaxFileOpenSize()) {
					lines.add("TRUNCATED HERE");
					break;
				}
			}
			data = new String[lines.size()][headers.length];
			int index = 0;

			for (String row : lines) {
				data[index++] = row.split(",");
			}
		}
		return data;
	}

	public static String parseDate(long time) {
		return parseDate(new Date(time), SandboxParameters.DATE_FORMAT_TIMESTAMP);
	}

	public static String parseDate(long time, String format) {
		return parseDate(new Date(time), format);
	}

	/**
	 * Parses a string from the input Date using a SimpleDateFormat.The expected
	 * date format of the input string is "yyyyMMdd".
	 *
	 * @param date
	 *            the date
	 * @param format
	 *            the date format string
	 * @return the String represented by the date
	 * @see SimpleDateFormat
	 */
	public static String parseDate(Date date, String format) {
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			return dateFormat.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Parses a Date from the input string using a SimpleDateFormat.The expected
	 * date format of the input string is "yyyyMMdd".
	 *
	 * @param input
	 *            the String representing the date
	 * @param format
	 *            the date format string
	 * @return the Date represented by the input
	 * @see SimpleDateFormat
	 */
	public static Date parseDate(String input, String format) {
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			return dateFormat.parse(input);
		} catch (Exception e) {
			return null;
		}
	}
	
	
	public static int showConfirmDialog(String message, SandboxParameters parameters) {
		parameters.hideMessages();
		parameters.setAlertUp(true);
		int decision = JOptionPane.showConfirmDialog(parameters.tabbedPane, message);
		parameters.setAlertUp(false);
		parameters.showMessages();
		return decision;
	}
	
	public static void showMessageDialog(String message, SandboxParameters parameters) {
		parameters.hideMessages();
		parameters.setAlertUp(true);
		JOptionPane.showMessageDialog(parameters.tabbedPane, message);
		parameters.setAlertUp(false);
		parameters.showMessages();
	}
	
	public static void showChiclet(SandboxParameters params) {
		showChiclet(params, -1, -1);
	}
	public static void showChiclet(SandboxParameters params, int x, int y) {
		
		SandboxConnection sandboxConnection = params.getGlobals().getMap().get("chiclet");
		if (sandboxConnection != null && sandboxConnection.getObj() instanceof SandboxFrame) {
			SandboxFrame chiclet = (SandboxFrame) sandboxConnection.getObj();
			if (chiclet.isVisible()) {
				chiclet.setAlwaysOnTop(true);
		        chiclet.setExtendedState(JFrame.NORMAL); // To maximize a frame
				return;
			} else {
				params.getGlobals().getMap().remove("chiclet");
			}
		}
		
		JLabel label = new JLabel(SandboxParameters.targetPackageName);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(label);
		panel.setToolTipText(params.getConfigurationDirectory());
		JScrollPane scrollPane = new JScrollPane(panel);
		SandboxFrame frame = new SandboxFrame();
//		JFrame frame = new JFrame();
		int width = 200;
		int height = 20;
		String chicletMinWidHgt = System.getProperty("sbx.value.chiclet.min.wid.hgt", "200,20");
		width = Integer.valueOf(chicletMinWidHgt.split(",")[0]);
		height = Integer.valueOf(chicletMinWidHgt.split(",")[1]);
		Dimension dime = new Dimension(width,height);
		frame.setMinimumSize(dime);
		frame.setTitle(params.getTargetPackageName() + "-chicklet");
		label.setToolTipText(params.getConfigurationDirectoryRoot());
		frame.add(scrollPane);
		if (x < 0 && y < 0) {
			frame.setLocationRelativeTo(params.getTabbedPane());
		} else {
			frame.setLocation(x, y);
		}
		label.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				params.printLine("mouseClicked");
				if (arg0.getClickCount() == 2) {
					SandboxFrameBase.phoneCall("Oh, there you are!", "Where are you?");
				}
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
			}
			@Override
			public void mousePressed(MouseEvent arg0) {
			}
			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
			
		});
		setBorderColor(frame);
		frame.setIconImage(SandboxParameters.sandboxImage);
		frame.pack();
		frame.setExtendedState(JFrame.NORMAL);

		frame.setVisible(true);
		frame.setAlwaysOnTop(true);
		params.addToGlobalMap("chiclet", frame);

	}
	
//	 static boolean confirm(String message, SandboxParameters parameters) {
//		
//		SandboxTestBase test = parameters.getTest(Tool160.class.getSimpleName());
//		test.parameterValues.put("string", message);
//		test.presetParameters = true;
//		
//		Hashtable<String, JButton> buttonTable = parameters.getButtons();
//		javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX
//				+ Tool160.class.getSimpleName());
//		if (button != null) {
//			button.doClick();
//			button.doClick();
//		}
//
//		int decision = (Integer)test.returnedFromList[0];
//		
//		if (decision == 0) {
//			return true;
//		} else {
//			return false;
//		}
//		
//	}


	
	public static String showInputDialog(String message, SandboxParameters parameters) {
		parameters.hideMessages();
		parameters.setAlertUp(true);
		String input = JOptionPane.showInputDialog(parameters.getTabbedPane(), message);
		parameters.setAlertUp(false);
		parameters.showMessages();
		return input;
	}

	public static void playSound(String wavFilePath) throws SandboxException {
		try {
			File soundFile = new File(wavFilePath);
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
			AudioFormat audioFormat = audioInputStream.getFormat();

			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
			try {
				SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
				line.open(audioFormat);
				line.start();

				int nBytesRead = 0;
				byte[] abData = new byte[128000];
				while (nBytesRead != -1) {
					nBytesRead = audioInputStream.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						line.write(abData, 0, nBytesRead);
					}
				}
				line.drain();
				line.close();
			} catch (LineUnavailableException lue) {
				throw (new SandboxException(
						lue.getClass().getSimpleName() + ": Can't play sound file - " + wavFilePath));
			}
		} catch (IOException ioe) {
			throw (new SandboxException(ioe.getClass().getSimpleName() + ": Sound file not found - " + wavFilePath));
		} catch (UnsupportedAudioFileException uafe) {
			throw (new SandboxException(
					uafe.getClass().getSimpleName() + ": Not a supported audio file format -" + wavFilePath));
		} catch (Exception en) {
			throw (new SandboxException(en.getClass().getSimpleName() + ": File - " + wavFilePath));
		}
	}

	public static void playThreadedSound(InputStream inputstream, SandboxParameters params) {
		(new UtilitiesTools.PlaySoundThread(inputstream, params)).start();
	}

	public static void playThreadedSound(String wavFilePath, SandboxParameters params) {
		File wavFile = new File(wavFilePath);
		try {
			FileInputStream fileInputStream = new FileInputStream(wavFile);
			(new PlaySoundThread(fileInputStream, params)).start();
		} catch (FileNotFoundException fnfe) {
			params.printLine("Sound file problem", fnfe);
		}
	}

	private static void printLine(Object obj, SandboxTestBase test) {
		if (test != null) {
			test.printLine(obj);
		} else {
			System.out.println(obj);
		}
	}

	public static void saveFile(File sourceLocation, File targetLocation, SandboxTestBase test) throws IOException {

		printLine("SAVING: " + parseDate(new Date(sourceLocation.lastModified()), SandboxParameters.DATE_FORMAT) + " :: "
				+ sourceLocation.getPath(), test);
		printLine("\tto\n" + targetLocation.getPath(), test);
		printLine(SandboxParameters.VISUAL_SEPARATOR, test);

		File targetDirectory = new File(targetLocation.getParent());
// For some reason this did not work on a Windows system, so I have to just make the directories
//		if (targetDirectory.exists()) { 
//			targetDirectory.mkdirs();
//		}
		targetDirectory.mkdirs();

		if (!targetLocation.isDirectory()) {
			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);

			}
			in.close();
			out.close();

		} else {
			printLine(targetLocation.getPath() + "Is not a file!", test);
		}
	}

	/*
	 * @param filePath
	 * 
	 * @param ser
	 * 
	 * @throws FileNotFoundException
	 * 
	 * @throws IOException
	 */
	public static void saveFileObject(String filePath, Serializable ser) throws FileNotFoundException, IOException {
		FileOutputStream underlyingStream = new FileOutputStream(filePath);
		ObjectOutputStream serializer = new ObjectOutputStream(underlyingStream);
		serializer.writeObject(ser);
		serializer.close();
	}

	public static void saveFileString(String filepath, byte[] bytes) throws FileNotFoundException, IOException {
		FileOutputStream fos = null;
		File file = new File(filepath);

		File directory = new File(file.getParent());
		if (!directory.exists()) {
			sandboxLogger.info("Making directory: " + directory.getPath());
			directory.mkdirs();
		}
		fos = new FileOutputStream(file);
		fos.write(bytes);
		fos.close();
	}

	public static void saveFileList(String filepath, List<String> lines) throws FileNotFoundException, IOException {
		FileOutputStream fos = null;
		File file = new File(filepath);

		File directory = new File(file.getParent());
		if (!directory.exists()) {
			sandboxLogger.info("Making directory: " + directory.getPath());
			directory.mkdirs();
		}
		fos = new FileOutputStream(file);
		for (String line : lines) {
			fos.write(line.getBytes());
			fos.write((System.getProperty("line.separator", "\n")).getBytes());
		}
		fos.close();
	}

	public static void saveFileString(String filepath, String filestring) throws FileNotFoundException, IOException {
		saveFileString(filepath, filestring.getBytes());
	}

	public static void setBorderColor(JFrame frame) {
		String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
		Color borderColor = new Color(255, 255, 255);
		try {
			borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]), Integer.parseInt(ebc[2]));
		} catch (NumberFormatException nfe) {
			System.err.println("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");
		}
		Border border = BorderFactory.createLineBorder(borderColor, 5);
		frame.getRootPane().setBorder(border);
	}

	public static SandboxTable tabulate(SandboxParameters params, String[][] columnRows, String[] headers,
			String tooltip, boolean sort) throws ArrayIndexOutOfBoundsException {

		int numRows = columnRows.length;
		int numCols = 0;
		try {
			numCols = columnRows[0].length;
		} catch (ArrayIndexOutOfBoundsException aioobe) {// Allow for empty
															// results
			if (headers == null) {// No headers,AND no results
				return null;
			} else {
				numCols = headers.length;
			}
		}

		if (headers == null) {// allow for no column headers
			headers = new String[numCols];
			for (int i = 0; i < numCols; i++) {
				headers[i] = String.valueOf(i + 1);
			}
		}
		
		TestBaseTableModel model = new TestBaseTableModel(columnRows, headers);
		SandboxTable table = new SandboxTable(model);

		String[] clf = {"Monospace", "0", "11"};
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont","Monospace_0_11"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		table.setFont(textFont);
		Font headerFont = new Font(clf[0], 1, Integer.parseInt(clf[2]));
		table.getTableHeader().setFont(headerFont);
		
		int gap = textFont.getSize() - 10;
		table.setRowHeight(table.getRowHeight() + gap);

		if (numRows == 0) {
			return table;
		}
		TableRowSorter<TableModel> reverseSorter = new TableRowSorter<TableModel>(table.getModel());

		for (int col = 0; col < numCols; col++) {
			int rowToCheck = 0;
			if (columnRows[0][col] == null || columnRows[0][col].length() == 0) {
				rowToCheck = numRows / 2;
				if (columnRows[rowToCheck][col] == null || columnRows[rowToCheck][col].length() == 0) {
					params.printLine(headers[col] + " is an empty value", true, true);
					continue;
				}
			}

			try {
				Integer.parseInt(columnRows[rowToCheck][col]);
				reverseSorter.setComparator(col, new Comparator<String>() {
					@Override
					public int compare(String a, String b) {
						try {
							int result = Integer.parseInt(b) - Integer.parseInt(a);
							return result;
						} catch (NumberFormatException nfe) {
							return -1;
						}
					}
				});
				params.printLine(columnRows[rowToCheck][col] + " IS an integer", true, true);
				continue;
			} catch (NumberFormatException nfe) {
				params.printLine(columnRows[rowToCheck][col] + " not an integer", true, true);
			}
			try {
				Float.parseFloat(columnRows[rowToCheck][col]);
				reverseSorter.setComparator(col, new Comparator<String>() {
					@Override
					public int compare(String a, String b) {
						try {
							float result = Float.parseFloat(b) - Float.parseFloat(a);
							return (int) (10000f * result);
						} catch (NumberFormatException nfe) {
							return -1;
						}
					}
				});
				params.printLine(columnRows[rowToCheck][col] + " IS a float", true, true);
				continue;
			} catch (NumberFormatException nfe) {
				params.printLine(columnRows[rowToCheck][col] + " not a float", true, true);
			}
			try {
				Long.parseLong(columnRows[rowToCheck][col]);
				reverseSorter.setComparator(col, new Comparator<String>() {
					@Override
					public int compare(String a, String b) {
						try {
							long result = Long.parseLong(b) - Long.parseLong(a);
							return (int) (100001 * result);
						} catch (NumberFormatException nfe) {
							return -1;
						}
					}
				});
				params.printLine(columnRows[rowToCheck][col] + " IS a long", true, true);
				continue;
			} catch (NumberFormatException nfe) {
				params.printLine(columnRows[rowToCheck][col] + " not a long", true, true);
			}
			try {
				Double.parseDouble(columnRows[rowToCheck][col]);
				reverseSorter.setComparator(col, new Comparator<String>() {
					@Override
					public int compare(String a, String b) {
						try {
							double result = Double.parseDouble(b) - Double.parseDouble(a);
							return (int) (10000d * result);
						} catch (NumberFormatException nfe) {
							return -1;
						}
					}
				});

				params.printLine(columnRows[rowToCheck][col] + " IS a double", true, true);
				continue;
			} catch (NumberFormatException nfe) {
				params.printLine(columnRows[rowToCheck][col] + " not a double", true, true);
			}

//			String[] dateFormats = params.sandboxDateFormats.split(",");
			String[] dateFormats = System.getProperty(SandboxParameters.SANDBOX_DATE_FORMATS_ENV, 
					SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP).split(",");

			for (int index = 0; index < dateFormats.length; index++) {
				String dateFormat = dateFormats[index];
				try {
					SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
					sdf.parse(columnRows[rowToCheck][col]);

					reverseSorter.setComparator(col, new DateComparator<String>(dateFormat));
					params.printLine(columnRows[rowToCheck][col] + " IS a " + dateFormat, true, true);
					continue;

				} catch (Exception e) {
					params.printLine(columnRows[rowToCheck][col] + " not a " + dateFormat, true, true);
				}
			}

		}

		reverseSorter.setComparator(numCols, new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				return Integer.parseInt(b) - Integer.parseInt(a);
			}
		});
		table.setRowSorter(reverseSorter);

		// table.addFocusListener(params.focusListener);
		int numberedColumnMaxWidth = table.getFontMetrics(table.getFont()).stringWidth(String.valueOf(numRows)) + 20;
//		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.setDefaultRenderer(table.getColumnClass(0), renderer);

		for (int i = 0; i < numCols; i++) {
			TableColumn tableColumn = table.getColumnModel().getColumn(i);
			params.printLine(tableColumn.getHeaderValue() + ": i = " + i, true, true);
			int textWidth = 0;
			if (numRows > 0) {
				textWidth = table.getFontMetrics(table.getFont()).stringWidth(noNulls(columnRows[0][i])) + 10;
				if (textWidth > 510) {
					textWidth = 510;
				}
			}

			int headerWidth = table.getFontMetrics(table.getFont()).stringWidth(noNulls(headers[i])) + 30;

			if (headerWidth < textWidth) {
				tableColumn.setPreferredWidth(textWidth);
			} else {
				tableColumn.setPreferredWidth(headerWidth);

			}
		}
		TableColumn tableColumnNumbers = table.getColumnModel().getColumn(numCols);
		tableColumnNumbers.setMaxWidth(numberedColumnMaxWidth);
		table.moveColumn(numCols, 0);
		table.addKeyListener(params.keyListener);
	    table.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyPressed(KeyEvent e) {
	        	SandboxTable table = (SandboxTable) e.getComponent();
	    		table.setDirty(true);
	        }
	      });

//	    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	    
		return table;
	}

	public static String xmlPrettyPrint(String input, int indent) {
		if (input.split("\n").length == 1) {
			try {
				Source xmlInput = new StreamSource(new StringReader(input));
				StringWriter stringWriter = new StringWriter();
				StreamResult xmlOutput = new StreamResult(stringWriter);
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				transformerFactory.setAttribute("indent-number", indent);
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(xmlInput, xmlOutput);

				return xmlOutput.getWriter().toString();

			} catch (Exception e) {
				return input;
			}
		} else {
			return input;
		}
	}
	
	public static String limitTabLength(String tabTitle){		
		int tabLimit = 20;
		try {
			tabLimit = Integer.parseInt(System.getProperty(TAB_LENGTH_MAX, "20"));
		} catch(NumberFormatException nfe){			
			SandboxParameters.createSandboxParameters().printLine(UtilitiesTools.TAB_LENGTH_MAX + " not set", true, true);
		}
		
//		return StringUtils.abbreviate(tabTitle, tabLimit);
		if (tabTitle.length() < tabLimit) {			
			return tabTitle;
		} else {
			return "..."+ tabTitle.substring(tabTitle.length() - tabLimit);
		}
	}


	
	public static int findOpenPort(int portIndex, int portLimit, SandboxTestBase test) throws IOException {
		
//*///////////////////////////DMH
		int openPort = 0;
		boolean success = false;

		while (!success) {
			try {
				(new ServerSocket(portIndex)).close();;
				(new DatagramSocket(portIndex)).close();;
				success = true;
				openPort = portIndex;
			} catch (SocketException se) {
				test.printLine(portIndex + " is a socket");
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			} catch (IOException ioe) {
				test.printLine(portIndex + " is a server");
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			}
		}
		return openPort;
		
		
/*////////////////////////////
		int openPort = 0;
		ServerSocket serverSocket = null;
		boolean success = false;

		while (!success) {
			try {
				serverSocket = new ServerSocket(portIndex);
				success = true;
				openPort = portIndex;
			} catch (IOException ioe) {
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			}
		}
		serverSocket.close();
		return openPort;
//*////////////////////////////
	
	}

}
