package sandbox.test;

import java.util.Base64;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "test simple base64 functions",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test8 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String focusedString = getString("focusedString");
		 
//		 printLine(new String(Base64.getDecoder().decode(encoded.getBytes())).trim();

		 String coded = new String(Base64.getEncoder().encode(focusedString.getBytes())).trim();
		 printLine("Coded: " + coded);
		 String decoded = new String(Base64.getDecoder().decode(coded.getBytes())).trim();
		 printLine("Decoded: " + decoded);

		 //END TEST
	 }

}
