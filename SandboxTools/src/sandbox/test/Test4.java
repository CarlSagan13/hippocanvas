package sandbox.test;

import java.awt.Component;

import sandbox.SandboxAnnotation;
import sandbox.SandboxBasket;
import sandbox.SandboxMessage;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Show SandboxMessage for 30 seconds",
 		parameterDescriptions = {
// 				"1", "string",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
 				SandboxParameters.SCRATCHPAD_NAME, "message",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
 				SandboxParameters.TRUEFALSE_NAME, "modal",
 				SandboxParameters.YESNO_NAME, "editable",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test4 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String message = getString("message");
		 boolean modal = getBoolean("modal");
		 boolean editable = getBoolean("editable");
		 
		 String title = "The Title";
		 int maxLinesVisible = 20;
		 int closeSeconds = 30;
		 int messageCount = 0;
		 SandboxBasket<String,String> basket = new SandboxBasket<String,String> ("one","two");
		 
		 SandboxMessage sandboxMessage = new SandboxMessage(title, message, parameters.getTabbedPane(), maxLinesVisible, closeSeconds, modal, editable, messageCount, basket);
//		 this.addConnection(sandboxMessage);
		 //END TEST
	 }

}
