package sandbox.test;

import java.util.Arrays;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Validate Utilities.concatenateArrays",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "strings",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test10 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String[] strings = getStringArray("strings");
		 
		 byte[] baOne = strings[0].getBytes();
		 byte[] baTwo = strings[1].getBytes();
		 byte[] baThree = strings[2].getBytes();
		 
		 printLine(baOne);
		 printLine(baTwo);
		 printLine(baThree);

		 
		 String[] strings1 = {"one", "two"};
		 String[] strings2 = {"three", "four"};
		 
		 printLine(Utilities.concatenateArrays(strings1, strings2));
		 printALine();
		 
		 Byte[] bytes1 = {1,2};
		 Byte[] bytes2 = {3,4};
		 for (Byte b : Utilities.concatenateArrays(bytes1, bytes2)) {
			 printLine(b);
		 }
		 printALine();

		 Byte[] contatenated = Utilities.concatenateArrays(toObjects(baOne), toObjects(baTwo));
		 contatenated = Utilities.concatenateArrays(contatenated, toObjects(baThree));
		 
		 
//		 printLine(new String(Utilities.concatenateArrays(toObjects(baOne), toObjects(baTwo))));
//		 for (Byte b : Utilities.concatenateArrays(toObjects(baOne), toObjects(baTwo))) {
//			 printLine(((byte)b));
//		 }
		 

//		 byte[] concatenatedPrimative = new byte[contatenated.length];
//		    for (int i = 0; i < contatenated.length; i++)
//		    {
//		    	concatenatedPrimative[i] = contatenated[i];
//		    }
//		 printLine(new String(concatenatedPrimative));
		 
		 
		 printLine(new String(Utilities.concatenateByteArrays(baOne, baTwo)));

		 printALine();
		 
		 byte[] concatenated = {};
		 for (String string : strings) {
			 byte[] bytes = string.getBytes();
			 concatenated = Utilities.concatenateByteArrays(concatenated, bytes);
		 }
		 printLine(new String(concatenated));
		 

		 //END TEST
	 }
	 
	 Byte[] toObjects(byte[] bytesPrim) {
		    Byte[] bytes = new Byte[bytesPrim.length];
		    Arrays.setAll(bytes, n -> bytesPrim[n]);
		    return bytes;
		}

}
