package sandbox.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.Utilities;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Attempt internal resource file access",
 		parameterDescriptions = {
// 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "resourcePath",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test12 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String resourcePath = getString("resourcePath");
		 
		 //printLine(Utilities.getResourceStringInternal(resourcePath));
		 
		 File file = new File(resourcePath);
		 
		 printLine(file.exists());
		 
		 this.newTab(getResourceString(resourcePath), "One");
		 
		 this.newTab(new String(Utilities.getFileBytes(getResourceFile(resourcePath))), "Two");
		 
		 this.newTab(new String(getResourceFileBytes(resourcePath)), "Three");
		 //END TEST
	 }
	 
	File getResourceFile(String resourcePath) {
		return new File(resourcePath);
	}

	byte[] getResourceFileBytes(String resourcePath) {
		File file;
		try {
			file = getResourceFile(resourcePath);
			return Utilities.getFileBytes(file);
		} catch (Exception en) {
			return null;
		}
	}

	String getResourceString(String resourcePath) throws IOException {
		File file = new File(resourcePath);
		FileInputStream fis = new FileInputStream(file);

		InputStreamReader inputStreamReader = new InputStreamReader(fis);

		BufferedReader input = new BufferedReader(inputStreamReader);
		String inputString = "";

		StringBuffer buffer = new StringBuffer();

		while (null != ((inputString = input.readLine()))) {
			buffer.append(inputString).append("\n");
		}
		return buffer.toString();
	}

}
