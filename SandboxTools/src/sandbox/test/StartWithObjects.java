package sandbox.test;

import java.util.ArrayList;
import java.util.List;

import sandbox.Sandbox;
import sandbox.Utilities;

public class StartWithObjects {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String marker = Utilities.getTime();
		 
		 Object obj1 = new Object();
		 Object obj2 = new Object();
		 Object obj3 = new Object();
		 List<Object> objects = new ArrayList<Object> ();
		 objects.add(obj1);
		 objects.add(obj2);
		 objects.add(obj3);
		 objects.add(marker);
		 
		 Runnable runnable = new Runnable() {
			 public void run() {
	 	 	 	 new Sandbox(args, objects); 
			 }
		 };
		 Thread sandboxThread = new Thread(runnable);
		 sandboxThread.start();
	}

}
