package sandbox;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(description = SandboxParameters.SANDBOX_UNDER_CONSTRUCTION + 
	"Access web resource (optional use global map 'content'; using environment sandbox.connect.timeout)", 
	parameterDescriptions = {
		SandboxParameters.FOCUSED_NAME, "strUrl", 
		SandboxParameters.POPPAD_NAME, "content",
		SandboxParameters.POPPAD_NAME, "contentParameters", 
		SandboxParameters.POPPAD_NAME, "hostnameExceptions",
		SandboxParameters.POPPAD_NAME, "requestMethod"
	}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
public class Tool119 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String strUrl = getString("strUrl");
		String ppContent = getString("content");
		String[] contentParameters = getStringArray("contentParameters");
		String[] hostnameExceptions = getStringArray("hostnameExceptions");
		String[] requestMethods = getStringArray("requestMethod");

		String fileDateStamp = (new SimpleDateFormat(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP)).format(new Date());
		String startDir = params.configurationDirectory + File.separator + "files";

		printLine("sandbox.connect.timeout = " + System.getProperty(SandboxParameters.SBX_CONNECT_TIMEOUT_ENV_NAME,
				String.valueOf(SandboxParameters.DEFAULT_CONNECT_TIMEOUT)));

		byte[] baContent = ppContent.getBytes();
		Object objContent = null;

		if (baContent.length == 0) {// No content from poppad, so check global
									// map: key = "content"
			objContent = this.getGlobalMapObject("content");
		}
		printLine("strUrl = " + strUrl);
		URL url = new URL(strUrl);

		URLConnection urlConn = null;
		try {
			HttpURLConnection httpsUrlConn = (HttpURLConnection) url.openConnection();
			addConnection(httpsUrlConn);

			if (requestMethods.length > 0) {
				httpsUrlConn.setRequestMethod(requestMethods[0]);
			}
//			if (hostnameExceptions.length > 0) {// Only allow these exceptions
//				boolean isHostnameException = false;
//				for (String hostname : hostnameExceptions) {
//					if (strUrl.startsWith(hostname)) {
//						isHostnameException = true;
//					}
//				}
//
//				if (isHostnameException) {
//					printLine("Setting host name verifier");
//					httpsUrlConn.setHostnameVerifier(new HostnameVerifier() {
//
//						@Override
//						public boolean verify(String argO, SSLSession arg1) {
//							return true;
//						}
//					});
//				}
//			}
			urlConn = (URLConnection) httpsUrlConn;
		} catch (ClassCastException cce) {
			urlConn = url.openConnection();
//			urlConn = (URLConnection) url.openConnection();
		}
		if (contentParameters.length > 0) {
			for (int i = 0; i < contentParameters.length; i += 2) {
				urlConn.addRequestProperty(contentParameters[i], contentParameters[i + 1]);
			}
		}

		if (baContent.length > 0) {
			printLine("Sending content, length=" + baContent.length);
			urlConn.addRequestProperty("Content-Length", String.valueOf(baContent.length));
			urlConn.setDoOutput(true);
			OutputStream output = urlConn.getOutputStream();
			this.addConnection(output);
			output.write(baContent);
			output.flush();
		}

		if (objContent != null) {
			printLine("Sending serialized java object");
			urlConn.addRequestProperty("Content-Type", "application/x-java-serialized-object");
			urlConn.setDoOutput(true);
			ObjectOutputStream serializer = new ObjectOutputStream(urlConn.getOutputStream());
			serializer.writeObject(objContent);
			serializer.close();
		}

		InputStream inputStream = null;
		try {
			urlConn.setConnectTimeout(
					Integer.parseInt(System.getProperty(SandboxParameters.SBX_CONNECT_TIMEOUT_ENV_NAME,
							String.valueOf(SandboxParameters.DEFAULT_CONNECT_TIMEOUT))));
			inputStream = urlConn.getInputStream();
			if (urlConn instanceof HttpsURLConnection) {
				printLine("Cipher Suite: " + ((HttpsURLConnection) urlConn).getCipherSuite());
			}
		} catch (Exception en) {// handle server exceptions
			
			en.printStackTrace();
			
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("*** CONNECTION ERROR ***");
			printLine("TYPE: " + en.getClass().getName());
			printLine("MESSAGE: " + en.getMessage());
			printLine("*** ENCODING: " + urlConn.getContentEncoding());
			printLine("*** LENGTH: " + urlConn.getContentLength());
			printLine("***TYPE: " + urlConn.getContentType());
			printLine(SandboxParameters.VISUAL_SEPARATOR);

			if (urlConn instanceof HttpURLConnection) {
				HttpURLConnection httpUrlConn = (HttpURLConnection) urlConn;
				try {
					int statusCode = httpUrlConn.getResponseCode();
					if (statusCode > 300) {
						inputStream = httpUrlConn.getErrorStream();
					}
				} catch(javax.net.ssl.SSLProtocolException spe) {
					if (spe.getMessage().contains("unrecognized_name")) {
						printLine("Caused by a stupid Java 7 bug, that eventually should go away if Oracle gets off its @$$a");
						printLine("Try setting: jsse.enableSNIExtension = false in the java environment", spe);
					}
				}
			}
		}
		this.addConnection(inputStream);

		File contentFile = null;
		printLine("***HEADERS START***");
		Map<String, List<String>> headers = urlConn.getHeaderFields();
		Set<String> keys = headers.keySet();
		for (String key : keys) {
			printIt(key + ": ");
			List<String> values = headers.get(key);
			for (String value : values) {
				printIt(value + " :: ");
				if (value.toLowerCase().startsWith("attachment")) {// handle
																	// files
					try {
						String strAttachmentsDir = params.configurationDirectory + File.separator + "attachments";
						contentFile = new File(strAttachmentsDir);
						if (!contentFile.exists()) {
							contentFile.mkdir();
						}

						String filePath = null;
						try {
							String filename = value.split("=")[1];
							filePath = strAttachmentsDir + File.separator + filename;
						} catch (IndexOutOfBoundsException npe) {
							filePath = strAttachmentsDir + File.separator + "unknown";
						}
						contentFile = new File(filePath);
					} catch (Exception en) {
						printLine("Could not create attachment file ");
					}
				}

			}
			printLine();
		}
		printLine("***HEADERS STOP***");

		if (contentFile == null) {// Not an attachment so save default
			contentFile = new File(startDir);
			if (!contentFile.exists()) {
				contentFile.mkdir();
			}

			// Determine the extension of the file
			String extension = "content.sbc";
			if (headers.get("Content-Type") != null) {
				extension = headers.get("Content-Type").get(0).replace('/', '.').split(";")[0];
			} else if (headers.get("content-type") != null) {
				extension = headers.get("content-type").get(0).replace('/', '.').split(";")[0];
			}

			contentFile = new File(startDir + File.separator + this.getClass().getName() + "." + fileDateStamp
					+ "." + extension);
		}

		/**
		 * Handle content type of serialized java object bu putting it in the
		 * global map
		 */
		List<String> contentTypes = headers.get("Content-Type");
		if (contentTypes == null) {
			contentTypes = headers.get("content-type");
		}
		if (contentTypes != null && contentTypes.contains("application/x-java-serialized-object")) {
			ObjectInputStream deserializer = new ObjectInputStream(inputStream);
			Object object = deserializer.readObject();
			deserializer.close();
			this.addToGlobalMap(this.getClass().getSimpleName() + getTime(), object);
			return;
		}

		byte[] content = null;

		List<String> tranferEncodings = headers.get("Transfer-Encoding");
		if (tranferEncodings != null && tranferEncodings.get(0).equals("chunked")) {
			content = new byte[0];
			BufferedInputStream bis = new BufferedInputStream(inputStream, 8192);
			byte[] buffer = new byte[8192];
			int read = 0;
			while ((read = bis.read(buffer)) != -1) {
				//add buffer to end of content
				content = Utilities.concatenateByteArrays(content, Arrays.copyOf(buffer, read));
			}
		} else {// handle NOT chunked
			// inputStream.read(content);
			if (urlConn != null && urlConn.getContentLength() > 0) {
				content = new byte[urlConn.getContentLength()];
				BufferedInputStream bis = new BufferedInputStream(inputStream, 8192);
				byte[] buffer = new byte[8192];
				int read;
				while ((read = bis.read(buffer)) != -1) {
					content = Utilities.concatenateByteArrays(content, Arrays.copyOf(buffer, read));
//					content = (Arrays.toString(content) + Arrays.toString(Arrays.copyOf(buffer, read))).getBytes();
//					content = ArrayUtils.addAll(content, ArrayUtils.subarray(buffer, 0, read));
				}
			} else if (inputStream != null && inputStream.available() > 0) {
				content = new byte[inputStream.available()];
				BufferedInputStream bis = new BufferedInputStream(inputStream, 8192);
				byte[] buffer = new byte[inputStream.available()];
				int read;
				while ((read = bis.read(buffer)) != -1) {
					content = Utilities.concatenateByteArrays(content, Arrays.copyOf(buffer, read));
//					content = (Arrays.toString(content) + Arrays.toString(Arrays.copyOf(buffer, read))).getBytes();
//					content = ArrayUtils.addAll(content, ArrayUtils.subarray(buffer, 0, read));
				}
			}

		}
		if (content != null) {
			FileOutputStream fos = new FileOutputStream(contentFile);
			this.addConnection(fos);
			String toDisplay = new String(content);
			printIt("Writing content to file ");
			printIt(contentFile.getPath());
			fos.write(content);
			fos.close();
			printLine(" ... done");
			if (contentFile.length() < params.getMaxFileOpenSize()) {// TODO: Handle
																		// this:
																		// Content-Type:
																		// image/gif
				if (headers.get("Content-Type") != null || headers.get("content-type") != null) {

					// Determine content type
					List<String> ctHeaders = headers.get("Content-Type");
					if (ctHeaders == null) {
						ctHeaders = headers.get("content-type");
					}
					for (String header : ctHeaders) {
						if (header.contains("application/json")) {
//							this.newTab(toDisplay.trim(), "json", "text/plain", false);
							//TODO add json pretty print capability to Utilities
							try {
								String uglyJson = toDisplay.trim();
//								String uglyJson = toDisplay.substring((content.length / 2) - 1).trim();
								String prettyJson = UtilitiesTools.jsonPrettyPrint(uglyJson);
								this.newTab(prettyJson, "json", "text/plain", false);
							} catch(com.google.gson.JsonSyntaxException jse){
								printLine("Bad JSON syntax", jse);
								jse.printStackTrace();
							}
						} else if (header.contains("text/xml")) {
							this.newTab(Utilities.xmlPrettyPrint(Utilities.getFileString(contentFile.getAbsolutePath()), 5),
									"xml", "text/plain", false);
						} else if (header.contains("application/xml")) {
							this.newTab(Utilities.xmlPrettyPrint(Utilities.getFileString(contentFile.getAbsolutePath()), 5),
									"xml", "text/plain", false);
						} else if (header.contains("text/html")) {
							this.newTab(Utilities.getFileString(contentFile.getAbsolutePath()), "Browser", "text/html",
									false);
						}
					}
				}
				this.newTab(toDisplay, "content", "text/plain", false);
//				this.newTab(toDisplay.substring((content.length / 2) - 1), "content", "text/plain", false);
			} else {
				printIt("Content file size: " + contentFile.length());
				printLine(" which is larger than maximum of " + params.getMaxFileOpenSize());
			}
			printLine("Content file saved as:\n" + contentFile.getPath());

			String filepath = startDir + File.separator + this.getClass().getName() + "." + fileDateStamp
					+ ".sbf";
			Utilities.saveFileString(filepath, this.textArea.getText());
			printLine("Tool ouput saved as:\n" + filepath);

		}
		// END TEST
	}
}
