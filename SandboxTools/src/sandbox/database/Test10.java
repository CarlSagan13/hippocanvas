package sandbox.database;

import java.sql.Connection;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Query and tabulate results",
 		parameterDescriptions = {
 				SandboxParameters.TRUEFALSE_NAME, "refresh",
 				SandboxParameters.YESNO_NAME, "tabulate",
 				"15", "query",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test10 extends SandboxTestBase {
	/**
	 * Instantiate for dynamic loading
	 */
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		boolean refresh = getBoolean("refresh");
		String query = getString("query");
		boolean tabulate = getBoolean("tabulate");
		Database.query(query, (Connection)this.getGlobalMapThing(Connection.class), this, tabulate, refresh);
		//END TEST
	}
}
