package sandbox.database;

import sandbox.SandboxTestBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;

public class Database {
	private static int MAX_REC_NUM = 64000;
	public static String HEADERKEY = "headers";

	public static void shutdown(Connection connection) throws SQLException {
		Statement st = connection.createStatement();
		// db writes out to files and performs clean shuts down
		// otherwise there will be an unclean shutdown
		// when program ends
		st.execute("SHUTDOWN");
		connection.close();// if there are no other open connection
	}

	// use for SQL commands CREATE, DROP, INSERT and UPDATE
	public static synchronized void update(String expression,
			Connection connection) throws SQLException {
		Statement st = null; 
		st =connection.createStatement();// statements
		int i = st.executeUpdate(expression);// run the query
		if (i == -1){
			System.out.println("db error : " + expression);
		}

		st.close();
	} // void update ()
	
	public static synchronized HashMap <String, String[]> query(String expression, Connection connection, SandboxTestBase test) 
		throws SQLException {
		return query(expression, connection, test, true);
	}

	public static synchronized HashMap<String, String[]> query(String expression, Connection connection,
			SandboxTestBase test, boolean tabulate) throws SQLException {
		return query(expression, connection, test, tabulate, true);
	}

	public static synchronized HashMap<String, String[]> query(String expression, Connection connection,
			SandboxTestBase test, boolean tabulate, boolean refresh) throws SQLException {
		return query(expression, connection, test, tabulate, refresh, true);
	}

	public static synchronized HashMap<String, String[]> query (String expression, Connection connection, SandboxTestBase test, boolean tabulate,
			boolean refresh, boolean showData) 
		throws SQLException {
		Statement statement = null; 
		ResultSet resultSet = null; 
		String[][] results = null; 
		HashMap<String, String []> map = null; 
		
		SandboxTestBase sandboxTestBase = (SandboxTestBase) test;
		
		String fileHashStamp = String.valueOf((connection.getMetaData().getURL().toString() + expression).hashCode());
		String filePath = sandboxTestBase.getParameters().getConfigurationDirectory() + System.getProperty("file.separator", "\\" + fileHashStamp + ".ser");
		
		try {
			if (refresh) {
				throw new Exception("Getting new data.");
			}
			map = (HashMap<String, String[]>) sandbox.Utilities.getFileObject(filePath);
			test.printLine("Using serialized file: " + filePath);
		} catch(Exception en) {
			test.printLine("Not using serialized file: " + en.getMessage());
			
			try {
				statement = connection.createStatement();
			} catch(NullPointerException npe) {
				((SandboxTestBase) test).printLine("You might not be connected to the database.");
				return null;
			}
			resultSet = statement.executeQuery(expression); 

			map = gatherResults(resultSet); 
			statement.close();
		}

		String[] headers = map.get(Database.HEADERKEY); 
		results = new String[map.size() - 1][headers.length];//Subtract 1 to allow for headers element
		int recordNumber = 0;
		String[] result = null; 
		while (( result = map.get(String.valueOf(recordNumber) )) != null ) {
			results[recordNumber++] = result;
		}
		if (tabulate) {
			test.tabulate(results, headers);
		} else {
			for (String header : headers) {
				((SandboxTestBase)test).printIt(header + "\t");
			}
			((SandboxTestBase)test).printLine();
			for (String[]row : results) {
				for (String value : row) {
					((SandboxTestBase)test).printIt(value + "\t");
				}
				((SandboxTestBase)test).printLine();
			}
		}
		
		return map;
	}
	
	private static HashMap<String, String[]> gatherResults(ResultSet resultSet)
		throws SQLException {
		HashMap<String, String[]> map = new HashMap<String, String[]>();
 		ResultSetMetaData meta = resultSet.getMetaData();
		int numCols = meta.getColumnCount();
		Object object = null; 

		int recordNumber = 0;
		String[] headings = new String[numCols];
		for (int i = 1; i <=  numCols; i++) { 			String columnName = meta.getColumnName(i);
			String columnTypeName = meta.getColumnTypeName(i);
			headings[i - 1] = columnName + " (" + columnTypeName + ")";
		}
		map.put(Database.HEADERKEY, headings);
		while ( resultSet.next()) {
			String[] result = new String[numCols];
			for (int i = 0; i < numCols; i++) { 				object = resultSet.getObject(i + 1);
				if (object == null) {
					result[i] = "null";
				} else {
 					if (meta.getColumnType(i + 1) == java.sql.Types.TIMESTAMP) {
						result[i] = resultSet.getTimestamp(i + 1).toString();
					} else {
						result[i] = object.toString();
					}
				}
			}
			map.put(String.valueOf(recordNumber++), result);
		}
		return map;
	}
}
