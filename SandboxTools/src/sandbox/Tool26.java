// Tool26.java (erase me later)
package sandbox;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;
import java.util.List;

import difflib.ChangeDelta;
import difflib.Chunk;
import difflib.DeleteDelta;
import difflib.Delta;

import difflib.DiffUtils;
import difflib.InsertDelta;
import difflib.Patch;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Compare text in clipboard to selected text",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "selectedText",
			SandboxParameters.TRUEFALSE_NAME, "showTabs",
			SandboxParameters.YESNO_NAME, "replaceInFocused"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool26 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {

		super.test(parameterDescriptions);
		//BEGIN TOOL
		String selectedText = getString("selectedText");
		boolean showTabs = getBoolean("showTabs");
		boolean replace = getBoolean("replaceInFocused");

		if (!replace){
			selectedText = params.getFocusedText();
		}

		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		java.awt.datatransfer.Transferable tText = clip.getContents(null);
		String clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);

		List<String> clipboardList = new ArrayList<String> ();
		for (String line : 
			clipboardText.split("\n")){
			//	clipboardText.split(System.getProperty("line.separator","\n"))){
			clipboardList.add(line);
		}
		addToReport("clipboardList size is "+ clipboardList.size());

		List<String> selectedList = new ArrayList<String> ();
		for (String line : 
			selectedText.split("\n")){
			//			selectedText.split(System.getProperty("line.separator","\n"))){
			selectedList.add(line);
		}
		addToReport("selectedList size is "+ selectedList.size());

		Patch<String> patch = DiffUtils.diff(clipboardList,selectedList);

		for (Delta<String> delta : patch.getDeltas()){
			if (delta instanceof InsertDelta){
				Chunk<String> revised = delta.getRevised();
				addToReport("INSERTED "+ revised.getLines().size()+ " lines at "+ revised.getPosition());
				for (String revision : revised.getLines()){
					addToReport("\t"+ revision);

				}
			}else if (delta instanceof ChangeDelta){
				Chunk<String> originals = delta.getOriginal();
				
//				byte[] baOriginal = new byte[Integer.parseInt(System.getProperty("sandbox.conf.maxByteArraySize", "10000"))];
				StringBuffer sbOriginal = new StringBuffer();
				StringBuffer sbRevised = new StringBuffer();
				
				
				
				addToReport("CHANGED:");
				addToReport("\tORIGINAL "+ originals.getLines().size()+ " lines at "+ originals.getPosition());
				for (String original : originals.getLines()){
					addToReport("\t\t"+ original);
					sbOriginal.append(original);
				}
				Chunk<String> revised = delta.getRevised();
				addToReport("\tREVISED "+ revised.getLines().size()+ " lines at "+ revised.getPosition());
				for (String revision : revised.getLines()){
					addToReport("\t\t"+ revision);
					sbRevised.append(revision);
				}
				printLine(UtilitiesTools.bytesToHex(sbOriginal.toString().getBytes()));
				printLine(UtilitiesTools.bytesToHex(sbRevised.toString().getBytes()));
			}else if (delta instanceof DeleteDelta){
				Chunk<String> originals = delta.getOriginal();
				addToReport("DELETED ("+ originals.getLines().size()+ " lines at "+ originals.getPosition()+ ")");
				for (String original : originals.getLines()){
					addToReport("\t"+ original);
				}
			}else {
				addToReport(delta.toString());
			}
		}

		this.newTextTab(report.toString(),"Dif", null);

		if (showTabs){
			this.newTab(clipboardText,"Clp");
			this.newTab(selectedText,"Sel");
		}
		//END TOOL
	}
	private StringBuffer report = new StringBuffer();
	private void addToReport(String s){
		report.append(s).append('\n');
	}
}
