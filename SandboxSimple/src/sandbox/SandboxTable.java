//SandboxTable.java (erase me later)
package sandbox;

import java.io.File;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class SandboxTable extends JTable implements ISandboxBackedUp, SandboxCommon {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private File file = null;
	private Object mapId = null;

	protected boolean isDirty = false;

	protected boolean text = false;

	protected boolean isEditable = false;
	public SandboxTable(){
	}

	public SandboxTable(DefaultTableModel model){
		super(model);
	
		model.addTableModelListener(new TableModelListener (){
			@Override
			public void tableChanged(TableModelEvent e){
				isDirty = true;
			}
		
		});
	}

	public SandboxTable(File file){
		this();
		this.file = file;
		sandboxLogger.info("Opening file in "+ file.getPath());
	}

	public File getFile(){
		return file;
	}
	public void setFile(File file){
		this.file = file;
	}
	
	public Object getMapId() {
		return this.mapId;
	}
	public void setMapId(Object mapId) {
		this.mapId = mapId;
	}
	
//*////////////////////////////////////DMH	
	public boolean addRow() {
		if (!isEditable) {
			return false;
		} else {
			if (this.getModel() instanceof TestBaseTableModel) {
				TestBaseTableModel model = (TestBaseTableModel) this.getModel();
				int selectedRow = this.getSelectedRow();
				int selectedCol = this.getSelectedColumn();
				int selectedRowInModel = this.getRowSorter().convertRowIndexToModel(selectedRow);
				Vector vector = (Vector)model.getDataVector().elementAt(selectedRowInModel);
				Vector newVector = (Vector)vector.clone();
				model.insertRow(selectedRowInModel, newVector);
//				model.insertRow(this.getSelectedRow(), (Vector)model.getDataVector().elementAt(this.getSelectedRow()));
				this.setDirty(true);
				return true;
			}
			return false;
		}
	}
/*///////////////////////////////////////	
	public boolean addRow() {
		if (!isEditable) {
			return false;
		} else {
			if (this.getModel() instanceof TestBaseTableModel) {
				TestBaseTableModel model = (TestBaseTableModel) this.getModel();
				Vector vector = (Vector)model.getDataVector().elementAt(this.getSelectedRow());
				
				Vector newVector = (Vector)vector.clone();
				
				model.insertRow(this.getSelectedRow(), newVector);
//				model.insertRow(this.getSelectedRow(), (Vector)model.getDataVector().elementAt(this.getSelectedRow()));
				this.setDirty(true);
				return true;
			}
			return false;
		}
	}
//*///////////////////////////////////////	
	
	public boolean removeRow() {
		if (!isEditable) {
			return false;
		} else {
			if (this.getModel() instanceof TestBaseTableModel) {
				TestBaseTableModel model = (TestBaseTableModel) this.getModel();
				int selectedRow = this.getSelectedRow();
				int selectedCol = this.getSelectedColumn();
				int selectedRowInModel = this.getRowSorter().convertRowIndexToModel(selectedRow);
				if (selectedRow >= 0 && selectedCol >= 0) {
					model.removeRow(selectedRowInModel);
					this.setDirty(true);
					return true;
				}
				return false;
			}
			return false;
		}
	}

	public boolean removeLastColumn() {
		if (!isEditable) {
			return false;
		} else {
			if (this.getModel() instanceof TestBaseTableModel) {
				TestBaseTableModel model = (TestBaseTableModel) this.getModel();
				model.setColumnCount(model.getColumnCount() - 1);
				return true;
			}
			return false;
		}
	}

	
	
//*///////////////////////////////////////////////////////////////////DMH
	public boolean addColumn() {
		if (!isEditable) {
			return false;
		} else {
			if (this.getModel() instanceof TestBaseTableModel) {
				TestBaseTableModel model = (TestBaseTableModel) this.getModel();

				Vector headers = new Vector(); //build headers vector
				for (int colIndex = 0; colIndex < model.getColumnCount(); colIndex++) { //grab existing headers
					headers.addElement(model.getColumnName(colIndex));
				}
				headers.addElement("new"); //Add the new column header
				
				Vector data = model.getDataVector(); //grab the current data
				Vector dataClone = (Vector)data.clone(); //make a copy
				for (int index = 0; index < dataClone.size(); index++) { //Add column to row data
					Vector rowVec = (Vector)dataClone.get(index);
					rowVec.add("whatever");
				}
				
				model = new TestBaseTableModel(dataClone, headers);
				this.setModel(model);

//				model.setColumnCount(model.getColumnCount() + 1);
//				model.addColumn(new TableColumn());
//				this.addColumn(new TableColumn());
				
//				Vector<String> headers = new Vector<String>(); //build headers vector
//				for (int colIndex = 0; colIndex < model.getColumnCount(); colIndex++) { //grab existing headers
//					headers.addElement(model.getColumnName(colIndex));
//				}
//				headers.addElement("new"); //Add the new column header
//				
//				Vector data = model.getDataVector(); //grab the current data
//				Vector dataClone = (Vector)data.clone(); //make a copy
//				for (int index = 0; index < dataClone.size(); index++) { //Add column to row data
//					Vector rowVec = (Vector)dataClone.get(index);
//					rowVec.add("whatever");
//				}
//				model.setDataVector(dataClone, headers); //Set the table model data to new data
				
				return true;
			}
			return false;
		}
	}
	
	@Override
	public void columnAdded(TableColumnModelEvent e) {
//		System.err.println(e.getFromIndex());
//		System.err.println(e.getToIndex());
//		System.err.println(e.getSource().getClass().getName());
		super.columnAdded(e);
	}
//*//////////////////////////////////////////////////////////////////////
	
	public String getBackupText(){
		StringBuffer buffer = new StringBuffer();
		Enumeration<TableColumn> enumeration = getTableHeader ()
			.getColumnModel().getColumns();
		enumeration.nextElement();//skip sandbox-added first column
		while (enumeration.hasMoreElements()){
			buffer.append(enumeration.nextElement().getHeaderValue())
				.append(",");		
		}
		buffer.deleteCharAt(buffer.length()-1);//get rid of final comma 		
		buffer.append("\n");
		TableModel model = getModel();
		for (int row = 0;row < model.getRowCount ();row++){
			for (int column = 0; column < model.getColumnCount()- 1; column++){
				String cellValue = (String)model.getValueAt(row,column);
				cellValue = cellValue.replaceAll(System.getProperty("line.separator", "\n"), "<SBNL>");
				
				
				
				buffer.append(
						cellValue
							.replaceAll(
									",",
									SandboxParameters.SANDBOX_COMMA_DELIMITER))
									.append(",");
			}
			buffer.deleteCharAt(buffer.length()- 1);
			buffer.append("\n");
		}
		return buffer.toString();
	}
	
	public void saveBackedUp() throws SandboxException {
		if (file != null){
			if (file.setWritable(true)){
				if (isDirty()){
					try {
						Utilities.saveFileString(file.getPath(), getBackupText());
					} catch (Exception en){
						throw new SandboxException("Could not save "+ file.getPath());
					}
				}
			} else {
				throw new SandboxException("Could not save "+ file.getPath());
			}
		} else {
			//oh well!
		}
	}
	
	@Override
	public void close() {
		try {
			saveBackedUp();
			closeBackedUp();
		} catch (SandboxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeBackedUp() throws SandboxException {
		if (isEditable() && isDirty) {
			saveBackedUp();
		}
		if (isEditable() && file != null) {
			file.setWritable(true);
		}
	}

	@Override
	public boolean isDirty(){
		return isDirty;
	}
	@Override
	public void setDirty(boolean dirty){
		isDirty = dirty;
	}
	@Override
	public boolean isText(){
		return text;
	}
	@Override
	public boolean isEditable(){
		//TODO Auto-generated method stub
		return isEditable;
	}
	/**
	*@param isEditable the isEditable to set 	*/
	public void setEditable(boolean isEditable){		
		this.isEditable = isEditable;
	}
}
//page break