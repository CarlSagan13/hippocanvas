//Command.java (erase me later)
package sandbox;

import java.io.File;
import java.io.IOException;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = "Commands",
		parameterDescriptions = 	{
				SandboxParameters.FOCUSED_NAME,"focused1,focused2,focused3",
				SandboxParameters.TRUEFALSE_NAME,"showErrorOut"
		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = false)

public class Command extends SandboxTestBase /*dmh327 */{
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TOOL
		boolean showErrorOut = getBoolean("showErrorOut");
		String commandName = parameters.commandName; // TODO this is a temporary
														// fix
		printLine("COMMAND: " + commandName);
		String focused1 = getString("focused1");
		String focused2 = getString("focused2");
		String focused3 = getString("focused3");

		String osName = System.getProperty("os.name", "Windows").split(" ")[0];
		String commandsDirectoryPath = params.userDir + File.separator + SandboxParameters.SANDBOX_COMMAND_DIRECTORY
				+ File.separator + osName;
		commandsDirectoryPath = System.getProperty(COMMANDS_DIR_ENV, commandsDirectoryPath);

		String popPadFilePath = commandsDirectoryPath + File.separator + commandName;
		String parameterName = commandName;
		printLine("File used:");
		printLine("\t" + popPadFilePath);

		if (parameters.showPopPad.isSelected()) {
			SandboxTextAreaFrame frame = new SandboxTextAreaFrame(this, parameterName, popPadFilePath, parameters.poppadMaxLines);
			
			try {
				while (frame.isVisible()) {
					Thread.sleep(100);
				}
			} catch (InterruptedException ie) {
				printLine("Wait for input was interrupted.");
			} finally {
				resetStartTime();
			}
			
		} else {
			try {
				this.getPopPadValues().put(parameterName, Utilities.getFileString(popPadFilePath));
			} catch (IOException ioe) {
				parameters.printLine("Could not load Pop Pad: " + popPadFilePath);
			}
		}
		String poppadValue = Utilities.noNulls(this.getPopPadValues().get(parameterName));

		poppadValue = parameters.replaceValues(poppadValue);

		int indexOfPopPadNotes = poppadValue.indexOf(SandboxParameters.POPPAD_NOTES);
		if (indexOfPopPadNotes > 0) {
			poppadValue = poppadValue.substring(0, poppadValue.indexOf(SandboxParameters.POPPAD_NOTES));
			poppadValue = removePoppadComments(poppadValue);
		}

		String[] commandParameters = poppadValue.split("\n");

		String command = commandParameters[0];
		for (int i = 1; i < commandParameters.length; i++) {
			if (commandParameters[i].contains("=")) {
				String name = commandParameters[i].split("=")[0];
				String value = commandParameters[i].split("=")[1].trim();
				command = command.replaceAll(name, value);
			}
		}
		command = command.replace(SandboxParameters.SANDBOX_FOCUSED + "1", Utilities.noNulls(focused1));
		command = command.replace(SandboxParameters.SANDBOX_FOCUSED + "2", Utilities.noNulls(focused2));
		command = command.replace(SandboxParameters.SANDBOX_FOCUSED + "3", Utilities.noNulls(focused3));
		command = command.replace(SandboxParameters.SANDBOX_FOCUSED, Utilities.noNulls(focused1));

		boolean noTab = false;
		if (command.startsWith(NO_TAB)) {
			noTab = true;
			command = command.substring(NO_TAB.length());
		}

		setTooltip(command.replaceAll("\\r|\\n", ""));
		if (this.isRunning) {
			String[] commandWithWorkspace = command.split(SandboxParameters.SANDBOX_COMMAND_WORKSPACE);
			if (commandWithWorkspace.length == 1) {
				this.runInSeparateProcess(command.replaceAll("\\r|\\n", ""), true, showErrorOut);
			}
			if (commandWithWorkspace.length == 2) {
				command = commandWithWorkspace[0];
				File workspace = new File(commandWithWorkspace[1].trim());
				if (workspace.exists() && workspace.isDirectory()) {
					printLine("Using workspace: " + workspace.getPath());
					this.runInSeparateProcess(command.replaceAll("\\r|\\n", ""), workspace, true, showErrorOut);
				} else {
					printLine("Invalid workspace: " + commandWithWorkspace[1].trim());
				}
			}
		}

		if (noTab) {
			parameters.tabbedPane.remove(getTabNumber());
		}
		// END TOOL
	}

	private static String NO_TAB = "NOTAB::";
	protected static String COMMANDS_DIR_ENV = "sbx.commands.directory"; 
}
