//Tool33.java (erase me later)
package sandbox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Start SSL page server", 
		parameterDescriptions = 
		{ 
			SandboxParameters.FOCUSED_NAME,	"startEndPorts", 
			SandboxParameters.POPPAD_NAME, "headers", 
			SandboxParameters.POPPAD_NAME, "enabledProtocols",
			SandboxParameters.POPPAD_NAME, "enabledCipherSuites", 
			SandboxParameters.TRUEFALSE_NAME, "showSupportedProtocolsCiphers",
			SandboxParameters.YESNO_NAME, "needClientAuth" 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class Tool33 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		int[] startEndPorts = getIntegerArray("startEndPorts");
		String headers = getString("headers");
		String[] enabledProtocols = getStringArray("enabledProtocols");
		String[] enabledCipherSuites = getStringArray("enabledCipherSuites");
		boolean showSupportedProtocolsCiphers = getBoolean("showSupportedProtocolsCiphers");
		boolean needClientAuth = getBoolean("needClientAuth");

		// Find open port
		boolean success = false;
		int port = 0;
		ServerSocket serverSocket = null;

		int portIndex = startEndPorts[0];
		int portLimit = startEndPorts[1];
		while (!success) {
			try {
				serverSocket = new ServerSocket(portIndex);
				addConnection(serverSocket);
				printLine("Listening on port " + portIndex);
				success = true;
				port = portIndex;
			} catch (IOException ioe) {
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			}
		}
		serverSocket.close();

		SSLServerSocketFactory sslServerSocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		SSLServerSocket sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(port);
		sslServerSocket.setNeedClientAuth(needClientAuth);
		addConnection(sslServerSocket);

		if (showSupportedProtocolsCiphers) {
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("Supported Protocols:");
			printLine(sslServerSocket.getSupportedProtocols());
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("Supported Cipher Suites:");
			printLine(sslServerSocket.getSupportedCipherSuites());
			printLine(SandboxParameters.VISUAL_SEPARATOR);
		}

		if (enabledProtocols.length > 0) {
			sslServerSocket.setEnabledProtocols(enabledProtocols);
		}
		if (enabledCipherSuites.length > 0) {
			sslServerSocket.setEnabledCipherSuites(enabledCipherSuites);
		}

		while (this.isRunning) {
			SSLSocket sslSocket = (SSLSocket) sslServerSocket.accept();
			printLine(this.getTime() + ": " + this.getTestName() + " accepting message.");
			printLine(sslSocket.getInetAddress());
			try {
				printLine("DN = " + sslSocket.getSession().getPeerCertificateChain()[0].getSubjectDN());
			} catch (Exception en) {
				printLine("Could not get DN because ", en);
			}
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			ClientThread clientThread = new ClientThread(sslSocket, headers, this);
			clientThread.start();
		}
		sslServerSocket.close();
		// END TEST
	}

	class ClientThread extends Thread {
		SSLSocket client;
		String headers;
		SandboxTestBase test;

		ClientThread(SSLSocket client, String headers, SandboxTestBase test) {
			this.client = client;
			this.headers = headers;
			this.test = test;
		}

		@Override
		public void run() {
			try {
				// Get streams to talk to the client
				InputStream inputStream = client.getInputStream();
				InputStreamReader inputstreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputstreamReader);
				OutputStream out = client.getOutputStream();
				PrintWriter pout = new PrintWriter(out);

				List<String> contentParameters = showHeaders(bufferedReader);
				String filePath = "/";
				if (contentParameters.size() > 0) {
					filePath = contentParameters.remove(0);
					int contentInputLength = 0;

					Map<String, List<String>> map = getHeaderMap(contentParameters);
					List<String> contentLengths = map.get("Content-Length");
					if (contentLengths != null && contentLengths.size() > 0) {
						contentInputLength = Integer.valueOf(contentLengths.get(0));
					}
					List<String> contentTypes = map.get("Content-Type");
					if (contentTypes != null && contentTypes.size() > 0) {
						if (contentTypes.contains("application/x-java-serialized-object")) {
							ObjectInputStream ois = new ObjectInputStream(inputStream);
							try {
								Object objContent = ois.readObject();
								String mapId = "content" + new Date().getTime();
								printLine("Putting object in global map with ID: " + mapId);
								replaceInGlobalMap(mapId, objContent);
								contentInputLength = 0;
							} catch (ClassNotFoundException e) {
								// TODO Autogenerated catch block
								printLine("Problem with getting serialized object", e);
							}
						}
					}

					if (contentInputLength > 0) {
						byte[] contentInput = new byte[contentInputLength];
						inputStream.read(contentInput, 0, contentInputLength);
						String contentName = getTestName() + "."
								+ (new SimpleDateFormat(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP))
										.format(new Date());
						String contentFilePath = getParameters().configurationDirectory + File.separator
								+ "files" + File.separator + contentName;
						printLine("Saving received content to global map with ID " + contentName);
						test.addToGlobalMap(contentName, contentInput);
						printLine("Saving received content to " + System.getProperty("line.separator", "\n") + "\t" + contentFilePath);
//						Utilities.saveFileObject(contentFilePath, contentInput);
						Utilities.saveFileString(contentFilePath, contentInput);
					}

					printLine("Input content length = " + contentInputLength);

					String[] filePathParts = filePath.split(" ", 4);

					//String[] charsetNames = { "US-ASCII", "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE", "UTF-16" };
					filePath = URLDecoder.decode(filePathParts[1], "US-ASCII"); //To allow reference to filenames with space characters
				}

				if (filePath.trim().length() == 0) {
					printLine("No file path specified");
				} else {
					printLine("filePath is " + filePath);
				}
				
				byte[] contentOutput = null;
				boolean displayTypeText = true;
				if (filePath.endsWith("shutdown")) {
					closeConnections();
					setIsRunning(false);
					contentOutput = "Shutting down".getBytes();
				} else {
					try {
						File file = Utilities.getResourceFile(filePath);
						if (file.isDirectory()) {
							StringBuffer contentStringBuffer = new StringBuffer();
							if (filePath.contentEquals("/")) {
								filePath = "";
							}
							for (String fileName : file.list()) {
								String fileDirectory = "F\t";
								if (Utilities.getResourceFile(filePath + "/" + fileName).isDirectory()) {
									fileDirectory = "D\t";
								}
								contentStringBuffer.append(fileDirectory).append(fileName).append("\n");
							}
							contentOutput = contentStringBuffer.toString().getBytes();
							headers = headers.concat("\n" + "Content-Type: text/plain");
						} else {
							contentOutput = sandbox.Utilities.getResourceFileBytes(filePath);
							printLine("Resource size: " + contentOutput.length);
							if (!headers.contains("Content-Type") && filePath.contains(".")) {
								String mimeType = filePath.substring(filePath.lastIndexOf(".") + 1);
								printLine("mimeType");
								
								switch(mimeType) {
								case "png":
									headers = headers.concat("\n" + "Content-Type: image/png");
									displayTypeText = false;
									
								case "jpg":
									headers = headers.concat("\n" + "Content-Type: image/jpeg");
									displayTypeText = false;
									
								case "jpeg":
									headers = headers.concat("\n" + "Content-Type: image/jpeg");
									displayTypeText = false;
									
								case "gif":
									headers = headers.concat("\n" + "Content-Type: image/gif");
									displayTypeText = false;
									
								case "js":
									headers = headers.concat("\n" + "Content-Type: application/javascript");
									displayTypeText = false;
									
								case "json":
									headers = headers.concat("\n" + "Content-Type: application/json");
									break;
									
								case "xml":
									headers = headers.concat("\n" + "Content-Type: application/xml");
									break;
									
								case "zip":
									headers = headers.concat("\n" + "Content-Type: application/zip");
									break;
									
								case "pdf":
									headers = headers.concat("\n" + "Content-Type: application/pdf");
									break;
									
								case "sql":
									headers = headers.concat("\n" + "Content-Type: application/sql");
									break;
									
								case "doc":
									headers = headers.concat("\n" + "Content-Type: application/msword (.xls)");
									displayTypeText = false;
									break;
									
								case "docx":
									headers = headers.concat("\n" + "Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document(.docx)");
									displayTypeText = false;
									break;
									
								case "xls":
									headers = headers.concat("\n" + "Content-Type: application/vnd.ms-excel (.xls)");
									displayTypeText = false;
									break;
									
								case "xlsx":
									headers = headers.concat("\n" + "Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (.xlsx)");
									displayTypeText = false;
									break;
									
								case "ppt":
									headers = headers.concat("\n" + "Content-Type: application/vnd.ms-powerpoint (.ppt)");
									displayTypeText = false;
									break;
									
								case "pptx":
									headers = headers.concat("\n" + "Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation (.pptx)");
									displayTypeText = false;
									break;
									
								case "unknown":
									headers = headers.concat("\n" + "Content-Type: multipart/form-data");
									break;
									
								case "css":
									headers = headers.concat("\n" + "Content-Type: text/css");
									break;
									
								case "html":
									headers = headers.concat("\n" + "Content-Type: text/html");
									break;
									
								case "htm":
									headers = headers.concat("\n" + "Content-Type: text/html");
									break;
									
								case "csv":
									headers = headers.concat("\n" + "Content-Type: text/csv");
									break;
									
								default:
									headers = headers.concat("\n" + "Content-Type: text/plain");
									break;
								}
							} else {
								printLine("*** ContentType is predefined in poppad ***");
							}
						}
					} catch(Throwable cnfe) {
						contentOutput = "File not found".getBytes();
					}
				}

				headers = headers.concat("\n" + "Content-Length: " + contentOutput.length);

				try {
					out.write(headers.trim().getBytes());
					out.write("\n\n".getBytes());
					if (displayTypeText) {
						pout.println(new String(contentOutput));
					} else {
						out.write(contentOutput);
					}
				} catch (Exception en) {
					printLine(this.getClass().getName() + ": " + en.getClass().getSimpleName());
					for (StackTraceElement element : en.getStackTrace()) {
						printLine(element.toString());
					}
				} finally {
					pout.close();
					bufferedReader.close();
					client.close();
				}
					
				pout.close();
				bufferedReader.close();
				client.close();

			} catch (IOException ioe) {
				printLine(ioe.getClass().getName() + ": " + ioe.getMessage());
				for (StackTraceElement element : ioe.getStackTrace()) {
					printLine(element.toString());
				}
			}
		}
	}

	List<String> showHeaders(BufferedReader bufferedReader) throws IOException {
		String line = null;

		List<String> contentParameters = new ArrayList<String>();
		printLine("*****************HEADERS START");
		while ((line = bufferedReader.readLine()) != null) {
			if (line.length() == 0)
				break;
			printLine("HEADER: " + line);
			contentParameters.add(line);
		}
		printLine("******************HEADERS DONE ");
		return contentParameters;

	}

	Map<String, List<String>> getHeaderMap(List<String> listHeaders) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String header : listHeaders) {
			String[] nameValue = header.split(": ", 2);
			String name = nameValue[0];
			String value = nameValue[1];
			List<String> list = map.get(name);
			if (list == null) {// header is new
				list = new ArrayList<String>();
			}
			list.add(value);
			map.put(name, list);
		}
		return map;
	}

}
