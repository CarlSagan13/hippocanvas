//SandboxKeyListener.java (erase me later)
package sandbox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.TextComponent;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.Timer;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class SandboxKeyListener implements KeyListener {
	private SandboxParameters params = null;
	private String searchString = null;
	// private Stack<KeyEvent> keyStack = new Stack <KeyEventXLP>);
	private Component componentToSearch = null;

	// private Stack<String> textStack = new Stack <StringXLP>);
	public SandboxKeyListener(SandboxParameters params) {
		this.params = params;
	}

	public SandboxKeyListener(SandboxParameters params, Component componentToSearch) {
		this.params = params;
		this.componentToSearch = componentToSearch;
	}

	private void handleFunctionKey(KeyEvent event, String text, String envName) throws Throwable {
		String decoded = "UNKNOWN";
		String type = text;
		if (text.contains(SandboxParameters.sandboxDelimiter)) {
			type = text.split(SandboxParameters.sandboxDelimiter, 2)[0];
			text = text.split(SandboxParameters.sandboxDelimiter, 2)[1];
			if (text.startsWith(SandboxParameters.SANDBOX_FUNCTION_BASE64 + ":")) {
				decoded = Utilities
						.base64Decode(text.substring((SandboxParameters.SANDBOX_FUNCTION_BASE64 + ":").length()));
			} else {
				decoded = URLDecoder.decode(text, "US-ASCII");
			}
		}
		Object focused = event.getComponent();
		switch (type) {
		case SandboxParameters.SANDBOX_FUNCTION_TEXT:
			if (focused instanceof SandboxTextArea) {
				SandboxTextArea textArea = (SandboxTextArea) focused;
				Document doc = textArea.getDocument();
				SandboxTestBase test = textArea.getTest();
				if (test != null) {
					test.printLine(decoded);
				} else {
					int start = textArea.getCaretPosition();
					int selStart = textArea.getSelectionStart();
					int selEnd = textArea.getSelectionEnd();
					if (selStart == selEnd) {
						doc.insertString(start, decoded, null);
					} else {
						// replace the selected part
						textArea.replaceSelection(decoded);
					}
				}
			} else if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
				JTextComponent textComponent = (JTextComponent) focused;
				Document doc = textComponent.getDocument();
				if (!textComponent.isEditable()) {
					return;
				}

				int start = textComponent.getCaretPosition();
				int selStart = textComponent.getSelectionStart();
				int selEnd = textComponent.getSelectionEnd();
				if (selStart == selEnd) {
					doc.insertString(start, decoded, null);
				} else { // replace the selected part
					textComponent.replaceSelection(decoded);
				}
			} else if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
				System.err.println("It's a combobox!");
				JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
				int itemCount = comboBox.getItemCount();
				Set<String> entries = new HashSet<String>();
				// String[]stringArray = new String[itemCount];
				for (int i = 0; i < itemCount; i++) {
					String value = (String) comboBox.getItemAt(i);
					// stringArray[i]= value;
					entries.add(value);
				}
				if (entries.contains(decoded)) {
					comboBox.setSelectedItem(decoded);
				} else {
					comboBox.addItem(decoded);
					comboBox.setSelectedItem(decoded);
				}
			} else {
				System.out.println("Focus on an editable component is required for this function");
			}
			break;
		case SandboxParameters.SANDBOX_FUNCTION_SET:
			try {
				String value = params.getFocusedText();
				if (value != null && value.length() > 0) {
					if (Utilities.showConfirmDialog("Do you want to set the property: " + envName, params) == 0) {
						System.setProperty(envName,
								SandboxParameters.SANDBOX_FUNCTION_TEXT + SandboxParameters.sandboxDelimiter + URLEncoder.encode(value, "US-ASCII"));
					}
				}
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case SandboxParameters.SANDBOX_FUNCTION_UNSET:
			try {
				String value = params.getFocusedText();
				if (Utilities.showConfirmDialog("Do you want to unset the property: " + envName, params) == 0) {
					System.clearProperty(envName);
				} else {
					System.setProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				}
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case SandboxParameters.SANDBOX_FUNCTION_TEST:
			try {
				params.runTest(decoded);

			} catch (ArrayIndexOutOfBoundsException aioobe) {
				params.printLine("Not the correct number of parameters used");
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case SandboxParameters.SANDBOX_FUNCTION_TIME:
			String timeStamp = Utilities.getTime(decoded);
			if (focused instanceof SandboxTextArea) {
				SandboxTextArea textArea = (SandboxTextArea) focused;
				SandboxTestBase test = textArea.getTest();
				if (test != null) {
					test.printLine(timeStamp);
				} else {
					int start = textArea.getCaretPosition();
					textArea.getDocument().insertString(start, timeStamp, null);
				}
			} else if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
				JTextComponent textComponent = (JTextComponent) focused;
				if (!textComponent.isEditable()) {
					return;
				}

				int start = textComponent.getCaretPosition();
				textComponent.getDocument().insertString(start, timeStamp, null);
			} else {
				System.out.println("Focus on an editable component is required for this function");
			}
			break;
		case SandboxParameters.SANDBOX_FUNCTION_FIND:
			if (componentToSearch == null) {
				params.highlightSearchedTextInTab(decoded, params.getSelectedTabComponent(), true);
			} else {
				params.highlightSearchedTextInTab(decoded, componentToSearch, true);
			}
			break;
		default:
		}
	}

	public void keyPressed(KeyEvent event) {
		
//		params.printLine(event.getComponent().getClass().getName());
		
		int keyCode = event.getKeyCode();
		if (event.getComponent() instanceof ISandboxBackedUp) {
			ISandboxBackedUp backedUp = (ISandboxBackedUp) event.getComponent();
			if (backedUp.isEditable()) {
				backedUp.setDirty(true);
			}
		}

		// Function key events
		switch (keyCode) {
		case KeyEvent.VK_F1:
			try {
				String envName = "sbx.kbd.f1";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F2:
			try {
				String envName = "sbx.kbd.f2";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F3:
			try {
				String envName = "sbx.kbd.f3";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F4:
			try {
				String envName = "sbx.kbd.f4";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F5:
			try {
				String envName = "sbx.kbd.f5";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F6:
			try {
				String envName = "sbx.kbd.f6";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F7:
			try {
				String envName = "sbx.kbd.f7";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F8:
			try {
				String envName = "sbx.kbd.f8";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F9:
			try {
				String envName = "sbx.kbd.f9";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F10:
			try {
				String envName = "sbx.kbd.f10";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F11:
			try {
				String envName = "sbx.kbd.f11";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		case KeyEvent.VK_F12:
			try {
				String envName = "sbx.kbd.f12";
				String text = System.getProperty(envName, SandboxParameters.SANDBOX_FUNCTION_SET);
				handleFunctionKey(event, text, envName);
			} catch (Throwable en) {
				params.printLine(en);
			}
			break;
		default:
		}

		// ===============================================================================
		// Alt key events
		if (event.isAltDown()) {
			switch (keyCode) {
			case KeyEvent.VK_UP:
//				System.err.println("here i am in " + this.getClass());

				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent) focused;
						Font currentFont = textComponent.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 1);
						textComponent.setFont(newFont);
					} else if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
						System.err.print(JTable.AUTO_RESIZE_ALL_COLUMNS);
						System.err.print("::");
						System.err.print(JTable.AUTO_RESIZE_LAST_COLUMN);
						System.err.print("::");
						System.err.print(JTable.AUTO_RESIZE_NEXT_COLUMN);
						System.err.print("::");
						System.err.print(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
						System.err.print("::");
						System.err.println(table.getAutoResizeMode());
						
						Font currentFont = table.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 1);
						table.setFont(newFont);
					} else if (focused instanceof SandboxList) {
						SandboxList sandboxList = (SandboxList) focused;
	    				int fixedCellHeight = sandboxList.getFixedCellHeight();
						sandboxList.setFixedCellHeight(fixedCellHeight + 1);
						Font currentFont = sandboxList.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 1);
						sandboxList.setFont(newFont);
					} else {
						System.out.println("Focus on text component is required for this function");
					}
		
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_DOWN:
				System.err.println("here i am in " + this.getClass());

				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent) focused;
						Font currentFont = textComponent.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() - 1);
						textComponent.setFont(newFont);
					} else if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
						Font currentFont = table.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() - 1);
						table.setFont(newFont);
					} else if (focused instanceof SandboxList) {
						SandboxList sandboxList = (SandboxList) focused;
	    				int fixedCellHeight = sandboxList.getFixedCellHeight();
						sandboxList.setFixedCellHeight(fixedCellHeight - 1);
						Font currentFont = sandboxList.getFont();
						Font newFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() - 1 );
						sandboxList.setFont(newFont);
					} else {
						System.out.println("Focus on text component is required for this function");
					}
		
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_V: //Print path with sandbox directory and home replaced
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent) focused;
//						params.runTest(textComponent.getSelectedText());
						
						Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
//						java.awt.datatransfer.Transferable tText = new StringSelection(params.replaceValues(searchString));
//						clip.setContents(tText, null);
						java.awt.datatransfer.Transferable tText = new StringSelection(params.replaceValues(searchString));
						tText = clip.getContents(tText);
						String clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);
						
//						String replaced = params.sandboxHome; //SANDBOXHOME
//						replaced = params.configurationDirectory; //SANDBOXDIRECTORY
						
						String replaced = clipboardText
								.replace(params.configurationDirectory, "SANDBOXDIRECTORY")
								.replace(params.sandboxHome, "SANDBOXHOME");
				
						
						int start = textComponent.getCaretPosition();
						textComponent.getDocument().insertString(start, replaced, null);

						
					} else {
						System.out.println("Focus on " + SandboxTextArea.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_R: //Run a test
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent) focused;
						params.runTest(textComponent.getSelectedText());
					} else {
						System.out.println("Focus on " + SandboxTextArea.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_A: //Add a row to a table
				try {
					Object focused = event.getComponent();
					if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
						table.addRow();
					} else {
						System.out.println("Focus on " + SandboxTable.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_D: //Remove a row from a table
				params.printLine(SandboxParameters.UNDER_CONSTRUCTION + " dmh344, you pressed Alt-D", true, true);

				try {
					Object focused = event.getComponent();
					if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
						table.removeRow();
					} else {
						System.out.println("Focus on " + SandboxTable.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_L: //Add a column to a table
				params.printLine(SandboxParameters.UNDER_CONSTRUCTION + " dmh344, you pressed Alt-L", true, true);

				try {
					Object focused = event.getComponent();
					if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
	
						    table.setAutoCreateColumnsFromModel(false);
						    DefaultTableModel model = (DefaultTableModel)table.getModel();
						    TableColumn col = new TableColumn(model.getColumnCount());

						    String headerLabel = "new";
						    Vector values = new Vector();
						    int vColIndex = table.getSelectedColumn();
						    col.setHeaderValue(headerLabel);
						    table.addColumn(col);
						    model.addColumn(headerLabel.toString(), values);
						    table.moveColumn(table.getColumnCount()-1, vColIndex);
						    table.setDirty(true);
					} else {
						System.out.println("Focus on " + SandboxTable.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_M: //Remove last column
				params.printLine(SandboxParameters.UNDER_CONSTRUCTION + " dmh344, you pressed Alt-M", true, true);

				try {
					Object focused = event.getComponent();
					if (focused instanceof SandboxTable) {
						SandboxTable table = (SandboxTable) focused;
						
						    table.setAutoCreateColumnsFromModel(false);
						    DefaultTableModel model = (DefaultTableModel)table.getModel();
						    model.setColumnCount(model.getColumnCount() - 1);
						    table.updateUI();
						    table.setDirty(true);
					} else {
						System.out.println("Focus on " + SandboxTable.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			default:
			}
		}
		
		// ===============================================================================
		// Control key events
		if (event.isControlDown()) {
			switch (keyCode) {
			case KeyEvent.VK_D:
				break;
			case KeyEvent.VK_B: // Go back to last tab
//				if (params.tabHistory.size() > 0) {
//					List<Integer> tabHistoryLocal = params.tabHistory; //for debugging
//					System.err.println("Trying to remove " + params.tabHistory.get(0));
//					params.getTabbedPane().setSelectedIndex(params.tabHistory.remove(0));
//				}
//				System.err.println(params.lastSelectedTab);
//				int selectedTab = params.lastSelectedTab;
//				params.getTabbedPane().setSelectedIndex(params.lastSelectedTab);
//				params.lastSelectedTab = selectedTab;
				break;

			case KeyEvent.VK_H:
				System.err.println("here i am in " + this.getClass());

				try {
					Object focused = event.getComponent();
					String hourMinute = Utilities.getTime("HH:mm:ss");
					if (focused instanceof SandboxTextArea) {
						SandboxTextArea textArea = (SandboxTextArea) focused;
						SandboxTestBase test = textArea.getTest();

						if (test != null) {
							test.printLine(hourMinute);
						} else {
							int start = textArea.getCaretPosition();
							textArea.getDocument().insertString(start, hourMinute, null);
						}
					} else if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
						JTextComponent textComponent = (JTextComponent) focused;
						if (!textComponent.isEditable()) {
							return;
						}

						int start = textComponent.getCaretPosition();
						textComponent.getDocument().insertString(start, hourMinute + "", null);
					} else {
						System.out.println("Focus on an editable component is required for this function");
					}
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
			case KeyEvent.VK_E: // Edit a file
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
						JTextComponent textComponent = (JTextComponent) focused;

						String[] resources = Utilities.noNulls(textComponent.getSelectedText())
								.split(System.getProperty("line.separator", "\n"));
						for (String resource : resources) {
							SandboxOpen sandboxOpen = new SandboxOpen(params, params.replaceValues(resource), true);
//							params.addToGlobalMap(sandboxOpen);
							Thread thread = new Thread(sandboxOpen);
							thread.setName("Editing" + resource);
//							params.addToGlobalMap(thread);
							thread.start();
//							Thread.sleep(10);
						}
					} else if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
						JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
						String resource = (String) comboBox.getSelectedItem();
						SandboxOpen sandboxOpen = new SandboxOpen(params, params.replaceValues(resource), true);
//						params.addToGlobalMap(sandboxOpen);
						Thread thread = new Thread(sandboxOpen);
						thread.setName("Editing " + resource);
//						params.addToGlobalMap(thread);
						thread.start();
					} else {
						System.out.println("Focus on an editable component is required for this function");
					}
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_I: // Run a test
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextComponent) {
						JTextComponent textComponent = (JTextComponent) focused;
						params.runTest(textComponent.getSelectedText());
					} else {
						System.out.println("Focus on " + SandboxTextArea.class.getSimpleName()
								+ " component is required for this function");
					}

				} catch (ArrayIndexOutOfBoundsException aioobe) {
					params.printLine("Not the correct number of parameters used");

				} catch (Throwable en) {
					params.printLine(en);
				}
				break;
				
			case KeyEvent.VK_J: // Replace all in clipboard - grouping
				Object focusedObjectJ = params.getFocusedObject();
				params.printLine("Replace all in clipboard with grouping");
				if (focusedObjectJ != null) {
					String regex = "", preSplitString = "";
					JTextComponent textComponent = (JTextComponent) params.getSelectedTabComponent();
					
					//Search/Replace combobox
					if (focusedObjectJ instanceof JTextField) {
						JTextField textField = (JTextField) focusedObjectJ;
						JComboBox comboBox = (JComboBox) textField.getParent();
						if (comboBox.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
							regex = (String) params.searchComboBox.getSelectedItem();
							preSplitString = params.replaceValues((String) params.replaceComboBox.getSelectedItem());
						}
					} else if (focusedObjectJ instanceof JTextComponent) {
						regex = textComponent.getText();
					}
					String[] replace = preSplitString.split(",");
					
					Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
					java.awt.datatransfer.Transferable tText = clip.getContents(null);
					
					String clipboardText = null;
					try {
						clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);
						if (clipboardText != null && clipboardText.split("\n").length == 2) {
							regex = clipboardText.split("\n")[0];
							preSplitString = params.replaceValues(clipboardText.split("\n")[1]);
						}
					} catch (UnsupportedFlavorException | IOException e) {
						// TODO Auto-generated catch block
						params.printLine(e.getMessage());
						e.printStackTrace();
					}
					
					StringBuffer passedText = new StringBuffer(clipboardText);
					StringBuffer newText = new StringBuffer();
					
					if (regex.matches(".*\\(.*\\).*")) {
						Utilities.grabReplaceRegex(regex, replace, passedText, newText);
						
						//put newText in clipboard
						
						
//						Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
						java.awt.datatransfer.Transferable tTextNew = new StringSelection(params.replaceValues(searchString));
						clip.setContents(tTextNew, null);
	
						
						
						
						
//						textComponent.selectAll();
//						textComponent.replaceSelection(newText.toString());
//						textComponent.grabFocus();
					} else { //regex not grouped so advise
						params.printLine("Use grouped regex");
					}
					
				}
				break;
				
			case KeyEvent.VK_O: // Open a resource
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
						JTextComponent textComponent = (JTextComponent) focused;

						String selectedText = Utilities.noNulls(textComponent.getSelectedText());
						String[] resources = params.replaceValues(selectedText).split("\n");
						List<String> list = new ArrayList<String>(resources.length);
						for (String string : resources) {
							list.add(string);
						}
						SandboxOpen sandboxOpen = new SandboxOpen(params, list);
						Thread thread = new Thread(sandboxOpen);
						thread.setName("Opening " + list.size() + " files ...");
						thread.start();
					} else if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
						JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
						String resource = (String) comboBox.getSelectedItem();
						SandboxOpen sandboxOpen = new SandboxOpen(params, params.replaceValues(resource), false);
						Thread thread = new Thread(sandboxOpen);
						thread.setName("Editing " + resource);
						thread.start();
					} else {
						System.out.println("editable component is required for this function");
					}
				} catch (Throwable en) {
					params.printLine(en);
					System.err.println(en);
				}
				break;

			case KeyEvent.VK_M: // Real-time timestamp mark
				try {
					Object focused = event.getComponent();
					if (focused instanceof SandboxTextArea) {
						SandboxTextArea textArea = (SandboxTextArea) focused;
						SandboxTestBase test = textArea.getTest();
						String mark = SandboxParameters.VISUAL_SEPARATOR + "\n" + Utilities.getTime()
								+ " :: SANDBOX MANUAL MARK" + "\n" + SandboxParameters.VISUAL_SEPARATOR + "\n";
						if (test != null) {
							test.printLine(mark);
						} else {
							int start = textArea.getCaretPosition();
							textArea.getDocument().insertString(start, mark, null);
						}
					} else {
						System.out.println("Focus on an " + SandboxTextArea.class.getSimpleName()
								+ " component is required for this function");
					}
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_N: // Replace all
				Object focusedObject = params.getFocusedObject();
				params.printLine("Replace all");

				if (focusedObject != null) {
					String regex = "", preSplitString = "";
					JTextComponent textComponent = (JTextComponent) params.getSelectedTabComponent();
					
					//Search/Replace combobox
					if (focusedObject instanceof JTextField) {
						JTextField textField = (JTextField) focusedObject;
						JComboBox comboBox = (JComboBox) textField.getParent();
						if (comboBox.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
							regex = (String) params.searchComboBox.getSelectedItem();
							preSplitString = params.replaceValues((String) params.replaceComboBox.getSelectedItem());
						}
					} else if (focusedObject instanceof JTextComponent) {
						Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
						java.awt.datatransfer.Transferable tText = clip.getContents(null);
						
						String clipboardText = null;
						try {
							clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);
							if (clipboardText != null && clipboardText.split("\n").length == 2) {
								regex = clipboardText.split("\n")[0];
								preSplitString = params.replaceValues(clipboardText.split("\n")[1]);
							}
						} catch (UnsupportedFlavorException | IOException e) {
							// TODO Auto-generated catch block
							params.printLine(e.getMessage());
							e.printStackTrace();
						}
						if (!params.getSelectedTabComponent().equals(focusedObject)) {
							//Undocked
							textComponent = (JTextComponent) focusedObject;
							params.printLine("Undocked", true, true);
						}
					}
					String[] replace = preSplitString.split(",");
					StringBuffer passedText = new StringBuffer(textComponent.getText());
					StringBuffer newText = new StringBuffer();
					newText.append(passedText.toString().replaceAll(regex, preSplitString));
					
					textComponent.selectAll();
					textComponent.replaceSelection(newText.toString());
					textComponent.grabFocus();
				}
				break;

			case KeyEvent.VK_P: // Replace all - grouping
				Object focusedObjectP = params.getFocusedObject();
				params.printLine("Replace all with grouping");
				if (focusedObjectP != null) {
					String regex = "", preSplitString = "";
					JTextComponent textComponent = (JTextComponent) params.getSelectedTabComponent();
					
					//Search/Replace combobox
					if (focusedObjectP instanceof JTextField) {
						JTextField textField = (JTextField) focusedObjectP;
						JComboBox comboBox = (JComboBox) textField.getParent();
						if (comboBox.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
							regex = (String) params.searchComboBox.getSelectedItem();
							preSplitString = params.replaceValues((String) params.replaceComboBox.getSelectedItem());
						}
					} else if (focusedObjectP instanceof JTextComponent) {
						Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
						java.awt.datatransfer.Transferable tText = clip.getContents(null);
						
						String clipboardText = null;
						try {
							clipboardText = (String)tText.getTransferData(DataFlavor.stringFlavor);
							if (clipboardText != null && clipboardText.split("\n").length == 2) {
								regex = clipboardText.split("\n")[0];
								preSplitString = params.replaceValues(clipboardText.split("\n")[1]);
							}
						} catch (UnsupportedFlavorException | IOException e) {
							// TODO Auto-generated catch block
							params.printLine(e.getMessage());
							e.printStackTrace();
						}
						if (!params.getSelectedTabComponent().equals(focusedObjectP)) {
							//Undocked
							textComponent = (JTextComponent) focusedObjectP;
							params.printLine("Undocked", true, true);
						}
					}
					String[] replace = preSplitString.split(",");
					StringBuffer passedText = new StringBuffer(textComponent.getText());
					StringBuffer newText = new StringBuffer();
					
					if (regex.matches(".*\\(.*\\).*")) {
						Utilities.grabReplaceRegex(regex, replace, passedText, newText);
						
						textComponent.selectAll();
						textComponent.replaceSelection(newText.toString());
						textComponent.grabFocus();
					} else { //regex not grouped so advise
						params.printLine("Use grouped regex");
					}
					
				}
				break;

			case KeyEvent.VK_U: // Print a visual divider
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
						JTextComponent textComponent = (JTextComponent) focused;
						if (!textComponent.isEditable()) {
							break;
						}

						int start = textComponent.getCaretPosition();
						textComponent.getDocument().insertString(start, SandboxParameters.VISUAL_SEPARATOR, null);
					} else {
						System.out.println("Focus on an editable component is required for this function");
					}
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_T: // Tab
				try {
					Object focused = event.getComponent();
					if (focused instanceof JTextArea || focused instanceof SandboxTextPane) {
						JTextComponent textComponent = (JTextComponent) focused;
						if (!textComponent.isEditable()) {
							break;
						}
						int start = textComponent.getSelectionStart();
						String[] strings = textComponent.getSelectedText().split("\n");
						StringBuffer buffer = new StringBuffer();
						
/*///////////////////////
						boolean useMeta = (System.getProperty("sbx.use.meta") != null);
						for (String string : strings) {
							if (event.isShiftDown()) {
								buffer.append(string.replaceFirst("[ \t*]", "")).append("\n");
							} else {
								buffer.append("\t").append(string).append("\n");
							}
						}
/*///////////////////////						
						for (String string : strings) {
							if (event.isShiftDown()) {
								buffer.append(string.replaceFirst("[ \t*]", "")).append("\n");
							} else {
								buffer.append("\t").append(string).append("\n");
							}
						}
//*///////////////////////						
						
						
						textComponent.replaceSelection(buffer.toString());
						textComponent.select(start, start + buffer.length());
					} else {
						System.out.println("Focus on an editable component is required for this function");
					}
				} catch (NullPointerException npe) {
					params.printLine("You must select text before you can tab it");
				} catch (Throwable en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_F: // Find
				if (searchString == null) {
					searchString = params.getFocusedText();
				}
				if (componentToSearch == null) {
					params.highlightSearchedTextInTab(searchString, params.getSelectedTabComponent(), false);
				} else {
					params.highlightSearchedTextInTab(searchString, componentToSearch, false);
				}
				break;

			case KeyEvent.VK_R: // Find regular expression
				int numRepeat = 1;
				if (event.isShiftDown()) {
					params.hideMessages();
					String strNth = JOptionPane.showInputDialog("Find nth occurrence");
					params.showMessages();
					try {
						numRepeat = Integer.valueOf(strNth);
					} catch(NumberFormatException nfe) {
						numRepeat = 0;
						params.alert("Invalid occurence number '" + strNth + "' entered", params.iconStopped);
					}
				}
				try {
					if (event.getComponent() != null 
							&& event.getComponent().getClass().getEnclosingClass() != null
							&& event.getComponent().getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
						JComboBox comboBox = (JComboBox) ((JComponent) event.getComponent()).getParent();
						System.err.println("dmh421 " + comboBox.getName());
						if (comboBox.getName().equals(SandboxParameters.REPLACE_COMBOBOX_NAME)) {
							params.alert("Use the Search combobox instead", params.iconStopped);
							return;
						}
					}
				} catch (InstantiationException | IllegalAccessException | SecurityException e) {
					params.printLine("Regex search exception: " + e.getMessage());
					e.printStackTrace();
				}
				
				if (searchString == null) {
					searchString = params.getFocusedText();
				}
				while(numRepeat != 0) {
					if (componentToSearch == null) {
						params.highlightSearchedTextInTab(searchString, params.getSelectedTabComponent(), true);
					} else {
						params.highlightSearchedTextInTab(searchString, componentToSearch, true);
					}
					numRepeat--;
				}
				break;

			case KeyEvent.VK_S: // Put string with replacements in system
								// clipboard
				if (searchString == null) {
					searchString = params.getFocusedText();
				}
				Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
				java.awt.datatransfer.Transferable tText = new StringSelection(params.replaceValues(searchString));
				clip.setContents(tText, null);
				break;
			case KeyEvent.VK_G: // Go to line n and highlight o characters
				if (searchString == null) {
					searchString = params.getFocusedText();
				}

				int row = 0;
				int col = 0;
				int pos = -1;
				try {
					String[] rowCol = searchString.split(",");
					if (rowCol.length == 1) { //Just the line number provided
//						pos = Integer.parseInt(rowCol[0]);
						row = Integer.parseInt(rowCol[0]);
						col = 20; //Just to highlight a little of the line
					}
					if (rowCol.length == 2) {
						row = Integer.parseInt(rowCol[0]);
						col = Integer.parseInt(rowCol[1]);
					}
				} catch (Exception en) {
					params.printLine(en);
				}
				if (pos >= 0) {
					if (componentToSearch == null) {
						params.goToInTab(pos, params.getSelectedTabComponent());
					} else {
						params.goToInTab(pos, componentToSearch);
					}
				} else {
					if (componentToSearch == null) {
						params.goToInTab(row, col, params.getSelectedTabComponent());
					} else {
						params.goToInTab(row, col, componentToSearch);
					}
				}
				break;

			case KeyEvent.VK_L: // List combobox entries
				try {
					Object focused = params.getFocusedObject();
					if (focused != null && focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
						JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
						SandboxListFrame slf = new SandboxListFrame(comboBox, null, params, "Choose from list");
						slf.setAlwaysOnTop(true);
						params.addToGlobalMap(slf);
					} else {
						System.out.println("Focus on a parameter is required for this function");
					}
				} catch (Exception en) {
					params.printLine(en);
				}
				break;

			case KeyEvent.VK_V:
				try {
					if (event.isShiftDown()) {
						Utilities.showConfirmDialog("Can't paste with Caps Lock ON", params);
					}
				} catch (Exception en) {
					params.printLine(en);
				}
				break;

			default:
			}

		}
	}

	public void keyTyped(KeyEvent event) {
		int keyCode = event.getKeyCode();
		if (event.getComponent() instanceof ISandboxBackedUp) {
			ISandboxBackedUp backedUp = (ISandboxBackedUp) event.getComponent();
			if (backedUp.isEditable()) {
				backedUp.setDirty(true);
			}
		}
	}

	public void keyReleased(KeyEvent event) {
		int keyCode = event.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_CONTROL:
			this.searchString = null;
		}
	}
}
// page break