package sandbox;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.Position;

public class SandboxShell extends SandboxThread {
	
	static String SBXPROCMAPID = "sbxproc";	
	static private String MARK_ENTRY = "sbxmarkentry";
	static private String SANDBOX_EDIT = "sbxedit";
	List<String> forbiddenRegex = new ArrayList<String>();

	List<String> history = new ArrayList<String>();	int historyPointer = 0;
	String strHistory = "sbxhistory";
	StringBuilder command = new StringBuilder();	StringBuilder lastCommand = null;
	StringBuilder prompt = new StringBuilder();
	InputStream inputStream = null;
	BufferedReader bufferedReader = null;
	BufferedReader errorBufferedReader = null;
	BufferedWriter procStdIn = null;

	OutputStream outputStream = null;
	OutputStreamWriter outputStreamWriter = null;
	InputStream errorStream = null;
	String strProcessEnd = "exit";

	String mapId = String.valueOf((new Date().getTime()));

	String lineSeparator = System.getProperty("line.separator","\n");

	ShowProcessOutput showProcOut = null;

	SandboxShellKeyListener shellKeyListener = null;

	String baseHost = "host";
	String baseUser = "user";
	boolean canIExit = false;
	private String filePath = null;
	private int frequencyInMilliseconds = 100;	private int windowSize = 1000;
	private boolean isRunning = false;
	private boolean entryCommandNext = false;

	public void run() {
		isRunning = true;
	}
	
	public SandboxShell() {
		try {
			//Set up the process
			SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		
			//Get formidden commands
			for (String commandRegex : parameters.properties.getProperty("forbiddenShellCommands", "\\bping").split(",")) {	
				forbiddenRegex.add(commandRegex);
			}
		
			for (int index=0; index < 100; index++) {
				String tryMapId = SBXPROCMAPID + index;
				if (parameters.getGlobals().getMap().get(tryMapId)== null){
					mapId = tryMapId;
					break;
				}
			}
			SandboxConnection connection = Utilities.createSandboxConnection(mapId);
			if (connection.getOwner()!= null){
				parameters.printLine("Process " + mapId + " in use by "+ connection.getOwner());
				throw new sandbox.SandboxException("Process "+ mapId + "in use by "+ connection.getOwner());
			}
			initStreams();
		
			//Get baseline
			baseHost = uname();
			baseUser = whoami();
			prompt = getPrompt();
			
			SandboxTextArea textArea = new SandboxTextArea(this);
		
			try {
				String[] clf = System.getProperty("sandbox.conf.shellFont").split("_");
				Font shellFont = new Font(clf[0], Integer.parseInt(clf[1]),Integer.parseInt(clf[2]));
				textArea.setFont(shellFont);
			} catch(Exception en){
				System.out.println("sandbox.conf.shellFont not set correctly (<Font Name>_<Type>_<Size> e.g.'"+ SandboxParameters.FONT_DEFAULT + "')");
			}
			textArea.setBackground(Color.black);
			textArea.setForeground(Color.white);
		
			textArea.setEditable(false);
		
			shellKeyListener = new SandboxShellKeyListener(this);
			textArea.addKeyListener(shellKeyListener);
			DefaultCaret caret = (DefaultCaret)textArea.getCaret();
			caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			textArea.append("Starting Sandbox Shell " + lineSeparator + prompt);
		
			parameters.newTab(textArea, mapId);
			parameters.tabbedPane.setSelectedIndex(parameters.tabbedPane.getTabCount() - 1);		
			try {
				String historyString = Utilities.getFileString(parameters.getConfigurationDirectory() + File.separator + "history.sbf");
				String[] historyArray = historyString.split("\n");
				for (String historyCommand : historyArray){
					history.add(historyCommand);
				}
				this.historyPointer = history.size ();
			} catch(IOException ioe){
				parameters.printLine("Could not load shell history",true,true);
			}
//			startBlinkingCursor(textArea);		
		}catch(Exception en){
			System.err.println("Could not start "+ SandboxShell.class.getName()+ ": "+ en);
		}
	}

	private void initStreams() {
		SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		Process proc = (Process)parameters.getGlobals().getMap().get(mapId).getObj();
		inputStream = proc.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		bufferedReader = new BufferedReader(inputStreamReader);
	
		errorStream = proc.getErrorStream();		InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
		errorBufferedReader = new BufferedReader(errorStreamReader);
	
		outputStream = proc.getOutputStream();		outputStreamWriter = new OutputStreamWriter(outputStream);
		procStdIn = new BufferedWriter(outputStreamWriter);
	}
	
	private StringBuilder getPrompt() {
		StringBuilder prompt = new StringBuilder();		
		try {
			String user = whoami();
			prompt.append("[").append(user).append("@");
		
			String host = uname();
			prompt.append(host);
		
			prompt.append(pwd()).append("]");
		
			if (user.equals(baseUser)&&host.equals(baseHost)){
				this.canIExit = false;
			}else {
				this.canIExit = true;
			}
		
		}catch(Exception en){
			prompt = new StringBuilder("[undetermined]");
			System.err.println(en);
		}
		return prompt;
	}

	private String whoami() {
		try {
			procStdIn.write("whoami");
			procStdIn.newLine();
			procStdIn.flush();
			return bufferedReader.readLine();
		}catch(Exception en){
			System.err.println(en);
			return "user";
		}
	}

	private String uname() {
		try {
			if (Utilities.isWindows()) {
				return "hostname";
			} else {
				procStdIn.write("uname -n");
				procStdIn.newLine();
				procStdIn.flush();
				String line = bufferedReader.readLine();
				return line.indexOf('.') > 0 ? line.substring(0, line.indexOf('.')) : line;
			}
		}catch(Exception en){
			System.err.println(en);
			return "host";
		}
	}

	private String pwd() {
		try {
			if (Utilities.isWindows()) {
				return "dir";
			} else {
				procStdIn.write("pwd");
				procStdIn.newLine();
				procStdIn.flush();
				String line = bufferedReader.readLine();
				return line.substring(line.lastIndexOf ('/'));
			}
		} catch(Exception en){
			System.err.println(en);
			return "dir";
		}
	}

	private String killLastProcess() {
		try {
			procStdIn.write("ps -af | grep '" + history.get(historyPointer)+ "'");
			procStdIn.newLine();
			procStdIn.flush();
			String line = bufferedReader.readLine();
			return line.split("")[1];
		
		} catch(Exception en){
			System.err.println(en);
			return "dir";
		}
	}

	public void keyReturn(SandboxTextArea textArea) {	
		SandboxParameters params = SandboxParameters.createSandboxParameters();
		SandboxConnection connection = params.getGlobals ().getMap().get(mapId);
		if (connection.getOwner()!= null){
			textArea.append(lineSeparator);
			textArea.append("Process "+ mapId + "already in use!");
			textArea.append(lineSeparator);
			return;
		}
		try {
			if (inputStream.available() > 0) {
				textArea.append(lineSeparator);
				textArea.append("PROCESS DIRTY: ");
				new Thread(new ShowProcessOutput(this, textArea)).start();
				command = new StringBuilder();
				return;
			}
		} catch (IOException e1){
			//TODO Auto-generated catch block
			textArea.append(lineSeparator);
			textArea.append("PROCESS MALFUNCTION! ");
			e1.printStackTrace();
			command = new StringBuilder();
			return;
		}
	
		textArea.append(lineSeparator);
	
		if (command.length()== 0){
			textArea.append(this.prompt.toString ());
			return;
		}
	
		if (command.toString().equals(MARK_ENTRY)){//an entry is coming so flag it for next around
			entryCommandNext = true;
			textArea.append(this.prompt.toString ());
			command = new StringBuilder();
			return;
		}
	
		for (String cmd : forbiddenRegex){
			if (command.toString().matches("A[a-z]{0,10}()"+ cmd + "().*| /'"+ cmd + "()?.*")){
				textArea.append("'"+ command.toString()+ "' is forbidden (see "+ SandboxParameters.SANDBOX_WORKSPACE + "/system.xml for list)");
				textArea.append(lineSeparator);				textArea.append(prompt.toString());
				params.printLine(command + " is forbidden",true,true);
				command = new StringBuilder();				return;
			}
		}
	
		if (command.toString().equals(strHistory)){
			for (String singleCommand : history){				textArea.append(singleCommand);				textArea.append("\n");			}
			textArea.append(this.prompt.toString ());
			command = new StringBuilder();
			return;
		}
	
		if (command.toString().startsWith(SANDBOX_EDIT)){
			history.add(command.toString());
			historyPointer = history.size()-1;
			try {
				procStdIn.write(command.replace(0, SANDBOX_EDIT.length(), "less").toString());
				procStdIn.newLine();
				procStdIn.write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
				procStdIn.newLine();
				procStdIn.flush();
				
				CaptureProcessOutput processOutput = new CaptureProcessOutput(this);
				new Thread(processOutput).start();
			}catch(IOException ioe){
				textArea.append("Sandbox Edit Failed: "+ ioe);
			}
		
			textArea.append(this.prompt.toString ());
			command = new StringBuilder();
		
			return;
		}

		try {
			if (command.toString().equals(strProcessEnd)){//Exiting 			
				if (!this.canIExit){
					textArea.append(prompt.toString());
					command = new StringBuilder();
					params.printLine("Already at home base,so no more exiting!",true,true);
					return;
				}
			
				procStdIn.write(strProcessEnd);
				procStdIn.newLine();
				procStdIn.newLine();
				procStdIn.write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
				procStdIn.newLine();
				procStdIn.flush();
				showProcOut = new ShowProcessOutput(this,textArea);
				(new Thread(showProcOut)).start();
			
				history.add(command.toString());
				historyPointer = history.size() - 1;
			} else {
				procStdIn.write(command.toString ());
				procStdIn.newLine();
				if (entryCommandNext){
					String entry = JOptionPane.showInputDialog("Entry required");
					procStdIn.write(entry);
					procStdIn.flush();				}
				procStdIn.write("echo "+ SandboxParameters.SANDBOX_END_OF_FILE);
				procStdIn.newLine();
				procStdIn.flush();
			
				showProcOut = new ShowProcessOutput(this,textArea);
				(new Thread(showProcOut)).start();
			
				history.add(command.toString());
				historyPointer = history.size()- 1;
			
			}
		} catch(IOException ioe){
			System.err.println("sumptin bad");
			ioe.printStackTrace();
			isRunning = false;
		}
	
		lastCommand = command;
		command = new StringBuilder();
	
		entryCommandNext = false;
	}

	public void keyBackspace(SandboxTextArea textArea){
		if (command.length()> 0){
		command.deleteCharAt(command.length()- 1);
		int endOfText = textArea.getText().length();
		textArea.replaceRange("", endOfText - 1, endOfText);
		}
	}

	public void keyControl_V(SandboxTextArea textArea){
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		try {
			String strClip = (String)clip.getData (DataFlavor.stringFlavor);
			command.append(strClip);
			textArea.append(strClip);
		}catch(Exception en){
			System.err.println("Could not paste");
		}
	}

	public void keyControl_up(SandboxTextArea textArea) {
		if (historyPointer >=	0){
			int endOfText = textArea.getText().length();
			textArea.replaceRange("", endOfText - command.length(), endOfText);
			command = new StringBuilder(history.get (historyPointer--));
			textArea.append(command.toString());
		}
	}

	public void keyControl_down(SandboxTextArea textArea){
		if (historyPointer < history.size()- 1){
			int endOfText = textArea.getText().length();
			textArea.replaceRange("", endOfText - command.length(), endOfText);
			command = new StringBuilder(history.get (++historyPointer));
			textArea.append(command.toString());
		}
	}

	public void keyControl_C(){
		//don't do anything: system is taking care of it for you 	
	}
	
//public void keyControl_B(){
//	breakout = true;
//}
	
	public void key(SandboxTextArea textArea,char c){
		command.append(c);
		textArea.append(String.valueOf(c));
	}

	public void keyCommand(String command){
		this.command = new StringBuilder(command);
	}

/**
*@return the filePath
*/
public String getFilePath(){
	return filePath;
}
/**
*@param filePath the filePath to set
*/
public void setFilePath(String filePath){
	this.filePath = filePath;
}
/**
*@return the frequencyInMilliseconds
*/
public int getFrequencyInMilliseconds(){
	return frequencyInMilliseconds;
}
/**
*@param frequencyInMilliseconds the frequency InMilliseconds to set 	
**/
public void setFrequencyInMilliseconds(int frequencyInMilliseconds){
	this.frequencyInMilliseconds = frequencyInMilliseconds;
}
/**
*@return the windowSize
*/
public int getWindowSize(){
	return windowSize;
}
/**
*@param windowSize the windowSize to set
*/
public void setWindowSize(int windowSize){
	this.windowSize = windowSize;
}
/**
//page break
*@return the isRunning
*/
public boolean isRunning(){
	return isRunning;
}
/**
*@param isRunning the isRunning to set
*/
public void setRunning(boolean isRunning){
	this.isRunning = isRunning;
}

@Override
public void close(){




//*//////////
//////DMH
//	System.out.println(killLastProcess());
//	SandboxParameters parameters = SandboxParameters.createSandboxParameters();
//	Process proc = (Process)parameters 1.getGlobals().getMap().get(mapId).getObj();
//	if (showPr.ocOut.isHung){
//		SandboxShell shellThread = new SandboxShell();
//		new Thread(shellThread).start ();
//		return;
//	}
//*//////////
/////////

/*///////////
////////////////DMH
	try {
	
		procStdIn.	write(27);
		procStdIn.	newLine();
		procStdIn.	flush();
		procStdIn.	write("echo "+
//page break
SandboxParameters.SANDBOX_END_OF_FILE);
		procStdIn.newLine();
		procStdIn.flush();
		return;
	}catch (IOException e){
		//TODO Auto-generated catch block
		e.printStackTrace();
	}
//*//////////////////////////////	




	this.isRunning = false;
	try {
		shellKeyListener.setEnabled(false);
		SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		if (!this.showProcOut.isHung){
			bufferedReader.close();
			errorBufferedReader.close();				
			outputStreamWriter.close();
			procStdIn.close();
		}
		parameters.printLine(parameters.getGlobals ().getMap().get(mapId).close(), true, true);
		parameters.getGlobals().getMap().remove(mapId);
		StringBuffer historyStrings = new StringBuffer();
		for (String command : history){
			historyStrings.append(command).append("\n");
		}
		Utilities.saveFileString(parameters.getConfigurationDirectory()+ System.getProperty("file.separator","/")+ "history.sbf ",
				historyStrings.toString());
	} catch (IOException e){
		//TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	
	class SandboxShellKeyListener implements KeyListener {

		private SandboxShell shell;
		private boolean enabled = true;
		public SandboxShellKeyListener(SandboxShell shell){
			this.shell = shell;
		}
	
		@Override
		public void keyTyped(KeyEvent e){
		}
		@Override
		public void keyReleased(KeyEvent e){
		}
		@Override
		public void keyPressed(KeyEvent event){
			if (enabled){
				SandboxTextArea textArea = ((SandboxTextArea)event.getComponent());
			
				if (event.isControlDown()){//Control key events
					switch (event.getKeyCode()){
					case KeyEvent.VK_V:
						shell.keyControl_V(textArea);
						break;
//					case KeyEvent.VK_B:
//						shell.keyControl_B ();
//						break;
					case KeyEvent.VK_C:
						shell.keyControl_C();
						break;
					}
				} else {
					switch (event.getKeyCode()) {
						case KeyEvent.VK_ENTER:
							shell.keyReturn(textArea);
							break;
						case KeyEvent.VK_BACK_SPACE:
							shell.keyBackspace(textArea);						
							break;
						case KeyEvent.VK_SHIFT:
							break;
						case KeyEvent.VK_UP:
							shell.keyControl_up(textArea);
							break;
						case KeyEvent.VK_DOWN:
							shell.keyControl_down(textArea);				
							break;
						
						default:
							if (!event.isActionKey()){
								shell.key(textArea,event.getKeyChar());
							}	
					}		
				}		
			}			
		}				
		public void setEnabled(boolean enabled){
			this.enabled = enabled;
		}
	}

	class ShowProcessOutput implements Runnable {
		
		JTextArea textArea = null;
		SandboxShell shell = null;
		boolean isDone = false;
		boolean isHung = true;
	
		public ShowProcessOutput(SandboxShell shell, JTextArea textArea){
			this.shell = shell;
			this.textArea = textArea;
		}
		
		@Override
		public void run(){
			try {
				shell.shellKeyListener.enabled = false;
				Date startTime = new Date();
				String line = bufferedReader.readLine ();
				int count = 0;
				SandboxConnection connection = Utilities.createSandboxConnection(mapId);
				while (isRunning && line != null &&!line.contains(SandboxParameters.SANDBOX_END_OF_FILE)){
					textArea.append(line);
					textArea.append(lineSeparator);
					textArea.setCaretPosition(textArea.getText().length());
					System.out.println(line);
					line = bufferedReader.readLine ();

				}
				
				if (errorStream.available() > 0) {
					String errorLine = errorBufferedReader.readLine();
					textArea.append(errorLine);
					textArea.append(lineSeparator);
				}
					
				prompt = getPrompt();
				textArea.append(prompt.toString());
				isDone = true;
				isHung = false;
				connection.setOwner(null);
			} catch(IOException ioe) {
				if (showProcOut.isHung) {
					textArea.append("PROCESS IS HUNG\n");
				}
				textArea.append("COULD NOT READ OUTPUT: " + ioe + "\n");
				ioe.printStackTrace();
				return;//Don't enable hung text area
			}
			shell.shellKeyListener.enabled = true;		
		}
		public boolean isDone(){
			return isDone;
		}
	}

	class CaptureProcessOutput implements Runnable {
		
		SandboxShell shell = null;
		StringBuilder output = new StringBuilder();
		boolean isDone = false;
	
		public CaptureProcessOutput(SandboxShell shell){
			this.shell = shell;
		}
		
		@Override
		public void run(){
			SandboxParameters params = SandboxParameters.createSandboxParameters();
			try {
				Date startTime = new Date();
				String line = bufferedReader.readLine();
				int count = 0;
				SandboxConnection connection = Utilities.createSandboxConnection(mapId);
				while (isRunning && line != null && !line.contains(SandboxParameters.SANDBOX_END_OF_FILE)){
					output.append(line).append (lineSeparator);
					line = bufferedReader.readLine ();
	
					if ((10000 < count++)|| (startTime.getTime()+ (60 * 1000) < new Date().getTime())){
						if (JOptionPane.showConfirmDialog(textArea,"Continue background process? (If no, process will be killed)") != 0) {
							if (!connection.getOwner().equals(shell)){
								params.printLine ("Process " + mapId + " in use by " + connection.getOwner());
							} else {
								shell.close();
								return;
							}
						}
						count = 0;
						startTime = new Date();
					}	
				}
				
				if (errorStream.available() > 0) {
					String	errorLine = errorBufferedReader.readLine();
					params.printLine(errorLine);
				}
			
				isDone = true;
				connection.setOwner(null);
			}	catch(IOException	ioe)	{
				params.printLine("COULD NOT READ OUTPUT: " + ioe.getStackTrace());
			}
		
			params.newTab("The description",output.toString(),"The title");
		}
		
		public boolean isDone(){
			return isDone;
		}
	}

	
}
