// Tool35.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		description = "Migrate classpath and copy resources to destination",
	
parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"find,replace",
			SandboxParameters.SCRATCHPAD_NAME,"classpath",
			SandboxParameters.TRUEFALSE_NAME,"get"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool35 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String classpath = getString ("classpath");
		String find = getString("find");
		String replace = getString("replace");
		boolean get = getBoolean("get");
		
		if (classpath.length()== 0 || find.length()== 0 || replace.length() == 0){
			throw new SandboxException("Need classpath,find and replace");
		}
		
		String classpathDelimiter = System.getProperty("path.separator",";");

		Set <String> resources = new TreeSet<String>(Arrays.asList(classpath.split(classpathDelimiter)));

		StringBuffer newClasspath = new StringBuffer();

		List<String> newResources = new ArrayList<String>();

		for (String resource : resources){
			if (resource.startsWith(find)){
				String newResource = resource.replaceFirst(find.replace("\\","\\\\"),replace.replace("\\","\\\\"));
				newResources.add(newResource);
				if (get){
					File sourceFile = new File(resource);
					if (sourceFile.exists()&&sourceFile.isFile()){
						File targetFile = new File(newResource);
						File targetDirectory = new File(targetFile.getParent());
						if (!targetDirectory.exists()){
							targetDirectory.mkdirs();
						}
						sandbox.Utilities.copyFile(sourceFile,targetFile);
						
					} else {
						printLine("Not copying because not a file: " + sourceFile.getAbsolutePath());
					}
				}	
			} else {
				newResources.add(resource);
			}

		}
		resources = new TreeSet <String>(newResources);
		
		for (String resource : resources){
			newClasspath.append(resource);
			newClasspath.append(classpathDelimiter);

		}
		newClasspath.deleteCharAt(newClasspath.length()- 1);
		printLine(newClasspath.toString());
		//END TEST
	}

}
