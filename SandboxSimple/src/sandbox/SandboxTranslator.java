package sandbox;

public interface SandboxTranslator {
	void setVerbose(boolean verbose);
	void translate(Object obj, SandboxTestBase test, String depth) throws SandboxException;
	void translate(Object obj, SandboxTestBase test) throws SandboxException;
}
