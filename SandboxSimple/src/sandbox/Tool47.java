// Tool47.java (erase me later)
package sandbox;

import java.util.ArrayList;

import java.util.List;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = "Parse using regex delimiter and put list in global map",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"stringRegex",
//			SandboxParameters.SCRATCHPAD_NAME,"scratchPadString",
//			SandboxParameters.PASSWORD_NAME,"password",
//			SandboxParameters.POPPAD_NAME,"poppad",
			SandboxParameters.TRUEFALSE_NAME,"tabulate",
//			SandboxParameters.YESNO_NAME,"yesno"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool47 extends SandboxTestBase {

@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		String string = getStringArray("stringRegex")[0];
		String regex = getStringArray("stringRegex")[1];
		boolean tabulate = getBoolean("tabulate");
		
		String[] parts = string.split(regex);
		String[][] data = new String[parts.length][1];
		String[] headers = {"Parsed values"};

		List <String> list = new ArrayList<String>();

		int index = 0;
		for (String part : parts){
			printLine(part);
			if (list.contains(part)){
				printLine("Duplicated: "+ part);
			}else {
				list.add(part);
			}
			data[index][0]= parts[index];
			index++;
			checkRunning();
		}
		printLine("Line count: "+ parts.length);

		replaceInGlobalMap(list);
		
		if (tabulate){
			this.tabulate(data,headers);
		}
		//END TOOL
	}
}

