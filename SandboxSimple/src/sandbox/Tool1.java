//Tool2.java (erase me later)
package sandbox;

import java.io.File;

@SandboxAnnotation(
		description = "Run command in a separate java process",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"javaCommandLine,workingDirectory",
},showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool1 extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] javaCommandLine = getStringArrayNoDelim("javaCommandLine");
		String commandLine = javaCommandLine[0];
		File workingDir = null;
		if (javaCommandLine.length > 1) {
			workingDir = new File(javaCommandLine[1]);
			printLine("Working directory is " + workingDir);
		} else {
			workingDir = new File(getString("workingDirectory"));
		}
		setTooltip(commandLine);

		long delay = 0;

		if (workingDir.exists() && workingDir.isDirectory()) {
			delay = runInSeparateJavaProcess(commandLine, workingDir, this);
		} else {
			String workingDirectoryString = System.getProperty("user.dir", ".");
			printLine("Working directory is " + workingDirectoryString);
			delay = runInSeparateJavaProcess(commandLine, new File(workingDirectoryString), this);
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR + System.getProperty("line.separator", "\n") + "Delay = " + delay);
		// END TEST
	}
}
