package sandbox.client.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.net.SocketFactory;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "web client",
 		parameterDescriptions = {
 				"7", "port",
 				"10", "host",
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test3 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String host = getString("host");
		 int port = getInteger("port");
		 
		 SocketFactory factory = SSLSocketFactory.getDefault();
		 try (Socket connection = factory.createSocket(host, port)) {
		     ((SSLSocket) connection).setEnabledCipherSuites(
		       new String[] { "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256" });
		     ((SSLSocket) connection).setEnabledProtocols(
		       new String[] { "TLSv1.2" });
		     
		     SSLParameters sslParams = new SSLParameters();
		     sslParams.setEndpointIdentificationAlgorithm("HTTPS");
		     ((SSLSocket) connection).setSSLParameters(sslParams);
		     
		     BufferedReader input = new BufferedReader(
		       new InputStreamReader(connection.getInputStream()));
		     printLine(input.readLine());
		 }
		 //END TEST
	 }

}
