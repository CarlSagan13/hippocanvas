package sandbox.client.server;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Simple server",
 		parameterDescriptions = {
 				"7", "port"
// 				SandboxParameters.FOCUSED_NAME, "focusedString",
// 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
// 				SandboxParameters.PASSWORD_NAME, "password",
// 				SandboxParameters.POPPAD_NAME, "poppad",
// 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
// 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test2 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 int port = getInteger("port");
		 ServerSocketFactory factory = SSLServerSocketFactory.getDefault();
		 try (ServerSocket listener = factory.createServerSocket(port)) {
		     SSLServerSocket sslListener = (SSLServerSocket) listener;
		     sslListener.setNeedClientAuth(true);
		     sslListener.setEnabledCipherSuites(
		       new String[] { "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256" });
		     sslListener.setEnabledProtocols(
		       new String[] { "TLSv1.2" });
		     while (true) {
		    	 this.checkRunning(3);
		         try (Socket socket = sslListener.accept()) {
		             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		             out.println("Hello World!");
		         }
		     }
		 }
		 
		 
		 //END TEST
	 }

}
