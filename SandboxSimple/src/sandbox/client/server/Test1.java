// Tool56.java (erase me later)
package sandbox.client.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sandbox.SandboxAnnotation;
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = "Start page server", 
		parameterDescriptions = 
		{ 
			SandboxParameters.FOCUSED_NAME,"startEndPorts", 
			SandboxParameters.POPPAD_NAME,"headers", 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Test1 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		int[] startEndPorts = getIntegerArray("startEndPorts");
		String headers = getString("headers");

		// Find open port
		boolean success = false;
		ServerSocket serverSocket = null;

		int portIndex = startEndPorts[0];
		int portLimit = startEndPorts[1];
		while (!success) {
			try {
				serverSocket = new ServerSocket(portIndex);
				addConnection(serverSocket);
				printLine("Listening on port " + portIndex);
				success = true;
			} catch (IOException ioe) {
				portIndex++;
				if (portIndex > portLimit) {
					break;
				}
			}
		}

		addConnection(serverSocket);

		while (this.isRunning) {
			Socket socket = serverSocket.accept();
			printLine(this.getTime() + ": " + this.getTestName() + " accepting message.");
			printLine(socket.getInetAddress());
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			ClientThread clientThread = new ClientThread(socket, headers, this);
			clientThread.start();
		}
		serverSocket.close();
		// END TEST
	}

	class ClientThread extends Thread {
		Socket client;
		String headers;
		SandboxTestBase test;

		ClientThread(Socket client, String headers, SandboxTestBase test) {
			this.client = client;
			this.headers = headers;
			this.test = test;
		}

		@Override
		public void run() {
			try {
				// Get streams to talk to the client
				InputStream inputStream = client.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				OutputStream out = client.getOutputStream();
				PrintWriter pout = new PrintWriter(out);

				List<String> contentParameters = showHeaders(bufferedReader);
				String filePath = "/";
				if (contentParameters.size() > 0) {
					filePath = contentParameters.remove(0);
					int contentInputLength = 0;

					Map<String, List<String>> map = getHeaderMap(contentParameters);
					List<String> contentLengths = map.get("Content-Length");
					if (contentLengths != null && contentLengths.size() > 0) {
						contentInputLength = Integer.valueOf(contentLengths.get(0));
					}
					List<String> contentTypes = map.get("Content-Type");
					if (contentTypes != null && contentTypes.size() > 0) {
						if (contentTypes.contains("application/x-java-serialized-object")) {
							printLine("DANGER ZONE: Might seize up here because the content has to be a Java serialized object");
							ObjectInputStream ois = new ObjectInputStream(inputStream);
							try {
								Object objContent = ois.readObject();
								String mapId = "content" + new Date().getTime();
								printLine("Putting object in global map with ID: " + mapId);
								replaceInGlobalMap(mapId, objContent);
								contentInputLength = 0;
							} catch (ClassNotFoundException e) {
								printLine("Problem with getting serialized object", e);
							}
						}
					}

					if (contentInputLength > 0) {
						byte[] contentInput = new byte[contentInputLength];
						inputStream.read(contentInput, 0, contentInputLength);
						String contentName = getTestName() + "."
								+ (new SimpleDateFormat(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP))
										.format(new Date());
						String contentFilePath = getParameters().getConfigurationDirectory()
								+ File.separator	+ "files" + File.separator
								+ contentName;
						printLine("Saving received content to global map with ID " + contentName);
						test.addToGlobalMap(contentName, contentInput);
						printLine("Saving received content to " + contentFilePath);
						sandbox.Utilities.saveFileObject(contentFilePath, contentInput);
					}

					printLine("Input content length = " + contentInputLength);
					String[] filePathParts = filePath.split(" ", 4);

					// *////////////DMH To allow reference to filenames with
					// space characters
					// String[]charsetNames
					// ={"US-ASCII","ISO-8859-1","UTF-8","UTF-16BE","UTF-16LE","UTF-16"};
					filePath = URLDecoder.decode(filePathParts[1], "US-ASCII"); // TODO Suspicious OCR artifacts
					/*
					 * /////////////// filePath = filePathParts[1]; //
					 *///////////////

				}
				if (filePath.trim().length() == 0) {
					printLine("No file path specified");
				} else {
					printLine("filePath is " + filePath);
				}
				String contentOutput = null;
				if (filePath.endsWith("shutdown")) {
					closeConnections();
					setIsRunning(false);
					contentOutput = "Shutting down";
				} else {
					try {
						contentOutput = sandbox.Utilities.getResourceString(filePath);
					} catch (FileNotFoundException fnfe) {
						contentOutput = "File not found";
					}
				}

				headers = headers.concat("\n" + "Content-Length: " + contentOutput.length());
				try {
					out.write(headers.trim().getBytes());
					out.write("\n\n".getBytes());
					pout.println(contentOutput);
				} catch (Exception en) {
					printLine(this.getClass().getName() + ": " + en.getClass().getSimpleName());
					for (StackTraceElement element : en.getStackTrace()) {
						printLine(element.toString());
					}
				} finally {
					pout.close();
					bufferedReader.close();
					client.close();
				}

				pout.close();
				bufferedReader.close();
				client.close();

			} catch (IOException ioe) {
				printLine(ioe.getClass().getName() + ": " + ioe.getMessage());
				for (StackTraceElement element : ioe.getStackTrace()) {
					printLine(element.toString());
				}
			}
		}
	}

	List<String> showHeaders(BufferedReader bufferedReader) throws IOException {
		String line = null;
		List<String> contentParameters = new ArrayList<String>();
		printLine("******************HEADERS START");
		while ((line = bufferedReader.readLine()) != null) {
			if (line.length() == 0)
				break;
			printLine("HEADER: " + line);
			contentParameters.add(line);
		}
		printLine("******************HEADERS DONE ");
		return contentParameters;
	}

	Map<String, List<String>> getHeaderMap(List<String> listHeaders) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String header : listHeaders) {
			String[] nameValue = header.split(": ", 2);
			String name = nameValue[0];
			String value = nameValue[1];
			List<String> list = map.get(name);
			if (list == null) {// header is new
				list = new ArrayList<String>();
			}
			list.add(value);
			map.put(name, list);
		}
		return map;
	}

}
