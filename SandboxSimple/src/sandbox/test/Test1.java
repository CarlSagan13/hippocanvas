package sandbox.test;

import sandbox.SandboxAnnotation;
import sandbox.SandboxTestBase;
import sandbox.SandboxParameters;

@SandboxAnnotation(
		description = "Get all the parameters",
 		parameterDescriptions = {
 				"1", "string",
 				SandboxParameters.FOCUSED_NAME, "focusedString",
 				SandboxParameters.SCRATCHPAD_NAME, "scratchPadString",
 				SandboxParameters.PASSWORD_NAME, "password",
 				SandboxParameters.POPPAD_NAME, "poppad",
 				SandboxParameters.POPPAD_NAME, "poppad-sbx-values",
 				SandboxParameters.TRUEFALSE_NAME, "truefalse",
 				SandboxParameters.YESNO_NAME, "yesno",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Test1 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
		 String string = getString("string");
		 String focusedString = getString("focusedString");
		 String scratchPadString = getString("scratchPadString");
		 String password = getString("password");
		 String poppad = getString("poppad");
		 String test_value_string = getString("test_value_string");	//set in poppad
//		 int test_value_number = getInteger("test_value_number"); //set in poppad
		 boolean truefalse = getBoolean("truefalse");
		 boolean yesno = getBoolean("yesno");
		 
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " string");
		 printLine(string);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " focusedString");
		 printLine(focusedString);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " scratchPadString");
		 printLine(scratchPadString);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " password");
		 printLine(password);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " poppad");
		 printLine(poppad);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " test_value_string");
		 printLine(test_value_string);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " test_value_number");
//		 printLine(test_value_number);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " truefalse");
		 printLine(truefalse);
		 printLine(SandboxParameters.VISUAL_SEPARATOR + " yesno");
		 printLine(yesno);
		 
		 
		 //END TEST
	 }

}
