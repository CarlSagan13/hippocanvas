package sandbox;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class SandboxFrame extends JFrame implements SandboxLogging {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean done = false;
	private boolean savedAOT = false;

	public SandboxFrame() throws HeadlessException {
		super();
		sandboxLogger.info("Creating a new " + SandboxFrame.class.getName());
	}

	public SandboxFrame(GraphicsConfiguration arg0) {
		super(arg0);
	}

	public SandboxFrame(String arg0) throws HeadlessException {
		super(arg0);
	}

	public SandboxFrame(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	protected void phoneIn() {
		try {
			int timeOpen = Integer.valueOf(System.getProperty(SandboxParameters.SBX_PHONE_CALL_DURATION, "1000"));
			Timer clockTimer = new Timer(timeOpen, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setAlwaysOnTop(savedAOT);
			        setExtendedState(JFrame.NORMAL); // To maximize a frame
				}
			});
			clockTimer.setRepeats(false);
			clockTimer.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected boolean isDone() {
		return done;
	}

	protected void setDone(boolean done) {
		this.done = done;
	}

	public boolean isSavedAOT() {
		return savedAOT;
	}

	public void setSavedAOT(boolean savedAOT) {
		this.savedAOT = savedAOT;
	}

}
