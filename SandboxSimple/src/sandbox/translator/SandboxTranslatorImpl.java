package sandbox.translator;

import sandbox.*;

/**
 * Put this class in another project that uses the sandbox.
 * put in translate package with class name SandboxTranslatorImpl
 * (translator.SandboxTranslatorImpl)
 * Add translations of the projects classes so you can see the contents
 * when the class is in the sandbox global map. Use Tool4.
 * @author carls
 *
 */
public class SandboxTranslatorImpl implements SandboxTranslator {
	private boolean verbose = true;

	@Override
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	@Override
	public void translate(Object obj, SandboxTestBase test, String depth) throws SandboxException {
//		if (obj instanceof org.bson.types.Binary) {
//			org.bson.types.Binary binary = (org.bson.types.Binary) obj;
//			test.printLine(binary.getData());
//			for (byte b : binary.getData()) {
//				test.printIt(b);
//				test.printIt(",");
//			}
//			test.printLine();
//		}
//		if (obj instanceof com.google.gson.JsonObject) {
//			com.google.gson.JsonObject jo = (com.google.gson.JsonObject) obj;
//			test.printLine(jo.toString());
//		}
		
		//Add if blocks for each class you want translated
	}

	@Override
	public void translate(Object obj, SandboxTestBase test) throws SandboxException {
		translate(obj, test, "");
	}

}
