package sandbox;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class SandboxProcess {
	
//	protected static String PROCESS_QUOTE_PLACEHOLDER = "{sbx-quote}";
//	protected static String PROCESS_TAB_PLACEHOLDER = "{sbx-tab}";
//	protected static String PROCESS_DOLLAR_PLACEHOLDER = "{sbx-dollar}";
	
	protected static String PROCESS_ID_PREFIX = "processid-";
	protected static String PROCESS_CMD_PROP_NAME = "processCommand";
	protected static String PROCESS_PID_CAPTURE = "processPidCapture";

	
		
//	protected static String PROCESS_HOST_ENV_CAPTURE = "sbx.process.host.capture";
//	protected static String PROCESS_DIR_ENV_CAPTURE = "sbx.process.dir.capture";
//	protected static String PROCESS_USER_ENV_CAPTURE = "sbx.process.user.capture";

	Process proc = null;
	InputStream inputStream = null;
	BufferedReader bufferedReader = null;
	BufferedReader errorBufferedReader = null;
	BufferedWriter procStdIn = null;
	OutputStream outputStream = null;
	OutputStreamWriter outputStreamWriter = null;
	InputStream errorStream = null;
	
	Properties props = null;
	boolean done = true;
	String strProcessEnd = "exit";
	List<String> history = new ArrayList<String>();
	SandboxTestBase test = null;
	int commandPid = -1;
	boolean capturingPid = false;
	String globalMapId = null;
	
	String host = "unknown.host";
	String workspace = "unknown.workspace";
	String user = "unknown.user";
	
	String resource = null;
	ProcessCommands processCommands = new ProcessCommands(this, test);

	public SandboxProcess(Process proc, SandboxTestBase test) {
		super();
		this.proc = proc;
		this.test = test;
		initStreams(proc);

		if (test != null && test.getParameters() != null) {
			props = test.getParameters().properties;
			
		} else {
			String systemFilePath = System.getProperty("user.dir") + File.separator + "system.xml";
			File systemSandboxFile = new File(systemFilePath);
			props = new Properties();
			if (systemSandboxFile.exists() && systemSandboxFile.isFile()) {
				FileInputStream fileInputStream;
				try {
					fileInputStream = new FileInputStream(systemSandboxFile);
					props.loadFromXML(fileInputStream);
					test.printLine("Loaded sandbox system configuration file");
				} catch (Exception en) {
					test.printLine("Could not load sandbox system configuration file");
				}

			}
		}
		processCommands.init(props);
	}
	
	private void initStreams(Process proc) {
		inputStream = proc.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		bufferedReader = new BufferedReader(inputStreamReader);

		errorStream = proc.getErrorStream();
		InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
		errorBufferedReader = new BufferedReader(errorStreamReader);

		outputStream = proc.getOutputStream();
		outputStreamWriter = new OutputStreamWriter(outputStream);
		procStdIn = new BufferedWriter(outputStreamWriter);
	}

	protected void initConfig(Properties processProps) {
		processCommands.init(processProps);
	}
	
	protected List<String> getFileText(String resource) throws IOException, InterruptedException, SandboxException {
		//sbx.address.replacement

		//Check that the file exists. If not, create.
		List<String> resultsFileExists = new ArrayList<String> ();
		if (processCommands.checkFileExistsCommand == null || processCommands.checkFileExistsCommand.length() == 0) {
			throw new SandboxException(ProcessCommands.PROCESS_CHECK_FILE_EXISTS_CMD_PROP_NAME + " not defined");
		}
		String commandFileExists = processCommands.checkFileExistsCommand.replace("sbx.address.replacement", resource);
		int commandReturn = runCommand(commandFileExists, null, resultsFileExists);
		if (commandReturn == 0) {
			if (processCommands.skipOutputNumber > 0) {
				resultsFileExists.remove(0); // drop first return which is just the command line printed
			}
			boolean fileExists = false;
			for (String line : resultsFileExists) {
				Matcher matcher = processCommands.checkFileExistsPattern.matcher(line);
				if (matcher.find()) {
					fileExists = true;
				}
			}
			
			if (processCommands.processStartSaveFileCommand == null || processCommands.processStartSaveFileCommand.length() == 0) {
				throw new SandboxException(ProcessCommands.PROCESS_START_SAVE_FILE_CMD_PROP_NAME + " is not defined.");
			}
			
			if (!fileExists && processCommands.processStartSaveFileCommand != null) {
				//Make the empty file
				String startFileCommand = processCommands.processStartSaveFileCommand.replace("sbx.address.replacement", resource)
						.replace("sbx.text.replacement", "NEW FILE " + Utilities.getTime());
				runCommand(startFileCommand, null);
			}
		} else {
			System.err.println(commandFileExists);
			test.printLine("\t" + commandFileExists);
			throw new SandboxException("Cannot get access to file: commandFileExists");
		}
		
		//Check that file is writable
		List<String> results = new ArrayList<String> ();
		if (processCommands.checkWritableFileCommand == null || processCommands.checkWritableFileCommand.length() == 0) {
			throw new SandboxException(ProcessCommands.PROCESS_CHECK_FILE_WRITABLE_CMD_PROP_NAME + " not defined");
		}
		String checkWritableFileCommand = processCommands.checkWritableFileCommand.replace("sbx.address.replacement", resource);
		commandReturn = runCommand(checkWritableFileCommand, null, results);
		if (commandReturn == 0) {
//			if (Utilities.isWindows()) {
				results.remove(0); // drop first return which is just the command line printed
//			}
			for (String line : results) {
				Matcher matcher = processCommands.checkWritableFilePattern.matcher(line);
				if (matcher.find()) {
					if (matcher.groupCount() > 0) {
						throw new SandboxException("File is locked");
					}
				}
			}
		} else {
			throw new SandboxException("Cannot get access to file.");
		}
		
		//Make a remote copy
		if (processCommands.processCopyFileCommand == null || processCommands.processCopyFileCommand.length() == 0) {
			throw new SandboxException(ProcessCommands.PROCESS_COPY_FILE_CMD_PROP_NAME + " not defined");
		}
		String copyFileCommand = processCommands.processCopyFileCommand.replace("sbx.address.replacement", resource);
		List<String> copyFileLines = new ArrayList<String>();
		int retval = runCommand(copyFileCommand, null, copyFileLines);
		if (retval != 0) {
			throw new SandboxException("Cannot save copy of file\n\t" + resource);
		}
		
		//Grab the text from the file
		List<String> lines = new ArrayList<String>();
		if (processCommands.processGrabTextCommand != null) {
			this.resource = resource;
			String commandGrabText = processCommands.processGrabTextCommand.replace("sbx.address.replacement", resource);
			runCommand(commandGrabText, null, lines);
		}
//		done = false;
		if (lines.toString().length() > 10000) {
			throw new SandboxException("Process file too large: " + lines.toString().length());
		}
		return lines;
	}

	protected int saveFileText(List<String> lines) throws IOException, InterruptedException, SandboxException {
		if (processCommands.processStartFileCommand != null && processCommands.processAddFileCommand != null
				&& processCommands.processPlaceholders != null && processCommands.processPlaceholders.size() > 1) {
			String startLine = lines.get(0);

			try {
				for (String stringRegex : processCommands.processPlaceholders) {
					String replacement = stringRegex.split(SandboxParameters.DEFAULT_DELIMITER)[0];
					String toReplace = stringRegex.split(SandboxParameters.DEFAULT_DELIMITER)[1];
					startLine = startLine.replaceAll(toReplace, replacement); 
				}
				String startFileCommand = processCommands.processStartFileCommand.replace("sbx.text.replacement", startLine)
						.replace("sbx.address.replacement", resource);
				runCommand(startFileCommand, null);
				lines = lines.subList(1, lines.size());
				for (String line : lines) {
					for (String stringRegex : processCommands.processPlaceholders) {
						String replacement = stringRegex.split(SandboxParameters.DEFAULT_DELIMITER)[0];
						String toReplace = stringRegex.split(SandboxParameters.DEFAULT_DELIMITER)[1];
						line = line.replaceAll(toReplace, replacement);
					}
					String addFileCommand = processCommands.processAddFileCommand.replace("sbx.text.replacement", line)
							.replace("sbx.address.replacement", resource);
					runCommand(addFileCommand, null);
				}
				String postFileCommand = processCommands.processPostFileCommand.replace("sbx.address.replacement", resource);
				runCommand(postFileCommand, null);
			} catch(PatternSyntaxException pse) {
				test.printLine("Could not save file: Configuration regex is wrong");
			} catch(ArrayIndexOutOfBoundsException aioobe) {
				test.printLine("Could not save file: Check delimiters in process configuration.");
			} catch(Throwable th) {
				test.printLine("Could not save file: Some unknown reason", th);
			}
		} else {
			String message = "Both " + ProcessCommands.PROCESS_START_SAVE_FILE_CMD_PROP_NAME 
					+ " and " + ProcessCommands.PROCESS_ADD_SAVE_FILE_CMD_PROP_NAME + " must be set";
			test.printLine(message);
			return -1;
		}
//		done = false;
		return 0;
	}

//*//////////////////////////////////////////////////////DMH
	protected int updatePromptInfo() {
		int returnVal = 0;
		if (processCommands.hostCommand != null && processCommands.hostPattern != null) {
			List<String> results = new ArrayList<String> ();
			try {
				//Use runCommand to populate the results list
				int commandReturn = runCommand(processCommands.hostCommand, null, results);
				if (commandReturn == 0) {
					for (String line : results) {
						Matcher matcher = processCommands.hostPattern.matcher(line);
						if (matcher.find()) {
							if (matcher.groupCount() > 0) {
								this.host = line.substring(matcher.start(1), matcher.end(1));
							}
						}
					}
				}
			} catch(Throwable th) {
				returnVal = 1 | returnVal;
				System.err.println(th);
				if (test != null) {
//					test.printLine(th);
				}
			}
		}
		if (processCommands.workspaceCommand != null && processCommands.workspacePattern != null) {
			List<String> results = new ArrayList<String> ();
			try {
				//Use runCommand to populate the results list
				int commandReturn = runCommand(processCommands.workspaceCommand, null, results);
				if (commandReturn == 0) {
					for (String line : results) {
						Matcher matcher = processCommands.workspacePattern.matcher(line);
						if (matcher.find()) {
							if (matcher.groupCount() > 0) {
								this.workspace = line.substring(matcher.start(1), matcher.end(1));
							}
						}
					}
				}
			} catch(Throwable th) {
				returnVal = 2 | returnVal;
				System.err.println(th);
				if (test != null) {
//					test.printLine(th);
				}
			}
		}
		if (processCommands.userCommand != null && processCommands.userPattern != null) {
			List<String> results = new ArrayList<String> ();
			try {
				//Use runCommand to populate the results list
				int commandReturn = runCommand(processCommands.userCommand, null, results);
				if (commandReturn == 0) {
					for (String line : results) {
						Matcher matcher = processCommands.userPattern.matcher(line);
						if (matcher.find()) {
							if (matcher.groupCount() > 0) {
								this.user = line.substring(matcher.start(1), matcher.end(1));
							}
						}
					}
				}
			} catch(Throwable th) {
				returnVal = 4 | returnVal;
				System.err.println(th);
				if (test != null) {
//					test.printLine(th);
				}
			}
		}
				
		return 0;
	}

	protected int runCommand(String command, List<String> arguments)
			throws IOException, InterruptedException, SandboxException {
		return runCommand(command, arguments, null);
	}
	protected int runCommand(String command, List<String> arguments, List<String> results) 
			throws IOException, InterruptedException, SandboxException {
		return runCommand(command, null, null, null, arguments, results);
	}
	protected int runCommand(String command, String processCommand, String processPidCapture, 
			String pidEnvName, List<String> arguments, List<String> results)
			throws IOException, InterruptedException, SandboxException {
		commandPid = -1; //guarantee no accidental pid kills
		history.add(command);
		done = false;
		String line = new String();
		
		StringBuffer prompt = new StringBuffer(":");
		
		String cmd = "echo No Command";
		if (processCommand != null && processPidCapture != null) {
			cmd = processCommand.replaceFirst("sbx.replace.command", command);
			capturingPid = true;
		} else {
			cmd = command;
		}
		
		getProcStdIn().write(cmd);
		
		System.err.println("Actual command:");
		System.err.println(cmd);
		
		
		test.getParameters().printLine("Actual command:");
		test.getParameters().printLine(cmd);
		
		
		
		
		getProcStdIn().newLine();
		getProcStdIn().write("echo " + SandboxParameters.SANDBOX_END_OF_FILE);
		getProcStdIn().newLine();
		getProcStdIn().flush();
		
		
/*//////////////////////////////DMH			
		int exitValue = 0;
		if (proc != null && proc.isAlive()) {
			exitValue = proc.exitValue();
			System.err.println("exit value is " + proc.exitValue());
		}
//*/////////////////////////////////			
				
				
				

		if (processCommands.skipOutputNumber > 0) {
			for (int count = 0; count < processCommands.skipOutputNumber; count++) {
				getBufferedReader().readLine();
			}
		}
//		if (Utilities.isWindows()) {
//			getBufferedReader().readLine();
//			getBufferedReader().readLine();
//		}

		line = getBufferedReader().readLine();
		int lineIndex = 0;
		String toPrint = prompt.append(" ").append(command).toString();
		if (results != null) {
			results.add(lineIndex++, toPrint);
		} else {
			test.printLine(toPrint);
		}
//		results.add(lineIndex++, line);
		if (capturingPid) {
			String[] groups = Utilities.grabRegex(processPidCapture, new StringBuffer(line));
			if (groups != null && groups.length > 1) {
				try {
					commandPid = Integer.parseInt(groups[1]);
					System.err.println("PID = " + commandPid);
					prompt.append("(" + commandPid + ") " + command);
					System.setProperty(pidEnvName, groups[1]);
				} catch(NumberFormatException nfe) {
					capturingPid = false;
					prompt.append(" " + command);
				}
			}
			line = getBufferedReader().readLine();
			capturingPid = false;
////		} else {
////			prompt.append(" " + command);
		}
//		test.printLine(prompt);
		while (test.isRunning && line != null && !line.contains(SandboxParameters.SANDBOX_END_OF_FILE)) {
			if (arguments != null && arguments.size() > 0) {
				regexEnvCapture(line, arguments);
			}
//			test.printLine(line);
			
			if (results != null) {
				results.add(lineIndex++, line);
			} else {
				test.printLine(line);
			}

			
			
//			results.add(lineIndex++, line);
			line = getBufferedReader().readLine();
		}
		if (Utilities.isWindows()) {
			getBufferedReader().readLine(); //Throw away Microsoft trash
		}

		if (test.isRunning) {
			done = true;
		}
		if (getErrorStream().available() > 0) {
			String errorLine = new String();

//			test.printIt("PROCESS ERROR OUT");
			toPrint = SandboxParameters.VISUAL_SEPARATOR + "PROCESS ERROR OUT";
			if (results != null) {
				results.add(lineIndex++, toPrint);
			} else {
				test.printLine(toPrint);
			}

//			results.add(lineIndex++, SandboxParameters.VISUAL_SEPARATOR + "PROCESS ERROR OUT");

//			test.printALine();
			while (test.isRunning && (getErrorStream().available() > 0)
					&& (errorLine = getErrorBufferedReader().readLine()) != null) {
//				test.printLine(errorLine);
//				results.add(lineIndex++, errorLine);
				if (results != null) {
					results.add(lineIndex++, errorLine);
				} else {
					test.printLine(errorLine);
				}

			}
			
			return -1;
		}
		
		return 0;
	}
	
	private void regexEnvCapture(String line, List<String> arguments) {
		for (String argument : arguments) {
			String[] argumentParts = argument.split(SandboxParameters.DEFAULT_DELIMITER);
			if (argumentParts.length == 2) {
				String envName = argumentParts[0];
				String regex = argumentParts[1];
				
				Pattern pattern = Pattern.compile(regex);
				
				try {
					Matcher matcher = pattern.matcher(line);
					if (matcher.find()) {
						String theMatch = "NO MATCH";
						if (matcher.groupCount() > 0) {
							theMatch = line.substring(matcher.start(1), matcher.end(1));
						} else {
							theMatch = line.substring(matcher.start(0), matcher.end(0)); // No groups
							test.getParameters().printLine("You should be using groups in this regex: " + regex, true, true);
						}
						System.setProperty(envName, theMatch);
					}
				} catch (Throwable th) {
					test.getParameters().printLine(th);
				}

			}
		}
	}

	public List<String> getFileLines(String resource) { //UNDER CONSTRUCTION
		String fileContentCommand = (String) props.get("fileContentCommand");
		List<String> returnList = new ArrayList<String>();
		returnList.add(SandboxParameters.UNDER_CONSTRUCTION);
//		protected int runCommand(String command, List<String> arguments, List<String> results) 
		return returnList;
	}

	public int close() {
		String killCommand = (String) props.get("processPidKill");
		if (commandPid > 0 && killCommand != null && killCommand.length() > 0) {
//		if (capturingPid && commandPid > 0 && killCommand != null && killCommand.length() > 0) {
			String command = killCommand.replace("sbx.replace.pid", String.valueOf(commandPid));
			
	//		test.printLine("To kill process:");
	//		test.printLine("\t" + command);
			try {
				getProcStdIn().write(command);
				getProcStdIn().newLine();
				getProcStdIn().flush();
				
				getProcStdIn().close();
				inputStream.close();
				errorStream.close();
				outputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			test.removeFromGlobalMap(globalMapId);

		}
		//<entry key="processPidKill">kill sbx.replace.pid</entry>
		
/*////////////////////////////////////DMH
		try {
			System.err.println("dmh1011 Exit value = " + proc.waitFor());
		} catch (InterruptedException e) {
			System.err.println("dmh1011 Could not wait for process");
		}

		if (proc.exitValue() < 0) {
			System.err.println("***ABNORMAL PROCESS TERMINATION AT: ***");
			return proc.exitValue();
		}
//*///////////////////////////////////////			

		

		return 0;
	}
	
	protected Process getProc() {
		return proc;
	}

	protected void setProc(Process proc) {
		this.proc = proc;
	}

	protected InputStream getInputStream() {
		return inputStream;
	}

	protected void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	protected BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	protected void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	protected BufferedReader getErrorBufferedReader() {
		return errorBufferedReader;
	}

	protected void setErrorBufferedReader(BufferedReader errorBufferedReader) {
		this.errorBufferedReader = errorBufferedReader;
	}

	protected BufferedWriter getProcStdIn() {
		return procStdIn;
	}

	protected void setProcStdIn(BufferedWriter procStdIn) {
		this.procStdIn = procStdIn;
	}

	protected OutputStream getOutputStream() {
		return outputStream;
	}

	protected void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	protected OutputStreamWriter getOutputStreamWriter() {
		return outputStreamWriter;
	}

	protected void setOutputStreamWriter(OutputStreamWriter outputStreamWriter) {
		this.outputStreamWriter = outputStreamWriter;
	}

	protected InputStream getErrorStream() {
		return errorStream;
	}

	protected void setErrorStream(InputStream errorStream) {
		this.errorStream = errorStream;
	}
 
	protected List<String> getHistory() {
		return history;
	}

	protected void setHistory(List<String> history) {
		this.history = history;
	}

	protected void setTest(SandboxTestBase test) {
		this.test = test;
	}

	protected void setDone(boolean done) {
		this.done = done;
	}

	protected String getHost() {
		return host;
	}

	protected String getWorkspace() {
		return workspace;
	}

	protected String getUser() {
		return user;
	}


	@Override
	public String toString() {
		return "history=" + history;
	}
	
}

class ProcessCommands {
	protected static String PROCESS_SKIP_OUTPUT_NUM_PROP_NAME = "processSkipOutputNumber";
	
	protected static String PROCESS_HOST_CMD_PROP_NAME = "processHostCommand";
	protected static String PROCESS_HOST_REGEX_PROP_NAME = "processHostGroupRegex";
	protected static String PROCESS_WS_CMD_PROP_NAME = "processWorkspaceCommand";
	protected static String PROCESS_WS_REGEX_PROP_NAME = "processWorkspaceGroupRegex";
	protected static String PROCESS_USER_CMD_PROP_NAME = "processUserCommand";
	protected static String PROCESS_USER_REGEX_PROP_NAME = "processUserGroupRegex";
	protected static String PROCESS_CHECK_FILE_EXISTS_CMD_PROP_NAME = "processCheckFileExistsCommand";
	protected static String PROCESS_CHECK_FILE_EXISTS_REGEX_PROP_NAME = "processCheckFileExistsRegex";
	protected static String PROCESS_CHECK_FILE_WRITABLE_CMD_PROP_NAME = "processCheckWritableFileCommand";
	protected static String PROCESS_CHECK_FILE_WRITABLE_REGEX_PROP_NAME = "processCheckWritableFileRegex";
	protected static String PROCESS_INIT_CMD_PROP_NAME = "processInitCommand";
	protected static String PROCESS_GRAB_TEXT_CMD_PROP_NAME = "processGrabTextCommand";
	protected static String PROCESS_START_SAVE_FILE_CMD_PROP_NAME = "processStartSaveFileCommand";
	protected static String PROCESS_ADD_SAVE_FILE_CMD_PROP_NAME = "processAddSaveFileCommand";
	protected static String PROCESS_POST_SAVE_FILE_CMD_PROP_NAME = "processPostSaveFileCommand";
	protected static String PROCESS_COPY_FILE_CMD_PROP_NAME = "processCopyFileCommand";
	protected static String PROCESS_PLACEHOLDERS_PROP_NAME = "processPlaceholders";
	
	boolean canEdit = false;
	int skipOutputNumber = 0;
	List<String> processPlaceholders = null;
	String hostCommand = null;
	String workspaceCommand = null;
	String userCommand = null;
	String checkWritableFileCommand = null;
	String checkFileExistsCommand = null;
	String processCopyFileCommand = null;
	String processStartSaveFileCommand = null;
	String processGrabTextCommand = null;
	String processStartFileCommand = null;
	String processAddFileCommand = null;
	String processPostFileCommand = null;
	
	Pattern hostPattern = null;
	Pattern workspacePattern = null;
	Pattern userPattern = null;
	Pattern checkWritableFilePattern = null;
	Pattern checkFileExistsPattern = null;

	SandboxProcess process = null;
	SandboxTestBase test = null;
	
	public ProcessCommands(SandboxProcess process, SandboxTestBase test) {
		super();
		this.process = process;
		this.test = test;
	}

	protected void init(Properties props) {
		this.canEdit = true;
		
		//	protected static String PROCESS_SKIP_OUTPUT_NUM_PROP_NAME = "processSkipOutputNumber";
		skipOutputNumber = Integer.valueOf(props.getProperty(PROCESS_SKIP_OUTPUT_NUM_PROP_NAME, "0"));
		hostCommand = props.getProperty(PROCESS_HOST_CMD_PROP_NAME);
		String processHostGroupRegex = props.getProperty(PROCESS_HOST_REGEX_PROP_NAME);
		workspaceCommand = props.getProperty(PROCESS_WS_CMD_PROP_NAME);
		String processWorkspaceGroupRegex = props.getProperty(PROCESS_WS_REGEX_PROP_NAME);
		userCommand = props.getProperty(PROCESS_USER_CMD_PROP_NAME);
		String processUserGroupRegex = props.getProperty(PROCESS_USER_REGEX_PROP_NAME);
		String processInitCommand = props.getProperty(PROCESS_INIT_CMD_PROP_NAME);

		checkFileExistsCommand = props.getProperty(PROCESS_CHECK_FILE_EXISTS_CMD_PROP_NAME);
		String processCheckFileExistsRegex = props.getProperty(PROCESS_CHECK_FILE_EXISTS_REGEX_PROP_NAME);
		
		checkWritableFileCommand = props.getProperty(PROCESS_CHECK_FILE_WRITABLE_CMD_PROP_NAME);
		String processCheckWritableFileRegex = props.getProperty(PROCESS_CHECK_FILE_WRITABLE_REGEX_PROP_NAME);
		processCopyFileCommand = props.getProperty(PROCESS_COPY_FILE_CMD_PROP_NAME);
		
		
		/**
		 * 			String processPostFileCommand = props.getProperty(ProcessCommands.PROCESS_POST_SAVE_FILE_CMD_PROP_NAME);

		 */
		
		
		processStartSaveFileCommand = props.getProperty(PROCESS_START_SAVE_FILE_CMD_PROP_NAME);
		processGrabTextCommand = props.getProperty(PROCESS_GRAB_TEXT_CMD_PROP_NAME);
		processStartFileCommand = props.getProperty(PROCESS_START_SAVE_FILE_CMD_PROP_NAME);
		processAddFileCommand = props.getProperty(PROCESS_ADD_SAVE_FILE_CMD_PROP_NAME);
		processPostFileCommand = props.getProperty(PROCESS_POST_SAVE_FILE_CMD_PROP_NAME);
		
		String strProcessPlaceholders = props.getProperty(PROCESS_PLACEHOLDERS_PROP_NAME);
		if (strProcessPlaceholders != null && strProcessPlaceholders.length() > 5) {
			processPlaceholders = new ArrayList<String>();
			for (String element: strProcessPlaceholders.split(",")) {
				processPlaceholders.add(0, element);
			}
		}
			
		if (processHostGroupRegex != null) {
			try {
				Pattern pattern = Pattern.compile(processHostGroupRegex); // prepping before
				hostPattern = pattern;
			} catch(PatternSyntaxException pse) {
				test.getParameters().printLine(pse);
			}
		}
		if (processWorkspaceGroupRegex != null) {
			try {
				Pattern pattern = Pattern.compile(processWorkspaceGroupRegex); // prepping before
				workspacePattern = pattern;
			} catch(PatternSyntaxException pse) {
				test.getParameters().printLine(pse);
			}
		}
		if (processUserGroupRegex != null) {
			try {
				Pattern pattern = Pattern.compile(processUserGroupRegex); // prepping before
				userPattern = pattern;
			} catch(PatternSyntaxException pse) {
				test.getParameters().printLine(pse);
			}
		}
		if (processCheckWritableFileRegex != null) {
			try {
				Pattern pattern = Pattern.compile(processCheckWritableFileRegex); // prepping before
				checkWritableFilePattern = pattern;
			} catch(PatternSyntaxException pse) {
				test.getParameters().printLine(pse);
				canEdit = false;
			}
		} else {
			canEdit = false;
		}
		if (processCheckFileExistsRegex != null) {
			try {
				Pattern pattern = Pattern.compile(processCheckFileExistsRegex); // prepping before
				checkFileExistsPattern = pattern;
			} catch(PatternSyntaxException pse) {
				test.getParameters().printLine(pse);
				canEdit = false;
			}
		} else {
			canEdit = false;
		}
		
		if (processInitCommand != null && !processInitCommand.equals("")) {
			try {
				process.runCommand(processInitCommand, null); //You can use this to source files in unix
															// or eliminate the first-run lines in Windows
			} catch (IOException | InterruptedException | SandboxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


}
