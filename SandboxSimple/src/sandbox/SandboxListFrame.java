//SandboxListFrame.java (erase me later)
package sandbox;

import javax.swing.*;

import javax.swing.border.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.ArrayList;

public class SandboxListFrame extends SandboxFrame {
	/**
	*
	*/
	private static String OK_COMMAND = "OK";
	private static String SORT_COMMAND = "Sort";
	private static String DELETE_COMMAND = "Delete";
	
	JButton okButton,sortButton,deleteButton = null;
	JComponent component = null;
	SandboxList list = null;
	SandboxTestBase test = null;
	SandboxParameters params = null;
	ArrayList <Object> itemsToRemove = null;
	
	public SandboxListFrame(JComponent component, SandboxTestBase test, SandboxParameters params, String title) {
		this.component = component;
		this.test = test;
		this.params = params;
		this.setTitle(title);
//		this.setMinimumSize(new Dimension(title.length()*5 + 150,100));
		Utilities.setBorderColor(this);
		
		//Allow for JComboBox as component
		if (component instanceof JComboBox){
			JComboBox comboBox = (JComboBox)component;
			int itemCount = comboBox.getItemCount();
			if (itemCount > 1){
				String[]stringArray = new String[itemCount];
				for (int i = 0;i < itemCount;i++){
					stringArray[i]= (String)comboBox.getItemAt(i);
				}
				list = new SandboxList(stringArray);
			}else {
				printLine("One or more entries needed in JComboBox for this funetion.");
			}
		}
		
		//Allow for SandboxList
		if (component instanceof SandboxList){
			list = (SandboxList)component;
		}
		
		JPanel panel = new JPanel(new BorderLayout());

		JPanel buttonPanel = new JPanel();
	
		ButtonListener buttonListener = new ButtonListener(this);
		okButton = new JButton(OK_COMMAND);
		okButton.addActionListener(buttonListener);
		buttonPanel.add(okButton);
		sortButton = new JButton(SORT_COMMAND);
		sortButton.addActionListener(buttonListener);
		buttonPanel.add(sortButton);

		if (test == null){
			deleteButton = new JButton(DELETE_COMMAND);
			deleteButton.addActionListener(buttonListener);
			buttonPanel.add(deleteButton);
		}

		Border border = BorderFactory.createEmptyBorder(2,5,2,5);
		list.setBorder(border);
		list.addMouseListener(new java.awt.event.MouseAdapter(){
			public void mouseClicked(java.awt.event.MouseEvent e){
				handleListMouseClicks(e);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(list);

		
		panel.add(buttonPanel,BorderLayout.NORTH);
		panel.add(scrollPane,BorderLayout.CENTER);
		
		getContentPane().add(panel);
		
		setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
		pack();
//		setAlwaysOnTop(Boolean.valueOf(System.getProperty(SandboxFrameBase.ALWAYS_ON_TOP_VAR_NAME, "false")));
		setAlwaysOnTop(true);
		setLocationRelativeTo(params.getTabbedPane());
		
		list.addFocusListener(params.focusListener);
		list.addKeyListener(params.keyListener);
		
		setVisible(true);

		if (test != null){
			try {
				while(isVisible()){
					Thread.sleep (100);
				}
			}catch(InterruptedException ie){
				test.printLine("Wait for input was interrupted.");
			}finally {
				test.resetStartTime();
			}
		}
	}
	
	private void printLine(String string){
		if (test != null){
			test.printLine(string);
		}else if (params != null){
			params.printLine(string);
		}else {
			System.out.println(string);
		}
	}
	
	private void handleListMouseClicks(java.awt.event.MouseEvent e){
		SandboxList list = (SandboxList)e.getComponent();
		Object selectedText = list.getSelectedValue();
		if (e.getClickCount()== 2){
			System.out.println("double click");
			okButton.doClick();
		} else {
			System.out.println("single click: button " + e.getButton());
		}
	}
	
	public class ButtonListener implements ActionListener {
		private SandboxListFrame frame;
		
		public ButtonListener(SandboxListFrame frame){
			this.frame = frame;
		}
		
		public void actionPerformed(ActionEvent actionEvent){
			try {
				if (actionEvent.getActionCommand ().equals(SandboxListFrame.OK_COMMAND)) {
					if (frame.test != null){
						Object[]strings = frame.list.getSelectedValues();

						Object[]returnedFromList = new Object[strings.length];
						if (strings.length == 0){
							returnedFromList = null;
						}else {
							for (int i = 0;i < strings.length;i++){
								returnedFromList [i]= strings[i];
							}
						}
						frame.test.setReturnedFromList(returnedFromList);
					}
					//Allow for JComboBox component
					if (frame.component.getClass().getName().equals(JComboBox.class.getName())){

						JComboBox comboBox = (JComboBox)frame.component;
						if (frame.list.getSelectedValue()!= null){
							Object []selects = frame.list.getSelectedValues();
							if (selects.length == 1){
								comboBox.setSelectedItem(selects[0 ]);
							}else {
								if (params != null){
									params.printLine(SandboxParameters.VISUAL_SEPARATOR);
									for (Object selected : selects){
										params.printLine(selected);
									}
									params.printLine(SandboxParameters.VISUAL_SEPARATOR);
								}
							}
						}
						if (frame.itemsToRemove != null){
							for (int i = 0;i < frame.itemsToRemove.size();i++){
								comboBox.removeItem(frame.itemsToRemove.get(i));
							}
						}
					}
					frame.setVisible(false);
					frame.setDone(true);
				} else if (actionEvent.getActionCommand().equals(SandboxListFrame.SORT_COMMAND)){
					int size = frame.list.getModel().getSize();
					ArrayList arrayList = new ArrayList();
					for (int i = 0;i < size;i++){
						arrayList.add(frame.list.getModel().getElementAt(i));
					}
					Collections.sort(arrayList);
					frame.list.setListData(arrayList.toArray());
				} else if (actionEvent.getActionCommand ().equals(SandboxListFrame.DELETE_COMMAND)){
					int size = frame.list.getModel().getSize();
					ArrayList arrayList = new ArrayList();
					for (int i = 0;i < size;i++){
						arrayList.add(frame.list.getModel().getElementAt(i));
					}
					Object[]selectedValues = frame.list.getSelectedValues();
					if (frame.itemsToRemove == null){
						frame.itemsToRemove = new ArrayList();
					}
					for (int i = 0;i < selectedValues.length;i++){
						arrayList.remove(selectedValues[i]);
						frame.itemsToRemove.add(selectedValues[i]);
					}
					frame.list.setListData(arrayList.toArray());
				}
			} catch (Exception en){
				frame.test.printLine(this.getClass().getName()
						+ ":: "+ en.getClass().getName()+
						": message = "+ en.getMessage(),en);
			}
		}
	}

	protected SandboxTestBase getTest() {
		return test;
	}
}
