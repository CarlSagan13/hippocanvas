// Tool32.java (erase me later)
package sandbox;

import java.io.File;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Run command in a separate process every so often",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"seconds,commandLine,workDir",
//			SandboxParameters.POPPAD_NAME,"exclusions",
			SandboxParameters.TRUEFALSE_NAME,"show",
			SandboxParameters.YESNO_NAME,"showProcessOutput"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool32 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		int seconds = Integer.parseInt(getString("seconds"));
		String commandLine = getString("commandLine");
		String workDir = getString("workDir");
//		List<String> exclusions = getStringList("exclusions");
		boolean show = getBoolean("show");
		boolean showProcessOutput = getBoolean("showProcessOutput");
		setTooltip(seconds + SandboxParameters.sandboxDelimiter + commandLine + 
				SandboxParameters.sandboxDelimiter + workDir + 
				SandboxParameters.sandboxDelimiter + " show=" + show);
		
		File dir = new File(workDir);
		int count = 0;
		if (dir.exists() && dir.isDirectory()){
			while(isRunning){
				printLine(this.getTime());
				this.runInSeparateProcess(commandLine, dir, show, showProcessOutput);
				this.checkRunning(seconds);
//				Thread.sleep(seconds);
				printLine("Run count = "+ count++);
			}
		}
		//END TEST
	}
}
