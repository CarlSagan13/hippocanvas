//SandboxMessage.java (erase me later)
package sandbox;

import javax.swing.*;

import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SandboxTestTab extends SandboxFrame implements SandboxCommon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SandboxKeyListener messageKeyListener = null;

	private String BUTTON_NAME_OK = "OK";
	private String BUTTON_NAME_CANCEL = "Cancel";
	private String BUTTON_NAME_SAVE = "Save";
	private String BUTTON_NAME_PHONE = "Phone";
	private String BUTTON_NAME_DELETE = "Delete";
	private String BUTTON_NAME_DOCK = "Dock";
	private String BUTTON_NAME_AOT = "AOT";
//	private JTextArea textArea = null;
	private Component component = null;
	private SandboxParameters params = SandboxParameters.createSandboxParameters();
	private SandboxBasket<String,String> envSet = null;
	private String title = "unknown";
	private String toolTip = "unknown";
	private Object mapId = null;
	private int tabIndex = -1;
	// private int staticTagNumber = -<N0>;

	private final JButton btnCancel = new JButton(BUTTON_NAME_CANCEL);

	private int status = 0;

	public SandboxTestTab() {
	}

	public SandboxTestTab(SandboxParameters params, boolean alwaysOnTop, boolean staticTab) {
		super();
		
		if (staticTab) {
			this.tabIndex = params.tabbedPane.getSelectedIndex();
		}
		this.setBorderColor();
		this.setIconImage(SandboxParameters.sandboxImage);
		this.params = params;
		this.component = params.tabbedPane.getSelectedComponent();
		this.title = params.tabbedPane.getTitleAt(params.tabbedPane.getSelectedIndex());
		this.toolTip = params.tabbedPane.getToolTipTextAt(params.tabbedPane.getSelectedIndex());
		String title = this.title;
		if (toolTip != null) {
			title = title + " / " + SandboxParameters.targetPackageName + " / " + toolTip;
		} else {
			title += SandboxParameters.sandboxDelimiter + params.configurationDirectory;
		}
		this.setTitle(title);

		this.setLayout(new BorderLayout());

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton btnOk = new JButton(BUTTON_NAME_OK);
		btnOk.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnOk);
		JButton btnDock = new JButton(BUTTON_NAME_DOCK);
		btnDock.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnDock);

		JComponent embeddedComponent = params.getEmbeddedComponent(component);

		if (embeddedComponent instanceof ISandboxBackedUp) {

			ISandboxBackedUp sandboxBackedUp = (ISandboxBackedUp) embeddedComponent;

			if (sandboxBackedUp.isEditable()) {
				JButton btnSave = new JButton(BUTTON_NAME_SAVE);
				btnSave.addActionListener(new ButtonListener(this));
				buttonPanel.add(btnSave);
			}

		}

		JButton btnPhone = new JButton(BUTTON_NAME_PHONE);
		btnPhone.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnPhone);

		
//*/////////////////////////DMH
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setEditable(true);
		comboBox.setName(SandboxParameters.SEARCH_COMBOBOX_NAME);

		for (int index = 0; index < params.searchComboBox.getItemCount(); index++) {
			comboBox.addItem(params.searchComboBox.getItemAt(index));
		}
		
		Component editorComponent = comboBox.getEditor().getEditorComponent();

		editorComponent.setName(SandboxParameters.MESSAGE_SEARCH_COMBOBOX_NAME);
		editorComponent.addFocusListener(params.focusListener);
		comboBox.addActionListener(params.cbActionListener);

		messageKeyListener = new SandboxKeyListener(params, embeddedComponent);
		comboBox.addKeyListener(messageKeyListener);
		comboBox.getEditor().getEditorComponent().addKeyListener(messageKeyListener);

		comboBox.setPreferredSize(new Dimension(200, 30));
		
		buttonPanel.add(comboBox);
	
		JButton btnAot = new JButton(BUTTON_NAME_AOT);
		btnAot.addActionListener(new ButtonListener(this));
		buttonPanel.add(btnAot);

		if (!staticTab) {
			btnCancel.addActionListener(new ButtonListener(this));
			this.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					btnCancel.doClick();
				}
			});
			buttonPanel.add(btnCancel);
		}

		embeddedComponent.removeKeyListener(params.keyListener);
		embeddedComponent.addKeyListener(messageKeyListener);
		
		
//*/////////////////////DMH		
//		params.newTab("description", component, "title", params.iconPretty);
		//	public void newTab(String description, String pageString, String title, int index, boolean enabled) {
		if (staticTab) {
			params.newTab("mydescription", params.getTabText(embeddedComponent), component.getName(),
					params.tabbedPane.getSelectedIndex(), false);
			params.tabbedPane.setIconAt(params.tabbedPane.getSelectedIndex() - 1, params.iconWatch);

		}
		
//*/////////////////////////	
		
		
		

		getContentPane().add(component, BorderLayout.CENTER);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		setDefaultCloseOperation(SandboxFrameBase.DISPOSE_ON_CLOSE);
		pack();

		if (alwaysOnTop) {
			setAlwaysOnTop(true);
			btnAot.setIcon(params.iconPretty);
		} else {
			setAlwaysOnTop(Boolean.valueOf(System.getProperty(SandboxFrameBase.ALWAYS_ON_TOP_VAR_NAME, "false")));
			btnAot.setIcon(params.iconWatch);
		}
		this.setBounds(params.tabbedPane.getX(), params.tabbedPane.getX(), 2 * params.tabbedPane.getWidth() / 3,
				params.tabbedPane.getHeight());
		this.setLocationRelativeTo(params.tabbedPane);
		setVisible(true);

		this.mapId = params.addToGlobalMap(this);
		
	}

	public int getStatus() {
		return status;

	}

//	public String getText() {
//		return this.textArea.getText();
//	}

	protected static boolean isEditing(Component component, SandboxParameters params) {
		if (component == null) {
			return false;
		}
		ISandboxBackedUp backedUp = SandboxTestTab.getSandboxTabComponent(component);
		if (backedUp != null) {
			if (backedUp.isEditable() && backedUp.isEnabled()) {
				return true;
			}
		}
		return false;
	}

	protected static boolean isRunning(Component component, SandboxParameters params) {
		if (component == null) {
			return false;
		}
		SandboxTextArea textArea = SandboxTestTab.getSandboxTextArea(component);
		if (textArea != null) {
			SandboxTestBase test = textArea.getTest();
			SandboxThread sandboxThread = textArea.getSandboxThread();
			if ((test != null && test.getIsRunning()) || (sandboxThread != null && sandboxThread.isRunning())) {
				return true;
			}
		}
		return false;
	}

	protected static boolean stopTest(Component component) {
		if (component == null) {
			return false;
		}
		SandboxTextArea textArea = SandboxTestTab.getSandboxTextArea(component);
		if (textArea != null) {
			SandboxTestBase test = textArea.getTest();

			if (test != null && test.getIsRunning()) {
				test.endMe();
				return true;
			}
		}
		return false;
	}

	protected static SandboxTextArea getSandboxTextArea(Component component) {
		if (component instanceof JScrollPane) {
			JScrollPane scrollPane = (JScrollPane) component;
			if (scrollPane.getComponent(0) instanceof JViewport) {
				JViewport viewport = (JViewport) scrollPane.getComponent(0);
				if (viewport.getComponent(0) instanceof SandboxTextArea) {
					SandboxTextArea textArea = (SandboxTextArea) viewport.getComponent(0);
					return textArea;
				}
			}
		}
		return null;

	}

	protected static ISandboxBackedUp getSandboxTabComponent(Component component) {
		if (component instanceof JScrollPane) {
			JScrollPane scrollPane = (JScrollPane) component;
			if (scrollPane.getComponent(0) instanceof JViewport) {
				JViewport viewport = (JViewport) scrollPane.getComponent(0);
				if (viewport.getComponent(0) instanceof ISandboxBackedUp) {
					return (ISandboxBackedUp) viewport.getComponent(0);
				}
			}
		}
		return null;
	}

	private void setBorderColor() {

		String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
		Color borderColor = new Color(255, 255, 255);

		try {
			borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]),

					Integer.parseInt(ebc[2]));
		} catch (NumberFormatException nfe) {
			System.err.println("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");

		}
		Border border = BorderFactory.createLineBorder(borderColor, 5);
		this.getRootPane().setBorder(border);
	}
	
	public class ButtonListener implements ActionListener {
		private SandboxTestTab frame;

		public ButtonListener(SandboxTestTab frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent actionEvent) {
			if (actionEvent.getActionCommand().equals(BUTTON_NAME_PHONE)) {
				System.err.println("dmh1451 " + params.tabbedPane.getParent().getClass().getName());
				System.err.println("dmh1121 " + params.frame.getName());
				SandboxFrameBase.phoneCall("Oh, there you are!", "Where are you?");
				return;
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_OK) || 
					actionEvent.getActionCommand().equals(BUTTON_NAME_DOCK) ||
					SandboxTestTab.isRunning(component, params)) {
				status = 1;
				
				if (component != null) {
					int currentTabIndex = params.tabbedPane.getTabCount();
					if (tabIndex >= 0) {
						params.tabbedPane.remove(tabIndex);
						currentTabIndex = tabIndex;
						params.tabbedPane.insertTab(component.getName(), params.iconWatch, component, toolTip, currentTabIndex);
					} else {
						params.tabbedPane.insertTab(component.getName(), null, component, toolTip, currentTabIndex);
					}
					if (SandboxTestTab.isRunning(component, params)) {
						params.tabbedPane.setIconAt(currentTabIndex, params.iconRunning);
						params.announce.setEnabled(true);
					}
					if (SandboxTestTab.isEditing(component, params)) {
						params.tabbedPane.setIconAt(currentTabIndex, params.iconPretty);
					}
					if (actionEvent.getActionCommand().equals(BUTTON_NAME_OK)) {
						params.tabbedPane.setSelectedComponent(component);
						
						
						params.selectTab(params.tabbedPane.getSelectedIndex());
						
						//params.selectTab(params.selectedTab);
//						params.unselectOtherTabs(params.lastSelectedTab);
						
//					} else if (actionEvent.getActionCommand().equals(BUTTON_NAME_DOCK)) {
//						params.selectTab(params.selectedTab);
					}

					if (messageKeyListener != null) {
						JComponent embeddedComponent = params.getEmbeddedComponent(component);
						embeddedComponent.removeKeyListener(messageKeyListener);
						embeddedComponent.addKeyListener(params.keyListener);
					}
					params.removeFromGlobalMap(mapId);
				}
			}
			if (actionEvent.getActionCommand().equals(BUTTON_NAME_SAVE)) {
				JComponent embeddedComponent = params.getEmbeddedComponent(component);

				if (embeddedComponent instanceof ISandboxBackedUp) {

					ISandboxBackedUp sandboxBackedUp = (ISandboxBackedUp) embeddedComponent;
					try {
						sandboxBackedUp.setDirty(true);
						sandboxBackedUp.saveBackedUp();
					} catch (SandboxException se) {
						params.printLine("Could not save " + sandboxBackedUp.getFile().getPath());
					}
				}
				return;
			}

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_CANCEL)) {

				if (SandboxTestTab.isRunning(component, params)) {
					SandboxTestTab.stopTest(component);
				}
				if (component != null) {
					JComponent embeddedComponent = params.getEmbeddedComponent(component);

					if (embeddedComponent instanceof ISandboxBackedUp) {
						ISandboxBackedUp sandboxBackedUp = (ISandboxBackedUp) embeddedComponent;
						if (sandboxBackedUp.isEditable()) {
							sandboxBackedUp.setDirty(false);
							try {
								sandboxBackedUp.closeBackedUp();
								params.removeFromGlobalMap(sandboxBackedUp.getMapId());
							} catch (SandboxException se) {
								params.printLine("Could not close " + sandboxBackedUp.getFile().getPath());
							}

							params.removeFromGlobalMap(sandboxBackedUp.getMapId());
						}
					}
					params.removeFromGlobalMap(mapId);
				}

				status = -1;
			}

			
			
			
//*/////////////////////////////////DMH
			if (actionEvent.getActionCommand().equals(BUTTON_NAME_AOT)) {
				if (actionEvent.getSource() instanceof JButton) {
					JButton aotButton = (JButton)actionEvent.getSource();
					if (this.frame.isAlwaysOnTop()) {
						this.frame.setAlwaysOnTop(false);
						aotButton.setIcon(params.iconWatch);
					} else {
						this.frame.setAlwaysOnTop(true);
						aotButton.setIcon(params.iconPretty);
					}
					return;
				}
			}
//*////////////////////////////////////

			if (actionEvent.getActionCommand().equals(BUTTON_NAME_DELETE)) {
				if (actionEvent.getSource() instanceof JButton) {
					System.clearProperty("sandbox.java." + envSet.getName());
					JButton deleteButton = (JButton)actionEvent.getSource();
					deleteButton.setEnabled(false);
					return;
				}
			}

			frame.setVisible(false);
			frame.dispose();
		}

	}

//	protected ScheduledFuture scheduleHandle = null;

//	class CloseTimer implements Runnable {
//
//		public CloseTimer() {
//		}
//
//		@Override
//		public void run() {
//			System.err.println("Auto-closing '" + title + "'");
//			if (status == 0) {
//				status = 1;
//			}
//			setVisible(false);
//		}
//	}

	public void close() {
		SandboxTestTab.stopTest(component);

		setVisible(false);
		dispose();
	}

	@Override
	public String toString() {
		if (component != null) {
			SandboxTextArea textArea = getSandboxTextArea(component);
			if (textArea != null) {
				String theReturn = textArea.getText();
				return theReturn;
			} else {
				Component embeddedComp = params.getEmbeddedComponent(component);
				if (embeddedComp instanceof JTextArea) {
					return ((JTextArea) embeddedComp).getText();
				} else {
					return embeddedComp.getClass().getName();
				}
			}
		} else {
			return "dmh239";
		}
	}

}
