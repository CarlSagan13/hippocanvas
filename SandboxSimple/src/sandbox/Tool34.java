package sandbox;

import java.net.URLDecoder;

@SandboxAnnotation(description = "Replace in a string (replacement is URL-encoded US-ASCII; don't use control characters as replacements!)", parameterDescriptions = {
		SandboxParameters.FOCUSED_NAME, "regexToString", }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool34 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] regexToString = getStringArrayNoDelim("regexToString");
		String regex = regexToString[0];
		String to = regexToString[1];
		String string = regexToString[2];
		String replacement = URLDecoder.decode(to, "US-ASCII");

		if (regexToString.length > 3) {
			string += "\n";
			for (int index = 3; index < regexToString.length; index++) {
				string += regexToString[index] + "\n";
			}
		}
		printLine("regex = " + regex);
		printLine("replacement = " + replacement);
		printLine("string = " + string);
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine(string.replaceAll(regex, replacement));
		// END TEST
	}

}
