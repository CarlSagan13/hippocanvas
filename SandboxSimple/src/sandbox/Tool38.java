package sandbox;

import java.io.File;
import java.util.List;
import java.util.Map;

@SandboxAnnotation(description = "Find string in jar files", parameterDescriptions = { SandboxParameters.FOCUSED_NAME,
		"searchString,jarDirectory", SandboxParameters.TRUEFALSE_NAME,
		"recurse" }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool38 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String searchString = getString("searchString");
		String jarDirectory = getString("jarDirectory");
		boolean recurse = getBoolean("recurse");

		File dirJar = new File(jarDirectory);
		Map<String, Map<String, List<String>>> jarMap = sandbox.Utilities.findInJars(dirJar, searchString, recurse, this);

		for (String jarKey : jarMap.keySet()) {
			Map<String, List<String>> entryMap = jarMap.get(jarKey);
			if (entryMap.size() > 0) {
				for (String entryKey : entryMap.keySet()) {
					List<String> lines = entryMap.get(entryKey);
					if (lines.size() > 0) {
						printLine(SandboxParameters.VISUAL_SEPARATOR);
						printLine(jarKey + "\n\t" + entryKey);
						for (String line : lines) {
							printLine(line);
						}
					}
				}
			}
		}
		// END TEST
	}
}
