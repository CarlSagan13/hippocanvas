// SandboxTestBase.java (erase me later)
package sandbox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.annotation.Annotation;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.text.Document;


import sandbox.Globals.MapCleanRunner;
import sandbox.SandboxParameters.TimeRunner;

public class SandboxTestBase extends Thread {
//	protected final Logger testLogger = LogManager.getLogManager().getLogger(getClass().getName());
	protected final Logger testLogger = Logger.getLogger(SandboxTestBase.class.getName());

	public static final String UNDER_CONSTRUCTION = SandboxParameters.SANDBOX_UNDER_CONSTRUCTION; //TODO Already referenced from SandboxParameters, redundant
	public static final String SANDBOX_ALERT_ENV = "sbx.alert";
	public static final String SANDBOX_POST_PROCESS_ALERT_ENV = "sbx.postprocess";
	public static final String SANDBOX_EXCLUDE_PREFIX = "sbx.exclude";
	public static final String SANDBOX_CONFIRM_COMMAND_PREFIX = "sbx.confirm.command";
	public static final String SANDBOX_CONFIRM_DIRECTORY_PREFIX = "sbx.confirm.directory";
	public static final String SANDBOX_ADVICE_PREFIX = "sbx.advice";
	public static final String SANDBOX_TEST_ANNOUNCE_PREFIX = "sbx.test.setting.announce.";
	public static final String SANDBOX_TEST_ENCLOSER_PREFIX = "sbx.test.setting.encloser.";
	public static final String SANDBOX_TEST_SETTING_NOTIFY_PREFIX = "sbx.test.setting.notify_";
	public static final String SANDBOX_TEST_SETTING_NOTIFY_PROFILE_PREFIX = "sbx.test.setting.notify.profile_";
	
	protected SandboxParameters parameters;
	private int numParams;
	private int tabNumber;
	private String tabDisplayName = this.getClass().getSimpleName();
//	private String encloser = null; //TODO This should go inside the process method and be passed through to the printFromBuffer()
	public boolean isRunning = false;
	public boolean isReady = false;
	public boolean announce = false;
	public boolean useButton = true;
	public boolean presetParameters = false;
	private String testName;
	private String description = "This is the description for " + this.getClass().getName();
	protected String[] parameterDescriptions;
	protected Hashtable<String, String> parameterValues = new Hashtable<String, String>();
	protected Hashtable<String, String> popPadValues = new Hashtable<String, String>();
	protected Hashtable<String, String> capturedValues = new Hashtable<String, String>();
	protected Set<SandboxConnection> connections = new HashSet<SandboxConnection>();
	protected SandboxTextArea textArea = new SandboxTextArea(this);
	protected JScrollPane scrollPane = null;
	protected Process process = null;
	protected Object[] returnedFromList = null;
	protected long startTime = 0;
	protected long stopTime = 0;
	private String resultFilePath = null;
	
//*/////////////////DMH
	private int lastLineCount = 0;
	
	private int lastFoundEnd = 0;
	
//*////////////////////

	public Set<SandboxConnection> getConnections() {
		return connections;
	}

	/**
	 * @ return the popPadValues
	 */
	public Hashtable<String, String> getPopPadValues() {
		return popPadValues;
	}

	/**
	 * @ param popPadValues the popPadValues to set
	 */
	public void setPopPadValues(Hashtable<String, String> popPadValues) {
		this.popPadValues = popPadValues;
	}

	public boolean isReady() {
		return isReady;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
		if (isReady) {
			parameters.showButtonReady(this.getTestName());
		} else {
			if (this.getIsRunning()) {
				parameters.showButtonRunning(this.getTestName());
			} else {
				parameters.showButtonNotReady(this.getTestName());
			}
		}
	}

	public SandboxParameters getParameters() {
		return parameters;
	}

	public String[] getParameterDescriptions() {
		return parameterDescriptions;
	}

	public void setParameterDescriptions(String[] parameterDescriptions) {
		this.parameterDescriptions = parameterDescriptions;
	}

	public Object[] getReturnedFromList() {
		return returnedFromList;
	}

	public void setReturnedFromList(Object[] returnedFromList) {
		this.returnedFromList = returnedFromList;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public int getNumParams() {
		return this.numParams;
	}

	protected void setNumParams(int numParams) {
		this.numParams = numParams;
	}

	public SandboxTestBase() {
		testLogger.fine("Empty constructor invoked");
		textArea.setEditable(false);
	}

	public SandboxTestBase(SandboxParameters parameters) {
		testLogger.fine("Constructor invoked");
		this.parameters = parameters;
		textArea.setEditable(false);
	}

	public int getTabNumber() {
		return tabNumber;
	}

	public void setTabNumber(int tabNumber) {
		this.tabNumber = tabNumber;
	}

	public String getTabDisplayName() {
		return tabDisplayName;
	}

	public void setTabDisplayName(String tabDisplayName) {
		this.tabDisplayName = tabDisplayName;
	}

	public boolean getIsRunning() {
		return this.isRunning;
	}

	public void setIsRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public String getDescription() {
		return this.description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	public String getTestName() {
		return this.testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public void resetStartTime() {
		startTime = (new Date()).getTime();
	}

	public String getStartTime() {
		Date date = new Date(startTime);
		SimpleDateFormat dateFormat = new SimpleDateFormat(SandboxParameters.DATE_FORMAT);
		return dateFormat.format(date);
	}

	public String getStopTime() {
		Date date = new Date(stopTime);
		SimpleDateFormat dateFormat = new SimpleDateFormat(SandboxParameters.DATE_FORMAT);
		return dateFormat.format(date);
	}

	/**
	 * 
	 * <b>Description:«FS> b> Set this if you want to save the results of a
	 * test to a file
	 * 
	 * @ param resultFilePath
	 */
	public void setResultFilePath(String resultFilePath) {
		this.resultFilePath = resultFilePath;
	}

	public String getTime() {
		return Utilities.getTime();
	}

	public String getTime(String format) {
		return Utilities.getTime(format);
	}

	public void run() {
		System.err.println("dmh358 tab " + parameters.tabbedPane.getSelectedIndex());
		
		testLogger.info("");
		Date startDate = new java.util.Date();
		try {
			startTime = startDate.getTime();
			parameters.printLine(this.getClass().getPackage().getName() + "." + testName + "[" + this.getId() + "] "
					+ getStartTime() + " Start", true, true);
			parameters.announce.setEnabled(true);

			if (parameters.getSelectedTab() > SandboxParameters.LAST_STATIC_TAB) { // DMH
				parameters.preTestSelectedTab = parameters.getSelectedTab();
			}

			test(parameters);
		} catch (NoClassDefFoundError ncdfe) {
			printLine("You are missing class " + ncdfe.getMessage() + " in the classpath");
		} catch (Throwable en) {
			if (scrollPane != null) {
				printLine(en.getClass().getName() + ": " + en.getMessage(), en);
				en.printStackTrace();
			} else {
				parameters.printLine(en.getClass().getName() + ": " + en.getMessage(), en);
				en.printStackTrace();
			}
			parameters.printLine(this.getClass().getPackage().getName() + "." + testName + "[" + this.getId() + "] "
					+ getStartTime() + " Error: " + en.getClass().getSimpleName(), true, true);
			testLogger.log(Level.ALL, en.getMessage(), en);
			en.printStackTrace();
			
			Properties props = System.getProperties();
			for (Object objKey : props.keySet()) {
				String key = (String) objKey;
				if (key.startsWith(SANDBOX_ADVICE_PREFIX)) {
					String[] regexMessage = System.getProperty(key, ".*\tNo message").split(SandboxParameters.sandboxDelimiter);
					String regex = regexMessage[0];
					String message = "pre-URL decoding";
					try {
						message = URLDecoder.decode(regexMessage[1], "US-ASCII");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					String exceptionMessage = this.getTestName() + " " + en.getClass().getName() + " " + en.getMessage();
					if (exceptionMessage.matches(regex)) {
						showConfirmMessage(message);
					}
				}
			}
		
		} finally {
			printLine("--SBXTESTSTOP--");
			
			printLine(SandboxParameters.VISUAL_SEPARATOR + " RECORDED:");
			printLine(this.getClass().getSimpleName() + "\t" + this.getDescription() + "\t" + SmartOpen.RECORDED_FLAG);
			for (String key : parameterValues.keySet()) {
				String value = parameterValues.get(key);
				if (value.split("\n").length > 1) {
					try {
						value = "-encoded " + URLEncoder.encode(value, "US-ASCII");
					} catch (UnsupportedEncodingException e) {
						printLine(e);
						e.printStackTrace();
					}
				}
				if (value.length() > 2000) {
					value = "TRUNCATED: " + value.substring(0, 2000);
				}
				printLine(key + "\t" + value);
			}

			stopTime = (new java.util.Date()).getTime();
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("Start:\t" + getStartTime());
			printLine("Stop:\t" + getStopTime());
			parameters.printLine(this.getClass().getPackage().getName() + "." + testName + "[" + this.getId() + "] "
					+ getStopTime() + " Stop", true, true);
			this.setIsRunning(false);

			if (scrollPane != null) {
				printLine("Thread " + this.getId() + " : " + (stopTime - startTime) + " ms lapse");
			}
			parameters.printLine(this.getClass().getPackage().getName() + "." + testName + "[" + this.getId() + "] "
					+ (stopTime - startTime) + " ms lapse", true, true);

			
			playSound();
			showMessage(this.getTestName() + " Finished");
			cleanup();
		}
	}

	protected void waitUntilCancelled() throws Exception {
		while (isRunning) {
			Thread.sleep(100L);
		}
	}

	protected void playSound() {
		if (announce) {
			Toolkit.getDefaultToolkit().beep();
//			try {
//				ClassPathResource resource = new ClassPathResource(parameters.sandboxSoundFile);
//				InputStream inputstream = resource.getInputStream();
//				Utilities.playThreadedSound(inputstream, parameters);
//			} catch (IOException ioe) {
//				printLine(ioe.getMessage(), ioe);
//				testLogger.debug(ioe);
//			}
		}
	}

	protected int showMessage() {
		return showMessage("Message");
	}

	protected int showMessage(String message) {

		if (announce) {
			Toolkit.getDefaultToolkit().beep();

			SandboxMessage sbm = new SandboxMessage("[" + SandboxParameters.targetPackageName + "] " + message,
					textArea.getText(), parameters.tabbedPane, parameters.poppadMaxLines, 60, 
					true, false, parameters.messageCount++, null);
			if (parameters.messageCount > 10) {
				parameters.messageCount = -10;
			}
			int result = sbm.getStatus();

			if (result == 0) {
				parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
			} else {
				parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconRunning);
			}
			sbm.setSavedAOT(false);
			sbm.setAlwaysOnTop(true);
			sbm.phoneIn();
			return sbm.getStatus();
		}
		return 0;
	}

	protected boolean confirm(String message) {
		if (showConfirmMessage(message) == 1) {
			return true;
		}
		return false;
	}

	public void translate(Object obj) {
		translate(obj, false, false);
	}

	public void translate(Object obj, boolean showContents, boolean showReflection) {
		try {
			parameters.translator.setVerbose(showContents);
			parameters.translator.setReflection(showReflection);
			parameters.translator.translate(obj, this);
		} catch (Exception en) {
			printLine("Could not translate", en);

		}
	}

	public boolean isAnnounce() {
		return announce;
	}

	public void setAnnounce(boolean announce) {
		this.announce = announce;
		this.parameters.announce.setSelected(announce);
	}

	protected void init() {
		Annotation annotation = this.getClass().getAnnotation(SandboxAnnotation.class);
		SandboxAnnotation doc = (SandboxAnnotation) annotation;
		setTestName(this.getClass().getSimpleName());
		initParameters(doc.parameterDescriptions());
		if (doc.description().length() > 0) {

			setDescription(doc.description() + (doc.parameterDescriptions().length > 0 ? ": " : "")
					+ getParametersDescription(doc.parameterDescriptions()));
		} else {
			setDescription(description);
		}
		setNumParams(doc.parameterDescriptions().length / 2);

	}

	protected void init(String description, String[] paramDescriptions) {
		this.setTestName(this.getClass().getSimpleName());
		initParameters(paramDescriptions);
		if (paramDescriptions.length > 0) {
			this.setDescription(description + " : " + getParametersDescription(paramDescriptions));
		} else {
			this.setDescription(description);
		}
		this.setNumParams(paramDescriptions.length / 2);
	}

	public void test(SandboxParameters parameters) throws Exception {
		// just for show: child.test()is run
		parameters.printLine("In the test(params) method that doesn't do anything.");
	}

	protected void test(String[] parameterDescriptions) throws SandboxException {
//		 if (parameters.getSelectedTab() >= SandboxParameters.LAST_STATIC_TAB) {
// 			 System.err.println("adding " + parameters.getSelectedTab());
//	 			parameters.tabHistory.add(0, parameters.getSelectedTab());
// 		 }
		 
		 
		 
		if (!this.presetParameters) {
			parameters.highlightParameters(parameterDescriptions);
			parameterValues = getParameterValues(parameterDescriptions);
			this.presetParameters = false;
		} else {
			printLine(getParameterValues(parameterDescriptions));
			printALine();
		}
		printLine("Start:\t" + getStartTime());
		printLine("--SBXTESTSTART--");

		if (!this.isRunning) { // last chance to abort
			throw (new SandboxException(getTestName() + " intentionally aborted"));
		}
		if (!parameters.guiLess) {
			try {
				if (scrollPane == null) {
					scrollPane = new JScrollPane(textArea);
					if (this instanceof Command) {
						scrollPane.setName(parameters.commandName);
					} else {
						scrollPane.setName(this.getTabDisplayName());
					}
					
//					parameters.lastSelectedTab = parameters.getTabbedPane().getSelectedIndex();
					
					setTabNumber(parameters.tabbedPane.getTabCount());
					textArea.addFocusListener(parameters.getFocusListener());
					textArea.addKeyListener(parameters.keyListener);
					textArea.addMouseListener(parameters.mouseListener);
//					textArea.setFont(Font.decode(Font.MONOSPACED).deriveFont(Font.PLAIN));
//					String[] clf = System.getProperty("sbx.conf.textFont","Dialog_2_11").split("_");
					String[] clf = Utilities.getEnvFont();
					Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
					textArea.setFont(textFont); // Re23.04.10.11.55.19.248
				
					
					Annotation annotation = this.getClass().getAnnotation(SandboxAnnotation.class);
					SandboxAnnotation doc = (SandboxAnnotation) annotation;
					if (doc.showInTabPanel()) {
						parameters.tabbedPane.add(scrollPane, getTabNumber()); //This is where the tab appears
						parameters.tabbedPane.setToolTipTextAt(getTabNumber(), getDescription());
						parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconRunning);
						parameters.tabbedPane.setSelectedIndex(getTabNumber());
						parameters.selectTab(getTabNumber());
					}
				}
			} catch (ArrayIndexOutOfBoundsException aioobe) {
				parameters.printLine("Not opening separate tabbed pane.", aioobe);
			} catch (Exception en) {
				parameters.printLine("Unable to open tabbed pane.", en);
			}
		}
	}

	protected void cleanup() {
		
		//TODO Record the test's parameters for some kind of replay mechanism
/*////////////////////DMH342
		for (String key : this.parameterValues.keySet()) {
			printLine(key + " :: " + this.parameterValues.get(key));
		}
//*///////////////////////		
		
		
		testLogger.fine("");

		this.setIsRunning(false);

		int tabNumber = this.parameters.tabbedPane.indexOfComponent(scrollPane);
		if (tabNumber > -1) {
			// this.parameters.tabbedPane.setTitleAt(tabNumber, getTestName());
			Icon icon = parameters.tabbedPane.getIconAt(getTabNumber());
			if (icon != null && !icon.equals(parameters.iconStopped)) {
				this.parameters.tabbedPane.setIconAt(getTabNumber(), null);
			}
//			if (!this.parameters.tabbedPane.getIconAt(getTabNumber()).equals(parameters.iconStopped)) {
//				this.parameters.tabbedPane.setIconAt(getTabNumber(), null);
//			}
		}
		if (tabNumber == parameters.tabbedPane.getSelectedIndex()) {
			parameters.announce.setEnabled(false);
			parameters.announce.setSelected(false);
		}

		if (this.resultFilePath != null) {
			try {
				Utilities.saveFileString(resultFilePath, this.textArea.getText());
				printLine("Results file:\n\t" + resultFilePath);
			} catch (FileNotFoundException e) {
				parameters.printLine("Could not save results file", e);
			} catch (IOException e) {
				parameters.printLine("Could not save results file", e);
			} catch (SecurityException se) {
				parameters.printLine("Could not save results file", se);
			}
		}

		closeConnections();

		this.presetParameters = false;

		parameters.showButtonStopped(getTestName());
		
// 		 if (parameters.getSelectedTab() > SandboxParameters.LAST_STATIC_TAB) {
// 			parameters.tabHistory.add(0, parameters.getSelectedTab());
// 		 }

	}

	public void setParameters(SandboxParameters parameters) {
		this.parameters = parameters;
	}

	protected boolean initParameters(String[] parameterDescriptions) {
		this.parameterDescriptions = parameterDescriptions;
		if (!parameters.isInitialized) {
			for (int i = 0; i < parameterDescriptions.length; i += 2) {
				try {
					initParameter(Integer.parseInt(parameterDescriptions[i]), parameterDescriptions[i + 1]);
				} catch (NumberFormatException nfe) {
					testLogger.fine("Has an unindexed parameter");
				}
			}
		}
		return true;
	}

	protected String getParametersDescription(String[] parameterDescriptions) {
		String parametersDescription = "";
		for (int i = 0; i < parameterDescriptions.length; i += 2) {
			parametersDescription += parameterDescriptions[i] + "-" + parameterDescriptions[i + 1] + " ";
		}
		return parametersDescription;
	}

	protected Hashtable<String, String> getParameterValues(String[] parameterDescriptions)
			throws SandboxException {

		Hashtable<String, String> parameterValues = new Hashtable<String, String>();
		printLine(getDescription() + "\n" + SandboxParameters.VISUAL_SEPARATOR); //This is where the test output begins
		for (int i = 0; i < parameterDescriptions.length; i += 2) {
			String key = "";
			String value = "";
			try {
				int parameterIndex = Integer.parseInt(parameterDescriptions[i]);
				key = parameterDescriptions[i + 1];
				value = parameters.getParameterValueAt(parameterIndex);
			} catch (NumberFormatException nfe) { // focused variable
				key = parameterDescriptions[i + 1];
				if (parameterDescriptions[i].equals(SandboxParameters.FOCUSED_NAME)) {

					String[] names = parameterDescriptions[i + 1].split(",");
					String[] values = parameters.getFocusedText(names.length);
					if (values.length == 1) {
						value = values[0];
					} else {
						value = null; // focused variable series
						int index = 0;
						for (String seriesKey : names) {
							parameterValues.put(seriesKey, parameters.replaceValues(noNulls(values[index])));
							printLine(seriesKey + " = " + parameterValues.get(seriesKey));
							index++;
						}

					}
				} else if (parameterDescriptions[i].equals(SandboxParameters.SCRATCHPAD_NAME)) {
					value = parameters.getScratchPadText();
				} else if (parameterDescriptions[i].equals(SandboxParameters.PASSWORD_NAME)) {
					value = parameters.getPassword();
				} else if (parameterDescriptions[i].equals(SandboxParameters.TRUEFALSE_NAME)) {
					value = parameters.getTrueFalse();
				} else if (parameterDescriptions[i].equals(SandboxParameters.YESNO_NAME)) {
					value = parameters.getYesNo();
				} else if (parameterDescriptions[i].startsWith(SandboxParameters.POPPAD_NAME)) {
					String parameterName = "";
					try {
						parameterName = parameterDescriptions[i + 1];
						String popPadName = Utilities.getPoppadFileName(this, parameterName);
						String popPadFilePath = parameters.configurationDirectory + File.separator
								+ popPadName;

						if (parameters.showPopPad.isSelected() || !(new File(popPadFilePath)).exists()) {
							
							SandboxTextAreaFrame sbxTAF = new SandboxTextAreaFrame();
							
							sbxTAF = new SandboxTextAreaFrame(this, parameterName,
									parameters.configurationDirectory + File.separator
											+ Utilities.getPoppadFileName(this, parameterName),
									parameters.poppadMaxLines);
							parameters.addToGlobalMap(sbxTAF);
							
							try {
								while(sbxTAF.isVisible()){
									Thread.sleep(100);
								}
							} catch(InterruptedException ie){
								sbxTAF.getTest().printLine("Wait for input was interrupted.");
							} finally {
								sbxTAF.getTest().resetStartTime();
							}


						} else {
							try {
								this.getPopPadValues().put(parameterName, Utilities.getFileString(popPadFilePath));
							} catch (IOException ioe) {
								parameters.printLine("Could not load Pop Pad: " + popPadFilePath);
								try {
									Utilities.saveFileString(Utilities.getPoppadFileName(this, parameterName),
											Utilities.getResourceStringInternal("start/sandbox/poppad.template"));
								} catch (Exception en) {
									parameters.printLine("Could not save poppad for "
											+ Utilities.getPoppadFileName(this, parameterName), en);
								}
							}
						}
						value = Utilities.noNulls(this.getPopPadValues().get(parameterName));
						int indexOfPopPadNotes = value.indexOf(SandboxParameters.POPPAD_NOTES);

						if (indexOfPopPadNotes > 0) {
							value = value.substring(0, value.indexOf(SandboxParameters.POPPAD_NOTES));
							value = removePoppadComments(value);
						}

					} catch (java.awt.HeadlessException he) {
						parameters.printLine("PopPad gui exception", he);
						String popPadPathName = "." + File.separator + SandboxParameters.targetPackageName
								+ File.separator + this.getClass().getSimpleName() + "_" + parameterName
								+ ".txt";

						printLine("Trying to load poppad file: " + popPadPathName);
						try {
							value = Utilities.getFileString(popPadPathName);

							try {
								value = value.substring(0, value.indexOf(SandboxParameters.POPPAD_NOTES));
								value = removePoppadComments(value);
							} catch (StringIndexOutOfBoundsException sioobe) {
								parameters.printLine("Could not parse out \"" + SandboxParameters.POPPAD_NOTES + "\"");
								Utilities.saveFileString(popPadPathName,
										value + "\n\n" + SandboxParameters.POPPAD_NOTES);
							}
						} catch (IOException ioe1) {

							parameters.printLine("PopPad file exception", ioe1);
							value = "";
							try {
								Utilities.saveFileString(popPadPathName, "Empty\n" + SandboxParameters.POPPAD_NOTES);
							} catch (IOException ioe2) {
								parameters.printLine("Could not save a default PopPad file", ioe2);
							}
						}
					} catch (StringIndexOutOfBoundsException sioobe) {
						parameters.printLine("PopPad exception", sioobe);
						value = "";
					}
					if (value.equals("")) {
						throw new SandboxException(getTestName() + " intentionally aborted");
					} else {
						String focusedText = parameters.getFocusedText();
					}
					//gather poppad parameter values
					if (parameterDescriptions[i].equals(SandboxParameters.POPPADVALS_NAME)) {
						String nameValues = value.trim();
						for (String line : nameValues.split("\\n")) {
							try {
								String ppvParameterName = line.substring(0, line.indexOf('='));
								String ppvParameterValue = line.substring(line.indexOf('=') + 1);
								parameterValues.put(ppvParameterName, ppvParameterValue);
							} catch(Exception en) {
								printLine("*** PopPad values malformed configuration: " + line);
									int ret = this.showConfirmMessage("PopPad values malformed configuration:\n\t" + line);
									if (ret != 1) {
										throw new SandboxException("Cancelled at poppad.");
									}
							}
						}
					}
				}
			}
			if (value != null && value.length() > 0) {
				parameterValues.put(key, parameters.replaceValues(value));
				if (key.equals(SandboxParameters.PASSWORD_NAME)) {
					value = Utilities.base64Encode(value);
				}
				printLine(key + " = " + noNulls(value).trim());
				
			}
		}

		printLine(SandboxParameters.VISUAL_SEPARATOR);
		return parameterValues;
	}

	protected String removePoppadComments(String text) {
		testLogger.fine(text);

		String newString = "";
		String[] lineSplitString = text.split("\n");
		for (String line : lineSplitString) {
			if (!line.startsWith("#")) {
				newString += line + "\n";
			}
		}

		return newString.length() == 0 ? " " : newString;
	}

	public int showConfirmMessage(String message) {
		
		if (SandboxFrameBase.messageCount > 5) {
			SandboxFrameBase.messageCount = -5;
		}
		SandboxMessage msg = new SandboxMessage("CHOOSE", message.toString(), parameters.getTabbedPane(), 20, 60, true,
				false, SandboxFrameBase.messageCount++, null);
		return msg.getStatus();
	}
	

//*////////////////////////////////////////////////	

	public Object[] getValueFromWindowList(String[] items) throws SandboxException {
		return getValueFromWindowList((Object[]) items, " no title");
	}

	public Object[] getValueFromWindowList(String[] items, String title) throws SandboxException {
		return getValueFromWindowList((Object[]) items, title);
	}

	public Object[] getValueFromWindowList(Object[] items) throws SandboxException {
		return getValueFromWindowList(items, "no title");
	}

	public Object[] getValueFromWindowList(List<String> items, String title) throws SandboxException {
		return getValueFromWindowList(items.toArray(), title);
	}

	public Object[] getValueFromWindowList(Object[] items, String title) throws SandboxException {
		SandboxList list = new SandboxList(items);
		try {
			SandboxListFrame slf = new SandboxListFrame(list, this, parameters, title);
//			slf.setAlwaysOnTop(true);
//			slf.setSavedAOT(true);
			parameters.addToGlobalMap(slf);
			if (this.getReturnedFromList() != null) {
				printIt("Value(s) returned from list: ");
				for (Object string : this.getReturnedFromList()) {
					printIt(string.toString() + " :: ");
				}
				printLine();
				printLine(SandboxParameters.VISUAL_SEPARATOR);
			}
			if (this.getReturnedFromList().length == 0) {
				throw new SandboxException("Aborted because no selection made");
			}
			try {
				while(slf.isVisible()){
					Thread.sleep(100);
				}
			} catch(InterruptedException ie){
				printLine("Wait for input was interrupted.");
			} finally {
				resetStartTime();
				parameters.removeFromGlobalMap(slf);
			}

			return this.getReturnedFromList();
		} catch (HeadlessException he) { // Running without GUI
			Object[] headlessList = { items[0] };
			return headlessList;
		}

	}

	public String createGlobalMapKey(Class classToCreate, boolean replace) {
		return parameters.createGlobalMapKey(classToCreate, replace);
	}

	public Object removeFromGlobalMap(Object mapId) {
		return parameters.removeFromGlobalMap(mapId);
	}

	/**
	 * Assign a different key to the current object for later processing. Add
	 * the new object with the old object's key.
	 * 
	 * @ param obj @ return
	 */
	public String replaceInGlobalMap(Object obj) {
		String key = obj.getClass().getName();
		return replaceInGlobalMap(key, obj);
	}

	public String replaceInGlobalMap(String key, Object obj) {
		return parameters.replaceInGlobalMap(key, obj, this);
	}

	/**
	 * Add a new object with a generated key to the map.
	 * 
	 * @ param obj @ return
	 */
	public String addToGlobalMap(Object obj) {
		return parameters.addToGlobalMap(obj, this);
	}

	public String addToGlobalMap(String key, Object obj) {
		return parameters.addToGlobalMap(key, obj, this);
	}

	public String createGlobalObject(Class classToCreate) {
		return parameters.createGlobalObject(classToCreate, this);
	}

	public SandboxConnection getGlobalMapConnection(Object key) {
		return parameters.getGlobals().getMap().get(key);
	}

	public Object getGlobalMapObject(Object key) {
		SandboxConnection connection = getGlobalMapConnection(key);
		if (connection == null) {
			return null;
		} else {
			if (connection.getOwner() != null) {
				parameters.printLine("Global map object " + key + " is in use by " + connection.getOwner());
				return null;
			}
			return connection.getObj();
		}
	}

	public <T> T getGlobalMapThing(Class theClass) throws InstantiationException, IllegalAccessException {
		return (T) this.getGlobalMapThing(theClass.getName(), theClass);
	}

	public <T> T getGlobalMapThing(Object key, Class theClass) throws InstantiationException, IllegalAccessException {
		return (T) this.getGlobalMapObject(key);
	}

	// Get parameter values as desired
	public String getString(String name) {
		return checkNull((String) parameterValues.get(name));
	}

	public int getInteger(String name) {
		return Integer.parseInt((String) parameterValues.get(name));
	}

	public int[] getIntegerArray(String name) {
		String value = (String) parameterValues.get(name);
		if (value == null)
			return null;
		String[] lines = ((String) parameterValues.get(name)).split("\n");
		for (int index = 0; index < lines.length; index++) {
			lines[index] = lines[index].trim();
		}
		if (lines.length == 1) {
			lines = ((String) parameterValues.get(name)).trim().split(",");
		}
		int[] retValue = new int[lines.length];
		for (int i = 0; i < retValue.length; i++) {
			retValue[i] = Integer.parseInt(lines[i]);
		}
		return retValue;
	}

	public List<String> getStringList(String name) {
		String[] array = getStringArray(name);
		List<String> list = new ArrayList<String>(array.length);
		for (String string : array) {
			list.add(checkNull(string));
		}
		return list;
	}

	public Set<String> getStringSet(String name) {
		String[] array = getStringArray(name);
		Set<String> set = new HashSet<String>(array.length);
		for (String string : array) {
			set.add(checkNull(string));
		}
		return set;
	}

	public String[] getStringArrayNoDelim(String name) {
		String value = (String) parameterValues.get(name);
		if (value == null)
			return null;
		value = parameters.replaceValues(value);
		String[] lines = value.split("\n");
		return lines;
	}

	public String[] getStringArray(String name) {
		return getStringArray(name, ",");
	}
	
	public String[] getStringArray(String name, String del) {
		String value = (String) parameterValues.get(name);
		if (value == null)
			return null;
		String[] lines = ((String) parameterValues.get(name)).split("\n");
		for (int index = 0; index < lines.length; index++) {
			lines[index] = checkNull(lines[index].trim());
		}
		if (lines.length == 1) {
			if (lines[0].equals("")) {
				return new String[0];
			}
			if (lines[0].equals(del)) {
				return lines;
			}
			return ((String) parameterValues.get(name)).trim().split(del);
		}
		return lines;
	}

	public float getFloat(String name) {
		try {
			return Float.parseFloat((String) parameterValues.get(name));
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public long getLong(String name) {
		try {
			return Long.parseLong((String) parameterValues.get(name));
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public boolean getBoolean(String name) {
		return Boolean.parseBoolean((String) parameterValues.get(name));
	}

	public char getChar(String name) {
		String strChar = (String) parameterValues.get(name);
		return strChar.charAt(0);
	}

	public Date getDate(String date) {
		if (date.equals(SandboxParameters.SANDBOX_NULL)) {
			return null;
		}
		return Utilities.parseDate(parameterValues.get(date), SandboxParameters.DATE_FORMAT);
	}

	public File getFileDir(String fileDir) throws IOException {
		if (fileDir.equals(SandboxParameters.SANDBOX_NULL)) {
			return null;
		}
		return new File(parameterValues.get(fileDir));
	}

	public File getDirectory(String path) throws Exception {
		File dir = new File(path);
		if (dir.exists() && dir.isDirectory()) {
			return dir;
		}
		throw new SandboxException("Directory does not exist");
	}
	
	public String getStuff(String name) throws Exception {
		String strOrFile = parameterValues.get(name);
		if (strOrFile.split("\n").length == 1) {
			File dir = new File(strOrFile);
			if (dir.exists() && !dir.isDirectory()) {
				return Utilities.getFileString(strOrFile);
			}
		}
		return strOrFile;
	}

	private <T> T checkNull(T t) {
		String value = null;
		try {
			value = (String) t;
		} catch (ClassCastException cce) {
			return t;
		}
		if (value == null || value.equals(SandboxParameters.SANDBOX_NULL)) {
			return null;
		}
		return t;
	}

	protected void initParameter(int componentNumber, String toolTipText) {
		JComponent component = (JComponent) parameters.getParameterComponentAt(componentNumber);
		component.setBackground(Color.CYAN);
		initParameter(component, toolTipText);
	}

	protected void initParameter(JComponent component, String toolTipText) {
		if (!parameters.isInitialized) {
			if (component.getToolTipText() == null)
				component.setToolTipText(this.getClass().getSimpleName() + ": " + toolTipText);
			else
				component.setToolTipText(component.getToolTipText() + " & " + this.getClass().getSimpleName() + ": " + toolTipText);
		}

	}

	// public boolean printLine(Object obj, List<String> exclusions) {
	// return printLine(String.valueOf(obj ) , exclusions, true);
	// }

	public void printLine(Object obj) {
		printLine(String.valueOf(obj), true);
	}

	public void printLineControlled(Object obj, boolean print) {
		if (print) {
			printLine(obj);
		}
	}

	public void printLine(byte[] bytes) {
		printLine(new String(bytes));
	}

	public void printLine(int[] integers) {
		for(int i : integers) {
			printIt(i + " ");
		}
		printLine();
	}

	protected void printLine(String[] strings) {
		if (strings != null) {
			for (int i = 0; i < strings.length; i++) {
				printLine(strings[i]);
			}
		} else {
			printLine("null");
		}
	}

	public void printLine() {
		printLine("");
	}

	public void printALine() {
		printLine(SandboxParameters.VISUAL_SEPARATOR);
	}
	public void printALineControlled(boolean show) {
		if (show) {
			printALine();
		}
	}

	protected void printLine(Object o, boolean stdOut) {
		String string = String.valueOf(o);

		if (string != null) {
			if (stdOut)
				System.out.println(string);
			try {
				this.textArea.append(string + "\n");
				Document doc = this.textArea.getDocument();
				this.textArea.setCaretPosition(doc.getLength());
			} catch (NullPointerException npe) {
				System.out.println("Not printing to GUI");
			}
		}
	}

	public void printLine(List<String> list) {
		for (String str : list) {
			printLine(str);
		}
	}

	public void report(Object obj) {
		report(obj, new Date());
	}

	public void report(Object obj, Date date) {
		printLine((new SimpleDateFormat(SandboxParameters.DATE_FORMAT)).format(date) + " : " + obj);
	}

	public void log(String s) {
		parameters.printLine(s, false, true);
	}
//	void pt(int i) {
//		this.printLine(i);
//	}
//
//	void pt(String s) {
//		this.printLine(s);
//	}

	void printLine(int i) {
		this.printLine(String.valueOf(i), true);
	}

	void printLine(double d) {
		this.printLine(String.valueOf(d), true);
	}

	void printLine(boolean b) {
		this.printLine(String.valueOf(b), true);
	}

	public void printIt(Object o) {
		String s = String.valueOf(o);
		this.textArea.append(s);
		System.out.print(s);
	}
	public void printItControlled(Object o, boolean print) {
		if (print) {
			printIt(o);
		}
	}

	/**
	 * Prints to the buffered area without updating the gui @ param o
	 */
	void printString(Object o) {
		String s = String.valueOf(o);
		this.textArea.append(s + "\n ");
	}

	/**
	 * Prints to standard out what was buffered by the { @ link
	 * SandboxTextBase.printstring(Object o) }
	 */
	void printTextArea() {
		System.out.println(this.textArea.getText());
	}

	protected void printLine(Object o, Throwable ex) {
		ex.printStackTrace();
		String s = String.valueOf(o);
		if (s == null) {
			s = new String();
		}
		s = s + "\n\t" + ex.getClass().getName();
		for (StackTraceElement element : ex.getStackTrace()) {
			s = s + "\n\t" + element.toString();
		}
		printLine(s);
	}

	/**
	 * Method for option to print only to standard out to avoid out-of-memory
	 * problems.
	 */

	public void printLine(boolean notGui, Object o) {
		String s = String.valueOf(o);
		if (s == null) {
			s = new String();
		}
		if (notGui) {
			System.out.println(s);
		} else {
			printLine(s);
		}
	}

	/**
	 * Prints to the output tab @ param o
	 */
	void outputString(Object o) {
		parameters.printLine(o);
	}

	public JTable tabulate(String[][] columnRows, String[] headers) {
		return tabulate(columnRows, headers, "Records: " + String.valueOf(columnRows.length));
	}

	public JTable tabulate(String[][] columnRows, String[] headers, String tooltip) {
		return tabulate(columnRows, headers, tooltip, true);
	}

	public JTable tabulate(String[][] columnRows, String[] headers, String tooltip, boolean sort)
			throws ArrayIndexOutOfBoundsException {

		SandboxTable table = Utilities.tabulate(parameters, columnRows, headers, tooltip, sort);
		table.setDirty(true);
		JScrollPane scrollPaneTable = new JScrollPane(table);
		scrollPaneTable.setName(this.getTestName() + "-R");
		parameters.tabbedPane.add(scrollPaneTable);
		parameters.tabbedPane.setToolTipTextAt(parameters.tabbedPane.getTabCount() - 1, tooltip);
		return table;
	}

	/* HELPERS */

	public String noNulls(String string) {
		return Utilities.noNulls(string);
	}

	public Object noNulls(Object obj) {
		return Utilities.noNulls(obj);
	}

	public File noNulls(File file) {
		return Utilities.noNulls(file);
	}

	public int noNulls(int integer) {
		return Utilities.noNulls(integer);
	}

	public boolean noNulls(boolean b) {
		return Utilities.noNulls(b);
	}

	public long noNulls(long l) {
		return Utilities.noNulls(1);
	}

	public static String[] getTestDescriptions(SandboxParameters params) {
		if (params.tests == null) {
			params.tests = Utilities.getTests(params);
		}
		String[] testDescriptions = new String[params.tests.size()];
		Iterator<SandboxTestBase> iterTests = params.tests.iterator();
		int index = 0;
		while (iterTests.hasNext()) {
			SandboxTestBase test = (SandboxTestBase) iterTests.next();
			testDescriptions[index++] = test.getTestName() + ": " + test.getNumParams() + " Parameters. "
					+ test.getDescription();
		}
		return testDescriptions;
	}

	/**
	 * Start of process methods
	 * @param commandLine
	 * @return
	 * @throws SandboxException
	 */
	public long runInSeparateProcess(String commandLine) throws SandboxException {
		return runInSeparateProcess(commandLine, new File(System.getProperty("user.dir"), "."), true, true);
	}

	public long runInSeparateProcess(String commandLine, boolean show, boolean showErrOutput) throws SandboxException {
		return runInSeparateProcess(commandLine, new File(System.getProperty("user.dir"), "."), show, showErrOutput);
	}

	public long runInSeparateProcess(String commandLine, File workingDir) throws SandboxException {
		return runInSeparateProcess(commandLine, workingDir, true, true);
	}

	public long runInSeparateProcess(String commandLine, File workingDir, boolean show, boolean showErrOutput)
			throws SandboxException {
		testLogger.info("commandLine = " + commandLine);
		testLogger.info("workingDir = " + workingDir.getPath());
		testLogger.info("show = " + show);
		testLogger.info("showErrOutput = " + showErrOutput);

		long delay = -1;
		List<NotifyAdvice> notifyRegexes = new CopyOnWriteArrayList<NotifyAdvice>();
		notifyRegexes.addAll(parameters.getListNotifyAdvice());
		
		try {

			// prep for exclusions
			StringBuffer exclusions = new StringBuffer();
			List<Pattern> exclusionRegexes = new ArrayList<Pattern>();
			Properties systemProps = System.getProperties();
			
			// check for confirm
			for (Object key : systemProps.keySet()) {
				if (((String) key).startsWith(SandboxTestBase.SANDBOX_CONFIRM_COMMAND_PREFIX)) {
					String regex = URLDecoder.decode((String) systemProps.getProperty((String) key), "US-ASCII");
					if (Utilities.groupMatch(regex, new StringBuffer(commandLine))) {
						int decision = showConfirmMessage("Are you sure of the command?\t"
								+ key + "\n" + parameters.VISUAL_SEPARATOR + "\n" + commandLine
								+ "\n" + workingDir.getPath() + "\n" +  parameters.VISUAL_SEPARATOR);
						
						if (decision != 1) {
							String message = "YOU CANCELLED: You didn't like command '" + commandLine + "'\n" + key;
							printLine(message);
							printALine();
							throw new SandboxException(message);
						} else {
							String message = "YOU APPROVED: Consider resetting the confirm:\n" + key;
							printLine(message);
							printALine();
						}
					}
				}
				if (((String) key).startsWith(SandboxTestBase.SANDBOX_CONFIRM_DIRECTORY_PREFIX)) {
					String regex = URLDecoder.decode((String) systemProps.getProperty((String) key), "US-ASCII");
					if (Utilities.groupMatch(regex, new StringBuffer(workingDir.getPath()))) {
						int decision = showConfirmMessage("Are you sure of the workspace?\t" + key 
								+ "\n" + parameters.VISUAL_SEPARATOR + "\n" + commandLine  + "\n"
								+ workingDir.getPath() + "\n" + parameters.VISUAL_SEPARATOR);
						if (decision != 1) {
							String message = "YOU CANCELLED: You didn't like path '" + workingDir.getPath() + "'\n" + key;
							printLine(message);
							printALine();
							throw new SandboxException(message);
						} else {
							String message = "YOU APPROVED: Consider resetting the confirm:\n" + key;
							printLine(message);
							printALine();
						}
					} else if (Utilities.groupMatch(regex, new StringBuffer(commandLine))) {
						int decision = showConfirmMessage("Are you sure of the workspace?\t" + key 
								+ "\n" + parameters.VISUAL_SEPARATOR + "\n" + commandLine  + "\n"
								+ workingDir.getPath() + "\n" + parameters.VISUAL_SEPARATOR );
						if (decision != 1) {
							String message = "YOU CANCELLED: You didn't like path '" + workingDir.getPath() + "'\n" + key;
							printLine(message);
							printALine();
							throw new SandboxException(message);
						} else {
							String message = "YOU APPROVED: Consider resetting the confirm:\n" + key;
							printLine(message);
							printALine();
						}
					} 
				}
			}
			
			for (Object key : systemProps.keySet()) {
				if (((String) key).startsWith(SandboxTestBase.SANDBOX_EXCLUDE_PREFIX)) {
					// prepping before the while loop
					exclusionRegexes.add(Pattern.compile((String) systemProps.getProperty((String) key)));
				}
			}

			String commandLineEnvName = null;
			String commandLineRegex = null;
			String encloser = "";
			try { // See if a tab-delimited process parameter is being passed
				if (commandLine.matches("^.*\t.*$")) {

					String[] commandLineMatchParts = commandLine.split("\t+");
					int index = 0;
					commandLine = commandLineMatchParts[index++];

					if (!commandLineMatchParts[1].startsWith("sbx.test.setting")) {
						commandLineEnvName = commandLineMatchParts[index++];
						commandLineRegex = commandLineMatchParts[index++];
					}
					while (index < commandLineMatchParts.length) {
						if (commandLineMatchParts[index].startsWith("sbx.test.setting")) { //TODO Make these strings static String values somewhere
							printLine(commandLineMatchParts[index]);
							if (commandLineMatchParts[index].equals("sbx.test.setting.announce.true")) {
								this.setAnnounce(true);
							} else if (commandLineMatchParts[index].equals("sbx.test.setting.announce.false")){
								this.setAnnounce(false);
							}

							if (commandLineMatchParts[index].startsWith("sbx.test.setting.encloser.")) {
								String encodedEncloser = commandLineMatchParts[index].substring("sbx.test.setting.encloser.".length());
								encloser = URLDecoder.decode(encodedEncloser,"US-ASCII");
							}
							if (commandLineMatchParts[index].startsWith("sbx.test.setting.notify_")) {
								String notifyRegex = commandLineMatchParts[index].substring("sbx.test.setting.notify_".length());
								notifyRegexes.add(new NotifyAdvice(".*",".*",notifyRegex,".*", "No advice", NotifyAdvice.CHECK_OUT | NotifyAdvice.CHECK_ERR));
							}

							if (commandLineMatchParts[index].startsWith(SANDBOX_TEST_SETTING_NOTIFY_PROFILE_PREFIX)) {
								String profileFilePath = commandLineMatchParts[index].substring(SANDBOX_TEST_SETTING_NOTIFY_PROFILE_PREFIX.length());
								
								
//*/////////////////////DMH
								File profileFile = new File(profileFilePath);
								if (profileFile.exists() && profileFile.isFile()) {
									notifyRegexes.addAll(NotifyAdvice.getAdvice(profileFile, parameters));
								}
/*////////////////////////
								BufferedReader bufferedReader = null;
								try{
									FileReader fileReader = new FileReader(profileFilePath);
									bufferedReader = new BufferedReader(fileReader);
									boolean done = false;

									while(!done) {
										int check = Integer.parseInt(parameters.replaceValues(bufferedReader.readLine())); //default should be to check ERR
										Pattern commandRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
										Pattern directoryRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
										Pattern outRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
										Pattern errRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
										StringBuffer advice = new StringBuffer();

										String string = "";
										while((string = bufferedReader.readLine())!= null){
											if (string.startsWith(SandboxParameters.VISUAL_SEPARATOR)) {
												break;
											} else {
												advice.append(string).append(System.getProperty("line.separator", "\n"));
											}
										}
										notifyRegexes.add(new NotifyAdvice(commandRegex, directoryRegex, outRegex, errRegex, advice.toString(), check));
										if (string.equals(SandboxParameters.VISUAL_SEPARATOR + SandboxParameters.VISUAL_SEPARATOR)) {
											done = true;
										}
									}
									
								} catch(Throwable th) {
									testLogger.error(th);
									th.printStackTrace();
								} finally {
									bufferedReader.close();
								}
//*/////////////////////////								
								
								
								
								
							}
							
							index++;
						}
					}
					

				}

			} catch (IndexOutOfBoundsException ioobe) {
				String message = "Failed a command line process output capture";
				testLogger.info(message);
				parameters.printLine(message, true, true);
			}

			//Remove all non-checks (check == 0)
			for (NotifyAdvice advice : notifyRegexes) {
				if (advice.getCheck() == 0) {
					notifyRegexes.remove(advice);
				}
			}
			

			//Check advice for the command line with directory
			
//*///////////////////////////////////////////////////DMH
			int checkCommand = NotifyAdvice.CHECK_CMD;
			int checkDir = NotifyAdvice.CHECK_DIR;
//			int checkCmdDir = NotifyAdvice.CHECK_CMD | NotifyAdvice.CHECK_DIR; 
			for (NotifyAdvice advice : notifyRegexes) {
				advice.setScore(0);
				//Check advice for the command line
				if ((advice.getCheck() & NotifyAdvice.CHECK_CMD) > 0) { // advice wants to check command
					if (commandLine.matches(advice.getCommandRegex().pattern())) {
						advice.setScore(advice.getScore() | NotifyAdvice.CHECK_CMD);
						if (advice.getCheck() == checkCommand) {
							newTextTab(advice.toString(), "CL", parameters.iconStopped).append(commandLine);
							parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
							notifyRegexes.remove(advice);
							continue;
						}
					}
				}
				//Check advice for the working directory
				if ((advice.getCheck() & NotifyAdvice.CHECK_DIR) > 0) { // advice wants to check directory
					if (workingDir.getPath().matches(advice.getDirectoryRegex().pattern())) {
						advice.setScore(advice.getScore() | NotifyAdvice.CHECK_DIR);
						if (advice.getCheck() == checkDir) {
							newTextTab(advice.toString(), "WD", parameters.iconStopped).append(workingDir.getPath());
							this.playSound();

//							parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
							notifyRegexes.remove(advice);
							continue;
						}
					}
				}
				//Check advice for the command + working directory
				if (advice.getScore() == advice.getCheck()) {
					newTextTab(advice.toString(), "CL+WD", parameters.iconStopped);
//					parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
					notifyRegexes.remove(advice);
					continue;
				}
			}
/*//////////////////////////////////////////////////////
			//Check advice for the command line
			int checkOutErr = NotifyAdvice.CHECK_OUT & NotifyAdvice.CHECK_ERR;
			for (NotifyAdvice advice : notifyRegexes) {
				if ((advice.getCheck() & NotifyAdvice.CHECK_CMD) > 0) {
					if (commandLine.matches(advice.getCommandRegex().pattern())
							&& (advice.getCheck() & checkOutErr) == 0) {
						this.newTab(advice.getAdvice(), "CL");
						parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
						notifyRegexes.remove(advice);
					} else {
						notifyRegexes.remove(advice); //Failed test, so eliminate for further consideration
					}
				}
			}
			
			//Check advice for the working directory
			for (NotifyAdvice advice : notifyRegexes) {
				if ((advice.getCheck() & NotifyAdvice.CHECK_DIR) > 0) {
					if (workingDir.getPath().matches(advice.getDirectoryRegex().pattern())
							&& (advice.getCheck() & checkOutErr) == 0) {
						this.newTab(advice.getAdvice(), "WD");
						parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
						notifyRegexes.remove(advice);
					} else {
						notifyRegexes.remove(advice); //Failed test, so eliminate for further consideration
					}
				}
			}
//*//////////////////////////////////////////////////////
			
			Runtime runtime = Runtime.getRuntime();
			commandLine = parameters.replaceValues(commandLine);

			boolean unixShell = !Utilities.isWindows();
			if (unixShell) {
				commandLine = commandLine.replace('"', '\'');
				commandLine = "sh -c \"" + commandLine + "\"";
			}

			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine(commandLine);

			List<String> commandParts = new ArrayList<String>();
			Matcher matcher = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(commandLine);
			while (matcher.find()) {
				//Replace to remove surrounding quotes
				commandParts.add(matcher.group(1).replace("\"", ""));
			}
			Date start = new Date();
			parameters.printLine("Command parts: ", true, true);
			parameters.printLine(commandParts, true, true);
			

			List<String> commands = new ArrayList<String>();
			if (Utilities.isWindows()) {
				commands.add("cmd /c dir");
			} else {
				commands.add("source ~/.bashrc");
			}
			
			//TODO Use a system of OS environment settings for these one-use processes
//*//////////////////////////////		
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.directory(workingDir);
			Map<String,String> environmentMap = processBuilder.environment();
			environmentMap.put("SANDBOX_TEST_VALUE", this.getTime());
			
			Set<Entry<String,String>> entrySet = environmentMap.entrySet();
		
			Set<String> environmentSet = new HashSet<String>();
			for (Entry<String,String> entry : entrySet) {
				environmentSet.add(entry.getKey() + "=" + entry.getValue());
			}
//			String[] stringArray = new String[environmentSet.size()];
//			String[] environment = environmentSet.toArray(stringArray);

			String[] environment = new String[environmentSet.size()];
			environmentSet.toArray(environment);

//			String[] environment = {"SANDBOX_TEST_VALUE=" + this.getTime()};
//			for (String envName : environmentMap.keySet()) {
//				
//			}
			
			process = runtime.exec(
					commandParts.toArray(new String[commandParts.size()]), 
					environment, 
					workingDir
					);
/*//////////////////////////////			
			process = runtime.exec(commandParts.toArray(new String[commandParts.size()]), null, workingDir);
//*//////////////////////////////			
		
			
			this.addConnection(this.process);

			InputStream inputStream = process.getInputStream();
			InputStreamReader inputstreamReader = new InputStreamReader(inputStream);
			BufferedReader bufferedReader = new BufferedReader(inputstreamReader);

			InputStream errorStream = process.getErrorStream();
			InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
			BufferedReader errorBufferedReader = new BufferedReader(errorStreamReader);

			// String line;
			// StringBuffer excluded = new StringBuffer();
			List<String> foundEnvValues = new ArrayList<String>();

			Pattern pattern = null;
			if (commandLineRegex != null) {
				pattern = Pattern.compile(commandLineRegex); // prepping before
																// the while
																// loop
			}

			StringBuffer postProcessOut = new StringBuffer();

			printFromBuffer("OUT", bufferedReader, postProcessOut, exclusions, pattern, commandLineEnvName,
					foundEnvValues, exclusionRegexes, notifyRegexes, commandLineRegex, encloser, show, NotifyAdvice.CHECK_OUT);

			if (isRunning) {
				if (foundEnvValues.size() > 0) {

					StringBuffer environmentValuesString = new StringBuffer();
					printLine("Setting environment variable " + commandLineEnvName);
					for (String value : foundEnvValues) {
						printLine(value);
						environmentValuesString.append(value).append(" ");
					}
					String enviromentValue = environmentValuesString.substring(0, environmentValuesString.length() - 1);
					System.setProperty(commandLineEnvName, enviromentValue);
				}

				StringBuffer postProcessErr = new StringBuffer();
				printFromBuffer("ERR", errorBufferedReader, postProcessErr, exclusions, pattern, commandLineEnvName,
						foundEnvValues, exclusionRegexes, notifyRegexes, commandLineRegex, encloser, showErrOutput, NotifyAdvice.CHECK_ERR);
				
				if (postProcessErr.length() > 0) {
					addToGlobalMap(getTestName() + "ERR" + this.startTime, postProcessErr.toString());
					postProcessOut.append('\n').append("ERROUT:").append('\n').append(postProcessErr);
				}

				for (String key : System.getProperties().stringPropertyNames()) {
					if (key.startsWith(SANDBOX_POST_PROCESS_ALERT_ENV)) {
						String ppRegex = System.getProperty(key);
						String[] grabs = Utilities.grabRegex(ppRegex, postProcessOut);

						parameters.printLine("START GRAB", true, true);
						parameters.printLine("ppRegex: " + ppRegex, true, true);
						parameters.printLine("postProcessOut: " + postProcessOut, true, true);
						if (grabs != null && grabs.length > 0) {

							List<String> arrayGrabs = new ArrayList<String>();
							for (String grab : grabs) {
								arrayGrabs.add(grab);
								parameters.printLine("GRAB: " + grab, true, true);
							}
							arrayGrabs.remove(0);
							for (String grab : arrayGrabs) {
								printLine(SandboxParameters.VISUAL_SEPARATOR);
								printLine("HIGHLIGHT: " + ppRegex);
								printLine();
								printLine(grab);
//								printLine(SandboxParameters.VISUAL_SEPARATOR);
								printLine();
							}
						}
					}
				}
				
			}
			
			
			Date end = new Date();
			delay = end.getTime() - start.getTime();
			if (exclusions.length() > 0) {
				this.addToGlobalMap("EXCLUSIONv" + end.getTime(), exclusions);
			}
			// this.addToGlobalMap (test.testName + " Out" + end.getTime(),
			// postProcessOut);
		} catch (IOException en) {
			en.printStackTrace();
			if (show)
				printLine(en.getClass().getSimpleName() + ": " + en.getMessage());
		} finally {
			testLogger.info("Process done");
		}
		try {
			printLine("Exit value = " + process.waitFor());
		} catch (InterruptedException e) {
			throw new SandboxException(e);
		}

		if (process.exitValue() < 0) {
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine("***ABNORMAL PROCESS TERMINATION AT: ***");
			printLine("Command: " + commandLine);
			printLine("Workspace: " + workingDir);
//			printLine(SandboxParameters.VISUAL_SEPARATOR);
			return -2;
		}

		return delay;
	}

	public long runInSeparateJavaProcess(String javaCommandLine, SandboxTestBase test)
			throws IOException, SandboxException {
		File workingDir = new File(parameters.configurationDirectory);
		test.printLine("Working directory: " + workingDir.getAbsolutePath());
		return runInSeparateJavaProcess(javaCommandLine, workingDir, test);
	}

	public long runInSeparateJavaProcess(String javaCommandLine, File workingDir, SandboxTestBase test)
			throws IOException, SandboxException {

		Properties globalProps = new Properties();
		String globalFilePath = parameters.configurationDirectoryRoot + File.separator + "environment.xml";
		File globalEnvironmentFile = new File(globalFilePath);
		try {
			if (globalEnvironmentFile.exists()) {
				FileInputStream environmentFIS = new FileInputStream(globalEnvironmentFile);
				globalProps.loadFromXML(environmentFIS);
				testLogger.info("Loaded global properties = " + globalFilePath);
			}

		} catch (Exception en) {
			testLogger.info("No global properties loaded");
		}

		java.util.Properties props = System.getProperties();
		StringBuffer javaSettings = new StringBuffer();
		Enumeration<?> propNames = props.propertyNames();
		Set<String> locals = new HashSet<String>();
		while (propNames.hasMoreElements()) {
			String propName = (String) propNames.nextElement();
			if (propName.startsWith("sandbox.java.")) {
				javaSettings.append("-D");
				String name = propName.substring("sandbox.java.".length());
				locals.add(name);

				javaSettings.append(name);
				javaSettings.append("=");
				javaSettings.append(props.getProperty(propName));
				javaSettings.append(" ");
			} // quote = " \" ";

			if (propName.equals("sandbox.home")) {
				javaSettings.append("-D");
				String name = "sandbox.home";
				locals.add(name);
				javaSettings.append(name);

				javaSettings.append("=");
				javaSettings.append(props.getProperty(propName));
				javaSettings.append(" ");
			} // quote = " \" ";
		}

		Enumeration<?> globalPropNames = globalProps.propertyNames();
		while (globalPropNames.hasMoreElements()) {
			String propName = (String) globalPropNames.nextElement();
			if (!propName.startsWith(SandboxTestBase.SANDBOX_CONFIRM_COMMAND_PREFIX)) {
				// Prevent confirming when being set as Java environment property
				if (locals.contains(propName)) {
					javaSettings.append("-D");
					javaSettings.append(propName);
					javaSettings.append("=");
					javaSettings.append(globalProps.getProperty(propName));
					javaSettings.append(" ");
				}
			}
		}

		javaSettings.append("-Dsandbox.home=" + parameters.sandboxHome).append(" ");
		javaSettings.append("-Duser.home=" + System.getProperty("user.home")).append(" ");

		String classPath = System.getProperty("java.class.path");
		String commandLine = "java -cp " + classPath + " " + javaSettings + javaCommandLine;
		return runInSeparateProcess(commandLine, workingDir, true, true);
	}

	public void webpage(String pageString) {
		newTab(pageString);

		JEditorPane editorPaneHtml = new JEditorPane();
		editorPaneHtml.setContentType("text/html");
		editorPaneHtml.setText(pageString);
		JScrollPane scrollPaneHtml = new JScrollPane(editorPaneHtml);

		scrollPaneHtml.setName(this.getTestName() + "-html");
		parameters.tabbedPane.add(scrollPaneHtml, parameters.tabbedPane.getTabCount());
	}

	private void printFromBuffer(String title, BufferedReader bufferedReader, StringBuffer postProcessOut,
			StringBuffer exclusions, Pattern pattern, String commandLineEnvName, List<String> foundEnvValues,
			List<Pattern> exclusionRegexes, List<NotifyAdvice> notifyRegexes, String commandLineRegex,
			String encloser, boolean show, int check) throws IOException {

		if (show) {
			printLine(SandboxParameters.VISUAL_SEPARATOR);
			printLine(title);
			printLine();
		}
		
		
		
/*//////////////////////////////////DMH		
		StringBuffer notifyPatternProcessOut = new StringBuffer();
		NotifyMatchRunner notifyMatchRunner = null;
		if (notifyRegexes.size() > 0 && (check & NotifyAdvice.CHECK_OUT) > 0 
				&& this.textArea.getLineCount() > this.lastLineCount) {
			
			System.err.println("this.textArea.getLineCount() = " + this.textArea.getLineCount());
			System.err.println("this.lastLineCount = " + this.lastLineCount);
			
			this.lastLineCount = this.textArea.getLineCount();
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			notifyMatchRunner = new NotifyMatchRunner(check, this, title, notifyRegexes, notifyPatternProcessOut);
			notifyPatternScheduleHandle = executor.scheduleAtFixedRate(notifyMatchRunner, 1, 3, TimeUnit.SECONDS);
		}
/*//////////////////////////////////////
		StringBuffer notifyPatternProcessOut = new StringBuffer();
		NotifyMatchRunner notifyMatchRunner = null;
		if (notifyRegexes.size() > 0 && (check & NotifyAdvice.CHECK_OUT) > 0 ) {
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			notifyMatchRunner = new NotifyMatchRunner(check, this, title, notifyRegexes, notifyPatternProcessOut);
			notifyPatternScheduleHandle = executor.scheduleAtFixedRate(notifyMatchRunner, 1, 3, TimeUnit.SECONDS);
		}
//*//////////////////////////////////////
		
		
		
		String line = new String();
		while ((line = bufferedReader.readLine()) != null && isRunning) {
			postProcessOut.append(line).append('\n');
			notifyPatternProcessOut.append(line).append('\n');
			if (pattern != null) {

				try {
					Matcher matcher = pattern.matcher(line);
					if (matcher.find()) {
						String theMatch = "NO MATCH";
						if (matcher.groupCount() > 0) {
							theMatch = line.substring(matcher.start(1), matcher.end(1));
						} else {
							theMatch = line.substring(matcher.start(0), matcher.end(0)); // No
																							// groups

							parameters.printLine("You should be using groups in this regex: " + commandLineRegex, true, true);
						}
						
						if (encloser != null) {
							foundEnvValues.add(encloser + theMatch + encloser);
						} else {
							foundEnvValues.add(theMatch);
						}

						if (commandLineEnvName.startsWith(SANDBOX_ALERT_ENV)) {
							printLine(SandboxParameters.VISUAL_SEPARATOR);
							printLine(getTime() + " *** SANDBOX ALERT *** [" + theMatch + "]");
							printLine(line);
							printLine(SandboxParameters.VISUAL_SEPARATOR);
							printLine();
							if (isAnnounce()) {
								int result = this.showMessage(commandLineRegex);
								if (result == 0) {

									parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconStopped);
								} else {
									parameters.tabbedPane.setIconAt(getTabNumber(), parameters.iconRunning);
								}

								this.setAnnounce(false);

							}
						}
					}
				} catch (Throwable th) {
					parameters.printLine(th);
				}
			}

			if (show) {
				boolean include = true;
				for (Pattern exclusionPattern : exclusionRegexes) {
					Matcher matcher = exclusionPattern.matcher(line);
					if (matcher.find()) {
						include = false;
					}
				}
				if (include) {
					printLine(line);
				} else {
					exclusions.append(line).append('\n');
				}
			}

		}
		
		if (show) {
			printLine(SandboxParameters.VISUAL_SEPARATOR);
		}
		encloser = "";
		if (notifyPatternScheduleHandle != null) {
			this.notifyPatternScheduleHandle.cancel(false);
		}
		notifyMatchRunner = new NotifyMatchRunner(check, this, title, notifyRegexes, notifyPatternProcessOut);
		Thread thread = new Thread(notifyMatchRunner);
		this.addConnection(thread);
		thread.start();
	}

	public JEditorPane newTab(String pageString) {
		return newTab(pageString, "plain");
	}

	public JEditorPane newTab(String pageString, String title) {
		return newTab(pageString, title, "text/plain");
	}

	public JEditorPane newTab(String pageString, String title, String contentType) {
		return newTab(pageString, title, contentType, true);
	}

	public JEditorPane newTab(String pageString, String title, String contentType, boolean editable) {
		try {
			JEditorPane editorPanePlain = new JEditorPane();
			editorPanePlain.setContentType(contentType);
//			editorPanePlain.setFont(Font.decode(Font.MONOSPACED).deriveFont(Font.PLAIN));
//			String[] clf = System.getProperty("sbx.conf.textFont","Dialog_2_11").split("_");
			String[] clf = Utilities.getEnvFont();

			Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
			editorPanePlain.setFont(textFont); // Re23.04.10.11.55.19.248

			editorPanePlain.setText(pageString);
			JScrollPane scrollPanePlain = new JScrollPane(editorPanePlain);
			scrollPanePlain.setName(this.getTestName() + "-" + title);
			parameters.tabbedPane.add(scrollPanePlain, parameters.tabbedPane.getTabCount());
			editorPanePlain.addFocusListener(parameters.getFocusListener());
			editorPanePlain.addKeyListener(parameters.keyListener);
			editorPanePlain.setEditable(editable);
			parameters.selectTab(parameters.selectedTab);
			
//			parameters.tabbedPane.setBackgroundAt(parameters.lastSelectedTab, parameters.tabBackgroundColor);
			
			return editorPanePlain;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			parameters.printLine("Not opening new tab");

			parameters.printLine(pageString);
			return null;
		} catch (IndexOutOfBoundsException ioobe) {
			parameters.printLine("Not opening new tab (some kind of head thing)");
			parameters.printLine(pageString);
			return null;
		}
	}

	public SandboxTextArea newTextTab(String pageString, String title, Icon icon) {
		try {
			SandboxTextArea textArea = new SandboxTextArea(this);
			// textArea.setContentType(contentType);
			String[] clf = Utilities.getEnvFont();
			Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
			textArea.setFont(textFont); // Re23.04.10.11.55.19.248
			textArea.setText(pageString);
			JScrollPane scrollPanePlain = new JScrollPane(textArea);
			scrollPanePlain.setName(this.getTestName() + "-" + title);

			parameters.tabbedPane.add(scrollPanePlain, parameters.tabbedPane.getTabCount());
			if (icon != null) {
				parameters.tabbedPane.setIconAt(getTabNumber(), icon);
			}

			textArea.addFocusListener(parameters.getFocusListener());
			textArea.addKeyListener(parameters.keyListener);
			// textArea.setEditable(true);
			return textArea;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			parameters.printLine("Not opening new tab");
			parameters.printLine(pageString);
			return null;
		}

	}

	public JTextArea newTextTab(JTextArea textArea, String title) {
		try {
			String[] clf = Utilities.getEnvFont();
			Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
			textArea.setFont(textFont); // Re23.04.10.11.55.19.248
			JScrollPane scrollPanePlain = new JScrollPane(textArea);
			scrollPanePlain.setName(title);
			parameters.tabbedPane.add(scrollPanePlain, parameters.tabbedPane.getTabCount());
			return textArea;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			parameters.printLine("Not opening new tab");

			return null;
		}
	}

	protected void setTooltip(String tooltip) {
		try {
			int maxLength = Integer
					.valueOf(System.getProperty("sandbox.conf.tabTooltipMaxLength", String.valueOf(100)));
			parameters.tabbedPane.setToolTipTextAt(getTabNumber(),
					tooltip.length() > maxLength ? tooltip.substring(0, maxLength) + "..." : tooltip);
		} catch (IndexOutOfBoundsException ioobe) {
			parameters.printLine("Could not set tool tip in tab " + getTabNumber(), true, true);
		}
	}

	public static void main(String[] args) {
		System.out.println("Version " + SandboxParameters.VERSION);

		if (args.length == 0) {
			System.out.println("Usage: java " + SandboxTestBase.class.getName()
					+ " <PackageName> <TestName> [<paramNum> <paramVal>] ...");
			System.out.println(" \tFor example: java " + SandboxTestBase.class.getName()
					+ " sandbox.whatever Test7 14 \"The parameter value to set 14 to\"");
			System.out.println(
					" \tSet sandbox.commandline.urlencode.usascii to true in Java environment if value is URL encoded.");

			System.exit(0);
		}

		SandboxParameters.targetPackageName = args[0];
		SandboxParameters params = SandboxParameters.createSandboxParameters(true);

		System.out.println("****************\nARGUMENTS START[" + args.length + "]");
		for (String arg : args) {
			System.out.print("(" + arg + ") :: ");
		}
		System.out.println("\nARGUMENTS STOP\n****************");

		params.tests = Utilities.getTests(params);

		if (args.length == 1) {
			giveDirectionsAndExit(params.tests, null, args);
		}

		try {
			int testCommandLineCount = (args.length - 2) / 2;

			SandboxTestBase test = params.getTest(args[1]);
			if (testCommandLineCount < test.getNumParams()) {
				giveDirectionsAndExit(params.tests, test, args);
			}
			int focusedCount = 0;

			for (int i = 2; i <= testCommandLineCount * 2; i += 2) {
				try {

					if (args[i + 1] == null) {
						throw new SandboxException("Missing matching parameter to " + args[i]);
					}
					if (args[i].trim().equals(SandboxParameters.FOCUSED_NAME)) { // allow
																					// for
																					// focused
																					// text
						String focusedValue = params.replaceValues(args[i + 1]);
						JTextField component = new JTextField();

						component.setName("TextField " + focusedCount);
						if (System.getProperty("sandbox.commandline.urlencode.usascii", "false").equals("true")) {
							component.setText(URLDecoder.decode(focusedValue, "US-ASCII"));
						} else {
							component.setText(focusedValue);
						}
						params.parameterList.remove(focusedCount);
						params.parameterList.add(focusedCount, component);

						focusedCount++;

					} else if (args[i].trim().equals(SandboxParameters.PASSWORD_NAME)) {
						String password = params.replaceValues(args[i + 1]);
						params.passwordComponent = new JPasswordField(password);
					} else if (args[i].trim().equals(SandboxParameters.YESNO_NAME)) {
						params.yesNo.setSelected(Boolean.parseBoolean(args[i + 1]));
					} else if (args[i].trim().equals(SandboxParameters.TRUEFALSE_NAME)) {
						params.trueFalse.setSelected(Boolean.parseBoolean(args[i + 1]));
					} else {
						int parameterNumber = Integer.parseInt(args[i]);
						String value = params.replaceValues(args[i + 1]);
						if (System.getProperty("sandbox.commandline.urlencode.usascii", "false").equals("true")) {
							params.addParameterValueAt(parameterNumber, URLDecoder.decode(value, "US-ASCII"));
						} else {
							params.addParameterValueAt(parameterNumber, value);
						}
					}
				} catch (SandboxException se) {
					System.out.println(SandboxTestBase.class.getName() + ".main(): Malformed command line parameter: "
							+ se.getMessage());
					giveDirectionsAndExit(params.tests, test, args);
				} catch (Exception en) {
					System.out.println(
							SandboxTestBase.class.getName() + ".main(): Malformed command line parameter: " + args[i]);
					giveDirectionsAndExit(params.tests, test, args);
				}
			}

			params.getFocusListener().setComponent(params.parameterList.get(0));
			params.setFocusListener(params.getFocusListener());

			if (test != null) {
				System.out.println(test.getTestName() + " : " + test.getNumParams() + " Parameter"
						+ (test.getNumParams() != 1 ? "s." : ".") + test.getDescription());
				test.setParameters(params);
				test.setIsRunning(true);

				params.guiLess = true;
				params.showPopPad = new JCheckBox();
				params.showPopPad.setSelected(false);
				test.test(params);
			}
		} catch (Throwable th) {
			System.out.println(th.getClass().getName() + ".main(): " + th.getMessage());
			th.printStackTrace();
			giveDirectionsAndExit(params.tests, null, args);
		}
		System.exit(0);
	}
//*//////////////////////////////////////////////////////////////////////////////////////////////	

	private static void giveDirectionsAndExit(List<SandboxTestBase> tests, SandboxTestBase thisTest, String[] args) {
		System.out.println(SandboxParameters.VISUAL_SEPARATOR);
		System.out.println();
		if (thisTest == null) {
			for (SandboxTestBase test : tests) {
				System.out.println(
						test.getTestName() + " : " + test.getNumParams() + " Parameters." + test.getDescription());
			}
		} else {
			System.out.println(thisTest.getTestName() + " : " + thisTest.getNumParams() + " Parameters."
					+ thisTest.getDescription());
		}
		System.out.println(" Usage: java " + SandboxTestBase.class.getName()
				+ " <TestPackage> <TestName> [ <paramNum> <paramValXRB>...");
		System.out.println(" \tFor example: java " + SandboxTestBase.class.getName()
				+ " sandbox.test Test7 1 4 The+US-ASCII+encoded+parameter+value+to+set+1 4+to");
		System.out.print(" Invalid parameters: ");
		for (String arg : args) {
			System.out.print(arg + " ");
		}
		System.exit(0);
	}

	public void close() {
		this.closeConnections();
	}

	public void closeConnections() {
		int connectionsClosed = 0;
		if (connections == null) {
			printLine("No connections to close");
		}

		try { // Close connections if any

			Iterator<SandboxConnection> iterConnections = connections.iterator();
			while (iterConnections.hasNext()) {
				SandboxConnection connection = iterConnections.next();
				parameters.printLine(connection.close(), true, true);
				connectionsClosed++;
			}
		} catch (Exception en) {
			printLine(en.getClass().getSimpleName() + " : Connections/Readers closing interrupted.");
			en.printStackTrace();
		}
		printLine("Number of connections closed: " + connectionsClosed);
	}

	public void addConnection(Object obj) {
		this.connections.add(new SandboxConnection(obj));
	}

	public void checkRunning() throws SandboxException {
		if (isRunning) {
			return;
		} else {
			throw new SandboxException("Stopping...");
		}
	}

	/**
	 * 
	 * <b>Description:<b> Every second for a number of seconds, check if the thread is
	 * running
	 * 
	 * @ param seconds @ throws SandboxException
	 * 
	 * @ throws InterruptedException
	 */
	public void checkRunning(int seconds) throws SandboxException, InterruptedException {
		for (int index = 0; index < seconds; index++) {
			Thread.sleep(1000);
			checkRunning();
		}
	}

	public void endMe() {
		setIsRunning(false);
		setReady(false);
		if (getProcess() != null) {
			getProcess().destroy();
		} else {
			printLine("Stopping");
			closeConnections();
		}
	}
	
	protected ScheduledFuture notifyPatternScheduleHandle = null;
	protected SandboxTextArea notifyTab = null;
	
	class NotifyMatchRunner implements Runnable {
		private int type = 0;
		private SandboxTestBase test = null;
		private String title = "unknown";
		private List<NotifyAdvice> notifyMatchPatterns = null;
		private StringBuffer postProcessOut = null;
		private int start = 0;
		public NotifyMatchRunner(int type, SandboxTestBase test, String title, List<NotifyAdvice> notifyMatchPatterns, StringBuffer postProcessOut) {
			this.type = type;
			this.test = test;
			this.title = title;
			this.notifyMatchPatterns = notifyMatchPatterns;
			this.postProcessOut = postProcessOut;
		}

		@Override
		public void run() {
			System.err.println(title + " Checking for notify match");
			for (NotifyAdvice regexAdvice : notifyMatchPatterns) {
				start = 0;				
				if ((regexAdvice.getCheck() & type) == 0) {
					continue; //Was not for this type
				}
				Pattern pattern = null;
				if (type == NotifyAdvice.CHECK_OUT) {
					pattern = regexAdvice.getOutRegex();
				} else if (type == NotifyAdvice.CHECK_ERR) {
					pattern = regexAdvice.getErrRegex();
				}

				List<Integer> listEnds = Utilities.grabEndsRegex(pattern, postProcessOut, start);
				
				if (listEnds.size() > 0) {
					Integer end = 0;
					StringBuffer founds = new StringBuffer();
					
					int currentFoundEnd = listEnds.get(listEnds.size() - 1); 
					if (currentFoundEnd > test.lastFoundEnd) {
						
						System.err.println("dmh1448 currentFoundEnd: " + currentFoundEnd);
						System.err.println("test.lastFoundEnd: " + test.lastFoundEnd);
						test.lastFoundEnd = currentFoundEnd;
						while(listEnds.size() > 0) {
							Integer start = listEnds.remove(0);
							end = listEnds.remove(0);
							founds.append(postProcessOut.substring(start, end)).append("\n\n");
						}
						founds.append(SandboxParameters.VISUAL_SEPARATOR).append("\n");
						if (founds.length() > 0) {
							String description = "ADVICE v2 : " + title + " : " + pattern.pattern();
							try {
								
								if (notifyTab == null || announce) {
									int check = regexAdvice.getCheck();
									regexAdvice.setScore(type | regexAdvice.getScore());
									founds.insert(0, regexAdvice.toString());
									if (check == regexAdvice.getScore()) {
										notifyTab = newTextTab(founds.toString(), title, null);
										notifyTab.setTest(null);
	
										String tooltip = regexAdvice.getAdvice();
										
//										int maxLength = Integer
//												.valueOf(System.getProperty("sandbox.conf.tabTooltipMaxLength", String.valueOf(100)));
										parameters.tabbedPane.setToolTipTextAt(getTabNumber() + 1, tooltip);

										
//										notifyTab.setToolTipText(regexAdvice.getAdviceFile().getPath());
										
										parameters.printLine(regexAdvice.toString());
									}
									
								} else {
									notifyTab.append("\n" + description + "\n");
								}
								
							} catch(IndexOutOfBoundsException ioobe) {
								parameters.printLine("Whoops! Tab not at " + getTabNumber());
								parameters.printLine("Ending test due to abundance of caution! " + test.getTestName());
								test.endMe();
							}

							start = end;
						}
						
					}
				}
			}
		}
	}
}