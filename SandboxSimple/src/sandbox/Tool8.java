package sandbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
//import javax.swing.JOptionPane;

@SandboxAnnotation(
		description = "Put string(s) in global map", 
		parameterDescriptions = {
				SandboxParameters.FOCUSED_NAME,	"cdNamesValues",
				SandboxParameters.TRUEFALSE_NAME, "clearProperties"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool8 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] namesValues = getStringArray("cdNamesValues");
		boolean clearProperties = getBoolean("clearProperties");
		String entered = getString("cdNamesValues");
		String rawEntered = params.getFocusedText();
		
		if (namesValues.length > 20) {
			if (!confirm("Confirm that you want to set " + namesValues.length + " properties.")) {
				printLine("YOU CANCELLED");
				return;
			} else {
				printLine("*** SETTING " + namesValues.length + " VALUES ***");
			}
		}
		
		if (clearProperties) {
			if (!confirm("Confirm that you want to clear the properties")) {
				clearProperties = false;
			} else {
				printLine("*** CLEARING FROM GLOBAL MAP ***");
			}
		}
		
		boolean isMultiLine = entered.split("\n").length > 1;
		
		if (!isMultiLine) { //single line
			if (clearProperties && entered.contains(",")) {
				String systemId = entered.split(",")[0];
				this.removeFromGlobalMap(systemId);
			} else if (clearProperties && !entered.contains(",")) { //clearing
				printLine(entered + "," + System.getProperty(entered));
				this.removeFromGlobalMap(entered);
			} else {
				String[] nameValue = entered.split(",", 2);
				String name = nameValue[0];
				String value = nameValue[1];
				this.addToGlobalMap(name, value);
			}
		} else { //multi-line
			if (!namesValues[0].contains(",") && !clearProperties) { // name/value
				if (namesValues.length > 2) {
					String message = "*** INCORRECT FORMAT FOR SETTING (To clear properties, select clearProperties) ***"; 
					//JOptionPane.showMessageDialog(parameters.tabbedPane, message);
					showConfirmMessage(message);

					printLine(message);
				} else {
					this.addToGlobalMap(namesValues[0], namesValues[1]);
				}
			} else { // name,value/name,value/...
				List<String> processedNamesValues = new ArrayList<String>();
				
				
				
				rawEntered = params.replaceValues(rawEntered);
				
								
				for (String line : rawEntered.split("\n")) {
					String[] nameValue = line.split(",",2);
//					String name = replaceValuesLocal(nameValue[0]);
					
					String processedName = nameValue[0];
					String replacedName = params.replaceValues(processedName);

					if (nameValue.length == 2) {
						String processedValue = nameValue[1];
						String replacedValue = params.replaceValues(processedValue);
						processedNamesValues.add(replacedName + "," + replacedValue);
					} else if (nameValue.length == 1) {
						processedNamesValues.add(replacedName); //For clearing property
					}
				}
				
				for (String nameValue : processedNamesValues) {
					if (!nameValue.contains(",") || clearProperties) {
						printLine(nameValue + "," + this.getGlobalMapThing(nameValue, String.class));
						String name = nameValue.split(",")[0];
						this.removeFromGlobalMap(name);
					} else {
						String name = nameValue.split(",")[0];
						String value = nameValue.split(",",2)[1];
						this.addToGlobalMap(name, value);
					}
				}
			}
		}
		// END TEST
	}

}
