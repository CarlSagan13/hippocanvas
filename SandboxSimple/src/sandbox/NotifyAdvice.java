package sandbox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class NotifyAdvice implements SandboxCommon {
	public static int CHECK_CMD = 1;
	public static int CHECK_DIR = 2;
	public static int CHECK_OUT = 4;
	public static int CHECK_ERR = 8;

	private File adviceFile = null;
	private Pattern commandRegex = Pattern.compile(".*", Pattern.MULTILINE);
	private Pattern directoryRegex = Pattern.compile(".*", Pattern.MULTILINE);
	private Pattern outRegex = Pattern.compile(".*", Pattern.MULTILINE);
	private Pattern errRegex = Pattern.compile(".*", Pattern.MULTILINE);
	private String advice = "No advice";
	private int check = 0; // this says which to check: cmd/dir/out/err
	private int score = 0; // This holds what has matched: cmd/dir/out/err
	
	public NotifyAdvice() {
		super();
	}

	public NotifyAdvice(File adviceFile, SandboxParameters parameters) throws FileNotFoundException, IOException {
		super();
		this.adviceFile = adviceFile;
		BufferedReader bufferedReader = null;
		
		try{
			FileReader fileReader = new FileReader(adviceFile);
			bufferedReader = new BufferedReader(fileReader );

			check = Integer.parseInt(parameters.replaceValues(bufferedReader.readLine())); //default should be to check ERR
			commandRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
			directoryRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
			outRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
			errRegex = Pattern.compile(parameters.replaceValues(bufferedReader.readLine()), Pattern.MULTILINE);
			StringBuffer advice = new StringBuffer();

			String string = "";
			while((string = bufferedReader.readLine())!= null){
				advice.append(string).append(System.getProperty("line.separator", "\n"));
			}		
			this.advice = advice.toString();
		} catch(Throwable th) {
			th.printStackTrace();
			if (parameters != null) {
				parameters.printLine("Advice config error: " + adviceFile.getPath());
			}
		} finally {
			bufferedReader.close();
		}
	}

	public NotifyAdvice(Pattern commandRegex, Pattern directoryRegex, Pattern outRegex, Pattern errRegex,
			String advice, int check) {
		super();
		this.commandRegex = commandRegex;
		this.directoryRegex = directoryRegex;
		this.outRegex = outRegex;
		this.errRegex = errRegex;
		this.advice = advice;
		this.check = check;
	}

	public NotifyAdvice(String commandRegex, String directoryRegex, String outRegex, String errRegex,
			String advice, int check) {
		super();
		this.commandRegex = Pattern.compile(commandRegex, Pattern.MULTILINE);
		this.directoryRegex = Pattern.compile(directoryRegex, Pattern.MULTILINE);
		this.outRegex = Pattern.compile(outRegex, Pattern.MULTILINE);
		this.errRegex = Pattern.compile(errRegex, Pattern.MULTILINE);
		this.advice = advice;
		this.check = check;
	}

	public File getAdviceFile() {
		return adviceFile;
	}

	public void setAdviceFile(File adviceFile) {
		this.adviceFile = adviceFile;
	}

	public Pattern getCommandRegex() {
		return commandRegex;
	}
	public void setCommandRegex(Pattern commandRegex) {
		this.commandRegex = commandRegex;
	}
	public void setCommandRegex(String commandRegex) {
		this.commandRegex = Pattern.compile(commandRegex, Pattern.MULTILINE);
	}
	public Pattern getDirectoryRegex() {
		return directoryRegex;
	}
	public void setDirectoryRegex(Pattern directoryRegex) {
		this.directoryRegex = directoryRegex;
	}
	public void setDirectoryRegex(String directoryRegex) {
		this.directoryRegex = Pattern.compile(directoryRegex, Pattern.MULTILINE);
	}
	public Pattern getOutRegex() {
		return outRegex;
	}
	public void setOutRegex(Pattern outRegex) {
		this.outRegex = outRegex;
	}
	public void setOutRegex(String outRegex) {
		this.outRegex = Pattern.compile(outRegex, Pattern.MULTILINE);
	}
	public Pattern getErrRegex() {
		return errRegex;
	}
	public void setErrRegex(Pattern errRegex) {
		this.errRegex = errRegex;
	}
	public void setErrRegex(String errRegex) {
		this.errRegex = Pattern.compile(errRegex, Pattern.MULTILINE);
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public int getCheck() {
		return check;
	}
	public void setCheck(int check) {
		this.check = check;
	}

	protected int getScore() {
		return score;
	}

	protected void setScore(int score) {
		this.score = score;
	}

	public static  List<NotifyAdvice> getAdvice(File profileFilePath, SandboxParameters parameters) throws IOException {
		BufferedReader bufferedReader = null;
		List<NotifyAdvice> notifyRegexes = new ArrayList<NotifyAdvice>();
		try{
			FileReader fileReader = new FileReader(profileFilePath);
			bufferedReader = new BufferedReader(fileReader);
			boolean done = false;

//			 done = true; //Re23.03.12.17.23.05.269
			while(!done) {
				parameters.printLine("Re23.03.12.17.23.05.269");
				//				value = nameValue.split("[\\t|\\s]+",2)[1]; //Allowing for accidental tabs and spaces
//				int check = Integer.parseInt(bufferedReader.readLine().split("\t", 2)[0]); //default should be to check ERR
				int check = Integer.parseInt(bufferedReader.readLine().split("[\\t|\\s]+", 2)[0]); //default should be to check ERR
				Pattern commandRegex = Pattern.compile(bufferedReader.readLine().split("\t", 2)[0], Pattern.MULTILINE);
				Pattern directoryRegex = Pattern.compile(bufferedReader.readLine().split("\t", 2)[0], Pattern.MULTILINE);
				Pattern outRegex = Pattern.compile(bufferedReader.readLine().split("\t", 2)[0], Pattern.MULTILINE);
				Pattern errRegex = Pattern.compile(bufferedReader.readLine().split("\t", 2)[0], Pattern.MULTILINE);
				StringBuffer advice = new StringBuffer();

				String string = "";
				while((string = bufferedReader.readLine())!= null){
					if (string.startsWith(SandboxParameters.VISUAL_SEPARATOR)) {
						break;
					} else {
						advice.append(string).append(System.getProperty("line.separator", "\n"));
					}
				}
				notifyRegexes.add(new NotifyAdvice(commandRegex, directoryRegex, outRegex, errRegex, advice.toString(), check));
				if (string == null || string.equals(SandboxParameters.VISUAL_SEPARATOR + SandboxParameters.VISUAL_SEPARATOR)) {
					done = true;
				}
			}
		} catch(Throwable th) {
			th.printStackTrace();
			if (parameters != null) {
				parameters.printLine("Advice config error: " + profileFilePath.getPath());
				parameters.printLine(th.getMessage(), th);
			}
		} finally {
			bufferedReader.close();
		}
		return notifyRegexes;
	}
	
	@Override
	protected NotifyAdvice clone() throws CloneNotSupportedException {
		return new NotifyAdvice(commandRegex, directoryRegex, outRegex, errRegex, advice, check);
	}

	@Override
	public String toString() {
//		return "NotifyAdvice [commandRegex=" + commandRegex + ", directoryRegex=" + directoryRegex + ", outRegex="
//				+ outRegex + ", errRegex=" + errRegex + ", advice=" + advice + ", check=" + check + "]";
		
		
		StringBuffer founds = new StringBuffer(SandboxParameters.VISUAL_SEPARATOR);
		founds.append("Timestamp: " + Utilities.getTime("HH:mm:ss"))
			.append("\n").append("check/score ").append(this.check).append('/').append(this.score)
			.append("\n").append(getCommandRegex())
//			.append("\n").append(SandboxParameters.VISUAL_SEPARATOR).append("\n")
			.append("\n").append(getDirectoryRegex())
//			.append("\n").append(SandboxParameters.VISUAL_SEPARATOR).append("\n")
			.append("\n").append(getOutRegex())
//			.append("\n").append(SandboxParameters.VISUAL_SEPARATOR).append("\n")
			.append("\n").append(getErrRegex())
//			.append("\n").append(SandboxParameters.VISUAL_SEPARATOR).append("\n")
			.append("\n").append("\n").append("ADVICE:").append("\n")
			.append("\n").append(getAdvice()).append("\n")
//			.append(SandboxParameters.VISUAL_SEPARATOR).append("\n")
			.append("FOUNDS:").append("\n");
		return founds.toString();
		
	}


}
