// Tool36.java (erase me later)
package sandbox;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		description = "Match classpath order",
		parameterDescriptions = 
		{
			SandboxParameters.SCRATCHPAD_NAME,"classpaths",
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool36 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[] classpaths = getStringArray("classpaths");
		String classpathDelimiter = System.getProperty("path.separator",";");

		String classpathModel = classpaths[0];
		String classpathToCorrect = classpaths[1];
		
		Set<String> resourcesModel = new TreeSet<String>(Arrays.asList(classpathModel.split(classpathDelimiter)));
		Set<String> resourcesToCorrect = new TreeSet<String>(Arrays.asList(classpathToCorrect.split(classpathDelimiter)));

		int index = 0;
		Map <String,Integer> map = new HashMap<String,Integer> ();
		for (String resource : resourcesModel){
			File file = new File(resource);
			printLine(file.getName()+ ":: "+ resource);
			map.put(file.getName(),new Integer(index++));
		}
		
		String[]resourcesCorrected = new String[resourcesToCorrect.size()];
		for (String resource : resourcesToCorrect){
			File file = new File(resource);
			resourcesCorrected[map.get(file.getName())]= resource;
		}
		
		StringBuffer newResources = new StringBuffer();
		for (String resource : resourcesCorrected){
			newResources.append(resource).append(classpathDelimiter);

		}
		newResources.deleteCharAt (newResources.length()- 1);

		printLine(newResources);
		//END TEST
	}
}