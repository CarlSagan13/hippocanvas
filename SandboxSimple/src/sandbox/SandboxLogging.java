package sandbox;

//import java.util.logging.LogManager;
import java.util.logging.Logger;

public interface SandboxLogging {
	public static final Logger sandboxLogger = Logger.getLogger(SandboxParameters.class.getName());
	public static final Logger startLogger = Logger.getLogger(Start.class.getName());
}
