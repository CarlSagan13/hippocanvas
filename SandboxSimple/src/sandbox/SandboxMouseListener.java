package sandbox;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.text.JTextComponent;

public class SandboxMouseListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		JTextComponent textComponent = (JTextComponent)e.getComponent();
				
		SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
		if (textComponent != null) {
//			sandboxParameters.status.setForeground(Color.red);
//			sandboxParameters.status.setText(String.valueOf(textComponent.getCaretPosition()));
//			sandboxParameters.status.setText("Now is the time for all good men to come to the aid of their brillig");
			sandboxParameters.validateTestComponent();
			System.err.println(textComponent.getClass().getSimpleName());
			System.err.println(textComponent.getCaretPosition());
		}
				
		if (textComponent != null && textComponent.getSelectedText() != null && textComponent.getSelectedText().length() == 0) {
			return;
		}
		
		boolean altKeyDown = e.isAltDown();
		boolean ctlKeyDown = e.isControlDown();
		boolean shftKeyDown = e.isShiftDown();
		
		if (e.getClickCount() == 2){
			System.out.println("double click");
			if (e.getButton() == 1) {
				if (Boolean.getBoolean("sbx.mouse.button.one.click.two.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.one.click.two.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.one.click.two.test.command","default.sbc");
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			} else if (e.getButton() == 2) {
				if (Boolean.getBoolean("sbx.mouse.button.two.click.two.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.two.click.two.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.two.click.two.test.command","default.sbc");
//				SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			} else if (e.getButton() == 3) {
				if (Boolean.getBoolean("sbx.mouse.button.three.click.two.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.three.click.two.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.three.click.two.test.command","default.sbc");
//				SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			}
		} else {
			System.out.println("single click: button " + e.getButton());
			if (e.getButton() == 1) {
				if (Boolean.getBoolean("sbx.mouse.button.one.click.one.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.one.click.one.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.one.click.one.test.command","default.sbc");
//				SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			} else if (e.getButton() == 2) {
				if (Boolean.getBoolean("sbx.mouse.button.two.click.one.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.two.click.one.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.two.click.one.test.command","default.sbc");
//				SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			} else if (e.getButton() == 3) {
				if (Boolean.getBoolean("sbx.mouse.button.three.click.one.select")) {
					Utilities.autoSelectText();
				}
				String testName = System.getProperty("sbx.mouse.button.three.click.one.test","Default");
				String commandName = System.getProperty("sbx.mouse.button.three.click.one.test.command","default.sbc");
//				SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters();
				Hashtable<String, JButton> buttonTable = sandboxParameters.getButtons();
				sandboxParameters.commandName = commandName;
				javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX + testName);
				if (button != null) {
					button.doClick();
					button.doClick();
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
