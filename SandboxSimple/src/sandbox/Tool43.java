// Tool43.java (erase me later)
package sandbox;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
		description = "Search classpath for a class",
		parameterDescriptions = 
		{
			SandboxParameters.SCRATCHPAD_NAME,"classpathClass",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool43 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String[]classpathClass = getStringArray("classpathClass");
		String classpath = classpathClass[0];
		String className = classpathClass[1];
		
		if (classpath.length() == 0 || className.length() == 0){
			throw new SandboxException("Need classpath and class name");
		}
		
		String classpathDelimiter = System.getProperty("path.separator",";");

		Set<String> resources = new TreeSet <String>(Arrays.asList(classpath.split(classpathDelimiter)));

		for (String jarFilePath : resources){
			File jarResource = new File(jarFilePath);
			if (jarResource.exists()){
				Map<String,List<String>> foundClasses = sandbox.Utilities.findClasses(jarResource,className,false,this);
				for (String key : foundClasses.keySet()){
					List<String> classNames = foundClasses.get (key);
					if (classNames.size() > 0){
						printLine(SandboxParameters.VISUAL_SEPARATOR);
						printLine(key);
						for (String name : classNames){
							printLine("\t"+ name);

						}
					}
				}
			}else {
				printLine("Resource does not exist: "+ jarFilePath);
			}
		}
		//END TEST
	}

}

