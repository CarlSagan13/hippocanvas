package sandbox;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

@SandboxAnnotation(
        description = "Base64 encode or decode (into system clipboard) v1",
 		parameterDescriptions = {
 	             SandboxParameters.FOCUSED_NAME, "selectedText",
 	             SandboxParameters.TRUEFALSE_NAME, "encode",
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool10 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String selectedText = getString("selectedText");
		boolean encode = getBoolean("encode");
         
		if (encode) {
			printLine(sandbox.Utilities.base64Encode(selectedText));
		} else {
			Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection stringSelection = new StringSelection(sandbox.Utilities.base64Decode(selectedText));
			clip.setContents(stringSelection, null);

			this.setTooltip("Decoded text and placed in system clipboard.");
		}
		 //END TEST
	 }

}
