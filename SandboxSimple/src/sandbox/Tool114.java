package sandbox;

import java.util.ArrayList;
import java.util.List;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(description = "Tabulate data array", parameterDescriptions = { 
		SandboxParameters.FOCUSED_NAME,"delimitedData", 
		SandboxParameters.POPPADVALS_NAME, "regexRowColumn",
		SandboxParameters.TRUEFALSE_NAME, "quotes",
		SandboxParameters.YESNO_NAME, "header" 
		}, showInButtonPanel = false, showInTabPanel = true, isTest = false, isTool = true)

public class Tool114 extends SandboxTestBase /* dmh327 */ {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TOOL
		String delimitedData = getString("delimitedData");
		
		String regexRow = getString("regexRow");
		if (regexRow == null || regexRow.length() == 0) {
			regexRow = getStringArray("regexRowColumn")[0];
		}
		String[] delimitedLinesArray = delimitedData.split(regexRow);
		
		String regexColumn = getString("regexColumn");
		if (regexColumn == null || regexColumn.length() == 0) {
			regexColumn = getStringArray("regexRowColumn")[1];
		}
		
		boolean quotes = getBoolean("quotes");
		boolean header = getBoolean("header");

		List<String> delimitedLines = new ArrayList<String>();

		int numCols = 0;
		for (String string : delimitedLinesArray) {
			delimitedLines.add(string);
			int currentNumCols = string.split(regexColumn).length;
			if (currentNumCols > numCols) {
				numCols = currentNumCols;
			}
		}

		String[] headers = {};
		int numHeaders = delimitedLines.get(0).split(regexColumn).length;
		if (header && numHeaders < numCols) {
			printLine("Not enough headers! Need " + numCols);
			delimitedLines.remove(0);
			header = false;
		}

		if (header) {
			String headerLine = delimitedLines.remove(0);
			headers = headerLine.split(regexColumn);
			if (quotes) {
				for (int index = 0; index < headers.length; index++) {
					headers[index] = "\"" + headers[index] + "\"";
				}
			}
		} else {

			String[] headerArray = new String[numCols];
			List<String> headersList = new ArrayList<String>();
			for (int i = 1; i <= numCols; i++) {
				headersList.add(String.valueOf(i));
			}

			headers = headersList.toArray(headerArray);
		}

		String[][] data = new String[delimitedLines.size()][numCols];

		int rowNum = 0;
		for (String rowValue : delimitedLines) {
			int colNum = 0;
			for (String cellValue : rowValue.split(regexColumn)) {
				if (quotes) {
					cellValue = "\"" + cellValue + "\"";
				}
				data[rowNum][colNum++] = cellValue;
			}
			rowNum++;
		}

		this.tabulate(data, headers, "some tooltip");
		// END TOOL
	}
}
// page break