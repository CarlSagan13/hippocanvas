package sandbox;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

@SandboxAnnotation(description = "Archive by name filter into storage directory",
	parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"fileRegex,sourceDirectory",
			SandboxParameters.TRUEFALSE_NAME,"recurse",
			SandboxParameters.YESNO_NAME,"copyFiles"
	}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool11 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String sourceDir = getString("sourceDirectory");
		String fileRegex = getString("fileRegex");
		boolean recurse = getBoolean("recurse");
		boolean copyFiles = getBoolean("copyFiles");

		String targetDir = params.getConfigurationDirectory() + File.separator + "storage";


		if (copyFiles) {
			StringBuffer message = new StringBuffer();

			String newline = System.getProperty("line.separator", "\n");
			message.append("ARE VOU SURE YOU WANT T0 COPY:");

			message.append(newline).append(sourceDir);
			message.append(newline).append("T0");
			message.append(newline).append(targetDir);

			if ((new SandboxMessage("VERIFY", message.toString(), params.getTabbedPane(), 20, true)).getStatus() < 0) {
				throw new sandbox.SandboxException("You cancelled!");
			}
		}

		File source = new File(sourceDir);

		File target = new File(targetDir);
		if (source.exists()) {
			Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
			Collection<File> files = sandbox.Utilities.listFilesRegex(source, recurse, true,  pattern, this);
			Set<String> copiedSet = new HashSet<String>();

			for (File file : files) {

				// Check is directory
				if (!file.isDirectory()) {

					// Check has not already been copied
					boolean copied = false;
					for (String copiedDir : copiedSet) {
						if (file.getPath().startsWith(copiedDir)) {
							copied = true;
						}
					}
					if (!copied) {
						String copyPath = file.getPath();
						if (copyPath.contains(":")) {
							copyPath = copyPath.substring(copyPath.indexOf(':') + 1);
						}

						int indexOfColon = copyPath.indexOf(":");
						if (indexOfColon > 0) {
							copyPath = copyPath.substring(indexOfColon + 1);
						}

						File sandboxTargetPath = new File(target.getPath() + copyPath);
						if (!sandboxTargetPath.exists()) {
							sandboxTargetPath.getParentFile().mkdirs();
						}

						printLine(file.getPath() + "\n\tto\n" + sandboxTargetPath);
						printLine(SandboxParameters.VISUAL_SEPARATOR);
						if (copyFiles) {
							sandbox.Utilities.copyFile(file, sandboxTargetPath, true);
						}
						copiedSet.add(file.getPath());
					}
				}
			}
		} else {
			printLine(sourceDir + " does not exist!");
		}
		// END TEST
	}

}
