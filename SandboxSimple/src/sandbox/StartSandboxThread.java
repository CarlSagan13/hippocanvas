package sandbox;

import java.util.logging.Level;

public class StartSandboxThread extends StartThread {
	private String sandboxPackage = "sandbox";

	public StartSandboxThread(String sandboxHome, String sandboxPackage) {
		super(sandboxHome);
		this.sandboxHome = sandboxHome;
		this.sandboxPackage = sandboxPackage;
	}

	public void run() {
		try {
			runInSeparateJavaProcess(Sandbox.class.getName() + " " + sandboxPackage);
		} catch (Exception en) {
			startLogger.log(Level.ALL, "Could not start", en);
		}
	}

}
