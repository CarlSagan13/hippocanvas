//ISandboxBackedUp.java (erase me later) 
package sandbox;
import java. io. File;
public interface ISandboxBackedUp {
	 public boolean isDirty();
	 public boolean isText();
	 public boolean isEnabled();
	 public boolean isEditable();
	 public void setDirty(boolean dirty);
	 public File getFile();
	 public void setFile(File file);
	 public String getBackupText();
	 public void setMapId(Object mapId);
	 public Object getMapId();
	 public void saveBackedUp() throws SandboxException; 	 
	 public void closeBackedUp() throws SandboxException; 
	 public void close();
}
