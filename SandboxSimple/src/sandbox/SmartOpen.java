//Tool2.java (erase me later)
package sandbox;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JButton;

@SandboxAnnotation(
		description = "Smart open",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"commandLine,workingDirectory",
			SandboxParameters.FOCUSED_NAME,"recordedTest"
		}, showInButtonPanel = true, showInTabPanel = false, isTest = false, isTool = true)

public class SmartOpen extends SandboxTestBase {

	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		if (getString("commandLine") == null || getString("commandLine").length() == 0) {
			params.printLine("Empty command line", false, false);
			return;
		}
		String[] lineArray = getStringArrayNoDelim("commandLine");
		String headLine = lineArray[0];
		String recordedTest = getString("recordedTest");
		
		/**
		 * 			printLine(URLDecoder.decode(selectedText,(String)charset[0]));

		 */

		if (lineArray.length == 1 && headLine.contains(SEARCH_FLAG)) { //Construct recorded test for search
			SandboxTestBase findInFileTool = params.getTest(FindInFiles.class.getSimpleName());
			String[] descriptions = findInFileTool.getParameterDescriptions();
			for (String desc : descriptions) {
				params.printLine(desc);
				//TODO you need to these descriptions to the recorded test string below
			}
			String[] search = headLine.split("(\\t)", 2);
			
			recordedTest = FindInFiles.class.getSimpleName() + "\nsearch	" + search[0]
					+ "\nfileRegex	" + System.getProperty(LINK_SEARCH_FILE_REGEX_ENV, ".*\\.dmh$")
					+ "\ndirectory	" + System.getProperty(LINK_SEARCH_DIR_ENV, params.sandboxHome)
					+ "\nrecurse	" + System.getProperty(LINK_SEARCH_RECURSE_ENV, "true");
			params.runTest(recordedTest, Utilities.limitTabLength(search[0]));

			
			return;
		}
		String searchLinkRegex = URLDecoder.decode(System.getProperty(LINK_SEARCH_REGEX_ENV, "%5Cb%5Cd%7B13%7D%5Cb"), "US-ASCII"); 
		if (lineArray.length == 1 && headLine.matches(searchLinkRegex)) { //Construct recorded test for search
			SandboxTestBase findInFileTool = params.getTest(FindInFiles.class.getSimpleName());
			String[] descriptions = findInFileTool.getParameterDescriptions();
			for (String desc : descriptions) {
				params.printLine(desc);
				//TODO you need to these descriptions to the recorded test string below
			}
			String[] search = headLine.split("(\\s|\\t)", 2);
			String tabName = headLine;
			if (search.length == 2) {
				findInFileTool.setTabDisplayName(Utilities.limitTabLength(search[1]));
				tabName = search[1];
//				findInFileTool.setTooltip(search[1]);
			}
			
			recordedTest = FindInFiles.class.getSimpleName() + "\nsearch	" + search[0]
					+ "\nfileRegex	" + System.getProperty(LINK_SEARCH_FILE_REGEX_ENV, ".*\\.dmh$")
					+ "\ndirectory	" + System.getProperty(LINK_SEARCH_DIR_ENV, params.sandboxHome)
					+ "\nrecurse	" + System.getProperty(LINK_SEARCH_RECURSE_ENV, "true");
			params.runTest(recordedTest, Utilities.limitTabLength(tabName));

			
			return;
		}
		
		if (headLine.matches("^(Tool|Test)\\d.*") | headLine.matches(".*\\t" + RECORDED_FLAG)) { //Recorded tool/test
//			if (headLine.matches("^(Tool|Test)\\d.*") | headLine.matches("^Command.*")) { //Recorded tool/test
			params.printLine(this.getClass().getSimpleName() + "*** Running as recorded test ***", true, true);
			params.runTest(recordedTest);
			return;
		} else if (lineArray.length == 1 && 
				((new File(headLine.split("\\t")[0])).exists()) &&
				((new File(headLine.split("\\t")[0])).isDirectory())) { //Open prompt
			String[] addressCommand = headLine.split("\\t");
			String defaultPromptCommand = System.getProperty("sbx.default.prompt", "gnome-terminal");
			defaultPromptCommand = URLDecoder.decode(defaultPromptCommand, "US-ASCII");
			defaultPromptCommand = defaultPromptCommand.replace(ADDRESS_REPLACEMENT, addressCommand[0]);
			if (addressCommand.length == 2) {
				String promptCommand = addressCommand[1].replace("sbx.address.replacement", addressCommand[0]);
				runInSeparateProcess(promptCommand, new File(addressCommand[0]), true, true); //Prompt workspace-command
			} else {
				runInSeparateProcess(defaultPromptCommand, new File(headLine), true, true); //Prompt command
			}
			return;
		} else if (lineArray.length == 1 && 
				headLine.split("\\t+").length == 2 && 
				((new File(headLine.split("\\t+")[0])).exists())) { //Open file with tab title
			params.printLine(this.getClass().getSimpleName() + "*** Opening file ***", true, true);
			SandboxOpen sandboxOpen = new SandboxOpen(params, headLine, true);
			sandboxOpen.setName("Opening " + headLine);
			sandboxOpen.start();
			return;
		} else if (lineArray.length == 1 && ((new File(headLine)).exists())) { //Open file in sandbox
			params.printLine(this.getClass().getSimpleName() + "*** Opening file ***", true, true);
			SandboxOpen sandboxOpen = new SandboxOpen(params, headLine, true);
			sandboxOpen.setName("Opening " + headLine);
			sandboxOpen.start();
			return;
		} else if (lineArray.length == 1 && //Not sure this if block is good for anything
				headLine.split("\\t+").length == 2 && 
				((new File(headLine.split("\\t+")[0])).exists())) { //Open file with command
			String adr = headLine.split("\\t+")[0];
			String cmd = headLine.split("\\t+")[1];
			cmd = URLDecoder.decode(cmd, "US-ASCII");
			cmd = cmd.replace(ADDRESS_REPLACEMENT, adr);
			runInSeparateProcess(cmd, new File(params.sandboxHome), true, true);
			return;
		} else if ((!(new File(headLine)).exists()) && headLine.contains(";")
				&& !headLine.split(";")[0].contains(" ")) { //Set global map value
			params.printLine(this.getClass().getSimpleName() + "*** Setting global map ***", true, true);
			Hashtable<String, JButton> buttonTable = params.getButtons();
			javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX
					+ Tool58.class.getSimpleName());
			if (button != null) {
				button.doClick();
				button.doClick();
			}
			return;
		} else if ((!(new File(headLine)).exists()) && headLine.contains(",")
				&& !headLine.split(",")[0].contains(" ")) { //Set env value
			params.printLine(this.getClass().getSimpleName() + "*** Setting environment ***", true, true);
			Hashtable<String, JButton> buttonTable = params.getButtons();
			javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX
					+ Tool8.class.getSimpleName());
			if (button != null) {
				button.doClick();
				button.doClick();
			}
			return;
//		} else if (lineArray.length == 1 && headLine.matches("^.*\\t.*sbx\\.action\\.start\\.sandbox.*")) { //Open a sandbox
		} else if (lineArray.length == 1 && headLine.matches("^.*\\t.*" + START_SANDBOX_FLAG + ".*")) { //Open a sandbox
			String selectedSandbox = headLine.split("\\t")[0];
			StartThread thread = new StartThread(params.sandboxHome, selectedSandbox);
			thread.start();
//			addToGlobalMap(thread);
			return;
		} else if (lineArray.length == 1 && headLine.matches("^(http|file).*://.*")) {
			String[] addressCommand = headLine.split("\\t+");
			String browserCommand = System.getProperty(BROWSER_REPLACEMENT, "google-chrome+" + ADDRESS_REPLACEMENT);
			browserCommand = URLDecoder.decode(browserCommand, "US-ASCII");
			String address = addressCommand[0];
			if (addressCommand.length == 2) {
				browserCommand = addressCommand[1];
				if (browserCommand.startsWith("sbx.")) {
					String encodedBrowserCommand = System.getProperty(browserCommand); // browserCommand here is the enviroment var name
					if (encodedBrowserCommand == null) {
						params.alert(browserCommand + " is not defined", params.iconStopped);
						return;
					}
					browserCommand = URLDecoder.decode(encodedBrowserCommand, "US-ASCII");
				}
				browserCommand = browserCommand.replace(ADDRESS_REPLACEMENT, address);
				runInSeparateProcess(browserCommand, new File(params.sandboxHome), true, true);
				return;
			}
			browserCommand = browserCommand.replace(ADDRESS_REPLACEMENT, address);
			runInSeparateProcess(browserCommand, new File(params.sandboxHome), true, true);
			return;
			
//*////////////////////////////////////////////////////////////////DMH		
		} else if (lineArray[0].startsWith(SandboxProcess.PROCESS_ID_PREFIX) && lineArray.length > 1) {
			params.printLine(this.getClass().getSimpleName() + "*** Running in a sandbox process ***", true, true);

			SandboxTestBase runProcessTool = params.getTest(RunProcess.class.getSimpleName());

			String[] descriptions = runProcessTool.getParameterDescriptions();
			for (String desc : descriptions) {
				params.printLine(desc);
				//TODO you need to these descriptions to the recorded test string below
			}

			StringBuffer selectedText = new StringBuffer(); 
			for (String line : lineArray) {
				if (line.startsWith(SandboxProcess.PROCESS_ID_PREFIX)) {
					//if collected, run the previous process
					if (selectedText.length() > 0) {
						// run it
						recordedTest = RunProcess.class.getSimpleName() 
								+ "\nprocidCommands\t"
								+ "-encoded " + URLEncoder.encode(selectedText.toString(), "US-ASCII");
						String processIdName = selectedText.toString().split("\n")[0];
						String processTabName = processIdName.substring(SandboxProcess.PROCESS_ID_PREFIX.length());
						if (processTabName.split("\t").length > 1) {
							processTabName = processTabName.split("\t")[0];
						}
						runProcessTool.setTabDisplayName(Utilities.limitTabLength(processTabName));

						params.runTest(recordedTest, processTabName);
						selectedText = new StringBuffer(); 
						selectedText.append(line).append("\n");
					} else {
						selectedText.append(line).append("\n");
					}
				} else { //collect the command line the process uses
					selectedText.append(line).append("\n");
				}
			}
			if (selectedText.length() > 0) {
				// run it
				recordedTest = RunProcess.class.getSimpleName() 
						+ "\nprocidCommands\t"
						+ "-encoded " + URLEncoder.encode(selectedText.toString(), "US-ASCII");
				
				//				findInFileTool.setTabDisplayName(Utilities.limitTabLength(search[1]));
				String processIdName = selectedText.toString().split("\n")[0];
				String processTabName = processIdName.substring(SandboxProcess.PROCESS_ID_PREFIX.length());
				if (processTabName.split("\t").length > 1) {
					processTabName = processTabName.split("\t")[0];
				}

				runProcessTool.setTabDisplayName(Utilities.limitTabLength(processTabName));
				params.runTest(recordedTest, processTabName);
			}
			
			return;
		}
//*////////////////////////////////////////////////////////////////			
//		} else if (lineArray[0].startsWith(SandboxProcess.PROCESS_ID_PREFIX) && lineArray.length > 1) {
//			params.printLine(this.getClass().getSimpleName() + "*** Running in a sandbox process ***", true, true);
//			Hashtable<String, JButton> buttonTable = params.getButtons();
//			javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX
//					+ RunProcess.class.getSimpleName());
//			
//			String processTabName = lineArray[0].substring(SandboxProcess.PROCESS_ID_PREFIX.length());
//			if (processTabName.split("\t").length > 1) {
//				processTabName = processTabName.split("\t")[0];
//			}
//			SandboxTestBase processTest = params.getTest(RunProcess.class.getSimpleName());			
//			processTest.setTabDisplayName(processTabName);
//			
//			if (button != null) {
//				button.doClick();
//				button.doClick();
//			}
//			return;
//		}
//*////////////////////////////////////////////////////////////////			
		
		
		try {
			params.printLine(this.getClass().getSimpleName() + "*** Running ***", true, true);
			Hashtable<String, JButton> buttonTable = params.getButtons();
			javax.swing.JButton button = buttonTable.get(SandboxParameters.BUTTON_PREFIX
					+ Tool2.class.getSimpleName());
			if (button != null) {
				button.doClick();
				button.doClick();
			}
		} catch(Throwable th) {
			params.printLine("Run exception", th);
		}
		// END TEST
	}
	protected static String BROWSER_REPLACEMENT = "sbx.default.browser";
	protected static String ADDRESS_REPLACEMENT = "sbx.address.replacement";
	protected static String START_SANDBOX_FLAG = "sbx.action.start.sandbox";
	protected static String LINK_SEARCH_REGEX_ENV = "sbx.value.link.search.regex";
	protected static String LINK_SEARCH_FILE_REGEX_ENV = "sbx.value.link.file.regex";
	protected static String LINK_SEARCH_DIR_ENV = "sbx.value.link.directory";
	protected static String LINK_SEARCH_RECURSE_ENV = "sbx.value.link.recurse";
	protected static String SEARCH_FLAG = "sbx.search.flag";
	protected static String RECORDED_FLAG = "sbx.recorded.flag";
}
