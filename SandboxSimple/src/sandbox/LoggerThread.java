//LoggerThread.java (erase me later)
package sandbox; 
 
 import java. io. BufferedReader; 
 import java. io. File; 
 import java. io. FileNotFoundException; 
 import java. io. FileReader;

 import java. io. IOException; 
 import java. util . ArrayList; 
 import java. util. List; 
 
 import javax. swing. JTextArea; 
 import javax. swing. text. Document; 
 
 public class LoggerThread extends Thread {
 	 private JTextArea textArea = null; 
 	 private String filePath = null; 
 	 private int frequencyInMilliseconds = 100; 
 	 private int windowSize = 1000; 
 	 private boolean isRunning = true; 
 	 
 	 public void run(){
 	 	 File tailedFile = new File(filePath); 
 	 	 long lastModified = tailedFile. lastModified (); 
 	 	 long lastSize = tailedFile. length(); 
 
 	 	 try {
 	 	 	 //find parameters of file to show initial tailing
 	 	 	 FileReader fileReader = null; 
 	
	 	 fileReader = new FileReader(tailedFile); 
 	 	 	 BufferedReader bufferedReader = new BufferedReader(fileReader); 
 
 	 	 	 List<String> lines = new ArrayList<String> (); 
 	 	 	 String string = ""; 
 	 	 	 int lastLineCount = 0; 
 	 	 	 while((string = bufferedReader. readLine())!= null){
 	 	 	 	 lines . add(string); 
 	 	 	 	 if (lines . size() > windowSize){
 	 	 	
	 	 lines. remove(0); 
 	 	 	
	 }
 	 	 	 	 lastLineCount++; 

	 	 	 }
 	 	 	 fileReader. close(); 
 
 	 	 	 for (String line : lines){
 	 	 	 	 printLine(line); 
 	 	 	 }
 	 	 	 lines.clear(); 
 	 	 	 
 	 	 	 while (this.isRunning){
 	 	 	 	 Thread. sleep(frequencyInMilliseconds); 
 	 	 	 	 if (lastModified != tailedFile.lastModified()
	 	 	 	 		 || lastSize < tailedFile. length ()){
 	 	 	 	 	tailedFile = new File(filePath); 
 	 	 	 	 	fileReader = new FileReader(tailedFile); 
 	 	 	 	 	bufferedReader = new BufferedReader(fileReader); 
 	 	 	 	 	String line = ""; 
 	 	 	 	 	int index = 0; 
 	 	 	 	 	 
 	 	 	 	 	 while((line = bufferedReader. readLine())!= null){
 	 	 	 	 	 	 index++; 
 	 	 	 	 	 	 if (index > lastLineCount){
 	 	 	 	 	 	 	 printLine(line); 
 	 	 	 	 	 	 }
 	 	 	 	 	 }
 	 	 	 	 	 fileReader. close(); 
 	 	 	 	 	 if (index < lastLineCount){
 	 	 	 	 	 	 printLine("File shortened"); 
 	 	 	 	 	 	 isRunning = false; 
 	 	 	 	 	 }

 	 	 	 	 	 lastLineCount = index; 
 	 	 	 	 	 lastModified = tailedFile. lastModified(); 
 	 	 	 	 	 lastSize = tailedFile. length(); 
 	 	 	 	 }
 	 	 	 }
 	 	 	 
 	 	 }catch(FileNotFoundException fnfe){

	 	 	 printLine("Could not open file: "+ fnfe . getMessage()); 
 	 	 }catch(IOException ioe){
 	 	 	 printLine("Could not read file: "+ ioe. getMessage());

 	 	 }catch(InterruptedException ie){
 	 	 	 printLine("Thread interrupted unexpectedly: "+ ie. getMessage()); 
 	 	 }
 	 	 
 	 }
 	 
 	 private void printLine(String string){
 	 	 textArea. append(string + '\n'); 
 	 	 Document doc = this. textArea. getDocument(); 
 	 	 this. textArea. setCaretPosition(doc. getLength()); 
 	 }
 
 	 /**
 	 *@return the textArea
 	 */
 	 public JTextArea getTextArea(){
 	 	 return textArea; 
 	 }
 
 	 /**
 	 *@param textArea the textArea to set
 	 */
 	 public void setTextArea(JTextArea textArea){
 	 	 this. textArea = textArea; 
 	 }
 
 	 /**
 	 *@return the filePath
 	 */
 	 public String getFilePath(){
 	 	 return filePath; 
 	 }
 
 	 public void setFilePath(String filePath){
 	 	 this. filePath = filePath ; 
 	 }
 
 	 /**
 	 *@return the frequencyInMilliseconds
 	 */
 	 public int getFrequencyInMilliseconds(){
 	 	 return frequencyInMilliseconds; 
 	 }
 
 	 /**
 	  * @param frequencyInMilliseconds the frequencyInMilliseconds to set
 	 */
 	 public void setFrequencyInMilliseconds(int frequencyInMilliseconds){
 	 	 this. frequencyInMilliseconds = frequencyInMilliseconds; 
 	 }
 
 	 /**
 	 *@return the windowsize
 	 */
 	 public int getWindowSize(){
 	 	 return windowSize; 
 	 }
 
 	 /**
 	 *@param windowSize the windowSize to set 
 	 */
 	 public void setWindowSize(int windowsize){
 	 	 this. windowSize = windowsize; 
 	 }
 
 	 /**
 	 *@return the isRunning
 	 */
 	 public boolean isRunning(){
 	 	 return isRunning; 
 	 }
 
 	 /**
 	 *@param isRunning the isRunning to set 
 	 */
 	 public void setRunning(boolean isRunning){
 	 	 this. isRunning = isRunning; 
 	 }
 
 	 public void close(){
 	 	 this. isRunning = false; 
 	 }
}

