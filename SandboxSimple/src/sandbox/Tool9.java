// Tool9.java (erase me later)
package sandbox;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@SandboxAnnotation(
		description = "Regex replace in files (see sandbox help for directions)",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"fileRegex,sourceDirectory",
			SandboxParameters.POPPAD_NAME,"changes",
			SandboxParameters.TRUEFALSE_NAME,"recurse",
			SandboxParameters.YESNO_NAME,"makeChanges"
		},showInButtonPanel = true, showInTabPanel = true,isTest = false,isTool = false)
		
public class Tool9 extends SandboxTestBase {
	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String sourceDirectory = getString("sourceDirectory");
		String fileRegex = getString("fileRegex");
		String[] changes = getStringArray("changes");
		boolean recurse = getBoolean("recurse");
		boolean makeChanges = getBoolean("makeChanges");
		setTooltip(sourceDirectory + ":: "+ fileRegex + ":: recurse= "+ recurse + ":: makeChanges= "+ makeChanges);

		if (changes.length % 2 != 0){
			throw new SandboxException ("Each change must have a from/to");
		}

		StringBuffer display = new StringBuffer();
		String newline = System.getProperty("line.separator","\n");
		String tab = "\t ";
		display.append("sourceDirectory = "+ sourceDirectory).append(newline);
		display.append("fileRegex = "+ fileRegex).append(newline);
		display.append("changes:").append(newline);
		for (String change : changes){
			display.append(tab).append(change).append(newline);
		}

		if (makeChanges){
			SandboxMessage message = new SandboxMessage("Are you sure???", display.toString(), params.getTabbedPane(), 10, true);

			if (message.getStatus() < 0){
				throw new SandboxException("You cancelled!");
			}
		}

		File directory = new File (sourceDirectory);
		if (!directory.isDirectory()){
			throw new SandboxException("Not a directory!");
		}
		Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);

		Collection<File> files = sandbox.Utilities.listFilesRegex(directory, recurse, true,  pattern, this);

		Iterator<File> iterFiles = files.iterator();

		Map<String,String> grabSlapMap = new HashMap<String,String> ();

		String directoryTimeStamp = Utilities.parseDate(new Date (),SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP);		
		String strTargetDir = params.getConfigurationDirectory()+ File.separator + "storage"+ File.separator + directoryTimeStamp;
		File targetDir = new File (strTargetDir);
		if (!targetDir.exists()){
			targetDir.mkdirs ();
		}

		while(iterFiles.hasNext()){
			File file = null;
			int changeCount = 0;
			List<String> changeHistory = new ArrayList <String> ();
			try {
				file = iterFiles.next();
				grabSlapMap.clear();
				printLine(file.getAbsolutePath());
				StringBuffer fileStringBuffer = new StringBuffer(sandbox.Utilities.getFileString(file.getAbsolutePath()));

				changeHistory.add(changeCount++, fileStringBuffer.toString());

				if (changes.length > 0){
					for (int index = 0;index < changes.length;index += 2){

//						try {
							String regex = changes[index];
							String[] replace = changes[index + 1].split(",");
							
							StringBuffer newText = new StringBuffer();
							List<SandboxBasket<String[],String[]>> list = Utilities.grabReplaceRegex(regex, replace, fileStringBuffer, newText);
							if (list.size() > 0) {
								changeHistory.add(changeCount++, newText.toString());
//								this.addToGlobalMap(file.getAbsolutePath(), newText);
							}
							fileStringBuffer = newText;
//						}catch(SandboxToolException ste){
//							printLine(ste);
//						}

					}
					if (makeChanges && changeHistory.size() > 1){
						String targetFilePath = strTargetDir + file.getPath();
						if (Utilities.isWindows()){
							targetFilePath = strTargetDir + file.getPath().substring(file.getPath().indexOf(':') + 1);
						}
						Utilities.copyFile(file,new File(targetFilePath),true);
						printLine (tab + "Copied " + file.getName()+ " to " + strTargetDir);

						sandbox.Utilities.saveFileString(file.getAbsolutePath(), fileStringBuffer.toString());
						translate(grabSlapMap, true, true);
					}
				}

			} catch(Exception en){
				printLine(tab + "Did not process "+ file.getName()+ " :: "+ en.getMessage());
			}

			if (changeHistory.size() > 1){
				String mapId = file.getAbsolutePath() + Utilities.getTime("hhmmss.S");
				this.addToGlobalMap(mapId, changeHistory);
				printLine(tab + mapId);
			}
		}
		//END TEST
	}
	
	class SandboxToolException extends sandbox.SandboxException { //TODO Use SandboxException instead
		/**
		*
		*/
		private static final long serialVersionUID = 1L;

		public SandboxToolException(String message){
			super(message);
		}
	}
	
	public static final String abort = "abort:";
	public static final String required = "required:";
	public static final String regexPrefix = "regex:";
	public static final String groups = "groups:";
	public static final String grabPrefix = "grab:";
	public static final String slapPrefix = "slap:";
	public static final String replacePrefix = "replace:";

}