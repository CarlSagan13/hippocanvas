package sandbox;
import java.util.Date;

import sandbox.SandboxAnnotation;
//gone
import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = SandboxTestBase.UNDER_CONSTRUCTION
 			+ "Display formatted date (dateFormat ignored if dateString is a Long)",
 		parameterDescriptions = {
 				SandboxParameters.FOCUSED_NAME, "dateFormat,dateString,newDateFormat"
 		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool59 extends SandboxTestBase {
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String dateFormat = (String)parameterValues.get("dateFormat");
		String dateString = (String)parameterValues.get("dateString");
		String newDateFormat = (String)parameterValues.get("newDateFormat");
		java.util.Date date = new java.util.Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(dateFormat);
		
		if (dateString.matches("\\d+$")) {
			Long longDate = Long.parseUnsignedLong(dateString);
			Date newDate = new Date(longDate);
			java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
			printLine(newSdf.format(newDate));
		} else {
			java.util.Date newDate = sdf.parse(dateString);
			java.text.SimpleDateFormat newSdf = new java.text.SimpleDateFormat(newDateFormat);
			printLine(newSdf.format(newDate));
		}
		//END TEST
	}
}
