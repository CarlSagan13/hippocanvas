package sandbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SandboxAnnotation(
        description = "Compare delimited string lists (parameters: listl/list2/delimiter)",
 		parameterDescriptions = {
 	       SandboxParameters.FOCUSED_NAME, "parameters"
 	    }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool17 extends SandboxTestBase {
	 
	 @Override
	 public void test(SandboxParameters params) throws Exception {
		 super.test(parameterDescriptions);
		 //BEGIN TEST
	        String[] parameters = getStringArray("parameters");
	                String stringOne = parameters[0];
	                String stringTwo = parameters[1];
	                String delimiter = parameters[2];
	                String[] saListl = stringOne.split(delimiter);
	                String[] saList2 = stringTwo.split(delimiter);
	                Set<String> setl = new HashSet<String> ();
	                for (String line : saListl) {
	                    setl.add(line);
	                }
	                Set<String> set2 = new HashSet<String> ();
	                for (String line : saList2) {
	                    set2.add(line);
	                }
	                List<String> bothLists = new ArrayList<String> ();
	                List<String> listOneOnly = new ArrayList<String> ();
	                for (Object line : setl.toArray()) {
	                    if (set2.contains(line)) {
	                        bothLists.add((String)line);
	                    } else {
	                        listOneOnly.add((String)line);
	                    }
	                }

	                List<String> listTwoOnly = new ArrayList<String> ();
	                for (Object line : set2.toArray()) {
	                    if (!setl.contains(line)) {
	                        listTwoOnly.add((String)line);
	                    }
	                }

	                Collections.sort(bothLists);
	                printLine(SandboxParameters.VISUAL_SEPARATOR + "\nBOTH LISTS\n" + SandboxParameters.VISUAL_SEPARATOR);
	                for (String line : bothLists) {

	                    printLine(line);

	                }

	                Collections.sort(listOneOnly);

	                printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST ONE ONLY\n" + SandboxParameters.VISUAL_SEPARATOR);
	                for (String line : listOneOnly) {

	                    printLine(line);

	                }

	                Collections.sort(listTwoOnly);

	                printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST TWO ONLY\n" + SandboxParameters.VISUAL_SEPARATOR);
	                for (String line : listTwoOnly) {

	                    printLine(line);

	                }
		 //END TEST
	 }

}
