package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SandboxAnnotation(
		description = "Regex find in files/directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "fileRegex,regexString,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)
 
public class Tool61 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String regex = getString("fileRegex");
		String directory = getString("directory");
		String regexString = getString("regexString");
		boolean recurse = getBoolean("recurse");
		
		Pattern patternSearch = Pattern.compile(regexString, Pattern.MULTILINE);
		
		params.getTabbedPane().setToolTipTextAt(params.getTabbedPane().getSelectedIndex(), 
				regex + " :: " + regexString + " :: " + directory);

//		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
//		params.getTabbedPane().setTitleAt(params.getTabbedPane().getSelectedIndex(), Utilities.limitTabLength(regexString));
//		params.selectTab(params.getTabbedPane().getSelectedIndex());

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
			boolean finding = false;
			Map<SandboxBasket<String, Integer>, Integer> map = new HashMap<SandboxBasket<String, Integer>, Integer>();
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)){
//				if (file.isFile() && !Utilities.isBinaryFile(file)) { //doesn't work consistently
				if (file.isFile()) {
					StringBuffer searchMe = new StringBuffer(new String(Utilities.getFileBytes(file)));
					List<SandboxBasket<String, Integer>> finds = Utilities.grabRegexList(patternSearch, searchMe, 0);
					if (finds != null && finds.size() > 0) {
						printLine(file.getPath());
//						String hash = file.getName() + file.getPath().hashCode();
						
						for (SandboxBasket<String, Integer> find : finds) {
							if (map.get(find) == null) {
								map.put(find, 1);
							} else {
								map.put(find, map.get(find) + 1);
							}
							printLine(find.getValue() + "\t" + find.getName().replace('\n', ' '));
						}
					}
				}
				checkRunning();
			}
			if (finding) {
				printALine();
			}
			
			if (map.size() > 0) {
				String[] headers = {"count","group","key"};
				String[][] columnRows = new String[map.size()][3];
				int row = 0;
				for (SandboxBasket<String, Integer> key : map.keySet()) {
					columnRows[row][0] = String.valueOf(map.get(key));
					columnRows[row][1] = String.valueOf(key.getValue()); 
					columnRows[row++][2] = key.getName(); 
				}
				String tooltip = "report";
				boolean sort = true;
				this.tabulate(columnRows, headers, tooltip, sort);
			}
			
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
}
