package sandbox;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

@SandboxAnnotation(
		description = "Set up SSL environment", 
		parameterDescriptions = { 
				SandboxParameters.POPPAD_NAME, "sslParameters", 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool18 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String[] sslParameters = getStringArray("sslParameters");
		String keystoreFilePath = sslParameters[0];
		String keystoreType = sslParameters[1];
		String keymanagerType = sslParameters[2];
		String truststoreFilePath = sslParameters[3];
		String truststorePassword = Utilities.base64Decode(sslParameters[4]);
		String ssl = sslParameters[5];
		String password = truststorePassword;
		if (sslParameters.length == 7) {
			password = sslParameters[6];
		}
		KeyStore keyStore = KeyStore.getInstance(keystoreType); // "PKCS12" or
																// "JKS"
		keyStore.load(new FileInputStream(keystoreFilePath), password.toCharArray()); // "theP12file.pl2"
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(keymanagerType);
		kmf.init(keyStore, password.toCharArray());

		KeyStore trustStore = KeyStore.getInstance(keystoreType);

		trustStore.load(new FileInputStream(truststoreFilePath), truststorePassword.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(keymanagerType);

		tmf.init(trustStore);

		SSLContext sslContext = SSLContext.getInstance(ssl); // SSL

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		// END TEST
	}

}
