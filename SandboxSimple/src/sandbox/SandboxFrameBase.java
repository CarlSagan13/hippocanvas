//SandboxFrameBase.java (erase me later)
package sandbox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;

import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.Timer;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.text.JTextComponent;

import sandbox.Globals.MapCleanRunner;

class SandboxFrameBase extends JFrame implements SandboxCommon {
	static final long serialVersionUID = 0;

	static private String SAVE_BUTTON_NAME = "Save";
	static private String CANCEL_BUTTON_NAME = "Cancel";
	static private String PHONE_BUTTON_NAME = "Phone";
//	static private String CHICLET_BUTTON_NAME = "Chiclet";

	static private String HISTORY_DIRECTORY_NAME = "history";
	
	static public String ALWAYS_ON_TOP_VAR_NAME = "sbx.conf.alwaysOnTop";
	
	static public int messageCount = 0; //For calculating message offset

	final JButton buttonCancel = new JButton(CANCEL_BUTTON_NAME);
	final JButton buttonSave = new JButton(SAVE_BUTTON_NAME);
	final JButton buttonPhone = new JButton(PHONE_BUTTON_NAME);
//	final JButton buttonChiclet = new JButton(CHICLET_BUTTON_NAME);

	protected SandboxParameters sandboxParameters = SandboxParameters.createSandboxParameters(false);

	final JFrame frameEdit = new JFrame();
	final JCheckBox checkBox = new JCheckBox("Save configuration", true);
	protected JCheckBoxMenuItem cbMenuItemSaveOnExit = null;

	protected int tabId = 0;

	public SandboxFrameBase() {
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// SandboxFrameBase sfb = (SandboxFrameBase) e.getSource();
				e.getSource();
				closeSandbox();
			}
		});

		buttonCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonAction(e);
			}
		});

		buttonSave.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemFileSaveTab_actionPerformed(e);
				// buttonAction(e);
			}
		});

		buttonPhone.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemPhone_actionPerformed(e);
				// buttonAction(e);
			}
		});

//		buttonChiclet.addActionListener(new java.awt.event.ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				menuItemChiclet_actionPerformed(e);
//				// buttonAction(e);
//			}
//		});

		String systemFilePath = System.getProperty("user.dir") + File.separator + "system.xml";

		String[] clf = Utilities.getEnvFont();
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		} catch(Throwable th) {
			System.err.println(th);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));

		this.setIconImage(SandboxParameters.sandboxImage);
		
		JPanel paramsPanel = new JPanel(new GridLayout(sandboxParameters.numRows, 1));
		for (int i = 1; i <= sandboxParameters.numParameters; i++) {
			paramsPanel.add((Component) sandboxParameters.parameterList.get(i));
		}

		JScrollPane scrollPaneOutput = new JScrollPane(sandboxParameters.textAreaOutput);
		JScrollPane scrollPanePad = new JScrollPane(sandboxParameters.scratchPad);

		sandboxLogger.fine("Getting tools and tests");
		sandboxParameters.tests = Utilities.getTests(sandboxParameters);
		sandboxLogger.fine("Got tools and tests");
		List<SandboxTestBase> tests = new ArrayList<SandboxTestBase>();
		List<SandboxTestBase> tools = new ArrayList<SandboxTestBase>();
		for (SandboxTestBase test : sandboxParameters.tests) {
			if (test.getTestName().startsWith("Tool")) {
				tools.add(test);
			} else if (test.getTestName().startsWith("Test")) {
				tests.add(test);
			} else {
				tools.add(test);
			}
		}
//		for (SandboxTestBase test : sandboxParameters.tests) {
//			if (test.getTestName().startsWith("Tool")) {
//				tools.add(test);
//			}
//			if (test.getTestName().startsWith("Test")) {
//				tests.add(test);
//			}
//		}
		String[] testDescriptions = new String[tests.size()];
		int testIndex = 0;
		for (SandboxTestBase test : tests) {
			testDescriptions[testIndex++] = test.getTestName() + ": " + test.getDescription();
		}
		sandboxParameters.listTestDescriptions = new SandboxList(testDescriptions);
		JScrollPane scrollPaneTests = new JScrollPane(sandboxParameters.listTestDescriptions);
		scrollPaneTests.setName("Tests");
//		sandboxParameters.listTestDescriptions.addKeyListener(new SandboxListKeyListener(sandboxParameters));
		sandboxParameters.listTestDescriptions.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent event) {
				
				int keyCode = event.getKeyCode();

				// Function key events
				switch (keyCode) {
				case KeyEvent.VK_ENTER:
					try {
						SandboxList sandboxList = (SandboxList) event.getComponent();
						String selectedText = (String) sandboxList.getSelectedValue();
						String[] selects = selectedText.split(": ");
						String selectedTestName = selects[0];
						javax.swing.JButton button = sandboxParameters.buttons
								.get(SandboxParameters.BUTTON_PREFIX + selectedTestName);
						SandboxTestBase test = sandboxParameters.getTest(selectedTestName);
						if (!test.isReady) {
							test.setReady(true);
							button.doClick();
						} else {
							test.setParameters(sandboxParameters);
							sandboxParameters.unhighlightParameters();
							sandboxParameters.highlightParameters(test.getParameterDescriptions());
						}
					} catch (Exception en) {
						sandboxParameters.printLine("Key click exception in list", true, true);
					}
					break;
				default:
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});

//		sandboxParameters.listTestDescriptions.addKeyListener(sandboxParameters.keyListener);
//		sandboxParameters.listTestDescriptions.addFocusListener(sandboxParameters.focusListener);

		String[] toolDescriptions = new String[tools.size()];
		int toolIndex = 0;
		for (SandboxTestBase tool : tools) {
			toolDescriptions[toolIndex++] = tool.getTestName() + ": " + tool.getDescription();
		}
		sandboxParameters.listToolDescriptions = new SandboxList(toolDescriptions);
		JScrollPane scrollPaneTools = new JScrollPane(sandboxParameters.listToolDescriptions);
		scrollPaneTools.setName("Tools");
		sandboxParameters.listToolDescriptions.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent event) {
				
				int keyCode = event.getKeyCode();

				// Function key events
				switch (keyCode) {
				case KeyEvent.VK_ENTER:
					try {
						SandboxList sandboxList = (SandboxList) event.getComponent();
						String selectedText = (String) sandboxList.getSelectedValue();
						String[] selects = selectedText.split(": ");
						String selectedTestName = selects[0];
						javax.swing.JButton button = sandboxParameters.buttons
								.get(SandboxParameters.BUTTON_PREFIX + selectedTestName);
						SandboxTestBase test = sandboxParameters.getTest(selectedTestName);
						if (!test.isReady) {
							test.setReady(true);
							button.doClick();
						} else {
							test.setParameters(sandboxParameters);
							sandboxParameters.unhighlightParameters();
							sandboxParameters.highlightParameters(test.getParameterDescriptions());
						}
					} catch (Exception en) {
						sandboxParameters.printLine("Key click exception in list", true, true);
					}
					break;
				default:
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
//		sandboxParameters.listToolDescriptions.addKeyListener(new SandboxListKeyListener(sandboxParameters));

		String osName = System.getProperty("os.name", "Windows").split(" ")[0];
		sandboxParameters.commandsDirectory = sandboxParameters.userDir + File.separator + SandboxParameters.SANDBOX_COMMAND_DIRECTORY
				+ File.separator + osName;
		sandboxParameters.commandsDirectory = System.getProperty(Command.COMMANDS_DIR_ENV, sandboxParameters.commandsDirectory);

		File directory = new File(sandboxParameters.commandsDirectory);
		Collection<File> files = sandbox.Utilities.listFilesDirs(directory, null, false);
		String[] commandDescriptions = null;
		sandboxParameters.printLine("Pulling commands from " + sandboxParameters.commandsDirectory);
		if (files == null) {
			// TODO Add code in this area that elegantly handles the absence of
			// a commands directory in the workspace
			System.err.println("There are no commands in " + sandboxParameters.commandsDirectory);
			JOptionPane.showMessageDialog(this, "There are no commands in " + sandboxParameters.commandsDirectory);
			System.exit(-1);
		} else {
			commandDescriptions = new String[files.size()];
		}
		int commandIndex = 0;
		for (File commandFile : files) {
			String description = "No description";
			try {
				description = Utilities.getFileString(commandFile, 200);
				description = description.replaceAll("\n", " :: ");
			} catch (IOException ioe) {
				description = "Could not get description";
			}
			commandDescriptions[commandIndex++] = commandFile.getName() + ": " + description;
		}
		int size = commandDescriptions.length;
		List<String> arrayList = new ArrayList<String>();
		for (String commandDescription : commandDescriptions) {
			arrayList.add(commandDescription);
		}
		Collections.sort(arrayList);
		sandboxParameters.listCommandDescriptions = new SandboxList(arrayList.toArray());
		JScrollPane scrollPaneCommands = new JScrollPane(sandboxParameters.listCommandDescriptions);
		scrollPaneCommands.setName("Commands");
		sandboxParameters.listCommandDescriptions.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent event) {
				
				int keyCode = event.getKeyCode();

				// Function key events
				switch (keyCode) {
				case KeyEvent.VK_ENTER:
					try {
						SandboxList commandList = (SandboxList) event.getComponent();
						String selectedText = (String) commandList.getSelectedValue();
						String[] selects = selectedText.split(": ");
						String selectedCommandName = selects[0];
						javax.swing.JButton button = (javax.swing.JButton) sandboxParameters.buttons
								.get(SandboxParameters.BUTTON_PREFIX + Command.class.getSimpleName());
						Command command = (Command) sandboxParameters.getTest(Command.class.getSimpleName());
						command.setReady(true);
						button.doClick();
//
//						sandboxParameters.commandName = selectedCommandName;
//						command.setParameters(sandboxParameters);
//						sandboxParameters.unhighlightParameters();
//						sandboxParameters.highlightParameters(command.getParameterDescriptions());
					} catch (Exception en) {
						sandboxParameters.printLine("Key click exception in list", true, true);
					}
					break;
				default:
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});

//		sandboxParameters.listCommandDescriptions.addKeyListener(sandboxParameters.keyListener);
//		sandboxParameters.listCommandDescriptions.addKeyListener(new SandboxListKeyListener(sandboxParameters));
//		sandboxParameters.listCommandDescriptions.addFocusListener(sandboxParameters.focusListener);

		sandboxParameters.newTab(toolDescriptions.length + " Tools", scrollPaneTools, "Tools",
				sandboxParameters.iconWatch);
		sandboxParameters.newTab(testDescriptions.length + " Tests", scrollPaneTests, "Tests",
				sandboxParameters.iconWatch);
		sandboxParameters.newTab(commandDescriptions.length + " Commands", scrollPaneCommands, "Commands",
				sandboxParameters.iconWatch);
		sandboxParameters.newTab(sandboxParameters.textAreaOutput.getText().split(" \n", 10000).length + " Lines",
				scrollPaneOutput, "Output", sandboxParameters.iconWatch);
		sandboxParameters.newTab(sandboxParameters.scratchPad.getText().split(" \n", 10000).length + " Lines",
				scrollPanePad, "Scratch", sandboxParameters.iconWatch);
		sandboxParameters.lastSelectedTab = SandboxParameters.LAST_STATIC_TAB;
		sandboxParameters.selectTab(SandboxParameters.LAST_STATIC_TAB);

		JPanel bottomPanel = new JPanel(new BorderLayout());
		int numRows = 1 + (sandboxParameters.tests.size() / sandboxParameters.numButtonsPerRow);
		JPanel buttonPanel = new JPanel(new GridLayout(numRows, 6, 2, 3));
		Iterator<SandboxTestBase> iterTests = sandboxParameters.tests.iterator();
		while (iterTests.hasNext()) {
			SandboxTestBase test = iterTests.next();
			JButton button = new JButton();
			String buttonName = SandboxParameters.BUTTON_PREFIX + test.getTestName();
			button.setName(buttonName);
			button.setFont(textFont);
			sandboxParameters.buttons.put(SandboxParameters.BUTTON_PREFIX + test.getTestName(), button);
			Annotation annotation = test.getClass().getAnnotation(SandboxAnnotation.class);
			if (annotation == null || ((SandboxAnnotation) annotation).showInButtonPanel()) {
				buttonPanel.add(button);
			}
		}
		
		boolean showButtonPanel = Boolean.valueOf(System.getProperty("sbx.show.button.panel", "true"));
		if (showButtonPanel) {
			bottomPanel.add(buttonPanel, BorderLayout.NORTH);
		}

		bottomPanel.add(sandboxParameters.componentPanel, BorderLayout.SOUTH);

		sandboxParameters.alert("Started up " + Utilities.getTime());

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(paramsPanel, BorderLayout.NORTH);
		panel.add(sandboxParameters.tabbedPane, BorderLayout.CENTER);
		panel.add(bottomPanel, BorderLayout.SOUTH);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu, saveMenu, parametersMenu, configurationMenu = null;
		JMenuItem menuItemFollowList, menuItemFollow, menuItemFileOpen, menuItemFileRaw, menuItemFileUnlock, menuItemFilelock,
				menuItemFilePath, menuItemFileOpenList, menuItemFileEdit, menuItemFileEditList, menuItemFileGet, menuItemFileOpenHistory,
				menuItemFileSaveTab, menuItemFileSaveTabAs, menuItemSaveHistoryTab, menuItemRefresh, menuItemExit,
				menuItemExitTo, menuItemShell, menuItemChiclet, menuItemStart, menuItemRestoreConfiguration, menuItemSaveConfiguration,
				menuItemSaveAsConfiguration, menuItemParametersDelete, menuItemParametersDeleteAll, menuItemHelp,
				menuItemParametersLoad, menuItemParametersGoTo, menuItemParametersMakeDir, menuItemParametersTest = null;
		FlowLayout menuBarLayout = new FlowLayout(FlowLayout.LEFT);
		menuBar.setLayout(menuBarLayout);
		this.setJMenuBar(menuBar);

		fileMenu = new JMenu("File");
		fileMenu.setFont(textFont);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
		menuItemFilePath = new JMenuItem("Path...");
		menuItemFilePath.setMnemonic(KeyEvent.VK_P);
		fileMenu.add(menuItemFilePath);

		fileMenu.addSeparator(); // Why doesn't this work?
		menuItemFileOpen = new JMenuItem("Open");
		menuItemFileOpen.setMnemonic(KeyEvent.VK_O);
		fileMenu.add(menuItemFileOpen);
		menuItemFileOpenList = new JMenuItem("Open...");
		fileMenu.add(menuItemFileOpenList);
		menuItemFileEdit = new JMenuItem("Edit");
		menuItemFileEdit.setMnemonic(KeyEvent.VK_E);
		fileMenu.add(menuItemFileEdit);
		menuItemFileEditList = new JMenuItem("Edit...");
		fileMenu.add(menuItemFileEditList);
		menuItemFileGet = new JMenuItem("Get");
		menuItemFileGet.setMnemonic(KeyEvent.VK_G);
		fileMenu.add(menuItemFileGet);
		menuItemParametersMakeDir = new JMenuItem("Make Directory");
		menuItemParametersMakeDir.setMnemonic(KeyEvent.VK_G);
		fileMenu.add(menuItemParametersMakeDir);		
		
		menuItemFileOpenHistory = new JMenuItem("Open History...");
		fileMenu.add(menuItemFileOpenHistory);
		menuItemFollow = new JMenuItem("Follow");
		fileMenu.add(menuItemFollow);
		menuItemFollowList = new JMenuItem("Follow...");
		fileMenu.add(menuItemFollowList);
		menuItemFileRaw = new JMenuItem("Raw");
		fileMenu.add(menuItemFileRaw);
		menuItemFilelock = new JMenuItem("Lock");
		fileMenu.add(menuItemFilelock);
		menuItemFileUnlock = new JMenuItem("Unlock");
		fileMenu.add(menuItemFileUnlock);
		saveMenu = new JMenu("Save");
		saveMenu.setFont(textFont);
		saveMenu.setMnemonic(KeyEvent.VK_S);
		menuBar.add(saveMenu);
		menuItemFileSaveTab = new JMenuItem("Save Tab");
		menuItemFileSaveTab.setMnemonic(KeyEvent.VK_T);
		saveMenu.add(menuItemFileSaveTab);
		menuItemFileSaveTabAs = new JMenuItem("Save Tab As...");

		saveMenu.add(menuItemFileSaveTabAs);

		menuItemSaveHistoryTab = new JMenuItem("Save History");
		saveMenu.add(menuItemSaveHistoryTab);
		menuItemRefresh = new JMenuItem("Refresh");
		saveMenu.add(menuItemRefresh);
		menuItemExit = new JMenuItem("Exit");
		saveMenu.add(menuItemExit);
		menuItemExitTo = new JMenuItem("Exit to...");
		saveMenu.add(menuItemExitTo);

		menuItemStart = new JMenuItem("Start");
		menuItemStart.setMnemonic(KeyEvent.VK_S);
		saveMenu.add(menuItemStart);
		menuItemShell = new JMenuItem("Shell");
		saveMenu.add(menuItemShell);
		menuItemChiclet = new JMenuItem("Chiclet");
		saveMenu.add(menuItemChiclet);

		configurationMenu = new JMenu("Config");
		configurationMenu.setMnemonic(KeyEvent.VK_C);
		configurationMenu.setFont(textFont);
		menuBar.add(configurationMenu);
		menuItemSaveConfiguration = new JMenuItem("Save");
		menuItemSaveConfiguration.setMnemonic(KeyEvent.VK_S);
		configurationMenu.add(menuItemSaveConfiguration);
		menuItemSaveAsConfiguration = new JMenuItem("Save as ...");
		configurationMenu.add(menuItemSaveAsConfiguration);
		menuItemRestoreConfiguration = new JMenuItem("Restore from...");
		configurationMenu.add(menuItemRestoreConfiguration);

		cbMenuItemSaveOnExit = new JCheckBoxMenuItem("Save on exit ", true);

		configurationMenu.add(cbMenuItemSaveOnExit);

		parametersMenu = new JMenu("Parameters");
		parametersMenu.setMnemonic(KeyEvent.VK_P);
		parametersMenu.setFont(textFont);
		menuBar.add(parametersMenu);

		menuItemParametersDelete = new JMenuItem("Delete");
		parametersMenu.add(menuItemParametersDelete);
		menuItemParametersDeleteAll = new JMenuItem("Delete all");
		parametersMenu.add(menuItemParametersDeleteAll);
		menuItemParametersLoad = new JMenuItem("Load");
		parametersMenu.add(menuItemParametersLoad);
		menuItemParametersGoTo = new JMenuItem("Go to...");
		parametersMenu.add(menuItemParametersGoTo);
		menuItemParametersTest = new JMenuItem("Test");
		parametersMenu.add(menuItemParametersTest);
		menuItemHelp = new JMenuItem("Help");
		parametersMenu.add(menuItemHelp);

		JPanel popAnnouncePanel = new JPanel();
		popAnnouncePanel.setLayout(new BorderLayout());
		sandboxParameters.showPopPad.setFont(textFont);
		popAnnouncePanel.add(sandboxParameters.showPopPad, BorderLayout.NORTH);
		sandboxParameters.announce.setEnabled(false);
		sandboxParameters.announce.setFont(textFont);
		popAnnouncePanel.add(sandboxParameters.announce, BorderLayout.SOUTH);
		menuBar.add(popAnnouncePanel);
		sandboxParameters.announce.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox announce = (JCheckBox) e.getSource();
				Component component = sandboxParameters.getSelectedTabComponent();
				if (component instanceof SandboxTextArea) {
					SandboxTextArea textArea = (SandboxTextArea) component;
					textArea.getTest().announce = announce.isSelected();
				} else {
					sandboxParameters.announce.setEnabled(false);
				}
			}
		});

		sandboxParameters.passwordComponent.setColumns(5);
		JPanel passClockPanel = new JPanel();
		passClockPanel.setFont(textFont);
		passClockPanel.setLayout(new BorderLayout());
		sandboxParameters.clock.setFont(textFont);
		passClockPanel.add(sandboxParameters.clock, BorderLayout.SOUTH);
		passClockPanel.add(sandboxParameters.passwordComponent, BorderLayout.NORTH);
		menuBar.add(passClockPanel);

		// Add password field
		JPanel booleanPanel = new JPanel();
		booleanPanel.setLayout(new BorderLayout());
		sandboxParameters.trueFalse.setFont(textFont);
		booleanPanel.add(sandboxParameters.trueFalse, BorderLayout.NORTH);
		sandboxParameters.yesNo.setFont(textFont);
		booleanPanel.add(sandboxParameters.yesNo, BorderLayout.SOUTH);
		menuBar.add(booleanPanel);
		buttonCancel.setFont(textFont);
		menuBar.add(buttonCancel);

		JComboBox<String> replaceComboBox = sandboxParameters.replaceComboBox;
		replaceComboBox.setToolTipText("REPLACE");
		replaceComboBox.setEditable(true);
		replaceComboBox.setName(SandboxParameters.REPLACE_COMBOBOX_NAME);

		replaceComboBox.addActionListener(sandboxParameters.cbActionListener);
		replaceComboBox.addKeyListener(sandboxParameters.keyListener);
//		replaceComboBox.getEditor().getEditorComponent().addKeyListener(sandboxParameters.keyListener);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_BASE64);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_TEXT);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_TEST);
		
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_TIME);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_FIND);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_SET);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_UNSET);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_FUNCTION_UNSET_EXAMPLE);
		replaceComboBox.addItem(SandboxParameters.SANDBOX_COMMAND_WORKSPACE);
		
		JComboBox<String> searchComboBox = sandboxParameters.searchComboBox;
		searchComboBox.addItem(SandboxParameters.SANDBOX_HOME);
		searchComboBox.addItem(SandboxParameters.SANDBOX_DIRECTORY);
		searchComboBox.addItem(SandboxParameters.SANDBOX_WORKSPACE);
		searchComboBox.addItem(SandboxParameters.SANDBOX_CMD_DIR);
		searchComboBox.addItem(SandboxParameters.SANDBOX_CLASSPATH);
		searchComboBox.addItem(SandboxParameters.SANDBOX_PASSWORD);
		searchComboBox.addItem(SandboxParameters.SANDBOX_NULL);
		searchComboBox.addItem(SandboxParameters.SANDBOX_CLIPBOARD);

		searchComboBox.addItem(SandboxParameters.SANDBOX_BLANK);
		searchComboBox.addItem(SandboxParameters.SANDBOX_EMPTY);
		searchComboBox.addItem(SandboxParameters.SANDBOX_END_OF_FILE);
		searchComboBox.addItem(SandboxParameters.SANDBOX_COMBOBOX);
		searchComboBox.addItem(SandboxParameters.SANDBOX_JAVA_ENV_PROP_EXAMPLE);
		searchComboBox.addItem(SandboxParameters.SANDBOX_JAVA_LITERAL_EXAMPLE);
		searchComboBox.addItem(SandboxParameters.DATE_FORMAT);

		searchComboBox.addItem(SandboxParameters.SANDBOX_COMBOBOX_COMMENT_START + " comment "
				+ SandboxParameters.SANDBOX_COMBOBOX_COMMENT_STOP);
		searchComboBox.addItem(SandboxParameters.SBX_CONNECT_TIMEOUT_ENV_NAME);
		searchComboBox.addItem(SandboxParameters.SBX_PHONE_CALL_DURATION);
		searchComboBox.addItem(SandboxParameters.SBX_MSECONDS_OPEN_PHONE_CALL);
		
		searchComboBox.addItem(SandboxTestBase.SANDBOX_ALERT_ENV);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_POST_PROCESS_ALERT_ENV);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_EXCLUDE_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_CONFIRM_COMMAND_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_CONFIRM_DIRECTORY_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_ADVICE_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_TEST_ANNOUNCE_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_TEST_ENCLOSER_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_TEST_SETTING_NOTIFY_PREFIX);
		searchComboBox.addItem(SandboxTestBase.SANDBOX_TEST_SETTING_NOTIFY_PROFILE_PREFIX);
		
		searchComboBox.addItem(SmartOpen.BROWSER_REPLACEMENT);
		searchComboBox.addItem(SmartOpen.ADDRESS_REPLACEMENT);
		searchComboBox.addItem(SmartOpen.START_SANDBOX_FLAG);
		searchComboBox.addItem(Utilities.TAB_LENGTH_MAX);
		searchComboBox.addItem(Command.COMMANDS_DIR_ENV);
		searchComboBox.addItem(Utilities.CHICLET_SIZE_PAR_NAME);

		searchComboBox.setEditable(true);
		searchComboBox.setName(SandboxParameters.SEARCH_COMBOBOX_NAME);
		Component searchComponent = searchComboBox.getEditor().getEditorComponent();
		searchComponent.setName(SandboxParameters.SEARCH_COMBOBOX_NAME);
//		searchComponent.addFocusListener(sandboxParameters.focusListener);

		searchComboBox.addActionListener(sandboxParameters.cbActionListener);
		searchComboBox.addKeyListener(sandboxParameters.keyListener);
//		searchComboBox.getEditor().getEditorComponent().addKeyListener(sandboxParameters.keyListener);

		JPanel comboPanel = new JPanel();
		comboPanel.setLayout(new BorderLayout());
		comboPanel.add(replaceComboBox, BorderLayout.SOUTH);
		comboPanel.add(sandboxParameters.searchComboBox, BorderLayout.NORTH);
		menuBar.add(comboPanel);

		JLabel labelId = new JLabel(Utilities.base64Decode(SandboxParameters.ID));
		JLabel labelVersion = new JLabel("Version" + SandboxParameters.VERSION);
		JPanel versionPanel = new JPanel();

		versionPanel.setLayout(new BorderLayout());
		versionPanel.add(labelVersion, BorderLayout.NORTH);
		versionPanel.add(labelId, BorderLayout.SOUTH);
		menuBar.add(versionPanel);
		buttonSave.setFont(textFont);
		menuBar.add(buttonSave);
		buttonPhone.setFont(textFont);
		menuBar.add(buttonPhone);
//		buttonChiclet.setFont(textFont);
//		menuBar.add(buttonChiclet);

		sandboxParameters.listToolDescriptions.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				handleTestListMouseClicks(e);
			}
		});

		sandboxParameters.listTestDescriptions.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				handleTestListMouseClicks(e);
			}
		});

		sandboxParameters.listCommandDescriptions.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				handleCommandListMouseClicks(e);
			}
		});

		menuItemFollowList.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFollowList_actionPerformed(e);
			}
		});

		menuItemFollow.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFollow_actionPerformed(e);
			}
		});

		menuItemShell.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuShell_actionPerformed(e);
			}
		});

		menuItemChiclet.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemChiclet_actionPerformed(e);
			}
		});

		menuItemFileOpen.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileOpen_actionPerformed(e);
			}
		});
		menuItemFileRaw.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileRaw_actionPerformed(e);
			}
		});
		menuItemFilelock.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFilelock_actionPerformed(e);
			}
		});
		menuItemFileUnlock.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileUnlock_actionPerformed(e);
			}
		});
		menuItemFileEdit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileEdit_actionPerformed(e);
			}
		});
		menuItemFileEditList.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileEditList_actionPerformed(e);
			}
		});
		menuItemFileGet.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileGet_actionPerformed(e);
			}
		});
		menuItemParametersMakeDir.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersMakeDir_actionPerformed(e);
			}
		});
		menuItemFilePath.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFilePath_actionPerformed(e);
			}
		});
		menuItemFileOpenList.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileOpenList_actionPerformed(e);
			}
		});

		menuItemFileOpenHistory.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuFileOpenHistory_actionPerformed(e);
			}
		});

		menuItemFileSaveTab.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemFileSaveTab_actionPerformed(e);
			}
		});

		menuItemFileSaveTabAs.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemFileSaveTabAs_actionPerformed(e);
			}
		});

		menuItemRefresh.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemRefresh_actionPerformed(e);
			}
		});

		menuItemExit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemExit_actionPerformed(e);
			}
		});

		menuItemExitTo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemExitTo_actionPerformed(e);
			}
		});

		menuItemStart.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemStart_actionPerformed(e);
			}
		});

		menuItemSaveHistoryTab.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemSaveHistoryTab_actionPerformed(e);
			}
		});

		menuItemRestoreConfiguration.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuRestoreConfiguration_actionPerformed(e);
			}
		});

		menuItemSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemSaveConfiguration_actionPerformed(e);
			}
		});

		menuItemSaveAsConfiguration.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemSaveAsConfiguration_actionPerformed(e);
			}
		});

		menuItemParametersDelete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersDelete_actionPerformed(e);
			}
		});

		menuItemParametersDeleteAll.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersDeleteAll_actionPerformed(e);
			}
		});

		menuItemParametersLoad.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersLoad_actionPerformed(e);
			}
		});

		menuItemParametersGoTo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersGoTo_actionPerformed(e);
			}
		});

		menuItemParametersTest.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemParametersTest_actionPerformed(e);
			}
		});

		menuItemHelp.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuItemHelp_actionPerformed(e);
			}
		});

		frameEdit.setSize(500, 500);

		this.getContentPane().add(panel);
		this.setDefaultCloseOperation(SandboxFrameBase.DISPOSE_ON_CLOSE);
		this.pack();
		this.setSize(new Dimension(600, 400));

		String colorInHex = "ff0000";
		java.awt.Color color = new java.awt.Color(java.lang.Integer.parseInt(colorInHex, 16));
		frameEdit.setBackground(color);
		sandboxParameters.isInitialized = true;
		
		setAlwaysOnTop(Boolean.valueOf(System.getProperty(SandboxFrameBase.ALWAYS_ON_TOP_VAR_NAME, "false")));
		
		SandboxParameters.frame = this;
	}
	// END OF CONSTRUCTOR

	public static void phoneCall(String message, String title) {
		try {
			SandboxParameters.frame.setAlwaysOnTop(true);
			SandboxParameters.frame.setState(Frame.MAXIMIZED_BOTH);
			if (SandboxParameters.frame.getState() == Frame.ICONIFIED) {
				SandboxParameters.frame.setState(Frame.NORMAL);
			}

			int timeOpen = Integer.valueOf(System.getProperty(SandboxParameters.SBX_MSECONDS_OPEN_PHONE_CALL, "1000"));
			Timer clockTimer = new Timer(timeOpen, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SandboxParameters.frame.setAlwaysOnTop(false);
					SandboxParameters.frame.setState(Frame.NORMAL);
				}
			});
			clockTimer.setRepeats(false);
			clockTimer.start();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void handleTestListMouseClicks(java.awt.event.MouseEvent e) {
		try {
			SandboxList testList = (SandboxList) e.getComponent();
			String selectedText = (String) testList.getSelectedValue();
			String[] selects = selectedText.split(": ");
			String selectedTestName = selects[0];
			sandboxLogger.fine("test = " + selectedTestName);
			javax.swing.JButton button = sandboxParameters.buttons
					.get(SandboxParameters.BUTTON_PREFIX + selectedTestName);
			SandboxTestBase test = sandboxParameters.getTest(selectedTestName);
			if (e.getClickCount() == 2) {
				test.setReady(true);
				button.doClick();
			} else {
				test.setParameters(sandboxParameters);
				sandboxParameters.unhighlightParameters();
				sandboxParameters.highlightParameters(test.getParameterDescriptions());
			}
		} catch (Exception en) {
			sandboxParameters.printLine("Mouse click exception in test list", true, true);
		}
	}

	void handleCommandListMouseClicks(java.awt.event.MouseEvent e) {
		try {
			SandboxList commandList = (SandboxList) e.getComponent();
			String selectedText = (String) commandList.getSelectedValue();
			String[] selects = selectedText.split(": ");
			String selectedCommandName = selects[0];
			javax.swing.JButton button = (javax.swing.JButton) sandboxParameters.buttons
					.get(SandboxParameters.BUTTON_PREFIX + Command.class.getSimpleName());
			Command command = (Command) sandboxParameters.getTest(Command.class.getSimpleName());
			if (e.getClickCount() == 2) {
				command.setReady(true);
				button.doClick();
			} else {
				sandboxParameters.commandName = selectedCommandName;
				command.setParameters(sandboxParameters);
				sandboxParameters.unhighlightParameters();
				sandboxParameters.highlightParameters(command.getParameterDescriptions());
			}
		} catch (Exception en) {
			sandboxParameters.printLine("Mouse click exception in command list", true, true);
		}
	}

	private String getStartDirectory() {
		String startDir = getFocusedText();
		File dirFile = new File(startDir);
		if (!dirFile.exists()) {
			startDir = System.getProperty("user.dir");
			sandboxParameters.printLine("Directory does not exist: " + startDir);
		}
		return startDir;
	}

	private String getFocusedText() {
		return sandboxParameters.replaceValues(sandboxParameters.getFocusedText());
	}

	void menuFilePath_actionPerformed(ActionEvent e) {
		try {
			String startDir = getStartDirectory();
			JFileChooser chooser = new JFileChooser(startDir);
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setFileHidingEnabled(false);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				if (Utilities.isWindows() || file.isFile()) {
					String filePath = file.getPath();
					
/*///////////////////////////DMH
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					}
/*///////////////////////////
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					} else if (filePath.startsWith(sandboxParameters.configurationDirectoryRoot)) {
						filePath = SandboxParameters.SANDBOX_HOME
								+ filePath.substring(sandboxParameters.configurationDirectoryRoot.length());
					}
//*///////////////////////////
					
					
					//	new SandboxBasket<String, String>(SandboxParameters.SANDBOX_HOME, this.configurationDirectoryRoot));

					
					sandboxParameters.setFocusedText(filePath);
					sandboxParameters.globals.currentDirectory = file.getPath();
				} else {
					String filePath = file.getParentFile().getPath();
					
/*///////////////////////////DMH
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					}
/*///////////////////////////
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					} else if (filePath.startsWith(sandboxParameters.configurationDirectoryRoot)) {
						filePath = SandboxParameters.SANDBOX_HOME
								+ filePath.substring(sandboxParameters.configurationDirectoryRoot.length());
					} 
//*///////////////////////////
					sandboxParameters.setFocusedText(filePath);
					sandboxParameters.globals.currentDirectory = file.getParentFile().getPath();
				}
			} else {
				sandboxParameters.printLine("Open command cancelled by user.", true, true);
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

	void menuFollowList_actionPerformed(ActionEvent e) {
		String filePath = getStartDirectory();
		sandboxLogger.info("Following " + filePath);
		try {
			JFileChooser chooser = new JFileChooser(filePath);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();

//			int retVal = chooser.showOpenDialog(this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				sandboxParameters.setFocusedText(file.getPath());
				menuFollow_actionPerformed(e);
			} else {
				sandboxParameters.printLine("Follow command cancelled by user.", true, true);
			}
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			sandboxParameters.printLine("Not opening new tab: " + aioobe.getMessage());
		} catch (Exception en) {
			sandboxParameters.printLine("Not opening new tab: " + en.getMessage());
		}
	}

	void menuFollow_actionPerformed(ActionEvent e) {
		try {
			String filePath = this.getFocusedText();
			File file = new File(filePath);
			if (!file.exists() || file.isDirectory()) {
				sandboxParameters.printLine("File does not exist:");
				sandboxParameters.printLine(filePath);
				return;
			}
//			String filePath = getFocusedText();
			sandboxLogger.info("Following " + filePath);

			SandboxLogger thread = new SandboxLogger();
			SandboxTextArea textArea = new SandboxTextArea(thread);

			String idForTab = "Follow:" + tabId++;
			textArea.setName(idForTab);
			textArea.addKeyListener(sandboxParameters.keyListener);
			JScrollPane scrollPanePlain = new JScrollPane(textArea);
			scrollPanePlain.setName(idForTab);
			sandboxParameters.tabbedPane.add(scrollPanePlain);
			sandboxParameters.tabbedPane.setToolTipTextAt(sandboxParameters.getTabbedPane().getTabCount() - 1,
					filePath);
			sandboxParameters.tabbedPane.setIconAt(sandboxParameters.getTabbedPane().getTabCount() - 1,
					sandboxParameters.iconRunning);

			textArea.addFocusListener(sandboxParameters.getFocusListener());
			sandboxParameters.tabbedPane.setSelectedIndex(sandboxParameters.getTabbedPane().getTabCount() - 1);

			thread.setWindowSize(1000);
			thread.setFilePath(filePath);
			thread.setTextArea(textArea);
			thread.start();
			sandboxParameters.addToGlobalMap(idForTab, thread);
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			sandboxParameters.printLine("Not opening new tab: " + aioobe.getMessage());
		} catch (Exception en) {
			sandboxParameters.printLine("Not opening new tab: " + en.getMessage());
		}

	}

	void menuShell_actionPerformed(ActionEvent e) {
		sandboxLogger.info("Opening shell");

		try {
			SandboxShell shellThread = new SandboxShell();
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			sandboxParameters.printLine("Not opening new tab: " + aioobe.getMessage());
		} catch (Exception en) {
			sandboxParameters.printLine("Not opening new tab: " + en.getMessage());
		}
	}

	void menuFileRaw_actionPerformed(ActionEvent e) {
		String resource = getFocusedText();
		try {
			sandboxParameters.alert(resource, sandboxParameters.iconRunning);
			sandboxParameters.newTab(resource, Utilities.getFileString(resource),
					"Raw-" + sandboxParameters.getTabbedPane().getTabCount());
		} catch (Exception en) {
			sandboxParameters.alert("Could not open raw file: " + resource, sandboxParameters.iconStopped);
			sandboxParameters.printLine("Could not open raw file:");
			sandboxParameters.printLine(resource);
			sandboxLogger.log(Level.ALL, "Could not open raw file: " + resource, en);
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
			sandboxParameters.alert(resource);
		}
	}

	void menuFilelock_actionPerformed(ActionEvent e) {
		try {
			String[] resources = Utilities.noNulls(getFocusedText()).split(System.getProperty("line.separator", "\n"));
			for (String resource : resources) {
				File file = new File(resource);
				if (file.isFile()) {
					file.setWritable(false);
				} else {
					sandboxParameters.printLine("Could not lock because not a file", true, true);
				}
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("UNLOCK DONE", true, true);
		}
	}

	void menuFileUnlock_actionPerformed(ActionEvent e) {
		try {
			String[] resources = Utilities.noNulls(getFocusedText()).split(System.getProperty("line.separator", "\n"));
			for (String resource : resources) {
				File file = new File(resource);
				if (file.isFile()) {
					file.setWritable(true);
				} else {
					sandboxParameters.printLine("Could not unlock because not a file", true, true);
				}
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("UNLOCK DONE", true, true);
		}
	}

	void menuFileOpen_actionPerformed(ActionEvent e) {
		try {
			String selectedText = Utilities.noNulls(getFocusedText());
			String[] resources = sandboxParameters.replaceValues(selectedText).split("\n");
			List<String> list = new ArrayList<String>(resources.length);
			for (String string : resources) {
				list.add(string);
			}
			SandboxOpen sandboxOpen = new SandboxOpen(sandboxParameters, list);
			Thread thread = new Thread(sandboxOpen);
			thread.setName("Opening " + list.size() + " files ...");
			thread.start();
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

	void menuFileEdit_actionPerformed(ActionEvent e) {
		try {
			String[] resources = Utilities.noNulls(getFocusedText())
					.split(System.getProperty("line.separator", "\n"));
			for (String resource : resources) {
				SandboxOpen sandboxOpen = new SandboxOpen(sandboxParameters, resource, true);
				sandboxOpen.setName("Editing " + resource);
				sandboxOpen.start();
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

	void menuFileGet_actionPerformed(ActionEvent e) {
		try {
			String[] resources = Utilities.noNulls(getFocusedText())
					.split(System.getProperty("line.separator", "\n"));
			sandboxParameters.alert("Getting resources ...", sandboxParameters.iconRunning);
			for (String resource : resources) {
				File original = new File(resource);
				File newFile = new File(sandboxParameters.configurationDirectory + File.separator + original.getName());
				if (original.exists() && !newFile.exists()) {
					Utilities.copyFile(original, new File(sandboxParameters.configurationDirectory + File.separator + original.getName()));
					sandboxParameters.alert("Got resources");
				} else {
					sandboxParameters.alert("Could not copy: Original exists-" + original.exists() + " Target exists-" + newFile.exists());
					sandboxParameters.printLine("Could not copy: Original exists-" + original.exists() + " Target exists-" + newFile.exists(), true, false);
				}
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

	
	
	
//*////////////////////////////////DMH	
	void menuItemParametersMakeDir_actionPerformed(ActionEvent e) {
		try {
			String strDir = Utilities.noNulls(getFocusedText());
			File dir = new File(strDir);
			boolean success = dir.mkdir();
			if (success) {
				sandboxParameters.alert(dir.getPath());
			} else {
				sandboxParameters.alert(dir.getPath(), sandboxParameters.iconStopped);
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("MAKE DIRECTORY DONE", true, true);
		}
	}
//*///////////////////////////////////	

	
	
	
	
	
	void menuFileEditList_actionPerformed(ActionEvent e) {
		try {
			String startDir = getStartDirectory();
			JFileChooser chooser = new JFileChooser(startDir);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setFileHidingEnabled(false);
			chooser.setMultiSelectionEnabled(true);
//			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();

			if (retVal == JFileChooser.APPROVE_OPTION) {
				File[] files = chooser.getSelectedFiles();
				for (File file : files) {
					String filePath = file.getPath();
					
					
					//	new SandboxBasket<String, String>(SandboxParameters.SANDBOX_HOME, this.configurationDirectoryRoot));
//*//////////////DMH
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					} else if (filePath.startsWith(sandboxParameters.configurationDirectoryRoot)) {
						filePath = SandboxParameters.SANDBOX_HOME
								+ filePath.substring(sandboxParameters.configurationDirectoryRoot.length());
					}
/*/////////////////
					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					}
//*/////////////////
					sandboxParameters.setFocusedText(filePath);
					menuFileEdit_actionPerformed(e);
				}
			} else {
				sandboxParameters.printLine("Open command cancelled by user.", true, true);
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

/*////////////////////////////////////////////////////DMH	
	void menuItemPhone_actionPerformed(ActionEvent e) {
		phone(sandboxParameters);
	}
	static void phone(SandboxParameters sandboxParameters) {
		List<SandboxFrame> sandboxFrames = new ArrayList<SandboxFrame> ();
		for (SandboxConnection connection : sandboxParameters.getGlobals().getMap().values()) {
			if (connection.getObj() instanceof SandboxFrame) {
				SandboxFrame sandboxFrame = (SandboxFrame) connection.getObj();
				if (sandboxFrame.isDone()) {
					sandboxParameters.removeFromGlobalMap(sandboxFrame);
				} else {
					System.err.println("dmh451 - adding " + sandboxFrame.getName());

					sandboxFrames.add(sandboxFrame);
					sandboxFrame.setState(JFrame.ICONIFIED); // To minimize a frame
					sandboxFrame.setSavedAOT(sandboxFrame.isAlwaysOnTop());
					sandboxFrame.setAlwaysOnTop(false);
						
				}
			}
		}
		if (sandboxFrames.size() > 0) {
			PhoneRevealRunner runner = new PhoneRevealRunner(sandboxFrames);
			sandboxParameters.addToGlobalMap(runner);
			new Thread(runner).start();
		}

	}
/*///////////////////////////////////////////////////////	
	void menuItemPhone_actionPerformed(ActionEvent e) {
		List<SandboxFrame> sandboxFrames = new ArrayList<SandboxFrame> ();
		for (SandboxConnection connection : sandboxParameters.getGlobals().getMap().values()) {
			if (connection.getObj() instanceof SandboxFrame) {
				SandboxFrame sandboxFrame = (SandboxFrame) connection.getObj();
				if (sandboxFrame.isDone()) {
					sandboxParameters.removeFromGlobalMap(sandboxFrame);
				} else {
					System.err.println("dmh451 - adding " + sandboxFrame.getName());

					sandboxFrames.add(sandboxFrame);
					sandboxFrame.setState(JFrame.ICONIFIED); // To minimize a frame
					sandboxFrame.setSavedAOT(sandboxFrame.isAlwaysOnTop());
					sandboxFrame.setAlwaysOnTop(false);
						
				}
			}
		}
		if (sandboxFrames.size() > 0) {
			PhoneRevealRunner runner = new PhoneRevealRunner(sandboxFrames);
			sandboxParameters.addToGlobalMap(runner);
			new Thread(runner).start();
		}
	}
//*///////////////////////////////////////////////////////	

	void menuItemChiclet_actionPerformed(ActionEvent e) {
		Utilities.showChiclet(sandboxParameters);
	}

	void menuItemFileSaveTab_actionPerformed(ActionEvent e) {
		String filePath = "unknown";
		// Disallow saving anything up to the scratchpad; Allow scratchpad
		// saving because it might be necessary at some point.
		if (sandboxParameters.tabbedPane.getSelectedIndex() < SandboxParameters.LAST_STATIC_TAB) {
			return;
		}
		try {
			JComponent tabComponent = sandboxParameters.getSelectedTabComponent();
			boolean isEditable = false;
			if (tabComponent instanceof SandboxTextPane) {
				isEditable = ((SandboxTextPane) tabComponent).isEditable();
			}
			Component component = sandboxParameters.tabbedPane.getSelectedComponent();

			if (component instanceof JScrollPane) {
				JScrollPane scrollPane = (JScrollPane) component;
				filePath = scrollPane.getToolTipText();
				if (scrollPane.getViewport().getComponent(0) instanceof ISandboxBackedUp) {
					ISandboxBackedUp backedUp = (ISandboxBackedUp) scrollPane.getViewport().getComponent(0);
					sandboxParameters.printLine("text: " + backedUp.isText() + " isDirty:" + backedUp.isDirty(), true, true);
					if (backedUp.isEditable() && backedUp.isDirty()) {
						backedUp.saveBackedUp();
					}
				} else if (scrollPane.getViewport().getComponent(0) instanceof JTextComponent) {
					JTextComponent textComponent = (JTextComponent) scrollPane.getViewport().getComponent(0);
					if (filePath != null && new File(filePath).exists() && isEditable) {
						Utilities.saveFileString(filePath, textComponent.getText());
					} else {
						filePath = sandboxParameters.configurationDirectory + File.separator + "files"
								+ File.separator + "saved-tab-" + Utilities.getTime("ddMMMyy.hh.mm.ss");
						Utilities.saveFileString(filePath, textComponent.getText());
						int selectedTabIndex = sandboxParameters.tabbedPane.getSelectedIndex();
						sandboxParameters.tabbedPane.setToolTipTextAt(selectedTabIndex, filePath);
						scrollPane.setToolTipText(filePath);
					}
				}
			}
			
		} catch (FileNotFoundException fnfe) {
			sandboxParameters.printLine("Could not save file: probably locked or something.");
		} catch (NullPointerException npe) {
			sandboxParameters.printLine("Could not save file.");
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.alert(filePath);
			sandboxParameters.printLine("FILE SAVE ATTEMPTED:\n\t" + filePath, true, true);
		}
	}

	void menuItemFileSaveTabAs_actionPerformed(ActionEvent e) {
		String fileName = "unknown";
		try {
			String startDir = sandboxParameters.configurationDirectory;
			JFileChooser chooser = new JFileChooser(startDir);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			
			
			String strFileDir = sandboxParameters.configurationDirectory + File.separator + "files";
			
			String strFilePath = sandboxParameters.configurationDirectory + File.separator + "files"
					+ File.separator + "saved-tab-" + Utilities.getTime("ddMMMyy.hh.mm.ss");
			File fileDir = new File(strFileDir);
			File filePath = new File(strFilePath);
			chooser.setCurrentDirectory(fileDir);
			chooser.setSelectedFile(filePath);

			Component component = sandboxParameters.tabbedPane.getSelectedComponent();
			if (component instanceof JScrollPane) {
				JScrollPane scrollPane = (JScrollPane) component;
//				int retVal = chooser.showSaveDialog(this);
				sandboxParameters.hideMessages();
				int retVal = chooser.showOpenDialog(this);
				sandboxParameters.showMessages();

				if (retVal == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					fileName = file.getName();
					sandboxParameters.printLine("Saving file:");
					sandboxParameters.printLine("\t" + file.getPath());
					Utilities.saveFileString(file.getPath(), sandboxParameters.getSelectedTabComponentText());
					sandboxParameters.alert(file.getPath());
					sandboxParameters.printLine("File saved:\n\t" + file.getPath());
				} else {
					sandboxParameters.printLine("Open command cancelled by user.", true, true);
				}
			}
		} catch (NullPointerException npe) {
			sandboxParameters.printLine("Could not save file.");
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("FILE SAVE DONE:\n\t" + fileName, true, true);
		}
	}

	void menuItemRefresh_actionPerformed(ActionEvent e) {
		try {
			String[] packageNames = { SandboxParameters.targetPackageName };
			this.closeSandbox(packageNames);
		} catch (Exception en) {
			en.printStackTrace();
		}
	}

	void menuItemExit_actionPerformed(ActionEvent e) {
		try {
			this.closeSandbox();
		} catch (Exception en) {
			en.printStackTrace();
		}
	}

	void menuItemExitTo_actionPerformed(ActionEvent e) {
		sandboxParameters.printLine(SandboxParameters.SANDBOX_UNDER_CONSTRUCTION, true, true);
		try {
			StartThread thread = new StartThread(sandboxParameters.sandboxHome);
			thread.start();

			sandboxParameters.addToGlobalMap(thread);
			this.closeSandbox();
		} catch (Exception en) {
			sandboxLogger.log(Level.ALL, "Could not exit to", en);
		}
	}

	void menuItemStart_actionPerformed(ActionEvent e) {
		try {
			StartThread thread = new StartThread(sandboxParameters.sandboxHome);
			thread.start();

			sandboxParameters.addToGlobalMap(thread);
		} catch (Exception en) {
			sandboxLogger.log(Level.ALL, "Could not start", en);
		}
	}

	public static String SANDBOX_HISTORY_METADATA_NAME = "sandbox-history-metadata";

	void menuItemSaveHistoryTab_actionPerformed(ActionEvent e) {
		try {
			int selectedTab = sandboxParameters.tabbedPane.getSelectedIndex();
			int numTabs = sandboxParameters.tabbedPane.getTabCount();
			if (numTabs == SandboxParameters.LAST_STATIC_TAB + 1) {
				sandboxParameters.printLine("No history to save");
			}
			if (selectedTab <= SandboxParameters.LAST_STATIC_TAB) {
				selectedTab = SandboxParameters.LAST_STATIC_TAB + 1;
			}
			String directoryTimeStamp = Utilities.parseDate(new Date(), SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP);
			File directory = new File(sandboxParameters.configurationDirectory + File.separator
					+ HISTORY_DIRECTORY_NAME + File.separator + directoryTimeStamp);
			directory.mkdirs();
			List<String> record = new ArrayList<String>();
			int bookendInt = Integer.parseInt((String) sandboxParameters.properties.get("bookendChar"));
			for (int tabIndex = selectedTab; tabIndex < numTabs; tabIndex++) {
				StringBuffer buffer = new StringBuffer();
				buffer.append(sandboxParameters.getTabComponentText(tabIndex));
				Component component = sandboxParameters.getTabComponent(tabIndex);
				if (component instanceof ISandboxBackedUp) {
					ISandboxBackedUp backedUp = (ISandboxBackedUp) component;
					File sourceFile = backedUp.getFile();
					if (sourceFile == null) {
						sandboxParameters.printLine("Skipped tab " + tabIndex, true, true);
					} else {

						String fileName = sourceFile.getName().replace((char)bookendInt, '_').replaceAll(" ", "");
						
						File targetFile = new File(directory.getPath() + File.separator + tabIndex + "_" + fileName);
						record.add(targetFile.getPath());
						backedUp.setFile(targetFile);
						targetFile.createNewFile(); // Only happens if it doesn't exist
						if (!backedUp.isText()) {
							Utilities.copyFile(sourceFile, targetFile);
						} else {
							Utilities.saveFileString(targetFile.getPath(), buffer.toString());
						}
					}
				} else {

					String fileName = tabIndex + "_" + sandboxParameters.getTabbedPane().getTitleAt(tabIndex);
					fileName = fileName.replace((char)bookendInt, '_').replaceAll(" ", "");
					fileName = fileName.replace('\\', '_').replace('/', '_').replace('*', '-');
					String filePath = directory.getPath() + File.separator + fileName + ".sbh";
					record.add(filePath);
					Utilities.saveFileString(directory.getPath() + File.separator + fileName + ".sbh",
							buffer.toString());
				}
			}

//			sandboxParameters.hideMessages();
//			sandboxParameters.setAlertUp(true);
//			StringBuffer description = new StringBuffer(JOptionPane.showInputDialog(this, "Describe this history"));
//			sandboxParameters.setAlertUp(false);
//			sandboxParameters.showMessages();
			StringBuffer description = new StringBuffer(Utilities.showInputDialog("Describe this history", sandboxParameters));
			
			description.append('\n');
			for (String entry : record) {
				description.append('\n').append(entry);
			}
			String descriptionFilePath = directory.getPath() + File.separator + "description.sbf";
			sandboxParameters.printLine("History description path:\n\t" + descriptionFilePath, false, false);
			Utilities.saveFileString(descriptionFilePath, description.toString());
			sandboxParameters.alert(descriptionFilePath);
		} catch (Exception en) {
			sandboxLogger.log(Level.ALL, "Could not save history", en);
			sandboxParameters.printLine("Could not save history", en);
		}
	}

	void menuFileOpenList_actionPerformed(ActionEvent e) {
		try {
			String startDir = getStartDirectory();
			JFileChooser chooser = new JFileChooser(startDir);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

			chooser.setFileHidingEnabled(false);

			chooser.setMultiSelectionEnabled(true);

//			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();

			if (retVal == JFileChooser.APPROVE_OPTION) {
				File[] files = chooser.getSelectedFiles();
				for (File file : files) {
					String filePath = file.getPath();
					
					//	new SandboxBasket<String, String>(SandboxParameters.SANDBOX_HOME, this.configurationDirectoryRoot));

					if (filePath.startsWith(sandboxParameters.configurationDirectory)) {
						filePath = SandboxParameters.SANDBOX_DIRECTORY
								+ filePath.substring(sandboxParameters.configurationDirectory.length());
					}
					sandboxParameters.setFocusedText(filePath);
					menuFileOpen_actionPerformed(e);
				}

			} else {
				sandboxParameters.printLine("Open command cancelled by user.", true, true);
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN DONE", true, true);
		}
	}

	void menuFileOpenHistory_actionPerformed(ActionEvent e) {
		try {
			String startDir = this.sandboxParameters.configurationDirectory + File.separator
					+ HISTORY_DIRECTORY_NAME;
			File[] dirs = sandbox.Utilities.listFilesDirsAsArray(new File(startDir), null, false);
			String[] items = new String[dirs.length];

			int index = 0;
			for (File file : dirs) {
				String descriptionFilePath = startDir + File.separator + file.getName()
						+ File.separator + "description.sbf";
				String description = "No Description";
				try {
					description = sandbox.Utilities.getFileString(descriptionFilePath);
					if (description.split("\\n").length > 1) {
						description = description.split("\\n")[0];
					}
				} catch (FileNotFoundException fnfe) {
					sandboxParameters.printLine("No description found: " + descriptionFilePath, true, true);
				}
				items[index++] = file.getName() + " " + description;
			}

			sandboxParameters.hideMessages();
			String history = (String) JOptionPane.showInputDialog(this, "ddmmmyy_hh.mm.ss", "Choose History",
					JOptionPane.PLAIN_MESSAGE, sandboxParameters.iconPretty, items, items[0]);
			sandboxParameters.showMessages();
//			String history = new StringBuffer(Utilities.showInputDialog("Choose History", sandboxParameters)).toString();


			File directory = new File(startDir + File.separator + history.split(" ", 2)[0]);
			sandboxParameters.printLine("HISTORY: " + directory.getPath(), true, true);
			try {

				File[] files = sandbox.Utilities.listFilesDirsAsArray(directory, null, false);
				List<String> resources = new ArrayList<String>();
				for (File resource : files) {
					resources.add(resource.getPath());
				}

				SandboxOpen sandboxOpen = new SandboxOpen(sandboxParameters, resources);
				sandboxOpen.setName("Opening " + resources);
				sandboxOpen.start();
			} catch (Exception en) {
				sandboxParameters.printLine("Could not get history", en);
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("OPEN HISTORY DONE", true, true);
		}
	}

	void menuRestoreConfiguration_actionPerformed(ActionEvent e) {
		try {
			String startDir = this.sandboxParameters.configurationDirectory;
			SandboxFileFilterChooser filter = new SandboxFileFilterChooser(".xml", "sandbox");

			JFileChooser chooser = new JFileChooser(startDir);

			chooser.setFileFilter(filter);

//			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();

			if (retVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				sandboxParameters.properties = new Properties();
				FileInputStream filelnputStream = new FileInputStream(file);
				sandboxParameters.properties.loadFromXML(filelnputStream);
				sandboxParameters.resetParameterValues();
				sandboxParameters.loadConfiguration(sandboxParameters.properties);
			} else {
				sandboxParameters.printLine("Restore configuration command cancelled by user.");
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("RESTORE CONFIGURATION DONE", true, true);
		}
	}

	void menuItemSaveConfiguration_actionPerformed(ActionEvent e) {
		try {
			sandboxParameters.storeConfiguration();
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("CONFIGURATION SAVE DONE", true, true);
		}
	}

	void menuItemSaveAsConfiguration_actionPerformed(ActionEvent e) {
		try {
			String startDir = sandboxParameters.configurationDirectory;
			JFileChooser chooser = new JFileChooser(startDir);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			File startPath = new File(startDir);
			chooser.setCurrentDirectory(startPath);
			File filePath = new File(startDir + File.separator + "sandbox." 
					+ (new SandboxTestBase()).getTime(SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP) + ".xml");
//			File filePath = new File(startDir + File.separator + "sandbox." + (new Date()).getTime() + ".xml");
			chooser.setCurrentDirectory(filePath);
			chooser.setSelectedFile(filePath);
			
//			int retVal = chooser.showSaveDialog(this);
			sandboxParameters.hideMessages();
			int retVal = chooser.showOpenDialog(this);
			sandboxParameters.showMessages();
			
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String scratchPadName = SandboxParameters.SCRATCHPAD_NAME + (new Date()).getTime() + ".txt";
				sandboxParameters.storeConfiguration(file.getAbsolutePath(), scratchPadName);

			} else {
				sandboxParameters.printLine("Save-as command cancelled by user.");
			}
		} catch (Exception en) {
			en.printStackTrace();
		} finally {
			sandboxParameters.printLine("CONFIGURATION SAVE-AS DONE", true, true);
		}
	}

	void menuItemParametersDelete_actionPerformed(ActionEvent e) {
		try {
			sandboxParameters.deleteFocusedParameter();
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not delete parameter.");
		}
	}

	void menuItemParametersDeleteAll_actionPerformed(ActionEvent e) {
		try {
			sandboxParameters.deleteFocusedParameters();
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not delete parameter.");
		}
	}

	void menuItemParametersLoad_actionPerformed(ActionEvent e) {
		try {
			Object focused = sandboxParameters.getFocusedObject();
			if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
				JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
				String[] scratchpadText = sandboxParameters.getScratchPadText().split("\n");
				sandboxParameters.printLine("Adding " + scratchpadText.length + " items to " + comboBox.getName());
				for (String entry : scratchpadText) {
					comboBox.addItem(entry);
				}
			} else {
				sandboxParameters.printLine("Must select a combo box");
			}
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not load parameters.");
		}
	}

	void menuItemParametersGoTo_actionPerformed(ActionEvent e) {
		try {
			sandboxParameters.printLine(SandboxParameters.UNDER_CONSTRUCTION, false, false);
			Utilities.showMessageDialog(SandboxParameters.UNDER_CONSTRUCTION, sandboxParameters);
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not go to " + en);
		}
	}

	void menuItemParametersTest_actionPerformed(ActionEvent e) {
		try {
			sandboxParameters.runTest(sandboxParameters.getFocusedText());
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not start test", en);
		}
	}

	void menuItemHelp_actionPerformed(ActionEvent e) {
		try {
			String resource = "start/sandbox/.sandboxhelp.sbx";
			SandboxOpen sandboxOpen = new SandboxOpen(sandboxParameters, resource, false);
			sandboxOpen.start();
		} catch (Exception en) {
			sandboxParameters.printLine(en.getClass().getSimpleName() + " : Could not go to " + en);
		}
	}

	void buttonAction(ActionEvent e) {
		int selectedIndex = sandboxParameters.tabbedPane.getSelectedIndex();

		if (selectedIndex > SandboxParameters.LAST_STATIC_TAB) {
			JComponent portComponent = sandboxParameters.getTabComponent(selectedIndex);
			System.out.println(this.getClass().getSimpleName() + ".buttonAction()component is " + portComponent);
			if (portComponent instanceof SandboxTextArea) {
				if (e.getActionCommand().equals(SAVE_BUTTON_NAME)) {
					return;
				}
				SandboxTextArea textArea = (SandboxTextArea) portComponent;
				SandboxTestBase test = textArea.getTest();
				SandboxThread sandboxThread = textArea.getSandboxThread();
				if (test != null && test.isRunning) {
					test.endMe();
				} else if (sandboxThread != null && sandboxThread.isRunning()) {
					sandboxThread.close();
					sandboxParameters.tabbedPane.setIconAt(sandboxParameters.tabbedPane.getSelectedIndex(), null);
				} else {
					sandboxParameters.tabbedPane.removeTabAt(sandboxParameters.tabbedPane.getSelectedIndex());
				}
			} else if (portComponent instanceof SandboxTextPane) {
				SandboxTextPane textPane = (SandboxTextPane) portComponent;
				try {
					if (e.getActionCommand().equals(SAVE_BUTTON_NAME) && textPane.isDirty() && textPane.text) {
						textPane.saveBackedUp();
					} else {
						if (e.getActionCommand().equals(CANCEL_BUTTON_NAME)) {
//							textPane.saveBackedUp();
							if (textPane.getFile() != null && !textPane.getFile().exists()) {
								sandboxParameters.printLine("Could not save " + textPane.getFile().getPath());
							}
							textPane.closeBackedUp();
							sandboxParameters.tabbedPane.removeTabAt(sandboxParameters.tabbedPane.getSelectedIndex());
							if (textPane.getMapId() != null) {
								sandboxParameters.removeFromGlobalMap(textPane.getMapId());
							}
						}
					}

				} catch (Exception en) {
					sandboxParameters.printLine("Could not save " + textPane.getFile().getPath(), en);
				}
			} else if (portComponent instanceof SandboxTable) {
				SandboxTable table = (SandboxTable) portComponent;
				try {
					if (e.getActionCommand().equals(SAVE_BUTTON_NAME)) {
						table.saveBackedUp();
					} else {
						if (e.getActionCommand().equals(CANCEL_BUTTON_NAME)) {
							sandboxParameters.tabbedPane.removeTabAt(sandboxParameters.tabbedPane.getSelectedIndex());
							if (table.isEditable) {
								table.closeBackedUp();
								sandboxParameters.removeFromGlobalMap(table.getMapId());
							}
						}
					}
				} catch (Exception en) {
					sandboxParameters.printLine("Could not save " + table.getFile().getPath(), en);
				}
			} else {
				sandboxParameters.tabbedPane.removeTabAt(sandboxParameters.tabbedPane.getSelectedIndex());
			}
			sandboxParameters.selectTab(sandboxParameters.tabbedPane.getSelectedIndex());
		} else {
			sandboxParameters.printLine("No implementation", true, true);
		}
	}

	protected void closeSandbox() {
		closeSandbox(null);
	}

	protected void closeSandbox(String[] packageNames) {
		int connectionsClosed = 0;
		try { // Close out tests
			List<SandboxTestBase> tests = sandboxParameters.tests;
			Iterator<SandboxTestBase> iterTests = tests.iterator();
			while (iterTests.hasNext()) {
				SandboxTestBase testObj = (SandboxTestBase) iterTests.next();
				SandboxTestBase test = sandboxParameters.getTest(testObj.getTestName());
				if (test.getIsRunning()) {
					test.setAnnounce(false);
					test.closeConnections();
					test.setIsRunning(false);
					Process process = test.getProcess();
					if (process != null) {
						process.destroy();
					}
					sandboxParameters.printLine(this.getClass().getName() + ".windowclosing() is stopping "
							+ test.getTestName() + " process.", true, true);
				}
			}
		} catch (Exception en) {
			sandboxParameters.printLine(this.getClass().getName() + ".windowClosing()unable to close all processes.",
					true, true);
		}

		try {// Close connections in globals members, if any
			
			sandboxParameters.properties.put("chicletX", "-1");
			sandboxParameters.properties.put("chicletY", "-1");
			for (String key : sandboxParameters.globals.getMap().keySet()) {
				
//*////////////////DMH				
				SandboxConnection connection = sandboxParameters.globals.getMap().get(key);
				if (key.equals("chiclet")) {
					SandboxFrame chiclet = (SandboxFrame)connection.getObj();
					if (chiclet.isVisible()) {
						sandboxParameters.properties.put(key + "X", String.valueOf(chiclet.getX()));
						sandboxParameters.properties.put(key + "Y", String.valueOf(chiclet.getY()));
					} else {
						sandboxParameters.properties.put(key + "X", "-1");
						sandboxParameters.properties.put(key + "Y", "-1");
					}
				}
				connection.close();
/*///////////////////				
				SandboxConnection connection = sandboxParameters.globals.getMap().get(key);
				connection.close();
//*///////////////////				

				
				/**
				 * 				

				 */
				
				
				// TODO print out the list of strings this makes
			}
		} catch (Exception en) {
			sandboxLogger.info(en.getClass().getSimpleName() + " : Connections/Readers closing interrupted.");
		}
		sandboxLogger.info("Number of connections closed: " + connectionsClosed);
		if (cbMenuItemSaveOnExit.getState()) {
			Dimension frameSize = getSize();
			if ((getExtendedState() & JFrame.MAXIMIZED_BOTH) == 0) {
				sandboxParameters.frameHeight = frameSize.height;
				sandboxParameters.frameWidth = frameSize.width;
			}

			// Store configuration and environment
			sandboxParameters.storeConfiguration();
			sandboxParameters.storeEnvironment();
		}

		if (packageNames != null) { // If there are package names, then restart
			for (String packageName : packageNames) {
				String[] arguments = new String[1];
				arguments[0] = packageName;
				Properties globalProps = new Properties();
				String globalFilePath = sandboxParameters.configurationDirectoryRoot + File.separator
						+ "environment.xml";

				File globalEnvironmentFile = new File(globalFilePath);
				try {
					if (globalEnvironmentFile.exists()) {

						FileInputStream environmentFIS = new FileInputStream(globalEnvironmentFile);
						globalProps.loadFromXML(environmentFIS);
						sandboxLogger.info("Loaded global properties = " + globalFilePath);
					}

				} catch (Exception en) {
					sandboxLogger.info("No global properties 'loaded");
				}
				java.util.Properties props = System.getProperties();
				String javaMemorySettings = ((String) props.getProperty("sandbox.javamemory", "-Xms64m -Xmx512m"))
						.replaceAll(SandboxParameters.SANDBOX_NULL, "");
				StringBuffer javaSettings = new StringBuffer();
				Enumeration<?> propNames = props.propertyNames();
				Set<String> locals = new HashSet<String>();

				while (propNames.hasMoreElements()) {
					String propName = (String) propNames.nextElement();
					if (propName.startsWith("sandbox.java.")) {
						javaSettings.append("-D");
						String name = propName.substring("sandbox.java.".length());
						locals.add(name);
						javaSettings.append(name);
						javaSettings.append("=");
						javaSettings.append(props.getProperty(propName));
						javaSettings.append(" ");
					} // quote = "\" ";

					if (propName.startsWith("sandbox.conf.")) {
						javaSettings.append("-D");
						javaSettings.append(propName);
						javaSettings.append("=");
						javaSettings.append(props.getProperty(propName));
						javaSettings.append(" ");
					}
					if (propName.startsWith("sbx.conf.")) {
						javaSettings.append("-D");
						javaSettings.append(propName);
						javaSettings.append("=");
						javaSettings.append(props.getProperty(propName));
						javaSettings.append(" ");
					}
				}
				Enumeration<?> globalPropNames = globalProps.propertyNames();
				while (globalPropNames.hasMoreElements()) {
					String propName = (String) globalPropNames.nextElement();
					if (!locals.contains(propName)) {
						javaSettings.append("-D");
						javaSettings.append(propName);
						javaSettings.append("=");
						javaSettings.append(globalProps.getProperty(propName));
						javaSettings.append(" ");
					}
				}

				javaSettings.append("-Dsandbox.home=" + sandboxParameters.sandboxHome).append(" ");
				javaSettings.append("-Duser.home=" + System.getProperty("user.home")).append(" ");

				String classPath = System.getProperty(Start.SANDBOX_CLASS_PATH_NAME);
				if (classPath == null) {
					classPath = System.getProperty("java.class.path");
				}
				String password = sandbox.Utilities.base64Encode(sandboxParameters.getPassword());
				String javaCommandLine = "sandbox.Sandbox " + packageName + " " + password;

				String commandLine = "java";
				if (javaMemorySettings.length() > 0)
					commandLine += " " + javaMemorySettings + " ";
				if (javaSettings.length() > 0)
					commandLine += javaSettings;
				String quote = "";
				if (Utilities.isWindows()) {
					quote = "\"";
				}
				commandLine += "-cp " + quote + classPath + quote + " " + javaCommandLine;
				
				Process process = null;
				try {
					Runtime runtime = Runtime.getRuntime();

					process = runtime.exec(commandLine);
					sandboxLogger.info("Starting new Sandbox: " + commandLine);
					
				} catch (IOException ioe) {
					sandboxLogger.log(Level.ALL, "Could not start new Sandbox", ioe);
				}
			}
		}
		System.exit(0);
	}
	class PhoneRevealRunner implements Runnable {
		private boolean done = false;
		private List<SandboxFrame> sandboxFrames = null;
		public PhoneRevealRunner(List<SandboxFrame> sandboxFrames) {
			this.sandboxFrames = sandboxFrames;
		}

		@Override
		public void run() {
			System.out.println("phoning " + this.sandboxFrames.size());
			try {
				this.sandboxFrames.sort(new Comparator<SandboxFrame>() {

					@Override
					public int compare(SandboxFrame sf1, SandboxFrame sf2) {
						// TODO Auto-generated method stub
						if (sf1.isSavedAOT()) {
							return 1;
						}
						if (sf2.isSavedAOT()) {
							return -1;
						}
						return 0;
					}
					
				});
				int timeBetweenPhoning = Integer.valueOf(System.getProperty(SandboxParameters.SBX_PHONE_CALL_DURATION, "1000"));
				
				while (!this.sandboxFrames.isEmpty() && !done) {
					
					SandboxFrame sandboxFrame = this.sandboxFrames.remove(0);
					System.out.println("phoning " + sandboxFrame.getName());
					sandboxFrame.phoneIn();
					Thread.sleep(timeBetweenPhoning);
				}
				done = true;
			} catch (Throwable th) {
				System.out.println("NOPE! " + th.getMessage());
				th.printStackTrace();
				done = true;
			}

		}

		protected void setDone(boolean done) {
			this.done = done;
		}

		protected boolean isDone() {
			return done;
		}
	}

}
