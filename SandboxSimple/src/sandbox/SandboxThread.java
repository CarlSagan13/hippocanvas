//SandboxThread.java (erase me later)
package sandbox; 
 
import javax.swing.JTextArea; 
import javax.swing.text.Document; 
 
 public class SandboxThread extends Thread {
 	 protected JTextArea textArea = null; 
 	 protected int windowSize = 1000; 
 	 protected boolean isRunning = true ; 
 	 
 	 protected void printLine(String string ){
 	 	 textArea.append(string + '\n'); 
 	 	 Document doc = this.textArea.getDocument(); 
 	 	 this.textArea.setCaretPosition(doc.getLength()); 
 	 }
 
 	 /**
 	 *@return the textArea
 	 */
 	 public JTextArea getTextArea(){
 	 	 return textArea; 
 	 }
 
 	 /**
	 *@param textArea the textArea to set
 	 */
 	 public void setTextArea(JTextArea textArea){
 	 	 this.textArea = textArea; 
 	 }
 
 	 /**
 	 *@return the windowsize
 	 */
 	 public int getWindowSize (){
 	 	 return windowSize; 
 	 }
 
 	 /**
 	 *@param windowSize the windowSize to set
 	 */
 	 public void setWindowSize(int windowSize){
 	 	 this.windowSize = windowSize; 
 	 }
 
 	 /**
 	 *@return the isRunning
 	 */
 	 public boolean isRunning (){
 	 	 return isRunning; 
 	 }
 
 	 /**
 	 *@param isRunning the isRunning to set
 	 */
 	 public void setRunning(boolean isRunning){
 	 	 this.isRunning = isRunning; 
 	 }
 
 	 public void close(){
 	 	 this.isRunning = false; 
 	 }
 }
