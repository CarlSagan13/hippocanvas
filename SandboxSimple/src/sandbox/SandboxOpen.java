//SandboxOpen.java (erase me later) 
package sandbox;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import javax.swing.text.rtf.RTFEditorKit;
import javax.swing.undo.UndoManager;

public class SandboxOpen extends Thread implements SandboxCommon {
	
	private SandboxParameters params = null;
	private String address = null;
	private List<String> addresses = new ArrayList<String>();
	private InputStream inputStream = null;
	private JComponent component = null;
	private boolean edit = false;
	private ScheduledFuture<?> scheduleHandle = null;
	private PeriodicFileBackup periodicFileBackup = null;	
	private boolean doneLoading = false;
	private UndoManager undoManager = null;
	private File file = null;
	private String description = null;
	private SandboxProcess sandboxProcess = null;
	private SandboxTextPane textPane = null;
	private String tabTitle = null;

//	public SandboxOpen(SandboxParameters params, String address){
//		this.params = params;
//		this.address = address;
//	}
	
	public SandboxOpen(SandboxParameters params, List<String> addresses){
		this.addresses = addresses;
		this.params = params;
		this.address = addresses.remove(0);
	}

	public SandboxOpen(SandboxParameters params, String address, boolean edit){
		this.params = params;
		this.address = address;
		this.edit = edit;
	}
	
//	public SandboxOpen(SandboxParameters params, String address, String description, boolean edit){
//		this.params = params;
//		this.address = address;
//		this.description = description;
//		this.edit = edit;
//	}
	
	public SandboxOpen(SandboxParameters params, String address, String description, SandboxProcess sandboxProcess, boolean edit){
		this.params = params;
		this.address = address;
		this.description = description;
		this.sandboxProcess = sandboxProcess;
		this.edit = edit;
	}
	
	@Override
	public void run(){
		try {
			if (address.indexOf('\t') > 0) {
				String[] addrTabTitle = address.split("\t+?",2);
				address = addrTabTitle[0];
				tabTitle = addrTabTitle[1];
			}
			
			params.alert("Opening: " + address, params.iconRunning);

			if (address.substring(0, 4).equals("http")){
				openWeb(address);
				recurse();
				return;
			}
			// Validate the file
			try {
				file = new File(address);
				if (edit){
					if (file.exists()) {
						printLine("File " + address + " already exists");
						//			cmd = URLDecoder.decode(cmd, "US-ASCII");

						String acceptableFileRegex = URLDecoder.decode(System.getProperty("sbx.conf.open.acceptable.file.regex.url.ascii", ".*\\.dmh"), "US-ASCII");
						if (!address.matches(acceptableFileRegex)) {
							if (file.canWrite()) {
								int result = showConfirmMessage("Open for edit?\n\t" + file.getPath());
								if (result == 3) {
									throw new SandboxException("You cancelled\n\t" + file.getPath());
								}
								if (result == 2) {
									printLine("File " + address + " rejected for editing");
									edit = false;
								}
							} else {
								edit = false;
							}
						}
					} else {
						int result = showConfirmMessage("Create new file?\n\t" + file.getPath());

						if (result == 1) {
							file.createNewFile();
						} else {
							throw new SandboxException("Unwilling to create the file:\n\t" + file.getPath());
						}
					}
				}
				inputStream = new FileInputStream(address);
//				FileSystemResource resource = new FileSystemResource(address);
//				inputStream = resource.getInputStream();
			} catch(FileNotFoundException fnfe){ //Not found as file, so try as resource:
				printLine("Could not get as file... trying to get as a resource");

				byte[] bytes = Utilities.getResourceStringInternal(address).getBytes();
				file = File.createTempFile("tmpfile",".tmp");
				OutputStream out = new FileOutputStream(file);
				int read = bytes.length;
				out.write(bytes,0,read);
				try {
					out.close();
				} catch(IOException ioe) {
					printLine("Could not close temp output stream.");
				}
				file.deleteOnExit();
				
				
//		        InputStream instr = this.getClass().getClassLoader().getResourceAsStream(address);
//
//				ClassPathResource resource = new ClassPathResource(address);
//				inputStream = resource.getInputStream();
//				try {
//					file = resource.getFile();
//				} catch(FileNotFoundException fnfe2){
//					printLine("Could not get as resource...trying to get from an internal resource file");
//					inputStream = this.getClass().getResourceAsStream(address);
//					file = File.createTempFile("tmpfile",".tmp");
//					OutputStream out = new FileOutputStream(file);
//					int read;
//					byte[] bytes = new byte[params.getMaxByteArraySize()];
//
//					while((read = inputStream.read(bytes)) != -1){
//						out.write(bytes,0,read);
//					}
//					try {
//						out.close();
//					} catch(IOException ioe) {
//						printLine("Could not close temp output stream.");
//					}
//					file.deleteOnExit();
//				}
			}
		
			String filename = file.getName();
			int dotIndex = filename.lastIndexOf(".");
			String extension = "";
			if (dotIndex >= 0){
				extension = filename.substring(dotIndex);
			}
			printLine("Opening \""+ file.getAbsolutePath()+ "\"...");
			long fileSize = file.length();
			printLine("File size: "+ fileSize);
			
			//Sound files
			if (extension.toLowerCase().equals (".wav")) { //WAV
				
				//FileSystemResource
				
				/**
				 * 				ClassPathResource resource = new ClassPathResource(parameters.sandboxSoundFile);
				InputStream inputstream = resource.getInputStream();
				Utilities.playThreadedSound(inputstream, parameters);

				 */
				Utilities.playThreadedSound(inputStream, params);
				recurse();
				return;//No need to open a tab 			
			}
			if (extension.toLowerCase().equals (".ogg")) { //OGG
				Utilities.playThreadedSound(inputStream, params);
				recurse();
				return;//No need to open a tab 			
			}
			//Handle CSV files
			if (extension.toLowerCase().equals (".csv") && inputStream != null){
				LineNumberReader reader = new LineNumberReader (new InputStreamReader(inputStream));
				List<String> lines = new ArrayList<String> ();
				String line = reader.readLine();
				printLine(line);
			
				String[] headers = line.split ("\",\"");
				boolean useQuotes = true;
				if (headers.length == 1){
					headers = line.split(",");
					useQuotes = false;
				} else {
					String firstValue = headers[0 ];
					String lastValue = headers[headers.length - 1];
					headers[0]= firstValue.substring(1);
					headers[headers.length - 1]= lastValue.substring(0,lastValue.length()- 1);
				}
			
				int byteSize = line.length();
				while ((line = reader.readLine ())!= null){
					lines.add(line);
					byteSize += line.length();
					if (byteSize > params.getMaxFileOpenSize()){
						lines.add("TRUNCATED HERE");
						break;
					}
				}
				String[][] data = new String[lines.size()][headers.length];				int index = 0;
				for (String row : lines){
					row = row.replaceAll("\\,\\,",",,").replaceAll("\\,$",","); 
					//Had to put this in because giving an empty character to the JTable messed it up
					if (useQuotes){
						data[index]= row.split("\",,\"");
						String firstValue = data[index][0];
						String lastValue = data[index][headers.length - 1];
						data[index][0] = firstValue.substring(1);
						data[index][headers.length - 1] = lastValue.substring(0,lastValue.length() - 1);
						index++;
					}else {
						
						
						row = row.replaceAll("<SBNL>", System.getProperty("line.separator", "\n"));
						
						
						
						data[index++] = row.split(",");
					}
				}
				try {
//					SandboxTable table = new SandboxTable(file);
					SandboxTable table = Utilities.tabulate(params, data, headers, address, false);
					component = table;
					table.setFile(file);
					table.setEnabled(true);
					table.setEditable(edit);
					JScrollPane scrollPaneTable = new JScrollPane(table);
					
					
					
					scrollPaneTable.setName(limitTabLength("T: "+ filename));
					
					
					
					scrollPaneTable.setToolTipText(file.getPath());

				
//						int pageNumber = params.numTabs + 1;
//						scrollPaneTable.setName("Table:"+ (pageNumber - SandboxParameters.LAST_STATIC_TAB));
					params.tabbedPane.add(scrollPaneTable);
					int pageNumber = params.tabbedPane.getTabCount()- 1;
					if (this.description != null) {
						description = file.getPath();
					}
					if (tabTitle == null) {
						tabTitle = scrollPaneTable.getName();
					}
					params.newTab(description, scrollPaneTable, limitTabLength(tabTitle));
					params.tabbedPane.setToolTipTextAt(pageNumber, address);
				
					if (file.canWrite() && edit){
						file.setWritable(false);
						params.tabbedPane.setIconAt(pageNumber,params.iconPretty);
						table.setMapId(params.addToGlobalMap(table));
						backupSetup();
					}
				
				} catch(ArrayIndexOutOfBoundsException aioobe){
					printLine("*** PARSING ERROR ON LINE " + aioobe.getMessage());
				}
				recurse();
				return;
			}
		
			textPane = new SandboxTextPane(file, sandboxProcess);
			
			
			/*/////////////////////////////DMH
			//textPane.setFont(Font.);/
			java.awt.Font font = new java.awt.Font("Courier", java.awt.Font.PLAIN, 20); 
			textPane.setFont(font);
			//*/////////////////////////////
			
//			String[] clf = System.getProperty("sbx.conf.textFont","Dialog_0_14").split("_");
//			Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
//			textPane.setFont(textFont);
			
			
			
			component = textPane;
			if (extension.toLowerCase().equals (".gif")//Images
				||	extension.toLowerCase().equals(".jpg")
				||	extension.toLowerCase().equals(".jpeg")
				||	extension.toLowerCase().equals(".png"))
			{
			
				textPane.text = false;
				textPane.setEnabled(false);
				textPane.setEditable(false);
				textPane.insertIcon(new ImageIcon(file.getPath()));
				JScrollPane scrollPaneImage = new JScrollPane(textPane);
				scrollPaneImage.setName(limitTabLength("I: "+ filename));
			
				scrollPaneImage.setToolTipText(file.getPath());
				params.tabbedPane.add(scrollPaneImage);
				int pageNumber = params.tabbedPane.getTabCount()- 1;
				params.tabbedPane.setToolTipTextAt(pageNumber,address);
				if (this.description != null) {
					description = file.getPath();
				}
				if (tabTitle == null) {
					tabTitle = scrollPaneImage.getName();
				}
				params.newTab(description, scrollPaneImage, limitTabLength(tabTitle));
				
				recurse();
				params.alert(address);
				return;
			}
			textPane.setContentType("text/txt");
		
			//HTML files
			if (extension.toLowerCase().equals (".html") || extension.toLowerCase().equals (".htm")){
				if (file.length()> params.getMaxFileOpenSize()){
					printLine("File size: " + fileSize + " which is larger than maximum of "+ params.getMaxFileOpenSize());
					recurse(); 
					return;
				}
				if (edit) {
					textPane.setContentType("text/txt");
					textPane.setEditable(true);
				} else {
					textPane.setContentType("text/html");
					String pageString = Utilities.getFileString(file.getPath());
					try {
						JEditorPane editorPanePlain = new JEditorPane();
						editorPanePlain.setEditable(false);
						JScrollPane scrollPanePlain = new JScrollPane(editorPanePlain);
						HTMLEditorKit kit = new HTMLEditorKit();
						editorPanePlain.setEditorKit(kit);
						Document doc = kit.createDefaultDocument();
						editorPanePlain.setDocument(doc);
						doc.insertString(0, pageString, null);
//// 						editorPanePlain.setText(pageString); //this causes performance issues
//						String pageTabLabel = "html";
//						scrollPanePlain.setName(limitTabLength(pageTabLabel));
//						int pageNumber = params.tabbedPane.getTabCount();
//						params.tabbedPane.add(scrollPanePlain,pageNumber);
//						printLine("in tab \""+ pageTabLabel + "\"");
//						params.tabbedPane.setToolTipTextAt(pageNumber,filename);
//						params.tabbedPane.setSelectedIndex(pageNumber);
					}catch (ArrayIndexOutOfBoundsException aioobe){
						printLine("Not opening web page");
						printLine(pageString);					
					}
				}
			}
			
			//RTF files
			if (extension.toLowerCase().equals(".rtf")) {
				textPane.setContentType("text/rtf");
				textPane.setEditable(edit);
				RTFEditorKit kit = new RTFEditorKit();
				textPane.setEditorKit(kit);
//			} else {
//				textPane.setContentType("text/txt");
			}
			
			if (file.length() > params.getMaxFileOpenSize()) {
				printLine("***TRUNCATING***");
				textPane.setText(Utilities.getFileString(file, params.getMaxFileOpenSize())
					+ "...\n ***TRUNCATED***");
			}
		
			//Outlook MSG files
			if (extension.toLowerCase().equals(".msg")) {
				try {
					byte[] messageBytes = Utilities.getFileBytes(file);
					byte[] classificationWithZeros = {67,0,108,0,97,0,115,0,0,105,0,102,0,105,0,99,97,0,116,0,105,0,111,0,110,0,58};
					byte[] classification = {67,108,97,115,115,105,102,105,99,97,116,105,111,110,58};
					
					int from = Utilities.indexInByteArray(0, messageBytes, classificationWithZeros);
					if (from >= 0) {
						int to = Utilities.indexInByteArray(from + classificationWithZeros.length,messageBytes,classificationWithZeros);
						if (to > 0){
							byte[]readableText = Arrays.copyOfRange(messageBytes,from,to);
							String readableString = new String(readableText);
							textPane.setText(new String(readableString.replaceAll("\0","")));
						}
					}else {
						from = Utilities.indexInByteArray(0,messageBytes,classification);
						if (from >= 0){							int to = Utilities.indexInByteArray(from + classification.length,messageBytes,classification);
							if (to > 0){								byte[]readableText = Arrays.copyOfRange(messageBytes,from ,to);
								String readableString = new String(readableText);
								textPane.setText (new String(readableString.replaceAll("\0","")));
							}
						}
					}
				
				} catch(Exception en){
					printLine("Could not read the Outlook message file",en);
					textPane.setText(Utilities.getFileString(file.getPath()));
				}
			} else if (extension.toLowerCase().equals(".json")) {
				try {
//					textPane.setText(Utilities.jsonPrettyPrint(Utilities.getFileString(file.getPath())));
					textPane.setText(Utilities.getFileString(file.getPath()));
				} catch(Throwable th) {
					printLine("Could not pretty print the JSON: " + th.getMessage());
					th.printStackTrace();
					textPane.setText(Utilities.getFileString(file.getPath()));
				}
			} else if (extension.toLowerCase().equals(".xml")){
					textPane.setText(Utilities.xmlPrettyPrint(Utilities.getFileString(file.getPath()),5));
			} else {
				textPane.setText(Utilities.getFileString(file.getPath()));
			}
		
			textPane.setCaretPosition(0);
			textPane.addKeyListener(params.keyListener);
		
			JScrollPane scrollPane = new JScrollPane(textPane);
			textPane.addFocusListener(params.focusListener);

			int pageNumber = params.tabbedPane.getTabCount();
			
			if (tabTitle == null) {
				tabTitle = filename;
			}
			String pageTabLabel = tabTitle;
			
			
			
			
			scrollPane.setName(limitTabLength(pageTabLabel));
			
			
			
			scrollPane.setToolTipText(address);			
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			if (description != null) {
				scrollPane.setName(description);
			}
			params.tabbedPane.add(scrollPane, pageNumber);
			params.selectTab(pageNumber);
			printLine("in tab \""+ pageTabLabel + "\"");
			params.tabbedPane.setToolTipTextAt(pageNumber, address);
			params.tabbedPane.setForegroundAt(params.tabbedPane.getSelectedIndex(), params.tabForegroundColor);
			params.tabbedPane.setSelectedIndex(pageNumber);
			params.tabbedPane.setForegroundAt(pageNumber, params.tabSelectedColor);
			
			if (file.canWrite() && edit){
				undoManager = Utilities.addUndo(textPane);
				file.setWritable(false);
				textPane.setEditable(true);
				textPane.setMapId(params.addToGlobalMap(textPane));
				backupSetup();
				
//				scrollPane.setFont(scrollPane.getFont().deriveFont(Font.BOLD));
//				String[] clf = System.getProperty("sbx.conf.textFont","Dialog_0_14").split("_");
//				Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
//				textPane.setFont(textFont);
				
				
				
				
				params.tabbedPane.setIconAt(pageNumber, params.iconPretty);
			} else {
				edit = false;
				textPane.setEditable(false);
			}
		
//			if (edit){
//				//DMH get to SandboxTextPane.save ()periodically
//			}
		
			params.alert(address);

			recurse();
		
		} catch(SandboxException sex){
			printLine(sex.getMessage());
//			params.alert("Could not open address " + address);
//			close();
		} catch(Exception en){
			printLine("Could not open address " + address, en);
			params.alert("Could not open address " + address);
			close();
		} finally {
			this.doneLoading = true;
			if (inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e){
					printLine("Trying to close InputStream", e);
				}
			}
		}
	} //run()

	private void recurse(){
		if (addresses.size()> 0){
			SandboxOpen sandboxOpen = new SandboxOpen(params, addresses);
			Thread thread = new Thread(sandboxOpen);
			thread.setName("Opening " + address);
			thread.start();
		}
		return;
	}

	private void openWeb(String url){
		try {
			JTextPane textPane = new JTextPane();
			textPane.setPage(url);
			JScrollPane scrollPane = new JScrollPane(textPane);
		
			int pageNumber = params.tabbedPane.getTabCount();
			String pageTabLabel = url.substring(url.lastIndexOf('/'));
//			String pageTabLabel = "Page-"+ (pageNruriber - SandboxParameters.LAST_STATIC_TAB);			
			scrollPane.setName(limitTabLength(limitTabLength(pageTabLabel)));
			params.tabbedPane.add(scrollPane,pageNumber);
			printLine("in tab \""+ pageTabLabel + "\"");
			params.tabbedPane.setToolTipTextAt(pageNumber,url);
//			params.tabbedPane.setForegroundAt(params.tabbedPane.getSelectedIndex(), params.tabForegroundColor);
			params.tabbedPane.setSelectedIndex(pageNumber);
//			params.tabbedPane.setForegroundAt(pageNumber, params.tabSelectedColor);
		
		} catch(IOException ioe){
			printLine("Could not open url "+ url ,ioe);
		}
	}

	private String limitTabLength(String tabTitle){
		return Utilities.limitTabLength(tabTitle);
	}

	private void printLine(String string) {
		params.printLine(string, true, true);
	}

	private void printLine(String string, Throwable th) {
		params.printLine(string, th, false);
	}

	public InputStream getInputStream() {
		return inputStream;
	}
	public JComponent getComponent() {
		return component;
	}
	public void setComponent(JComponent component) {		
		this.component = component;
	}
	public String getAddress() {
		return address;
	}

	public boolean isEdit() {
		return edit;
	}

	private void backupSetup()throws Exception {
		long initialDelay = 1;
		long period = Long.valueOf(System.getProperty("sandbox.file.backup.minutes", "5"));
	
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		periodicFileBackup = new PeriodicFileBackup();
		scheduleHandle = executor.scheduleAtFixedRate(periodicFileBackup, initialDelay,period,TimeUnit.MINUTES);
	}
	
	class PeriodicFileBackup implements Runnable {
		private String filePath = params.configurationDirectory + File.separator + "backups"
				+ File.separator + Utilities.getFileTimestamp()+ ".sbf";//just a default
		public PeriodicFileBackup(){
			String backupDirectory = params.configurationDirectory + File.separator + "backups";
			String backupsListFilePath = backupDirectory + File.separator + "backups.xml";
		
			ISandboxBackedUp backedUp = (ISandboxBackedUp)component;
			if (backedUp.getFile()!= null){
				//If this file already saved in the past,use same backup file name,else create new file name and save to properties
				Properties properties = new Properties();
				try {
					FileInputStream fileInputStream = new FileInputStream(new File(backupsListFilePath));
					properties.loadFromXML(fileInputStream);
				} catch(IOException ioe){
					params.printLine("Could not load backup properties file "+ backupsListFilePath, true, true);
				}
				String lastBackupFilePath = properties.getProperty(backedUp.getFile().getPath());
				if (lastBackupFilePath == null) {//Was NOT saved before!
					filePath = backupDirectory
							+ File.separator + backedUp.getFile().getName()+ "_" +
							Utilities.getFileTimestamp()+ ".sbf";
					properties.put(backedUp.getFile().getPath(),filePath);
					try {
						FileOutputStream fileOutputStream = new FileOutputStream(new File(backupsListFilePath));
						properties.storeToXML(fileOutputStream,"Last backup: "+ backupsListFilePath);
					}catch(Exception en){						params.printLine("Could not save backup properties file "+ backupsListFilePath, true, true);
					}
				}else {//old version already exists
					filePath = lastBackupFilePath;
				
					try {
						String[]newFileArray = sandbox.Utilities.getFileString(backedUp.getFile().getPath()).split("\n");
						List<String> newFilelist = new ArrayList<String>(newFileArray.length);
						for(String string : newFileArray){
							newFilelist.add(string);
						}
						String[]oldFileArray = sandbox.Utilities.getFileString(lastBackupFilePath).split("\n");
						List<String> oldFilelist = new ArrayList<String> (oldFileArray.length);
						for(String string : oldFileArray){
							oldFilelist.add(string);
						}
					
						//First check size just in case diff library not available
						boolean filesDiffer = false ;
						File backupFile = new File(filePath);
						if (backupFile.length()!= backedUp.getFile().length()){
							params.printLine(SandboxParameters.VISUAL_SEPARATOR, true, true);
							params.printLine("FILES DIFFER BY SIZE:", true, true);
							params.printLine(backedUp.getFile().getPath(), true, true);
							params.printLine(lastBackupFilePath, true, true);
							params.printLine(SandboxParameters.VISUAL_SEPARATOR, true, true);
							openPage(backupFile, "BACK");
						
							backedUp.setDirty(true);
							filesDiffer = true;						}
						if (filesDiffer){
							try {
								filePath = backupDirectory //Change to a new backup file,just for safety's sake
										+ File.separator + backedUp.getFile().getName()
										+ "_"	+ Utilities.getFileTimestamp()+ ".sbf";
								properties.put(backedUp.getFile().getPath(),filePath);
								FileOutputStream fileOutputStream = new FileOutputStream(new File(backupsListFilePath));
								properties.storeToXML(fileOutputStream,"Last backup: " + backupsListFilePath);
							}catch(Exception en){
								params.printLine ("Could not save backup properties file "+ backupsListFilePath, true, true);
							}
						}
					
					} catch(FileNotFoundException fnfe) {
						params.printLine(SandboxParameters.VISUAL_SEPARATOR,true,true);
						params.printLine("COULD NOT COMPARE FILES:",true,true);
						params.printLine(backedUp.getFile().getPath(),true,true);
						params.printLine(lastBackupFilePath,true,true);
						params.printLine(SandboxParameters.VISUAL_SEPARATOR,true,true);
					}catch(Exception en){
						params.printLine(SandboxParameters.VISUAL_SEPARATOR);
						params.printLine("COULD NOT COMPARE FILES:");
						params.printLine(backedUp.getFile().getPath());
						params.printLine(lastBackupFilePath);
						params.printLine(en.getStackTrace());
						params.printLine(SandboxParameters.VISUAL_SEPARATOR);
					}
				}
			}
		}
		@Override
		public void run(){
			ISandboxBackedUp backedUp = (ISandboxBackedUp)component;
			if (backedUp.isDirty()){
				backedUp.setDirty(false);
				backup();
			}
		}
	}

	private void openPage(File file, String tabName)throws IOException {
		SandboxTextPane pane = new SandboxTextPane();
		pane.setText(new String(Utilities.getFileBytes(file)));
		pane.setEditable(false);
	
		pane.setBackground(Color.LIGHT_GRAY);	
	
		JScrollPane scrollPane = new JScrollPane(pane);
		pane.addFocusListener(params.focusListener);
		int pageNumber = params.tabbedPane.getTabCount ();
		scrollPane.setName(limitTabLength(tabName));
		scrollPane.setToolTipText(file.getAbsolutePath());
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);		
		params.tabbedPane.add(scrollPane,pageNumber);
		printLine("in tab \""+ tabName + "\"");
		params.tabbedPane.setToolTipTextAt(pageNumber ,file.getAbsolutePath());
//		params.tabbedPane.setForegroundAt(params.tabbedPane.getSelectedIndex(), params.tabForegroundColor);
		params.tabbedPane.setSelectedIndex(pageNumber);
//		params.tabbedPane.setForegroundAt(pageNumber, params.tabSelectedColor);
	}
	
	public int showConfirmMessage(String message) {
		int theReturn = (new SandboxMessage("CHOOSE", message.toString(), params.getTabbedPane(), 20, true)).getStatus();
		return theReturn;
	}

	/**
	*@return the doneLoading
	*/
	public boolean isDoneLoading(){
		return doneLoading;
	}
	
	/**
	 * This is for timed backup
	 */
	protected void backup(){
		ISandboxBackedUp backedUp = (ISandboxBackedUp)component;
		try {
			Utilities.saveFileString(periodicFileBackup.filePath, backedUp.getBackupText());
			params.printLine(periodicFileBackup.filePath + " " + SandboxParameters.SANDBOX_COMBOBOX_COMMENT_START
					+ " File backed up: " + Utilities.getTime("HH:mm:ss") + SandboxParameters.SANDBOX_COMBOBOX_COMMENT_STOP, true, true);
		} catch (FileNotFoundException en){
			params.printLine("Failed back up " + periodicFileBackup.filePath, true, true);
			System.err.println("Could not back up file: "+ periodicFileBackup.filePath);
			showConfirmMessage("Could not back up file:\n\t" + periodicFileBackup.filePath
					+ " in " + this.getClass().getName()+ ".close()");

		} catch (IOException en){
			params.printLine("Failed back up " + periodicFileBackup.filePath, true, true);
			System.err.println("Could not back up file: "+ periodicFileBackup.filePath);
			showConfirmMessage("Could not back up file:\n\t" + periodicFileBackup.filePath
					+ " in " + this.getClass().getName()+ ".close()");
		} catch (SecurityException se) {
			params.printLine("Failed back up " + periodicFileBackup.filePath, true, true);
			System.err.println("Could not back up file: "+ periodicFileBackup.filePath);
			showConfirmMessage("Could not back up file due to security exception:\n\t" + periodicFileBackup.filePath
					+ " in " + this.getClass().getName()+ ".close()" + System.lineSeparator() + se.getMessage());
		}
	}
	
/*//////////////////////////////////DMH	
	protected void save() {
		ISandboxBackedUp backedUp = (ISandboxBackedUp)component;
		try {
			if (textPane.isEditable() && textPane.isDirty) {
				String[] saLines = textPane.getText().split("\\n");
				List<String> lines = new ArrayList<String>();
				for (String line : saLines) {
					lines.add(line);
				}
				sandboxProcess.saveFileText(lines);
			}
			backedUp.saveBackedUp();
		} catch (IOException se) {
			params.printLine(se);
		} catch (InterruptedException se) {
			params.printLine(se);
		} catch(SandboxException se){
			params.printLine(se);
		}
	}
/*/////////////////////////////////////	
	protected void save() { 
		ISandboxBackedUp backedUp = (ISandboxBackedUp)component;
		try {
			backedUp.saveBackedUp();
		} catch(SandboxException se){
			params.alert(se.getMessage());
			params.printLine(se);
		} catch(Throwable th) {
			String message = th.getMessage();
			if (message == null) {
				message = "File operation error.";
			}
			params.alert(message, params.iconStopped);
		}
	}
//*/////////////////////////////////////	
	
	
	
	public void close(){
		save();
		if (scheduleHandle != null){
			scheduleHandle.cancel(true);
		}
		new SandboxConnection(undoManager).close();
		new SandboxConnection(file).close();
		new SandboxConnection(component).close();
		new SandboxConnection(inputStream).close();
		if (sandboxProcess != null) {
			sandboxProcess.setDone(true);
		}
			
	}
}
