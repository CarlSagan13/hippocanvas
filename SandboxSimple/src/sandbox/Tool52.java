// Tool52.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;

@SandboxAnnotation(
		description = "Activate lost sandboxes",
		parameterDescriptions =	{
		},showInButtonPanel = true, showInTabPanel = true,isTest = false ,isTool = true)
		
public class Tool52 extends SandboxTestBase /*dmh327 */{

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		File directory = new File(params.getConfigurationDirectoryRoot());

		sandbox.SandboxFilenameFilter directoryFilter = new sandbox.SandboxFilenameFilter();
		directoryFilter.setRegex(".*");
		sandbox.SandboxFilenameFilter filenameFilter = new sandbox.SandboxFilenameFilter();
		filenameFilter.setRegex("sandbox\\.properties");
		List<String> closedSandboxNames = new ArrayList<String> ();
		for (File sandboxDirectory : sandbox.Utilities.listFiles(directory, directoryFilter, false, false, true, this)){
			if (sandbox.Utilities.listFiles(sandboxDirectory, filenameFilter, false, true, false, this).size() == 0){
				closedSandboxNames.add(sandboxDirectory.getName());
			}
		}
		
		Object[] chosenSandboxes = this.getValueFromWindowList(
				closedSandboxNames.toArray(),"Choose sandboxes to open");
		for (Object sandboxName : chosenSandboxes){
			String filePath = params.getConfigurationDirectoryRoot()+ System.getProperty("file.separator","/")
					+ sandboxName + System.getProperty("file.separator","/") + "sandbox.properties";
			printLine ("Saving "+ filePath);
			sandbox.Utilities.saveFileString(filePath,
					"#Just in case the XML doesn't work\n#Wed Jan 30 14:49:48 EST 2013");
		}
		//END TOOL
	}
}
