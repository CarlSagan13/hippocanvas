//SandboxParameters.java (erase me later)
package sandbox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import sandbox.SandboxFrameBase.PhoneRevealRunner;

public class SandboxParameters implements SandboxCommon {
	// final Log logger = LogFactory.getLog(getClass());

	public static final String VERSION = "2.14i-HS";
	public static final String ID = "MjQ4Njc0ODYy";
	public static final String AUTHOR = "RGF2aWQgTWFyc2hhbGwgSGFydGVy";

	public static SandboxParameters sandboxParameters = null;

	public static final int FRAME_HEIGHT = 800;
	public static final int FRAME_WIDTH = 1200;

	public static final int NUM_COLS = 2;

	public static final int NUM_ROWS = 9;
	private static int NUM_PARAMETERS = NUM_COLS * NUM_ROWS;
	public static final int MAX_PARAMETER_DEPTH = 1000;
	public static final String DATE_FORMAT = "ddMMMyy HH:mm:ss.SSSSSSS";
	public static final String DATE_DAY_FORMAT = "ddMMMyy";
	public static final String DATE_FORMAT_FILE_TIMESTAMP = "ddMMMyy_HH.mm.ss";
	public static final String DATE_FORMAT_TIMESTAMP = "HH.mm.ss.SSSSSSS";
	public static final String SANDBOX_PASSWORD = "SANDBOXPASSWORD";
	public static final String SANDBOX_FOCUSED = "SANDBOXFOCUSED";
	public static final String SANDBOX_WORKSPACE = "SBXWORKSPACE";
	public static final String SANDBOX_CMD_DIR = "SANDBOXCOMMANDS";
	public static final String SANDBOX_BLANK = "SANDBOXBLANK";
	public static final String SANDBOX_NULL = "SANDBOXNULL";
	public static final String SANDBOX_EMPTY = "SANDBOXEMPTY";
	public static final String SANDBOX_COMBOBOX_COMMENT_START = "/*SBC";
	public static final String SANDBOX_COMBOBOX_COMMENT_STOP = "SBC*/";
	public static final String SANDBOX_DIRECTORY = "SANDBOXDIRECTORY";
	public static final String SANDBOX_HOME = "SANDBOXHOME";
	public static final String SANDBOX_CLASSPATH = "SANDBOXCLASSPATH";
	public static final String SANDBOX_HOME_DIR = "SANDBOX_HOME";
	public static final String SANDBOX_COMBOBOX = "SANDBOXCBX";
	public static final String SANDBOX_CLIPBOARD = "SANDBOXCLIP";
	public static final String SANDBOX_JAVA_ENV_PROP_EXAMPLE = "{sbx-prop sandbox.name}";
	public static final String SANDBOX_JAVA_LITERAL_EXAMPLE = "{sbx-lit []}";
	public static final String SANDBOX_FUNCTION_BASE64 = "SBX-FUNC-BASE64";
	public static final String SANDBOX_FUNCTION_TEXT = "SBX-FUNC-TEXT";
	public static final String SANDBOX_FUNCTION_TEST = "SBX-FUNC-TEST";
	public static final String SANDBOX_FUNCTION_TIME = "SBX-FUNC-TIME";
	public static final String SANDBOX_FUNCTION_FIND = "SBX-FUNC-FIND";
	public static final String SANDBOX_FUNCTION_SET = "SBX-FUNC-SET";
	public static final String SANDBOX_FUNCTION_UNSET = "SBX-FUNC-UNSET";
	public static final String SANDBOX_FUNCTION_UNSET_EXAMPLE = "sbx.kbd.f1," + SANDBOX_FUNCTION_UNSET;
	public static final String SANDBOX_COMMAND_WORKSPACE = "SBXCMDWORKSPACE";
	public static final String SANDBOX_COMMAND_DIRECTORY = "commands";

	public static final String SANDBOX_UNDER_CONSTRUCTION = "***UNDER CONSTRUCTION***";
	public static final String UNDER_CONSTRUCTION = SANDBOX_UNDER_CONSTRUCTION;

	public static final String SANDBOX_COMMA_DELIMITER = "<SBCD>";
	public static final String SANDBOX_CLOSED = "SANDBOXCLOSED";
	public static final String SANDBOX_OPENED = "SANDBOXOPENED";
	public static final String SANDBOX_END_OF_FILE = "SANDBOXEOF";
	public static final String SANDBOX_TEST_VALUE = "SANDBOXTESTVALUE";

	// public static final String TRANSLATOR_MAPID = "TRANSLATORMAPID";
	public static final String TRANSLATOR_CONFIG_PACKAGE = "sandbox.translator";

	private static final long MAX_FILE_OPEN_SIZE = 1000000;
	protected static final int MAX_BYTE_ARRAY_SIZE = 1000000;
	// public static long MAX_STR ING_BUFFER_SIZE = 100000;
	// public static int MAX_FILE_LINES = 500000;
	public static final int DEFAULT_CONNECT_TIMEOUT = 1000;
	public static final String SBX_CONNECT_TIMEOUT_ENV_NAME = "sandbox.connect.timeout";
	public static final String SBX_PHONE_CALL_DURATION = "sbx.mseconds.between.phone.call";
	public static final String SBX_MSECONDS_OPEN_PHONE_CALL = "sbx.mseconds.open.phone.call";
	
	public static final String SBX_CONF_BORDER_COLOR = "sandbox.conf.borderColor";

	public static final int NUM_PARAMETERS_V1_0 = 18;

	public static final String SCRATCHPAD_PROPERTY_NAME = "scratchPadName";
	public static final String SCRATCHPAD_NAME = "scratchpad";
	public static final String POPPAD_NAME = "poppad";
	public static final String POPPADVALS_NAME = "poppadvals";
	public static final String FOCUSED_NAME = "focused";
	public static final String TRUEFALSE_NAME = "truefalse";
	public static final String YESNO_NAME = "yesno";
	public static final String PASSWORD_NAME = "password";
	public static final String BUTTON_PREFIX = "button";
	public static final String US_ASCII_ENCODED = "-encoded";
	public static final String DEFAULT_DELIMITER = "__";

	public static final int TOOLS_TAB_INDEX = 0;
	public static final int TESTS_TAB_INDEX = 1;
	public static final int COMMANDS_TAB_INDEX = 2;
	public static final int OUTPUT_TAB_INDEX = 3;
	public static final int LAST_STATIC_TAB = 4;
	public static final int SCRATCHPAD_TAB_INDEX = LAST_STATIC_TAB;

	public static final int PARAM_HIGHLIGHT_COLOR = 0xFFFFFF;
	public static final int PARAM_UN_HIGHLIGHT_COLOR = 0xC0C0C0;
	public static final int BUTTON_HIGHLIGHT_COLOR = Color.red.getRGB();
	public static final int BUTTON_UN_HIGHLIGHT_COLOR = Color.black.getRGB();
	public static final int TAB_FOREGROUND_COLOR = Color.black.getRGB();
	public static final int TAB_BACKGROUND_COLOR = Color.lightGray.getRGB();
	public static final int TAB_SELECTED_COLOR = Color.red.getRGB();
	public static final int BORDER_COLOR = Color.black.getRGB();

	public static final int COMBOBOX_BUTTON_FONT_SIZE = 11;
	public static final int COMBOBOX_BUTTON_POS_X = 3;
	public static final int COMBOBOX_BUTTON_POS_Y = 13;
	public static final int COMBOBOX_BUTTON_POS_X_ADJ = 4;

	public static final String FONT_DEFAULT = "Dialog_2_12";

	public static final String SEARCH_COMBOBOX_NAME = "Search Combobox";
	public static final String REPLACE_COMBOBOX_NAME = "Replace Combobox";
	public static final String MESSAGE_SEARCH_COMBOBOX_NAME = "Message Search Combobox";

	public static final String SANDBOX_SOUND_FILE = "sound/sandboxSoundFile.wav";

	public static final int MAX_NUM_TESTS = 200;
	public static final int MAX_NUM_PACKAGES = 100;
	public static final int BUTTONS_PER_ROW = 10;

	public static final String VISUAL_SEPARATOR = new String(new char[32]).replace("\0", "_");
	public static final String ELLIPSIS = "...";
	public static final String POPPAD_NOTES = "POPPAD NOTES:";
	public static final int POPPAD_MAX_LINES = 30;

	public static SandboxParameters createSandboxParameters() {
		return SandboxParameters.createSandboxParameters(false);
	}

	public static SandboxParameters createSandboxParameters(boolean guiLess) {
		if (sandboxParameters == null) {
			sandboxParameters = new SandboxParameters(guiLess);
		}
		return sandboxParameters;
	}

	protected static JFrame frame = null;
	protected static String sandboxDelimiter = System.getProperty("sandbox.delimiter", DEFAULT_DELIMITER);

	Globals globals = new Globals(this);
	Set<SandboxConnection> hiddenMessages = new CopyOnWriteArraySet<SandboxConnection> ();

	protected JTabbedPane tabbedPane = new JTabbedPane();
	protected int selectedTab = -1;
	protected int lastSelectedTab = -1;
	protected int preTestSelectedTab = -1;
//	protected List<Integer> tabHistory = new ArrayList<Integer> ();

	Properties properties = null;

	List<String> sandboxComboboxEntries = new ArrayList<String>();

	Color parameterHighlightColor = new Color(PARAM_HIGHLIGHT_COLOR);
	Color parameterUnHighlightColor = new Color(PARAM_UN_HIGHLIGHT_COLOR);
	Color buttonHighlightColor = new Color(BUTTON_HIGHLIGHT_COLOR);
	Color buttonUnHighlightColor = new Color(BUTTON_UN_HIGHLIGHT_COLOR);
//	Color tabForegroundColor = new Color(TAB_FOREGROUND_COLOR);
//	Color tabBackgroundColor = new Color(TAB_BACKGROUND_COLOR);
//	Color tabSelectedColor = new Color(TAB_SELECTED_COLOR);
	Color tabForegroundColor = null;
	Color tabBackgroundColor = null;
	Color tabSelectedColor = null;

	Icon iconRunning = null;
	Icon iconStopped = null;
	Icon iconWatch = null;
	Icon iconPretty = null;
	static String STATUS_RUNNING = "running";
	static String STATUS_STOPPED = "stopped";
	static String STATUS_WATCH = "watch";
	static String STATUS_PRETTY = "pretty";
	
	int numCols = NUM_COLS;
	int numRows = NUM_ROWS;
	int numParameters = NUM_PARAMETERS;
	int chicletX = -1;
	int chicletY = -1;

	int frameWidth = FRAME_WIDTH;
	int frameHeight = FRAME_HEIGHT;
	int numButtonsPerRow = BUTTONS_PER_ROW;
	long maxFileOpenSize = MAX_FILE_OPEN_SIZE;
	int maxByteArraySize = MAX_BYTE_ARRAY_SIZE;
//	int listFixedHeight = -1;

	int comboBoxButtonFontSize;
	int comboBoxButtonPositionX;
	int comboBoxButtonPositionY;
	int comboBoxButtonPositionXAdjustment;

	String sandboxSoundFile;
	
	String commandsDirectory = null;

	// int numTabs = LAST_STATIC_TAB + 1;
	// int selectedTab = LAST_STATIC_TAB + 1;
	int poppadMaxLines = POPPAD_MAX_LINES;

	public static String targetPackageName = null;
	static String passedPassword = PASSWORD_NAME;

//	public String fileSeparator = System.getProperty("file.separator");
//	public String lineSeparator = System.getProperty("line.separator");

	String sandboxHome = null;
	String configurationDirectoryRoot = null;
	String configurationDirectory = null;
	String configurationFileRoot = null;
	String userDir = null;

	public static String SANDBOX_DATE_FORMATS_ENV = "sandbox.date.formats";
	//String sandboxDateFormats = null;

	JPanel componentPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	//public JTextField status = null;

	JComponent testComponent = null;
	JPasswordField passwordComponent = null;
	JCheckBox trueFalse = new JCheckBox("True/False", false);
	JCheckBox yesNo = new JCheckBox("Yes/No", false);
	JLabel clock = new JLabel("00:00");
	int dayOfMonth = 0;
	JCheckBox showPopPad = new JCheckBox("Pop", false);
	JCheckBox announce = new JCheckBox("Announce", false);

	List<JComponent> parameterList = new ArrayList<JComponent>(NUM_PARAMETERS);
	List<SandboxTestBase> tests = null;
	Hashtable<String, JButton> buttons = new Hashtable<String, JButton>();

	public boolean isInitialized = false;

	final SandboxTextArea textAreaOutput = new SandboxTextArea();
//	final JTextArea textAreaOutput = new JTextArea(5, 80);

	SandboxList listTestDescriptions = new SandboxList();
	SandboxList listToolDescriptions = new SandboxList();
	SandboxList listCommandDescriptions = new SandboxList();
	String commandName = null; // TODO this might be unnecessary

	final JTextArea scratchPad = new JTextArea(5, 80);
//	final SandboxTextArea scratchPad = new SandboxTextArea();

	ComponentFocusListener focusListener = new ComponentFocusListener(this);
	SandboxKeyListener keyListener = new SandboxKeyListener(this);
	SandboxMouseListener mouseListener = new SandboxMouseListener();
	ComboBoxActionListener cbActionListener = new ComboBoxActionListener();

	SandboxTranslatorImpl translator = new SandboxTranslatorImpl();

	JComboBox<String> searchComboBox = new JComboBox<String>();
	JComboBox<String> replaceComboBox = new JComboBox<String>();
//	JComboBox<String> statusComboBox = new JComboBox<String>();
	
	private List<NotifyAdvice> listNotifyAdvice = new ArrayList<NotifyAdvice>();
	
	static BufferedImage sandboxImage = null;
//	static BufferedImage chicletImage = null;

	boolean guiLess = false;
	boolean alertUp = false; //For when JOptionPane is used, and would be obscurred by sandbox messages
	
	protected static int messageCount = 0;
	
	protected Properties environmentProps = new Properties();

	private SandboxParameters(boolean guiLess) {
		sandboxLogger.info("dmh1125 Instantiating " + SandboxParameters.class.getName());
		this.guiLess = guiLess;
		String systemFilePath = System.getProperty("user.dir") + File.separator + "system.xml";

		File systemSandboxFile = new File(systemFilePath);
		properties = new Properties();
		if (systemSandboxFile.exists() && systemSandboxFile.isFile()) {
			FileInputStream fileInputStream;
			try {
				printLine(systemFilePath, true, true);
				fileInputStream = new FileInputStream(systemSandboxFile);
				properties.loadFromXML(fileInputStream);
				printLine("Loaded sandbox system configuration file at " + Utilities.getTime(), true, true);
			} catch (Exception en) {
				printLine("Could not load sandbox system configuration file", true, true);
			}

		}
		sandboxHome = System.getProperty("sandbox.home",
				System.getProperty("user.home") + File.separator + SandboxParameters.SANDBOX_HOME_DIR);

		configurationDirectoryRoot = sandboxHome;
		configurationDirectory = configurationDirectoryRoot + File.separator + targetPackageName;

		configurationFileRoot = configurationDirectory + File.separator + targetPackageName;

		userDir = System.getProperty("user.dir", sandboxHome);

		// Make sure directories exist; if not,create
		try {
			File sandboxHome = new File(configurationDirectoryRoot);

			if (!sandboxHome.exists()) {
				Utilities.createSandboxHome(sandboxHome);
			}
			File sandboxDirectory = new File(configurationDirectory);
			if (!sandboxDirectory.exists()) {
				Utilities.createSandbox(sandboxHome, targetPackageName);
			}
		} catch (Throwable th) {
//			new SandboxMessage("COULD NOT CREATE ENVIRONMENT", "Could not create environment: " + th.getMessage(), tabbedPane, 3, false);
			System.err.println("java.class.path," + System.getProperty("java.class.path", "Not Defined"));
			th.printStackTrace();
			JOptionPane.showMessageDialog(tabbedPane, "COULD NOT CREATE ENVIRONMENT: " + th.getMessage());
			
			return;
		}

		boolean isLoaded = false;
		try {// Load properties from XML file
			// properties = new Properties();
			File file = new File(this.configurationDirectoryRoot);
			if (!file.exists()) {
				file.mkdir(); // Note: Logging creates these at the start,so
								// probably never called
			}
			file = new File(configurationDirectory);
			if (!file.exists()) {
				file.mkdir(); // Note: Logging creates these at the start,so
								// probably never called
			}
			file = new File(configurationDirectory + File.separator + "scratchpad.txt");
			if (!file.exists()) {
				try {
					this.scratchPad.setText(Utilities.getResourceStringInternal("start/sandbox/scratchpad.txt"));

				} catch (Exception en) {
					printLine("Could not get scratchpad text for " + configurationDirectory, true, true);
				}
			}

			file = new File(configurationFileRoot + ".xml");
			FileInputStream fileInputStream = new FileInputStream(file);
			properties.loadFromXML(fileInputStream);

			isLoaded = true;
			fileInputStream.close();

			sandbox.Utilities.copyFile(file, new File(file.getAbsolutePath() + ".bak"));

			file = null;
		} catch (FileNotFoundException fnfe) {
			printLine("Could not load properties from XML configuration file - 1", fnfe);
		} catch (IOException ioe) {
			printLine("Could not load properties from XML configuration file - 2", ioe);
			File sourceFile = new File(configurationFileRoot + ".xml.bak");
			File targetFile = new File(configurationFileRoot + ".xml");
			String message = "The configuration file is empty! Copy the backup to the current before starting again: "
					+ sourceFile.getPath();

			int result = JOptionPane.showConfirmDialog(tabbedPane, message);

			if (result == 0) {
				try {
					Utilities.copyFile(sourceFile, targetFile, true);
				} catch (IOException ioe2) {
					JOptionPane.showMessageDialog(tabbedPane, "Could not copy " + sourceFile.getPath());
				}
			}

			System.err.println("Status = " + result);

			return;
		} catch (java.lang.AbstractMethodError ame) {
			printLine("Could not load properties from XML configuration file - 3", ame);
		}
		

		
		if (!isLoaded) {// XML configuration not loaded,so try properties file
			try {
				properties = new Properties();
				String filePath = configurationDirectory + File.separator + "sandbox.properties";
				printLine("Attempting to load from backup properties file: " + filePath);
				File file = new File(filePath);
				FileInputStream fileInputStream = new FileInputStream(file);
				properties.load(fileInputStream);
				printLine("Successfully loaded from properties configuration file.");
			} catch (FileNotFoundException fnfe) {
				printLine("Could not load properties from configuration file", fnfe);
			} catch (IOException ioe) {
				printLine("Could not load properties from configuration file", ioe);
			}
		}
		if (!isLoaded) {// Neither XML configuration nor properties were
						// loaded,so try loading default
			try {
				this.properties = new Properties();
				String filePath = targetPackageName + File.separator + "sandbox.properties";
				printLine("Loading from default configuration file: " + filePath);
				File file = new File(filePath);
				FileInputStream fileInputStream = new FileInputStream(file);
				properties.load(fileInputStream);
				printLine("Successfully loaded from default configuration file.");

				// copy all default configuration files to user configuration
				// directory
				File sourceDirectory = new File(targetPackageName);
				SandboxFilenameFilter filter = new SandboxFilenameFilter();
				Collection<File> files = sandbox.Utilities.listFilesDirs(sourceDirectory, filter, false);

				Iterator<File> iterFiles = files.iterator();
				while (iterFiles.hasNext()) {
					File configFile = iterFiles.next();
					String fileName = configFile.getName();
					File newFile = new File(configurationDirectory + File.separator + fileName);
					sandbox.Utilities.copyFile(configFile, newFile);
				}
			} catch (FileNotFoundException fnfe) {
				printLine("Could not load properties from configuration file", fnfe);
			} catch (IOException ioe) {
				printLine("Could not load properties from default configuration file", ioe);
			}
		}
		try {
			frameWidth = Integer.parseInt((String) properties.get("frameWidth"));
			frameHeight = Integer.parseInt((String) properties.get("frameHeight"));
		} catch (Exception en) {
			frameWidth = FRAME_WIDTH;

			frameHeight = FRAME_HEIGHT;
		}

		try {
			numCols = Integer.parseInt(System.getProperty("sandbox.conf.numCols", (String) properties.get("numCols")));
			numRows = Integer.parseInt(System.getProperty("sandbox.conf.numRows", (String) properties.get("numRows")));
			numParameters = numCols * numRows;
		} catch (java.lang.NumberFormatException nfe) {
			numCols = NUM_COLS;
			numRows = NUM_ROWS;
			numParameters = NUM_PARAMETERS;
			System.out.println("No parameter formatting loaded.");
		}

		
		try { //chiclets
			chicletX = Integer.parseInt(System.getProperty("sandbox.conf.chicletX", (String) properties.get("chicletX")));
			chicletY = Integer.parseInt(System.getProperty("sandbox.conf.chicletY", (String) properties.get("chicletY")));
		} catch (java.lang.NumberFormatException nfe) {
			System.out.println("No chiclet xy set.");
		}


		
		
		
		try {
			numButtonsPerRow = Integer.parseInt(
					System.getProperty("sandbox.conf.numButtonsPerRow", (String) properties.get("numButtonsPerRow")));
		} catch (java.lang.NumberFormatException nfe) {
			numButtonsPerRow = BUTTONS_PER_ROW;
			System.out.println("No buttons-per-row in configuration.");
		}

//		try {
//			listFixedHeight = Integer.parseInt(
//					System.getProperty("sandbox.conf.listFixedHeight", (String) properties.get("listFixedHeight")));
//		} catch (java.lang.NumberFormatException nfe) {
//			listFixedHeight = -1;
//			System.out.println("No listFixedHeight in configuration.");
//		}

		try {
			this.maxByteArraySize = Integer.parseInt(
					System.getProperty("sandbox.conf.maxByteArraySize", (String) properties.get("maxByteArraySize")));
		} catch (java.lang.NumberFormatException nfe) {
			maxByteArraySize = MAX_BYTE_ARRAY_SIZE;
			System.out.println("No maxByteArraySize in configuration.");
		}

		try {
			this.maxFileOpenSize = Integer.parseInt(
					System.getProperty("sandbox.conf.maxFileOpenSize", (String) properties.get("maxFileOpenSize")));
		} catch (java.lang.NumberFormatException nfe) {

			maxFileOpenSize = MAX_FILE_OPEN_SIZE;
			System.out.println("No maxFileOpenSize in configuration.");
		}
		try {
			if (properties.get("tabForegroundColor") != null) {
				tabForegroundColor = new Color(
						Integer.parseInt((String) properties.get("tabForegroundColor"), 16));
			}
			if (properties.get("tabBackgroundColor") != null) {
				tabBackgroundColor = new Color(
						Integer.parseInt((String) properties.get("tabBackgroundColor"), 16));
			}
			if (properties.get("tabSelectedColor") != null) {
				tabSelectedColor = new Color(
						Integer.parseInt((String) properties.get("tabSelectedColor"), 16));
			}
			parameterHighlightColor = new Color(
					Integer.parseInt((String) properties.get("parameterHighlightColor"), 16));
			parameterUnHighlightColor = new Color(
					Integer.parseInt((String) properties.get("parameterUnHighlightColor"), 16));
			buttonHighlightColor = new Color(Integer.parseInt((String) properties.get("buttonHighlightColor"), 16));
			buttonUnHighlightColor = new Color(Integer.parseInt((String) properties.get("buttonUnHighlightColor"), 16));
		} catch (Exception en) {
			printLine("Using standard colors", true, true);
			sandboxLogger.info("Using standard colors");
		}

		try {
			comboBoxButtonFontSize = Integer.parseInt((String) properties.get("comboBoxButtonFontSize"));
			comboBoxButtonPositionX = Integer.parseInt((String) properties.get("comboBoxButtonPositionX"));
			comboBoxButtonPositionY = Integer.parseInt((String) properties.get("comboBoxButtonPositionY"));
			comboBoxButtonPositionXAdjustment = Integer.parseInt((String) properties.get("comboBoxButtonPositionXAdjustment"));
		} catch (Exception en) {
			comboBoxButtonFontSize = COMBOBOX_BUTTON_FONT_SIZE;
			comboBoxButtonPositionX = COMBOBOX_BUTTON_POS_X;
			comboBoxButtonPositionY = COMBOBOX_BUTTON_POS_Y;
			comboBoxButtonPositionXAdjustment = COMBOBOX_BUTTON_POS_X_ADJ;
		}

		try {

			this.poppadMaxLines = Integer.parseInt((String) properties.get("poppadMaxLines"));
		} catch (Exception en) {
			this.poppadMaxLines = POPPAD_MAX_LINES;
		}

		sandboxSoundFile = (String) properties.get("sandboxSoundFile");
		if (Utilities.noNulls(sandboxSoundFile).equals(""))
			sandboxSoundFile = SANDBOX_SOUND_FILE;

		textAreaOutput.addKeyListener(keyListener);
		textAreaOutput.addFocusListener(focusListener);
		textAreaOutput.addMouseListener(mouseListener);
		
		String[] clf = Utilities.getEnvFont();
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		} catch(Throwable th) {
			System.err.println(th);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
		scratchPad.setFont(textFont); // Re23.04.10.11.55.19.248
		tabbedPane.setFont(textFont);
		tabbedPane.setForeground(tabForegroundColor);
		tabbedPane.setBackground(tabBackgroundColor);

		
		
		
		//tabbedPane.setBackground(Color.red); //dmh1018

		
		
		scratchPad.addKeyListener(keyListener);
		
		scratchPad.addMouseListener(mouseListener);

//		listTestDescriptions.setFont(thisFont);
		
		
//		listTestDescriptions.addKeyListener(new SandboxListKeyListener(this));
//		listTestDescriptions.addKeyListener(keyListener);
		listTestDescriptions.addFocusListener(focusListener);

//		listToolDescriptions.setFont(thisFont);
//		listToolDescriptions.addKeyListener(new SandboxListKeyListener(this));
//		listToolDescriptions.addKeyListener(keyListener);
		listToolDescriptions.addFocusListener(focusListener);
		
		listCommandDescriptions.addFocusListener(focusListener);
		
		searchComboBox.setEditable(true);
		searchComboBox.setName(SEARCH_COMBOBOX_NAME);
		searchComboBox.setFont(textFont);
		searchComboBox.setMaximumRowCount(20);
		SandboxIcon searchIcon = new SandboxIcon("S", comboBoxButtonPositionX + comboBoxButtonPositionXAdjustment, 
				comboBoxButtonPositionY, 10, 10, Color.BLACK);
		searchComboBox.setUI(new SandboxComboBoxUI(searchIcon, comboBoxButtonFontSize));
		searchComboBox.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		Component searchComponent = searchComboBox.getEditor().getEditorComponent();
		searchComponent.addFocusListener(focusListener);

		searchComboBox.addActionListener(cbActionListener);
		searchComboBox.getEditor().getEditorComponent().addKeyListener(keyListener);
		
		replaceComboBox.setEditable(true);
		replaceComboBox.setName(SEARCH_COMBOBOX_NAME);
		replaceComboBox.setFont(textFont);
		replaceComboBox.setMaximumRowCount(20);
		SandboxIcon replaceIcon = new SandboxIcon("R", comboBoxButtonPositionX + comboBoxButtonPositionXAdjustment, 
				comboBoxButtonPositionY, 10, 10, Color.BLACK);
		replaceComboBox.setUI(new SandboxComboBoxUI(replaceIcon, comboBoxButtonFontSize));
		replaceComboBox.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		Component replaceComponent = searchComboBox.getEditor().getEditorComponent();
		replaceComponent.addFocusListener(focusListener);

		replaceComboBox.addActionListener(cbActionListener);
		replaceComboBox.getEditor().getEditorComponent().addKeyListener(keyListener);
		
		parameterList.add(null);
		for (int i = 1; i <= numParameters; i++) {
			JComboBox comboBox = new JComboBox();
			comboBox.setEditable(true);
			comboBox.setName("Parameter " + i);
			comboBox.setFont(textFont);
			comboBox.setMaximumRowCount(20);

			int iconX = comboBoxButtonPositionX;

			int iconY = comboBoxButtonPositionY;

			if (i < 10) {
				iconX = comboBoxButtonPositionX + comboBoxButtonPositionXAdjustment;
			}
			SandboxIcon icon = new SandboxIcon(String.valueOf(i), iconX, iconY, 10, 10, Color.BLACK);
			comboBox.setUI(new SandboxComboBoxUI(icon, comboBoxButtonFontSize));
			comboBox.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

			parameterList.add(i, comboBox);

			Component component = comboBox.getEditor().getEditorComponent();
			component.addFocusListener(focusListener);

			comboBox.addActionListener(cbActionListener);
			comboBox.getEditor().getEditorComponent().addKeyListener(keyListener);
			comboBox.getEditor().getEditorComponent().addMouseListener(new SandboxMouseListener());
		}

		passwordComponent = new JPasswordField(passedPassword);

		// Handle focus
		scratchPad.addFocusListener(focusListener);
		textAreaOutput.addFocusListener(focusListener);
		JComponent component = parameterList.get(1);
		component.requestFocus();

		loadConfiguration(properties);

		/**
		 * Load sandbox environment values into system environment from file
		 */
		FileInputStream environmentFIS = null;
		String filepath = configurationDirectory + File.separator + "environment.xml";
		printLine("filepath = " + filepath);

		try {
			File environmentFile = new File(filepath);
			environmentFIS = new FileInputStream(environmentFile);
//			Properties environmentProps = new Properties();
			environmentProps.loadFromXML(environmentFIS);
			Enumeration<?> environmentPropNames = environmentProps.propertyNames();

			while (environmentPropNames.hasMoreElements()) {
				String propName = (String) environmentPropNames.nextElement();
				
				
/*////////////////////////DMH
//TODO every two/three/four weeks Re22.02.05.16.49.56.022
				if (propName.startsWith("sbx.time")) {

				String[] timeStrings = systemProps.getProperty(propName, "10:15__01:05__5__No+eso+set")
						.split(sandboxDelimiter);
				String day = dayFormat.format(now);

			} else if (timeStrings[0].matches("^[a-zA-Z]{3}\\.\\d\\d:\\d\\d.*$")) { //DOTW

				
//*///////////////////////////
				
				
				
				
				System.setProperty(propName, environmentProps.getProperty(propName));
//				printLine(propName + "," + environmentProps.getProperty(propName));
			}
			environmentFIS.close();
		} catch (IOException ioe) {
			sandboxLogger.log(Level.ALL, "Unable to load system environment from " + filepath, ioe);
		}

//		sandboxDateFormats = System.getProperty(SANDBOX_DATE_FORMATS_ENV, DATE_FORMAT);
		String[] sandboxDateFormats = System.getProperty(SandboxParameters.SANDBOX_DATE_FORMATS_ENV, 
				SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP).split(",");


		String filePath = configurationDirectory + File.separator + "sandbox.properties";
		if (!guiLess) {
			try {
				boolean success = (new File(filePath)).delete();
				if (!success) {
					throw new SandboxException("Could not delete " + filePath);
				}
			} catch (Exception en) {
				printLine("Could not delete " + filePath);

			}
		}

		iconPretty = Utilities.resizeIcon(new File(userDir + File.separator + "start" + File.separator + "images" + File.separator + "blueball.png"), 
				Integer.valueOf(properties.getProperty("iconSize", "50")));
		iconWatch = Utilities.resizeIcon(new File(userDir + File.separator + "start" + File.separator + "images" + File.separator + "yellowball.png"), 
				Integer.valueOf(properties.getProperty("iconSize", "50")));
		iconRunning = Utilities.resizeIcon(new File(userDir + File.separator + "start" + File.separator + "images" + File.separator + "greenball.png"), 
				Integer.valueOf(properties.getProperty("iconSize", "50")));
		iconStopped = Utilities.resizeIcon(new File(userDir + File.separator + "start" + File.separator + "images" + File.separator + "redball.png"), 
				Integer.valueOf(properties.getProperty("iconSize", "50")));
		
//		iconPretty = new ImageIcon(userDir + fileSeparator + "images" + fileSeparator + "blueDot.gif", STATUS_PRETTY);
//		iconWatch = new ImageIcon(userDir + fileSeparator + "images" + fileSeparator + "yellowDot.gif", STATUS_WATCH);
//		iconRunning = new ImageIcon(userDir + fileSeparator + "images" + fileSeparator + "greenDot.gif", STATUS_RUNNING);
//		iconStopped = new ImageIcon(userDir + fileSeparator + "images" + fileSeparator + "redDot.gif", STATUS_STOPPED);

		
		if (!guiLess) {
			try {
				String hippoCanvasImageFilePath = System.getProperty("sbx.home.icon.file.path", "blueball.png");
				sandboxImage = ImageIO.read(new File(hippoCanvasImageFilePath));
				Graphics2D g2d = sandboxImage.createGraphics();
				String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
				Color borderColor = new Color(255, 255, 255);
				try {
					borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]), Integer.parseInt(ebc[2]));
				} catch (NumberFormatException nfe) {
					sandboxParameters.printLine("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");
				}
				g2d.setColor(borderColor);
				g2d.fillRect(0, 3 * (sandboxImage.getHeight() / 4), sandboxImage.getWidth(), sandboxImage.getHeight() / 4);
				
				//create chicklet graphic
//				String chicletImageFilePath = System.getProperty("sbx.home.icon.file.path", "blueball.png");
//				chicletImage = ImageIO.read(new File(chicletImageFilePath));
//				Graphics2D g2dChiclet = chicletImage.createGraphics();
//				g2dChiclet.setColor(borderColor);
//				g2dChiclet.fillRect(0, 3 * (chicletImage.getHeight() / 4), chicletImage.getWidth(), chicletImage.getHeight());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

		Timer clockTimer = new Timer(10000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat hourMinuteSecondFormat = new SimpleDateFormat("MM/dd HH:mm");
				Date now = new Date();
				String strNow = hourMinuteSecondFormat.format(now);
				clock.setText(strNow);
				
				int dom = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
				if (dom != dayOfMonth) {
					// restart at the start of every day
					String[] packages = {targetPackageName};
					((sandbox.SandboxFrameBase) frame).closeSandbox(packages);
				}
				dayOfMonth = dom;
			}
		});

		if (System.getProperty("sbx.time", "no").equals("yes")) {
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

			TimeRunner timeRunner = new TimeRunner();
			scheduleHandle = executor.scheduleAtFixedRate(timeRunner, 1, 1, TimeUnit.MINUTES);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");

			clock.setText(dateFormat.format(new Date()));

			clockTimer.setRepeats(true);
			clockTimer.start();
		}

//		tabbedPane.setBackground(Color.RED);
		tabbedPane.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent event) {
				int keyCode = event.getKeyCode();
				JTabbedPane tabbedPane = (JTabbedPane) event.getSource();
				StringBuilder title = new StringBuilder(tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));

				// CONTROL KEY EVENTS
				if (event.isControlDown()) {// Control key events
					int selectedIndex = 0;
					switch (keyCode) {
					case KeyEvent.VK_LEFT:
						selectedIndex = tabbedPane.getSelectedIndex();
						int destinationIndex = selectedIndex - 1;
						if (destinationIndex > SandboxParameters.LAST_STATIC_TAB) {
							moveTab(tabbedPane, selectedIndex, destinationIndex);
						}
						break;

					case KeyEvent.VK_RIGHT:
						selectedIndex = tabbedPane.getSelectedIndex();
						destinationIndex = selectedIndex + 1;
						if ((selectedIndex > SandboxParameters.LAST_STATIC_TAB)
								&& (selectedIndex < tabbedPane.getTabCount() - 1)) {
							moveTab(tabbedPane, selectedIndex, destinationIndex);
						}
						break;

//					case KeyEvent.VK_C: //copy the tool tip to clipboard
//						
//						try {
//							selectedIndex = tabbedPane.getSelectedIndex();
//							tabbedPane.getToolTipTextAt(selectedIndex);
//							Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
//							java.awt.datatransfer.Transferable tText = new StringSelection(tabbedPane.getToolTipTextAt(selectedIndex));
//							clip.setContents(tText, null);
//						} catch(Throwable th) {
//							alert("Could not replace clipboard!", iconStopped);
//							printLine("Could not replace clipboard!", th);
//						}
//						
//						destinationIndex = selectedIndex + 1;
//						if ((selectedIndex > SandboxParameters.LAST_STATIC_TAB)
//								&& (selectedIndex < tabbedPane.getTabCount() - 1)) {
//							moveTab(tabbedPane, selectedIndex, destinationIndex);
//						}
//						break;

					case KeyEvent.VK_D:
						selectedIndex = tabbedPane.getSelectedIndex();
						if ((selectedIndex > SandboxParameters.LAST_STATIC_TAB)
								&& (selectedIndex < tabbedPane.getTabCount() - 1)) {
							int tabToDelete = tabbedPane.getTabCount();
							boolean closed = true;
							int count = 0;

							while(tabToDelete > selectedIndex) { 
								try {
									closed = deleteTab(tabbedPane, --tabToDelete);
									count++;
								} catch(SandboxException se) {
									se.printStackTrace();
									closed = false;
								} catch(Throwable th) {
									th.printStackTrace();
								}
							}
							sandboxParameters.printLine("Successfully deleted " + count + " tabs", true, true);
						}
						break;
						
					default:
					}
				} else {// For editing tab titles (Ctrl is NOT down)
					switch (keyCode) {
					case KeyEvent.VK_DELETE:
						if ((tabbedPane.getSelectedIndex() > SandboxParameters.LAST_STATIC_TAB)) {
							int tabToDelete = tabbedPane.getSelectedIndex();
							try {
								deleteTab(tabbedPane, tabToDelete);
								sandboxParameters.printLine("Successfully deleted tab " + tabToDelete, true, true);
							} catch (SandboxException e) {
								// TODO Auto-generated catch block
								sandboxParameters.printLine("Could not tab " + tabToDelete, true, true);
								e.printStackTrace();
							}
//							tabbedPane.remove(tabbedPane.getSelectedIndex());
						}
						break;

					case KeyEvent.VK_BACK_SPACE:
						if (title.length() > 0 && (tabbedPane.getSelectedIndex() > SandboxParameters.LAST_STATIC_TAB)) {
							tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(),
									title.deleteCharAt(title.length() - 1).toString());
						}
						break;

					case KeyEvent.VK_ENTER:
						break;
					case KeyEvent.VK_SHIFT:
						break;
					default:
						if (!event.isActionKey() && tabbedPane.getSelectedIndex() > SandboxParameters.LAST_STATIC_TAB) {
							title.append(event.getKeyChar());
							tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), title.toString());
						}
						break;
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent event) {
				int keyCode = event.getKeyCode();
			}

			@Override
			public void keyReleased(KeyEvent event) {
				int keyCode = event.getKeyCode();
			}

			private void moveTab(JTabbedPane tabbedPane, int selectedIndex, int destinationIndex) {
				Component comp = tabbedPane.getComponentAt(selectedIndex);
				String label = tabbedPane.getTitleAt(selectedIndex);
				Icon icon = tabbedPane.getIconAt(selectedIndex);
				Icon iconDis = tabbedPane.getDisabledIconAt(selectedIndex);
				String tooltip = tabbedPane.getToolTipTextAt(selectedIndex);

				boolean enabled = tabbedPane.isEnabledAt(selectedIndex);
				int keycode = tabbedPane.getMnemonicAt(selectedIndex);

				int mnemonicLoc = tabbedPane.getDisplayedMnemonicIndexAt(selectedIndex);
				Color fg = tabbedPane.getForegroundAt(selectedIndex);
				Color bg = tabbedPane.getBackgroundAt(selectedIndex);
				tabbedPane.remove(selectedIndex);

				tabbedPane.insertTab(label, icon, comp, tooltip, destinationIndex);

				tabbedPane.setDisabledIconAt(destinationIndex, iconDis);

				tabbedPane.setEnabledAt(destinationIndex, enabled);
				tabbedPane.setMnemonicAt(destinationIndex, keycode);

				tabbedPane.setDisplayedMnemonicIndexAt(destinationIndex, mnemonicLoc);
				tabbedPane.setForegroundAt(destinationIndex, fg);

				tabbedPane.setBackgroundAt(destinationIndex, bg);
				tabbedPane.setSelectedIndex(destinationIndex);
				
				
				SandboxParameters params = SandboxParameters.createSandboxParameters();
				params.lastSelectedTab = destinationIndex;
				

			}

			private boolean deleteTab(JTabbedPane tabbedPane, int index) throws SandboxException {
				JComponent comp = getTabComponent(index);
				if (comp == null) {
					return false;
				}
				if (comp instanceof ISandboxBackedUp) {
					ISandboxBackedUp backedUp = (ISandboxBackedUp) comp;
					if (backedUp != null) {
						backedUp.setDirty(true); // just to be sure
						backedUp.closeBackedUp();
						if (backedUp.getMapId() != null) {
							sandboxParameters.removeFromGlobalMap(backedUp.getMapId());
						}
//						if (backedUp.getFile() != null) {
//							printLine("Closed " + backedUp.getFile().getAbsolutePath(), true, true);
//						}
						printLine(SandboxParameters.VISUAL_SEPARATOR, true, true);
					}
				} else if (comp instanceof SandboxTextArea) {
					SandboxTextArea textArea = (SandboxTextArea) comp;
					SandboxTestBase test = textArea.getTest();
					SandboxThread sandboxThread = textArea.getSandboxThread();
					if (test != null && test.isRunning) {
						test.endMe();
//						sandboxParameters.alert("Must cancel running OS-based tests");
//						return false;
					} else if (sandboxThread != null && sandboxThread.isRunning()) {
						sandboxThread.close();
						sandboxParameters.tabbedPane.setIconAt(sandboxParameters.tabbedPane.getSelectedIndex(), null);
					} else {
						sandboxParameters.tabbedPane.removeTabAt(sandboxParameters.tabbedPane.getSelectedIndex());
					}
				}
				try {
					tabbedPane.remove(index);
					sandboxParameters.selectTab(index - 1);
					tabbedPane.setSelectedIndex(index - 1);
					return true;
				} catch(IndexOutOfBoundsException ioobe) {
					return false;
				}
			}
		});

		tabbedPane.addMouseListener(new MouseListener() { //Re23.03.11.10.05.08.398
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getComponent() instanceof JTabbedPane) {
					
					JTabbedPane pane = (JTabbedPane) e.getComponent();
					//int selectedIndex = pane.getSelectedIndex();

					SandboxParameters params = SandboxParameters.createSandboxParameters(true);
					params.selectedTab = pane.getSelectedIndex();
					
					boolean staticTab = true;
					if (selectedTab > SandboxParameters.LAST_STATIC_TAB) {
						staticTab = false;
					}
					
					if (pane.isEnabledAt(selectedTab) && selectedTab >= 0) {
						if (e.getClickCount() == 2) {
							System.err.println("boonda1627");
							new SandboxTestTab(sandboxParameters, (e.getButton() == 3), staticTab);
							if (sandboxParameters.lastSelectedTab == 0) {
								sandboxParameters.selectTab(1);
								sandboxParameters.tabbedPane.setSelectedIndex(1);
							} else {
								sandboxParameters.selectTab(sandboxParameters.lastSelectedTab - 1);
								sandboxParameters.tabbedPane.setSelectedIndex(sandboxParameters.lastSelectedTab);
							}
							
							Component component = sandboxParameters.getSelectedTabComponent();
							if (component instanceof SandboxTextArea) {
								SandboxTextArea textArea = (SandboxTextArea) component;
								if (textArea.getTest() != null) {
									sandboxParameters.announce.setEnabled(textArea.getTest().isRunning());
									sandboxParameters.announce.setSelected(textArea.getTest().announce);
								}
							} else {
								sandboxParameters.announce.setEnabled(false);
								sandboxParameters.announce.setSelected(false);
							}
							
							return;
						}
						
						
						
						
						
						if (e.getClickCount() == 1 && (selectedTab != params.lastSelectedTab)) {
							params.selectTab(params.selectedTab);
//							return;
						}
						
						
						if (e.getClickCount() == 1 && !staticTab) {
							
							
							params.selectTab(params.selectedTab);
							String tabToolTip = tabbedPane.getToolTipTextAt(params.selectedTab);
							if (tabToolTip != null && (new File(tabToolTip)).exists() && e.getButton() == 3) {
								alert(tabToolTip); //Re23.03.11.10.05.08.398
							}
							
							
							
							
							if (pane.getIconAt(selectedTab) != null
									&& pane.getIconAt(selectedTab).equals(iconStopped)) {
								pane.setIconAt(selectedTab, null);
							}
							Component component = sandboxParameters.getSelectedTabComponent();
							if (component instanceof SandboxTextArea) {
								SandboxTextArea textArea = (SandboxTextArea) component;
								if (textArea.getTest() != null) {
									sandboxParameters.announce.setEnabled(textArea.getTest().isRunning());

									sandboxParameters.printLine(
											"textArea.getTest().isRunning() :: " + textArea.getTest().isRunning(), true,
											true);
									sandboxParameters.announce.setSelected(textArea.getTest().announce);

									sandboxParameters.printLine(
											"textArea.getTest().announce :: " + textArea.getTest().announce, true,
											true);
									if (textArea.getTest().isRunning) {
										tabbedPane.setIconAt(selectedTab, iconRunning);
									}
								}
							} else {
								sandboxParameters.announce.setEnabled(false);
								sandboxParameters.announce.setSelected(false);
							}

						}
					} else {
						sandboxParameters.announce.setEnabled(false);
						sandboxParameters.announce.setSelected(false);
					}
					params.lastSelectedTab = selectedTab;
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// new SandboxMessage("title","message",3,false);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// sandboxParameters.printLine("dmh309c");
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// sandboxParameters.printLine("dmh309d");
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// sandboxParameters.printLine("dmh309e");
			}
		});

		File adviceDirectory = new File(configurationDirectoryRoot + File.separator + "advice");
		if (adviceDirectory.exists() && adviceDirectory.isDirectory()) {
			Pattern pattern = Pattern.compile(".*\\.sba$", Pattern.MULTILINE);
			for (File adviceFile : Utilities.listFilesRegex(adviceDirectory, false, true, pattern, null)) {
				try {
					printLine("Advice file:\n\t" + adviceFile.getName());
					listNotifyAdvice.addAll(NotifyAdvice.getAdvice(adviceFile, this)); //Re23.03.12.17.23.05.269-problem
				} catch (Throwable th) {
					String msg = "Could not use advice file:\n\t" + adviceFile.getPath();
					sandboxLogger.fine(msg);
					printLine(msg);
					th.printStackTrace();
				}
			}
			printALine();
			for (NotifyAdvice advice : listNotifyAdvice) {
				printLine(advice.toString());
			}			
			printALine();
		}
		printLine("Advice size: " + configurationDirectoryRoot + " " + listNotifyAdvice.size());
		
		if (chicletX >= 0 && chicletY >= 0) {
			Utilities.showChiclet(this, chicletX, chicletY);
		}
		
		Enumeration<?> environmentPropNames = environmentProps.propertyNames();

		printLine("environment properties:");
		printALine();
		while (environmentPropNames.hasMoreElements()) {
			String propName = (String) environmentPropNames.nextElement();
			printLine(propName + "," + environmentProps.getProperty(propName));
		}
		printALine();
		
	} //construction ends here

	public int getSelectedTab() {
		return tabbedPane.getSelectedIndex();
	}

	// public void setSelectedTab(int selectedTab){
	// tabbedPane.setSelectedIndex(selectedTab);
	// }
	public ComponentFocusListener getFocusListener() {
		return focusListener;
	}

	public void setFocusListener(ComponentFocusListener focusListener) {
		this.focusListener = focusListener;
	}

	void printLine() {
		printLine("");
	}

	void printLine(Object o, boolean showInStandardOut, boolean noOutputTab) {
		String string = String.valueOf(o);
		if (string != null) {
			if (showInStandardOut)
				System.out.println(string);
			try {
				this.textAreaOutput.append(string + "\n");
		 	 	 Document doc = this.textAreaOutput.getDocument(); 
		 	 	 this.textAreaOutput.setCaretPosition(doc.getLength()); 
				if (!noOutputTab) {
					this.textAreaOutput.paint(this.textAreaOutput.getGraphics());
					JTabbedPane tabbedPane = (JTabbedPane) this.textAreaOutput
							.getParent().getParent().getParent();
					tabbedPane.setSelectedIndex(OUTPUT_TAB_INDEX);
				} else {// status
					sandboxLogger.info(string);
				}
			} catch (NullPointerException npe) {
				sandboxLogger.fine("Not printing to GUI");
			}
		}
	}

	void printLine(Object o, Throwable ex) {
		printLine(o, ex, true);
	}
	
	void printLine(Object o, Throwable ex, boolean selectOutputTab) {
		String s = String.valueOf(o);
		if (s == null) {
			s = new String();
		}
		for (StackTraceElement element : ex.getStackTrace()) {
			s = s + "\n\t" + element.toString();
		}
		sandboxLogger.info(s + "\n\t");
		try {
			this.textAreaOutput.append(s + "\n");
			Document doc = this.textAreaOutput.getDocument();
			this.textAreaOutput.setCaretPosition(doc.getLength());
			if (selectOutputTab) {
				tabbedPane.setSelectedIndex(OUTPUT_TAB_INDEX);
			}
		} catch(IndexOutOfBoundsException ioobe) {
			sandboxLogger.fine("Not printing to GUI - 1");
		} catch (NullPointerException npe) {
			sandboxLogger.fine("Not printing to GUI");
		}
	}

	void printLine(Object o, boolean stdOut) {
		printLine(o, stdOut, true);
	}

	void printLine(boolean notGui, Object o) {
		String s = String.valueOf(o);
		if (s == null) {
			s = new String();
		}
		if (notGui) {
			System.out.println(s);
		} else {
			printLine(s);
		}
	}

	public void printLine(Object o) {
		String s = String.valueOf(o);
		this.printLine(s, true);
	}

	void printLine(int i) {
		this.printLine(String.valueOf(i), true);
	}

	void printLine(double d) {
		this.printLine(String.valueOf(d), true);
	}

	void printLine(boolean b) {
		this.printLine(String.valueOf(b), true);
	}

	void printIt(Object o) {
		String s = String.valueOf(o);
		this.textAreaOutput.append(s);
	}

	void printString(Object o) {
		String s = String.valueOf(o);
		this.textAreaOutput.append(s + "\n");
	}

	void printALine() {
		this.textAreaOutput.append(VISUAL_SEPARATOR + "\n");
	}

	void printTextArea() {
		System.out.println(this.textAreaOutput.getText());
	}

	void setFocusedText(String value) {
		JComponent focused = focusListener.component;

		try {
			if (focused.getClass().getEnclosingClass() != null
					&& focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {

				JComboBox comboBox = (JComboBox) focused.getParent();
				addParameterValue(comboBox, value);
			}
		} catch (Throwable throwable) {
			printLine(throwable);
			throwable.printStackTrace();
		}
	}

	protected Object getFocusedObject() {
		return focusListener.component;
	}

//*///////////////////////////////////////////DMH	
	public String getFocusedText() {
		Object focused = getFocusedObject();

		if (focused == null) {
			focused = this.searchComboBox;
		}
		try {
			if (focused instanceof JTextArea || focused instanceof JTextPane || focused instanceof JPasswordField
					|| focused instanceof JEditorPane || focused instanceof SandboxTextArea) {
				return ((JTextComponent) focused).getSelectedText();
			} else if (focused.getClass().getEnclosingClass() != null 
					&& focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
				JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
				return getParameterValue(comboBox);
			}
		} catch (Throwable thr) {
			printLine("dmh929", thr);
		}
		return getParameterValue((JComponent) focused);
	}
/*///////////////////////////////////////////	
	public String getFocusedText() {
		Object focused = getFocusedObject();

		if (focused == null) {
			focused = getParameterComponentAt(1);
		}
		try {
			if (focused instanceof JTextArea || focused instanceof JTextPane || focused instanceof JPasswordField
					|| focused instanceof JEditorPane || focused instanceof SandboxTextArea) {
				return ((JTextComponent) focused).getSelectedText();
			} else if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
				JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
				return getParameterValue(comboBox);
			}
		} catch (Throwable thr) {
			printLine("dmh929", thr);
		}
		return getParameterValue((JComponent) focused);
	}
//*///////////////////////////////////////////	

	public String[] getFocusedText(int numberOfParameters) {
		Object focused = getFocusedObject();
		String[] returnArray = new String[numberOfParameters];

		if (focused == null) {// Nothing focused on
			focused = getParameterComponentAt(1);
		}
		try {
			if ((focused.getClass().getName().equals(JTextField.class.getName()))) {
				JTextField textField = (JTextField) focused;
				if (textField.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
					returnArray[0] = textField.getText();
					return returnArray;
				} else if (textField.getName().equals(SandboxParameters.REPLACE_COMBOBOX_NAME)) {
					returnArray[0] = textField.getText();
					return returnArray;
				} 
				int parameterNumber = Integer.parseInt(textField.getName().split(" ")[1]);
				returnArray[0] = getParameterValueAt(parameterNumber);

				if (numberOfParameters > 1) {
					for (int i = 1; i < numberOfParameters; i++) {
						returnArray[i] = getParameterValueAt(parameterNumber + i);
					}
				}
			} else if (focused instanceof JTextArea || focused instanceof JTextPane || focused instanceof JPasswordField
					|| focused instanceof JEditorPane || focused instanceof SandboxTextArea) {
				returnArray[0] = ((JTextComponent) focused).getSelectedText();
				return returnArray;
			} else if (focused instanceof JTable) {
				JTable table = (JTable) focused;
				try {
					returnArray[0] = (String) table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
				} catch (Exception en) {
					printLine(en.getMessage(), true, true);
				}
				return returnArray;
			} else if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
				JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
				if (comboBox.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
					returnArray[0] = (String) comboBox.getItemAt(comboBox.getSelectedIndex());
					return returnArray;
				} else {
					int parameterNumber = Integer.parseInt(comboBox.getName().split(" ")[1]);
					returnArray[0] = getParameterValueAt(parameterNumber);
					if (numberOfParameters > 1) {
						for (int i = 1; i < numberOfParameters; i++) {
							returnArray[i] = getParameterValueAt(parameterNumber + i);
						}
					}
				}
			}
		} catch (Throwable thr) {
			printLine(thr);
		}
		return returnArray;
	}

	String getAllScratchPadText() {
		return this.scratchPad.getText();
	}

	public String getScratchPadText() {
		String text = this.scratchPad.getSelectedText();
		if (text == null) {
			return this.scratchPad.getText();
		} else {
			return text;
		}
	}

	// void printToScratchPad(String text){
	// this.scratchPad.setText(text);
	// }
	//
	String[] getParameterValuesAt(int index) {
		return getParameterValues((JComponent) getParameterComponentAt(index));
	}

	private String[] getParameterValues(JComponent component) {
		if (component instanceof JTextField) {
			String[] stringArray = new String[1];
			stringArray[0] = ((JTextComponent) component).getText();
			return stringArray;
		} else if (component instanceof JComboBox) {
			JComboBox combobox = (JComboBox) component;
			String[] stringArray = new String[combobox.getItemCount() + 1];
			int selectedIndex = combobox.getSelectedIndex();
			stringArray[0] = String.valueOf(selectedIndex + 1);
			for (int i = 1; i < combobox.getItemCount() + 1; i++) {
				Object obj = Utilities.noNulls(combobox.getItemAt(i - 1));
				try {
					stringArray[i] = obj.toString();
				} catch (ArrayIndexOutOfBoundsException aioobe) {
					printLine(aioobe.getClass().getSimpleName() + ": Could not save \"" + obj.toString() + "\" in "
							+ combobox.getName() + " which has " + combobox.getItemCount() + " items.");
				}
			}
			return stringArray;
		} else {
			return null;
		}
	}

	void setParameterSelected(int componentIndex, int itemIndex) {
		JComboBox combobox = (JComboBox) getParameterComponentAt(componentIndex);
		if (combobox.getItemCount() >= itemIndex) {
			combobox.setSelectedIndex(itemIndex - 1);
		} else {
			combobox.setSelectedIndex(combobox.getItemCount() - 1);
		}
	}

	public String getParameterValueAt(int index) {
		JComponent component = (JComponent) getParameterComponentAt(index);
		if (component.getClass().getName().equals(JComboBox.class.getName())) {// It's
																				// a
																				// combobox
			JComboBox combobox = (JComboBox) component;
			int selectedIndex = combobox.getSelectedIndex();
			if (selectedIndex == -1) {
				return ""; // combobox is empty
			}
			Object selected = combobox.getSelectedItem();
			combobox.removeItem(selected);
			combobox.addItem(selected);
			combobox.setSelectedItem(selected);
		}
		return getParameterValue(component);
	}

	String getParameterValue(JComponent component) {

		if (component instanceof JPasswordField) {
			return new String(((JPasswordField) component).getPassword());
		} else if (component instanceof JTextField) {
			String value = ((JTextComponent) component).getText();
			return value.equals(SANDBOX_NULL) ? null : value;
		} else if (component instanceof JComboBox) {
			JComboBox combobox = (JComboBox) component;
			Object obj = Utilities.noNulls(combobox.getSelectedItem());
			return (String) obj;
		}
		return null;
	}

	void addParameterValueAt(int index, String value) {
		Object component = getParameterComponentAt(index);
		addParameterValue((JComponent) component, value);
	}

	private void addParameterValue(JComponent component, Object value) {

		if (component.getClass().getSimpleName().equals(JPasswordField.class.getSimpleName())) {
			((JPasswordField) component).setText((String) value);
		}
		if (component.getClass().getSimpleName().equals(JTextField.class.getSimpleName())) {
			((JTextComponent) component).setText((String) value);
		}
		if (component.getClass().getSimpleName().equals(JComboBox.class.getSimpleName())) {
			Object selectedValue = ((JComboBox) component).getSelectedItem();
			if (selectedValue == null) {// check initializing
				((JComboBox) component).addItem(value);
			} else if (!selectedValue.equals(value)) {// check if value in list
				((JComboBox) component).addItem(value);
				if (selectedValue != null) {
					if (selectedValue.equals("")) {
						int selectedIndex = ((JComboBox) component).getSelectedIndex();
						((JComboBox) component).removeItemAt(selectedIndex);
					}
				}
			}
			((JComboBox) component).setSelectedItem(value);
		}
		return;
	}

	void resetParameterValues() {
		for (int i = 1; i <= numParameters; i++) {
			JComponent component = (JComponent) getParameterComponentAt(i);
			if (component.getClass().getSimpleName().equals(JTextField.class.getSimpleName())) {
				((JTextComponent) component).setText("");
			} else if (component.getClass().getSimpleName().equals(JComboBox.class.getSimpleName())) {
				((JComboBox) component).removeAllItems();
			}
		}
	}

	public Object getParameterComponentAt(int index) {
		if (index > numParameters) {
			printLine("Parameter " + index + " is greater than maximum " + numParameters, true, true);
			return parameterList.get(numParameters);
		}
		return parameterList.get(index);
	}

	String getPassword() {
		return String.valueOf(passwordComponent.getPassword());
	}

	String getCommandsDirectory() {
		return commandsDirectory;
	}

	String getTrueFalse() {
		return String.valueOf(trueFalse.isSelected());
	}

	String getYesNo() {
		return String.valueOf(yesNo.isSelected());
	}

	public String replaceValues(String text) {
		if (text == null) {
			return text;
		} else {
			while (text.indexOf(SANDBOX_COMBOBOX_COMMENT_START) >= 0
					&& text.indexOf(SANDBOX_COMBOBOX_COMMENT_STOP) > 0) {
				int commentStart = text.indexOf(SANDBOX_COMBOBOX_COMMENT_START);
				int commentStop = text.indexOf(SANDBOX_COMBOBOX_COMMENT_STOP);
				text = (text.substring(0, commentStart)
						+ text.substring(commentStop + SANDBOX_COMBOBOX_COMMENT_STOP.length()));
			}

			text = text.trim();
			StringBuffer stringBuffer = new StringBuffer(text);

			// Replace with Base64-encoded
			String[] groupsBase64Code = Utilities.grabRegex("\\{sbx\\-b64\\-x\\s([\\s|\\S]+?)\\}", stringBuffer);
			while (groupsBase64Code != null) {
				int sbxTSStart = stringBuffer.indexOf(groupsBase64Code[0]);
				int sbxTSEnd = sbxTSStart + groupsBase64Code[0].length();
				String encoded = groupsBase64Code[1];
				String value = "UNKNOWN";
				value = Utilities.base64Encode(encoded);
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsBase64Code = Utilities.grabRegex("\\{sbx\\-b64\\-x\\s(.*?)\\}", stringBuffer);
			}
			// Replace with Base64-decoded
			String[] groupsBase64Decode = Utilities.grabRegex("\\{sbx\\-b64\\-d\\s([\\s|\\S]+?)\\}", stringBuffer);
			while (groupsBase64Decode != null) {
				int sbxTSStart = stringBuffer.indexOf(groupsBase64Decode[0]);
				int sbxTSEnd = sbxTSStart + groupsBase64Decode[0].length();
				String encoded = groupsBase64Decode[1];
				String value = "UNKNOWN";
				value = Utilities.base64Decode(encoded);
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsBase64Decode = Utilities.grabRegex("\\{sbx\\-b64\\-d\\s(.*?)\\}", stringBuffer);
			}

			String regexBk = "(\\{sbx\\-bk\\}\\s*?\\n\\t+)"; //To remove the break
			String[] replaceBk = {"1"};
			replaceBk[0] = Utilities.GRAB_REPLACE + SandboxParameters.SANDBOX_EMPTY;
			StringBuffer newTextBk = new StringBuffer();
			Utilities.grabReplaceRegex(regexBk, replaceBk, stringBuffer, newTextBk);
			stringBuffer = newTextBk;
			
			regexBk = "(\\{sbx\\-bk\\})";	//For places you want to replace with a break
			replaceBk[0] = Utilities.GRAB_REPLACE + "\n";
			newTextBk = new StringBuffer();
			Utilities.grabReplaceRegex(regexBk, replaceBk, stringBuffer, newTextBk);
			stringBuffer = newTextBk;
			
			text = stringBuffer.toString();

			List<SandboxBasket<String, String>> list = new ArrayList<SandboxBasket<String, String>>();
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_BLANK," "));
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_EMPTY,""));
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_CLASSPATH,
					System.getProperty("java.class.path")));
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_PASSWORD, getPassword()));
			
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_CMD_DIR, getCommandsDirectory()));
			
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_WORKSPACE,
					System.getProperty("user.dir")));
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_DIRECTORY, configurationDirectory));

			list.add(
					new SandboxBasket<String, String>(SandboxParameters.SANDBOX_HOME, this.configurationDirectoryRoot));
			// list.add(new SandboxBasket<String,String>
			// (SandboxParameters.SANDBOX_FOCUSED,//This is causing
			// major,hard-to-debug problems
			// this.getFocusedText()));
			list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_COMBOBOX,
					(String) Utilities.noNulls(this.replaceComboBox.getSelectedItem())));

//*/////////////////////////DMH
			//SANDBOX_CLIPBOARD
			try {
				Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
				java.awt.datatransfer.Transferable tText = clip.getContents(null);
				if (clip.isDataFlavorAvailable(DataFlavor.stringFlavor)) {
					String clipboardText = (String) tText.getTransferData(DataFlavor.stringFlavor);
					StringBuffer fileStringBuffer = new StringBuffer(clipboardText);
					list.add(new SandboxBasket<String, String>(SandboxParameters.SANDBOX_CLIPBOARD,
							fileStringBuffer.toString()));
//					if (fileStringBuffer.length() > 20) {
//						alert("Clipboard loaded: " + fileStringBuffer.toString().substring(0, 20) + " ...");
//					} else {
//						alert("Clipboard loaded: " + fileStringBuffer.toString());
//					}
				}
			} catch(Throwable th) {
				alert("Could not replace clipboard!", this.iconStopped);
				printLine("Could not replace clipboard!", th);
			}

//*/////////////////////////

			for (SandboxBasket<String, String> nameValue : list) {
				text = text.replace(nameValue.getName(), Utilities.noNulls(nameValue.getValue()));
			}

			// Replace with environment properties
			stringBuffer = new StringBuffer(text);
			String[] groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer);
			while (groupsEnv != null) {

				int sbxTSStart = stringBuffer.indexOf(groupsEnv[0]);
				int sbxTSEnd = sbxTSStart + groupsEnv[0].length();
				String environmentName = groupsEnv[1];
				String value = System.getProperty(environmentName, environmentName + "-UNKNOWN");
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsEnv = Utilities.grabRegex("\\{sbx\\-prop\\s(.*?)\\}", stringBuffer);
			}
			
//*/////////////////////DMH			
			// Replace with global map strings
//			stringBuffer = new StringBuffer(text);
			String[] groupsGmap = Utilities.grabRegex("\\{sbx\\-gmap\\s(.*?)\\}", stringBuffer);
			while (groupsGmap != null) {

				int sbxTSStart = stringBuffer.indexOf(groupsGmap[0]);
				int sbxTSEnd = sbxTSStart + groupsGmap[0].length();
				String mapid = groupsGmap[1];
				
				
				//String value = System.getProperty(mapid, mapid + "-UNKNOWN");
				SandboxConnection connection = this.getGlobals().getMap().get(mapid);
				String value = mapid + "-UNKNOWN";
				if (connection != null && connection.getObj() instanceof String) {
					value = (String) connection.getObj();
				}
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsGmap = Utilities.grabRegex("\\{sbx\\-gmap\\s(.*?)\\}", stringBuffer);
			}
//*////////////////////////
			
			

			// Replace literals
			String regex = "\\{sbx\\-lit.\\[(.*?)\\]\\}";
			String[] replace = { "1" };
			StringBuffer newText = new StringBuffer();
			List<SandboxBasket<String[], String[]>> basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer,
					newText);
			stringBuffer = newText;

			// Replace file separators
			regex = "(\\{sbx\\-fs\\})";
			replace[0] = Utilities.GRAB_REPLACE + System.getProperty("file.separator", "/");
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;

			
//*/////////////////////////////////DMH
			regex = "(\\{sbx\\-dmh\\})";
			replace[0] = Utilities.GRAB_REPLACE + "David Marhall Harter";
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;
			
//			regex = "(\\{sbx\\-bk\\}\\n)";
//			replace[0] = Utilities.GRAB_REPLACE + " ";
//			newText = new StringBuffer();
//			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
//			stringBuffer = newText;
			
//*////////////////////////////////////
			
			
			// Replace time stamps
			regex = "(\\{sbx\\-tsl\\})";
			replace[0] = Utilities.GRAB_REPLACE + String.valueOf(new Date().getTime());
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;

			regex = "(\\{sbx\\-tsh\\})";
			replace[0] = Utilities.GRAB_REPLACE + Utilities.getFileTimestamp();
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;

			// Replace with formatted timestamp
			String[] groupsTimeFormat = Utilities.grabRegex("\\{sbx\\-tsh\\s(.*?)\\}", stringBuffer);
			while (groupsTimeFormat != null) {
				int sbxTSStart = stringBuffer.indexOf(groupsTimeFormat[0]);

				int sbxTSEnd = sbxTSStart + groupsTimeFormat[0].length();
				String format = groupsTimeFormat[1];
				stringBuffer.replace(sbxTSStart, sbxTSEnd, Utilities.getTime(format));
				groupsTimeFormat = Utilities.grabRegex("\\{sbx\\-tsh\\s(.*?)\\}", stringBuffer);
			}

			regex = "(\\{sbx\\-tsd\\})";
			replace[0] = Utilities.GRAB_REPLACE + Utilities.getDay();
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;

			// Replace with URL-encoded ASCII
			String[] groupsUrlAscii = Utilities.grabRegex("\\{sbx\\-url\\-ascii\\s([\\s|\\S]+?)\\}", stringBuffer);
 			while (groupsUrlAscii != null) {

				int sbxTSStart = stringBuffer.indexOf(groupsUrlAscii[0]);
				int sbxTSEnd = sbxTSStart + groupsUrlAscii[0].length();
				String url = groupsUrlAscii[1];
				String value = "UNKNOWN";
				try {
					value = URLEncoder.encode(url, "US-ASCII");
				} catch (UnsupportedEncodingException uee) {
					value = uee.getClass().getName();
				}
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsUrlAscii = Utilities.grabRegex("\\{sbx\\-url\\-ascii\\s(.*?)\\}", stringBuffer);
			}

			// Replace with URL-encoded ASCII
			String[] groupsUrlAsciiDecode = Utilities.grabRegex("\\{sbx\\-url\\-ascii\\-decode\\s([\\s|\\S]+?)\\}", stringBuffer);
 			while (groupsUrlAsciiDecode != null) {

				int sbxTSStart = stringBuffer.indexOf(groupsUrlAsciiDecode[0]);
				int sbxTSEnd = sbxTSStart + groupsUrlAsciiDecode[0].length();
				String url = groupsUrlAsciiDecode[1];
				String value = "UNKNOWN";
				try {
					value = URLDecoder.decode(url, "US-ASCII");
				} catch (UnsupportedEncodingException uee) {
					value = uee.getClass().getName();
				}
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsUrlAsciiDecode = Utilities.grabRegex("\\{sbx\\-url\\-ascii\\-decode\\s(.*?)\\}", stringBuffer);
			}

			// Replace with parameter values
			String[] groupsParameters = Utilities.grabRegex("(\\{sbx\\-p)(\\d\\d)(\\})", stringBuffer);
			while (groupsParameters != null) {

				int sbxTSStart = stringBuffer.indexOf(groupsParameters[0]);
				int sbxTSEnd = sbxTSStart + groupsParameters[0].length();
				int parameterNumber = Integer.parseInt(groupsParameters[2]);
				String value = this.getParameterValueAt(parameterNumber);
				stringBuffer.replace(sbxTSStart, sbxTSEnd, value);
				groupsParameters = Utilities.grabRegex("(\\{sbx\\-p)(\\d\\d)(\\})", stringBuffer);
			}

//*////////////////////////DMH
			regex = "(\\{sbx\\-dmh\\})";
			replace[0] = "David Marshall Harter";
			newText = new StringBuffer();
			basket = Utilities.grabReplaceRegex(regex, replace, stringBuffer, newText);
			stringBuffer = newText;
//*///////////////////////////
			

			return stringBuffer.toString();
		}
	}
	
//	private void setBorderColor(JFrame frame) {
//
//		String[] ebc = System.getProperty(SandboxParameters.SBX_CONF_BORDER_COLOR, "255_255_255").split("_");
//		Color borderColor = new Color(255, 255, 255);
//
//		try {
//			borderColor = new Color(Integer.parseInt(ebc[0]), Integer.parseInt(ebc[1]),	Integer.parseInt(ebc[2]));
//		} catch (NumberFormatException nfe) {
//			System.err.println("You should set " + SandboxParameters.SBX_CONF_BORDER_COLOR + " correctly in the environment");
//		}
//		Border border = BorderFactory.createLineBorder(borderColor, 5);
//		frame.getRootPane().setBorder(border);
//		frame.setIconImage(SandboxParameters.sandboxImage);
//	}


	public void showButtonRunning(String testName) {
		JButton button = getButton(BUTTON_PREFIX + testName);
		button.setFont(button.getFont().deriveFont(Font.BOLD));
	}

	public void showButtonStopped(String testName) {
		JButton button = getButton(BUTTON_PREFIX + testName);
		button.setFont(button.getFont().deriveFont(Font.PLAIN));
	}

	public void showButtonReady(String testName) {
		JButton button = getButton(BUTTON_PREFIX + testName);
		button.setFont(button.getFont().deriveFont(Font.ITALIC | Font.BOLD));
	}

	public void showButtonNotReady(String testName) {
		JButton button = getButton(BUTTON_PREFIX + testName);
		button.setFont(button.getFont().deriveFont(Font.PLAIN));
	}

	public int setAllButtonsNotReady() {
		Iterator<SandboxTestBase> iterTests = this.tests.iterator();
		int readyButtonCount = 0;
		while (iterTests.hasNext()) {
			SandboxTestBase test = iterTests.next();
			if (test.isReady) {
				readyButtonCount++;
			}
			test.setReady(false);
		}
		return readyButtonCount;
	}

	private JButton getButton(String buttonName) {
		Enumeration<JButton> enumButtons = buttons.elements();
		while (enumButtons.hasMoreElements()) {
			JButton button = enumButtons.nextElement();
			if (button.getName().equals(buttonName)) {
				return button;
			}
		}
		return null;
	}

	public SandboxTestBase getTest(String testTitle) {
		Iterator<SandboxTestBase> iterTests = tests.iterator();
		synchronized (iterTests) {
			while (iterTests.hasNext()) {
				SandboxTestBase test = iterTests.next();
				if (test.getTestName().equals(testTitle)) {
					return test;
				}
			}
		}
		return null;
	}

	public void setTest(SandboxTestBase newTest) {
		Iterator<SandboxTestBase> iterTests = tests.iterator();
		synchronized (iterTests) {
			while (iterTests.hasNext()) {
				SandboxTestBase test = iterTests.next();
				if (test.getTestName().equals(newTest.getTestName())) {

					int testIndex = tests.indexOf(test);
					tests.set(testIndex, newTest);
				}
			}
		}
	}

	public void runLineTest(String testString) {
		String[] runString = replaceValues(testString).split("\n");
		int nameValueIndex = 0;
		String testName = runString[nameValueIndex++];

		SandboxTestBase test = getTest(testName);

		if (test == null) {
			printLine("Could not get test " + testName);
			return;
		}

		String[] parameterDescriptions = test.getParameterDescriptions();
		int numParamDescriptions = parameterDescriptions.length;

		Hashtable<String, JButton> buttonTable = getButtons();
		synchronized (buttonTable) {
			javax.swing.JButton button = getButtons()
					.get(SandboxParameters.BUTTON_PREFIX + testName);
			test.setReady(true);
			test.presetParameters = true;
			button.doClick();
		}
	}

	
	public void runTest(String testString) {
		runTest(testString, null);
	}	
	public void runTest(String testString, String tabTitle) {
		String[] testStringArray = testString.split("\n");
		String testName = testStringArray[0];
		if (testName.indexOf('\t') > 0) { //Has description?
			testName = testName.substring(0, testName.indexOf('\t'));
		}
		SandboxTestBase test = getTest(testName);

		if (test == null) {
			printLine("Could not get test " + testName);
			this.alert("Could not get test " + testName, this.iconStopped);
			return;
		}

		for (int index = 1; index < testStringArray.length; index++) {
			String nameValue = testStringArray[index];
			String name = nameValue.split("\t+")[0].trim();
			String value = "";
			try {
//				value = nameValue.split("\t+")[1];
				value = nameValue.split("[\\t|\\s]+",2)[1]; //Allowing for accidental tabs and spaces
				
				if (value.startsWith(SandboxParameters.US_ASCII_ENCODED)) {
					value = value.substring(SandboxParameters.US_ASCII_ENCODED.length() + 1); //plus one for the space delimiter
					value = URLDecoder.decode(value, "US-ASCII");
				}
			} catch(IndexOutOfBoundsException | UnsupportedEncodingException ioobe) {
				printLine(name + " does not have a value", true, true);
			}
			test.parameterValues.put(name, value);
		}
		Hashtable<String, JButton> buttonTable = getButtons();
		synchronized (buttonTable) {
			javax.swing.JButton button = getButtons()
					.get(SandboxParameters.BUTTON_PREFIX + testName);
			
			if (tabTitle != null) {
				test.setTabDisplayName(tabTitle);
			}
			test.setReady(true);
			test.presetParameters = true;
			button.doClick();
		}
	}

	public void unhighlightParameters() {
		for (int i = 0; i < numParameters; i++) {
			Object obj = getParameterComponentAt(i + 1);
			Component component = null;
			if (obj instanceof JComboBox) {
				JComboBox comboBox = (JComboBox) obj;
				component = comboBox.getEditor().getEditorComponent();
			} else {
				component = (Component) obj;
			}
			component.setBackground(this.parameterUnHighlightColor);
			component.setFont(component.getFont().deriveFont(Font.PLAIN));
		}
	}

	public void highlightParameters(String[] parameterDescriptions) {

		try {
			for (int j = 0; j < parameterDescriptions.length / 2; j++) {
				int index = j * 2;
				Component component = null;
				try {
					int parameterIndex = Integer.parseInt(parameterDescriptions[index]);
					Object obj = this.getParameterComponentAt(parameterIndex);
					if (obj instanceof JComboBox) {
						JComboBox comboBox = (JComboBox) obj;
						component = comboBox.getEditor().getEditorComponent();
					} else {
						component = (Component) obj;
					}
				} catch (NumberFormatException nfe) {// focused variable
					if (parameterDescriptions[index].equals(FOCUSED_NAME)) {

						component = getFocusListener().component;
						try {
							JComboBox comboBox = (JComboBox) ((JComponent) component).getParent();
							if (comboBox.getName().equals(SandboxParameters.SEARCH_COMBOBOX_NAME)) {
								return; // Handle search combobox differently
							}
							int sequenceCount = Integer.parseInt(comboBox.getName().split(" ")[1]);
							String[] valueNames = parameterDescriptions[index + 1].split(",");

							for (String valueName : valueNames) {// highlight
																	// multiple
																	// values
								comboBox = (JComboBox) getParameterComponentAt(sequenceCount++);
								component = comboBox.getEditor().getEditorComponent();
								component.setBackground(this.parameterHighlightColor);
								component.setFont(component.getFont().deriveFont(Font.BOLD));
							}

						} catch (ClassCastException cce) {
							printLine(true, "Not a " + JComboBox.class.getSimpleName());
							component = null;
						}
					} else if (parameterDescriptions[index].equals(SCRATCHPAD_NAME)) {
						component = null;
					} else if (parameterDescriptions[index].equals(PASSWORD_NAME)) {
						component = passwordComponent;
					}
				}
				if (component != null) {
					component.setBackground(this.parameterHighlightColor);
					component.setFont(component.getFont().deriveFont(Font.BOLD));
				}
			}
		} catch (Throwable thr) {
			this.printLine("Could not highlight parameters", true, true);
		}
	}

	public void unhiglightButtons() {
		Enumeration<JButton> enumButtons = buttons.elements();
		while (enumButtons.hasMoreElements()) {
			JButton button = enumButtons.nextElement();
			button.setForeground(this.buttonUnHighlightColor);
		}
	}

	void loadConfiguration(Properties properties) {
		String version = (String) properties.get("version");
		if (version == null) {
			printLine("Loading version 1.0 configuration"); // Version 1.0,so
															// read in
															// parameters from
															// that format
			for (int i = 1; i <= NUM_PARAMETERS_V1_0; i++) {
				String propString = (String) properties.get("tfParameter" + (i));
				if (propString != null) {
					addParameterValueAt(i, propString);
				}
			}
		} else {// not version 1.0
			try {
				String scratchPadName = (String) properties.get(SCRATCHPAD_PROPERTY_NAME);
				if (scratchPadName != null) {
					String scratchpadFilePath = this.configurationDirectory + File.separator + scratchPadName;
					scratchPad.setText(Utilities.getFileString(scratchpadFilePath));
					if (!(new File(scratchpadFilePath)).canWrite()) {
						scratchPad.setEditable(false);
						JOptionPane.showMessageDialog(frame, scratchpadFilePath + "IS NOT EDITABLE");
					}
					Utilities.addUndo(scratchPad);
				}
			} catch (Exception en) {
				printLine("Scratchpad not loaded");
				sandboxLogger.info("Scratchpad not loaded");
			}
			for (int i = 1; i <= numParameters; i++) {
				try {
					for (int j = 1; j < MAX_PARAMETER_DEPTH; j++) {
						String propString = (String) properties.get("tfParameter" + (i) + "." + j);
						if (propString != null) {
							addParameterValueAt(i, propString);
						} else {
							break;
						}
					}
					int selectionIndex = Integer.parseInt((String) properties.get("tfParameter" + (i) + ".index"));
					setParameterSelected(i, selectionIndex);

				} catch (NullPointerException en) {
					sandboxLogger.fine("More program parameters than config parameters.");
				} catch (NumberFormatException nfe) {
					sandboxLogger.fine("Missing selection offset value.");
				}
			}
		}
	}

	void storeEnvironment() {
		Properties systemProps = System.getProperties();
		Properties sandboxProps = new Properties();
		Enumeration<?> systemPropNames = systemProps.propertyNames();
		while (systemPropNames.hasMoreElements()) {
			String propName = (String) systemPropNames.nextElement();
			if (propName.startsWith("sandbox.")) {
				sandboxProps.put(propName, systemProps.get(propName));
			}
		}

		FileOutputStream fos = null;
		String filepath = configurationDirectory + File.separator + "environment.xml";
		try {
			File environment = new File(filepath);
			fos = new FileOutputStream(environment);
			sandboxProps.storeToXML(fos,
					"sandbox saved environment: " + Utilities.parseDate(new Date(), SandboxParameters.DATE_FORMAT),
					"UTF-8");
			fos.close();

		} catch (IOException ioe) {

			String msg = "Unable to save system environment in " + filepath;
			printLine(msg, true, true);
			sandboxLogger.fine(msg);
		}
	}

	void storeConfiguration() {
		storeConfiguration(this.configurationFileRoot + ".xml");
	}

	void storeConfiguration(String newFilename) {
		String scratchPadName = (String) properties.get(SCRATCHPAD_PROPERTY_NAME);
		if (scratchPadName == null) {
			scratchPadName = SCRATCHPAD_NAME + ".txt";
		}
		storeConfiguration(newFilename, scratchPadName);
	}

	void storeConfiguration(String newFilename, String scratchPadName) {
		String javaMemorySettings = properties.getProperty("javaMemorySettings");
		String sandboxSoundFile = properties.getProperty("sandboxSoundFile");
		
		String chicletX = properties.getProperty("chicletX");
		String chicletY = properties.getProperty("chicletY");
		
		properties = new Properties();
		properties.put("javaMemorySettings", Utilities.noNulls(javaMemorySettings));
		properties.put("sandboxSoundFile", Utilities.noNulls(sandboxSoundFile));
		
		properties.put("chicletX", Utilities.noNulls(chicletX));
		properties.put("chicletY", Utilities.noNulls(chicletY));
		
		properties.put("version", VERSION);
		if (scratchPadName != null) {
			properties.put(SCRATCHPAD_PROPERTY_NAME, scratchPadName);
			try {
				Utilities.saveFileString(this.configurationDirectory + File.separator + scratchPadName,
						getAllScratchPadText());
			} catch (Exception en) {
				printLine("Cannot save scratch pad", en);
				JOptionPane.showMessageDialog(SandboxParameters.frame, "Cannot save scratch pad");
//				new SandboxMessage("COULD NOT SAVE SCRATCHPAD", "Could not save scratchpad ", tabbedPane, 3, false);
			}
		}
		properties.put("numCols", String.valueOf(numCols));
		properties.put("numRows", String.valueOf(numRows));
		properties.put("numButtonsPerRow", String.valueOf(numButtonsPerRow));
//		properties.put("listFixedHeight", String.valueOf(listFixedHeight));
		properties.put("maxByteArraySize", String.valueOf(maxByteArraySize));
		properties.put("maxFileOpenSize", String.valueOf(maxFileOpenSize));

		properties.put("frameHeight", String.valueOf(frameHeight));
		properties.put("frameWidth", String.valueOf(frameWidth));
		
//		properties.put(SANDBOX_DATE_FORMATS_ENV, this.sandboxDateFormats);

		for (int i = 1; i <= numParameters; i++) {
			String[] stringArray = getParameterValuesAt(i);
			properties.put("tfParameter" + i + ".index", stringArray[0]);

			for (int j = 1; j < stringArray.length; j++) {
				if (stringArray[j] != null) {
					properties.put("tfParameter" + i + "." + j, stringArray[j]);
				}
			}
		}

		try {
			FileOutputStream fileOutputStream = null;
			try {// Storing to XML configuration
				File file = null;

				file = new File(newFilename);
				fileOutputStream = new FileOutputStream(file);

				properties.storeToXML(fileOutputStream, this.getClass().getName(), "UTF-8");
				fileOutputStream.close();
				file = null;

			} catch (java.lang.AbstractMethodError ame) {
				printLine("Could not store configuration to XML file.", ame);
				fileOutputStream.close();
			}
		} catch (IOException ioe) {
			printLine("Could not store configuration to XML file.", ioe);
			JOptionPane.showMessageDialog(frame, "COULD NOT SAVE XML CONFIGURATION");
//			this.addToGlobalMap(new SandboxMessage("COULD NOT SAVE XML CONFIGURATION", "Could not save " + newFilename, tabbedPane, 3, false));
		}

		try {// As a backup,storing to properties configuration
			File file = new File(this.configurationDirectory + File.separator + "sandbox.properties");
			FileOutputStream fileOutputStream = new FileOutputStream(file); //this created the actual file
			properties.store(fileOutputStream, "Just in case the XML doesn't work");
			fileOutputStream.close();
			file = null;
		} catch (IOException ioe) {
			printLine("Could not save properties configuration", ioe);
			JOptionPane.showMessageDialog(frame, "COULD NOT SAVE PROPERTIES FILE"); //TODO Re22.03.01.11.44.44.410
		} catch (SecurityException se) {
			JOptionPane.showMessageDialog(frame, "COULD NOT SAVE PROPERTIES FILE - " + this.getConfigurationFileRoot() + " - Security Exception");
		}

	}

	public void addTestComponent(JComponent component) {
		try {
			this.removeTestComponent();
			this.testComponent = component;
			this.componentPanel.add(component, 1);
			componentPanel.getParent().getParent().validate();

		} catch (Exception en) {
			printLine(en.getClass().getSimpleName() + ": Could not add test component.");
		}
	}

	public void removeTestComponent() {
		this.componentPanel.remove(1);
		this.testComponent = null;
		componentPanel.validate();
	}

	public void validateTestComponent() {
		componentPanel.getParent().getParent().validate();
	}

	void selectTestInList(String testName) {
		JScrollPane pane = null;
		if (testName.startsWith("Tool")) {
			tabbedPane.setSelectedIndex(TOOLS_TAB_INDEX);
			pane = (JScrollPane) tabbedPane.getComponentAt(TOOLS_TAB_INDEX);
		}
		if (testName.startsWith("Test")) {
			tabbedPane.setSelectedIndex(TESTS_TAB_INDEX);
			pane = (JScrollPane) tabbedPane.getComponentAt(TESTS_TAB_INDEX);
		}
		if (pane != null) {
			JViewport port = (JViewport) pane.getComponent(0);
			if (port.getComponent(0) instanceof SandboxList) {
				SandboxList list = (SandboxList) port.getComponent(0);
				for (int i = 0; i < list.getModel().getSize(); i++) {
					String[] strings = ((String) list.getModel().getElementAt(i)).split(":");
					if (testName.equals(strings[0])) {
						list.setSelectedIndex(i);
						list.ensureIndexIsVisible(i);
					}
				}
			}
		}
	}

	void deleteFocusedParameters() throws Exception {
		Object focused = this.getFocusedObject();
		if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
			JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
			comboBox.removeAllItems();
			printLine("Deleting all items", true, true);
		} else {
			printLine("Could not delete parameters because not in focus.");
		}
	}

	void deleteFocusedParameter() throws Exception {
		Object focused = this.getFocusedObject();
		if (focused.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
			JComboBox comboBox = (JComboBox) ((JComponent) focused).getParent();
			String selectedValue = (String) comboBox.getItemAt(comboBox.getSelectedIndex());
			printLine("Deleting: \"" + selectedValue + "\"", true, true);
			comboBox.removeItemAt(comboBox.getSelectedIndex());
		} else {
			printLine("Could not delete parameter because not in focus.");
		}
	}

	public void newTab(String description, String pageString) {
		newTab(description, pageString, "plain");
	}

	public void newTab(String description, String pageString, String title) {
		newTab(description, pageString, title, tabbedPane.getTabCount());
	}

	public void newTab(String description, String pageString, String title, int index) {
		newTab(description, pageString, title, tabbedPane.getTabCount(), true);
	}
	public void newTab(String description, String pageString, String title, int index, boolean enabled) {
		try {
			JEditorPane editorPanePlain = new JEditorPane();
			
			
			
			String[] clf = Utilities.getEnvFont();

			Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
			editorPanePlain.setFont(textFont);

			
			
			editorPanePlain.setText(pageString);
			editorPanePlain.setEditable(enabled);
			editorPanePlain.addKeyListener(this.keyListener);
			editorPanePlain.addFocusListener(this.focusListener);
//			if (!enabled) {
//				editorPanePlain.setForeground(Color.GRAY);
//			}
			JScrollPane scrollPanePlain = new JScrollPane(editorPanePlain);
			scrollPanePlain.setName(title);
			scrollPanePlain.setToolTipText(description);
			tabbedPane.add(scrollPanePlain, index);
			tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, description);
			
			tabbedPane.setEnabledAt(index, enabled);
			
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			printLine("Not opening web page");
			printLine(pageString);
		}
	}
	
	/**
	 * un-select all tabs except the one specified
	 * if specified tab is <0 all tabs are unselected
	 * @param selectedTab
	 */
//	public void unselectOtherTabs(int other) {
//		char bookend = 5784;
//		for (int tabIndex = 0; tabIndex < tabbedPane.getTabCount(); tabIndex++) {
//			if (tabIndex != other) {
//				String title = tabbedPane.getTitleAt(tabIndex);
//				
//				if (title.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
//					title = title.substring(2, title.length() - 2);
//					tabbedPane.setTitleAt(lastSelectedTab, title);
//				}
//			}
//		}
//	}
	
	/**
	 * //								if (params.lastSelectedTab >= 0 && params.lastSelectedTab < pane.getTabCount()) {
//									pane.setBackgroundAt(params.lastSelectedTab, params.tabBackgroundColor);
//									pane.setForegroundAt(params.lastSelectedTab, params.tabForegroundColor);
//									pane.setForegroundAt(selectedIndex, params.tabSelectedColor);
//								}
//								pane.setBackgroundAt(selectedIndex, params.tabSelectedColor);
								
//								pane.setComponentAt(2, new JLabel("poon"));
//								pane.setForegroundAt(params.lastSelectedTab, params.tabForegroundColor);
//								pane.setForegroundAt(selectedIndex, params.tabSelectedColor);

	 * @param selectedTab
	 */
	
	
//*///////////////////////////////////////////////DMH	
	public void selectTab(int selectedTab) {
		
		//char bookend = 5784;
		
		char bookend = 0;
		try {
			int bookendInt = Integer.parseInt((String)properties.get("bookendChar"));
			bookend = (char) bookendInt;
		} catch(NumberFormatException nfe) {
			System.out.println("Not using tab bookends");
		}

		boolean useColor = (tabBackgroundColor != null && tabForegroundColor != null && tabSelectedColor != null);
		
		if (lastSelectedTab < tabbedPane.getTabCount()) {
			if (bookend != 0) {
				try {
					String lastTitle = tabbedPane.getTitleAt(lastSelectedTab);

					if (lastTitle.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
						lastTitle = lastTitle.substring(2, lastTitle.length() - 2);
						tabbedPane.setTitleAt(lastSelectedTab, lastTitle);
					}
				} catch(IndexOutOfBoundsException ioobe){
					printLine("Could not bookend tabs", true, true);
				}
			}
			if (useColor) {
				try {
					tabbedPane.setBackgroundAt(lastSelectedTab, tabBackgroundColor);
					tabbedPane.setForegroundAt(lastSelectedTab, tabForegroundColor);
				} catch(IndexOutOfBoundsException ioobe) {
					printLine("Could not use colored tabs", true, true);
				}
			}
		}

//*/////////////////////////////DMH
		if (bookend != 0) {
			String currentTitle = tabbedPane.getTitleAt(selectedTab);
			if (currentTitle.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
				currentTitle = currentTitle.substring(2, currentTitle.length() - 2);
			} else {
				currentTitle = bookend + " " + tabbedPane.getTitleAt(selectedTab) + " " + bookend;
			}
			tabbedPane.setTitleAt(selectedTab, currentTitle);
		}
/*////////////////////////////////		
		if (bookend != 0) {
			String currentTitle = tabbedPane.getTitleAt(selectedTab);
			if (currentTitle.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
				currentTitle = currentTitle.substring(2, currentTitle.length() - 2);
			}
			currentTitle = bookend + " " + tabbedPane.getTitleAt(selectedTab) + " " + bookend;
			tabbedPane.setTitleAt(selectedTab, currentTitle);
		}
//*////////////////////////////////		
		
		
		
		if (useColor) {
			tabbedPane.setBackgroundAt(selectedTab, tabBackgroundColor);
			tabbedPane.setForegroundAt(selectedTab, tabSelectedColor);
		}
		lastSelectedTab = selectedTab;
	}
/*//////////////////////////////////////////////////	
	public void selectTab(int selectedTab) {
		
		//char bookend = 5784;
		
		char bookend = ' ';
		try {
			int bookendInt = Integer.parseInt((String)properties.get("bookendChar"));
			bookend = (char) bookendInt;
			if (lastSelectedTab < tabbedPane.getTabCount()) {
				String lastTitle = tabbedPane.getTitleAt(lastSelectedTab);
				
				if (lastTitle.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
					lastTitle = lastTitle.substring(2, lastTitle.length() - 2);
					tabbedPane.setTitleAt(lastSelectedTab, lastTitle);
				}
			}
			
			
			
			String currentTitle = tabbedPane.getTitleAt(selectedTab);
			if (currentTitle.matches("^" + bookend + "\\s.*\\s" + bookend + "$")) {
				currentTitle = currentTitle.substring(2, currentTitle.length() - 2);
			}
			currentTitle = bookend + " " + tabbedPane.getTitleAt(selectedTab) + " " + bookend;
			tabbedPane.setTitleAt(selectedTab, currentTitle);
			lastSelectedTab = selectedTab;
		} catch(Throwable th) {
			printLine("bookendChar undefined", false, false);
		}
		
//		char bookend = (char) Integer.parseInt((String)properties.get("bookendChar"));
		
		
	}
//*//////////////////////////////////////////////////	

	public void newTab(String description, JScrollPane scrollPanePlain, String title) {
		newTab(description, scrollPanePlain, title, null);
	}

	public void newTab(String description, JScrollPane scrollPanePlain, String title, Icon icon) {
		try {
			scrollPanePlain.setName(title);
			tabbedPane.add(scrollPanePlain);
			tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, description);
			tabbedPane.setIconAt(tabbedPane.getTabCount() - 1, icon);
			tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
			selectTab(tabbedPane.getTabCount() - 1);
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			printLine("Not opening web page");
			printLine(description);
		}
	}
	
	public void newTab(String description, Component component, String title, Icon icon) {
		try {
			tabbedPane.add(component);
			tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, description);
			tabbedPane.setIconAt(tabbedPane.getTabCount() - 1, icon);
			selectTab(tabbedPane.getTabCount() - 1);
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			printLine("Not opening web page");
			printLine(description);
		}
	}

	public void newTab(SandboxTextArea textArea, String title) {
		try {
			JScrollPane scrollPanePlain = new JScrollPane(textArea);
			scrollPanePlain.setName(title);
			tabbedPane.add(scrollPanePlain);
			tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, "Whatever 1447");
			selectTab(tabbedPane.getTabCount() - 1);
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			printLine("Not opening web page");
		}
	}

	public void removeTab(int tabNumber) {
		tabbedPane.remove(tabNumber);
	}

	public JComponent getSelectedTabComponent() {
		// JScrollPane pane = (JScrollPane)tabbedPane.getSelectedComponent();
		// JViewport port = (JViewport)pane.getComponent(0 );
		// return (JComponent)port .getComponent(0);
		return getEmbeddedComponent(tabbedPane.getSelectedComponent());
	}

	public JComponent getEmbeddedComponent(Component component) {
		JViewport port = (JViewport) ((JScrollPane) component).getComponent(0);
		return (JComponent) port.getComponent(0);
	}

	public String getTabComponentText(int tabNum) {
		JComponent component = getTabComponent(tabNum);
		return getTabText(component);
	}

	public JComponent getTabComponent(int tabNum) {
		try {
			JScrollPane pane = (JScrollPane) tabbedPane.getComponentAt(tabNum);
			JViewport port = (JViewport) pane.getComponent(0);
			return (JComponent) port.getComponent(0);
		} catch(IndexOutOfBoundsException ioobe) {
			return null;
		}
	}

	protected String getTabText(JComponent component) {
		if (component instanceof JTextArea || component instanceof SandboxTextArea) {
			return ((JTextArea) component).getText();
		} else if (component instanceof JTable) {
			JTable table = (JTable) component;
			StringBuffer buffer = new StringBuffer();

			Enumeration<TableColumn> enumeration = table.getTableHeader().getColumnModel().getColumns();
			enumeration.nextElement(); // skip sandbox-added first column
			while (enumeration.hasMoreElements()) {
				buffer.append(enumeration.nextElement().getHeaderValue()).append(",");
			}
			buffer.deleteCharAt(buffer.length() - 1); // get rid of final comma
			buffer.append("\n");

			TableModel model = table.getModel();
			for (int row = 0; row < model.getRowCount(); row++) {
				for (int column = 0; column < model.getColumnCount() - 1; column++) {
					buffer.append(((String) model.getValueAt(row, column)).replaceAll(",",
							SandboxParameters.SANDBOX_COMMA_DELIMITER)).append(",");
				}
				buffer.deleteCharAt(buffer.length() - 1);
				buffer.append("\n");
			}
			return buffer.toString();
		} else if (component instanceof SandboxTextPane) {
			SandboxTextPane textPane = (SandboxTextPane) component;
			if (textPane.isText()) {
				printLine("Removing \\r characters from the text extracted from the component", true, true);
				return ((JTextPane) component).getText().replaceAll("\\r", "");
			} else {
				return "";
			}
		} else if (component instanceof JTextPane) {
			return ((JTextPane) component).getText();
		} else if (component.getClass().getName().equals(JEditorPane.class.getName())) {
			return ((JEditorPane) component).getText();
		}
		return null;
	}

	public String getSelectedTabComponentText() {
		JComponent component = getSelectedTabComponent();
		if (component instanceof JTextArea || component instanceof SandboxTextArea) {
			return ((JTextArea) getSelectedTabComponent()).getText();
		} else if (component instanceof JTextPane || component instanceof SandboxTextPane) {
			return ((JTextPane) getSelectedTabComponent()).getText();
		} else if (component instanceof JTable) {
			JTable table = (JTable) component;
			StringBuffer buffer = new StringBuffer();

			Enumeration<TableColumn> enumeration = table.getTableHeader().getColumnModel().getColumns();
			enumeration.nextElement(); // skip sandbox-added first column
			while (enumeration.hasMoreElements()) {
				buffer.append(enumeration.nextElement().getHeaderValue()).append(",");
			}
			buffer.deleteCharAt(buffer.length() - 1); // get rid of final comma
			buffer.append("\n");

			TableModel model = table.getModel();
			for (int row = 0; row < model.getRowCount(); row++) {
				for (int column = 0; column < model.getColumnCount() - 1; column++) {
					
					String replacedValue = (String) model.getValueAt(row, column);
//					replacedValue = replacedValue.replaceAll(System.getProperty("line.separator", "\n"), "<SBNL>");
					replacedValue = replacedValue.replaceAll("\r\n", "<SBNL>");
					replacedValue = replacedValue.replaceAll("\n", "<SBNL>");
					buffer.append(replacedValue.replaceAll(",",
							SandboxParameters.SANDBOX_COMMA_DELIMITER)).append(",");
				}
				buffer.deleteCharAt(buffer.length() - 1);
				buffer.append("\n");
			}
			return buffer.toString();
		} else if (component instanceof JTextPane) {
			return ((JTextPane) getSelectedTabComponent()).getText();
		} else if (component instanceof JEditorPane) {
			return ((JEditorPane) getSelectedTabComponent()).getText();
		}
		return null;
	}

	void goToInTab(int pos, Component component) {
		if (component instanceof JTable) {
			JTable table = (JTable) component;

			table.setRowSelectionInterval(pos, pos);
			table.scrollRectToVisible(new Rectangle(table.getCellRect(pos, pos, true)));
		} else if (component instanceof SandboxList) {
			SandboxList list = (SandboxList) component;
			list.setSelectedIndex(pos);
		} else {
			JTextComponent textComponent = (JTextComponent) component;

			String string = textComponent.getText();

			textComponent.requestFocus();
			int backup = 0; // 10 > pos ? pos : 10;
			// int forward = pos > string.length()? string.length(): pos;
			textComponent.select(backup, pos);
		}

	}

	void goToInTab(int row, int col, Component component) {
		if (component instanceof JTable) {
			JTable table = (JTable) component;

			table.setRowSelectionInterval(row, row);
			table.scrollRectToVisible(new Rectangle(table.getCellRect(row, col, true)));
		} else if (component instanceof SandboxList) {
			SandboxList list = (SandboxList) component;
			list.setSelectedIndex(row);
		} else {
			JTextComponent textComponent = (JTextComponent) component;

			String string = textComponent.getText();
			int indexForRow = Utilities.indexOfNthCharInString(string, '\n', row);

			textComponent.requestFocus();
			textComponent.select(indexForRow + 1, indexForRow + (col == 0 ? 10 : col));
		}

	}

	void highlightSearchedTextInTab(String searchString, Component component, boolean regex) {
		if (component instanceof JTable) {
			JTable table = (JTable) component;

			int row = Utilities.findInTable(searchString, table, regex);
			printLine("Row = " + row, true, true);
			table.setRowSelectionInterval(row, row);
			table.scrollRectToVisible(new Rectangle(table.getCellRect(row, 0, true)));
		} else if (component instanceof SandboxList) {
			SandboxList list = (SandboxList) component;
			// List<Object> items = new ArrayList<Object>();
			List<Integer> items = new ArrayList<Integer>();
			for (int index = 0; index < list.getModel().getSize(); index++) {
				String itemValue = (String) list.getModel().getElementAt(index);

				if (itemValue.toLowerCase().indexOf(searchString.toLowerCase()) >= 0) {
					items.add(index);
				}
			}
			if (items.size() > 1) {
				list.getSelectionModel();
				list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			}
			int[] indices = new int[items.size()];
			int index = 0;
			for (Integer itemIndex : items) {
				indices[index++] = itemIndex;
			}
			for (int listIndex : indices) {
				printIt(String.valueOf(listIndex)); printIt(";");
			}
			list.setSelectedIndices(indices);
		} else {
			int[] searchStartEnd = findTextInTab(searchString, component, regex);
			JTextComponent textComponent = (JTextComponent) component;
			String strStatus = "Search - line/start/stop/ ";
			strStatus += textComponent.getText().substring(0, searchStartEnd[0]).split("\n").length;
			strStatus += "/";
			for (int number : searchStartEnd) {
				strStatus += number;
				strStatus += "/";
			}
			printLine(strStatus, true, true);
			alert(strStatus);
			
			textComponent.requestFocus();
			textComponent.select(searchStartEnd[0], searchStartEnd[1]);
		}

	}

	void replaceSearchedTextInTab(String searchString, String replaceString, Component component, boolean regex) {
		int[] searchStartEnd = findTextInTab(searchString, component, regex);
		JTextComponent textComponent = (JTextComponent) component;
		textComponent.requestFocus();
		textComponent.select(searchStartEnd[0], searchStartEnd[1]);
		textComponent.replaceSelection(replaceString);
	}

	int[] findTextInTab(Component component) {
		return findTextInTab(getFocusedText(), component, false);
	}

	int[] findTextInTab(String searchString, Component component, boolean regex) {
		if (searchString == null) {
			searchString = (String) this.searchComboBox.getSelectedItem();
		}
		int[] searchStartEnd = { 0, 0 };
		if (component instanceof SandboxList) {
			// Handle search of the test list
			printLine(SandboxParameters.SANDBOX_UNDER_CONSTRUCTION);
		} else {
			try {
				JTextComponent textComponent = (JTextComponent) component;
				int startFrom = textComponent.getCaretPosition();
				if (regex) {
					Pattern pattern = Pattern.compile(searchString, Pattern.MULTILINE);
					
					
					//Matcher matcher = pattern.matcher(textComponent.getText().replaceAll("\r\n", "\n"));
					Matcher matcher = null;
					if (component instanceof SandboxTextArea) {
						matcher = pattern.matcher(textComponent.getText());
					} else {
						matcher = pattern.matcher(textComponent.getText().replaceAll("\r\n", "\n"));
					}
					
					
					matcher.reset();
					if (matcher.find(startFrom)) {
						searchStartEnd[0] = matcher.start();

						searchStartEnd[1] = matcher.end();
						textComponent.setCaretPosition(matcher.end());
					}
				} else {
					// searchStartEnd[0]=
					// textComponent.getText().toLowerCase().indexOf (
					searchStartEnd[0] = textComponent.getText().replaceAll("\r\n", "\n")
							.indexOf(searchString.toLowerCase(), startFrom);
					searchStartEnd[1] = searchStartEnd[0] + searchString.length();
				}
				if (searchStartEnd[0] < 0) {

					searchStartEnd[0] = 0;
					searchStartEnd[1] = 0;
				}
			} catch (Exception en) {
				printLine(en.getClass().getSimpleName() + ": Could not search '" + searchString + "' in this text component.");
//				en.printStackTrace();
			}
		}
		return searchStartEnd;

	}

	public String createGlobalMapKey(Class classToCreate, boolean replace) {
		return createGlobalMapKey(classToCreate, replace, null);
	}

	public String createGlobalMapKey(Class classToCreate, boolean replace, SandboxTestBase test) {
		String key = classToCreate.getName();
		if (!replace) {
			Object obj = getGlobals().getMap().get(key);

			if (obj != null) {
				key = classToCreate.getName() + (new Date()).getTime();
				if (test == null) {
					printLine("New key for old object " + classToCreate.getName() + ": " + key, true, true);
				} else {
					test.printLine("New key for old object " + classToCreate.getName() + ": " + key);
				}
			}
		}
		return key;

	}

	/**
	 * Assign a different key to the current object for later processing.Add the
	 * new object with the old object's key.
	 *
	 * @param obj
	 * @return
	 */
	public Object removeFromGlobalMap(Object mapId) {
		ConcurrentHashMap<String, SandboxConnection> map = getGlobals().getMap();
		SandboxConnection connection = map.get(mapId);
		printLine("Removing from global map: " + connection, true, true);
		connection = null;
		return getGlobals().getMap().remove(mapId);
	}

	public String replaceInGlobalMap(Object obj) {
		return replaceInGlobalMap(obj, null);
	}

	public String replaceInGlobalMap(Object mapId, Object obj) {
		removeFromGlobalMap(mapId);
		return addToGlobalMap((String) mapId, obj);
	}

	public String replaceInGlobalMap(Object obj, SandboxTestBase test) {
		String key = obj.getClass().getName();
		return replaceInGlobalMap(key, obj, test);
	}

	public String replaceInGlobalMap(String key, Object obj, SandboxTestBase test) {
		SandboxConnection connection = getGlobals().getMap().get(key);
		if (connection != null) {
			addToGlobalMap(connection.getObj(), test); // put object back in map
														// with
														// different key for
														// later
														// handling
		}
		getGlobals().getMap().put(key, new SandboxConnection(obj));
		test.printLine("Adding " + obj.getClass().getName() + " with key " + key + " to global map");
		return key;
	}

	public String addToGlobalMap(Object obj) {
		return addToGlobalMap(obj, null);
	}
	public String addToGlobalMap(Object obj, SandboxTestBase test) {
		String key = createGlobalMapKey(obj.getClass(), false, test);
		addToGlobalMap(key, obj);
		return key;
	}
	public String addToGlobalMap(String key, Object obj) {
		return addToGlobalMap(key, obj, null);
	}
	public String addToGlobalMap(String key, Object obj, SandboxTestBase test) {
		if (test == null) {
			printLine("Adding object with key \"" + key + "\" to global map", true, true);
		} else {
			test.printLine("Adding object with key \"" + key + "\" to global map");
		}
		getGlobals().getMap().put(key, new SandboxConnection(obj));
		
/*//////////TODO	Re22.02.10.14.56.22.968	
		
		if (obj instanceof SandboxTestTab) {
			xxx
		}
		
//*/////////////
		
		return key;
	}

	/**
	 * Put object of this class in the global map
	 *
	 * @param classToCreate
	 * @return
	 */
	public String createGlobalObject(Class classToCreate) {
		return createGlobalObject(classToCreate, null);
	}

	public String createGlobalObject(Class classToCreate, SandboxTestBase test) {
		String key = createGlobalMapKey(classToCreate, false);
		try {
			Object obj = classToCreate.newInstance();
			getGlobals().getMap().put(key, new SandboxConnection(obj));
		} catch (Exception en) {
			if (test == null) {
				printLine("Couldn't create global object");
			} else {
				printLine("Couldn't create global object");
			}
		}

		return key;
	}
	
	public void alert(String alert) {
		alert(alert, this.iconPretty);
	}
	public void alert(String alert, Icon icon) {
		JTextArea textArea = new JTextArea();
		textArea.setToolTipText("Alert: " + alert);
		String[] clf = {"Monospace", "0", "11"};
		try {
			clf = URLDecoder.decode(System.getProperty("sbx.conf.textFont","Monospace_0_11"), "US-ASCII").split("_");
		} catch(UnsupportedEncodingException uee) {
			System.err.println(clf);
		}
		Font textFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));

		textArea.setFont(textFont);
		textArea.setBackground(Color.white);
		textArea.setForeground(Color.black);
		textArea.setText(alert);
		textArea.addMouseListener(sandboxParameters.mouseListener);
		textArea.addKeyListener(sandboxParameters.keyListener);
		sandboxParameters.componentPanel.removeAll();
		if (icon != null) {
			sandboxParameters.componentPanel.add(new JLabel(icon));
		}
		sandboxParameters.componentPanel.add(textArea);
		sandboxParameters.componentPanel.updateUI();
	}
	
	public void hideMessages() {
		for (SandboxConnection connection : getGlobals().getMap().values()) {
			if (connection.getObj() instanceof SandboxMessage) {
				SandboxMessage sandboxMessage = (SandboxMessage) connection.getObj();
				if (sandboxMessage.isAlwaysOnTop() && sandboxMessage.getState() != JFrame.ICONIFIED) {
					sandboxMessage.setState(JFrame.ICONIFIED); // To minimize a frame
					hiddenMessages.add(connection);
				}
			}
			if (connection.getObj() instanceof JFrame) {
				JFrame frame = (JFrame) connection.getObj();
				if (frame.isAlwaysOnTop() && frame.getState() != JFrame.ICONIFIED) {
					frame.setState(JFrame.ICONIFIED); // To minimize a frame
					hiddenMessages.add(connection);
				}
			}
		}
	}

	public void showMessages() {
		
		for (SandboxConnection connection : hiddenMessages) {
			if (connection.getObj() instanceof SandboxMessage) {
				SandboxMessage sandboxMessage = (SandboxMessage) connection.getObj();
				sandboxMessage.setState(JFrame.NORMAL); // To max a frame
				hiddenMessages.remove(connection);
			}
			if (connection.getObj() instanceof JFrame) {
				JFrame frame = (JFrame) connection.getObj();
				frame.setState(JFrame.NORMAL); // To max a frame
				hiddenMessages.remove(connection);
			}
		}
		
		
//		for (SandboxConnection connection : getGlobals().getMap().values()) {
//			if (connection.getObj() instanceof SandboxMessage) {
//				SandboxMessage sandboxMessage = (SandboxMessage) connection.getObj();
//				sandboxMessage.setState(JFrame.NORMAL); // To max a frame
//			}
//			if (connection.getObj() instanceof JFrame) {
//				JFrame frame = (JFrame) connection.getObj();
//				frame.setState(JFrame.NORMAL); // To max a frame
//			}
//		}
	}

	class ComponentFocusListener implements FocusListener {
		public JComponent component = null;
		public SandboxParameters params = null;

		public ComponentFocusListener(SandboxParameters params) {
			this.params = params;
		}

		@Override
		public void focusGained(FocusEvent e) {
			System.out.println("Focus gained : component name = " + e.getComponent().getClass().getName());
			try {
				component = (JComponent) e.getSource();
				if (component.getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {
					System.err.println("Uh Oh!");
					params.unhiglightButtons();
					String[] toolTipArray = component.getToolTipText().split(":");
					for (int i = 0; i < toolTipArray.length; i++) {
						String[] testNameArray = toolTipArray[i].split("&");
						String testName = BUTTON_PREFIX + testNameArray[testNameArray.length - 1].trim();
						JButton button = params.buttons.get(testName);
						button.setForeground(params.buttonHighlightColor);
					}
				}
			} catch (ClassCastException cce) {
				System.out.println("Not a parameter.");
			} catch (NullPointerException npe) {
				System.out.println("Not a reportable parameter.");
			} catch (Throwable thr) {
				printLine(thr);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			System.out.println("Focus lost : component name = " + e.getComponent().getClass().getName());
			try {
				if (e.getOppositeComponent().getClass().getEnclosingClass() != null
							&& e.getOppositeComponent().getClass().getEnclosingClass().newInstance() instanceof BasicComboBoxEditor) {

					JComboBox comboBox = (JComboBox) e.getOppositeComponent().getParent();
					printLine(comboBox.getName(), true, true);
					if (comboBox.getName().equals(SEARCH_COMBOBOX_NAME)) {
						params.focusListener.setComponent(comboBox);
					}
				}
			} catch (Throwable throwable) {
				printLine("Focusing", true, true);
			}
		}

		void setComponent(JComponent component) {
			this.component = component;
		}
	}

	// For alarms and periodicals
	protected ScheduledFuture scheduleHandle = null;

	class TimeRunner implements Runnable {
//		private int messagesCount = 0;
		private Map<String, SandboxMessage> messageMap = null;

		public TimeRunner() {
			messageMap = new HashMap<String, SandboxMessage>();
		}

		@Override
		public void run() {
			SimpleDateFormat hourMinuteFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat dmyformat = new SimpleDateFormat("ddMMMyy");
			SimpleDateFormat dotwFormat = new SimpleDateFormat("E");
			SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMMyy.HH:mm");
			SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
			SimpleDateFormat yearFormat = new SimpleDateFormat("yy");
			SimpleDateFormat dayFormat = new SimpleDateFormat("dd");

			Date now = new Date();
			
			SandboxFrameBase.messageCount = 0; // reset the offset for centering on screen

			Properties systemProps = System.getProperties();
			Enumeration<?> systemPropNames = systemProps.propertyNames();
			while (systemPropNames.hasMoreElements()) {
				String propName = (String) systemPropNames.nextElement();
				if (propName.startsWith("sbx.time")) {
					if (messageMap.get(propName) != null) {

						SandboxMessage sandboxMessage = messageMap.get(propName);
						if (sandboxMessage.getStatus() == 1) {
							messageMap.remove(propName); // A message has been
															// OKed so ready to
															// show again next
															// time
							continue;
						} else if (sandboxMessage.getStatus() == -1) {
							messageMap.remove(propName); // A message has been
															// cancelled so
															// allow future
															// start by removing
															// from map,and stop
															// by removing from
															// environment
							System.clearProperty(propName);
							continue;
						}
						continue; // Leave because no interaction with message
									// yet; no need to duplicate existing
									// message
					}
				}
				if (propName.startsWith("sbx.time.eso")) {
					try {
						if (alertUp) {
							printLine("Reminder " + propName + " stopped by alert that's up.");
							continue; //JOptionPane is up and this would lock sandbox
						}
						String[] timeStrings = systemProps.getProperty(propName, "10:15__01:05__5__No+eso+set")
								.split(sandboxDelimiter);
						String dmy = dmyformat.format(now);
						String month = monthFormat.format(now);
						String year = yearFormat.format(now);
						String day = dayFormat.format(now);

						Date planned = null;
						
						if (timeStrings[0].matches("^EDY\\.\\d\\d:\\d\\d.*$")) { //EDY
							planned = dateFormat.parse(dmy + "." + timeStrings[0].split("\\.")[1]);
						} else if (timeStrings[0].matches("^LWD\\.\\d\\d:\\d\\d.*$")) { //LWD
							LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
							LocalDate lastWorkDayCurrentMonth = Utilities.getLastWorkingDayOfMonth(lastDayofCurrentMonth);
							DateTimeFormatterBuilder dtfBuilder = new DateTimeFormatterBuilder();
							dtfBuilder.appendPattern("ddMMMyy");
							if (dmy.equals(lastWorkDayCurrentMonth.format(dtfBuilder.toFormatter()))) {
								planned = dateFormat.parse(dmy + "." + timeStrings[0].split("\\.")[1]);
							} else {
								System.clearProperty(propName);
							}
							
//*////////////////////////////////////DMH							
						} else if (timeStrings[0].matches("^MWD\\.\\d\\d:\\d\\d.*$")) { //MWD
							
							LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
							LocalDate middleDayofCurrentMonth = lastDayofCurrentMonth.minusDays(16);
							if (lastDayofCurrentMonth.getDayOfMonth() == 30) {
								middleDayofCurrentMonth = lastDayofCurrentMonth.minusDays(15);
							}
							LocalDate middleWorkdayCurrentMonth = Utilities.getLastWorkingDayOfMonth(middleDayofCurrentMonth);
							DateTimeFormatterBuilder dtfBuilder = new DateTimeFormatterBuilder();
							dtfBuilder.appendPattern("ddMMMyy");
							if (dmy.equals(middleWorkdayCurrentMonth.format(dtfBuilder.toFormatter()))) {
								planned = dateFormat.parse(dmy + "." + timeStrings[0].split("\\.")[1]);
							} else {
								System.clearProperty(propName);
							}
/*///////////////////////////////////////
						} else if (timeStrings[0].matches("^MWD\\.\\d\\d:\\d\\d.*$")) { //MWD
							
							LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
							LocalDate middleDayofCurrentMonth = lastDayofCurrentMonth.minusDays(15);
							LocalDate middleWorkdayCurrentMonth = Utilities.getLastWorkingDayOfMonth(middleDayofCurrentMonth);
							DateTimeFormatterBuilder dtfBuilder = new DateTimeFormatterBuilder();
							dtfBuilder.appendPattern("ddMMMyy");
							if (dmy.equals(middleWorkdayCurrentMonth.format(dtfBuilder.toFormatter()))) {
								planned = dateFormat.parse(dmy + "." + timeStrings[0].split("\\.")[1]);
							} else {
								System.clearProperty(propName);
							}
						
//*///////////////////////////////////////
							
							
						} else if (timeStrings[0].matches("^[a-zA-Z]{3}\\.\\d\\d:\\d\\d.*$")) { //DOTW
							if (timeStrings[0].startsWith(dotwFormat.format(now))) {
								planned = dateFormat.parse(dmy + "." + timeStrings[0].split("\\.")[1]);
							} else {
								System.clearProperty(propName);
							}
						} else if (timeStrings[0].matches("^\\d\\dMTH\\d\\d.*$")) { //Date - Month doesn't matter
							String specifiedDmy = timeStrings[0].substring(0, 7);
							specifiedDmy = specifiedDmy.replace("MTH", month);
							if (dmy.equals(specifiedDmy)) { //Is today the day of the month?
								planned = dateFormat.parse(timeStrings[0].replace("MTH", month));
							} else {
								System.clearProperty(propName);
							}
							
						} else if (timeStrings[0].matches("^\\d\\d[a-zA-Z]{3}\\d\\d.*$")) { //Date
							planned = dateFormat.parse(timeStrings[0]);
						} else if (timeStrings[0].matches("^\\d\\d:\\d\\d.*$")) { //Time
							planned = dateFormat.parse(dmy + "." + timeStrings[0]);
						} else if (planned == null) {
							throw new SandboxException(propName + " is malformed");
						}
						
						if (planned != null && now.after(planned)) {
							int minutesOpen = Integer.parseInt(timeStrings[2]);

							String title = "[" + SandboxParameters.targetPackageName + "] - "
									+ hourMinuteFormat.format(now) + " - ("
									+ ManagementFactory.getRuntimeMXBean().getName() + ")";

							String[] deltas = timeStrings[1].split(":");

							long hourDelta = Long.parseLong(deltas[0]) * 60 * 60 * 1000;
							long minuteDelta = Long.parseLong(deltas[1]) * 60 * 1000;
							planned.setTime(now.getTime() + hourDelta + minuteDelta - 2000); // subtract 2 seconds to stay within the delta window

							timeStrings[0] = hourMinuteFormat.format(planned);

							printLine(timeStrings[0] + " :: Next message for " + propName, true, true);
							String propertyValue = timeStrings[0] + sandboxDelimiter + timeStrings[1] + sandboxDelimiter + timeStrings[2] + sandboxDelimiter
									+ timeStrings[3];
							System.setProperty(propName, propertyValue);
							String defaultValue = "30Jan56.10:15" + SandboxParameters.DEFAULT_DELIMITER + "01:05"
									+ SandboxParameters.DEFAULT_DELIMITER + "5" + SandboxParameters.DEFAULT_DELIMITER + "No eso set";

							StringBuffer value = new StringBuffer("30Jan56." + systemProps.getProperty(propName, defaultValue));
							int startOfMessage = value.lastIndexOf(SandboxParameters.DEFAULT_DELIMITER) + SandboxParameters.DEFAULT_DELIMITER.length();
							value.insert(startOfMessage, "{sbx-url-ascii ");
							value.append("}");
							
							String message = timeStrings[3] + "\n" + SandboxParameters.VISUAL_SEPARATOR
									+ "\nNext message at " + timeStrings[0] + "\nOpen for " + minutesOpen + " minutes"
									+ "\nsbx.eso.message," + value
									+ "\n" + propName + ",{sbx-prop sbx.eso.message}"
									+ "\n" + "sandbox.java." + propName + ",{sbx-prop sbx.eso.message}"
									+ "\n" + SandboxParameters.VISUAL_SEPARATOR;
							if (propName.startsWith("sbx.time.eso.run")) {
								alert("could have run! " + timeStrings[3]);
								//			params.runTest(recordedTest, Utilities.limitTabLength(search[0]));
								String recordedTest = URLDecoder.decode(timeStrings[3], "US-ASCII");
								alert(propName, iconRunning);
								runTest(recordedTest);
							} else {
								showMessage(title, message, minutesOpen, 
									new SandboxBasket<String,String>(propName, systemProps.getProperty(propName, defaultValue)));
							}
						} else {
							printLine(propName + " Planned for: " + Utilities.parseDate(planned, SandboxParameters.DATE_FORMAT));
						}

					} catch (Exception en) {
						printLine(propName + " not handled correctly. Removing from Java properties", true, true);
						System.clearProperty(propName);
					}
				}
			}
			// TODO something like this is going to have to be done to keep the
			// messages from sliding of the edge of the screen
			SandboxFrameBase.messageCount = 0;

		}

		void showMessage(String title, String message, int minutesOpen, SandboxBasket<String,String> basket) throws SandboxException {
			Toolkit.getDefaultToolkit().beep();

			try {
				/*
				 * sandbox.SandboxMessage.SandboxMessage(String title, String message, Component parentComponent, 
				 * int maxLinesVisible, int closeSeconds, boolean modal, boolean editable, 
				 * int messageCount, SandboxBasket<String, String> envSet)
				 * 
				 */
				SandboxMessage sandboxMessage = new SandboxMessage(title, URLDecoder.decode(message, "US-ASCII"),
						tabbedPane, 20, minutesOpen * 60, false, false, SandboxFrameBase.messageCount ++, basket);
				messageMap.put(basket.getName(), sandboxMessage);
				sandboxMessage.setAlwaysOnTop(true);
				sandboxMessage.setSavedAOT(true);
				if (SandboxFrameBase.messageCount > 5) {
					SandboxFrameBase.messageCount = -5;
				}
			} catch (Throwable thr) {
				throw new SandboxException("Bad thing with timer: " + thr.getMessage());
			}
		}
	}

	class ComboBoxActionListener implements ActionListener {

		public JComboBox combobox = null;

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("comboBoxChanged")) {
				combobox = (JComboBox) e.getSource();
				Object selectItem = combobox.getSelectedItem();
				int itemCount = combobox.getItemCount();
				Set<Object> allItems = new HashSet<Object>();
				boolean match = false;
				for (int i = 0; i < itemCount; i++) {
					Object obj = combobox.getItemAt(i);
					allItems.add(obj);

				}
				if (allItems.contains(selectItem)) {

					match = true;
				}
				if (!match && (selectItem != null)) {
					if (selectItem.equals("")) {
						combobox.removeItem(selectItem);

					} else {
						combobox.addItem(((String) selectItem).trim());
					}
				}
			}
		}
	}

	class SandboxComboBoxUI extends BasicComboBoxUI {
		private Icon icon;
		private int comboBoxButtonFontSize = 8;

		public SandboxComboBoxUI() {
			super();
		}

		public SandboxComboBoxUI(Icon icon, int comboBoxButtonFontSize) {
			super();
			this.icon = icon;

			this.comboBoxButtonFontSize = comboBoxButtonFontSize;
		}

		@Override
		protected JButton createArrowButton() {
			JButton button = new JButton();
			Font newComboBoxFont = new Font(button.getFont().getName(), Font.PLAIN, comboBoxButtonFontSize);

			button.setFont(newComboBoxFont);

			button.setIcon(this.icon);
			button.setOpaque(false);
			button.setFocusable(false); // This doesn’t work
			return button;
		}
	}

	class SandboxIcon implements Icon {
		private int x, y, width, height;
		private String string;
		private Color color;

		public SandboxIcon(String string, int x, int y, int width, int height, Color color) {
			this.string = string;

			this.x = x;
			this.y = y;
			this.width = width;

			this.height = height;

			this.color = color;
		}

		@Override
		public int getIconWidth() {

			return width;
		}

		@Override
		public int getIconHeight() {
			return height;
		}

		@Override
		public void paintIcon(Component component, Graphics graphics, int x, int y) {
			char[] charString = this.string.toCharArray();
			graphics.setColor(color);
			graphics.drawChars(charString, 0, charString.length, this.x, this.y);
		}
	}
	
	public Globals getGlobals() {
		// TODO Default getter,check logic
		return globals;
	}

	public void setGlobals(Globals globals) {
		this.globals = globals;
	}

	public String getTargetPackageName() {
		return targetPackageName;
	}

	public void setTargetPackageName(String targetPackageName) {
		SandboxParameters.targetPackageName = targetPackageName;
	}

	public Logger getSandboxLogger() {
		return sandboxLogger;
	}

	public JComponent getTestComponent() {
		return testComponent;
	}

	public void setTestComponent(JComponent testComponent) {
		this.testComponent = testComponent;
	}

	/**
	 * @return the configurationRoot
	 */
	public String getConfigurationFileRoot() {
		return configurationFileRoot;
	}

	public String getConfigurationDirectoryRoot() {
		return configurationDirectoryRoot;
	}

	public String getConfigurationDirectory() {
		return configurationDirectory;
	}

	/**
	 * @return the buttons
	 */
	public Hashtable<String, JButton> getButtons() {
		return buttons;
	}

	protected List<NotifyAdvice> getListNotifyAdvice() {
		return listNotifyAdvice;
	}

	public long getMaxFileOpenSize() {
		return maxFileOpenSize;
	}

	public void setMaxFileOpenSize(long maxFileOpenSize) {
		this.maxFileOpenSize = maxFileOpenSize;
	}

	public int getMaxByteArraySize() {
		return maxByteArraySize;
	}

	public void setMaxByteArraySize(int maxByteArraySize) {
		this.maxByteArraySize = maxByteArraySize;
	}
	
	public String getSandboxDelimiter() {
		return sandboxDelimiter;
	}

	public JTabbedPane getTabbedPane() {
		return this.tabbedPane;
	}

	protected boolean isAlertUp() {
		return alertUp;
	}

	protected void setAlertUp(boolean alertUp) {
		this.alertUp = alertUp;
	}
}
