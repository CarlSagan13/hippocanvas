// Tool6.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = SandboxParameters.SANDBOX_UNDER_CONSTRUCTION + " Rename files",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME, "searchReplace,fileRegex,directory",
			SandboxParameters.TRUEFALSE_NAME, "recurse",
			SandboxParameters.YESNO_NAME, "change",
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool100 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		String searchReplace = getString("searchReplace");
		String search = searchReplace.split(SandboxParameters.DEFAULT_DELIMITER)[0];
		String replace = searchReplace.split(SandboxParameters.DEFAULT_DELIMITER)[1];
		String fileRegex = getString("fileRegex");
		String directory = getString("directory");
		boolean recurse = getBoolean("recurse");

		List<String> excluded = new ArrayList<String>();

		try {
			File dir = new File(directory);
			if (!dir.exists()){
				throw new SandboxException("The directory does not exist!");
			}
			if (!dir.isDirectory()) {
				throw new SandboxException("Not a directory! ");
			}
			Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
			printALine();
			for (File file : Utilities.listFilesRegex(dir, recurse, false, pattern, this)) {
				printLine(file.getAbsolutePath());
				
				//change file name:
					//determine new name from grouping string
				
				
				
				if (!file.canWrite()){
					printLine("\tLOCKED!");
				}
				checkRunning();
			}
			printALine();
		}catch(ArrayIndexOutOfBoundsException aioobe){
			printLine("Configuration might be wrong",aioobe);
		}catch(NullPointerException npe){
			printLine ("Parameter error",npe);
		}
		if (excluded.size() > 0) {
			printLine("***EXCLUDED ***");
			this.addToGlobalMap(getTestName() + "Excluded", excluded);
		}
	}
}
