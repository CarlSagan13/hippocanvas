//SandboxAnnotation.java (erase me later)
package sandbox;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SandboxAnnotation {
	public String description();

	public String[] parameterDescriptions();

	public boolean isTest();

	public boolean isTool();
	
	public boolean showInTabPanel();

	public boolean showInButtonPanel();
}
