package sandbox;

import java.io.IOException;
import java.net.ServerSocket;

@SandboxAnnotation(
		description = "Local port scanner (if untilGone, try every 5 seconds)", 
		parameterDescriptions = {
		SandboxParameters.FOCUSED_NAME, "csvStartEnd", 
		SandboxParameters.TRUEFALSE_NAME, "untilGone" 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool16 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST

		boolean untilGone = getBoolean("untilGone");

		String[] saStartEnd = getStringArray("csvStartEnd");
		int startPort = Integer.parseInt(saStartEnd[0]);
		int endPort = startPort;

		if (saStartEnd.length == 2) {
			endPort = Integer.parseInt(saStartEnd[1]);
		}

		printLine("Start: " + startPort + " End: " + endPort);
		if (endPort <= 0 || endPort > 65535) {
			endPort = 65536;
		}

		do {
			boolean found = false;
			for (int port = startPort; port <= endPort; port++) {
				try {
					(new ServerSocket(port)).close();
				} catch (IOException ioe) {
					found = true;
					printLine(port + " :: " + ioe.getMessage());
				} finally {
					if (!this.isRunning) {
						break;
					}
				}
			}

			untilGone = untilGone && found;
			if (untilGone) {
				this.checkRunning(5);
			}

			printLine(SandboxParameters.VISUAL_SEPARATOR);
		} while (untilGone);
		// END TEST
	}

}
