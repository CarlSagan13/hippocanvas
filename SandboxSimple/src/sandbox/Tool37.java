// Tool37.java (erase me later)
package sandbox;

import java.io.File;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Validate classpath",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"classpath",
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool37 extends SandboxTestBase {

	@Override 
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String classpath = getString("classpath");
		
		String classpathDelimiter = System.getProperty("path.separator",";");
		
		Set<String> resources = new TreeSet<String>(Arrays.asList(classpath.split(classpathDelimiter)));

		printLine("INVALID RESOURCES:\n"+ SandboxParameters.VISUAL_SEPARATOR);
		for (String resource : resources){
			File file = new File(resource);
			if (!file.exists()){
				printLine(file.getName()+ "::"+ file.getPath());
			}		
		}
		//END TEST
	}
}