package sandbox;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(	
		description = "Find java class",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"className,directory",
			SandboxParameters.TRUEFALSE_NAME,"recurse"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool3 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String className = getString ("className");
		String directory = getString("directory");
		boolean recurse = getBoolean("recurse");
		
		try {
			File dir = new File(directory);
			if (dir.exists()){
				Map <String,List<String>> foundClasses = sandbox.Utilities.findClasses(dir,className,recurse,this);

				for (String key : foundClasses.keySet()){
					List <String> classNames = foundClasses.get(key);
					if (classNames.size()> 0){
						printLine(SandboxParameters.VISUAL_SEPARATOR);
						printLine(key);
						for (String name : classNames){
							printLine("\t "+ name);
						}
					}
					checkRunning();
				}
			}else {
				printLine("Directory does not exist");
			}
		}catch (IOException ioe){
			printLine(ioe.getMessage ());
		}
		//END TEST
	}

}