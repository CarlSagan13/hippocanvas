//SandboxList.java (erase me later) 
package sandbox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

public class SandboxList extends JList implements SandboxCommon {
	public SandboxList(ListModel dataModel) {
		super(dataModel);
		initStyle();
//		try {
//			int listFixedHeight = Integer.parseInt((String)System.getProperty("sbx.conf.listFixedHeight", "-1"));
//			if (listFixedHeight > 0) {
//				this.setFixedCellHeight(listFixedHeight);
//			}
//		} catch(NumberFormatException nfe){
//			sandboxLogger.debug("Environment sbx.conf.listFixedHeight is not set.");
//		}//TODO Auto-generated constructor stub
	}
	public SandboxList(Object[] listData){
		super(listData);
		
		
		try {
			int sandboxVisibleRowCount = Integer.parseInt(System.getProperty("sbx.conf.visible.row.count", "-1"));
			if (listData.length < sandboxVisibleRowCount) {
				this.setVisibleRowCount(listData.length);
			} else {
				if (sandboxVisibleRowCount > 0) {
					this.setVisibleRowCount(sandboxVisibleRowCount);
				}
			}
		} catch(NumberFormatException nfe) {
			sandboxLogger.log(Level.ALL, "Environment sbx.visible.row.count is not set.");
		}
		
		initStyle();

		
//		try {
//			int listFixedHeight = Integer.parseInt((String)System.getProperty("sbx.conf.listFixedHeight", "-1"));
//			if (listFixedHeight > 0){
//				this.setFixedCellHeight(listFixedHeight);
//			}
//		}catch(NumberFormatException nfe){
//			sandboxLogger.debug("Environment sbx.conf.listFixedHeight is not set.");
//		}
//		this.setCellRenderer(new MyCellRenderer());
	}
	public SandboxList(Vector listData){
		super(listData);
		
		initStyle();

//		try {
//			int listFixedHeight = Integer.parseInt((String)System.getProperty("sbx.conf.listFixedHeight", "-1"));
//			if (listFixedHeight > 0){
//				this.setFixedCellHeight(listFixedHeight);
//			}
//		}catch(NumberFormatException nfe){
//			sandboxLogger.debug("Environment sbx.conf.listFixedHeight is not set.");
//		}
	}
	public SandboxList(){
		super();
		
		initStyle();

//		try {
//			int listFixedHeight = Integer.parseInt((String)System.getProperty("sbx.conf.listFixedHeight","-1"));
//			if (listFixedHeight > 0){
//				this.setFixedCellHeight(listFixedHeight);
//			}
//		} catch(NumberFormatException nfe){
//			sandboxLogger.debug("Environment sbx.conf.listFixedHeight is not set.");
//		}
	}
	
	private void initStyle() {
		Font listFont = this.getFont().deriveFont(Font.ITALIC);
		try {
			String[] clf = System.getProperty("sbx.conf.listFont").split("_");
			listFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
			setFont(listFont);
		} catch(Exception en) {
			System.out.println("sbx.conf.listFont not set correctly (<Font Name>_<Type>_<Size> e.g. '" 
					+ SandboxParameters.FONT_DEFAULT + "')");
		}
		
		try {
			int listFixedHeight = Integer.parseInt(System.getProperty("sbx.conf.listFixedHeight","-1"));
			if (listFixedHeight > 0) {
				setFixedCellHeight(listFixedHeight);
			}
		} catch(Exception en) {
			System.out.println("sbx.conf.listFixedHeight not set correctly (e.g. '15');");
		}
		
		setOpaque(true);

	}
	
/*/////////////////////////////////////DMH
    	class MyCellRenderer extends JLabel implements ListCellRenderer <Object> {
    		
    		@Override
    		public Component getListCellRendererComponent(
    				JList<?> list,
    				Object value,
    				int index,
    				boolean isSelected,
    				boolean cellHasFocus)
    		{
    			String s = value.toString();
    			setText(s);
    			if (isSelected) {
    				setBackground(list.getSelectionBackground());
    				setForeground(list.getSelectionForeground());
    			} else {
    				setBackground(list.getBackground());
    				setForeground(list.getForeground());
    			}
    			setEnabled(list.isEnabled());
    			Font listFont = list.getFont().deriveFont(Font.ITALIC);
    			try {
    				String[] clf = System.getProperty("sbx.conf.listFont").split("_");
    				listFont = new Font(clf[0], Integer.parseInt(clf[1]), Integer.parseInt(clf[2]));
    				setFont(listFont);
    			} catch(Exception en) {
    				System.out.println("sbx.conf.listFont not set correctly (<Font Name>_<Type>_<Size> e.g. '" 
    						+ SandboxParameters.FONT_DEFAULT + "')");
    			}
    			
    			try {
    				int listFixedHeight = Integer.parseInt(System.getProperty("sbx.conf.listFixedHeight","-1"));
    				if (listFixedHeight > 0) {
    					setFixedCellHeight(listFixedHeight);
    				}
    			} catch(Exception en) {
    				System.out.println("sbx.conf.listFixedHeight not set correctly (e.g. '15');");
    			}
    			setOpaque(true);
    			return this;
    		}
    	}
//*/////////////////////////////////////DMH

	}