// Tooll2.java (erase me later)
package sandbox;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;


import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Archive by time",
		parameterDescriptions = {
			SandboxParameters.FOCUSED_NAME,"startTime,endTime,sourceDir",
			SandboxParameters.POPPAD_NAME,"fileRegex",
			SandboxParameters.TRUEFALSE_NAME,"copy",
			SandboxParameters.YESNO_NAME,"saveOutliers"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = false, isTool = true)

public class Tool12 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String sourceDir = getString("sourceDir");
		String directoryTimeStamp = Utilities.parseDate(new Date(), SandboxParameters.DATE_FORMAT_FILE_TIMESTAMP);		
		String targetDir = params.getConfigurationDirectory() + File.separator + "storage" + File.separator + directoryTimeStamp;
		Date startTime = getDate("startTime");
		Date endTime = getDate("endTime");
		String fileRegex = getString("fileRegex");
		boolean copy = getBoolean("copy");
		boolean saveOutliers = getBoolean("saveOutliers");
		
		if (startTime.after(endTime)){
			throw new SandboxException("Time error: End time must be after start time");
		}

		String archiveDescription = null;
		if (copy || saveOutliers) {
			//Save description file
			archiveDescription = JOptionPane.showInputDialog("Enter description of this archive", params.getTabbedPane());
			if (archiveDescription == null){
				printLine("Aborted");
				return;
			}
			printLine("\nARCHIVE DESCRIPTION: "+ archiveDescription + "\n");

			StringBuffer message = new StringBuffer();
			String newline = System.getProperty("line.separator","\n");
			String tab = "\t";
			message.append("ARE YOU SURE YOU WANT TO COPY:");
			message.append(newline).append(sourceDir);
			message.append(newline).append("TO");
			message.append(newline).append(targetDir);
			message.append(newline).append("INCLUDE FROM "+ Utilities.parseDate(startTime,SandboxParameters.DATE_FORMAT));
			message.append(newline).append("TO "+ Utilities.parseDate(endTime,SandboxParameters.DATE_FORMAT));
			if ((new SandboxMessage("VERIFY",message.toString(),params.getTabbedPane(),20,true)).getStatus() < 0){
				throw new sandbox.SandboxException("You cancelled!");
			}
		}
		File source = new File(sourceDir);
		File target = new File(targetDir);
		
		Set<File> filteredFiles = new HashSet<File>();
		
		if (source.exists()){
//			Set<String> copiedSet = new HashSet<String> ();
			Pattern pattern = Pattern.compile(fileRegex, Pattern.MULTILINE);
			
			//Check is directory
			if (source.isDirectory()){
				
				filteredFiles = new HashSet<File> (Utilities.listFilesRegex(source, startTime, endTime, true, true, pattern, this));
				

				for (File file: filteredFiles) {
					String copyPath = file.getPath();
					if (copyPath.contains(":")) { //Allow for MSDOS type paths
						copyPath = copyPath.substring(copyPath.indexOf(':')	+ 1);
					}
					int indexOfColon = copyPath.indexOf(':');
		
					if (indexOfColon > 0){
						copyPath = copyPath.substring(indexOfColon + 1);
					}
					File sandboxTargetPath = new File(target.getPath() + File.separator + copyPath);
					printLine(file.getAbsoluteFile());
//					File targetFile = new File(sandboxTargetPath + File.separator + file.getName());
					if (copy) {
						printLine(sandboxTargetPath.getAbsoluteFile());
						Utilities.copyFile(file, sandboxTargetPath, true);
					}
				}
			} else {
				throw new SandboxException("Not a directory! " + source.getAbsolutePath());
			}
			
				
//			Map<Long,List<File>> mapFiles = new HashMap<Long,List<File>>();
//			if (filteredFiles != null) {
//				for (File file : filteredFiles){
//					Long lastModified = new Long(file.lastModified() / 1000);//Dividing by 1000 to eliminate trivial microsecond differences
//					List<File> stampedFiles = mapFiles.get(lastModified);
//					if (stampedFiles == null){
//						stampedFiles = new ArrayList<File> ();
//						stampedFiles.add(file);
//						mapFiles.put(lastModified,stampedFiles);
//					}else {
//						stampedFiles.add(file);
//					}
//				}
//			}
//			for (Long lastModified : mapFiles.keySet()){
//				List<File> stampedFiles = mapFiles.get(lastModified);
//				if (stampedFiles != null &&stampedFiles.size()== 1){
//					File file = stampedFiles.get(0);
//					String copyPath = file.getPath();
//					int indexOfColon = copyPath.indexOf(':');
//					if (indexOfColon > 0){
//						copyPath = copyPath.substring(indexOfColon + 1);
//					}
//					File sandboxTargetPath = new File(target.getPath()+ copyPath);
//					if (saveOutliers){
//						Utilities.saveFile(file,sandboxTargetPath,this);
//					}else {
//						printLine(file.getPath());
//						printLine("TO");
//						printLine(sandboxTargetPath.getPath());
//						printLine();
//					}
//				}
//			}

			if (saveOutliers || copy){
				this.setResultFilePath(target.getPath()+ File.pathSeparator + "results.sbf");
			}
		}else {
			printLine(sourceDir + " does not exist!");
		}
		//END TEST
	}
}

