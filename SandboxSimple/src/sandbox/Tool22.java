package sandbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;

@SandboxAnnotation(
		description = "Compare list in clipboard to selected list", 
		parameterDescriptions = {
		SandboxParameters.FOCUSED_NAME, "setl", 
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool22 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		Set<String> setl = getStringSet("setl");
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		java.awt.datatransfer.Transferable tText = clip.getContents(null);
		String clipboardText = (String) tText.getTransferData(DataFlavor.stringFlavor);
		Set<String> set2 = new HashSet<String>();
		for (String line : clipboardText.split("\n")) {
			set2.add(line);
		}
		List<String> bothLists = new ArrayList<String>();
		List<String> listOneOnly = new ArrayList<String>();
		for (Object line : setl.toArray()) {
			if (set2.contains(line)) {
				bothLists.add((String) line);
			} else {
				listOneOnly.add((String) line);
			}
		}

		List<String> listTwoOnly = new ArrayList<String>();
		for (Object line : set2.toArray()) {
			if (!setl.contains(line)) {
				listTwoOnly.add((String) line);

			}
		}

		Collections.sort(bothLists);

		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST BOTH +=\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : bothLists) {
			printLine(line);
		}

		Collections.sort(listOneOnly);

		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST ONE +=\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listOneOnly) {
			printLine(line);
		}

		Collections.sort(listTwoOnly);

		printLine(SandboxParameters.VISUAL_SEPARATOR + "\nLIST TWO +=\n" + SandboxParameters.VISUAL_SEPARATOR);
		for (String line : listTwoOnly) {
			printLine(line);
		}
		// END TEST
	}

}
