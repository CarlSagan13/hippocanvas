// Tool48.java (erase me later)
package sandbox;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


import sandbox.SandboxParameters;
import sandbox.
SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(		
	description = "Analyze list (newline delimiter)",
	parameterDescriptions = 
		{
//		"1","string",
			SandboxParameters.FOCUSED_NAME,"list",
//			SandboxParameters.SCRATCHPAD_NAME,"scratchPadString",
//			SandboxParameters.PASSWORD_NAME,"password",
//			SandboxParameters.POPPAD_NAME,"poppad",
//			SandboxParameters.TRUEFALSE_NAME,"spaceCounts",
			SandboxParameters.YESNO_NAME,"casingCounts"
		},showInButtonPanel = true, showInTabPanel = true,isTest = true,isTool = false)

public class Tool48 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TOOL
		List<String> list = getStringList("list");
		boolean casingCounts = getBoolean("casingCounts");
		
		Set<String> set = new HashSet<String>();
		List<String> dups = new ArrayList<String>();
		
		for (String string : list){
			if (!casingCounts) {
				string = string.toLowerCase ();
			}
			if (set.contains(string)){
				dups.add(string);
			}else {
				set.add(string);
			}
		}
		
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("SET: (size "+ set.size()+ ")");
		for (String string : set){
			printLine(string);
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("DUPS: (size "+ dups.size()+ ")");
		for (String string : dups){
			printLine(string);
		}
		printLine(SandboxParameters.VISUAL_SEPARATOR);
		printLine("TOTAL "+ list.size());
		//END TOOL
	}
}