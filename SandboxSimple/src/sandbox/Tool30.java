// Tool30.java (erase me later)
package sandbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import sandbox.SandboxParameters;
import sandbox.SandboxTestBase;
import sandbox.SandboxAnnotation;

@SandboxAnnotation(
		description = "Synchronize directories",
		parameterDescriptions = 
		{
			SandboxParameters.FOCUSED_NAME,"activeDirectory,regex,duplicateDirectory",
			SandboxParameters.TRUEFALSE_NAME,"update",
			SandboxParameters.YESNO_NAME,"overrideConflicts"
		}, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)

public class Tool30 extends SandboxTestBase {

	@Override
	public void test(SandboxParameters params)throws Exception {
		super.test(parameterDescriptions);
		//BEGIN TEST
		String activeDirectory = getString("activeDirectory");
		String regex = getString("regex");
		String duplicateDirectory = getString("duplicateDirectory");
		boolean update = getBoolean("update");
		boolean overrideConflicts = getBoolean("overrideConflicts");
		
		//Check that duplicate is older than active
		File activeDir = new File(activeDirectory);
		File dupDir = new File (duplicateDirectory);
		if (!activeDir.exists() || !dupDir.exists()){
			throw new SandboxException("Directories don't exist");
		}
		
		long lastSynch = 0;
		File synchFile = new File(dupDir.getPath() + File.separator + SYNCH_FILE_NAME);
		if (synchFile.exists()) {
			lastSynch = synchFile.lastModified();
		} else {
			lastSynch = new Date().getTime();
			Utilities.saveFileString(dupDir.getPath() + File.separator + SYNCH_FILE_NAME, getTime());
		}
		if (update){
//			if (activeDir.lastModified() < dupDir.lastModified()){
//				if (!confirm("Active directory is newer than duplicate directory"
//						+ Utilities.parseDate(new Date (activeDir.lastModified()),SandboxParameters.DATE_FORMAT)+ ":"
//						+ Utilities.parseDate(new Date(dupDir.lastModified ()),SandboxParameters.DATE_FORMAT)
//						+ ". \n\n\tPROCEED?")){
//					printLine("Aborted");
//					return;
//				}
//			}
			if (!confirm("PROCEED (last chance)?")){
				printLine("Aborted");
				return;
			}
		}

		/**
		 * 	public static Collection<File> listFilesRegex(File directory, boolean recurse, boolean onlyFiles, Pattern pattern,
			SandboxTestBase test) {
		 */
		Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		Collection<File> activeFiles = sandbox.Utilities.listFilesRegex(new File(activeDirectory), true, true, pattern, this);

//		Collection<File> activeFiles = sandbox.Utilities.listFiles(new File(activeDirectory),
//				new SandboxFilenameFilter(extensions),
//				null,
//				true,this);
		
		List<File> filteredFiles = new ArrayList<File>();
		for (File file : activeFiles){
			try {
				if (file.isFile() && !file.getName().equals(SYNCH_FILE_NAME)){
					filteredFiles.add(file);
					
					String fileLeaf = file.getPath().substring(activeDirectory.length());
					File copy = new File(duplicateDirectory + fileLeaf);
					if (copy.exists()){
						boolean isFileNewerThanLastSynch = file.lastModified() > lastSynch;
						if (isFileNewerThanLastSynch){
							printLine("UPDATE FROM/TO:\n\t"+ file.getPath() + "\n\t" + copy.getPath());
							if (synchFile.lastModified() < copy.lastModified() && !overrideConflicts) {
								printLine("\tNOT UPDATED: POSSIBLE CONFLICT");
							} else if (update){
								Utilities.copyFile(file,copy);
								Utilities.saveFileString(dupDir.getPath() + File.separator + SYNCH_FILE_NAME, getTime());
								Utilities.saveFileString(activeDir.getPath() + File.separator + SYNCH_FILE_NAME, getTime());
							}
							
						}
					} else {//The file doesn't exist
						printLine("COPY FROM/TO:\n\t" + file.getPath() + "\n\t" + copy.getPath());
						if (update){
							//copy file over
							Utilities.copyFile(file, copy, true);
							Utilities.saveFileString(dupDir.getPath() + File.separator + SYNCH_FILE_NAME, getTime());
							Utilities.saveFileString(activeDir.getPath() + File.separator + SYNCH_FILE_NAME, getTime());
						}
					}		
				}	
			} catch(FileNotFoundException fnfe) {
				printLine("\t*** Could not synch ***" + file.getPath());
				printLine(fnfe);
			}
		}	
		//END TEST
	}
	private static String SYNCH_FILE_NAME = "sandbox_synch.sbf";
}	