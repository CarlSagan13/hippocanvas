//SandboxSerializable.java (erase me later)
package sandbox;

import java.io.Serializable;

public class SandboxSerializable implements Serializable {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private Object obj;

	public SandboxSerializable(Object obj) {
		this.obj = obj;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
