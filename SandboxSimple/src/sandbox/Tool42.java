package sandbox;

import java.util.Map;

@SandboxAnnotation(
        description = "Remove objects from global map",
 		parameterDescriptions = {
 			SandboxParameters.FOCUSED_NAME, "mapIdRegex",
			SandboxParameters.TRUEFALSE_NAME, "killRunning",
        }, showInButtonPanel = true, showInTabPanel = true, isTest = true, isTool = false)
 
public class Tool42 extends SandboxTestBase {
	 
	@Override
	public void test(SandboxParameters params) throws Exception {
		super.test(parameterDescriptions);
		// BEGIN TEST
		String mapIdRegex = getString("mapIdRegex");
		boolean killRunning = getBoolean("killRunning");

		Globals globals = this.getParameters().getGlobals();
		
		Map<String, SandboxConnection> map =  globals.getMap();
		for (String key : map.keySet()) {
			if (key.matches(mapIdRegex)) {
				printLine(key);
				if (map.get(key).getObj() instanceof SandboxProcess) {
					SandboxProcess process = (SandboxProcess) map.get(key).getObj();
					if (!process.done && !killRunning) {
						printLine("\t" + key + " is a running process:");
						printALine();
						printLine(process.getHistory());
						printALine();
					} else {
						process.close();
						globals.getMap().remove(key);
					}
				} else {
					globals.getMap().remove(key);
				}
			}
		}
		// END TEST
	}
}
