@echo off
set SBX_WKSP=C:\Users\David\eclipse\workspaceOne\Sandbox\
set SANDBOX_CLASSPATH=%SBX_WKSP%;%SBX_WKSP%bin;%SBX_WKSP%lib\commons-codec-1.3.jar;%SBX_WKSP%lib\log4j-core-2.11.1.jar;%SBX_WKSP%lib\log4j-api-2.11.1.jar;%SBX_WKSP%lib\commons-httpclient-3.1.jar;%SBX_WKSP%lib\commons-lang.jar;%SBX_WKSP%lib\jmf.jar;%SBX_WKSP%lib\spring.jar;%SBX_WKSP%lib\gson-2.6.2.jar;%SBX_WKSP%lib\diffutils-1.3.0.jar
set SANDBOX_ENVIRONMENT=-Dsbx.conf.listFont=Dialog_2_12 -Dcatalina.base=C:\Users\David\SANDBOX_HOME_NEW 
java %SANDBOX_ENVIRONMENT% -cp %SANDBOX_CLASSPATH% sandbox.Start C:\Users\David\SANDBOX_HOME_NEW
rem pause