package sandbox;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.Position;

public class SandboxShell extends SandboxThread {
	
	static String SBXPROCMAPID = "sbxproc";	
	static private String MARK_ENTRY = "sbxmarkentry";
	static private String SANDBOX_EDIT = "sbxedit";
	List<String> forbiddenRegex = new ArrayList<String>();

	List<String> history = new ArrayList<String>();	int historyPointer = 0;
	String strHistory = "sbxhistory";
	StringBuilder command = new StringBuilder();	StringBuilder lastCommand = null;
	StringBuilder prompt = new StringBuilder();
	InputStream inputStream = null;
	BufferedReader bufferedReader = null;
	BufferedReader errorBufferedReader = null;
	BufferedWriter procStdIn = null;

	OutputStream outputStream = null;
	OutputStreamWriter outputStreamWriter = null;
	InputStream errorStream = null;
	String strProcessEnd = "exit";

	String mapId = String.valueOf((new Date().getTime()));

	String lineSeparator = System.getProperty("line.separator","\n");

	ShowProcessOutput showProcOut = null;

	SandboxShellKeyListener shellKeyListener = null;

	String baseHost = "host";
	String baseUser = "user";
	boolean canIExit = false;
	private int frequencyInMilliseconds = 100;	private int windowSize = 1000;
	private boolean isRunning = false;
	private boolean entryCommandNext = false;

	public void run(){
		isRunning = true;
	}
	
	public SandboxShell(){
		try {
			//Set up the process
			SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		
			//Get formidden commands
			for (String commandRegex : parameters.properties.getProperty("forbiddenShellCommands", "\\bping").split(",")) {	
				forbiddenRegex.add(commandRegex);
			}
		
			for (int index= 0;index < 100;index++){
				String tryMapId = SBXPROCMAPID + index;
				if (parameters.getGlobals().getMap().get(tryMapId)== null){
					mapId = tryMapId;
					break;
				}
			}
			SandboxConnection	connection = Utilities.createSandboxConnection(mapId);
			if (connection.getOwner()!= null){
				parameters.printLine("Process " + mapId + " in use by "+ connection.getOwner());
				throw new sandbox.SandboxException("Process "+ mapId + "in use by "+ connection.getOwner());
			}
			initStreams();
		
			//Get baseline
			baseHost = uname();
			baseUser = whoami();
			prompt = getPrompt();
			SandboxTextArea textArea = new SandboxTextArea (this);
		
			try {
				String[] clf = System.getProperty("sandbox.conf.shellFont").split ("_");
				Font shellFont = new Font(clf[0], Integer.parseInt(clf[1]),Integer.parseInt(clf[2]));
				textArea.setFont(shellFont);
			}catch(Exception en){
				System.out.println("sandbox.conf.shellFont not set correctly (<Font Name>_<Type>_<Size> e.g.'"+ SandboxParameters.FONT_DEFAULT + "')");
			}
			textArea.setBackground(Color.black);
			textArea.setForeground(Color.white);
		
			textArea.setEditable(false);
		
			shellKeyListener = new SandboxShellKeyListener(this);
			textArea.addKeyListener(shellKeyListener);
			DefaultCaret caret = (DefaultCaret)textArea.getCaret();
			caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			textArea.append("Starting Sandbox Shell "+ lineSeparator + prompt);
		
			parameters.newTab(textArea, mapId);
			parameters.tabbedPane.setSelectedIndex(parameters.tabbedPane.getTabCount()- 1);		
			try {
				String historyString = Utilities.getFileString(parameters.getConfigurationDirectory()+ System.getProperty("file.separator","/")+ "history.sbf");
				String[]historyArray = historyString.split("\n");
				for (String historyCommand : historyArray){
					history.add(historyCommand);
				}
//page break
				this.historyPointer = history.size ();
			}catch(IOException ioe){				parameters.printLine("Could not load shell history",true,true);
			}
//			startBlinkingCursor(textArea);		
		}catch(Exception en){
			System.err.println("Could not start "+ SandboxShell.class.getName()+ ": "+ en);
		}
	}

	private void initStreams(){
		SandboxParameters parameters = SandboxParameters.createSandboxParameters();
		Process proc = (Process)parameters.getGlobals().getMap().get(mapId).getObj();
		inputStream = proc.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		bufferedReader = new BufferedReader(inputStreamReader);
	
		errorStream = proc.getErrorStream();		InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
		errorBufferedReader = new BufferedReader(errorStreamReader);
	
		outputStream = proc.getOutputStream();		outputStreamWriter = new OutputStreamWriter(outputStream);
		procStdIn = new BufferedWriter(outputStreamWriter);
	}
	
	private StringBuilder getPrompt(){
		StringBuilder prompt = new StringBuilder();		
		try {
			String user = whoami();
			prompt.append("[").append (user).append("@");
		
			String host = uname();
			prompt.append(host);
		
			prompt.append(pwd()).append ("]");
		
			if (user.equals(baseUser)&&host.equals(baseHost)){
				this.canIExit = false;
			}else {
				this.canIExit = true;
			}
		
		}catch(Exception en){
			prompt = new StringBuilder("[undetermined]");
			System.err.println(en);
		}
		return prompt;
	}

	private String whoami(){
		try {
			procStdIn.write("whoami");			procStdIn.newLine();
			procStdIn.flush();
			return bufferedReader.readLine();
		}catch(Exception en){
			System.err.println(en);
			return "user";
		}
	}

	private String uname(){
		try {
			procStdIn.write("uname -n");
			procStdIn.newLine();
			procStdIn.flush();
			String line = bufferedReader.readLine();
			return line.indexOf('.') > 0 ? line.substring(0, line.indexOf('.')) : line;
		}catch(Exception en){
			System.err.println(en);
			return "host";
		}
	}

	private String pwd(){
		try {
			procStdIn.write("pwd");			procStdIn.newLine();
			procStdIn.flush();
			String line = bufferedReader.readLine();
			return line.substring(line.lastIndexOf ('/'));
		}catch(Exception en){
			System.err.println(en);
			return "dir";
		}
	}

	private String killLastProcess(){
		try {
			procStdIn.write("ps -af | grep '" + history.get(historyPointer)+ "'");
			procStdIn.newLine();
			procStdIn.flush();
			String line = bufferedReader.readLine();
			return line.split("")[1];
		
		}catch(Exception en){
			System.err.println(en);
			return "dir";
		}
	}

	class SandboxShellKeyListener implements KeyListener {

		private SandboxShell shell;
		private boolean enabled = true;
		public SandboxShellKeyListener(SandboxShell shell){
			this.shell = shell;
		}
	
		@Override
		public void keyTyped(KeyEvent e){
		}
		@Override
		public void keyReleased(KeyEvent e){
		}
		@Override
		public void keyPressed(KeyEvent event){
			if (enabled){
				SandboxTextArea textArea = ((SandboxTextArea)event.getComponent());
			
				if (event.isControlDown()){//Control key events
					switch (event.getKeyCode()){
					case KeyEvent.VK_V:
						shell.keyControl_V(textArea);
						break;
//					case KeyEvent.VK_B:
//						shell.keyControl_B ();
//						break;
					case KeyEvent.VK_C:
						shell.keyControl_C()
;
						break;
					}
				}else {
					switch (event.getKeyCode()){
						case KeyEvent.	VK_ENTER:
							shell.keyReturn(
textArea);
							break;
						
//page break
						case KeyEvent.VK_BACK_SPACE:
							shell.keyBackspace(
textArea);						
							break;
						case KeyEvent.VK SHIFT:
							break;
						case KeyEvent.VK_UP:
							shell.keyControl_up(
textArea);
							break;
						case KeyEvent.VK_DOWN:
							shell.keyControl_down(
textArea)		• ;				
							break;
						
						default:
							if (!event.
isActionKey()){
								shell.key(
textArea,event.<		getKeyChar())	;
							}	
					}		
						
				}		
						
			}			
						
		}				
		public void setEnabled(boolean enabled){			this.enabled = enabled;
		}

	}
	class ShowProcessOutput implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class CaptureProcessOutput implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
